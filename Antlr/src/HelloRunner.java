 import org.antlr.v4.runtime.*;
    import org.antlr.v4.runtime.tree.*;
    public class HelloRunner 
    {
    	public static void main( String[] args) throws Exception 
    	{
    
    		ANTLRInputStream input = new ANTLRInputStream( System.in);
    
    		JavaLexer lexer = new JavaLexer(input);
    
    		CommonTokenStream tokens = new CommonTokenStream(lexer);
    
    		JavaParser parser = new JavaParser(tokens);
    		//ParseTree tree = parser.r(); // begin parsing at rule 'r'
    	//	System.out.println(tree.toStringTree(parser)); // print LISP-style tree
    	}
}
