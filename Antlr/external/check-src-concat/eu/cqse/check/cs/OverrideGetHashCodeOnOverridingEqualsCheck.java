/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: OverrideGetHashCodeOnOverridingEqualsCheck.java 56512 2016-04-06 05:25:09Z poehlmann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.cs;

import eu.cqse.check.clike.HashCodeAndEqualsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.util.ShallowParsingUtils;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: poehlmann $
 * @version $Rev: 56512 $
 * @ConQAT.Rating YELLOW Hash: 1AD125A2C0E763FE86A6AEDAF48F7FAF
 */
@ACheck(name = "Override GetHashCode on overriding Equals", description = "A public type "
		+ "overrides Object.Equals but does not override Object.GetHashCode or vice versa.", groupName = CheckGroupNames.CORRECTNESS, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CS }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class OverrideGetHashCodeOnOverridingEqualsCheck
		extends HashCodeAndEqualsCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getEqualsName() {
		return "Equals";
	}

	/** {@inheritDoc} */
	@Override
	protected String getHashCodeName() {
		return "GetHashCode";
	}

	/**
	 * Returns the first method entity of the given type, that has the given
	 * name, parameter count. For CS, the method must have an override modifier.
	 */
	@Override
	protected ShallowEntity getMethodEntity(ShallowEntity type, String name,
			int parameterCount) throws CheckException {
		/*for (ShallowEntity methodEntity : select(type,
				"METHOD[subtype('method') and name-matches('" + name + "')]")) {
			// TODO (MP) I think you can add a and modifier('override') in above
			// expression and remove the one below
			// TODO (MP) Instead of overriding the complete method you can then
			// just create a method getMethodSelector(name) that returns the
			// xpath expression and move the rest back to the base class.
			int overrideIndex = TokenStreamUtils
					.find(methodEntity.ownStartTokens(), ETokenType.OVERRIDE);
			if (ShallowParsingUtils.extractParameterNameTokens(methodEntity)
					.size() == parameterCount
					&& overrideIndex != TokenStreamUtils.NOT_FOUND) {
				return methodEntity;
			}
		}*/
		return null;
	}
}
