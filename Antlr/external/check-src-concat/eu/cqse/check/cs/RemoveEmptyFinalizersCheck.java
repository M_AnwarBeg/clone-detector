/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: RemoveEmptyFinalizersCheck.java 53185 2015-07-03 13:54:40Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.cs;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: 8001B90B079E2458412301835EA654DE
 */
@ACheck(name = "Remove empty finalizers", description = "A type implements a "
		+ "finalizer that is empty, calls only the base type finalizer, or calls only "
		+ "conditionally emitted methods.", groupName = CheckGroupNames.UNUSED_CODE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CS }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class RemoveEmptyFinalizersCheck extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE/METHOD[subtype('destructor')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		if (!entity.hasChildren()) {
			createFinding("Empty finalizer must be removed.", entity);
		}
	}
}
