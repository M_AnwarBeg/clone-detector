/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: PointersShouldNotBeVisibleCheck.java 56019 2016-02-29 16:10:33Z heinemann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.cs;

import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.ScopedTypeLookup;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 56019 $
 * @ConQAT.Rating GREEN Hash: C73A33E35D4B13A9AFE903DCF581218E
 */
@ACheck(name = "Pointers should not be visible", description = "A public or protected "
		+ "System.IntPtr or System.UIntPtr field is not read-only.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CS }, parameters = {
						ECheckParameter.TYPE_RESOLUTION, ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class PointersShouldNotBeVisibleCheck extends EntityCheckBase {

	/**
	 * Set of pointer types that must not be public or protected without being
	 * readonly.
	 */
	private static final Set<String> POINTER_TYPE_NAMES = CollectionUtils
			.asHashSet("System.IntPtr", "System.UIntPtr");

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE/ATTRIBUTE[anymodifier('public','protected') and not(modifiers('readonly'))]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> fieldNames = LanguageFeatureParser.CS
				.getVariableNamesFromTokens(entity.ownStartTokens());
		if (fieldNames.isEmpty()) {
			return;
		}

		ScopedTypeLookup typeLookup = context.getTypeResolution()
				.getTypeLookup(entity);
		String fieldName = CollectionUtils.getAny(fieldNames).getText();

		for (String illegalTypeName : POINTER_TYPE_NAMES) {
			if (typeLookup.containsVariable(fieldName) && typeLookup
					.hasFullQualifiedTypeName(fieldName, illegalTypeName)) {
				createFinding(
						"Fields of type '" + illegalTypeName
								+ "' must not be public or protected without being readonly.",
						entity);
				break;
			}
		}
	}
}
