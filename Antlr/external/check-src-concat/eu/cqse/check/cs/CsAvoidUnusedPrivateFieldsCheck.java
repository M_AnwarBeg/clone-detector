/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: CsAvoidUnusedPrivateFieldsCheck.java 56580 2016-04-07 11:13:13Z kupka $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.cs;

import eu.cqse.check.clike.CLikeAvoidUnusedPrivateFieldsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kupka $
 * @version $Rev: 56580 $
 * @ConQAT.Rating YELLOW Hash: BE57D575DE74B4CDA67BE7143DBE5CCC
 */
@ACheck(name = "Avoid unused private fields (C#)", description = "This rule is reported when a "
		+ "private field in your code exists but is not used by any code path.", groupName = CheckGroupNames.UNUSED_CODE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CS }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE,
						ECheckParameter.TYPE_RESOLUTION })
public class CsAvoidUnusedPrivateFieldsCheck
		extends CLikeAvoidUnusedPrivateFieldsCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Ignore partial classes", description = "Whether to ignore partial classes in the unused private fields check.")
	private boolean ignorePartialClasses = false;

	/** Constructor. */
	public CsAvoidUnusedPrivateFieldsCheck() {
		super(LanguageFeatureParser.CS);
	}

	/** {@inheritDoc} */
	@Override
	protected String getFieldSelectionXPath() {
		StringBuilder builder = new StringBuilder("//TYPE");
		if (ignorePartialClasses) {
			builder.append("[not(tokens('partial'))]");
		}
		builder.append("/ATTRIBUTE[subtype('");
		builder.append(SubTypeNames.ATTRIBUTE);
		builder.append("') and modifiers('private')]");
		return builder.toString();
	}

	/** {@inheritDoc} */
	@Override
	protected String getProcessedEntitiesXPath() {
		// TODO (MP) does this also check if/while conditions? In general isn't
		// there a better way to search for variable usage? If not extract to
		// common place?
		// TODO (AK) The place where variables can be used is rather language
		// dependent, therefore it is specified separately in every check.
		// If/while conditions are covered by the STATEMENT selector
		return "descendant::METHOD | descendant::ATTRIBUTE | descendant::STATEMENT | descendant::META[subtype('"
				+ SubTypeNames.CASE + "')]";
	}
}
