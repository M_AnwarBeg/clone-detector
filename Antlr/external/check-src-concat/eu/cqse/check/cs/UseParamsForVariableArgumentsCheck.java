package eu.cqse.check.cs;

import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 56074 $
 * @ConQAT.Rating GREEN Hash: 74338D0C6FD93704A9901F84ECD93EEF
 */
@ACheck(name = "Use params for variable arguments", description = "A public or "
		+ "protected type contains a public or protected method that uses the VarArgs "
		+ "calling convention. ", groupName = CheckGroupNames.BAD_PRACTICE, languages = { ELanguage.CS }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UseParamsForVariableArgumentsCheck extends EntityCheckBase {

	/** The identifier that is used for variable arguments. */
	private static final String ARGLIST_IDENTIFIER = "__arglist";

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE[anymodifier('public', 'protected')]/METHOD[anymodifier('public', 'protected')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> parameterTokens = TokenStreamUtils.tokensBetweenWithNesting(entity.ownStartTokens(),
				ETokenType.LPAREN, ETokenType.RPAREN);
		int argListIndex = TokenStreamTextUtils.findFirst(parameterTokens, ARGLIST_IDENTIFIER);
		if (argListIndex != TokenStreamUtils.NOT_FOUND) {
			createFinding("Use 'params' instead of '" + ARGLIST_IDENTIFIER + "' for variable arguments.",
					parameterTokens.get(argListIndex));
		}
	}
}
