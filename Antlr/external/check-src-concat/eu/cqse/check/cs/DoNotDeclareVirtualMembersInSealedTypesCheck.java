package eu.cqse.check.cs;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: 93FAA08E8FF182DA97F9E7CF8B0677BF
 */
@ACheck(name = "Do not declare virtual members in sealed types", description = "A public "
		+ "type is sealed and declares a method that is both virtual (Overridable in Visual Basic) "
		+ "and not final. This rule does not report violations for delegate types, which must follow "
		+ "this pattern.", groupName = CheckGroupNames.BAD_PRACTICE, languages = { ELanguage.CS }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class DoNotDeclareVirtualMembersInSealedTypesCheck extends EntityFindingCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE[modifiers('public','sealed')]/METHOD[subtype('method') and modifiers('virtual') and not(modifiers('sealed'))]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return "A sealed type must not have virtual members.";
	}
}
