/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cs;

import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.THIS;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.SetMap;

import eu.cqse.check.clike.CLikeAvoidUnusedPrivateMethodsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.languages.cs.CsShallowParser;
import eu.cqse.check.framework.util.LanguageFeatureParser;
import eu.cqse.check.framework.util.tokens.TokenPattern;
import eu.cqse.check.framework.util.tokens.TokenPatternMatch;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kupka $
 * @version $Rev: 56583 $
 * @ConQAT.Rating YELLOW Hash: E45ED47DE4412443F8C00184CE5D4431
 */
@ACheck(name = "Avoid unused private methods (C#)", description = CLikeAvoidUnusedPrivateMethodsCheckBase.DESCRIPTION, groupName = CheckGroupNames.UNUSED_CODE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.CS }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class CsAvoidUnusedPrivateMethodsCheck
		extends CLikeAvoidUnusedPrivateMethodsCheckBase {

	/**
	 * Stores names and arities of methods that are excluded from this checks.
	 */
	private static final SetMap<String, Integer> EXCLUDED_METHODS = new SetMap<>();

	static {
		// TODO (MP) If I look at the test code and around in the net, I do not
		// find a reason why e.g. Dispose(IDisposable) should not be detected by
		// our check, maybe discuss briefly in person?
		// TODO (AK) I am not quite sure but as far as I understand the
		// documentation at
		// https://msdn.microsoft.com/en-us/library/fs2xkftw%28v=vs.110%29.aspx
		// a private Dispose method has nothing to do with IDisposable and thus
		// should not be included by this check. If you agree, just remove the
		// EXCLUDED_METHODS field and getExcludedMethods method.
		EXCLUDED_METHODS.add("Dispose", 1);
	}

	/** Pattern that matches a possible reference to a method. */
	private static final TokenPattern REFERENCE_PATTERN = new TokenPattern()
			.sequence(CsShallowParser.VALID_IDENTIFIERS).group(0)
			.sequence(EnumSet.complementOf(EnumSet.of(LPAREN)));

	/** Constructor. */
	public CsAvoidUnusedPrivateMethodsCheck() {
		super(LanguageFeatureParser.CS);
	}

	/** {@inheritDoc} */
	@Override
	protected Set<Integer> getMethodCallArities(
			List<List<IToken>> parameterTokens) {
		Set<Integer> callArities = super.getMethodCallArities(parameterTokens);
		int parameterCount = parameterTokens.size();
		if (parameterCount > 0
				&& TokenStreamUtils.containsAll(parameterTokens.get(0), THIS)) {
			callArities.add(parameterCount - 1);
		}
		return callArities;
	}

	/** {@inheritDoc} */
	@Override
	protected void retrieveMethodReferences(List<IToken> tokens,
			SetMap<String, Integer> calledMethods) {
		// TODO (MP) I'd just return a set of method names and add the
		// UNKNOWN_ARITY in the base class
		// TODO (AK) I'd keep this method as is because there may be references
		// in other languages where you can determine the call arity
		for (TokenPatternMatch match : REFERENCE_PATTERN.findAll(tokens)) {
			calledMethods.add(match.groupString(0), UNKNOWN_ARITY);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected SetMap<String, Integer> getExcludedMethods() {
		return EXCLUDED_METHODS;
	}
}
