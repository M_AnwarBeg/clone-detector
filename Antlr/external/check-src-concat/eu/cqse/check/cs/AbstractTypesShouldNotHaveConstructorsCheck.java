package eu.cqse.check.cs;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: 1E6C9577054AC2129A8244EC796782EC
 */
@ACheck(name = "Abstract types should not have constructors", description = "Constructors "
		+ "on abstract types can be called only by derived types. Because public constructors create "
		+ "instances of a type, and you cannot create instances of an abstract type, an abstract type "
		+ "that has a public constructor is incorrectly designed.", groupName = CheckGroupNames.BAD_PRACTICE, languages = {
				ELanguage.CS }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class AbstractTypesShouldNotHaveConstructorsCheck extends EntityFindingCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE[modifiers('public', 'abstract')]/METHOD[subtype('constructor') and modifiers('public')]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return "An abstract type must not have a public constructor.";
	}
}
