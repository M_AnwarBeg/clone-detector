package eu.cqse.check.cs;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.base.EntityTokenCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: 419ADFB5FA08633D2305911C898AF9D5
 */
@ACheck(name = "Test for NaN correctly", description = "An expression tests a value against Single.Nan "
		+ "or Double.Nan.", groupName = CheckGroupNames.CORRECTNESS, languages = { ELanguage.CS }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class TestForNaNCorrectlyCheck extends EntityTokenCheckBase {

	/**
	 * The token offset of a NaN sequence as left operand of a binary operator.
	 */
	private static final int LEFT_OPERAND_OFFSET = -3;

	/**
	 * The token offset of a NaN sequence as right operand of a binary operator.
	 */
	private static final int RIGHT_OPERAND_OFFSET = 1;

	/** Token text sequences that represent the NaN value. */
	private static final Set<String[]> NAN_SEQUENCES = CollectionUtils
			.asHashSet(new String[] { "Single", ".", "NaN" }, new String[] {
					"Double", ".", "NaN" });

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT | //ATTRIBUTE";
	}

	/** Tests if the given tokens contain a test against NaN. */
	@Override
	protected void processTokens(List<IToken> tokens) throws CheckException {
		List<Integer> operatorIndices = TokenStreamUtils.findAll(tokens,
				EnumSet.of(ETokenType.EQEQ, ETokenType.NOTEQ));

		for (Integer operatorIndex : operatorIndices) {
			if (!checkIndexForNaN(tokens, operatorIndex + LEFT_OPERAND_OFFSET)) {
				checkIndexForNaN(tokens, operatorIndex + RIGHT_OPERAND_OFFSET);
			}
		}
	}

	/**
	 * Checks and reports a finding, if one of the NaN text sequences is found
	 * at the given index.
	 */
	private boolean checkIndexForNaN(List<IToken> tokens, int index)
			throws CheckException {
		for (String[] nanSequence : NAN_SEQUENCES) {
			if (TokenStreamTextUtils.hasSequence(tokens, index, nanSequence)) {
				createFinding(
						"A value must not be tested against '"
								+ StringUtils.concat(nanSequence,
										StringUtils.EMPTY_STRING) + "'.",
						tokens.get(index));
				return true;
			}
		}
		return false;
	}
}
