/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cs;

import org.conqat.lib.commons.collections.UnmodifiableList;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kupka $
 * @version $Rev: 56499 $
 * @ConQAT.Rating YELLOW Hash: CCCED489520CFAE4C3C6625E5B12A211
 */
@ACheck(name = "Use literals where appropriate", description = "A field is declared static "
		+ "and readonly and initialized with a value that is computable at compile time. "
		+ "Change the declaration to a const field so that the value is computed at compile "
		+ "time instead of at runtime. ", groupName = CheckGroupNames.PERFORMANCE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CS }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UseLiteralsWhereAppropriate extends EntityCheckBase {

	/**
	 * Returns an XPath selection string that selects attributes. This matches
	 * fields, but neither properties nor events.
	 */
	@Override
	protected String getXPathSelectionString() {
		// TODO (MP) Can't we use subtype(..) and modifiers('static',
		// 'readonly')) instead of hasTheModifiers in processEntity
		return "//ATTRIBUTE[subtype('attribute')]";
	}

	/**
	 * Given an entity that is a field declaration, creates a finding if the
	 * field has static and readonly modifiers and its right-hand side is a
	 * constant.
	 */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		UnmodifiableList<IToken> ownTokens = entity.ownStartTokens();
		boolean hasTheModifiers = TokenStreamUtils.containsAll(ownTokens,
				ETokenType.STATIC, ETokenType.READONLY);
		if (!hasTheModifiers) {
			return;
		}

		// Check whether there is a constant between EQ and SEMICOLON
		int eqIndex = TokenStreamUtils.find(ownTokens, ETokenType.EQ);
		int semicolonIndex = TokenStreamUtils.find(ownTokens,
				ETokenType.SEMICOLON);
		if (eqIndex == TokenStreamUtils.NOT_FOUND
				|| semicolonIndex == TokenStreamUtils.NOT_FOUND
				|| semicolonIndex != eqIndex + 2) {
			return;
		}
		IToken rhsToken = ownTokens.get(eqIndex + 1);
		if (ETokenType.LITERALS.contains(rhsToken.getType())) {
			createFindingOnFirstLine("Use literals where appropriate.", entity);
		}
		// TODO (MP) As we check exactly the sequence EQ, LITAERALS, SEMICOLON,
		// can you try whether TokenStreamUtils.findTokenTypeSequences(tokens,
		// 0, EQ, LITAERALS, SEMICOLON); works as well? Andre suggested this for
		// me in ThrowNotImplementedExceptionCheck.
		// TODO (AK) As LITERALS contains multiple token types this won't work,
		// but you can use eu.cqse.check.framework.util.tokens.TokenPattern for
		// more complex patterns
	}

}
