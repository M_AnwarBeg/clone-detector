/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: CCE9009FC8932D0F56ACA779D0E910D3
 */
@ACheck(name = "Too Many Constructors", description = "Classes should not contain more than the configured number of constructors (default=5). Too many constructors make the class difficult to use and should thus be avoided.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class NumberOfConstructorsCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Maximum number of constructors", description = "The maximum number of constructors that are allowed.")
	private int maximumNumberOfConstructors = 5;

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		/*int actualNumberOfConstructors = select(entity,
				"METHOD[subtype('constructor')]").size();
		if (actualNumberOfConstructors > maximumNumberOfConstructors) {
			createFindingOnFirstLine(
					"Too many constructors (" + actualNumberOfConstructors + "/"
							+ maximumNumberOfConstructors + ")",
					entity);
		}*/
	}
}
