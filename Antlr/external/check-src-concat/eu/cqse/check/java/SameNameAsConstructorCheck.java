/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: SameNameAsConstructorCheck.java 54535 2015-09-29 15:53:49Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: 9D237C013C300E98FF7BBCB9EB1E176D
 */
@ACheck(name = "Method must not be named like constructor", description = "A method should not be named like "
		+ "its containing type's constructor, because this can lead to confusion.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class SameNameAsConstructorCheck extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE/METHOD[subtype('" + SubTypeNames.METHOD
				+ "') or subtype('" + SubTypeNames.STATIC_METHOD + "')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		String methodName = entity.getName();
		if (methodName.equals(entity.getParent().getName())) {
			createFindingOnFirstLine(
					"Method " + methodName
							+ " must not be named like the constructor",
					entity);
		}
	}
}
