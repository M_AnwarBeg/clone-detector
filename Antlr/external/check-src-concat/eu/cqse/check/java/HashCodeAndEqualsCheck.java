/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.clike.HashCodeAndEqualsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 55467 $
 * @ConQAT.Rating GREEN Hash: 56750721ED6F02A1F9A9C6192EFD9090
 */
@ACheck(name = "hashCode() and equals()", description = "A class implementing one of equals() or hashCode() should also implement the "
		+ "other.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA, ELanguage.XTEND }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class HashCodeAndEqualsCheck extends HashCodeAndEqualsCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getEqualsName() {
		return "equals";
	}

	/** {@inheritDoc} */
	@Override
	protected String getHashCodeName() {
		return "hashCode";
	}
}
