/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: UnwantedConstructorCallsCheck.java 54700 2015-10-14 15:41:52Z heinemann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.STRING_LITERAL;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.base.EntityTokenCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 54700 $
 * @ConQAT.Rating GREEN Hash: E4EAD208F449C13EDD21C64CBF6437FD
 */
@ACheck(name = "Unwanted constructor calls", description = "Constructors of some types, e.g. primitive "
		+ "wrappers and String must not be called, because they only create extra objects which take up "
		+ "more memory and reduce performance, without any other functional effect.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UnwantedConstructorCallsCheck extends EntityTokenCheckBase {

	/** Token type sequence that indicates a constructor call. */
	private static final ETokenType[] CONSTRUCTOR_CALL_SEQUENCE = { NEW,
			IDENTIFIER, LPAREN };

	/**
	 * Token types of the first parameter of the String constructor that
	 * indicate a forbidden constructor call. This is either a string literal or
	 * the constructor without parameters (thus there is a closing parenthesis).
	 */
	private static final EnumSet<ETokenType> FORBIDDEN_STRING_CONSTRUCTOR_PARAMETER_TOKENS = EnumSet
			.of(STRING_LITERAL, RPAREN);

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Unwanted constructor names", description = "All constructor names, that shall not be called.")
	private final List<String> constructorNames = Arrays.asList("Boolean",
			"Byte", "Short", "Integer", "Long", "Float", "Double", "String");

	/** {@inheritDoc} */
	@Override
	public String getXPathSelectionString() {
		return "//STATEMENT | //ATTRIBUTE";
	}

	/** {@inheritDoc} */
	@Override
	public void processTokens(List<IToken> tokens) throws CheckException {
		List<Integer> callIndices = TokenStreamUtils
				.findTokenTypeSequences(tokens, 0, CONSTRUCTOR_CALL_SEQUENCE);
		for (int callIndex : callIndices) {
			if (isAllowedStringCall(tokens, callIndex)) {
				continue;
			}

			IToken typeToken = tokens.get(callIndex + 1);
			String typeName = typeToken.getText();
			for (String wrapperName : constructorNames) {
				if (wrapperName.equals(typeName)) {
					createFinding(
							"Constructor of type " + wrapperName
									+ " must not be called explicitly",
							typeToken);
					return;
				}
			}
		}
	}

	/**
	 * Checks the special case of a String constructor call. If the given index
	 * is a call of the String constructor and its parameter is no string
	 * literal (but there is a parameter) the call is allowed and this method
	 * returns true. Otherwise false is returned.
	 */
	private static boolean isAllowedStringCall(List<IToken> tokens,
			int callIndex) {
		String typeName = tokens.get(callIndex + 1).getText();
		int parameterIndex = callIndex + 3;

		if (typeName.equals(String.class.getSimpleName())
				&& parameterIndex < tokens.size()) {
			ETokenType parameterType = tokens.get(parameterIndex).getType();
			return !FORBIDDEN_STRING_CONSTRUCTOR_PARAMETER_TOKENS
					.contains(parameterType);
		}
		return false;
	}
}
