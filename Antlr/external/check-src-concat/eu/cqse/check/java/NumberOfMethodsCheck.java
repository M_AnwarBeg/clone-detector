/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: 7D393650E23C7ACAA6571321669A00FF
 */
@ACheck(name = "Too Many Methods", description = "Classes should not contain more than then configured number of methods (default=32) as this indicates a high complexity that is difficult to understand.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class NumberOfMethodsCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Maximum number of methods", description = "The maximum number of methods that are allowed within a type.")
	private int maximumNumberOfMethods = 32;

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		/*List<ShallowEntity> methods = select(entity,
				"child::METHOD[not(subtype('constructor'))]");
		if (methods.size() > maximumNumberOfMethods) {
			createFindingOnFirstLine("Too many methods (" + methods.size() + "/"
					+ maximumNumberOfMethods + ")", entity);
		}*/
	}
}
