/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.scanner.ETokenType.EQ;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: B562B036B9661CDF5B632C0F3412767C
 */
@ACheck(name = "Properly initialize static variable", description = "Static variables that are not protected or private should be initialized directly or in a static initializer.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class StaticVariablesInitCheck extends CheckImplementationBase {

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		/*List<ShallowEntity> attributes = select(
				"//ATTRIBUTE[modifiers('static') and not(anymodifier('protected', 'private') or tokens('eq'))]");
		Map<String, ShallowEntity> missingInit = new HashMap<>();
		for (ShallowEntity attribute : attributes) {
			missingInit.put(attribute.getName(), attribute);
		}*/

		// Filter those that are assigned in a static initializer
		/*List<ShallowEntity> sinitStatements = select("//METHOD[subtype('"
				+ SubTypeNames.STATIC_INITIALIZER + "')]//*");
		for (ShallowEntity statement : sinitStatements) {
			if (!missingInit.containsKey(statement.getName())) {
				continue;
			}
			List<IToken> tokens = statement.ownStartTokens();
			if (TokenStreamUtils.containsAny(tokens, EQ)) {
				missingInit.remove(statement.getName());
			}
		}*/

		// Create findings for the remaining
		/*for (ShallowEntity entity : missingInit.values()) {
			createFinding("Static variable is not initialized.", entity);
		}*/
	}
}
