/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;
import java.util.Optional;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: heinemann $
 * @version $Rev: 57706 $
 * @ConQAT.Rating GREEN Hash: 66B3A88E62B4EFA920159B6EFDC7896F
 */
@ACheck(name = "Method returns null but return type is Optional", description = "Optional.empty() should be returned instead of null.", groupName = CheckGroupNames.CORRECTNESS, defaultEnablement = EFindingEnablement.RED, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class MethodReturnsNullAsOptionalCheck extends EntityCheckBase {

	/** Optional short name. */
	private static final String OPTIONAL_SHORT_CLASS_NAME = Optional.class
			.getSimpleName();
	/** Optional qualified name. */
	private static final String OPTIONAL_QUALIFIED_CLASS_NAME = Optional.class
			.getName();

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity method) throws CheckException {
		if (isMethodWithOptionalReturnType(method)) {
			checkStatements(method);
		}
	}

	/**
	 * Check for each statement of the method if it returns <code>null</code>.
	 */
	private void checkStatements(ShallowEntity method) throws CheckException {
		/*List<ShallowEntity> statements = select(method, "//STATEMENT[subtype('"
				+ SubTypeNames.SIMPLE_STATEMENT + "')]");*/

		/*for (ShallowEntity statement : statements) {
			// if-condition is needed at the moment because
			// select(ShallowEntity, String) does not fulfill its specification
			// (see #10154)
			if (method.getStartLine() <= statement.getStartLine()
					&& statement.getStartLine() <= method.getEndLine()) {
				checkStatementForReturnNull(statement);
			}
		}*/
	}

	/** Check if the statement returns <code>null</code> */
	private void checkStatementForReturnNull(ShallowEntity statement)
			throws CheckException {

		if (TokenStreamUtils.hasTokenTypeSequence(statement.includedTokens(), 0,
				ETokenType.RETURN, ETokenType.NULL_LITERAL)) {
			createFinding("Return Optional.empty() instead of null.",
					statement);
		}
	}

	/** Check if the return type of the method is {@link Optional}. */
	private static boolean isMethodWithOptionalReturnType(
			ShallowEntity method) {
		int genericsDepth = 0;

		for (IToken token : method.ownStartTokens()) {
			ETokenType tokenType = token.getType();

			if (tokenType == ETokenType.LT) {
				genericsDepth++;
			}
			if (tokenType == ETokenType.GT) {
				genericsDepth--;
				CCSMAssert.isTrue(genericsDepth >= 0,
						"Generics depth is negative");
			}

			// only consider the first identifier outside of generics (we do not
			// want to catch List<Optional<?>> and we must skip the declaration
			// of generic types)
			if (genericsDepth == 0 && tokenType == ETokenType.IDENTIFIER) {
				return OPTIONAL_SHORT_CLASS_NAME.equals(token.getText())
						|| OPTIONAL_QUALIFIED_CLASS_NAME
								.equals(token.getText());
			}
		}

		return false;
	}
}