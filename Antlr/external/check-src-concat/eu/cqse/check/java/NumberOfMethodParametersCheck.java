/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.util.ShallowParsingUtils;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53933 $
 * @ConQAT.Rating GREEN Hash: EEBF14AAE5B649CEF5B6796030185B2E
 */
@ACheck(name = "Number of Method Parameters", description = "Methods should not take more than 5 parameters. Using a large number of parameters is an indication of high complexity and should be avoided.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class NumberOfMethodParametersCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Maximum number of parameters", description = "The maximum number of parameters that are allowed.")
	private int maximumNumberOfParameters = 5;

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> parameterNames = ShallowParsingUtils
				.extractParameterNameTokens(entity);
		if (parameterNames.size() > maximumNumberOfParameters) {
			createFindingOnFirstLine(
					"Too many method parameters (" + parameterNames.size() + "/"
							+ maximumNumberOfParameters + ")",
					entity);
		}
	}
}
