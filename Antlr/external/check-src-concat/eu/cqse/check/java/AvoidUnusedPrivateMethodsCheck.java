/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;

import java.util.List;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.SetMap;

import eu.cqse.check.clike.CLikeAvoidUnusedPrivateMethodsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: hummelb $
 * @version $Rev: 56672 $
 * @ConQAT.Rating GREEN Hash: B68D0596F7E9ACCF56AE0312D7F624E5
 */
@ACheck(name = "Avoid unused private methods", description = "This rule is reported when a private method in your code exists but is not called by any code path.", groupName = CheckGroupNames.UNUSED_CODE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA, ELanguage.XTEND }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class AvoidUnusedPrivateMethodsCheck
		extends CLikeAvoidUnusedPrivateMethodsCheckBase {

	/**
	 * Stores names and parameter counts of methods that are excluded from this
	 * check.
	 */
	private static final SetMap<String, Integer> EXCLUDED_METHODS = new SetMap<>();

	static {
		// ignored custom serialization methods, for details see
		// http://stackoverflow.com/questions/7290777/java-custom-serialization
		EXCLUDED_METHODS.add("readObject", 1);
		EXCLUDED_METHODS.add("writeObject", 1);
	}

	/** Constructor. */
	public AvoidUnusedPrivateMethodsCheck() {
		super(LanguageFeatureParser.JAVA);
	}

	/** Retrieves all method references from the given tokens. */
	@Override
	protected void retrieveMethodReferences(List<IToken> tokens,
			SetMap<String, Integer> calledMethods) {
		List<Integer> referenceIndices = TokenStreamUtils
				.findTokenTypeSequences(tokens, 0, COLON, COLON, IDENTIFIER);
		for (int referenceIndex : referenceIndices) {
			calledMethods.add(tokens.get(referenceIndex + 2).getText(),
					UNKNOWN_ARITY);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected SetMap<String, Integer> getExcludedMethods() {
		return EXCLUDED_METHODS;
	}

	/** {@inheritDoc} */
	@Override
	protected void retrieveMethodCalls(List<IToken> tokens,
			SetMap<String, Integer> calledMethods) {
		super.retrieveMethodCalls(tokens, calledMethods);

		// in Xtend you can have method calls without parenthesis
		if (!tokens.isEmpty()
				&& tokens.get(0).getLanguage() == ELanguage.XTEND) {
			CollectionUtils.filterAndMap(tokens,
					token -> token.getType() == IDENTIFIER,
					token -> calledMethods.add(token.getText(), UNKNOWN_ARITY));
		}
	}
}
