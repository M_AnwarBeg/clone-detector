/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: DoNotCompareThisWithNullCheck.java 56355 2016-03-23 16:20:26Z ezaga $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.EQEQ;
import static eu.cqse.check.framework.scanner.ETokenType.NOTEQ;
import static eu.cqse.check.framework.scanner.ETokenType.NULL_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.THIS;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.base.EntityTokenCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: ezaga $
 * @version $Rev: 56355 $
 * @ConQAT.Rating GREEN Hash: CCE353C96C811F322BE8BF492805E3A1
 */
@ACheck(name = "Do not compare this and null", description = "This must not be "
		+ "compared with null, because this cannot be null.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA, ELanguage.XTEND }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class DoNotCompareThisWithNullCheck extends EntityTokenCheckBase {

	/** Compare operator token types. */
	private static final EnumSet<ETokenType> COMPARE_OPERATORS = EnumSet
			.of(EQEQ, NOTEQ);

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD[not(modifiers('static'))]//STATEMENT";
	}

	/**
	 * Checks whether the given tokens contain a comparison between this and
	 * null.
	 */
	@Override
	protected void processTokens(List<IToken> tokens) throws CheckException {

		List<Integer> compareIndices = TokenStreamUtils.findAll(tokens, 1,
				tokens.size() - 1, COMPARE_OPERATORS);

		for (int compareIndex : compareIndices) {

			IToken leftOperand = tokens.get(compareIndex - 1);
			IToken rightOperand = tokens.get(compareIndex + 1);

			ETokenType leftType = leftOperand.getType();
			ETokenType rightType = rightOperand.getType();

			if (leftType == THIS && rightType == NULL_LITERAL) {
				createFinding(leftOperand, rightOperand);
			} else if (leftType == NULL_LITERAL && rightType == THIS
					&& !(tokens.size() >= compareIndex + 2
							&& tokens.get(compareIndex + 2).getType() == DOT)) {
				createFinding(leftOperand, rightOperand);
			}
		}
	}

	/** Creates the finding */
	private void createFinding(IToken leftOperand, IToken rightOperand)
			throws CheckException {
		createFinding("this must not be compared with null", leftOperand,
				rightOperand);
	}
}
