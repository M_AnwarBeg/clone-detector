/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;

import java.util.List;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: heinemann $
 * @version $Rev: 55467 $
 * @ConQAT.Rating GREEN Hash: 3179D7D29A2AC7468514B51589BA444D
 */
@ACheck(name = "Bad Assignments", description = "Using assignments in places where comparison operators are frequently used can lead to unreadable code and thus should be avoided.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA, ELanguage.XTEND }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class BadAssignmentsCheck extends CheckImplementationBase {

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		// Handle While, if and else if
		/*for (ShallowEntity statement : select("//STATEMENT[subtype('"
				+ SubTypeNames.WHILE + "') or subtype('" + SubTypeNames.IF
				+ "') or subtype('" + SubTypeNames.ELSE_IF + "') or subtype('"
				+ SubTypeNames.ELSE_IF_NOSPACE + "')]")) {
			List<IToken> tokens = statement.ownStartTokens();
			if (TokenStreamUtils.count(tokens, EQ) > 0) {
				createFindingOnFirstLine("Avoid assignments in "
						+ statement.getSubtype() + " statements.", statement);
			}
		}*/

		// For loops
		/*for (ShallowEntity statement : select(
				"//STATEMENT[subtype('" + SubTypeNames.FOR + "') ]")) {
			List<IToken> tokens = statement.ownStartTokens();
			List<IToken> conditionTokens = TokenStreamUtils
					.tokensBetween(tokens, SEMICOLON, SEMICOLON);
			if (TokenStreamUtils.count(conditionTokens, EQ) > 0) {
				createFindingOnFirstLine(
						"Avoid assignments in condition of "
								+ statement.getSubtype() + " statements.",
						statement);
			}
		}*/
	}
}
