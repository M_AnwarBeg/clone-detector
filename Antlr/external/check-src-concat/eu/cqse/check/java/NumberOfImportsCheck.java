/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: A3018F30D1D8CCA61F1B6F877E664B4C
 */
@ACheck(name = "Too Many Imports", description = "The number of imports should not be too big. Too many imports are an indication of too many concepts being used in one class.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class NumberOfImportsCheck extends CheckImplementationBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Maximum number of imports", description = "The maximum number of imports that are allowed.")
	private int maximumNumberOfImports = 50;

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		/*List<ShallowEntity> elements = select(
				"/META[subtype('" + SubTypeNames.IMPORT + "')]");
		if (elements.size() > maximumNumberOfImports) {
			createFinding("Too many imports (" + elements.size() + "/"
					+ maximumNumberOfImports + ")");
		}*/
	}
}
