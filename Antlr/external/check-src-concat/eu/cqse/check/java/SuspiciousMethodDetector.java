/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: SuspiciousMethodDetector.java 53185 2015-07-03 13:54:40Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.util.ShallowParsingUtils;

/**
 * Class for detecting suspicious methods, that are similar to a standard method
 * of the Java language, e.g. toString or equals.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: 33E5CB8103D480F3E325185F7E181C28
 */
public class SuspiciousMethodDetector {

	/** The method's name. */
	private final String name;

	/** The method's parameter count. */
	private final int parameterCount;

	/** Constructor. */
	public SuspiciousMethodDetector(String name, int parameterCount) {
		CCSMAssert.isNotNull(name);
		CCSMAssert.isTrue(parameterCount >= 0,
				"method cannot have negative parameters");
		this.name = name;
		this.parameterCount = parameterCount;
	}

	/** @see #name */
	public String getName() {
		return name;
	}

	/** @see #parameterCount */
	public int getParameterCount() {
		return parameterCount;
	}

	/** Returns whether the given shallow entity is a suspicious method. */
	public boolean isSuspicious(ShallowEntity methodEntity) {
		boolean nameMatches = methodEntity.getName().equalsIgnoreCase(name);
		if (ShallowParsingUtils.extractParameterNameTokens(methodEntity).size() == parameterCount) {
			return nameMatches && !methodEntity.getName().equals(name);
		}
		return nameMatches;
	}
}
