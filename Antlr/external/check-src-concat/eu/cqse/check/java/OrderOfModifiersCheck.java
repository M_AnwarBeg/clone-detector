/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Detects incorrectly ordered modifiers on methods and fields.
 * 
 * @author $Author: anlauff $
 * @version $Rev: 57819 $
 * @ConQAT.Rating YELLOW Hash: 7D5D847A43FC91BB33695B9535EBF9D7
 */
@ACheck(name = OrderOfModifiersCheck.FINDING_MESSAGE, description = "According to the Java Language Specification the modifiers on methods and fields should be ordered as follows: public/protected/private, abstract, static, final, transient, volatile, synchronized, native, strictfp.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class OrderOfModifiersCheck extends EntityFindingCheckBase {

	/** {@value} */
	protected static final String FINDING_MESSAGE = "Modifiers must be in the right order";

	/** Contains modifiers as keys with their priority as values */
	@SuppressWarnings("serial")
	private static final Map<String, Integer> MODIFIER_ORDER = Collections
			.unmodifiableMap(new HashMap<String, Integer>() {
				{
					put("public", 1);
					put("private", 1);
					put("protected", 1);
					put("abstract", 2);
					put("static", 3);
					put("final", 4);
					put("transient", 5);
					put("volatile", 6);
					put("synchronized", 7);
					put("native", 8);
					put("strictfp", 9);
				}
			});

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//ATTRIBUTE | //METHOD";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		int previousPriority = 0;
		for (IToken token : entity.includedTokens()) {
			if (!MODIFIER_ORDER.containsKey(token.getText())) {
				return;
			}
			if (MODIFIER_ORDER.get(token.getText()) < previousPriority) {
				createFinding("Modifiers of " + entity.getName()
						+ " are in the wrong order", entity);
				return;
			}
			previousPriority = MODIFIER_ORDER.get(token.getText());
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return FINDING_MESSAGE;
	}

}
