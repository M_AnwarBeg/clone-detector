/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: PrivateMethodsMustNotBeFinalCheck.java 55467 2016-01-27 10:25:25Z heinemann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.shallowparser.TokenStreamUtils.NOT_FOUND;
import static eu.cqse.check.framework.shallowparser.TokenStreamUtils.find;

import org.conqat.lib.commons.collections.UnmodifiableList;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 55467 $
 * @ConQAT.Rating GREEN Hash: 6616918864C96255A287C46C2E54B3E1
 */
@ACheck(name = "Private methods must not be final", description = "Since private methods cannot be "
		+ "overridden, declaring them as final is unnecessary.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA, ELanguage.XTEND }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class PrivateMethodsMustNotBeFinalCheck extends EntityFindingCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD[subtype('" + SubTypeNames.METHOD
				+ "') and modifiers('private')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		UnmodifiableList<IToken> tokens = entity.ownStartTokens();
		int methodStartIndex = find(tokens, ETokenType.LPAREN);
		int finalTokenIndex = find(tokens, ETokenType.FINAL);
		if (methodStartIndex == NOT_FOUND || finalTokenIndex == NOT_FOUND) {
			return;
		}
		if (finalTokenIndex < methodStartIndex) {
			createFinding(getFindingMessage(entity), entity);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return "Private method " + entity.getName() + " must not be final";
	}
}
