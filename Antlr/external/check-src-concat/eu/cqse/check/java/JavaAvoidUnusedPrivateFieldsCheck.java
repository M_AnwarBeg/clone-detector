/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: JavaAvoidUnusedPrivateFieldsCheck.java 57713 2016-07-15 07:55:22Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import eu.cqse.check.clike.CLikeAvoidUnusedPrivateFieldsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 57713 $
 * @ConQAT.Rating GREEN Hash: 44796CBB050079594472B9676D9081CC
 */
@ACheck(name = "Avoid unused private fields (Java)", description = "This rule is reported when a "
		+ "private field in your code exists but is not used by any code path.", groupName = CheckGroupNames.UNUSED_CODE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE,
						ECheckParameter.TYPE_RESOLUTION })
public class JavaAvoidUnusedPrivateFieldsCheck
		extends CLikeAvoidUnusedPrivateFieldsCheckBase {

	/** Constructor. */
	public JavaAvoidUnusedPrivateFieldsCheck() {
		super(LanguageFeatureParser.JAVA);
	}

	/**
	 * Checks if the given SuppressedWarning annotation entity has the 'unused'
	 * warning specified.
	 */
	private static boolean isSuppressWarningsUnused(ShallowEntity entity) {
		for (IToken token : entity.ownStartTokens()) {
			if (token.getText().replace("\"", "").equals("unused")) {
				return true;
			}
		}
		return false;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isAnnotationRelevant(String annotationToFind,
			List<ShallowEntity> annotationsOnField) {
		return annotationsOnField.stream()
				.anyMatch(annotation -> annotation.getName()
						.equals(annotationToFind)
						&& isSuppressWarningsUnused(annotation));
	}

	/** {@inheritDoc} */
	@Override
	protected Set<String> getAnnotations() {
		return Collections.singleton("SuppressWarnings");
	}

	/** {@inheritDoc} */
	@Override
	protected String getFieldSelectionXPath() {
		// we ignore inner classes as they occur rarely and their field uses are
		// hard to analyze
		return "/TYPE/ATTRIBUTE[subtype('" + SubTypeNames.ATTRIBUTE
				+ "') and modifiers('private') and not(name-matches('serialVersionUID') and tokens('static', 'final', 'long'))]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getProcessedEntitiesXPath() {
		return "descendant::METHOD | descendant::ATTRIBUTE | descendant::STATEMENT | descendant::META | descendant::META[subtype('"
				+ SubTypeNames.CASE + "')] | descendant::TYPE[subtype('"
				+ SubTypeNames.ANONYMOUS_CLASS + "')]";
	}
}
