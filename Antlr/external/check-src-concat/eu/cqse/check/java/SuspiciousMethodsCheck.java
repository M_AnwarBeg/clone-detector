/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: SuspiciousMethodsCheck.java 54535 2015-09-29 15:53:49Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.Arrays;
import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: 534B1AF5AC58626B365112C74E5F9E99
 */
@ACheck(name = "Suspicious methods", description = "If methods are named similar to ones from Object, this might "
		+ "be a typing error or can lead to confusion. Those methods should be renamed.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class SuspiciousMethodsCheck extends EntityCheckBase {

	/** A list of detectors for suspicious methods. */
	private static final List<SuspiciousMethodDetector> METHOD_DETECTORS = Arrays
			.asList(new SuspiciousMethodDetector("equals", 1),
					new SuspiciousMethodDetector("hashCode", 0),
					new SuspiciousMethodDetector("toString", 0),
					new SuspiciousMethodDetector("compareTo", 1));

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD[subtype('" + SubTypeNames.METHOD + "')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		for (SuspiciousMethodDetector detector : METHOD_DETECTORS) {
			if (detector.isSuspicious(entity)) {
				createFindingOnFirstLine(
						"Suspicious method " + entity.getName(), entity);
				break;
			}
		}
	}
}
