/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.clike.CLikeFieldUsageCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 57866 $
 * @ConQAT.Rating GREEN Hash: 7B8103D71B342C5B1A17877A599024F3
 */
@ACheck(name = "Field could be final (Java)", description = "Fields that can be made final, should be marked as such to avoid accidential assignment to them.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE,
				ECheckParameter.TYPE_RESOLUTION })
public class JavaFieldCouldBeFinalCheck extends CLikeFieldUsageCheckBase {

	/** Description for check option */
	private static final String CHECK_OPTION_DESCRIPTION = "List of Java annotations to exclude class fields from being reported as needing to be declared final";

	/** List of annotations to filter out fields. */
	@ACheckOption(name = "Java annotations for non-final fields", description = CHECK_OPTION_DESCRIPTION)
	private Set<String> annotations = CollectionUtils.asHashSet("Inject",
			"Autowired", "SpringBean");

	/** Constructor. */
	public JavaFieldCouldBeFinalCheck() {
		super(LanguageFeatureParser.JAVA);
	}

	/** {@inheritDoc} */
	@Override
	protected Set<String> getAnnotations() {
		return annotations;
	}

	/** {@inheritDoc} */
	@Override
	protected String getFieldSelectionXPath() {
		return "//TYPE[subtype('" + SubTypeNames.CLASS
				+ "')]/ATTRIBUTE[subtype('" + SubTypeNames.ATTRIBUTE
				+ "') and not(anymodifier('static', 'final'))]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(String fieldName) {
		return "Field '" + fieldName + "' could be made final.";
	}

	/** {@inheritDoc} */
	@Override
	protected String getProcessedEntitiesXPath() {
		return "descendant::METHOD[not(subtype('" + SubTypeNames.CONSTRUCTOR
				+ "') or subtype('" + SubTypeNames.STATIC_INITIALIZER
				+ "'))] | descendant::STATEMENT";
	}

	/** {@inheritDoc} */
	@Override
	protected boolean analyzeTokens(List<IToken> tokens, String fieldName,
			boolean isShadowed) {
		return !languageFeatureParser.getVariableWritesFromTokens(tokens,
				fieldName, true, isShadowed).isEmpty();
	}
}
