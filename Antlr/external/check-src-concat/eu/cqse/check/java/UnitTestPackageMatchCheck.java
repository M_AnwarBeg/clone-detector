package eu.cqse.check.java;

import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: heinemann $
 * @version $Rev: 57425 $
 * @ConQAT.Rating GREEN Hash: CDDA19CA4CFB267FF16236A783C18132
 */
@ACheck(name = "Unit test classes should be located in the same package as the class under test", description = "Unit test classes should be located in the same package as the class under test", groupName = CheckGroupNames.TEST_CONVENTIONS, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UnitTestPackageMatchCheck extends CheckImplementationBase {

	/** Xpath for packages */
	private static final String X_PATH_PACKAGE = "/META[subtype('"
			+ SubTypeNames.PACKAGE + "')]";
	/** Xpath for imports */
	private static final String X_PATH_IMPORTS = "/META[subtype('"
			+ SubTypeNames.IMPORT + "')]";
	/** Xpath pattern for test classes (the suffix still needs to be set) */
	private static final String X_PATH_TEST_CLASSES_PATTERN = "/TYPE[subtype('"
			+ SubTypeNames.CLASS + "') and name-matches('.*?%s')]";
	/** Package separator in qualified class name. */
	private static final char PACKAGE_SEPARATOR = '.';

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Test class name ending", description = "The class name suffix of test classes (e.g. \"Test\").")
	private String testClassNameSuffix = "Test";

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		String xPathTestClasses = String.format(X_PATH_TEST_CLASSES_PATTERN,
				testClassNameSuffix);
		/*List<ShallowEntity> testClasses = select(xPathTestClasses);

		for (ShallowEntity testClassUnit : testClasses) {
			checkTestClass(testClassUnit);
		}*/
	}

	/** Execute the check for the test class. */
	private void checkTestClass(ShallowEntity testClassEntity)
			throws CheckException {
		String expectedNameOfClassUnderTest = getNameOfExpectedClassUnderTest(
				testClassEntity.getName());
		ShallowEntity packageEntity = getPackageEntity();
		String testClassPackagePrefix = getPackagePrefixFromPackageEntity(
				packageEntity);

		/*List<ShallowEntity> importStatements = select(X_PATH_IMPORTS);

		for (ShallowEntity importStatement : importStatements) {
			if (isTestClassInWrongPackage(testClassPackagePrefix,
					expectedNameOfClassUnderTest, importStatement)) {
				createFinding(testClassEntity,
						getPackageNameFromImportStatement(importStatement));
				break;
			}
		}*/
	}

	/**
	 * Get the package of a class (with suffix {@value #PACKAGE_SEPARATOR}) or
	 * an empty string if the class is in the default package.
	 * 
	 * @param packageEntity
	 *            entity of a package
	 */
	private static String getPackagePrefixFromPackageEntity(
			ShallowEntity packageEntity) {
		if (packageEntity != null) {
			return packageEntity.getName() + PACKAGE_SEPARATOR;
		}
		return StringUtils.EMPTY_STRING;
	}

	/**
	 * Get the package name of a class or an empty string if the class is in the
	 * default package.
	 * 
	 * @param importStatement
	 *            entity of an import statement
	 */
	private static String getPackageNameFromImportStatement(
			ShallowEntity importStatement) {
		if (importStatement != null) {
			return StringUtils.removeLastPart(importStatement.getName(),
					PACKAGE_SEPARATOR);
		}
		return StringUtils.EMPTY_STRING;
	}

	/** Create a finding for this check. */
	private void createFinding(ShallowEntity testClassEntity,
			String expectedPackageName) throws CheckException {
		createFindingOnFirstLine("Expected this test class in package '"
				+ expectedPackageName + "'", testClassEntity);
	}

	/** Get the package entity. May return null. */
	private ShallowEntity getPackageEntity() throws CheckException {
		/*List<ShallowEntity> packageDeclarations = select(X_PATH_PACKAGE);

		if (!packageDeclarations.isEmpty()) {
			return packageDeclarations.get(0);
		}*/

		return null;
	}

	/**
	 * Check if the test class is supposed to be in another package.
	 * 
	 * @param testClassPackagePrefix
	 *            package name followed by {@value #PACKAGE_SEPARATOR} or empty
	 *            (if the class is in the default package)
	 * @param expectedSimpleNameOfClassUnderTest
	 *            expected simple name of the class under test
	 * @return true if the class under test is supposed to be in the wrong
	 *         package
	 */
	private static boolean isTestClassInWrongPackage(
			String testClassPackagePrefix,
			String expectedSimpleNameOfClassUnderTest,
			ShallowEntity importStatement) {
		String qualifiedImportClassName = importStatement.getName();

		if (expectedSimpleNameOfClassUnderTest.equals(StringUtils
				.getLastPart(qualifiedImportClassName, PACKAGE_SEPARATOR))) {
			// A class is imported which is supposed to be the class under
			// (unit) test. -> The test class is either in the wrong package or
			// the import is superfluous.
			return !qualifiedImportClassName.equals(testClassPackagePrefix
					+ expectedSimpleNameOfClassUnderTest);
		}
		return false;
	}

	/**
	 * Derive the name of the class under test from the name of the test class.
	 * 
	 * @param testClassName
	 *            simple name of test class
	 * @return simple name of expected class under test
	 */
	private String getNameOfExpectedClassUnderTest(String testClassName) {
		CCSMAssert.isTrue(testClassName.endsWith(testClassNameSuffix),
				"Unexpected test class name");
		return StringUtils.stripSuffix(testClassName, testClassNameSuffix);
	}
}