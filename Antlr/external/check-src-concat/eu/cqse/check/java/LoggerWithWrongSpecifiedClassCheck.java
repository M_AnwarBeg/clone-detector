/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import org.conqat.lib.commons.collections.UnmodifiableList;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * 
 * @author $Author: anlauff $
 * @version $Rev: 57851 $
 * @ConQAT.Rating YELLOW Hash: D026F7EF20C5090807679F25F83BEC8F
 */

@ACheck(name = LoggerWithWrongSpecifiedClassCheck.FINDING_MESSAGE, description = "The Logger should be specified with the class it operates in. This way, the log output can be linked to said class.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class LoggerWithWrongSpecifiedClassCheck extends EntityFindingCheckBase {

	/** {@value} */
	protected static final String FINDING_MESSAGE = "Logger was specified with a different class";

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return FINDING_MESSAGE;
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		UnmodifiableList<IToken> includedTokens = entity.includedTokens();
		for (int i = 0; i < includedTokens.size(); i++) {
			if ("getLogger".equals(includedTokens.get(i).getText())
					&& includedTokens.size() > i + 2) {
				String specifiedClass = includedTokens.get(i + 2).getText();
				String actualClass = entity.getParent().getName();
				if (!(specifiedClass.equals(actualClass))) {
					createFinding("Logger should be specified with "
							+ actualClass + ".class", entity);
					return;
				}
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//ATTRIBUTE";
	}

}
