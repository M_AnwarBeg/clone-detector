/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.ScopedTypeLookup;
import eu.cqse.check.framework.typetracker.TypedVariable;

/**
 * This check is about the encoding parameter in constructors like String. It
 * creates findings in case there is no charset parameter
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57418 $
 * @ConQAT.Rating RED Hash: FF8B3BC6C9A11DF7B7DC90ED148E3752
 */
@ACheck(name = ConstructorCallWithoutEncodingCheck.CHECK_NAME, description = "When transforming byte sequences into Strings the encoding must be considered. "
		+ "Using the platform encoding may result in interoperability issues.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA }, parameters = {
						ECheckParameter.TYPE_RESOLUTION,
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class ConstructorCallWithoutEncodingCheck extends EntityCheckBase {

	/** The name of this service. */
	public static final String CHECK_NAME = "Transformation of byte sequence into String must consider encoding";

	/** Token type sequence that indicates a constructor call. */
	private static final ETokenType[] CONSTRUCTOR_CALL_SEQUENCE = {
			ETokenType.NEW, ETokenType.IDENTIFIER, ETokenType.LPAREN };

	/** The constructors to be checked. */
	private static final List<String> CONSTRUCTOR_NAMES = Arrays.asList(
			"String", "InputStreamReader", "OutputStreamWriter", "FileReader",
			"Scanner", "PrintStream");

	/**
	 * A simple map to easily find the corresponding closing token type for
	 * various opening types.
	 */
	private static final Map<ETokenType, ETokenType> MATCHING_TOKEN_TYPES = new HashMap<>();
	static {
		MATCHING_TOKEN_TYPES.put(ETokenType.LPAREN, ETokenType.RPAREN);
		MATCHING_TOKEN_TYPES.put(ETokenType.LBRACE, ETokenType.RBRACE);
		MATCHING_TOKEN_TYPES.put(ETokenType.LBRACK, ETokenType.RBRACK);
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT | //ATTRIBUTE";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		processTokens(entity, entity.ownStartTokens());
		if (entity.hasChildren()) {
			processTokens(entity, entity.ownEndTokens());
		}
	}

	/** Processes the tokens of a statement entity. */
	protected void processTokens(ShallowEntity entity, List<IToken> tokens)
			throws CheckException {
		List<Integer> callIndices = TokenStreamUtils
				.findTokenTypeSequences(tokens, 0, CONSTRUCTOR_CALL_SEQUENCE);

		for (Integer callIndex : callIndices) {
			IToken constructorNameToken = tokens.get(callIndex + 1);
			String constructorName = constructorNameToken.getText();
			if (CONSTRUCTOR_NAMES.stream()
					.anyMatch(s -> s.equals(constructorName))) {

				List<IToken> constructorArguments = getConstructorArgumentTokens(
						tokens, callIndex);
				ScopedTypeLookup tokenTypeLookup = context.getTypeResolution()
						.getTypeLookup(entity);

				// TODO (LH) CONSTRUCTOR_NAMES contains 6 elements and this
				// switch only handles 4
				switch (constructorName) {
				case "String":
					stringCheck(tokenTypeLookup, constructorNameToken,
							constructorArguments);
					break;
				case "InputStreamReader":
				case "OutputStreamWriter":
					ioStreamReaderWriterCheck(constructorNameToken,
							constructorArguments);
					break;
				case "FileReader":
					createFinding(constructorNameToken);
					break;
				default:
					break;
				}
			}
		}
	}

	/** Checks the String constructor. */
	private void stringCheck(ScopedTypeLookup tokenTypeLookup,
			IToken constructorNameToken, List<IToken> constructorArguments)
			throws CheckException {
		List<List<IToken>> parameters = extractParameters(constructorArguments);
		int numberOfParameters = parameters.size();
		if (numberOfParameters == 0) {
			return;
		}
		if (isParameterByteArray(tokenTypeLookup, parameters.get(0))) {
			if (numberOfParameters == 1 || numberOfParameters == 3) {
				createFinding(constructorNameToken);
			} else if (numberOfParameters == 2 || numberOfParameters == 4) {
				List<IToken> lastParameter = parameters
						.get(parameters.size() - 1);
				if (parameterIsNotCharsetOrString(tokenTypeLookup,
						lastParameter)) {
					createFinding(constructorNameToken);
				}
			}
		}
	}

	/** Checks the String constructor. */
	private void ioStreamReaderWriterCheck(IToken constructorNameToken,
			List<IToken> constructorArguments) throws CheckException {
		List<List<IToken>> parameters = extractParameters(constructorArguments);
		int numberOfParameters = parameters.size();
		if (numberOfParameters == 1) {
			createFinding(constructorNameToken);
		}
	}

	/** Checks if a parameter is a charset or a string. */
	private static boolean parameterIsNotCharsetOrString(
			// TODO (LH) Naming: parameter -> parameterTokens
			ScopedTypeLookup tokenTypeLookup, List<IToken> parameter) {
		if (parameter.size() == 1) {
			// TODO (LH) Please extract local variable for parameter.get(0) and
			// parameter.get(0).getType()
			if (parameter.get(0).getType().equals(ETokenType.IDENTIFIER)) {
				return identifierTypeNotStringOrCharset(parameter.get(0),
						tokenTypeLookup);
			} else if (!parameter.get(0).getType()
					.equals(ETokenType.STRING_LITERAL)) {
				return true;
			}
		}
		// TODO (LH) Make this an else if and remove inline comment?
		// Parameter token number > 1
		if (TokenStreamUtils.endsWith(parameter,
				new ETokenType[] { ETokenType.DOT, ETokenType.IDENTIFIER })) {
			return identifierTypeNotStringOrCharset(
					parameter.get(parameter.size() - 1), tokenTypeLookup);
		}
		return false;
	}

	/** Returns true if an identifier is neither String nor Charset. */
	private static boolean identifierTypeNotStringOrCharset(IToken identifier,
			ScopedTypeLookup tokenTypeLookup) {
		TypedVariable typeInfo = tokenTypeLookup
				.getTypeInfo(identifier.getText());
		if (typeInfo != null) {
			String typeName = typeInfo.getTypeName();
			return ((!typeName.equals("String"))
					&& (!typeName.equals("Charset")));
		}
		return false;
	}

	/** Checks if a parameter is a byte array. */
	private static boolean isParameterByteArray(
			// TODO (LH) Naming: parameter -> parameterTokens
			ScopedTypeLookup tokenTypeLookup, List<IToken> parameter) {
		if (parameter.size() == 1
				// TODO (LH) Please extract local for parameter.get(0)
				&& parameter.get(0).getType().equals(ETokenType.IDENTIFIER)) {
			TypedVariable typeInfo = tokenTypeLookup
					.getTypeInfo(parameter.get(0).getText());
			if (typeInfo != null && typeInfo.getTypeName().equals("byte[]")) {
				return true;
			}
		}
		// Parameter has more than 1 tokens
		if (parameter.size() > 4) {
			int lastIndex = parameter.size() - 1;
			// TODO (LH) The two following calls are quite similar, except for
			// the 4th parameter. So you can extract a method.
			if (TokenStreamTextUtils.hasSequence(parameter, lastIndex - 3, ".",
					"getBytes", "(", ")")
					|| TokenStreamTextUtils.hasSequence(parameter,
							lastIndex - 3, ".", "toByteArray", "(", ")")) {
				return true;
			}
			if (parameter.size() >= 6) {
				return (
				// TODO (LH) This is rather hard to read. Please extract
				// variables or even methods for isCastToByteArray,
				// isByteArrayConstructor
				// Cast to byte [] ....
				TokenStreamTextUtils.hasSequence(parameter, 0, "(", "byte", "[",
						"]", ")") || (
				// new byte[] { .... }
				TokenStreamTextUtils.hasSequence(parameter, 0, "new", "byte",
						"[", "]", "{")
						&& parameter.get(lastIndex).getType()
								.equals(ETokenType.RBRACE)));
			}
		}
		return false;
	}

	/** Extracts the constructor argument tokens. */
	private static List<IToken> getConstructorArgumentTokens(
			List<IToken> tokens, Integer callIndex) {
		return TokenStreamUtils.tokensBetweenWithNesting(tokens, callIndex,
				ETokenType.LPAREN, ETokenType.RPAREN);
	}

	/** Extracts the constructor's parameters as lists of tokens. */
	private static List<List<IToken>> extractParameters(
			List<IToken> constructorArgumentTokens) {
		List<List<IToken>> parameters = new ArrayList<>();
		List<IToken> buffer = new ArrayList<>();
		int i = 0;
		while (i < constructorArgumentTokens.size()) {
			IToken currentToken = constructorArgumentTokens.get(i);
			if (currentToken.getType().equals(ETokenType.LPAREN)
					|| currentToken.getType().equals(ETokenType.LBRACE)) {
				buffer.add(currentToken);
				List<IToken> tokensBetweenWithNesting = TokenStreamUtils
						.tokensBetweenWithNesting(constructorArgumentTokens, i,
								currentToken.getType(), MATCHING_TOKEN_TYPES
										.get(currentToken.getType()));
				buffer.addAll(tokensBetweenWithNesting);
				i += tokensBetweenWithNesting.size() + 1;
				buffer.add(constructorArgumentTokens.get(i++));
			} else if (currentToken.getType().equals(ETokenType.COMMA)) {
				parameters.add(buffer);
				buffer = new ArrayList<>();
				i++;
			} else {
				buffer.add(currentToken);
				i++;
			}
		}
		if (!buffer.isEmpty()) {
			parameters.add(buffer);
		}
		return parameters;
	}

	/** Creates the finding for the specified constructor. */
	private void createFinding(IToken constructorNameToken)
			throws CheckException {
		createFinding(
				"Transformation of byte sequence into String uses platform encoding",
				constructorNameToken);
	}

}
