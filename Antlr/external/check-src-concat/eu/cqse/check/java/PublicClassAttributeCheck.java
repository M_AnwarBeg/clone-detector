/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: dreier $
 * @version $Rev: 55360 $
 * @ConQAT.Rating GREEN Hash: 092CA874481F8763090C21A7B8ABC03E
 */
@ACheck(name = "Public class attribute", description = "Classes should not have public attributes as this violates encapsulation. Instead access should be provided using public methods.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class PublicClassAttributeCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Annotation filter for public fields", description = "Attributes annotated with an annotation contained in this list will be ignored for the analysis. E.g.: @Inject, @Attribute")
	private List<String> filterAnnotationNameList = Arrays.asList("@Inject",
			"@Rule", "@Mock", "@AConQATFieldParameter",
			"@AOptionFieldDescription");

	/** Annotation names for filtering public fields. */
	private Set<String> filterAnnotationNames;

	/** {@inheritDoc} */
	@Override
	public void initialize() {
		filterAnnotationNames = new HashSet<String>();
		for (String filterAnnotationName : filterAnnotationNameList) {
			filterAnnotationNames
					.add(StringUtils.stripPrefix(filterAnnotationName, "@"));
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//ATTRIBUTE[modifiers('public') and not(modifiers('static', 'final'))]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		if (!isFilteredByAnnotation(entity)) {
			createFinding("Non-constant public attribute", entity);
		}
	}

	/** Returns whether the given entity is filtered by an annotation. */
	private boolean isFilteredByAnnotation(ShallowEntity entity) {
		List<ShallowEntity> annotations = LanguageFeatureParser.JAVA
				.getAnnotations(entity);
		return annotations.stream().anyMatch(annotation -> filterAnnotationNames
				.contains(annotation.getName()));
	}
}
