/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import org.conqat.lib.commons.collections.UnmodifiableList;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * 
 * @author $Author: anlauff $
 * @version $Rev: 57852 $
 * @ConQAT.Rating YELLOW Hash: ACAAF6966A63362A7B63CFA23AD76D65
 */

@ACheck(name = AssertWithInvertedConditionCheck.FINDING_MESSAGE, description = "To improve readability, it is more desirable to have an assertFalse statement when checking for conditions to be unsatisfied. (and vice versa)", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class AssertWithInvertedConditionCheck extends EntityFindingCheckBase {

	/** {@value} */
	protected static final String FINDING_MESSAGE = "Use assertFalse instead of assertTrue with inverted condition (and vice versa)!";

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return FINDING_MESSAGE;
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT[anytoken('NOT')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		UnmodifiableList<IToken> includedTokens = entity.includedTokens();
		if (includedTokens.get(0).getText().startsWith("assert")
				&& "!".equals(includedTokens.get(2).getText())) {
			createFinding("Found assert with inverted condition", entity);
		}
	}

}
