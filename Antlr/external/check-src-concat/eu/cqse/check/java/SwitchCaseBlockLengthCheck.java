/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.HashSet;
import java.util.Set;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: hummelb $
 * @version $Rev: 54535 $
 * @ConQAT.Rating GREEN Hash: 00BDBD1A73A73D6143612099187327F6
 */
@ACheck(name = "Case Blocks Length", description = "Case blocks should not be longer than the configured number of statements (default=10).", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class SwitchCaseBlockLengthCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Maximum case block length", description = "The maximum allowed length of a case block in statements.")
	private int maximumCaseBlockLength = 10;

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//*[subtype('" + SubTypeNames.SWITCH + "') and count(node())>"
				+ maximumCaseBlockLength + "]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		Set<ShallowEntity> tooLongCases = new HashSet<>();
		ShallowEntity lastCaseEntity = null;
		int statementCounter = 0;
		for (ShallowEntity child : entity.getChildren()) {
			if (child.getSubtype().equals(SubTypeNames.CASE)
					|| child.getSubtype().equals(SubTypeNames.DEFAULT)) {
				statementCounter = 0;
				lastCaseEntity = child;
			} else {
				statementCounter++;
			}
			if (statementCounter > maximumCaseBlockLength
					&& lastCaseEntity != null) {
				tooLongCases.add(lastCaseEntity);
			}
		}
		for (ShallowEntity tooLongCase : tooLongCases) {
			createFindingOnFirstLine("Case block longer than "
					+ maximumCaseBlockLength + " statements.", tooLongCase);
		}
	}
}