/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Check for ignored JUnit tests.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 54726 $
 * @ConQAT.Rating GREEN Hash: 6A3F4EA5F0A44171148101E5AA8E42FE
 */
@ACheck(name = IgnoredJUnitTestCheck.FINDING_MESSAGE, description = "JUnit tests should not be ignored for longer periods of time.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class IgnoredJUnitTestCheck extends EntityFindingCheckBase {

	/** The message of the finding. */
	protected static final String FINDING_MESSAGE = "Avoid ignoring JUnit tests";

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//META[subtype('annotation') and name-matches('Ignore')]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return FINDING_MESSAGE;
	}
}
