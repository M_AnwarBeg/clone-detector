/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.java.ThrowHighlevelExceptionsCheck.isClassNameInClassSet;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eu.cqse.check.base.TopLevelTypeCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@AConQAT.Doc}
 * 
 * @author $Author: steckermeier $
 * @version $Rev: 57775 $
 * @ConQAT.Rating YELLOW Hash: 8B7B2E1633D1B50EB7575AD230D4E41C
 */
@ACheck(name = "Custom exception class", description = "Creates a finding for each custom exception class.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class IsCustomExceptionCheck extends TopLevelTypeCheckBase {

	/**
	 * Prefix for the message of created findings used to identify them as
	 * custom exception findings.
	 */
	public static final String FINDING_MESSAGE_PREFIX = "Custom exception:";

	/**
	 * The base {@link Throwable} classes which custom throwable classes extend.
	 */
	private static final Set<Class<?>> BASE_THROWABLE_CLASSES = new HashSet<>(
			ThrowHighlevelExceptionsCheck.FORBIDDEN_EXCEPTIONS);

	static {
		BASE_THROWABLE_CLASSES.add(Exception.class);
	}

	/** {@inheritDoc} */
	@Override
	protected void analyzeTopLevelTypes(
			List<ShallowEntity> topLevelTypeEntities) throws CheckException {

		for (ShallowEntity entity : topLevelTypeEntities) {
			analyzeTypeEntity(entity);
		}
	}

	/**
	 * Creates a finding if the entity representing a type extends one of the
	 * exceptions in {@link #BASE_THROWABLE_CLASSES}.
	 */
	protected void analyzeTypeEntity(ShallowEntity entity)
			throws CheckException {
		List<IToken> tokens = entity.ownStartTokens();
		int extendsIndex = TokenStreamUtils.find(tokens, ETokenType.EXTENDS);

		if (extendsIndex < 0) {
			return; // Doesn't extend anything.
		}

		if (tokens.size() > extendsIndex + 1 && tokens.get(extendsIndex + 1)
				.getType().equals(ETokenType.IDENTIFIER)) {
			String parentClassName = tokens.get(extendsIndex + 1).getText();

			if (isThrowable(parentClassName)) {
				createFinding(FINDING_MESSAGE_PREFIX + " extends class '"
						+ parentClassName + "'.", entity);
			}
		}
	}

	/**
	 * Determines whether the given class name represents a subclass of
	 * {@link Throwable} or {@link Throwable} itself. As a heuristic any class
	 * ending with "Exception" or "Error" is considered a subclass of
	 * {@link Throwable}, because no class hierarchy is available to calculate
	 * parent classes.
	 */
	private static boolean isThrowable(String className) {
		// A heuristic for determining subclasses of Exception and Error.
		if (className.endsWith("Exception") || className.endsWith("Error")) {
			return true;
		}

		return isClassNameInClassSet(BASE_THROWABLE_CLASSES, className);
	}

	/**
	 * Returns true if the type declaration is a class declaration which is not
	 * anonymous.
	 */
	@Override
	protected boolean isTopLevelType(ShallowEntity typeEntity) {
		List<IToken> tokens = typeEntity.ownStartTokens();
		int classTokenIndex = TokenStreamUtils.find(tokens, ETokenType.CLASS);

		return classTokenIndex >= 0;
	}
}