/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: JavaUnwantedMethodCallsCheck.java 57097 2016-05-27 12:43:38Z heinemann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.HashSet;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.base.UnwantedMethodCallsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57097 $
 * @ConQAT.Rating GREEN Hash: 841078FBC17C46CEED933DA051901A14
 */
@ACheck(name = "Java Unwanted Method Calls", description = UnwantedMethodCallsCheckBase.CHECK_DESCRIPTION, groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class JavaUnwantedMethodCallsCheck extends UnwantedMethodCallsCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Java Unwanted Methods", description = "Names of static methods that should not be called.")
	private Set<String> unwantedMethods = new HashSet<String>(
			CollectionUtils.asHashSet("System.exit", "System.gc",
					"System.out.println", "System.err.println"));

	/** {@inheritDoc} */
	@Override
	protected Set<String> getUnwantedMethods() {
		return unwantedMethods;
	}

}
