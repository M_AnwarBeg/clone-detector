/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.java.ThrowHighlevelExceptionsCheck.isClassNameInClassSet;

import java.util.List;
import java.util.Set;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@AConQAT.Doc}
 * 
 * @author $Author: steckermeier $
 * @version $Rev: 57774 $
 * @ConQAT.Rating YELLOW Hash: FF50BE1658340A9E3865DC632DFA35D0
 */
@ACheck(name = "Catch of generic exception.", description = "The exceptions java.lang.Throwable, java.lang.Error, java.lang.RuntimeException and java.lang.Exception may not be handled by a catch block.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.RED, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class CatchHighLevelExceptionCheck extends EntityCheckBase {

	/** The forbidden exceptions for this check. */
	private static final Set<Class<?>> FORBIDDEN_EXCEPTIONS = ThrowHighlevelExceptionsCheck.FORBIDDEN_EXCEPTIONS;

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD//STATEMENT[subtype('" + SubTypeNames.CATCH + "')]";
	}

	/**
	 * {@inheritDoc}
	 *
	 * Determines whether the catch block at the {@link ShallowEntity} contains
	 * a forbidden exception.
	 */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> catchArgumentTokens = TokenStreamUtils.tokensBetween(
				entity.ownStartTokens(), ETokenType.LPAREN, ETokenType.RPAREN);

		int currentIndex = 0;
		int numberOfTokens = catchArgumentTokens.size();

		do {
			int endOfClassName = findLastTokenIndexOfClassName(catchArgumentTokens,
					currentIndex);

			if (endOfClassName != TokenStreamUtils.NOT_FOUND) {
				List<IToken> typeTokens = catchArgumentTokens
						.subList(currentIndex, endOfClassName + 1);

				String className = TokenStreamTextUtils
						.concatTokenTexts(typeTokens);

				if (isClassNameInClassSet(FORBIDDEN_EXCEPTIONS, className)) {
					createFinding(getFindingMessage(className), entity);
				}

				if (numberOfTokens > endOfClassName + 1 && catchArgumentTokens
						.get(endOfClassName + 1).getType() == ETokenType.OR) {
					currentIndex = endOfClassName + 2;
				} else {
					break;
				}
			} else {
				break;
			}
		} while (currentIndex < numberOfTokens);
	}

	/** Creates a message for a finding with the given exception class name. */
	private static String getFindingMessage(String className) {
		return "Catch clause catches generic exception '" + className + "'";
	}

	/**
	 * Finds the index of the last token belonging to a class name starting at
	 * the start index in the tokens list.
	 */
	private static int findLastTokenIndexOfClassName(List<IToken> tokens, int startIndex) {
		if (tokens.get(startIndex).getType() != ETokenType.IDENTIFIER) {
			return TokenStreamUtils.NOT_FOUND;
		}

		int currentIndex = startIndex;
		int numberOfTokens = tokens.size();

		while (numberOfTokens > currentIndex + 2
				&& tokens.get(currentIndex + 1).getType() == ETokenType.DOT
				&& tokens.get(currentIndex + 2)
						.getType() == ETokenType.IDENTIFIER) {
			currentIndex = currentIndex + 2;
		}

		return currentIndex;
	}

}
