/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: geissler $
 * @version $Rev: $
 * @ConQAT.Rating GREEN Hash: 4B86E4CCFBB17CA9410E2D99E32B8BED
 */
@ACheck(name = "No star imports", description = "Star imports must not be used, because"
		+ "they clutter the local namespace.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVA, ELanguage.XTEND }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class NoStarImportsCheck extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "/META[subtype('" + SubTypeNames.IMPORT + "')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> statementTokens = entity.includedTokens();
		int numTokens = statementTokens.size();
		CCSMAssert.isTrue(numTokens > 1,
				"There should be at least 2 included tokens per entity.");
		if (statementTokens.get(1).getType() == ETokenType.STATIC) {
			return;
		}

		// In XTEND lang, statements don't end in ;
		if (statementTokens.get(numTokens - 1).getType() == ETokenType.MULT
				|| statementTokens.get(numTokens - 2)
						.getType() == ETokenType.MULT) {
			createFinding("Do not use star import: " + entity.getName(),
					entity);
		}
	}
}
