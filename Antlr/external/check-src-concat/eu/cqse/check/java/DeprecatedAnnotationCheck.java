/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Check for deprecated classes / methods / fields
 * 
 * @author $Author: anlauff $
 * @version $Rev: 57806 $
 * @ConQAT.Rating YELLOW Hash: DAF67B4E974B7236FC432C9E762CAA79
 */
@ACheck(name = DeprecatedAnnotationCheck.FINDING_MESSAGE, description = "Deprecated classes / methods / fields", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class DeprecatedAnnotationCheck extends EntityFindingCheckBase {

	/** The message of the finding. */
	protected static final String FINDING_MESSAGE = "Avoid leaving Deprecated classes / methods / fields";

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//META[subtype('annotation') and name-matches('Deprecated')]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return FINDING_MESSAGE;
	}
}
