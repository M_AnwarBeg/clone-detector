/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 56635 $
 * @ConQAT.Rating GREEN Hash: 72AF3F0DAE557761148BB447AC3BF56E
 */
@ACheck(name = "Package Name Mismatch", description = "Package names must start with one of the configured prefixes.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class PackageNameCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Allowed package prefixes", description = "The package prefixes that are allowed to be used.")
	private List<String> allowedPrefixes = new ArrayList<>();

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "/META[subtype('" + SubTypeNames.PACKAGE + "')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		if (!StringUtils.startsWithOneOf(entity.getName(), allowedPrefixes)) {
			createFinding("Package name '" + entity.getName()
					+ "' does not conform to the guideline. Allowed package prefixes: "
					+ allowedPrefixes, entity);
		}
	}
}
