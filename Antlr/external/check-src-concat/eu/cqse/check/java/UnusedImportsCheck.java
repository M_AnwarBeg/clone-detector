/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.ListMap;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Check for unused imports in Java.
 *
 * @author $Author: geissler $
 * @version $Rev: $
 * @ConQAT.Rating GREEN Hash: C69969D62D788BD69F628BAC62BCA8D4
 */
@ACheck(name = "Unused imports", description = "Unused imports are unnecessary.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UnusedImportsCheck extends CheckImplementationBase {

	/** Single star (marker for star imports). */
	private static final String STAR = "*";

	/**
	 * Regex that matches Links in JavaDoc comments. Group #1 will contain the
	 * type. Group #2 will contain parameter type(s) if any.
	 */
	private final static Pattern COMMENT_LINK_PATTERN = Pattern
			.compile("@(?:see|link|linkplain|throws)\\s+"
					+ "([^ \\t#}(\\n]+)(?:[^ \\t}(\\n]*)(?:[(](.*?)[)])?");

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {

		// stores for simple type name the import entities
		ListMap<String, ShallowEntity> imports = new ListMap<>();

		// we know that import statements are top-level, so processing direct
		// children is sufficient
		context.getUnfilteredRootEntity().getChildren()
				.forEach(entity -> processEntity(entity, imports));

		context.getUnfilteredTokens().stream()
				.filter(token -> token.getType()
						.getTokenClass() == ETokenClass.COMMENT)
				.forEach(token -> processComment(token, imports));

		createFindings(imports);
	}

	/** Recursively process all children entities. */
	private static void processEntity(ShallowEntity entity,
			ListMap<String, ShallowEntity> imports) {
		if (entity.getType() == EShallowEntityType.META
				&& SubTypeNames.IMPORT.equals(entity.getSubtype())) {
			recordImport(entity, imports);
		} else {
			markIdentifiersAsUsed(entity.includedTokens(), imports);
		}
	}

	/** Marks all identifiers found in the given token stream as used types. */
	private static void markIdentifiersAsUsed(List<IToken> tokens,
			ListMap<String, ShallowEntity> imports) {
		tokens.stream()
				.filter(token -> token.getType() == ETokenType.IDENTIFIER)
				.forEach(token -> useType(token.getText(), imports));
	}

	/** Records an import statement. */
	private static void recordImport(ShallowEntity entity,
			ListMap<String, ShallowEntity> imports) {
		List<IToken> tokens = entity.includedTokens();
		String typeName = tokens.get(tokens.size() - 2).getText();
		if (!STAR.equals(typeName)) {
			imports.add(typeName, entity);
		}
	}

	/** Processes a single comment token and marks included types. */
	private static void processComment(IToken token,
			ListMap<String, ShallowEntity> imports) {
		Matcher matcher = COMMENT_LINK_PATTERN.matcher(token.getText());
		while (matcher.find()) {
			useType(matcher.group(1), imports);

			String parameterString = matcher.group(2);
			if (parameterString != null) {
				for (String parameter : parameterString.split(",")) {
					useType(parameter.trim(), imports);
				}
			}
		}
	}

	/** Marks a type as used by removing the import from the imports. */
	private static void useType(String typeName,
			ListMap<String, ShallowEntity> imports) {
		imports.removeCollection(typeName);
	}

	/** Create findings for unused imports. */
	private void createFindings(ListMap<String, ShallowEntity> imports)
			throws CheckException {
		for (String typeName : imports.getKeys()) {
			for (ShallowEntity entity : imports.getCollection(typeName)) {
				createFinding(
						"Unused import: "
								+ getFullyQualifiedNameFromImport(entity),
						entity);
			}
		}
	}

	/** Returns the fully qualified name for an import statement. */
	private static String getFullyQualifiedNameFromImport(
			ShallowEntity entity) {
		List<String> identifiers = CollectionUtils.filterAndMap(
				entity.includedTokens(),
				token -> token.getType() == ETokenType.IDENTIFIER,
				token -> token.getText());
		return StringUtils.concat(identifiers, ".");
	}
}
