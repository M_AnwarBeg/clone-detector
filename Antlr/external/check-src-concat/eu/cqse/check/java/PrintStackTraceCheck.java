/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * The responsibility of this check is to create findings for occurrences of
 * calls to printStackTrace() in method bodies. The method call is not ideal
 * because it outputs trace information to the console, which brings no benefit
 * to users of an application.
 * 
 * @author $Author: ezaga$
 * @version $Rev: 56082 $
 * @ConQAT.Rating GREEN Hash: 2B1F918C8D7F19B51C2D4152C802BB05
 */
@ACheck(name = "Call to printStackTrace()", description = "Avoid use of printStackTrace() calls.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class PrintStackTraceCheck extends EntityFindingCheckBase {

	/** Name of the printStackTrace method. */
	private static final String PRINT_STACK_TRACE = "printStackTrace";

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return "Avoid use of printStackTrace() calls.";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {

		// get all sequences of .<IDENTIFIER>() and check
		// if the identifier is 'printStackTrace'
		List<IToken> statementTokens = entity.includedTokens();
		List<Integer> callIndices = TokenStreamUtils.findTokenTypeSequences(
				statementTokens, 0, ETokenType.DOT, ETokenType.IDENTIFIER,
				ETokenType.LPAREN, ETokenType.RPAREN);
		for (int index : callIndices) {
			if (statementTokens.get(index + 1).getText()
					.equals(PRINT_STACK_TRACE)) {
				createFinding(getFindingMessage(entity), entity);
			}
		}

	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT[subtype('" + SubTypeNames.SIMPLE_STATEMENT + "')]";
	}

}
