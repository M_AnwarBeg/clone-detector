/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: heinemann $
 * @version $Rev: 55467 $
 * @ConQAT.Rating GREEN Hash: 5AADBFE82FAE54AE4F939B8673F8E37B
 */
@ACheck(name = "Switch/case without default", description = "Switch/Case statements must have a default case.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA, ELanguage.XTEND }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class SwitchMustHaveDefaultCheck extends EntityFindingCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//*[subtype('" + SubTypeNames.SWITCH
				+ "') and not(node()[subtype('" + SubTypeNames.DEFAULT
				+ "')])]";
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return "Switch/case statements must have default value!";
	}
}
