/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * 
 * @author $Author: anlauff $
 * @version $Rev: 57853 $
 * @ConQAT.Rating YELLOW Hash: 3754C0EEECCC93EC86CFA46C9A03BB68
 */

@ACheck(name = AssertWithMultipleConditionsCheck.FINDING_MESSAGE, description = "Assert statements should never test more than one feature. This way, failed tests can be linked to errors more easily.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class AssertWithMultipleConditionsCheck extends EntityFindingCheckBase {

	/** {@value} */
	protected static final String FINDING_MESSAGE = "Never check for more than one condition in assert statements";

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return FINDING_MESSAGE;
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		String firstToken = entity.includedTokens().get(0).getText();
		if (firstToken.startsWith("assert")) {
			createFinding("More than one Condition found in " + firstToken
					+ " statement", entity);
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT[anytoken('ANDAND', 'OROR', 'AND', 'OR', 'XOR')]";
	}

}
