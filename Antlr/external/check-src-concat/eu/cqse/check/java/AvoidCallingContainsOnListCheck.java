/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.ScopedTypeLookup;
import eu.cqse.check.framework.typetracker.TypedVariable;

/**
 * Detects calls to contains() on java.util.List.
 * 
 * @author $Author: ezaga$
 * @version $Rev: 57101 $
 * @ConQAT.Rating GREEN Hash: F3063F7D4380380B56C20174C5165E11
 */
@ACheck(name = "Calling contains on list", description = "Calling contains() on lists is inefficient. Use hashsets instead.", groupName = CheckGroupNames.PERFORMANCE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE,
				ECheckParameter.TYPE_RESOLUTION })
public class AvoidCallingContainsOnListCheck extends EntityCheckBase {

	/** String constant for message for finding */
	private static final String FINDING_MESSAGE = "Calling contains() on lists is inefficient.";

	/**
	 * Holds all imported types as a map from short type name to full qualified
	 * type name.
	 */
	private Map<String, String> typeImports;

	/** A set of Java classes that are list types. */
	public static final Set<String> LIST_TYPES = new HashSet<String>(
			Arrays.asList("java.util.List", "java.util.AbstractList",
					"java.util.AbstractSequentialList", "java.util.ArrayList",
					"javax.management.AttributeList",
					"java.util.concurrency.CopyOnWriteArrayList",
					"java.util.LinkedList",
					"javax.management.relation.RoleList",
					"javax.management.relation.RoleUnresolvedList",
					"java.util.Stack", "java.util.Vector"));

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT";
	}

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		typeImports = new HashMap<String, String>();
		/*List<ShallowEntity> importEntities = select(
				"/META[subtype('" + SubTypeNames.IMPORT + "')]");
		for (ShallowEntity entity : importEntities) {
			String qualifiedName = entity.getName();
			typeImports.put(StringUtils.getLastPart(qualifiedName, '.'),
					qualifiedName);
		}*/

		super.execute();
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> tokens = entity.ownStartTokens();
		List<Integer> indices = TokenStreamUtils.findTokenTypeSequences(tokens,
				0, ETokenType.DOT, ETokenType.IDENTIFIER, ETokenType.LPAREN);
		ScopedTypeLookup lookup = context.getTypeResolution()
				.getTypeLookup(entity);

		for (int index : indices) {
			if (index == 0) {
				continue;
			}

			IToken variableNameOrMember = tokens.get(index - 1);
			IToken containsCallToken = tokens.get(index + 1);
			if (containsCallToken.getText().equals("contains")) {
				processVariableNameOrMember(tokens, lookup,
						variableNameOrMember, containsCallToken, index);
			}

		}
	}

	/**
	 * Given that we have a contains call, further processing is carried out
	 * here to decide if the variable name or member on which the call was made
	 * is or returns a list.
	 */
	private void processVariableNameOrMember(List<IToken> tokens,
			ScopedTypeLookup lookup, IToken variableNameOrMember,
			IToken containsCallToken, int index) throws CheckException {
		switch (variableNameOrMember.getType()) {
		case IDENTIFIER:
			TypedVariable typedVariable = lookup
					.getTypeInfo(variableNameOrMember.getText());
			if (typedVariable != null
					&& isListType(typedVariable.getTypeName())) {
				createFinding(FINDING_MESSAGE, containsCallToken);
			}
			break;

		case RPAREN:
			checkForSubListCall(tokens, containsCallToken, index - 2);
			break;

		default:
			break;
		}
	}

	/**
	 * Checks if the method call contains is made from the subList method of a
	 * list type. If so, a finding is created.
	 */
	private void checkForSubListCall(List<IToken> tokens,
			IToken containsCallToken, int searchStartIndex)
			throws CheckException {
		if (searchStartIndex < 0) {
			return;
		}
		int indexMatchingLparen = TokenStreamUtils.findMatchingOpeningToken(
				tokens, searchStartIndex, ETokenType.LPAREN, ETokenType.RPAREN);
		if (indexMatchingLparen < 2) {
			return;
		}

		if (tokens.get(indexMatchingLparen - 2).getType() == ETokenType.DOT
				&& tokens.get(indexMatchingLparen - 1).getText()
						.equals("subList")) {
			createFinding(FINDING_MESSAGE, containsCallToken);
		}
	}

	/**
	 * Determines if the given type name (short class name or full qualified) is
	 * a list type.
	 */
	private boolean isListType(String typeName) {
		if (LIST_TYPES.contains(typeName)) {
			return true;
		}
		String qualifiedName = typeImports.get(typeName);
		return qualifiedName != null && LIST_TYPES.contains(qualifiedName);
	}
}
