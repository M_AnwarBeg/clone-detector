/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.java;

import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@AConQAT.Doc}
 *
 * @author $Author: steckermeier $
 * @version $Rev: 57774 $
 * @ConQAT.Rating YELLOW Hash: 9E8C8C207D603ABF50AE73B041CC4E7A
 */
@ACheck(name = "Don't throw Throwable, Error or RuntimeException", description = "The exceptions java.lang.Throwable, java.lang.Error, java.lang.RuntimeException may not be thrown by methods. Appropriate Subclasses should be used instead.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.RED, languages = {
		ELanguage.JAVA }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class ThrowHighlevelExceptionsCheck extends EntityCheckBase {

	/** The forbidden exception names. */
	public static final Set<Class<?>> FORBIDDEN_EXCEPTIONS = CollectionUtils
			.asHashSet(RuntimeException.class, RuntimeException.class,
					Error.class, Throwable.class, Throwable.class);

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> tokens = entity.ownStartTokens();
		int throwsPosition = TokenStreamUtils.find(tokens, ETokenType.THROWS);

		if (throwsPosition < 0) {
			return; // No throws declaration.
		}

		int closingTokenIndex = getClosingTokenIndex(tokens, throwsPosition);
		List<IToken> throwsTypeTokens = tokens.subList(throwsPosition + 1,
				closingTokenIndex);
		String[] throwTypes = TokenStreamTextUtils
				.concatTokenTexts(throwsTypeTokens).split(",");

		for (String throwType : throwTypes) {
			if (isClassNameInClassSet(FORBIDDEN_EXCEPTIONS, throwType)) {
				createFinding("Method throws generic exception of type '"
						+ throwType + "'", entity);
			}
		}
	}

	/**
	 * Determines if the class name represents one of the classes in the set.
	 */
	public static boolean isClassNameInClassSet(Set<Class<?>> classes,
			String className) {
		for (Class<?> clazz : classes) {
			if (clazz.getName().equals(className)
					|| clazz.getSimpleName().equals(className)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Tries to find the closing index after a throws declaration which is
	 * either a left brace or a semicolon.
	 * 
	 * @throws CheckException
	 *             No closing token was found.
	 */
	private static int getClosingTokenIndex(List<IToken> tokens,
			int throwsPosition) throws CheckException {
		int leftBracePosition = TokenStreamUtils.find(tokens, throwsPosition,
				ETokenType.LBRACE);

		if (leftBracePosition < 0) {
			// Abstract function with no body.
			leftBracePosition = TokenStreamUtils.find(tokens, throwsPosition,
					ETokenType.SEMICOLON);
		}

		if (leftBracePosition < 0) {
			throw new CheckException(
					"Expected left brace or semicolon throws at the end of a throws declaration on a method.");
		}

		return leftBracePosition;
	}
}
