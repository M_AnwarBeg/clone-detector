/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: StaticMethodCallRecognizer.java 53185 2015-07-03 13:54:40Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.java;

import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;

import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;

/**
 * Finds static method calls in token streams.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: BAECD7979CF236B52048D179E05DAA61
 */
public class StaticMethodCallRecognizer {

	/** The full qualified method name. */
	private final String fullQualifiedName;

	/**
	 * The sequence of token texts, that identifies the method in a token
	 * stream.
	 */
	private String[] recognitionSequence;

	/** Constructor. */
	public StaticMethodCallRecognizer(String fullQualifiedMethodName) {
		CCSMAssert.isNotNull(fullQualifiedMethodName);
		this.fullQualifiedName = fullQualifiedMethodName;
		initializeRecognitionSequence();
	}

	/** Build the recognition sequence out of the full qualified method name. */
	private void initializeRecognitionSequence() {
		String[] splitTokens = fullQualifiedName.split("\\.");
		recognitionSequence = new String[splitTokens.length * 2];
		for (int i = 0; i < splitTokens.length; i++) {
			int j = i * 2;
			recognitionSequence[j] = splitTokens[i];
			if (i == splitTokens.length - 1) {
				recognitionSequence[j + 1] = "(";
			} else {
				recognitionSequence[j + 1] = ".";
			}
		}
	}

	/** Returns all indices of method calls within the given tokens. */
	public List<Integer> findCallsInTokens(List<IToken> tokens) {
		return TokenStreamTextUtils.findAllSequences(tokens, 0, IDENTIFIER,
				recognitionSequence);
	}

	/** Returns the length of the full qualified method name in tokens. */
	public int getTokenLength() {
		return recognitionSequence.length - 1;
	}

	/** Returns a string representation of the full qualified method name. */
	public String getFullQualifiedMethodName() {
		return fullQualifiedName;
	}
}
