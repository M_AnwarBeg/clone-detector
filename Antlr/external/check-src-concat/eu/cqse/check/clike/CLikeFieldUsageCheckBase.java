/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.clike;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.CLikeLanguageFeatureParserBase;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * Base class for checks that analyze the usage of fields in entities. By
 * default if no usage is found, a finding is created. This behavior can be
 * inverted by overriding {@link #createFindingOnFound()} and returning true.
 *
 * @author $Author: kupka$
 * @version $Rev: 57102 $
 * @ConQAT.Rating GREEN Hash: DADBECA0237436CD36C7CCD1D26786E0
 */
public abstract class CLikeFieldUsageCheckBase extends CheckImplementationBase {

	/** The internally used language utilities. */
	protected CLikeLanguageFeatureParserBase languageFeatureParser;

	/** Constructor. */
	public CLikeFieldUsageCheckBase(CLikeLanguageFeatureParserBase utils) {
		CCSMAssert.isNotNull(utils);
		this.languageFeatureParser = utils;
	}

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		/*List<ShallowEntity> selectedFields = select(
				context.getUnfilteredRootEntity(), getFieldSelectionXPath());
		for (ShallowEntity field : selectedFields) {
			processField(field);
		}*/
	}

	/**
	 * Checks if a particular sought-after annotation on a field is relevant
	 * from exclusion from the check.
	 */
	protected boolean isAnnotationRelevant(String annotationToFind,
			List<ShallowEntity> annotationsOnField) {
		return annotationsOnField.stream().anyMatch(
				annotation -> annotation.getName().equals(annotationToFind));
	}

	/**
	 * Helps child classes to filter out fields by annotations if they are
	 * provided, just before the are processed further.
	 */
	private boolean isFilteredByAnnotation(ShallowEntity field) {
		Set<String> annotationsToFind = getAnnotations();
		if (annotationsToFind.isEmpty()) {
			return false;
		}

		List<ShallowEntity> annotationsOnField = LanguageFeatureParser.JAVA
				.getAnnotations(field);

		return annotationsToFind.stream().anyMatch(
				annotationToFind -> isAnnotationRelevant(annotationToFind,
						annotationsOnField));
	}

	/** Processes the given field. */
	private void processField(ShallowEntity field) throws CheckException {
		/*if (isFilteredByAnnotation(field)) {
			return;
		}

		List<ShallowEntity> processedEntities = select(field.getParent(),
				getProcessedEntitiesXPath());
		processedEntities.remove(field);

		// If someone declares multiple attributes like this:
		// private int x, y;
		// it is necessary to create multiple findings, because otherwise it
		// is ambiguous, which field is unused.
		for (IToken fieldNameToken : languageFeatureParser
				.getVariableNamesFromTokens(field.ownStartTokens())) {
			if (createFindingOnFound() == analyze(processedEntities, field,
					fieldNameToken.getText())) {
				createFinding(getFindingMessage(fieldNameToken.getText()),
						fieldNameToken);
			}
		}*/
	}

	/**
	 * Walks through all given entities and checks their start and end tokens
	 * for uses of the field with the given field name. This is done executing
	 * the {@link #analyzeTokens(List, String, boolean)} method. If a match for
	 * the analyzed field usage criterion was found for any entity, true is
	 * returned, otherwise false.
	 */
	private boolean analyze(List<ShallowEntity> entities, ShallowEntity field,
			String fieldName) throws CheckException {

		for (ShallowEntity entity : entities) {
			boolean isShadowed = context.getUnfilteredTypeResolution()
					.getTypeLookup(entity)
					.isShadowingVariable(fieldName, field);

			boolean foundInTokens = analyzeTokens(entity.ownStartTokens(),
					fieldName, isShadowed);
			if (!foundInTokens && entity.hasChildren()) {
				foundInTokens = analyzeTokens(entity.ownEndTokens(), fieldName,
						isShadowed);
			}
			if (foundInTokens) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Provides a set of field annotations which will be used to filter out
	 * fields from further processing. Each annotation string should be without
	 * the at (@) symbol. Child classes requiring this feature should override
	 * this method.
	 */
	protected Set<String> getAnnotations() {
		return new HashSet<String>();
	}

	/**
	 * Analyzes the given tokens for field usages. Returns true if a match for
	 * the analyzed field usage criterion was found, otherwise false.
	 *
	 * @param fieldName
	 *            the name of the field
	 * @param isShadowed
	 *            whether the field is currently shadowed by a local variable
	 */
	protected abstract boolean analyzeTokens(List<IToken> tokens,
			String fieldName, boolean isShadowed);

	/**
	 * Returns a xPath string for selecting all fields that should be analyzed.
	 */
	protected abstract String getFieldSelectionXPath();

	/**
	 * Returns a xPath string for selecting all entities that are walked through
	 * and checked for field usages.
	 */
	protected abstract String getProcessedEntitiesXPath();

	/** Returns a finding message for the given field name. */
	protected abstract String getFindingMessage(String fieldName);

	/**
	 * Returns true if a finding should be created, if a field usage is found.
	 * Otherwise it returns false.
	 */
	protected boolean createFindingOnFound() {
		return false;
	}
}
