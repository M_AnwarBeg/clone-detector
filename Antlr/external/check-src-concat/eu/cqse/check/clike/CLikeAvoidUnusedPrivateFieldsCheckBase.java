/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: CLikeAvoidUnusedPrivateFieldsCheckBase.java 56513 2016-04-06 05:26:32Z poehlmann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.clike;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.util.CLikeLanguageFeatureParserBase;

/**
 * Base class for checks detecting unused private fields in clike languages.
 * 
 * @author $Author: poehlmann $
 * @version $Rev: 56513 $
 * @ConQAT.Rating GREEN Hash: C259DAA01F3050D94C92AB81F943776B
 */
public abstract class CLikeAvoidUnusedPrivateFieldsCheckBase
		extends CLikeFieldUsageCheckBase {

	/** Description of this check. */
	public static final String DESCRIPTION = "A private field is declared but never read.";

	/** Constructor. */
	public CLikeAvoidUnusedPrivateFieldsCheckBase(
			CLikeLanguageFeatureParserBase languageFeatureParser) {
		super(languageFeatureParser);
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(String fieldName) {
		return "Private field '" + fieldName + "' is never read.";
	}

	/** {@inheritDoc} */
	@Override
	protected boolean analyzeTokens(List<IToken> tokens, String fieldName,
			boolean isShadowed) {
		return !languageFeatureParser
				.getVariableReadsFromTokens(tokens, fieldName, true, isShadowed)
				.isEmpty();
	}
}
