/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.clike;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.util.ShallowParsingUtils;

/**
 * Base class for hash-code and equals checks. A class must either implement
 * both methods or none of them.
 * 
 * @author $Author: poehlmann $
 * @version $Rev: 56511 $
 * @ConQAT.Rating GREEN Hash: 8F56A8027094AB7997493ABE21DB48DF
 */
public abstract class HashCodeAndEqualsCheckBase extends EntityCheckBase {

	/** Returns the name of the hash-code method. */
	protected abstract String getHashCodeName();

	/** Returns the name of the equals method. */
	protected abstract String getEqualsName();

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		ShallowEntity equalsEntity = getMethodEntity(entity, getEqualsName(),
				1);
		ShallowEntity hashCodeEntity = getMethodEntity(entity,
				getHashCodeName(), 0);

		if (equalsEntity != null && hashCodeEntity == null) {
			createFindingOnFirstLine(
					getEqualsName() + "() but not " + getHashCodeName()
							+ "() implemented in class " + entity.getName(),
					equalsEntity);
		} else if (equalsEntity == null && hashCodeEntity != null) {
			createFindingOnFirstLine(
					getHashCodeName() + "() but not " + getEqualsName()
							+ "() implemented in class " + entity.getName(),
					hashCodeEntity);
		}
	}

	/**
	 * Returns the first method entity of the given type, that has the given
	 * name and parameter count.
	 */
	protected ShallowEntity getMethodEntity(ShallowEntity type, String name,
			int parameterCount) throws CheckException {
		/*for (ShallowEntity methodEntity : select(type,
				"METHOD[subtype('method') and name-matches('" + name + "')]")) {
			if (ShallowParsingUtils.extractParameterNameTokens(methodEntity)
					.size() == parameterCount) {
				return methodEntity;
			}
		}*/
		return null;
	}
}
