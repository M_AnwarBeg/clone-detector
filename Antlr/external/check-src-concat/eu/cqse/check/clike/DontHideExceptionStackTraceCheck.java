/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.clike;

import static eu.cqse.check.framework.scanner.ELanguage.CS;
import static eu.cqse.check.framework.scanner.ELanguage.JAVA;
import static eu.cqse.check.framework.scanner.ETokenType.CATCH;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.THROW;

import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.tokens.TokenPattern;
import eu.cqse.check.framework.util.tokens.TokenPatternMatch;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kinnen $
 * @version $Rev: 56623 $
 * @ConQAT.Rating GREEN Hash: AC405ADA28EBE09F1F3D9127978672AB
 */
@ACheck(name = "Exception stacktrace is lost", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		JAVA, CS }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE }, description = ""
						+ "If you catch an exception and then throw a different exception, you should preserve the original stacktrace by"
						+ " passing the caught exception to the constructor of the thrown exception."
						+ " Failing to do so will cause the original stacktrace to be lost, which will hinder debugging the root cause.")
public class DontHideExceptionStackTraceCheck extends EntityCheckBase {

	/**
	 * The message of all findings created by this check.
	 */
	private static final String FINDING_MESSAGE = "Exception stacktrace is lost";

	/**
	 * Matches the name and type of the caught exception in a catch entity.
	 * Group 1 contains the type, group 2 contains the name.
	 */
	private static final TokenPattern EXCEPTION_VARIABLE_NAME_PATTERN = new TokenPattern()
			.sequence(IDENTIFIER).group(1).sequence(IDENTIFIER).group(2)
			.sequence(RPAREN, LBRACE);

	/**
	 * Matches a single variable being used as a constructor argument. Might
	 * also match more than that, but we would rather have less false positives
	 * than a higher precision, thus this is OK. Group 1 contains the name of
	 * the matched variable.
	 */
	private static final TokenPattern VARIABLE_AS_CONSTRUCTOR_ARGUMENT_PATTERN = new TokenPattern()
			.sequence(EnumSet.of(LPAREN, COMMA)).sequence(IDENTIFIER).group(1)
			.sequence(EnumSet.of(RPAREN, COMMA));

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Ignored exception types", description = "A list of exception types for which not to check whether the stacktrace is passed on.")
	private Set<String> ignoredExceptionTypes = CollectionUtils
			.asHashSet("NumberFormatException");

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT[subtype('" + SubTypeNames.CATCH + "')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		if (context.getLanguage() == ELanguage.CS
				&& isAnonymousCsCatchBlock(entity)) {
			checkThrows(entity,
					this::checkThrowsStatementInAnonymousCsCatchBlock);
			return;
		}

		TokenPatternMatch exceptionVariableMatch = EXCEPTION_VARIABLE_NAME_PATTERN
				.findFirstMatch(entity.ownStartTokens());
		if (exceptionVariableMatch == null) {
			return;
		}

		if (ignoredExceptionTypes
				.contains(exceptionVariableMatch.groupString(1))) {
			return;
		}

		String exceptionVariableName = exceptionVariableMatch.groupString(2);
		checkThrows(entity,
				throwEntity -> checkThrowsStatement(exceptionVariableName,
						throwEntity));
	}

	/**
	 * Returns <code>true</code> if the given entity is an anonymous C# catch
	 * block: <code>catch {</code> or <code>catch (ExceptionClass) {</code>
	 */
	private static boolean isAnonymousCsCatchBlock(ShallowEntity entity) {
		return TokenStreamUtils.hasTypes(entity.ownStartTokens(), CATCH, LBRACE)
				|| TokenStreamUtils.hasTypes(entity.ownStartTokens(), CATCH,
						LPAREN, IDENTIFIER, RPAREN, LBRACE);
	}

	/**
	 * Check all throw statements in the given catch entity by applying the
	 * given check function to them.
	 */
	private void checkThrows(ShallowEntity catchEntity,
			ThrowEntityChecker checkFunction) throws CheckException {
		/*for (ShallowEntity throwEntity : select(catchEntity,
				".//STATEMENT[subtype('" + SubTypeNames.SIMPLE_STATEMENT
						+ "') and name-matches('throw')]")) {
			checkFunction.check(throwEntity);
		}*/
	}

	/**
	 * Checks a throw statement inside an anonymous C# catch block.
	 * 
	 * @see #isAnonymousCsCatchBlock(ShallowEntity)
	 */
	private void checkThrowsStatementInAnonymousCsCatchBlock(
			ShallowEntity throwEntity) throws CheckException {
		if (!TokenStreamUtils.hasTypes(throwEntity.includedTokens(), THROW,
				SEMICOLON)) {
			createFinding(throwEntity);
		}
	}

	/** Checks a normal throw statement. */
	private void checkThrowsStatement(String exceptionVariableName,
			ShallowEntity throwEntity) throws CheckException {
		if (!TokenStreamUtils.containsAny(throwEntity.ownStartTokens(), NEW)) {
			return;
		}

		List<TokenPatternMatch> matches = VARIABLE_AS_CONSTRUCTOR_ARGUMENT_PATTERN
				.findAll(throwEntity.includedTokens());

		for (TokenPatternMatch match : matches) {
			if (match.groupString(1).equals(exceptionVariableName)) {
				return;
			}
		}

		createFinding(throwEntity);
	}

	/**
	 * Creates the finding produced by this check for the given throw entity.
	 */
	private void createFinding(ShallowEntity throwEntity)
			throws CheckException {
		createFindingOnFirstLine(FINDING_MESSAGE, throwEntity);
	}

	/**
	 * Checks a throw entity and may create findings for it. Needed so we can
	 * throw {@link CheckException}.
	 */
	@FunctionalInterface
	private static interface ThrowEntityChecker {

		/** Performs the check and may create a findings for the entity. */
		public void check(ShallowEntity throwEntity) throws CheckException;

	}
}
