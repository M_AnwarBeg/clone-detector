/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.clike;

import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.SetMap;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.util.ShallowParsingUtils;
import eu.cqse.check.framework.util.CLikeLanguageFeatureParserBase;

/**
 * Base class for c-like avoid unused private methods checks.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56583 $
 * @ConQAT.Rating YELLOW Hash: 4CF33AF37AAD6384AA8A68B647285ECB
 */
public abstract class CLikeAvoidUnusedPrivateMethodsCheckBase
		extends CheckImplementationBase {

	/** The general check description. */
	public static final String DESCRIPTION = "This rule is reported when a private method exists but is never called by any code path.";

	/**
	 * Value for a unknown method arity. This value is used if a method name is
	 * found in a method reference.
	 */
	protected static final int UNKNOWN_ARITY = -1;

	/** The internally used language utilities. */
	protected CLikeLanguageFeatureParserBase languageFeatureParser;

	/** Constructor. */
	public CLikeAvoidUnusedPrivateMethodsCheckBase(
			CLikeLanguageFeatureParserBase languageFeatureParser) {
		CCSMAssert.isNotNull(languageFeatureParser);
		this.languageFeatureParser = languageFeatureParser;
	}

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		/*SetMap<String, Integer> methodCalls = retrieveMethodUsages();

		List<ShallowEntity> methods = select(context.getUnfilteredRootEntity(),
				"//METHOD[not(subtype('" + SubTypeNames.CONSTRUCTOR
						+ "')) and modifiers('private')]");
		for (ShallowEntity method : CollectionUtils.filter(methods,
				this::isMethodIncluded)) {
			String methodName = method.getName();
			Set<Integer> callArities = methodCalls.getCollection(methodName);
			if (callArities == null || !isCalled(method, callArities)) {
				createFindingOnFirstLine(
						"Private method '" + methodName + "' is unused",
						method);
			}
		}*/
	}

	/**
	 * Retrieves all method calls and references within the analyzed token
	 * element. A mapping from method names to the called method arities is
	 * returned.
	 */
	private SetMap<String, Integer> retrieveMethodUsages()
			throws CheckException {
		SetMap<String, Integer> calledMethods = new SetMap<>();
		// TODO (MP) Also see other CR:
		// CsAvoidUnusedPrivateFieldsCheck.getProcessedEntitiesXPath here the
		// selector contains way more expressions for variable usage, e.g. in
		// switch...
		// TODO (MP) Stupid question: Can we unify attribute/method usage for
		// both checks into a e.g. a common class that is reusable? Obviously
		// one has to either abstract or implement different method reference
		// types
		// TODO (AK) I think where a method reference and a variable use can
		// occur are two different things. E. g. you can not use method
		// references in case statements.
		/*for (ShallowEntity entity : select(context.getUnfilteredRootEntity(),
				"//STATEMENT | //ATTRIBUTE")) {
			retrieveMethodUsages(entity, calledMethods);
		}*/
		return calledMethods;
	}

	/**
	 * Retrieves all method calls and references from the given shallow entity
	 * (ignoring children).
	 */
	private void retrieveMethodUsages(ShallowEntity entity,
			SetMap<String, Integer> calledMethods) {
		retrieveMethodUsages(entity.ownStartTokens(), calledMethods);
		if (entity.hasChildren()) {
			retrieveMethodUsages(entity.ownEndTokens(), calledMethods);
		}
	}

	/** Retrieves all method calls from the given tokens. */
	private void retrieveMethodUsages(List<IToken> tokens,
			SetMap<String, Integer> calledMethods) {
		retrieveMethodCalls(tokens, calledMethods);
		retrieveMethodReferences(tokens, calledMethods);
	}

	/** Retrieves all method calls from the given tokens. */
	protected void retrieveMethodCalls(List<IToken> tokens,
			SetMap<String, Integer> calledMethods) {
		List<Integer> callIndices = TokenStreamUtils
				.findTokenTypeSequences(tokens, 0, IDENTIFIER, LPAREN);
		for (int callIndex : callIndices) {
			List<IToken> parameters = TokenStreamUtils.tokensBetweenWithNesting(
					tokens, callIndex + 1, LPAREN, RPAREN);
			int methodCallArity = 0;
			if (parameters.size() > 0) {
				methodCallArity = TokenStreamUtils.splitWithNesting(parameters,
						COMMA, Arrays.asList(LPAREN, LBRACK, LBRACE),
						Arrays.asList(RPAREN, RBRACK, RBRACE)).size();
			}
			calledMethods.add(tokens.get(callIndex).getText(), methodCallArity);
		}
	}

	/** Retrieves all method references from the given tokens. */
	protected abstract void retrieveMethodReferences(List<IToken> tokens,
			SetMap<String, Integer> calledMethods);

	/**
	 * Checks whether a method is called based on the length of its argument
	 * list.
	 */
	private boolean isCalled(ShallowEntity method, Set<Integer> callArities) {
		Set<Integer> methodCallArities = getMethodCallArities(
				languageFeatureParser.getParameterTokens(method));
		return callArities.contains(UNKNOWN_ARITY)
				|| !Collections.disjoint(methodCallArities, callArities)
				|| isCalledWithHigherArityAndHasVariableLengthArgumentList(
						method, Collections.min(methodCallArities),
						callArities);
	}

	/**
	 * Returns a set of possible method call arities for the given parameter
	 * tokens. This is necessary to represent the different possible arities for
	 * extension methods. The default implementation returns a set that contains
	 * the size of the given parameter tokens list.
	 */
	protected Set<Integer> getMethodCallArities(
			List<List<IToken>> parameterTokens) {
		return CollectionUtils.asHashSet(parameterTokens.size());
	}

	/**
	 * Returns whether this method accepts a variable length argument list and
	 * is called with more arguments than expected.
	 */
	private boolean isCalledWithHigherArityAndHasVariableLengthArgumentList(
			ShallowEntity method, int minimumArity, Set<Integer> callArities) {
		return languageFeatureParser.hasVariableLengthArgumentList(method)
				&& containsValueHigherOrEqualThan(callArities, minimumArity);
	}

	/**
	 * Tests if a given set contains at least one value that is higher or equal
	 * to the comparison value.
	 */
	private static boolean containsValueHigherOrEqualThan(Set<Integer> values,
			int comparisonValue) {
		return values.stream().anyMatch(value -> value >= comparisonValue);
	}

	/** Returns whether this methods shall be included by this analysis. */
	private boolean isMethodIncluded(ShallowEntity method) {
		Set<Integer> parameterCounts = getExcludedMethods()
				.getCollection(method.getName());
		return parameterCounts == null || !parameterCounts.contains(
				ShallowParsingUtils.extractParameterNameTokens(method).size());
	}

	/**
	 * Returns methods that are excluded from the check. The methods are
	 * returned as mapping from method names to arities.
	 */
	protected SetMap<String, Integer> getExcludedMethods() {
		return null;
	}
}
