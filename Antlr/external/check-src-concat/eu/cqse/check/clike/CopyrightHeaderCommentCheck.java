/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.clike;

import java.util.List;

import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;

/**
 * The purpose of this check is to see that every code file of (any of) our
 * client requiring this feature, has a copyright header @ the top of it.
 * 
 * @author $Author: ezaga$
 * @version $Rev: 57281 $
 * @ConQAT.Rating RED Hash: D4A1E1B4957D3C791341B669C249C55C
 */
@ACheck(name = "File must have a valid copyright header", description = "Ensures that each file has a valid copyright header.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
		ELanguage.JAVA, ELanguage.CPP, ELanguage.JAVASCRIPT,
		ELanguage.CS }, parameters = { ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class CopyrightHeaderCommentCheck extends CheckImplementationBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Copyright Header", description = "Company copyright information that should be contained in every code file.", multilineText = true)
	private String multilineCopyrightComment = "/**\n* Sample Copyright Comment.\n*/";

	/** Message to be displayed when no copyright information is found. */
	// TODO (LH) Please use the name of this check (available via
	// Finding#getTypeId) to identify a finding created by this check instead of
	// depending on the message
	public static final String FINDING_MESSAGE = "File is not (or incorrectly) copyrighted.";

	/**
	 * Stores upon initialization, the normalized expected copyright comment.
	 */
	private String normalizedExpectedComment;

	/** {@inhertDoc} */
	@Override
	public void initialize() {
		normalizedExpectedComment = normalizeText(multilineCopyrightComment);
	}

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		List<IToken> tokens = context.getTokens();
		if (tokens.size() > 0) {
			IToken token = tokens.get(0);
			if (token.getType().getTokenClass() != ETokenClass.COMMENT) {
				createFinding(FINDING_MESSAGE, token);
				return;
			}

			if (!normalizedExpectedComment
					.equals(normalizeText(token.getText()))) {
				createFinding(FINDING_MESSAGE, token);
			}

		}
	}

	/**
	 * Normalizes comment text by removing all occurrences of line breaks and
	 * whitespace characters.
	 */
	private static String normalizeText(String text) {
		String normalizedText = StringUtils.removeWhitespace(text);
		return StringUtils.replaceLineBreaks(normalizedText,
				StringUtils.EMPTY_STRING);
	}

}
