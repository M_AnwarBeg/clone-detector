/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.clike;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.base.EntityFindingCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Checks for the use of ternary operators in clike languages.
 * 
 * @author $Author: ezaga$
 * @version $Rev: 56987 $
 * @ConQAT.Rating GREEN Hash: 7A2B492CD4C8EEF6D307C4933E516D85
 */
@ACheck(name = "Avoid using ternary operators", description = "Ternary operators make debugging difficult "
		+ "and represent code that is hard to read.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.JAVASCRIPT, ELanguage.JAVA, ELanguage.CS,
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class DontUseTernaryOperatorCheck extends EntityFindingCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {

		List<IToken> tokens = entity.ownStartTokens();
		if (TokenStreamUtils.containsAll(tokens, ETokenType.QUESTION,
				ETokenType.COLON)) {
			List<Integer> indices = TokenStreamUtils.findAll(tokens,
					EnumSet.of(ETokenType.QUESTION));
			for (int i : indices) {
				checkSurroundingOfQuestionMark(entity, tokens, i);
			}
		}

	}

	/**
	 * Checks the surrounding of the question mark to exclude all cases which
	 * are not ternary operators.
	 */
	private void checkSurroundingOfQuestionMark(ShallowEntity entity,
			List<IToken> tokens, int indexQuestionMark) throws CheckException {
		if (indexQuestionMark < 1) {
			return;
		}

		// Avoid Java generic-type statements.
		if (tokens.get(indexQuestionMark - 1).getType() == ETokenType.LT) {
			return;
		}

		// What precedes or succeeds the ? character now could be (, [ or an
		// identifier like in a C# Null conditional
		// https://msdn.microsoft.com/en-us/library/dn986595(v=vs.140).aspx
		if (TokenStreamUtils.findMatchingClosingToken(tokens,
				indexQuestionMark + 1, ETokenType.QUESTION,
				ETokenType.COLON) > TokenStreamUtils.NOT_FOUND) {
			createFinding(getFindingMessage(entity),
					tokens.get(indexQuestionMark));
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getFindingMessage(ShallowEntity entity) {
		return "Avoid using ternary operators";
	}

}
