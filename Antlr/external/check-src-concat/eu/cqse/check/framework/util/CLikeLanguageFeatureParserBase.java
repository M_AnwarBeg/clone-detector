/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.GT;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.LT;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.VOID;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.Pair;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.variable.CLikeVariableUseExtractor;

/**
 * Abstract base class for clike language feature parser.
 * 
 * @author $Author: kupka $
 * @version $Rev: 57172 $
 * @ConQAT.Rating YELLOW Hash: 1634D9C08C51AF50D794C940DAC745A9
 */
public abstract class CLikeLanguageFeatureParserBase
		implements ILanguageFeatureParser {

	/** Conditional operator types. */
	public static final EnumSet<ETokenType> CONDITIONAL_OPERATOR_TYPES = EnumSet
			.of(ETokenType.EQEQ, ETokenType.LTEQ, ETokenType.GTEQ,
					ETokenType.LT, ETokenType.GT, ETokenType.NOTEQ);

	/**
	 * Common token types for all clike languages, that indicate, that the
	 * previous identifier is no variable name.
	 */
	protected static final Set<ETokenType> BASIC_NO_VARIABLE_SUCCESSOR_TYPES = EnumSet
			.of(GT);

	/** All token types that can be used as valid identifiers. */
	protected EnumSet<ETokenType> validIdentifiers;

	/** All token types that can be used to declare a type. */
	protected EnumSet<ETokenType> typeTokens;

	/**
	 * The variable use extractor to find uses of names (e.g. variables or
	 * fields).
	 */
	protected CLikeVariableUseExtractor useExtractor;

	/**
	 * All token types, that indicate, that the previous identifier is no
	 * variable name.
	 */
	protected EnumSet<ETokenType> noVariableSuccessorTypes;

	// TODO (MP) I'd pass the language as constructor parameter
	/** Constructor. */
	protected CLikeLanguageFeatureParserBase(
			EnumSet<ETokenType> validIdentifiers,
			EnumSet<ETokenType> typeTokens) {
		this.validIdentifiers = validIdentifiers;
		this.typeTokens = typeTokens;
		this.noVariableSuccessorTypes = EnumSet.copyOf(validIdentifiers);
		this.noVariableSuccessorTypes.addAll(BASIC_NO_VARIABLE_SUCCESSOR_TYPES);
		this.useExtractor = getVariableUseExtractor();
	}

	/** Returns whether the given tokens are a variable declaration. */
	public boolean isVariableDeclaration(List<IToken> variableTokens) {
		int equalIndex = TokenStreamUtils.find(variableTokens, EQ);
		// If there is an equals sign we assume the variable name to be in front
		// it. Otherwise the last valid identifier will be used.
		if (equalIndex == TokenStreamUtils.NOT_FOUND) {
			return TokenStreamUtils.findLast(variableTokens, 0,
					variableTokens.size(), validIdentifiers) > 0;
		}

		return (equalIndex - 1) > 0;
	}

	/**
	 * Returns whether the given type name matches the given full qualified type
	 * name considering the given imported namespaces. If the given type name is
	 * null, false is returned.
	 */
	public boolean matchesFullQualifiedTypeName(String typeName,
			List<String> importedNamespaces, String fullQualifiedTypeName) {
		if (typeName == null) {
			return false;
		} else if (typeName.equals(fullQualifiedTypeName)) {
			return true;
		}

		for (String namespace : importedNamespaces) {
			String potentialFullQualifiedTypeName = namespace + "." + typeName;
			if (potentialFullQualifiedTypeName.equals(fullQualifiedTypeName)) {
				return true;
			}
		}

		return false;
	}

	/** Extracts all tokens that are part of a condition from a statement. */
	public List<IToken> extractConditionTokens(ShallowEntity statement) {
		String subtype = statement.getSubtype();
		if (subtype.equals(SubTypeNames.FOR)) {
			return TokenStreamUtils.tokensBetween(statement.ownStartTokens(),
					ETokenType.SEMICOLON, ETokenType.SEMICOLON);
		} else if (subtype.equals(SubTypeNames.DO)) {
			List<IToken> tokens = statement.includedTokens();
			int whileOffset = TokenStreamUtils.findLast(tokens,
					ETokenType.WHILE);
			return TokenStreamUtils.tokensBetweenWithNesting(tokens,
					whileOffset, ETokenType.LPAREN, ETokenType.RPAREN);
		} else {
			return TokenStreamUtils.tokensBetweenWithNesting(
					statement.ownStartTokens(), ETokenType.LPAREN,
					ETokenType.RPAREN);
		}
	}

	/** Returns whether the given entity is static. */
	public boolean isStatic(ShallowEntity entity) {
		return TokenStreamUtils.find(entity.ownStartTokens(),
				ETokenType.STATIC) != TokenStreamUtils.NOT_FOUND;
	}

	/**
	 * Returns whether the given entity only has static members. If there are no
	 * attribute and methods members, <code>false</code> is returned.
	 */
	public boolean hasOnlyStaticMembers(ShallowEntity clazz) {
		CCSMAssert.isTrue(clazz.getType() == EShallowEntityType.TYPE,
				"clazz.getType() must be \"TYPE\"");
		CCSMAssert.isTrue(clazz.getSubtype().equals("class"),
				"clazz.getSubType() must be \"class\"");

		List<ShallowEntity> children = clazz.getChildren();
		int childCount = 0;
		for (ShallowEntity child : children) {
			if (child.getType() == EShallowEntityType.ATTRIBUTE
					|| child.getType() == EShallowEntityType.METHOD
					|| child.getType() == EShallowEntityType.TYPE) {
				childCount++;
				if (!isStatic(child)) {
					return false;
				}
			}
		}

		return childCount > 0;
	}

	/** Returns a list of all generic names for the given type. */
	public List<String> getGenericNames(ShallowEntity type) {
		CCSMAssert.isTrue(type.getType() == EShallowEntityType.TYPE,
				"Expected type entity");
		List<IToken> typeTokens = type.ownStartTokens();
		// find the type's name
		int nameIndex = TokenStreamUtils.find(typeTokens,
				ETokenType.IDENTIFIER);
		if (nameIndex == TokenStreamUtils.NOT_FOUND
				|| nameIndex >= typeTokens.size() - 1) {
			return CollectionUtils.emptyList();
		}

		// check if the name is followed by a '<'
		if (typeTokens.get(nameIndex + 1).getType() != ETokenType.LT) {
			return CollectionUtils.emptyList();
		}

		List<IToken> genericTokens = TokenStreamUtils.tokensBetweenWithNesting(
				typeTokens, ETokenType.LT, ETokenType.GT);
		List<List<IToken>> splitTokens = TokenStreamUtils.split(genericTokens,
				ETokenType.COMMA);

		return TokenStreamTextUtils.concatAllTokenTexts(splitTokens);
	}

	/** Returns whether the given method has a void return type. */
	public boolean hasVoidReturnType(ShallowEntity method) {
		CCSMAssert.isTrue(method.getType() == EShallowEntityType.METHOD,
				"method.getType() must be \"METHOD\"");
		List<IToken> methodTokens = method.ownStartTokens();
		// if the return type is void, it is two tokens before the opening
		// parenthesis, as it cannot be generic
		int returnTypeIndex = TokenStreamUtils.find(methodTokens,
				ETokenType.LPAREN) - 2;
		if (returnTypeIndex < 0) {
			return false;
		}

		return methodTokens.get(returnTypeIndex).getType().equals(VOID);
	}

	/** Returns a list of parameter token lists for the given method. */
	public List<List<IToken>> getParameterTokens(ShallowEntity method) {
		CCSMAssert.isTrue(method.getType() == EShallowEntityType.METHOD,
				"method.getType() must be \"METHOD\"");
		List<IToken> parameterTokens = TokenStreamUtils
				.tokensBetweenWithNesting(method.ownStartTokens(),
						ETokenType.LPAREN, ETokenType.RPAREN);
		if (parameterTokens.isEmpty()) {
			return CollectionUtils.emptyList();
		}
		return splitVariableTokens(parameterTokens);
	}

	/**
	 * Extracts a list of modifiers and a type name from the given token list.
	 * The given tokens must have the pattern of a variable declaration. If no
	 * type can be found, an empty list and null is returned. The generic part
	 * of types is omitted.
	 */
	public Pair<List<String>, String> getModifiersAndTypeFromTokens(
			List<IToken> tokens) {
		int endIndex = TokenStreamUtils.find(tokens, EQ);
		if (endIndex == TokenStreamUtils.NOT_FOUND) {
			// if there is no assignment, we search for the variable name
			endIndex = TokenStreamUtils.findLast(tokens, validIdentifiers);
		} else {
			// otherwise we set the end index to the variable name which is in
			// front of the equals sign
			endIndex -= 1;
		}

		// skip the generic part of the type
		if (endIndex > 1 && tokens.get(endIndex - 1).getType().equals(GT)) {
			endIndex = TokenStreamUtils.findMatchingOpeningToken(tokens,
					endIndex - 2, LT, GT);
		}

		int typeIndex = TokenStreamUtils.findLast(tokens, 0, endIndex,
				typeTokens);
		if (typeIndex == TokenStreamUtils.NOT_FOUND) {
			return new Pair<List<String>, String>(
					CollectionUtils.<String> emptyList(), null);
		}

		typeIndex = getTypeNameStartIndex(tokens, typeIndex);
		List<String> typeNameTokens = TokenStreamTextUtils.getTokenTexts(tokens,
				typeIndex, endIndex);
		String typeName = StringUtils.concat(typeNameTokens,
				StringUtils.EMPTY_STRING);
		List<String> modifiers = TokenStreamTextUtils.getTokenTexts(tokens, 0,
				typeIndex);
		return new Pair<List<String>, String>(modifiers, typeName);
	}

	/**
	 * Returns a list of lists of tokens that represent the parameters of the
	 * given method.
	 */
	public List<List<IToken>> getSplitParameterTokens(ShallowEntity method) {
		List<IToken> parameterTokens = TokenStreamUtils
				.tokensBetweenWithNesting(method.ownStartTokens(),
						ETokenType.LPAREN, ETokenType.RPAREN);
		if (parameterTokens.isEmpty()) {
			return CollectionUtils.emptyList();
		}

		List<List<IToken>> splitParameterTokens = TokenStreamUtils
				.splitWithNesting(parameterTokens, ETokenType.COMMA,
						Arrays.asList(ETokenType.LPAREN, ETokenType.LBRACK,
								ETokenType.LBRACE),
						Arrays.asList(ETokenType.RPAREN, ETokenType.RBRACK,
								ETokenType.RBRACE));
		return splitParameterTokens;
	}

	/** Returns all parameter type names of the given method. */
	public List<String> getParameterTypeNames(ShallowEntity method) {
		return CollectionUtils.map(getSplitParameterTokens(method),
				tokens -> getModifiersAndTypeFromTokens(tokens).getSecond());
	}

	/**
	 * Return the start index of the type name that ends at the given index
	 * within the given token list. This method is used to find type names that
	 * consist of multiple tokens, e. g. names that are prefixed with a
	 * namespace.
	 */
	protected int getTypeNameStartIndex(List<IToken> tokens,
			int lastTypeTokenIndex) {
		int i = lastTypeTokenIndex;
		while (i > 1) {
			ETokenType first = tokens.get(i - 1).getType();
			ETokenType second = tokens.get(i - 2).getType();

			if (first != DOT || !validIdentifiers.contains(second)) {
				break;
			}

			i -= 2;
		}
		return i;
	}

	/**
	 * Splits the given variable declaration tokens at commas regarding nesting
	 * between parenthesis, brackets, braces and angle brackets.
	 */
	public List<List<IToken>> splitVariableTokens(List<IToken> variableTokens) {
		List<List<IToken>> splitTokens = TokenStreamUtils.splitWithNesting(
				variableTokens, COMMA,
				Arrays.asList(LPAREN, LBRACK, LBRACE, LT),
				Arrays.asList(RPAREN, RBRACK, RBRACE, GT));
		return splitTokens;
	}

	/** Returns all variable name tokens from the given variable tokens. */
	public List<IToken> getVariableNamesFromTokens(
			List<IToken> variableTokens) {
		List<List<IToken>> splitTokens = splitVariableTokens(variableTokens);

		List<IToken> variableNames = new ArrayList<>();
		for (List<IToken> tokens : splitTokens) {
			IToken name = getVariableNameFromTokens(tokens);
			if (name != null) {
				variableNames.add(name);
			}
		}
		return variableNames;
	}

	/**
	 * Returns the variable name token from the given token list or
	 * <code>null</code> if none is found. The list of tokens must have the
	 * pattern of a variable declaration.
	 */
	public IToken getVariableNameFromTokens(List<IToken> tokens) {
		int equalIndex = TokenStreamUtils.find(tokens, EQ);
		// If there is an equals sign we assume the variable name to be in front
		// it. Otherwise the last valid identifier will be used.
		int nameIndex = equalIndex - 1;
		if (equalIndex == TokenStreamUtils.NOT_FOUND) {
			nameIndex = TokenStreamUtils.findLast(tokens, 0, tokens.size(),
					validIdentifiers);
		}

		if (nameIndex < 0) {
			return null;
		}
		return tokens.get(nameIndex);
	}

	/**
	 * Returns the variable name of an exception within a catch statement or
	 * null if the exception is not named.
	 */
	public IToken getVariableNameFromCatchTokens(List<IToken> catchTokens) {
		List<IToken> exceptionTokens = TokenStreamUtils.tokensBetween(
				catchTokens, ETokenType.LPAREN, ETokenType.RPAREN);

		int doubleIdentifierIndex = TokenStreamUtils.findTokenTypeSequence(
				exceptionTokens, 0, ETokenType.IDENTIFIER,
				ETokenType.IDENTIFIER);
		if (doubleIdentifierIndex == TokenStreamUtils.NOT_FOUND) {
			return null;
		}
		return exceptionTokens.get(doubleIdentifierIndex + 1);
	}

	/**
	 * Returns the type name of an exception within a catch statement or null if
	 * none is found.
	 */
	public String getTypeNameFromCatchTokens(List<IToken> catchTokens) {
		List<IToken> exceptionTokens = TokenStreamUtils.tokensBetween(
				catchTokens, ETokenType.LPAREN, ETokenType.RPAREN);
		if (exceptionTokens.isEmpty()) {
			return null;
		}

		if (exceptionTokens.size() == 1) {
			return exceptionTokens.get(0).getText();
		}

		int typeEndIndex = TokenStreamUtils.findTokenTypeSequence(
				exceptionTokens, 0, ETokenType.IDENTIFIER,
				ETokenType.IDENTIFIER);
		if (typeEndIndex == TokenStreamUtils.NOT_FOUND) {
			typeEndIndex = exceptionTokens.size() - 1;
		}

		int typeStartIndex = getTypeNameStartIndex(exceptionTokens,
				typeEndIndex);
		return TokenStreamTextUtils.concatTokenTexts(
				exceptionTokens.subList(typeStartIndex, typeEndIndex + 1));
	}

	/**
	 * Returns all variable declaration tokens within a for-like statement.
	 * For-like statements are for loops and similar constructs like using and
	 * catch statements in Cs. The given end token type specifies the token
	 * where the variable declaration tokens end.
	 */
	public List<IToken> getVariableTokensFromForLikeTokens(
			List<IToken> forLikeTokens, ETokenType endToken) {
		int leftParenIndex = TokenStreamUtils.find(forLikeTokens, LPAREN);
		if (leftParenIndex == TokenStreamUtils.NOT_FOUND) {
			return CollectionUtils.emptyList();
		}

		int endIndex;
		if (endToken == RPAREN) {
			endIndex = TokenStreamUtils.findMatchingClosingToken(forLikeTokens,
					leftParenIndex + 1, LPAREN, RPAREN);
		} else {
			endIndex = TokenStreamUtils.find(forLikeTokens, endToken);
		}

		if (endIndex == TokenStreamUtils.NOT_FOUND) {
			return CollectionUtils.emptyList();
		}

		List<IToken> variableTokens = forLikeTokens.subList(leftParenIndex + 1,
				endIndex);
		return variableTokens;
	}

	/**
	 * Returns all token indices from the given tokens that are uses of the
	 * variable with the given name. The heuristic is to look for all
	 * occurrences of the variable name in the given tokens. An occurrence is
	 * considered as variable usage, if the following token is not in
	 * {@link #noVariableSuccessorTypes}. Those token types are set from the
	 * subclass for a concrete language. Internally a
	 * {@link CLikeVariableUseExtractor} is used.
	 * 
	 * @param isField
	 *            if the variable is a field
	 * @param isShadowed
	 *            if the variable is a field and shadowed by another local
	 *            variable
	 */
	public List<Integer> getVariableUsesFromTokens(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed) {
		return useExtractor.extractVariableUses(tokens, variableName, isField,
				isShadowed);
	}

	/**
	 * Returns all token indices from the given tokens that read the value of
	 * the variable with the given name.
	 * 
	 * @param isField
	 *            if the variable is a field
	 * @param isShadowed
	 *            if the variable is a field and shadowed by another local
	 *            variable
	 */
	public List<Integer> getVariableReadsFromTokens(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed) {
		return useExtractor.extractVariableReads(tokens, variableName, isField,
				isShadowed);
	}

	/**
	 * Returns all token indices from the given tokens that change the value of
	 * the variable with the given name.
	 * 
	 * @param isField
	 *            if the variable is a field
	 * @param isShadowed
	 *            if the variable is a field and shadowed by another local
	 *            variable
	 */
	public List<Integer> getVariableWritesFromTokens(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed) {
		return useExtractor.extractVariableWrites(tokens, variableName, isField,
				isShadowed);
	}

	/** Returns a variable use extractor for clike languages. */
	protected CLikeVariableUseExtractor getVariableUseExtractor() {
		return new CLikeVariableUseExtractor(DOT, noVariableSuccessorTypes);
	}

	/** Returns whether the given entity is an import. */
	public abstract boolean isImport(ShallowEntity entity);

	/** Returns the imported name of the given import shallow entity. */
	public abstract String getImportName(ShallowEntity entity);

	/** Returns whether the given method has a variable length argument list. */
	public abstract boolean hasVariableLengthArgumentList(ShallowEntity method);

	/**
	 * Returns a list of names that are imported by the given "import" entities.
	 * If the import is an aliasing import (in the case of C#) it is ignored.
	 */
	public List<String> getImportedNames(List<ShallowEntity> importEntities) {
		List<String> importedNames = new ArrayList<>();
		for (ShallowEntity usingEntity : importEntities) {
			String importName = getImportName(usingEntity);
			if (importName != null) {
				importedNames.add(importName);
			}
		}
		return importedNames;
	}
}
