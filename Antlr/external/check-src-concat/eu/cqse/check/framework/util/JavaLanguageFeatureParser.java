/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import static eu.cqse.check.framework.scanner.ETokenType.BOOLEAN;
import static eu.cqse.check.framework.scanner.ETokenType.BYTE;
import static eu.cqse.check.framework.scanner.ETokenType.CHAR;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE;
import static eu.cqse.check.framework.scanner.ETokenType.ELLIPSIS;
import static eu.cqse.check.framework.scanner.ETokenType.FLOAT;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.INT;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LONG;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SHORT;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.UnmodifiableList;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Language feature parser for Java.
 * 
 * @author $Author: poehlmann $
 * @version $Rev: 56515 $
 * @ConQAT.Rating GREEN Hash: 198ACF3DEB7199E1C9DAA532AEFDA0A7
 */
public class JavaLanguageFeatureParser extends CLikeLanguageFeatureParserBase {

	/** All token types that can be used to specify a type. */
	public static final EnumSet<ETokenType> TYPE_TOKENS = EnumSet.of(IDENTIFIER,
			BOOLEAN, CHAR, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE);

	/** All token types that can be used as identifier. */
	public static final EnumSet<ETokenType> VALID_IDENTIFIERS = EnumSet
			.of(IDENTIFIER);

	/** Constructor. */
	public JavaLanguageFeatureParser() {
		super(VALID_IDENTIFIERS, TYPE_TOKENS);
	}

	/**
	 * Returns the imported package name from a given "import" entity. Static
	 * imports are ignored.
	 */
	@Override
	public String getImportName(ShallowEntity entity) {
		CCSMAssert.isTrue(isImport(entity),
				"entity.getType() must be equal to EShallowEntityType.META and "
						+ "entity.getSubtype() must be equal to SubTypeNames.IMPORT or SubTypeNames.STATIC_IMPORT");

		List<IToken> tokens = entity.ownStartTokens();
		if (TokenStreamUtils.find(tokens,
				STATIC) != TokenStreamUtils.NOT_FOUND) {
			return null;
		}

		String importName = entity.getName();
		return StringUtils.removeLastPart(importName, '.');
	}

	/**
	 * Returns whether the given entity is an import (including static imports).
	 */
	@Override
	public boolean isImport(ShallowEntity entity) {
		return entity.getType().equals(EShallowEntityType.META)
				&& (entity.getSubtype().equals(SubTypeNames.IMPORT) || entity
						.getSubtype().equals(SubTypeNames.STATIC_IMPORT));
	}

	/** Returns whether the given entity is a for each loop. */
	public boolean isForEachLoop(ShallowEntity entity) {
		if (entity.getType().equals(EShallowEntityType.STATEMENT)
				&& entity.getSubtype().equals(SubTypeNames.FOR)) {
			return TokenStreamUtils.findFirstTopLevel(entity.ownStartTokens(),
					COLON, Arrays.asList(LBRACE),
					Arrays.asList(RBRACE)) != TokenStreamUtils.NOT_FOUND;
		}
		return false;
	}

	/** Returns whether the given entity is an annotation. */
	public boolean isAnnotation(ShallowEntity entity) {
		return entity.getType() == EShallowEntityType.META
				&& SubTypeNames.ANNOTATION.equals(entity.getSubtype());
	}

	/** Returns all annotation entities that annotate the given entity. */
	public List<ShallowEntity> getAnnotations(ShallowEntity entity) {
		ShallowEntity parent = entity.getParent();
		if (parent == null) {
			return CollectionUtils.emptyList();
		}

		List<ShallowEntity> annotations = new ArrayList<>();

		UnmodifiableList<ShallowEntity> entities = parent.getChildren();
		for (int i = entities.indexOf(entity) - 1; i >= 0; i--) {
			ShallowEntity previousEntity = entities.get(i);
			if (!isAnnotation(previousEntity)) {
				break;
			}
			annotations.add(previousEntity);
		}
		return annotations;
	}

	/** {@inheritDoc} */
	@Override
	public boolean hasVariableLengthArgumentList(ShallowEntity method) {
		List<IToken> tokens = method.ownStartTokens();
		return TokenStreamUtils.containsSequence(tokens, 0, tokens.size(),
				ELLIPSIS, IDENTIFIER, RPAREN);
	}

	/** {@inheritDoc} */
	@Override
	public ELanguage getLanguage() {
		return ELanguage.JAVA;
	}
}
