/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;


/**
 * Matches if the given pattern does not match. Does not advance the token
 * stream.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: D506E8C0DBDA8429BE6451ED318B0093
 */
public class NotFollowedByPattern extends ZeroLengthTokenPatternBase {

	/** The matcher that should not match at the current position. */
	private final TokenPatternBase matcher;

	/** Constructor. */
	public NotFollowedByPattern(TokenPatternBase matcher) {
		this.matcher = matcher;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean conditionApplies(TokenStream stream) {
		if (stream.isExhausted()) {
			return true;
		}
		return matcher.matches(stream) == null;
	}

}
