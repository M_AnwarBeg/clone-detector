/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import static eu.cqse.check.framework.scanner.ETokenType.CHANGING;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.EVENT;
import static eu.cqse.check.framework.scanner.ETokenType.EXPORTING;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IMPORTING;
import static eu.cqse.check.framework.scanner.ETokenType.LIKE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RAISING;
import static eu.cqse.check.framework.scanner.ETokenType.RETURNING;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.STRUCTURE;
import static eu.cqse.check.framework.scanner.ETokenType.TABLES;
import static eu.cqse.check.framework.scanner.ETokenType.TESTING;
import static eu.cqse.check.framework.scanner.ETokenType.TYPE;
import static eu.cqse.check.framework.scanner.ETokenType.USING;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMPre;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.Pair;
import org.conqat.lib.commons.logging.ILogger;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntityTraversalUtils;
import eu.cqse.check.framework.typetracker.TypedVariable;

/**
 * Language feature parser for ABAP.
 * 
 * @author $Author: dreier$
 * @version $Rev: 56775 $
 * @ConQAT.Rating GREEN Hash: B0529E483B1471DEEDCA86837DEF1731
 */
public class AbapLanguageFeatureParser implements ILanguageFeatureParser {

	/**
	 * EnumSet containing all token types which can appear as section headers in
	 * a parameter declaration.
	 */
	public static final EnumSet<ETokenType> PARAMETER_SECTION_TOKENS = EnumSet
			.of(USING, CHANGING, IMPORTING, EXPORTING, RETURNING, TABLES,
					TESTING, RAISING);

	/** {@inheritDoc} */
	@Override
	public ELanguage getLanguage() {
		return ELanguage.ABAP;
	}

	/**
	 * Gets the type name from the given list of tokens.
	 * 
	 * @param tokens
	 *            List of tokens. For example v1 TYPE t value(v2) LIKE c v TYPE
	 *            t v3.
	 * 
	 * @return Returns a tuple of the type name and the corresponding index of
	 *         the last type token. The index is ensured to be bigger or equal
	 *         to startIndex-1. If no further type is found, ("", startIndex-1)
	 *         is returned.
	 */
	public Pair<String, Integer> getNextTypeName(List<IToken> tokens,
			int startIndex, ILogger logger) {
		int typeKeywordIndex = TokenStreamUtils.findAny(tokens, startIndex,
				TYPE, LIKE, STRUCTURE);
		if (typeKeywordIndex == TokenStreamUtils.NOT_FOUND) {
			logger.debug("TYPE, LIKE or STRUCTURE expected but found "
					+ tokens.toString());
			// No type is given
			return new Pair<>(StringUtils.EMPTY_STRING, startIndex - 1);
		}

		// Find the beginning of the next type declaration and try to step
		// backwards in order to find the end of the current variable type. This
		// is done because you can put so many different expressions behind the
		// type keyword, that it is nearly impossible to get an exhaustive list.
		int endIndex = TokenStreamUtils.findAny(tokens, typeKeywordIndex + 1,
				TYPE, LIKE, STRUCTURE);
		if (endIndex == TokenStreamUtils.NOT_FOUND) {
			endIndex = TokenStreamUtils.find(tokens, typeKeywordIndex, DOT);
			if (endIndex == TokenStreamUtils.NOT_FOUND) {
				endIndex = tokens.size();
			}
		} else if (endIndex >= 3 && tokens.get(endIndex - 3).getType() == LPAREN
				&& tokens.get(endIndex - 1).getType() == RPAREN) {
			// If the next parameter is defined using a value(var2) or similar
			// notation we have to skip those tokens in order to get to the
			// type we are looking for.
			endIndex -= 4;
		} else {
			// Normally the TYPE keyword is preceded by a parameter name, which
			// we have to skip to reach the end of the previous parameter type.
			endIndex--;
		}

		/*
		 * Type declarations may be ambiguous. Look at the following: I TYPE
		 * STRUCTURE TYPE STRUCTURE. This can either mean we have 5 variables
		 * (I, Type, STRUCTURE, ...) or (I of type STRUCTURE, TYPE and
		 * STRUCTURE) or (I, TYPE and STRUCTURE of type STRUCTURE) and so on.
		 * And we did not yet manage to find a reliable pattern on how the
		 * compiler parses those declarations. Especially because leaving out
		 * the type of a parameter is not officially documented in any ABAP
		 * book, but it is used throughout the whole MR project we used to test
		 * it. And the discussion is still open on how to deal with it.
		 * Currently we agreed on ignoring cases that we are not able to parse
		 * correctly and providing a workaround for cases where the above method
		 * fails. The workaround is what follows.
		 */
		if (typeKeywordIndex < endIndex) {
			return new Pair<>(TokenStreamTextUtils.concatTokenTexts(
					tokens.subList(typeKeywordIndex + 1, endIndex),
					StringUtils.SPACE), endIndex - 1);
		}
		return new Pair<>(StringUtils.EMPTY_STRING,
				Math.max(startIndex, endIndex));
	}

	/**
	 * Gets type information from a list of tokens holding a method's parameter
	 * declaration part, which may either use the
	 * <code>FOR EVENT ... OF ... IMPORTING</code> or the normal sectioned
	 * parameter style.
	 */
	public List<TypedVariable> getTypeInfoForMethodParameters(
			ShallowEntity entity, List<IToken> tokens, ILogger logger) {
		if (TokenStreamUtils.startsWith(tokens, FOR, EVENT)) {
			return processMethodEventHandler(entity, tokens);
		}
		return processParameterList(entity, tokens, logger);
	}

	/**
	 * Gets type information from a list of tokens holding a method's parameter
	 * declaration part, which may contain USING, CHANGING, IMPORTING, EXPORTING
	 * and RETURNING sections.
	 * 
	 * For example <code>METHODS method IMPORTING v TYPE t value(v2) LIKE c v3
	 * CHANGING v4 TYPE t EXPORTING v5 TYPE t</code>
	 */
	private List<TypedVariable> processParameterList(ShallowEntity entity,
			List<IToken> tokens, ILogger logger) {
		List<TypedVariable> typeInfo = new ArrayList<>();
		List<Integer> positionsOfSectionTokens = TokenStreamUtils
				.findAll(tokens, PARAMETER_SECTION_TOKENS);

		// Tokens before first parameter section token are irrelevant because
		// they cannot declare parameters
		for (int i = 0; i < positionsOfSectionTokens.size(); i++) {
			int positionOfCurrentSectionToken = positionsOfSectionTokens.get(i);
			String sectionName = tokens.get(positionOfCurrentSectionToken)
					.getText();

			int endOfSection;
			if (i < positionsOfSectionTokens.size() - 1) {
				endOfSection = positionsOfSectionTokens.get(i + 1);
			} else {
				endOfSection = tokens.size();
			}

			List<IToken> section = tokens
					.subList(positionOfCurrentSectionToken + 1, endOfSection);
			processHeaderSection(entity, logger, typeInfo, section,
					sectionName);
		}

		return typeInfo;
	}

	/**
	 * Processes a list of parameters that have been defined within the same
	 * parameter section. The detected parameters are added to the given list of
	 * typeInfos.
	 */
	private void processHeaderSection(ShallowEntity containingEntity,
			ILogger logger, List<TypedVariable> typeInfo, List<IToken> section,
			String sectionName) {
		List<String> modifiers = new ArrayList<String>();
		modifiers.add(sectionName);

		int dotIndex = TokenStreamUtils.find(section, DOT);
		if (dotIndex != TokenStreamUtils.NOT_FOUND) {
			section = section.subList(0, dotIndex);
		}
		int currentIndex = 0;
		while (currentIndex < section.size()) {
			String variableName;
			// If the method parameter is marked as pass-by-value with
			// <code>value(varName) TYPE t</code>, skip the parenthesized part
			if (TokenStreamUtils.tokenTypesAt(section, currentIndex + 1, LPAREN,
					IDENTIFIER, RPAREN)) {
				variableName = section.get(currentIndex + 2).getText();
				currentIndex += 4;
			} else {
				variableName = section.get(currentIndex).getText();
				currentIndex++;
			}

			Pair<String, Integer> type = getNextTypeName(section, currentIndex,
					logger);
			typeInfo.add(new TypedVariable(variableName, type.getFirst(),
					modifiers, containingEntity));
			currentIndex = type.getSecond() + 1;
		}
	}

	/**
	 * Gets type information from a list of tokens holding a method's parameter
	 * declaration part in the form of
	 * <code>([CLASS-]METHODS handler )FOR EVENT evt OF class|intf IMPORTING e1 e2 ...</code>
	 * The parameters that are returned are e1, e2, ...
	 */
	private static List<TypedVariable> processMethodEventHandler(
			ShallowEntity entity, List<IToken> tokens) throws AssertionError {
		int importingIndex = TokenStreamUtils.find(tokens, IMPORTING);
		if (importingIndex == TokenStreamUtils.NOT_FOUND) {
			// Only happens in non standard conform code.
			return CollectionUtils.emptyList();
		}

		List<IToken> parameterTokens = tokens.subList(importingIndex + 1,
				tokens.size());
		return CollectionUtils.filterAndMap(parameterTokens,
				token -> token.getType() != DOT,
				token -> new TypedVariable(token.getText(),
						StringUtils.EMPTY_STRING, CollectionUtils.emptyList(),
						entity));
	}

	/**
	 * Returns the method declaration which belongs to the given method
	 * implementation entity. May return <code>null</code> if the declaration
	 * was not found, because the rootEntities given are incomplete.
	 */
	public List<IToken> getDeclarationForMethod(
			List<ShallowEntity> rootEntities, ShallowEntity methodEntity) {
		ShallowEntity classDeclaration = getClassDeclaration(rootEntities,
				methodEntity.getParent());
		if (classDeclaration == null) {
			return null;
		}
		List<List<IToken>> methodDeclarations = getMethodDeclarations(
				classDeclaration);
		return methodDeclarations.stream()
				.filter(declaration -> isMethodDeclarationFor(
						methodEntity.getName(), declaration))
				.findFirst().orElse(null);
	}

	/**
	 * Gets the corresponding class declaration for the given class
	 * implementation. This may return <code>null</code> if the rootEntities are
	 * incomplete.
	 */
	private static ShallowEntity getClassDeclaration(
			List<ShallowEntity> rootEntities,
			ShallowEntity classImplementation) {
		List<ShallowEntity> typeEntities = ShallowEntityTraversalUtils
				.listEntitiesOfType(rootEntities, EShallowEntityType.TYPE);
		for (ShallowEntity typeEntity : typeEntities) {
			if (typeEntity.getSubtype().equals(SubTypeNames.CLASS_DEFINITION)
					&& classImplementation.getName()
							.equals(typeEntity.getName())) {
				return typeEntity;
			}
		}
		return null;
	}

	/**
	 * Returns all method declarations which are defined inside the given class.
	 * Colon notations are already rolled out and removed in the returned list.
	 */
	private static List<List<IToken>> getMethodDeclarations(
			ShallowEntity classDeclaration) {
		return CollectionUtils.filterAndMap(
				classDeclaration.getChildrenOfType(EShallowEntityType.METHOD),
				method -> method.getSubtype()
						.equals(SubTypeNames.METHOD_DECLARATION),
				ShallowEntity::includedTokens);
	}

	/**
	 * Returns whether the method name of the a given method declaration matches
	 * the given methodName.
	 * 
	 * The declaration looks like
	 * <code>[CLASS-]METHODS name IMPORTING ...</code>
	 */
	private static boolean isMethodDeclarationFor(String methodName,
			List<IToken> declaration) {
		CCSMPre.isTrue(declaration.size() >= 2,
				"Method declaration is expected to always have 2 or more tokens.");
		return declaration.get(1).getText().equals(methodName);
	}
}
