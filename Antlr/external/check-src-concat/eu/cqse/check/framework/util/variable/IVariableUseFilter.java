/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.variable;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;

/**
 * Interface for filters that filter variable uses within token lists for some
 * criteria.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 56055 $
 * @ConQAT.Rating GREEN Hash: FB34008806DB7C9C9A2A9E7F3538C149
 */
public interface IVariableUseFilter {

	/** A variable use filter that accepts all uses. */
	public final static IVariableUseFilter ACCEPT_ALL = new IVariableUseFilter() {
		/** {@inheritDoc} */
		@Override
		public boolean isFiltered(List<IToken> tokens, int index,
				boolean isField) {
			return true;
		}
	};

	/**
	 * Returns whether the variable used at the given index is filtered. If the
	 * variable use is filtered it should be considered.
	 * 
	 * @param isField
	 *            true if the given variable use is a field use
	 */
	public boolean isFiltered(List<IToken> tokens, int index, boolean isField);
}
