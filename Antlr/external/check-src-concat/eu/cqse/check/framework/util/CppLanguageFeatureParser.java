/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import static eu.cqse.check.framework.scanner.ETokenType.CONST;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.VIRTUAL;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Language feature parser for C/C++. In the future this class may be a subclass
 * of {@link CLikeLanguageFeatureParserBase}.
 * 
 * @author $Author: kupka $
 * @version $Rev: 55962 $
 * @ConQAT.Rating YELLOW Hash: 381FE4A278EE180B78B5871BBD7DD444
 */
public class CppLanguageFeatureParser implements ILanguageFeatureParser {

	/** Returns whether the given shallow entity is virtual. */
	public boolean isVirtual(ShallowEntity entity) {
		return TokenStreamUtils.tokenStreamContains(entity.ownStartTokens(),
				VIRTUAL);
	}

	/** Returns whether the given entity is static and const. */
	public boolean isStaticConst(ShallowEntity entity) {
		return TokenStreamUtils.containsAll(entity.ownStartTokens(), STATIC,
				CONST);
	}

	/** Returns whether the given entity is a class or a struct. */
	public boolean isClassOrStruct(ShallowEntity entity) {
		return entity.getType() == EShallowEntityType.TYPE
				&& (SubTypeNames.CLASS.equals(entity.getSubtype())
						|| SubTypeNames.STRUCT.equals(entity.getSubtype()));
	}

	/** Returns whether the given entity is a top-level class or struct. */
	public boolean isTopLevelClassOrStruct(ShallowEntity entity) {
		ShallowEntity parent = entity.getParent();
		boolean isTopLevel = parent == null
				|| parent.getType().equals(EShallowEntityType.MODULE);
		return isTopLevel && isClassOrStruct(entity);
	}

	/** {@inheritDoc} */
	@Override
	public ELanguage getLanguage() {
		return ELanguage.CPP;
	}
}
