/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * Transforms a token stream according to a search and a replace pattern. Both
 * patterns can contain variables in the form of <code>$name</code>.
 *
 * <h1>Example</h1>
 * <ul>
 * <li>Language: Java
 * <li>Search Pattern: <code>assertNotNull($a)</code>
 * <li>Replacement Pattern: <code>$a != null</code>
 * <li>Input: <code>assertNotNull(foo)</code>
 * <li>Output: <code>foo != null</code>
 * </ul>
 * 
 * Additionally, variables can be postfixed with a number, e.g. <code>$a1</code>
 * to signal that the code matched to <code>$a1</code> should only be exactly 1
 * token in length.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56413 $
 * @ConQAT.Rating GREEN Hash: 09424AF25DD806D0B18B90F6C41AB67E
 */
public class TokenStreamTransformationPattern {

	/**
	 * Prefix of pattern variables.
	 */
	private static final String VARIABLE_PREFIX = "$";

	/** The search pattern transformed into matchers. */
	private final List<IMatcher> matchers = new ArrayList<>();

	/** The replacement pattern as tokens of the input language. */
	private final List<IToken> replacementPatternTokens;

	/** Returned if the matcher did not match. */
	public static final int NO_MATCH = -1;

	/**
	 * Constructor.
	 * 
	 * @throws CheckException
	 *             if the search pattern has invalid syntax.
	 */
	public TokenStreamTransformationPattern(String searchPatternString,
			String replacementPatternString, ELanguage language)
			throws CheckException {
		List<IToken> searchPatternTokens = ScannerUtils
				.getTokens(searchPatternString, language);
		createMatchers(searchPatternTokens);
		this.replacementPatternTokens = ScannerUtils
				.getTokens(replacementPatternString, language);
	}

	/**
	 * Creates matchers from the given search pattern tokens.
	 * 
	 * @throws CheckException
	 *             if the pattern has invalid syntax.
	 */
	private void createMatchers(List<IToken> searchPatternTokens)
			throws CheckException {
		for (int i = 0; i < searchPatternTokens.size(); i++) {
			IToken token = searchPatternTokens.get(i);
			String text = token.getText();
			ETokenType type = token.getType();
			if (type == IDENTIFIER && text.startsWith(VARIABLE_PREFIX)) {
				if (i + 1 < searchPatternTokens.size()) {
					ETokenType endTokenType = searchPatternTokens.get(i + 1)
							.getType();
					matchers.add(new VariableMatcher(text, endTokenType));
				} else {
					throw new CheckException(
							"The last token in the search pattern my not be a variable!");
				}
			} else {
				matchers.add(new TokenTypeMatcher(type, text));
			}
		}
	}

	/**
	 * Checks if the given tokens after the given offset contain more variables
	 * that should be replaced.
	 */
	public int containsMoreVariables(int offset, List<IToken> tokens) {
		for (int i = offset; i < tokens.size(); i++) {
			IToken token = tokens.get(i);
			if (token.getType() == ETokenType.IDENTIFIER
					&& token.getText().startsWith(VARIABLE_PREFIX)) {
				return i;
			}
		}
		return NO_MATCH;
	}

	/**
	 * Applies the pattern to the given tokens and returns the transformed token
	 * list. In case the pattern does not match, <code>null</code> is returned.
	 */
	public Result apply(List<IToken> tokens, int position) {
		Map<String, List<IToken>> variables = new HashMap<>();
		int matchedTokens = matchSearchPattern(tokens, position, variables);
		if (matchedTokens == NO_MATCH) {
			return null;
		}
		List<IToken> result = createResult(variables);
		return new Result(result, matchedTokens);
	}

	/**
	 * Applies the given patterns on the token stream and replaces all matches
	 * with the transformed tokens.
	 */
	public static List<IToken> applyPatterns(List<IToken> tokens,
			List<TokenStreamTransformationPattern> patterns) {
		List<IToken> transformedTokens = new ArrayList<>();
		int position = 0;
		while (position < tokens.size()) {
			Result result = applyPatterns(tokens, patterns, position);
			if (result == null) {
				transformedTokens.add(tokens.get(position));
				position += 1;
			} else {
				transformedTokens.addAll(result.getTransformedTokens());
				position += result.getMatchedTokens();
			}
		}
		return transformedTokens;
	}

	/**
	 * Applies the given patterns at the given position. The result of the first
	 * match is returned. If no pattern matches, <code>null</code> is returned.
	 */
	private static Result applyPatterns(List<IToken> tokens,
			List<TokenStreamTransformationPattern> patterns, int position) {
		for (TokenStreamTransformationPattern pattern : patterns) {
			Result result = pattern.apply(tokens, position);
			if (result != null) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Matches the matchers against the given tokens and returns the variable
	 * map created by the matchers. Returns <code>{@link #NO_MATCH}</code> if
	 * the matchers do not match the token stream.
	 */
	private int matchSearchPattern(List<IToken> tokens, int startPosition,
			Map<String, List<IToken>> variables) {
		int tokenPosition = startPosition;
		for (IMatcher matcher : matchers) {
			if (tokenPosition >= tokens.size()) {
				return NO_MATCH;
			}
			int nextPosition = matcher.apply(tokens, tokenPosition, variables);
			if (nextPosition == NO_MATCH) {
				return NO_MATCH;
			}
			CCSMAssert.isTrue(nextPosition > tokenPosition,
					"Matcher did not advance token stream.");
			tokenPosition = nextPosition;
		}
		return tokenPosition - startPosition;
	}

	/**
	 * Creates the result token list based on the given variable map.
	 */
	private List<IToken> createResult(Map<String, List<IToken>> variables) {
		List<IToken> result = new ArrayList<>();
		for (int i = 0; i < replacementPatternTokens.size(); i++) {
			IToken token = replacementPatternTokens.get(i);
			String text = token.getText();
			if (token.getType() == IDENTIFIER
					&& text.startsWith(VARIABLE_PREFIX)) {
				List<IToken> variableMatch = variables.get(text);
				CCSMAssert.isNotNull(variableMatch,
						"Variable " + text + " was not matched");
				result.addAll(variableMatch);
			} else {
				result.add(token);
			}
		}
		return result;
	}

	/** Result of successfully matching one pattern. */
	private static class Result {

		/** The transformed tokens. */
		private final List<IToken> transformedTokens;

		/** The number of tokens that matched in the input token stream. */
		private final int matchedTokens;

		/** Constructor. */
		public Result(List<IToken> transformedTokens, int matchedTokens) {
			this.transformedTokens = transformedTokens;
			this.matchedTokens = matchedTokens;
		}

		/**
		 * @see #matchedTokens
		 */
		public int getMatchedTokens() {
			return matchedTokens;
		}

		/**
		 * @see #transformedTokens
		 */
		public List<IToken> getTransformedTokens() {
			return transformedTokens;
		}

	}

	/** Matches part of the search pattern against the token stream. */
	private static interface IMatcher {

		/**
		 * Tries to match this matcher at the given position in the token
		 * stream. May modify the given variables map. Returns
		 * {@link TokenStreamTransformationPattern#NO_MATCH} if the matcher does
		 * not apply at this position. Otherwise returns the position where the
		 * next matcher should be applied.
		 */
		public int apply(List<IToken> tokens, int position,
				Map<String, List<IToken>> variables);

	}

	/**
	 * Matches if the token at the current position has a certain type and text.
	 */
	private static class TokenTypeMatcher implements IMatcher {

		/** The token type to match. */
		private final ETokenType type;

		/** The expected text. */
		private final String text;

		/** Constructor. */
		public TokenTypeMatcher(ETokenType type, String text) {
			this.type = type;
			this.text = text;
		}

		/** {@inheritDoc} */
		@Override
		public int apply(List<IToken> tokens, int position,
				Map<String, List<IToken>> variables) {
			IToken token = tokens.get(position);
			if (token.getType() == type && token.getText().equals(text)) {
				return position + 1;
			}
			return NO_MATCH;
		}

		/** {@inheritDoc} */
		@Override
		public String toString() {
			return "TokenTypeMatcher[type=" + type + ",text=" + text + "]";
		}

	}

	/**
	 * Matches a variable from the current position in the token stream to the
	 * first occurrence of the end token.
	 */
	private static class VariableMatcher implements IMatcher {

		/** The pattern to find out how many tokens long a match should be */
		private static final Pattern MATCH_LENGTH_PATTERN = Pattern
				.compile("\\$[a-zA-Z]+([0-9]+)");

		/** The variable to match. */
		private final String variableName;

		/**
		 * The token type that signals the end of the variable match.
		 */
		private final ETokenType endTokenType;

		/**
		 * The number of tokens that the variable should match. This may be null
		 * to express that the current variable does not contain a variable
		 * count. This means, it will greedily match as many tokens as possible.
		 */
		private Integer numberOfTokensToMatch = null;

		/** Constructor. */
		public VariableMatcher(String variableName, ETokenType endTokenType) {
			this.variableName = variableName;
			this.endTokenType = endTokenType;

			Matcher matcher = MATCH_LENGTH_PATTERN.matcher(variableName);
			if (matcher.matches()) {
				this.numberOfTokensToMatch = Integer.parseInt(matcher.group(1));
			}
		}

		/** {@inheritDoc} */
		@Override
		public int apply(List<IToken> tokens, int position,
				Map<String, List<IToken>> variables) {

			int endIndex = TokenStreamUtils.findFirstTopLevel(tokens, position,
					EnumSet.of(endTokenType), Arrays.asList(ETokenType.LPAREN),
					Arrays.asList(ETokenType.RPAREN));
			if (endIndex == TokenStreamUtils.NOT_FOUND
					|| position == endIndex) {
				return NO_MATCH;
			}

			if (numberOfTokensToMatch != null
					&& endIndex > position + numberOfTokensToMatch) {
				endIndex = position + numberOfTokensToMatch;
			}

			variables.put(variableName, tokens.subList(position, endIndex));
			return endIndex;
		}

		/** {@inheritDoc} */
		@Override
		public String toString() {
			return "VariableMatcher[variableName=" + variableName
					+ ",endTokenType=" + endTokenType + "]";
		}

	}

}
