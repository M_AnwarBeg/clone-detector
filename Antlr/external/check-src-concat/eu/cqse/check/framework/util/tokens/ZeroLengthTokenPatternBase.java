/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

/**
 * Base class for patterns that check a condition but do not advance the token
 * stream.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: A162DFD8F56615FA7BEFC02AFE788A04
 */
public abstract class ZeroLengthTokenPatternBase extends TokenPatternBase {

	/** {@inheritDoc} */
	@Override
	protected TokenPatternMatch matchesLocally(TokenStream stream) {
		int beforePosition = stream.getPosition();
		boolean success = conditionApplies(stream);
		stream.setPosition(beforePosition);
		if (success) {
			return createMatch(stream);
		}
		return null;
	}

	/**
	 * Returns <code>true</code> if this pattern applies at the current position
	 * of the stream. May modify the stream's position.
	 */
	protected abstract boolean conditionApplies(TokenStream stream);

}
