/*-----------------------------------------------------------------------+
 | org.conqat.engine.index.incubator
 |                                                                       |
   $Id: TokenPatternMatch.java 56288 2016-03-21 13:58:11Z kupka $            
 |                                                                       |
 | Copyright (c)  2009-2013 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.ListMap;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;

/**
 * A single match created by a {@link TokenPattern}. Keeps track of any groups
 * that have been matched by parts of the pattern. A group is identified by an
 * arbitrary integer. See also {@link TokenPattern#group(int)}.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: D0AF0A7DADA17C5D9A45D334D116E564
 */
public class TokenPatternMatch {

	/** The token stream against which the matches were constructed. */
	private final List<IToken> tokenStream;

	/** Maps from the group index to the ranges that are included in the group. */
	private final ListMap<Integer, Range> groups = new ListMap<>();

	/** Constructor. */
	public TokenPatternMatch(List<IToken> tokenStream) {
		this.tokenStream = tokenStream;
	}

	/**
	 * Appends the given tokens to the group with the given index if the given
	 * range contains at least one token.
	 */
	public void appendToGroup(Integer groupIndex, int inclusiveStartIndex,
			int exclusiveEndIndex) {
		if (inclusiveStartIndex < exclusiveEndIndex) {
			groups.add(groupIndex, new Range(inclusiveStartIndex,
					exclusiveEndIndex));
		}
	}

	/** Returns the text of the tokens in the given group. */
	// TODO (BH): Document if group numbers start at 0 or 1
	// TODO (FS) group numbers do not start anywhere. You assign them explicitly
	// when creating the pattern
	public List<String> groupTexts(int groupIndex) {
		return TokenStreamTextUtils.getTokenTexts(groupTokens(groupIndex));
	}

	/**
	 * Returns the indices into the token stream in the given group or an empty
	 * list.
	 */
	// TODO (BH): Document if group numbers start at 0 or 1
	// TODO (FS) see above comment
	public List<Integer> groupIndices(int groupIndex) {
		List<Range> ranges = groups.getCollection(groupIndex);
		if (ranges == null) {
			return CollectionUtils.emptyList();
		}
		List<Integer> indices = new ArrayList<>();
		for (Range range : ranges) {
			for (int i = range.inclusiveStartIndex; i < range.exclusiveEndIndex; i++) {
				indices.add(i);
			}
		}
		return indices;
	}

	/**
	 * Returns the tokens in the given group or an empty list.
	 */
	// TODO (BH): Document if group numbers start at 0 or 1
	// TODO (FS) see above comment
	public List<IToken> groupTokens(int groupIndex) {
		List<IToken> tokens = new ArrayList<IToken>();
		for (Integer index : groupIndices(groupIndex)) {
			tokens.add(tokenStream.get(index));
		}
		return tokens;
	}

	/**
	 * Returns the concatenated text of the tokens in the given group. If the
	 * group was not matched, returns the empty string.
	 */
	// TODO (BH): Document if group numbers start at 0 or 1
	// TODO (FS) see above comment
	public String groupString(int groupIndex) {
		return StringUtils.concat(groupTexts(groupIndex),
				StringUtils.EMPTY_STRING);
	}

	/**
	 * Returns <code>true</code> if the match contains tokens in the given
	 * group.
	 */
	// TODO (BH): Document if group numbers start at 0 or 1
	// TODO (FS) see above comment
	public boolean hasGroup(int groupIndex) {
		return groups.containsCollection(groupIndex);
	}

	/** Merges the group information from given match into this match. */
	public void mergeFrom(TokenPatternMatch other) {
		groups.addAll(other.groups);
	}

	/**
	 * Returns a list of all group strings in all matches of the group with the
	 * given index.
	 */
	public static List<String> getAllStrings(List<TokenPatternMatch> matches,
			int groupIndex) {
		List<String> strings = new ArrayList<String>();
		for (TokenPatternMatch match : matches) {
			strings.add(match.groupString(groupIndex));
		}
		return strings;
	}

	/**
	 * Returns a list of all group tokens in all matches of the group with the
	 * given index.
	 */
	public static List<IToken> getAllTokens(List<TokenPatternMatch> matches,
			int groupIndex) {
		List<IToken> tokens = new ArrayList<IToken>();
		for (TokenPatternMatch match : matches) {
			tokens.addAll(match.groupTokens(groupIndex));
		}
		return tokens;
	}

	/** A range of indices into a token stream. */
	private static class Range {

		/** The inclusive start index into the token stream. */
		private final int inclusiveStartIndex;

		/** The exclusive end index into the token stream. */
		private final int exclusiveEndIndex;

		/** Constructor. */
		public Range(int inclusiveStartIndex, int exclusiveEndIndex) {
			this.inclusiveStartIndex = inclusiveStartIndex;
			this.exclusiveEndIndex = exclusiveEndIndex;
		}

	}

}
