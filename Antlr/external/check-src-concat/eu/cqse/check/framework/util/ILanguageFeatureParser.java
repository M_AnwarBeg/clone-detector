/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import eu.cqse.check.framework.scanner.ELanguage;

/**
 * TODO (LH) This interface appears odd to me as it only has a method to get the
 * language. I'd expect actual parsing methods
 * 
 * Marker interface for language feature parsers
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57695 $
 * @ConQAT.Rating RED Hash: A1AA3A0E00974C572C00272398F4BE99
 */
public interface ILanguageFeatureParser {
	// general base interface for language feature parser

	/** Returns that language the feature parsers are implemented for. */
	public ELanguage getLanguage();
}
