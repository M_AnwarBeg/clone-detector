/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

import eu.cqse.check.framework.scanner.IToken;

/**
 * Base class for patterns that look only at the current token in the stream and
 * advance the stream by 1 if they matched.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: EE715D96F79DBAFB0CC312FF9AEA64CF
 */
public abstract class SingleTokenPatternBase extends TokenPatternBase {

	/** {@inheritDoc} */
	@Override
	protected TokenPatternMatch matchesLocally(TokenStream stream) {
		IToken token = stream.next();
		if (token == null || !matchesToken(token)) {
			return null;
		}
		return createMatch(stream);
	}

	/** Returns <code>true</code> if the pattern matches the given token. */
	protected abstract boolean matchesToken(IToken token);

}
