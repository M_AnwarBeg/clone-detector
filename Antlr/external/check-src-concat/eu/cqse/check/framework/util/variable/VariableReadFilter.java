/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.variable;

import static eu.cqse.check.framework.scanner.ETokenType.EQ;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;

/**
 * Filters for variable uses that read the variable's value.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 56056 $
 * @ConQAT.Rating GREEN Hash: D95BA3DBF532484A94D90FBD3934176D
 */
public class VariableReadFilter implements IVariableUseFilter {

	/** Singleton instance. */
	public static final VariableReadFilter INSTANCE = new VariableReadFilter();

	/** {@inheritDoc} */
	@Override
	public boolean isFiltered(List<IToken> tokens, int index, boolean isField) {
		return (index >= tokens.size() - 1)
				|| !tokens.get(index + 1).getType().equals(EQ);
	}
}
