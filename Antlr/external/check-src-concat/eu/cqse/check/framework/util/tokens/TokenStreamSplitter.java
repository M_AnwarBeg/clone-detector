/*-----------------------------------------------------------------------+
 | org.conqat.engine.index.incubator
 |                                                                       |
   $Id: TokenStreamSplitter.java 56473 2016-04-05 10:29:51Z kupka $            
 |                                                                       |
 | Copyright (c)  2009-2013 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.Token;

/**
 * Splits a token stream into multiple parts.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56473 $
 * @ConQAT.Rating GREEN Hash: BD0291C90148A379D258619C2AB6B15D
 */
public class TokenStreamSplitter {

	/** The sentinel token to use when splitting nested structures. */
	private static final IToken SENTINEL_TOKEN = new SentinelToken();

	/** The split parts. */
	private List<List<IToken>> tokenStreams = new ArrayList<List<IToken>>();

	/** Constructor. */
	public TokenStreamSplitter(List<IToken> tokens) {
		tokenStreams.add(tokens);
	}

	/** Returns the split streams. */
	public List<List<IToken>> getTokenStreams() {
		return tokenStreams;
	}

	/**
	 * Splits all streams at the given open and close tokens, e.g. parentheses.
	 * The part inside the parentheses will become one new stream and the rest
	 * of the original stream as well. In the outside part, the inside part is
	 * replaced by the {@link #SENTINEL_TOKEN} token.
	 * 
	 * For example: <code>a ( b ) c</code> would be split into
	 * <code>a ( sentinel ) c</code> and <code>b</code>.
	 * 
	 * Outer parts are guaranteed to be stored before inner parts in the
	 * {@link #tokenStreams} list.
	 */
	public void splitNested(ETokenType openToken, ETokenType closeToken) {
		List<List<IToken>> splitStreams = new ArrayList<List<IToken>>();
		for (List<IToken> tokens : tokenStreams) {
			splitStreams.addAll(split(tokens, openToken, closeToken));
		}
		tokenStreams = splitStreams;
	}

	/**
	 * Splits a single token list at the open/close tokens by performing a depth
	 * first traversal of the nesting tree created by these tokens.
	 */
	private List<List<IToken>> split(List<IToken> tokens, ETokenType openToken,
			ETokenType closeToken) {
		List<List<IToken>> result = new ArrayList<>();
		Stack<List<IToken>> splits = new Stack<>();
		splits.push(new ArrayList<IToken>());
		result.add(splits.peek());

		for (IToken token : tokens) {
			ETokenType tokenType = token.getType();
			if (tokenType == openToken) {
				splits.peek().add(SENTINEL_TOKEN);
				splits.push(new ArrayList<IToken>());
				result.add(splits.peek());
			} else if (tokenType == closeToken) {
				splits.pop();
				if (splits.isEmpty()) {
					// too many closes (error condition), so just return
					// original input
					return Collections.singletonList(tokens);
				}
			} else {
				splits.peek().add(token);
			}
		}

		return result;
	}

	/** The sentinel token. */
	private static class SentinelToken extends Token {

		/** Constructor. */
		public SentinelToken() {
			super(ETokenType.SENTINEL, 0, 0, "sentinel", "sentinel");
		}

		/** {@inheritDoc} */
		@Override
		public ELanguage getLanguage() {
			CCSMAssert.fail("Operation not supported for sentinel token");
			return null;
		}

		/** {@inheritDoc} */
		@Override
		public IToken newToken(ETokenType type, int offset, int lineNumber,
				String text, String originId) {
			CCSMAssert.fail("Operation not supported for sentinel token");
			return null;
		}

	}

}
