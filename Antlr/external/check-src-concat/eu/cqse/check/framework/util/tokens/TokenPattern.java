/*-----------------------------------------------------------------------+
 | org.conqat.engine.index.incubator
 |                                                                       |
   $Id: TokenPattern.java 56289 2016-03-21 14:16:58Z kupka $            
 |                                                                       |
 | Copyright (c)  2009-2013 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;

/**
 * A pattern that can be applied like a regular expression to a token stream.
 * 
 * This class is not fully redundant to TokenTypePattern for two reasons:
 * <ul>
 * <li>It allows matching nested structures, e.g. parentheses, with
 * {@link #skipNested(Object, Object, boolean)}
 * <li>It allows named capture groups, something which is not currently possible
 * with Java 7: http://bugs.java.com/bugdatabase/view_bug.do?bug_id=8013252
 * </ul>
 * 
 * TODO (BH): I think there is still lots of potential for improvement in this
 * class. Many of the matchers seem somewhat redundant. However, I would rather
 * wait for the next round of improvements, as then the code should be easier to
 * read. (Note to self: another full review ahead :))
 * 
 * @author $Author: kupka $
 * @version $Rev: 56289 $
 * @ConQAT.Rating YELLOW Hash: 4C07EB810D5D042F3553D0DB86EC081A
 */
public class TokenPattern {

	/**
	 * The parts of the pattern that are matched in sequence.
	 */
	private final List<TokenPatternBase> submatchers = new ArrayList<>();

	/**
	 * Matches the pattern at all positions in the given token stream and
	 * returns the found matches. This also finds overlapping matches.
	 */
	public List<TokenPatternMatch> findAll(List<IToken> tokens) {
		List<TokenPatternMatch> matches = new ArrayList<TokenPatternMatch>();
		// TODO (BH): This looks extremely expensive (quadratic?), as you check
		// at every position. Can't you skip ahead if a pattern matched? Or do
		// you also intend to find overlapping matches? If so, please document.
		// TODO (FS) I'm not sure if we need the overlapping. I documented it
		// for now, but I'd have to take a closer look at the actual patterns to
		// be sure we can make it non-overlapping
		TokenStream stream = new TokenStream(tokens, 0);
		for (int i = 0; i < tokens.size(); i++) {
			stream.setPosition(i);
			TokenPatternMatch match = matchesLocally(stream);
			if (match != null) {
				matches.add(match);
			}
		}
		return matches;
	}

	/**
	 * Matches the pattern at all positions in the given token stream and
	 * returns the first found match or <code>null</code> if no match happened.
	 */
	public TokenPatternMatch findFirstMatch(List<IToken> tokens) {
		for (int i = 0; i < tokens.size(); i++) {
			TokenStream stream = new TokenStream(tokens, i);
			TokenPatternMatch match = matchesLocally(stream);
			if (match != null) {
				return match;
			}
		}
		return null;
	}

	/**
	 * Returns <code>true</code> if the pattern matches at least once in the
	 * given token stream.
	 */
	public boolean matchesAnywhere(List<IToken> tokens) {
		return findFirstMatch(tokens) != null;
	}

	/**
	 * Matches the pattern once at the current position of the given token
	 * stream and records the results in the given match.
	 * 
	 * @return <code>true</code> if the pattern matched or <code>false</code> if
	 *         the given match should be discarded.
	 */
	private TokenPatternMatch matchesLocally(TokenStream stream) {
		return new SequencePattern(
				CollectionUtils.toArray(submatchers, TokenPatternBase.class))
						.matchesLocally(stream);
		// TODO (BH): Actually, I do not understand why the submatchers
		// have to handle the "match" as well. It seems that in case of
		// groups, you just handle update of the match here. Why not
		// also the the unnamed match groups? Then the submatchers would
		// be much simpler. Just must return a boolean (match, nomatch).
		// TODO (FS) we need to pass the match down to collect the group
		// info (now moved to TokenPatternBase). I'd like to remove it from
		// the interface, but then I'd have to store it in an instance field
		// and the patterns would no longer be thread safe (stored as static
		// constants now most of the time). So TokenPatternMatch is sort of
		// the analogy to Java's Matcher class
	}

	/**
	 * Specifies that the last added subpattern should be appended to the group
	 * with the given index. The group index may be any integer number. Group
	 * indices are not required to be ordered or continuous.
	 */
	public TokenPattern group(int groupIndex) {
		CCSMAssert.isFalse(submatchers.isEmpty(),
				"You must specify a pattern part before you can assign a group to it.");
		submatchers.get(submatchers.size() - 1).setGroupIndex(groupIndex);
		return this;
	}

	/**
	 * Adds a new pattern part that consumes anything until it encounters one of
	 * the given match terms. The stop token matching one of the given terms is
	 * also consumed.
	 * 
	 * A group applied to this pattern will only contain the stop token.
	 * 
	 * @param matchTerms
	 *            a list of match terms which must match in order. These may be
	 *            instances of {@link ETokenType}, {@link ETokenClass}, or sets
	 *            of them, as well as {@link TokenPattern}s.
	 */
	public TokenPattern skipTo(Object... matchTerms) {
		TokenPatternBase[] matchers = convertMatchTerms(matchTerms);
		submatchers.add(new SkipPattern(matchers));
		submatchers.add(new AlternativePattern(matchers));
		return this;
	}

	/**
	 * Adds a new pattern part that consumes nested structures.
	 * 
	 * @param openTerm
	 *            a match term that identifies the opening braces. May be an
	 *            instance of {@link ETokenType}, {@link ETokenClass}, or a set
	 *            of them, as well as an {@link TokenPattern}.
	 * @param closeTerm
	 *            a match term that identifies the closing braces. May be an
	 *            instance of {@link ETokenType}, {@link ETokenClass}, or a set
	 *            of them, as well as an {@link TokenPattern}.
	 * @param optional
	 *            if this is <code>false</code>, the nested structure must be
	 *            present or the pattern will not match. If this is
	 *            <code>true</code>, this pattern may return an empty match.
	 */
	public TokenPattern skipNested(Object openTerm, Object closeTerm,
			boolean optional) {
		TokenPatternBase openMatcher = convertMatchTerm(openTerm);
		TokenPatternBase closeMatcher = convertMatchTerm(closeTerm);
		submatchers.add(
				new SkipNestedPattern(openMatcher, closeMatcher, optional));
		return this;
	}

	/**
	 * Adds a new pattern part that matches the given terms zero or more times
	 * in the given order.
	 * 
	 * @param matchTerms
	 *            a list of match terms which must match in order. These may be
	 *            instances of {@link ETokenType}, {@link ETokenClass}, or sets
	 *            of them, as well as {@link TokenPattern}s.
	 */
	public TokenPattern repeated(Object... matchTerms) {
		TokenPatternBase[] matchers = convertMatchTerms(matchTerms);
		submatchers.add(new RepeatedPattern(matchers));
		return this;
	}

	/**
	 * Adds a new pattern part that matches the given terms exactly once in the
	 * given order.
	 * 
	 * @param matchTerms
	 *            a list of match terms which must match in order. These may be
	 *            instances of {@link ETokenType}, {@link ETokenClass}, or sets
	 *            of them, as well as {@link TokenPattern}s.
	 */
	public TokenPattern sequence(Object... matchTerms) {
		TokenPatternBase[] matchers = convertMatchTerms(matchTerms);
		submatchers.add(new SequencePattern(matchers));
		return this;
	}

	/**
	 * Adds a new pattern part that matches the end of the token stream (i.e.
	 * there are no more tokens to match).
	 */
	public TokenPattern endOfStream() {
		submatchers.add(new ZeroLengthTokenPatternBase() {

			@Override
			public boolean conditionApplies(TokenStream stream) {
				return stream.isExhausted();
			}
		});
		return this;
	}

	/**
	 * Adds a new pattern part that matches the beginning of the token stream
	 * (i.e. no tokens have been consumed so far).
	 */
	public TokenPattern beginningOfStream() {
		submatchers.add(new ZeroLengthTokenPatternBase() {

			@Override
			public boolean conditionApplies(TokenStream stream) {
				return stream.isAtBeginning();
			}
		});
		return this;
	}

	/**
	 * Adds a new pattern part that matches anything but the beginning of the
	 * token stream (i.e. at least one token has been consumed so far).
	 */
	public TokenPattern notAtBeginningOfStream() {
		submatchers.add(new ZeroLengthTokenPatternBase() {

			@Override
			public boolean conditionApplies(TokenStream stream) {
				return !stream.isAtBeginning();
			}
		});
		return this;
	}

	/**
	 * Adds a new pattern part that matches if the last matched token is not
	 * followed by a token that matches the given term. This pattern part does
	 * not consume any tokens.
	 * 
	 * @param matchTerm
	 *            May be an instance of {@link ETokenType}, {@link ETokenClass},
	 *            or a set of them, as well as a {@link TokenPattern}.
	 */
	public TokenPattern notFollowedBy(Object matchTerm) {
		TokenPatternBase matcher = convertMatchTerm(matchTerm);
		submatchers.add(new NotFollowedByPattern(matcher));
		return this;
	}

	/**
	 * Adds a new pattern part that matches if the last matched token is not
	 * preceded by a token that matches the given term. This pattern part does
	 * not consume any tokens.
	 * 
	 * NOTE: passing a {@link TokenPattern} to this function will not result in
	 * an exception, but it will not yield the intuitive result. The given
	 * pattern will be matched against the token stream, starting with the token
	 * preceding the current token. It is therefore discouraged to pass a
	 * {@link TokenPattern} to this function as it makes understanding the
	 * pattern difficult.
	 * 
	 * @param matchTerm
	 *            May be an instance of {@link ETokenType}, {@link ETokenClass},
	 *            or a set of them, but usually not a {@link TokenPattern}.
	 */
	public TokenPattern notPrecededBy(Object matchTerm) {
		TokenPatternBase matcher = convertMatchTerm(matchTerm);
		submatchers.add(new NotPrecededByPattern(matcher));
		return this;
	}

	/**
	 * Adds a new pattern part that tries the given match terms one after the
	 * other and stops as soon as one of them matches.
	 * 
	 * @param matchTerms
	 *            a list of match terms which must match in order. These may be
	 *            instances of {@link ETokenType}, {@link ETokenClass}, or sets
	 *            of them, as well as {@link TokenPattern}s.
	 */
	public TokenPattern alternative(Object... matchTerms) {
		TokenPatternBase[] matchers = convertMatchTerms(matchTerms);
		submatchers.add(new AlternativePattern(matchers));
		return this;
	}

	/**
	 * Adds a new pattern part that matches the given terms zero times or once
	 * in the given order.
	 * 
	 * @param matchTerms
	 *            a list of match terms which must match in order. These may be
	 *            instances of {@link ETokenType}, {@link ETokenClass}, or sets
	 *            of them, as well as {@link TokenPattern}s.
	 */
	public TokenPattern optional(Object... matchTerms) {
		TokenPatternBase[] matchers = convertMatchTerms(matchTerms);
		submatchers.add(new OptionalPattern(matchers));
		return this;
	}

	/** Converts the given match terms to {@link TokenPatternBase}s. */
	private TokenPatternBase[] convertMatchTerms(Object[] matchTerms) {
		CCSMAssert.isFalse(matchTerms.length == 0,
				"You must provide at least one match term.");
		TokenPatternBase[] matchers = new TokenPatternBase[matchTerms.length];
		for (int i = 0; i < matchTerms.length; ++i) {
			matchers[i] = convertMatchTerm(matchTerms[i]);
		}
		return matchers;
	}

	/** Converts a match term to a matcher. */
	private TokenPatternBase convertMatchTerm(final Object matchTerm) {
		if (matchTerm instanceof TokenPattern) {
			return new TokenPatternBase() {

				@Override
				public TokenPatternMatch matchesLocally(TokenStream stream) {
					TokenPattern pattern = (TokenPattern) matchTerm;
					int beforePosition = stream.getPosition();
					TokenPatternMatch match = pattern.matchesLocally(stream);
					if (match == null) {
						stream.setPosition(beforePosition);
						return null;
					}
					return match;
				}
			};
		}

		if (matchTerm instanceof ETokenType) {
			return new SingleTokenPatternBase() {
				@Override
				public boolean matchesToken(IToken token) {
					return token.getType() == (ETokenType) matchTerm;
				}
			};
		}

		if (matchTerm instanceof ETokenClass) {
			return new SingleTokenPatternBase() {
				@Override
				public boolean matchesToken(IToken token) {
					return token.getType()
							.getTokenClass() == (ETokenClass) matchTerm;
				}
			};
		}

		if (matchTerm instanceof Set<?>) {
			final Set<?> set = (Set<?>) matchTerm;
			return new SingleTokenPatternBase() {
				@Override
				public boolean matchesToken(IToken token) {
					ETokenType type = token.getType();
					return set.contains(type)
							|| set.contains(type.getTokenClass());
				}
			};
		}

		throw new AssertionError(
				"Unsupported match term of type " + matchTerm.getClass());
	}

}
