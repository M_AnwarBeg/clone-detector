/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

/**
 * Inverse repeated pattern. Matches as many tokens as possible that are not
 * matched by any of the given subpatterns.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: 09F217796EC083A9ECCAB5E055A95CA7
 */
public class SkipPattern extends TokenPatternBase {

	/** The patterns that must not match. */
	private final TokenPatternBase[] matchers;

	/** Constructor. */
	public SkipPattern(TokenPatternBase[] matchers) {
		this.matchers = matchers;
	}

	/** {@inheritDoc} */
	@Override
	public TokenPatternMatch matchesLocally(TokenStream stream) {
		while (!stream.isExhausted()) {
			for (TokenPatternBase matcher : matchers) {
				int beforeMatch = stream.getPosition();
				TokenPatternMatch match = matcher.matches(stream);
				stream.setPosition(beforeMatch);
				if (match != null) {
					return createMatch(stream);
				}
			}

			stream.next();
		}
		return createMatch(stream);
	}

}
