/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

/**
 * Tries to match the given subpatterns in order, as many times as possible
 * (including 0 times).
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: B73A2B06C9FE001A5CE34EB0CE274AD6
 */
public class RepeatedPattern extends TokenPatternBase {

	/** The patterns so repeatedly match in order. */
	private final TokenPatternBase[] matchers;

	/** Constructor */
	public RepeatedPattern(TokenPatternBase[] matchers) {
		this.matchers = matchers;
	}

	/** {@inheritDoc} */
	@Override
	public TokenPatternMatch matchesLocally(TokenStream stream) {
		int i = 0;
		int lastFullMatch = stream.getPosition();
		TokenPatternMatch parentMatch = createMatch(stream);
		while (true) {
			int matcherIndex = i % matchers.length;
			if (i != 0 && matcherIndex == 0) {
				lastFullMatch = stream.getPosition();
			}

			TokenPatternMatch match = matchers[matcherIndex].matches(stream);
			if (match == null) {
				stream.setPosition(lastFullMatch);
				return parentMatch;
			}
			parentMatch.mergeFrom(match);

			i += 1;
		}
	}

}
