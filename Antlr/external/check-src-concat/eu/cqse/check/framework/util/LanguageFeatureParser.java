/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.scanner.ELanguage;

/**
 * Global instantiation of all language feature parsers.
 *
 * @author $Author: heinemann $
 * @version $Rev: 57692 $
 * @ConQAT.Rating RED Hash: 9CD3584B9BF7315A6400B6E142A35D50
 */
public class LanguageFeatureParser {

	// TODO (MP) Actually I'd use a map ELanguage -> FeatureParser. You could
	// also exploit ILanguageFeatureParser.getLanguage() for constructing
	// the map.

	/** Language feature parser for ABAP. */
	public static final AbapLanguageFeatureParser ABAP = new AbapLanguageFeatureParser();

	/** Language feature parser for C/C++. */
	public static final CppLanguageFeatureParser CPP = new CppLanguageFeatureParser();

	/** Language feature parser for Cs. */
	public static final CsLanguageFeatureParser CS = new CsLanguageFeatureParser();

	/** Language feature parser for Java. */
	public static final JavaLanguageFeatureParser JAVA = new JavaLanguageFeatureParser();

	/** Language feature parser for Matlab. */
	public static final MatlabLanguageFeatureParser MATLAB = new MatlabLanguageFeatureParser();

	/** Returns the language feature parser for the given language. */
	public static ILanguageFeatureParser getFeatureParser(ELanguage language) {
		// Note that the case/switch is based on the language and returns the
		// matching feature parser. Naming chosen for better usability of the
		// static
		// members.
		switch (language) {
		case CS:
			return CS;
		case JAVA:
			return JAVA;
		case ABAP:
			return ABAP;
		case CPP:
			return CPP;
		case MATLAB:
			return MATLAB;
		default:
			CCSMAssert.fail("There is no feature parser for"
					+ language.getReadableName());
		}
		return null;
	}
}
