/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Language feature parser for Matlab.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57696 $
 * @ConQAT.Rating GREEN Hash: C60BC7F805807CE8C78DCBA4AF36323F
 */
public class MatlabLanguageFeatureParser implements ILanguageFeatureParser {

	/**
	 * Extracts a list of output parameter tokens from the given method entity.
	 */
	public List<IToken> extractOutputParameterTokens(ShallowEntity method) {
		List<IToken> outputParameterTokens = getOutputParameterTokens(method);
		List<IToken> result = new ArrayList<>();
		for (int i = 0; i < outputParameterTokens.size(); i += 2) {
			result.add(outputParameterTokens.get(i));
		}
		return result;
	}

	/**
	 * Returns the the tokens that specify output parameters from the given
	 * method (excluding "[" and "]")
	 */
	public List<IToken> getOutputParameterTokens(ShallowEntity method) {
		List<IToken> tokens = method.ownStartTokens();

		int equalIndex = TokenStreamUtils.find(tokens, EQ);
		if (equalIndex == -1) {
			return CollectionUtils.emptyList();
		}

		int startIndex = 1;
		int endIndex = equalIndex;
		if (tokens.get(1).getType() == LBRACK
				&& tokens.get(equalIndex - 1).getType() == RBRACK) {
			startIndex++;
			endIndex--;
		}

		return tokens.subList(startIndex, endIndex);
	}

	/** {@inheritDoc} */
	@Override
	public ELanguage getLanguage() {
		return ELanguage.MATLAB;
	}
}
