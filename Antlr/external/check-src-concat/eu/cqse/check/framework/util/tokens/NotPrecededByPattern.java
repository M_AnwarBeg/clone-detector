/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

/**
 * Matches, if the given subpattern does not match at the position before the
 * current position in the token stream.
 * 
 * NOTE: passing a {@link TokenPattern} as the subpattern to this class will not
 * result in an exception, but it will not yield the intuitive result. The given
 * pattern will be matched against the token stream, starting with the token
 * preceding the current token. It is therefore discouraged to pass a
 * {@link TokenPattern} to this function as it makes understanding the pattern
 * difficult.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: 359A3598475B0C378215982542C60229
 */
public class NotPrecededByPattern extends ZeroLengthTokenPatternBase {

	/**
	 * The matcher that should not match at the position before the current
	 * position.
	 */
	private final TokenPatternBase matcher;

	/** Constructor. */
	public NotPrecededByPattern(TokenPatternBase matcher) {
		this.matcher = matcher;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean conditionApplies(TokenStream stream) {
		if (stream.moveBack() == null) {
			return true;
		}
		return matcher.matches(stream) == null;
	}

}
