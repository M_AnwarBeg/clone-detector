/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import java.util.EnumSet;

import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Utility methods for {@link IToken}s.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 55896 $
 * @ConQAT.Rating GREEN Hash: A64B93183ED6AFAB83A22B13E486A78B
 */
public class TokenUtils {

	/**
	 * Set of token types that always have to be treated case-sensitive, e.g.
	 * String literals.
	 */
	public static final EnumSet<ETokenType> ALWAYS_CASE_SENSITIVE_TOKEN_TYPES = EnumSet
			.of(ETokenType.STRING_LITERAL, ETokenType.CHARACTER_LITERAL,
					ETokenType.UNTERMINATED_STRING_LITERAL,
					ETokenType.UNTERMINATED_CHARACTER_LITERAL);

	/**
	 * Estimates the zero-based end line of the specified token by looking where
	 * the provided next token begins. Note that this cannot check, if the
	 * provided next token is really the next token, nor if there are blank
	 * lines between the two tokens.
	 * <p>
	 * This method is more efficient than
	 * {@link #calculateEndLineByCountingLines(IToken)}, but not as precise.
	 * 
	 * @param token
	 *            the token whose end line should be calculated.
	 * @param nextToken
	 *            the lookahead token.
	 * @return the end line of the specified first token.
	 */
	public static int estimateEndLineByLookahead(IToken token,
			IToken nextToken) {
		int tokenEndLine = nextToken.getLineNumber() - 1;
		return Math.max(tokenEndLine, token.getLineNumber());
	}

	/**
	 * Calculates the zero-based end line of the specified token by counting the
	 * lines of the text contained within.
	 * <p>
	 * This method is less efficient than
	 * {@link #estimateEndLineByLookahead(IToken, IToken)}, but always yields
	 * the correct result.
	 * 
	 * @param token
	 *            the token whose end line should be calculated.
	 * @return the end line of the specified token.
	 */
	public static int calculateEndLineByCountingLines(IToken token) {
		int tokenEndLine = token.getLineNumber()
				+ StringUtils.countLines(token.getText()) - 1;
		return Math.max(tokenEndLine, token.getLineNumber());
	}
}
