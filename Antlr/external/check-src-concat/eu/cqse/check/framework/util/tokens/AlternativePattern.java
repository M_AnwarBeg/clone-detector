/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

/**
 * Matches if any of the given sub patterns matches.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: 1C05F1AE535B9B8156BE8FDA82E5E8E1
 */
public class AlternativePattern extends TokenPatternBase {

	/** The subpatterns that are tried in order. */
	private final TokenPatternBase[] matchers;

	/** Constructor. */
	public AlternativePattern(TokenPatternBase[] matchers) {
		this.matchers = matchers;
	}

	/** {@inheritDoc} */
	@Override
	public TokenPatternMatch matchesLocally(TokenStream stream) {
		int beforeAlternative = stream.getPosition();
		for (int i = 0; i < matchers.length; i++) {
			TokenPatternMatch match = matchers[i].matches(stream);
			if (match != null) {
				return match;
			}
			stream.setPosition(beforeAlternative);
		}
		return null;
	}

}
