/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.variable;

import static eu.cqse.check.framework.scanner.ETokenType.ANDEQ;
import static eu.cqse.check.framework.scanner.ETokenType.DIVEQ;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.LSHIFTEQ;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSEQ;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSMINUS;
import static eu.cqse.check.framework.scanner.ETokenType.MODEQ;
import static eu.cqse.check.framework.scanner.ETokenType.MULTEQ;
import static eu.cqse.check.framework.scanner.ETokenType.OREQ;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSEQ;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSPLUS;
import static eu.cqse.check.framework.scanner.ETokenType.RSHIFTEQ;
import static eu.cqse.check.framework.scanner.ETokenType.URSHIFTEQ;
import static eu.cqse.check.framework.scanner.ETokenType.XOREQ;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Filters for variable uses that write the variable's value.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 56236 $
 * @ConQAT.Rating GREEN Hash: EF1E2B5097D0F90E922642FFF513F438
 */
public class VariableWriteFilter implements IVariableUseFilter {

	/** Singleton instance. */
	public static final VariableWriteFilter INSTANCE = new VariableWriteFilter();

	/** Predecessor token types that indicate a write to a variable. */
	private static final EnumSet<ETokenType> PREDECESSOR_TOKEN_TYPES = EnumSet
			.of(PLUSPLUS, MINUSMINUS);

	/** Successor token types that indicate a write to a variable. */
	private static final EnumSet<ETokenType> SUCCESSOR_TOKEN_TYPES = EnumSet.of(
			EQ, PLUSPLUS, MINUSMINUS, PLUSEQ, MINUSEQ, MULTEQ, DIVEQ, ANDEQ,
			OREQ, XOREQ, MODEQ, LSHIFTEQ, RSHIFTEQ, URSHIFTEQ);

	/** {@inheritDoc} */
	@Override
	public boolean isFiltered(List<IToken> tokens, int index, boolean isField) {
		return hasSuffixOperator(tokens, index)
				|| hasPrefixOperator(tokens, index)
				|| hasPrefixOperatorWithFieldAccess(tokens, index, isField);
	}

	/** Returns whether the given variable use has a suffix operator. */
	private static boolean hasSuffixOperator(List<IToken> tokens, int index) {
		return index < tokens.size() - 1 && SUCCESSOR_TOKEN_TYPES
				.contains(tokens.get(index + 1).getType());
	}

	/** Returns whether the given variable use has a prefix operator. */
	private static boolean hasPrefixOperator(List<IToken> tokens, int index) {
		return index > 0 && PREDECESSOR_TOKEN_TYPES
				.contains(tokens.get(index - 1).getType());

	}

	/**
	 * Returns whether the given variable is a field that is accessed via field
	 * access operator and has a prefix operator.
	 */
	private static boolean hasPrefixOperatorWithFieldAccess(List<IToken> tokens,
			int index, boolean isField) {
		return isField && index > 2 && PREDECESSOR_TOKEN_TYPES
				.contains(tokens.get(index - 3).getType());
	}
}
