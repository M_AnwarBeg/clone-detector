/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

/**
 * Base class for token patterns. Handles resetting the token stream's position
 * if the pattern did not match and appending the patterns match to the match
 * group, if a group index was set for this pattern.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56288 $
 * @ConQAT.Rating YELLOW Hash: 51A19E6E0723DC973CE3365C8C46BA51
 */
public abstract class TokenPatternBase {

	/**
	 * The group index of this pattern or <code>null</code> if this pattern's
	 * match should not be appended to a group.
	 */
	private Integer groupIndex = null;

	/** @see #groupIndex */
	public Integer getGroupIndex() {
		return groupIndex;
	}

	/** @see #groupIndex */
	public void setGroupIndex(Integer groupIndex) {
		this.groupIndex = groupIndex;
	}

	/**
	 * Tries to match this pattern at the current position of the given stream.
	 * Returns a match object if successful, <code>null</code> otherwise. If the
	 * match was unsuccessful, the streams position is not modified.
	 */
	/* package */TokenPatternMatch matches(TokenStream stream) {
		int beforePosition = stream.getPosition();
		TokenPatternMatch match = matchesLocally(stream);
		if (match == null) {
			stream.setPosition(beforePosition);
		} else if (groupIndex != null) {
			match.appendToGroup(groupIndex, beforePosition,
					stream.getPosition());
		}
		return match;
	}

	/** Utility method to create an empty match object. */
	protected TokenPatternMatch createMatch(TokenStream stream) {
		return new TokenPatternMatch(stream.getTokens());
	}

	/**
	 * Tries to match this pattern at the current position of the given stream.
	 * Returns a match object if successful, <code>null</code> otherwise. May
	 * modify the stream's position.
	 */
	protected abstract TokenPatternMatch matchesLocally(TokenStream stream);

}
