/*-----------------------------------------------------------------------+
 | org.conqat.engine.index.incubator
 |                                                                       |
   $Id: TokenStreamParser.java 56473 2016-04-05 10:29:51Z kupka $            
 |                                                                       |
 | Copyright (c)  2009-2013 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.framework.util.tokens;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMPre;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Allows parsing a token stream from left to right. Does not support
 * backtracking, i.e. it is only possible to advance the parser forward, not
 * backward.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56473 $
 * @ConQAT.Rating GREEN Hash: C86E71F56A9445B26AD868562FAAA10C
 */
public class TokenStreamParser {

	/** The token stream. */
	private final List<IToken> tokens;

	/** The current position of the parser. */
	private int currentToken = 0;

	/**
	 * Constructor.
	 * 
	 * @param tokens
	 *            The tokens to parse.
	 * @param filteredTokens
	 *            A set of token types. All tokens of these types are filtered
	 *            out before parsing, i.e. they are simply skipped.
	 */
	public TokenStreamParser(List<IToken> tokens, Set<ETokenType> filteredTokens) {
		this(filter(tokens, filteredTokens));
	}

	/** Constructor. */
	public TokenStreamParser(List<IToken> tokens) {
		this.tokens = tokens;
	}

	/**
	 * Returns a view of the given token list that does not contain any tokens
	 * of the given types.
	 */
	private static List<IToken> filter(List<IToken> tokens,
			Set<ETokenType> filteredTokens) {
		List<IToken> filtered = new ArrayList<IToken>();
		for (IToken token : tokens) {
			if (!filteredTokens.contains(token.getType())) {
				filtered.add(token);
			}
		}
		return filtered;
	}

	/**
	 * Consumes all sequential tokens of the given types and returns their
	 * texts.
	 */
	public List<String> consumeAnyOf(Set<ETokenType> types) {
		List<String> texts = new ArrayList<String>();
		while (isAnyOf(types)) {
			texts.add(currentText());
			currentToken++;
		}
		return texts;
	}

	/**
	 * Consumes one token of the given type and returns its text or
	 * <code>null</code> if the current token is not of any of those types. This
	 * method only advances the parser if the current token has one of the given
	 * types.
	 */
	public String consumeOneOrZeroOf(Set<ETokenType> types) {
		if (isAnyOf(types)) {
			String text = currentText();
			currentToken++;
			return text;
		}
		return null;
	}

	/**
	 * Consumes all sequential tokens that are not of the given types and
	 * returns their texts.
	 */
	public List<String> consumeAnyExcept(Set<ETokenType> types) {
		List<String> texts = new ArrayList<String>();
		while (!isAnyOf(types)) {
			if (currentType() == null) {
				break;
			}
			texts.add(currentText());
			currentToken++;
		}
		return texts;
	}

	/**
	 * Skips balanced braces as long as they only contain the allowed inner
	 * types. Checks that the current token is of the opening type.
	 * 
	 * Skipping may be interrupted prematurely if unexpected tokens are
	 * encountered. In this case, the parser is left pointing to the bad token.
	 * 
	 * @return <code>true</code> if the skipping succeeded and
	 *         <code>false</code> if there were invalid tokens inside the
	 *         balanced braces or the braces were not balanced.
	 */
	public boolean skipBalanced(ETokenType openingType, ETokenType closingType,
			Set<ETokenType> allowedInnerTypes) {
		CCSMPre.isFalse(allowedInnerTypes.contains(openingType),
				"The given inner token types contain the opening token type");
		CCSMPre.isFalse(allowedInnerTypes.contains(closingType),
				"The given inner token types contain the closing token type");

		if (currentType() != openingType) {
			return false;
		}
		consumeOneOrZeroOf(EnumSet.of(openingType));

		int openCount = 1;
		while (openCount > 0) {
			consumeAnyOf(allowedInnerTypes);
			if (consumeOneOrZeroOf(EnumSet.of(openingType)) != null) {
				openCount++;
			} else if (consumeOneOrZeroOf(EnumSet.of(closingType)) != null) {
				openCount--;
			} else {
				return false;
			}
		}
		return true;
	}

	/** Consumes tokens as long as they alternate between the two type sets. */
	public List<String> consumeAlternating(Set<ETokenType> typeSet1,
			Set<ETokenType> typeSet2) {
		List<String> list = new ArrayList<String>();
		while (true) {
			String token1 = consumeOneOrZeroOf(typeSet1);
			if (token1 == null) {
				break;
			}
			list.add(token1);
			String token2 = consumeOneOrZeroOf(typeSet2);
			if (token2 == null) {
				break;
			}
			list.add(token2);
		}
		return list;
	}

	/**
	 * Returns <code>true</code> if the current token has any of the given
	 * types.
	 */
	public boolean isAnyOf(Set<ETokenType> types) {
		return types.contains(currentType());
	}

	/** Returns <code>true</code> if all tokens have been consumed. */
	public boolean isDone() {
		return currentToken >= tokens.size();
	}

	/**
	 * Returns the text of the current token or <code>null</code> if all tokens
	 * have been consumed.
	 */
	public String currentText() {
		if (isDone()) {
			return null;
		}
		return tokens.get(currentToken).getText();
	}

	/**
	 * Returns the type of the current token or <code>null</code> if all tokens
	 * have been consumed.
	 */
	public ETokenType currentType() {
		if (isDone()) {
			return null;
		}
		return tokens.get(currentToken).getType();
	}

	/**
	 * Returns the number of tokens consumed so far.
	 */
	public int getConsumedTokenCount() {
		return currentToken;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "TokenStreamParser "
				+ tokens.subList(currentToken, tokens.size());
	}
}
