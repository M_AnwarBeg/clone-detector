/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util.variable;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;

/**
 * A variable use extractor extracts uses of variables from token streams.
 * Thereby it differentiates between fields and local variables and considers
 * shadowing of variables.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 56054 $
 * @ConQAT.Rating GREEN Hash: 198EC530D9BE1A94D020BFAD967F2785
 */
public class CLikeVariableUseExtractor {

	/** The token type of the operator that is used to access fields. */
	private final ETokenType accessOperator;

	/**
	 * All token types that indicate that the previous token is no variable.
	 */
	private final EnumSet<ETokenType> noVariableSuccessorTypes;

	/** Constructor. */
	public CLikeVariableUseExtractor(ETokenType accessOperator,
			EnumSet<ETokenType> noVariableSuccessorTypes) {
		this.accessOperator = accessOperator;
		this.noVariableSuccessorTypes = noVariableSuccessorTypes;
	}

	/**
	 * Extracts all uses of the given variable name from the given tokens.
	 * 
	 * @param isField
	 *            whether the given variable name is a field of a type
	 * @param isShadowed
	 *            whether the given variable name is shadowed by another
	 *            variable with the same name
	 */
	public List<Integer> extractVariableUses(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed) {
		return extractFilteredVariableUses(tokens, variableName, isField,
				isShadowed, IVariableUseFilter.ACCEPT_ALL);
	}

	/**
	 * Extracts all uses of the given variable name from the given tokens that
	 * read its value.
	 * 
	 * @param isField
	 *            whether the given variable name is a field of a type
	 * @param isShadowed
	 *            whether the given variable name is shadowed by another
	 *            variable with the same name
	 */
	public List<Integer> extractVariableReads(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed) {
		return extractFilteredVariableUses(tokens, variableName, isField,
				isShadowed, VariableReadFilter.INSTANCE);
	}

	/**
	 * Extracts all uses of the given variable name from the given tokens that
	 * change its value.
	 * 
	 * @param isField
	 *            whether the given variable name is a field of a type
	 * @param isShadowed
	 *            whether the given variable name is shadowed by another
	 *            variable with the same name
	 */
	public List<Integer> extractVariableWrites(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed) {
		return extractFilteredVariableUses(tokens, variableName, isField,
				isShadowed, VariableWriteFilter.INSTANCE);
	}

	/**
	 * Extracts all uses of the given variable name from the given tokens.
	 * 
	 * @param isField
	 *            whether the given variable name is a field of a type
	 * @param isShadowed
	 *            whether the given variable name is shadowed by another
	 *            variable with the same name
	 */
	public List<Integer> extractFilteredVariableUses(List<IToken> tokens,
			String variableName, boolean isField, boolean isShadowed,
			IVariableUseFilter filter) {
		List<Integer> useIndices = TokenStreamTextUtils.findAll(tokens,
				variableName);
		return filterUses(tokens, useIndices, isField, isShadowed, filter);
	}

	/**
	 * Filters then given variable uses based on whether they are fields,
	 * shadowed and the given custom filter. The parameter useIndices is a list
	 * of indices to tokens in the given token list, that are possible variable
	 * uses.
	 */
	public List<Integer> filterUses(List<IToken> tokens,
			List<Integer> useIndices, boolean isField, boolean isShadowed,
			IVariableUseFilter filter) {
		List<Integer> filteredUses = new ArrayList<>();
		for (int index : useIndices) {
			if (isVariableUse(tokens, index, isField, isShadowed)
					&& filter.isFiltered(tokens, index, isField)) {
				filteredUses.add(index);
			}
		}
		return filteredUses;
	}

	/**
	 * Returns whether the element at the given index is any kind of variable
	 * use.
	 */
	private boolean isVariableUse(List<IToken> tokens, int index,
			boolean isField, boolean isShadowed) {
		if (index < tokens.size() - 1 && noVariableSuccessorTypes
				.contains(tokens.get(index + 1).getType())) {
			return false;
		}

		if (isField) {
			return isFieldUse(tokens, index, isShadowed);
		}
		return isLocalUse(tokens, index, isShadowed);
	}

	/**
	 * Returns whether the variable use at the given index is the use of the
	 * field. If it is not shadowed, it is definitely a field, otherwise we
	 * check if it is prefixed by the field access operator.
	 */
	private boolean isFieldUse(List<IToken> tokens, int index,
			boolean isShadowed) {
		return !isShadowed || (index > 1
				&& accessOperator.equals(tokens.get(index - 1).getType()));
	}

	/**
	 * Returns whether the variable use at the given index is the use of the
	 * local variable. If it is shadowed by another local variable it is not,
	 * otherwise we check if it is prefixed by the field access operator. If it
	 * is not, it is the local variable.
	 */
	private boolean isLocalUse(List<IToken> tokens, int index,
			boolean isShadowed) {
		return !isShadowed && (index <= 1
				|| !accessOperator.equals(tokens.get(index - 1).getType()));
	}
}
