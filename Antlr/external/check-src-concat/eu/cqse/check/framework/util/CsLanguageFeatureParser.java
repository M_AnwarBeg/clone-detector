/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.util;

import static eu.cqse.check.framework.scanner.ETokenType.BOOL;
import static eu.cqse.check.framework.scanner.ETokenType.BYTE;
import static eu.cqse.check.framework.scanner.ETokenType.CHAR;
import static eu.cqse.check.framework.scanner.ETokenType.CONST;
import static eu.cqse.check.framework.scanner.ETokenType.DECIMAL;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.FLOAT;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.INT;
import static eu.cqse.check.framework.scanner.ETokenType.INTERNAL;
import static eu.cqse.check.framework.scanner.ETokenType.LONG;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.OBJECT;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.READONLY;
import static eu.cqse.check.framework.scanner.ETokenType.SBYTE;
import static eu.cqse.check.framework.scanner.ETokenType.SHORT;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.STRING;
import static eu.cqse.check.framework.scanner.ETokenType.UINT;
import static eu.cqse.check.framework.scanner.ETokenType.ULONG;
import static eu.cqse.check.framework.scanner.ETokenType.USHORT;
import static eu.cqse.check.framework.scanner.ETokenType.VAR;
import static eu.cqse.check.framework.scanner.ETokenType.VOLATILE;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.languages.cs.CsShallowParser;

/**
 * Language feature parser for Cs.
 * 
 * @author $Author: poehlmann $
 * @version $Rev: 56515 $
 * @ConQAT.Rating GREEN Hash: B7EE5CF6B5AC6FC0D67944962292F16C
 */
public class CsLanguageFeatureParser extends CLikeLanguageFeatureParserBase {

	/** All token types that can be used to specify a type. */
	public static final EnumSet<ETokenType> TYPE_TOKENS = EnumSet.of(IDENTIFIER,
			VAR, BOOL, CHAR, BYTE, SBYTE, SHORT, INT, LONG, USHORT, UINT, ULONG,
			FLOAT, DOUBLE, DECIMAL, STRING, OBJECT);

	/** All token types that are modifiers for variables or attributes. */
	public static final EnumSet<ETokenType> VARIABLE_DECLARATION_MODIFIERS = EnumSet
			.of(PUBLIC, PRIVATE, INTERNAL, PROTECTED, CONST, READONLY, STATIC,
					VOLATILE, NEW);

	/** Suffix for CS-EventHandlers. */
	public static final String EVENT_ARGS_SUFFIX = "EventArgs";

	/** Constructor. */
	public CsLanguageFeatureParser() {
		super(CsShallowParser.VALID_IDENTIFIERS, TYPE_TOKENS);
	}

	/**
	 * Returns the imported namespace name from a given "using" entity. If the
	 * "using" clause is an aliasing clause, null is returned.
	 */
	@Override
	public String getImportName(ShallowEntity entity) {
		CCSMAssert.isTrue(isImport(entity),
				"entity.getType() must be equal to EShallowEntityType.META and "
						+ "entity.getSubtype() must be equal to SubTypeNames.USING");

		List<IToken> tokens = entity.ownStartTokens();

		if (TokenStreamUtils.containsAll(tokens, EQ)) {
			// if this using directive is aliasing a namespace (thus it contains
			// a "="), we don't extract the namespace
			return null;
		}

		int lastIdentifierIndex = TokenStreamUtils.findLast(tokens,
				CsShallowParser.VALID_IDENTIFIERS);
		if (lastIdentifierIndex == TokenStreamUtils.NOT_FOUND) {
			return null;
		}

		return TokenStreamTextUtils
				.concatTokenTexts(tokens.subList(1, lastIdentifierIndex + 1));
	}

	/** Returns whether the given entity is a using directive. */
	@Override
	public boolean isImport(ShallowEntity entity) {
		return entity.getType().equals(EShallowEntityType.META)
				&& entity.getSubtype().equals(SubTypeNames.USING);
	}

	/** Checks whether the given method is a CS-EventHandler. */
	public boolean isEventHandler(ShallowEntity method) {
		CCSMAssert.isTrue(method.getType() == EShallowEntityType.METHOD,
				"method.getType() must be \"METHOD\"");
		if (!hasVoidReturnType(method)) {
			return false;
		}

		List<List<IToken>> parameterTokens = getParameterTokens(method);
		if (parameterTokens.size() != 2) {
			return false;
		}

		List<IToken> firstParameter = parameterTokens.get(0);
		List<IToken> secondParameter = parameterTokens.get(1);

		if (!TokenStreamUtils.hasTypes(firstParameter, ETokenType.OBJECT,
				ETokenType.IDENTIFIER)) {
			return false;
		}
		if (TokenStreamUtils.hasTypes(secondParameter, ETokenType.IDENTIFIER,
				ETokenType.IDENTIFIER)) {
			return secondParameter.get(0).getText().endsWith(EVENT_ARGS_SUFFIX);
		}
		return false;
	}

	/** Returns whether the given entity is partial. */
	public boolean isPartial(ShallowEntity entity) {
		return TokenStreamUtils.find(entity.ownStartTokens(),
				ETokenType.PARTIAL) != TokenStreamUtils.NOT_FOUND;
	}

	// Currently the following methods are only supported for CS, but the future
	// they should be moved to CLikeLanguageUtils and adapted in a way that they
	// support Java too.

	/** Returns a list of all class names the given entity inherits from. */
	public List<String> getParentNames(ShallowEntity type) {
		CCSMAssert.isTrue(type.getType() == EShallowEntityType.TYPE,
				"type.getType() must be \"TYPE\"");
		List<IToken> inheritanceTokens = TokenStreamUtils.tokensBetween(
				type.ownStartTokens(), ETokenType.COLON, ETokenType.LBRACE);
		if (inheritanceTokens.isEmpty()) {
			return CollectionUtils.emptyList();
		}

		List<List<IToken>> splitInheritanceTokens = TokenStreamUtils
				.splitWithNesting(inheritanceTokens, ETokenType.COMMA,
						ETokenType.LT, ETokenType.GT);
		return TokenStreamTextUtils.concatAllTokenTexts(splitInheritanceTokens);
	}

	/**
	 * Returns all variable names from a variable declaration within a for
	 * statement.
	 */
	public List<IToken> getVariableNamesFromForLikeTokens(
			List<IToken> forLikeTokens, ETokenType endToken) {
		List<IToken> variableDeclarationTokens = getVariableTokensFromForLikeTokens(
				forLikeTokens, endToken);
		return getVariableDeclarationNamesFromTokens(variableDeclarationTokens);
	}

	/**
	 * Returns all variable names from the given variable declaration tokens.
	 * Names are only returned if they are newly introduced, that means, that e.
	 * g. the variable "t" from "using(t) {" will not be returned. This method
	 * does not support generic types at the moment.
	 */
	public List<IToken> getVariableDeclarationNamesFromTokens(
			List<IToken> variableDeclarationTokens) {
		List<IToken> variableNames = new ArrayList<>();

		int offset = 0;
		while (variableDeclarationTokens.size() > 0
				&& VARIABLE_DECLARATION_MODIFIERS.contains(
						variableDeclarationTokens.get(offset).getType())) {
			offset += 1;
		}

		if (variableDeclarationTokens.size() >= 2 + offset) {
			IToken token1 = variableDeclarationTokens.get(offset);
			IToken token2 = variableDeclarationTokens.get(1 + offset);
			if (isVariableDeclaration(token1, token2)) {
				variableNames.add(token2);
				addAdditionalVariableNameTokens(variableDeclarationTokens,
						offset, variableNames);
			}

		}

		return variableNames;
	}

	/** Adds additional variable name tokens. */
	private static void addAdditionalVariableNameTokens(
			List<IToken> variableDeclarationTokens, int offset,
			List<IToken> variableNames) {
		int parenthesisNesting = 0;
		for (int i = 2 + offset; i < variableDeclarationTokens.size(); ++i) {
			IToken token = variableDeclarationTokens.get(i);
			ETokenType tokenType = token.getType();
			if (tokenType == ETokenType.LPAREN) {
				parenthesisNesting += 1;
			} else if (tokenType == ETokenType.RPAREN) {
				parenthesisNesting -= 1;
			} else if (tokenType == ETokenType.IDENTIFIER
					&& parenthesisNesting == 0 && variableDeclarationTokens
							.get(i - 1).getType() == ETokenType.COMMA) {
				variableNames.add(token);
			}
		}
	}

	/** Returns whether the two given tokens indicate a variable declaration. */
	private boolean isVariableDeclaration(IToken token1, IToken token2) {
		return (typeTokens.contains(token1.getType())
				&& validIdentifiers.contains(token2.getType()));
	}

	/** {@inheritDoc} */
	@Override
	public boolean hasVariableLengthArgumentList(ShallowEntity method) {
		List<IToken> tokens = method.ownStartTokens();
		return TokenStreamUtils.containsAll(tokens, ETokenType.PARAMS);
	}

	/** {@inheritDoc} */
	@Override
	public ELanguage getLanguage() {
		return ELanguage.CS;
	}
}
