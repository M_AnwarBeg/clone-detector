/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;

/**
 * Common interface for all preprocessors. The preprocessor is not guaranteed to
 * be state-free so a new instance should be created for every new file that
 * needs to be processed.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: D6514FD19096E4EC8E48DC54FB675A55
 */
public interface IPreprocessor {

	/**
	 * Performs the actual preprocessing of the given tokens.
	 * 
	 * @param uniformPath
	 *            may be null if this information is not available.
	 * @param tokens
	 *            A list of tokens that need to be preprocessed. The given list
	 *            may get modified.
	 * @return A new list with the preprocessed tokens.
	 */
	List<IToken> preprocess(String uniformPath, List<IToken> tokens);
}
