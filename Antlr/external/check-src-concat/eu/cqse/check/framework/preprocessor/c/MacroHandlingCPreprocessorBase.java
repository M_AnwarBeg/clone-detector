/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;

import org.conqat.lib.commons.cache4j.ICache;
import org.conqat.lib.commons.cache4j.SynchronizedCache;
import org.conqat.lib.commons.cache4j.backend.ECachingStrategy;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.UnmodifiableList;
import org.conqat.lib.commons.error.NeverThrownRuntimeException;
import org.conqat.lib.commons.factory.ForwardingFactory;
import org.conqat.lib.commons.region.RegionSet;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.preprocessor.IPreprocessor;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * Base class of the C preprocessor that deals with macro handling and the
 * overall parsing.
 *
 * The design of the preprocessor uses inheritance to separate the aspects macro
 * handling, include handling, and conditionals to separate classes. This is
 * meant to keep the classes small and easier to understand, while still
 * providing a final common class, that can be easily extended by subclassing
 * and overriding certain methods, which would be hard when using
 * delegation/composition.
 *
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: DD7AF72DAB0B5309DD56390A731B0DCE
 */
public abstract class MacroHandlingCPreprocessorBase implements IPreprocessor {

	/** The cache used for storing macro tokens. */
	@SuppressWarnings("unchecked")
	private static final ICache<ObtainMacroTokensKey, UnmodifiableList<IToken>, NeverThrownRuntimeException> MACRO_TOKEN_CACHE = new SynchronizedCache<>(
			"MACRO-TOKEN-CACHE", ForwardingFactory.INSTANCE,
			ECachingStrategy.LRU
					.<ObtainMacroTokensKey, UnmodifiableList<IToken>> getBackend(
							500));

	/** Default name for the varargs parameter. */
	private static final String DEFAULT_VAR_ARGS_NAME = "__VA_ARGS__";

	/** Patterns used for finding if directives (and ifdef, ifndef, ...). */
	private static final Pattern IF_DIRECTIVE_START_PATTERN = Pattern
			.compile("^#\\s*if");

	/**
	 * Maximal number of nested expansions, used to defend against infinite
	 * recursion.
	 */
	private static final int EXPANSION_LIMIT = 20;

	/**
	 * Maximal number of nested inclusions, used to defend against infinite
	 * recursion.
	 */
	private static final int INCLUSION_LIMIT = 20;

	/** The origin name used for macro tokens. */
	public static final String MACRO_ORIGIN = "##macro##";

	/** The macro provider used. */
	protected final OverlayMacroProvider macroProvider;

	/** Constructor. */
	protected MacroHandlingCPreprocessorBase(IMacroProvider macroProvider) {
		this.macroProvider = new OverlayMacroProvider(macroProvider);
	}

	/** {@inheritDoc} */
	@Override
	public List<IToken> preprocess(String uniformPath, List<IToken> tokens) {
		return preprocess(uniformPath, new ArrayList<>(tokens), INCLUSION_LIMIT,
				new HashSet<>());
	}

	/**
	 * Performs the actual preprocessing of the given tokens. The given list of
	 * tokens might be changed.
	 * 
	 * @param uniformPath
	 *            may be null if this information is not available.
	 */
	protected List<IToken> preprocess(String uniformPath, List<IToken> tokens,
			int inclusionLimit, Set<String> seenPaths) {
		List<IToken> result = new ArrayList<>();
		if (uniformPath != null && seenPaths.contains(uniformPath)) {
			return result;
		} else if (uniformPath != null) {
			seenPaths.add(uniformPath);
		}
		RegionSet ignoredRegions = new RegionSet();

		for (int i = 0; i < tokens.size(); ++i) {
			if (ignoredRegions.contains(i)) {
				continue;
			}

			IToken token = tokens.get(i);
			switch (token.getType()) {
			case PREPROCESSOR_INCLUDE:
				handleInclude(uniformPath, token, result, inclusionLimit,
						seenPaths);
				break;
			case PREPROCESSOR_DIRECTIVE:
				if (isIfDirective(token)) {
					processIfDirective(tokens, i, ignoredRegions);
				} else {
					handleMacroPreprocessorDirective(token, result);
				}
				break;
			case IDENTIFIER:
				if (macroProvider.isDefined(token.getText())) {
					i = expandMacro(tokens, i, result);
					break;
				}
				// fall-through in else-case intended
			default:
				result.add(token);
			}
		}
		return result;
	}

	/**
	 * Expands the macro at the given position in the token stream. Appends the
	 * results to the given result list and returns the index to continue at.
	 */
	private int expandMacro(List<IToken> tokens, int index,
			List<IToken> result) {
		List<IToken> expanded = new ArrayList<>();
		int tokensConsumed = expandMacro(tokens, index, expanded,
				EXPANSION_LIMIT);

		// in some cases we need a full rescan of the generated tokens
		if (containsFurtherMacros(expanded,
				tokens.subList(index, index + 1 + tokensConsumed))) {
			index = mergeExpansionIntoTokens(tokens, index, expanded,
					tokensConsumed);
		} else {
			result.addAll(expanded);
			index += tokensConsumed;
		}
		return index;
	}

	/**
	 * Merges the expanded tokens back into the tokens. The size of the tokens
	 * list will be preserved, but parts before <code>index</code> might be
	 * replaced with null.
	 * 
	 * @return one before the index to continue scanning from.
	 */
	protected static int mergeExpansionIntoTokens(List<IToken> tokens,
			int index, List<IToken> expanded, int tokensConsumed) {
		expanded.addAll(
				tokens.subList(index + tokensConsumed + 1, tokens.size()));
		int padding = Math.max(0, tokens.size() - expanded.size());
		tokens.clear();
		tokens.addAll(Collections.nCopies(padding, null));
		tokens.addAll(expanded);
		return padding - 1;
	}

	/**
	 * Returns whether the given list of tokens contains any potential macros
	 * that need expansion and that have not been before in the list of known
	 * tokens.
	 */
	private boolean containsFurtherMacros(List<IToken> tokens,
			List<IToken> knownTokens) {
		Set<String> knownIdentifiers = new HashSet<>(
				CollectionUtils
						.filterAndMap(knownTokens,
								token -> token
										.getType() == ETokenType.IDENTIFIER,
								IToken::getText));

		return tokens.stream()
				.anyMatch(token -> token.getType() == ETokenType.IDENTIFIER
						&& !knownIdentifiers.contains(token.getText())
						&& macroProvider.isDefined(token.getText()));
	}

	/** Returns whether the given token is an if/ifdef/ifndef directive. */
	protected static boolean isIfDirective(IToken token) {
		return IF_DIRECTIVE_START_PATTERN.matcher(token.getText()).find();
	}

	/**
	 * Handles the include directive.
	 * 
	 * @param uniformPath
	 *            the uniform path of the file with the include.
	 */
	protected abstract void handleInclude(String uniformPath, IToken token,
			List<IToken> result, int inclusionLimit, Set<String> seenPaths);

	/**
	 * Processes an if/ifdef/ifndef directive. This only updates ignored
	 * regions, to exclude parts of the tokens.
	 *
	 * @param tokens
	 *            the tokens being processed.
	 * @param ifIndex
	 *            the index in the tokens list, where the if directive is found.
	 * @param ignoredRegions
	 *            the regions of indexes to be ignored during parsing.
	 */
	protected abstract void processIfDirective(List<IToken> tokens, int ifIndex,
			RegionSet ignoredRegions);

	/** Handles any preprocessor directives affecting macro definitions. */
	private void handleMacroPreprocessorDirective(IToken token,
			List<IToken> result) {
		// strip leading '#'
		String content = token.getText().trim().substring(1);

		List<IToken> subTokens = parseMacroContent(content);
		if (subTokens.isEmpty()) {
			return;
		}

		switch (subTokens.get(0).getText()) {
		case "define":
			if (subTokens.size() > 1) {
				macroProvider.define(subTokens.get(1).getText(), StringUtils
						.stripPrefix(content.trim(), "define").trim());
			}
			break;
		case "undef":
			if (subTokens.size() > 1) {
				macroProvider.undefine(subTokens.get(1).getText());
			}
			break;
		default:
			// ignore other/unknown directives
			result.add(token);
		}
	}

	/** Parses a macor's content. */
	protected static List<IToken> parseMacroContent(String content) {
		return MACRO_TOKEN_CACHE.obtain(new ObtainMacroTokensKey(content));
	}

	/**
	 * Expands the macro at the given start position in the token stream.
	 *
	 * @param tokens
	 *            the input token stream where the macros was found.
	 * @param macroIndex
	 *            the index in the token stream where the macro was found.
	 * @param result
	 *            the list to append any new tokens to.
	 * @param expansionLimit
	 *            the maximal number of expansion performed
	 * @return the additional number of tokens consumed (besides the macro name
	 *         itself).
	 */
	private int expandMacro(List<IToken> tokens, int macroIndex,
			List<IToken> result, int expansionLimit) {
		String name = tokens.get(macroIndex).getText();
		String definition = macroProvider.getDefinition(name);
		if (definition == null || expansionLimit <= 0) {
			result.add(tokens.get(macroIndex));
			return 0;
		}
		expansionLimit -= 1;

		List<IToken> definitionTokens = parseMacroContent(definition);

		if (isFunctionMacro(definitionTokens)) {
			if (tokens.size() <= macroIndex + 1 || tokens.get(macroIndex + 1)
					.getType() != ETokenType.LPAREN) {
				result.add(tokens.get(macroIndex));
				return 0;
			}

			Map<String, Integer> parameterNameToPosition = determineParameterNames(
					definitionTokens);
			List<List<IToken>> actualArguments = extractActualArguments(tokens,
					macroIndex);
			List<IToken> macroContent = definitionTokens.subList(
					1 + determineParameterTokenCount(parameterNameToPosition),
					definitionTokens.size());
			expandMacroContent(macroContent, parameterNameToPosition,
					actualArguments, result, expansionLimit);
			return determineArgumentTokenCount(actualArguments);
		}

		expandMacroContent(definitionTokens.subList(1, definitionTokens.size()),
				CollectionUtils.<String, Integer> emptyMap(),
				CollectionUtils.<List<IToken>> emptyList(), result,
				expansionLimit);
		return 0;
	}

	/** Returns the number of tokens used to represent the given arguments. */
	private static int determineParameterTokenCount(
			Map<String, Integer> parameterNameToPosition) {
		if (parameterNameToPosition.isEmpty()) {
			return 2;
		}
		int result = 1 + 2 * parameterNameToPosition.size();
		for (Entry<String, Integer> entry : parameterNameToPosition
				.entrySet()) {
			if (entry.getValue() < 0
					&& !DEFAULT_VAR_ARGS_NAME.equals(entry.getKey())) {
				result += 1;
			}
		}
		return result;
	}

	/**
	 * Returns a mapping from parameter name to parameter index. A var args
	 * parameter will be encoded using a negative index (decremented by 1 to
	 * make it distinguishable from 0).
	 */
	private static Map<String, Integer> determineParameterNames(
			List<IToken> definitionTokens) {
		Map<String, Integer> argumentNameToPosition = new HashMap<>();
		for (int i = 2; i < definitionTokens.size(); i += 2) {
			if (definitionTokens.get(i - 1).getType() == ETokenType.RPAREN) {
				break;
			}
			if (definitionTokens.get(i).getType() == ETokenType.IDENTIFIER) {
				argumentNameToPosition.put(definitionTokens.get(i).getText(),
						i / 2 - 1);
				if (i + 1 < definitionTokens.size() && definitionTokens
						.get(i + 1).getType() == ETokenType.ELLIPSIS) {
					argumentNameToPosition.put(
							definitionTokens.get(i).getText(),
							-(i / 2 - 1) - 1);
					break;
				}
			} else if (definitionTokens.get(i)
					.getType() == ETokenType.ELLIPSIS) {
				argumentNameToPosition.put(DEFAULT_VAR_ARGS_NAME,
						-(i / 2 - 1) - 1);
			} else {
				break;
			}
		}
		return argumentNameToPosition;
	}

	/**
	 * Returns the number of tokens that are used to describe the given macro
	 * arguments.
	 */
	private static int determineArgumentTokenCount(
			List<List<IToken>> actualArguments) {
		int additionalTokens = 1 + actualArguments.size();
		for (List<IToken> argument : actualArguments) {
			additionalTokens += argument.size();
		}
		return additionalTokens;
	}

	/** Extracts the actual arguments to a macro. */
	private static List<List<IToken>> extractActualArguments(
			List<IToken> tokens, int macroIndex) {
		List<List<IToken>> actualArguments = new ArrayList<>();
		List<IToken> currentArgument = new ArrayList<>();
		int nesting = 0;
		for (int i = macroIndex + 2; i < tokens.size(); ++i) {
			switch (tokens.get(i).getType()) {
			case LPAREN:
				nesting += 1;
				currentArgument.add(tokens.get(i));
				break;
			case RPAREN:
				if (nesting == 0) {
					actualArguments.add(currentArgument);
					return actualArguments;
				}

				nesting -= 1;
				currentArgument.add(tokens.get(i));
				break;
			case COMMA:
				if (nesting > 0) {
					currentArgument.add(tokens.get(i));
				} else {
					actualArguments.add(currentArgument);
					currentArgument = new ArrayList<>();
				}
				break;
			default:
				currentArgument.add(tokens.get(i));
			}
		}
		return actualArguments;
	}

	/**
	 * Returns whether the macro defined by the given tokens is a function
	 * macro. This is the case if the second token is a parenthesis and follows
	 * <b>without</b> any whitespace to the macro's name.
	 */
	private static boolean isFunctionMacro(List<IToken> definitionTokens) {
		if (definitionTokens.size() < 2) {
			return false;
		}

		IToken nameToken = definitionTokens.get(0);
		IToken secondToken = definitionTokens.get(1);

		return secondToken.getType() == ETokenType.LPAREN
				&& nameToken.getEndOffset() + 1 == secondToken.getOffset();
	}

	/**
	 * Expands a macro's content. This applies recursive macro expansion to both
	 * the arguments and the completed expansion (also see
	 * https://gcc.gnu.org/onlinedocs/cpp/Argument-Prescan.html for an
	 * explanation of this).
	 */
	private void expandMacroContent(List<IToken> macroContent,
			Map<String, Integer> parameterNameToPosition,
			List<List<IToken>> actualArguments, List<IToken> result,
			int expansionLimit) {
		List<IToken> expansion = new ArrayList<>();
		for (int i = 0; i < macroContent.size(); ++i) {
			IToken token = macroContent.get(i);
			if (i + 2 < macroContent.size() && macroContent.get(i + 1)
					.getType() == ETokenType.CONCATENATION) {
				List<IToken> argument1 = expandParameter(token,
						parameterNameToPosition, actualArguments);
				List<IToken> argument2 = expandParameter(
						macroContent.get(i + 2), parameterNameToPosition,
						actualArguments);
				appendMerged(argument1, argument2, expansion);
				i += 2;
			} else if (token.getType() == ETokenType.HASH
					&& i + 1 < macroContent.size()) {
				i += 1;
				String text = toStringLiteral(
						expandParameter(macroContent.get(i),
								parameterNameToPosition, actualArguments));
				expansion.add(TokenStreamUtils.createToken(token, text,
						ETokenType.STRING_LITERAL));
			} else if (token.getType() == ETokenType.IDENTIFIER
					&& parameterNameToPosition.containsKey(token.getText())) {
				appendWithRepeatedMacroExpansion(expandParameter(token,
						parameterNameToPosition, actualArguments),
						expansionLimit, expansion);
			} else {
				expansion.add(token);
			}
		}

		appendWithMacroExpansion(expansion, expansionLimit, result);
	}

	/**
	 * Appends both token lists to the result list but merges the joining
	 * tokens.
	 */
	private static void appendMerged(List<IToken> argument1,
			List<IToken> argument2, List<IToken> result) {
		if (argument1.isEmpty() || argument2.isEmpty()) {
			result.addAll(argument1);
			result.addAll(argument2);
			return;
		}

		result.addAll(argument1.subList(0, argument1.size() - 1));

		IToken merge1 = CollectionUtils.getLast(argument1);
		IToken merge2 = argument2.get(0);
		result.addAll(parseMacroContent(merge1.getText() + merge2.getText()));

		result.addAll(argument2.subList(1, argument2.size()));
	}

	/**
	 * Returns the expanded parameter. Returns the parameter name token itself,
	 * if the name is not a valid parameter name.
	 */
	private static List<IToken> expandParameter(IToken parameterNameToken,
			Map<String, Integer> parameterNameToPosition,
			List<List<IToken>> actualArguments) {
		Integer index = parameterNameToPosition
				.get(parameterNameToken.getText());
		if (index == null) {
			return Collections.singletonList(parameterNameToken);
		}
		if (index >= actualArguments.size()) {
			return CollectionUtils.emptyList();
		}

		// handle var args
		if (index < 0) {
			List<IToken> result = new ArrayList<>();
			for (int i = -(index + 1); i < actualArguments.size(); ++i) {
				if (!result.isEmpty()) {
					IToken last = CollectionUtils.getLast(result);
					result.add(last.newToken(ETokenType.COMMA,
							last.getEndOffset(), last.getLineNumber(), ",",
							last.getOriginId()));
				}
				result.addAll(actualArguments.get(i));
			}
			return result;
		}

		return actualArguments.get(index);
	}

	/** Converts tokens to a string literal. */
	private static String toStringLiteral(List<IToken> tokens) {
		StringBuilder builder = new StringBuilder("\"");
		IToken previous = null;
		for (IToken token : tokens) {
			if (previous != null) {
				int count = token.getOffset() - previous.getEndOffset() - 1;
				for (int i = 0; i < count; ++i) {
					builder.append(StringUtils.SPACE);
				}
			}

			builder.append(token.getText());
			previous = token;
		}
		builder.append("\"");
		return builder.toString();
	}

	/**
	 * Appends the given input tokens to the result, applying macro expansion
	 * along the way.
	 */
	private void appendWithMacroExpansion(List<IToken> input,
			int expansionLimit, List<IToken> result) {
		for (int i = 0; i < input.size(); ++i) {
			IToken token = input.get(i);
			if (token.getType() == ETokenType.IDENTIFIER
					&& macroProvider.isDefined(token.getText())) {
				i += expandMacro(input, i, result, expansionLimit);
			} else {
				result.add(token);
			}
		}
	}

	/**
	 * Appends the given input tokens to the result, applying macro expansion as
	 * often as possible (within the expansion limit).
	 */
	private void appendWithRepeatedMacroExpansion(List<IToken> input,
			int expansionLimit, List<IToken> result) {
		List<IToken> previousExpanded = new ArrayList<>();
		List<IToken> expanded = new ArrayList<>();
		appendWithMacroExpansion(input, expansionLimit, expanded);

		while (!previousExpanded.equals(expanded)) {
			previousExpanded = expanded;
			expanded = new ArrayList<>();
			expansionLimit -= 1;
			appendWithMacroExpansion(previousExpanded, expansionLimit,
					expanded);
		}

		result.addAll(expanded);
	}
}
