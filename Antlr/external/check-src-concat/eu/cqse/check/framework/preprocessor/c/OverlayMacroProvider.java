/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMAssert;

/**
 * A macro provider that encapsulates another macro provider and allows
 * temporary addition and removal of macros.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating YELLOW Hash: 6414205B9B536480754350A133DCC654
 */
public class OverlayMacroProvider implements IMacroProvider {

	/** The encapsulated provider. */
	private final IMacroProvider innerProvider;

	/** The definitions (without leading '#' and 'define') of known macros. */
	private final Map<String, String> macros = new HashMap<>();

	/** Macros that have been explicitly undefined. */
	private final Set<String> undefinedMacros = new HashSet<>();

	/** Constructor. */
	public OverlayMacroProvider(IMacroProvider innerProvider) {
		CCSMAssert.isNotNull(innerProvider);
		this.innerProvider = innerProvider;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isDefined(String name) {
		if (undefinedMacros.contains(name)) {
			return false;
		}

		if (macros.containsKey(name)) {
			return true;
		}

		return innerProvider.isDefined(name);
	}

	/** {@inheritDoc} */
	@Override
	public String getDefinition(String name) {
		String definition = macros.get(name);
		if (definition != null) {
			return definition;
		}
		return innerProvider.getDefinition(name);
	}

	/**
	 * Defines a macro.
	 * 
	 * @param definition
	 *            the macro definition without leading '#' and 'define'.
	 */
	public void define(String name, String definition) {
		macros.put(name, definition);
		undefinedMacros.remove(name);
	}

	/** Undefines a macro. */
	public void undefine(String name) {
		undefinedMacros.add(name);
		macros.remove(name);
	}
}
