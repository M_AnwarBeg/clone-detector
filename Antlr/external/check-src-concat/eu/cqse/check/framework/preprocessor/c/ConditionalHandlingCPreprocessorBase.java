/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.conqat.lib.commons.cache4j.ICache;
import org.conqat.lib.commons.cache4j.SynchronizedCache;
import org.conqat.lib.commons.cache4j.backend.ECachingStrategy;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.error.NeverThrownRuntimeException;
import org.conqat.lib.commons.region.Region;
import org.conqat.lib.commons.region.RegionSet;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * Base class of the C preprocessor that deals with conditionals in addition to
 * macro handling and the overall parsing.
 * 
 * The design of the preprocessor uses inheritance to separate the aspects macro
 * handling, include handling, and conditionals to separate classes. This is
 * meant to keep the classes small and easier to understand, while still
 * providing a final common class, that can be easily extended by subclassing
 * and overriding certain methods, which would be hard when using
 * delegation/composition.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: 264D7615D767A89855AB81CC69FF246B
 */
public abstract class ConditionalHandlingCPreprocessorBase
		extends MacroHandlingCPreprocessorBase {

	/** The script engine used for evaluating conditionals. */
	private static final ScriptEngine CONDITIONAL_EVALUATION_ENGINE = new ScriptEngineManager()
			.getEngineByName("JavaScript");

	/**
	 * Caching for expressions to reduce the number of expensive evaluations.
	 */
	private static final ICache<String, Boolean, NeverThrownRuntimeException> EXPRESSION_CACHE = new SynchronizedCache<>(
			"EXPRESSION_CACHE",
			(String expression) -> evaluateExpression(expression),
			ECachingStrategy.LRU.<String, Boolean> getBackend(5000));

	/** Patterns used for finding endif directives. */
	private static final Pattern ENDIF_DIRECTIVE_START_PATTERN = Pattern
			.compile("^#\\s*endif");

	/** Patterns used for finding elif directives. */
	private static final Pattern ELIF_DIRECTIVE_START_PATTERN = Pattern
			.compile("^#\\s*elif");

	/** Patterns used for finding else directives. */
	private static final Pattern ELSE_DIRECTIVE_START_PATTERN = Pattern
			.compile("^#\\s*else");

	/** Constructor. */
	protected ConditionalHandlingCPreprocessorBase(
			IMacroProvider macroProvider) {
		super(macroProvider);
	}

	/** Returns whether the given token is an endif directive. */
	private static boolean isEndIfDirective(IToken token) {
		return ENDIF_DIRECTIVE_START_PATTERN.matcher(token.getText()).find();
	}

	/** Returns whether the given token is an elif directive. */
	private static boolean isElifDirective(IToken token) {
		return ELIF_DIRECTIVE_START_PATTERN.matcher(token.getText()).find();
	}

	/** Returns whether the given token is an else directive. */
	private static boolean isElseDirective(IToken token) {
		return ELSE_DIRECTIVE_START_PATTERN.matcher(token.getText()).find();
	}

	/** {@inheritDoc} */
	@Override
	protected void processIfDirective(List<IToken> tokens, int ifIndex,
			RegionSet ignoredRegions) {
		List<IfRegionDescriptor> ifRegions = new ArrayList<>();
		ifRegions.add(new IfRegionDescriptor(
				extractCondition(tokens.get(ifIndex)), ifIndex));
		int nestingCount = 0;
		for (int i = ifIndex + 1; i < tokens.size(); ++i) {
			IToken currentToken = tokens.get(i);
			if (isIfDirective(currentToken)) {
				nestingCount += 1;
			}

			if (nestingCount > 0) {
				if (isEndIfDirective(currentToken)) {
					nestingCount -= 1;
				}
				continue;
			}

			if (isEndIfDirective(currentToken)) {
				CollectionUtils.getLast(ifRegions).closeRegion(i);
				decideRegionInclusion(ifRegions);
				applyIfRegions(ifRegions, ignoredRegions);
				return;
			} else if (isElseDirective(currentToken)) {
				CollectionUtils.getLast(ifRegions).closeRegion(i);
				ifRegions.add(new IfRegionDescriptor("1", i));
			} else if (isElifDirective(currentToken)) {
				CollectionUtils.getLast(ifRegions).closeRegion(i);
				ifRegions.add(new IfRegionDescriptor(
						extractCondition(tokens.get(i)), i));
			}
		}

		// if we reached this, there is a dangling region at the end
		CollectionUtils.getLast(ifRegions).closeRegion(tokens.size() - 1);
	}

	/**
	 * Extracts and returns the condition for an if/ifdef/ifndef/elif directive.
	 */
	private static String extractCondition(IToken token) {
		// strip leading '#'
		String content = token.getText().trim().substring(1);
		List<IToken> subTokens = parseMacroContent(content);

		if (subTokens.size() == 2
				&& subTokens.get(0).getText().equals("ifdef")) {
			return "defined(" + subTokens.get(1).getText() + ")";
		}

		if (subTokens.size() == 2
				&& subTokens.get(0).getText().equals("ifndef")) {
			return "!defined(" + subTokens.get(1).getText() + ")";
		}

		return TokenStreamUtils
				.toString(subTokens.subList(1, subTokens.size()));
	}

	/**
	 * Decides for each ifRegion, whether it should be included or not. The
	 * default implementation does this by checking each region's condition and
	 * selecting the first region with a true condition. Sub classes may change
	 * the behavior, by including one or multiple regions based on other
	 * criteria.
	 * 
	 * @param ifRegions
	 *            the regions to decide for. The inclusion of the regions is
	 *            initially false and must be updated, if a region should be
	 *            included.
	 */
	protected void decideRegionInclusion(List<IfRegionDescriptor> ifRegions) {
		for (IfRegionDescriptor ifRegion : ifRegions) {
			if (conditionIsTrue(ifRegion.getCondition())) {
				ifRegion.setInclude(true);
				return;
			}
		}
	}

	/** Evaluates an if condition and returns its boolean value. */
	protected boolean conditionIsTrue(String condition) {
		List<IToken> conditionTokens = parseMacroContent(condition);
		conditionTokens = expandDefined(conditionTokens);

		// expand all macros
		conditionTokens = preprocess(null, conditionTokens);

		// if there are still identifiers left, we are missing values for them
		if (conditionTokens.isEmpty() || TokenStreamUtils
				.containsAny(conditionTokens, ETokenType.IDENTIFIER)) {
			return false;
		}

		if (conditionTokens.size() == 1 && conditionTokens.get(0)
				.getType() == ETokenType.INTEGER_LITERAL) {
			return Integer.parseInt(conditionTokens.get(0).getText()
					.replaceAll("[^0-9]", "")) != 0;
		}

		String expression = TokenStreamUtils
				.toString(CollectionUtils.filter(conditionTokens, token -> token
						.getType().getTokenClass() != ETokenClass.COMMENT));
		expression = stripNumberSuffixes(expression);
		return EXPRESSION_CACHE.obtain(expression);
	}

	/**
	 * Removes number suffixes for size and signedness (u, s, l, etc.) from
	 * decimal and hexadecimal numbers.
	 */
	private static String stripNumberSuffixes(String expression) {
		return expression.replaceAll("(\\d|[a-fA-F])[uUsSlL]{1,2}", "$1");
	}

	/** Expands "defined" statements. */
	private List<IToken> expandDefined(List<IToken> tokens) {
		List<IToken> result = new ArrayList<>();
		for (int i = 0; i < tokens.size(); ++i) {
			IToken token = tokens.get(i);
			if (token.getType() == ETokenType.IDENTIFIER
					&& token.getText().equals("defined")) {
				if (i + 3 >= tokens.size()
						|| !TokenStreamUtils.tokenTypesAt(tokens, i + 1,
								ETokenType.LPAREN, ETokenType.IDENTIFIER,
								ETokenType.RPAREN)) {
					// broken define; break here
					return result;
				}

				String macroName = tokens.get(i + 2).getText();
				String value = "0";
				if (macroProvider.isDefined(macroName)) {
					value = "1";
				}
				result.add(token.newToken(ETokenType.INTEGER_LITERAL,
						token.getOffset(), token.getLineNumber(), value,
						token.getOriginId()));

				// consume additional tokens
				i += 3;
			} else {
				result.add(token);
			}
		}
		return result;
	}

	/**
	 * Applies the inclusion state of the given if regions by updating the
	 * ignored regions.
	 */
	private static void applyIfRegions(List<IfRegionDescriptor> ifRegions,
			RegionSet ignoredRegions) {
		for (IfRegionDescriptor ifRegion : ifRegions) {
			if (ifRegion.isInclude()) {
				ignoredRegions.add(new Region(ifRegion.getStartIndex(),
						ifRegion.getStartIndex()));
				ignoredRegions.add(new Region(ifRegion.getEndIndex(),
						ifRegion.getEndIndex()));
			} else {
				ignoredRegions.add(new Region(ifRegion.getStartIndex(),
						ifRegion.getEndIndex()));
			}
		}
	}

	/** Evaluates a boolean expression. */
	private static Boolean evaluateExpression(String expression) {
		try {
			Object result;

			// the remainder at this point is a boolean or
			// arithmetic expression, which is compatible with
			// JavaScript, so we use this as parser
			synchronized (CONDITIONAL_EVALUATION_ENGINE) {
				result = CONDITIONAL_EVALUATION_ENGINE.eval(expression);
			}

			if (result instanceof Boolean) {
				return (Boolean) result;
			}
			if (result instanceof Integer) {
				return 0 != (Integer) result;
			}
			return false;
		} catch (ScriptException e) {
			// not parseable
			return false;
		}
	}
}
