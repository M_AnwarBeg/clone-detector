/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor;

import java.util.Optional;

import eu.cqse.check.framework.preprocessor.abap.AbapPreprocessor;
import eu.cqse.check.framework.preprocessor.c.CPreprocessor;
import eu.cqse.check.framework.preprocessor.c.EmptyMacroProvider;
import eu.cqse.check.framework.scanner.ELanguage;

/**
 * Factory for preprocessors.
 * 
 * @author $Author: goeb $
 * @version $Rev: 56641 $
 * @ConQAT.Rating GREEN Hash: 63487F575F8924B976B70778E4FF67C0
 */
public class PreprocessorFactory {

	/**
	 * Creates a simple version of the preprocessor for the given language. This
	 * simple version can be used e.g. in tests that parse only one file.
	 * 
	 * These simple preprocessors can especially not handle includes between
	 * files and other non-local language features.
	 */
	public static Optional<IPreprocessor> createSimplePreprocessor(
			ELanguage language) {
		switch (language) {
		case ABAP:
			return Optional.of(new AbapPreprocessor());
		case CPP:
			return Optional.of(new CPreprocessor(new EmptyMacroProvider()));
		default:
			return Optional.empty();
		}
	}

}
