/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import java.util.Collections;
import java.util.List;

import org.conqat.lib.commons.collections.Pair;

import eu.cqse.check.framework.scanner.IToken;

/**
 * Stores a pair of uniformPath and a corresponding list of included
 * {@link IToken}s.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating YELLOW Hash: 935D5698C21C8855A202555CEB7F0D8D
 */
public class IncludedTokens extends Pair<String, List<IToken>> {

	/** Constructor. */
	public IncludedTokens(String uniformPath, List<IToken> tokens) {
		super(uniformPath, tokens);
	}

	/**
	 * Returns an empty UniformPathTokens.
	 */
	public static IncludedTokens empty() {
		return new IncludedTokens(null, Collections.emptyList());
	}

}
