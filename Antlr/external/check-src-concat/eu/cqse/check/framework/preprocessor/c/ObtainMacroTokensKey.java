/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.UnmodifiableList;
import org.conqat.lib.commons.error.NeverThrownRuntimeException;
import org.conqat.lib.commons.factory.IFactory;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerUtils;

/**
 * The key/factory used to obtain new list of shallow entities from the cache.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: CC0A91A034301471F419C2F7A6EA7AFC
 */
/* package */ class ObtainMacroTokensKey implements
		IFactory<UnmodifiableList<IToken>, NeverThrownRuntimeException> {

	/** The macro's content. */
	private final String content;

	/** constructor */
	protected ObtainMacroTokensKey(String content) {
		this.content = content;
	}

	/** {@inheritDoc} */
	@Override
	public UnmodifiableList<IToken> create() {
		List<IToken> result = new ArrayList<>();
		for (IToken token : ScannerUtils.getTokens(content, ELanguage.CPP,
				MacroHandlingCPreprocessorBase.MACRO_ORIGIN)) {
			// we need special handling for the preprocessor directives,
			// as '#'has a different meaning in macros
			if (token.getType() == ETokenType.PREPROCESSOR_DIRECTIVE) {
				result.add(token.newToken(ETokenType.HASH, token.getOffset(),
						token.getLineNumber(), "#", token.getOriginId()));
				result.addAll(MacroHandlingCPreprocessorBase
						.parseMacroContent(token.getText().substring(1)));
			} else {
				result.add(token);
			}
		}
		return CollectionUtils.asUnmodifiable(result);
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ObtainMacroTokensKey)) {
			return false;
		}
		ObtainMacroTokensKey other = (ObtainMacroTokensKey) obj;
		return content.equals(other.content);
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		return content.hashCode();
	}

}