/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

/**
 * Interface for providing macros.
 *
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: 37080ECDF7E20E90D21F4B8EA7E4F0BE
 */
public interface IMacroProvider {

	/** Returns whether the macro of given name is defined. */
	boolean isDefined(String name);

	/**
	 * Returns the definition of the given macro without the leading '#' and
	 * 'define'. Returns null if no macro of given name is defined. For an empty
	 * macro, the empty string will be returned.
	 */
	String getDefinition(String name);

}
