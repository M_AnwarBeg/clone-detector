/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.abap;

import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.cqse.check.framework.preprocessor.IPreprocessor;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.NestingAwareTokenIterator;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * Preprocessor for ABAP. It rolls out so-called "chain sentences" in order to
 * make parsing easier.
 * 
 * For example, the following two code snippets are equivalent:
 * <ul>
 * <li><code>DATA: a TYPE i, b TYPE x.</code></li>
 * <li><code>DATA a TYPE i. DATA b TYPE x.</code></li>
 * </ul>
 * 
 * Basically, whenever there is a colon, this means that every statement after a
 * comma begins with whatever came before the colon.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56440 $
 * @ConQAT.Rating YELLOW Hash: 19DA9F6FC1AA2E0D44D7CAB949E5D223
 */
public class AbapPreprocessor implements IPreprocessor {

	/** {@inheritDoc} */
	@Override
	public List<IToken> preprocess(String uniformPath, List<IToken> tokens) {
		List<IToken> result = new ArrayList<>();
		int lastDot = -1;
		int lastColon = -1;

		NestingAwareTokenIterator iterator = new NestingAwareTokenIterator(
				tokens, 0, Arrays.asList(LPAREN), Arrays.asList(RPAREN));
		while (iterator.hasNext()) {
			IToken token = iterator.next();
			int currentIndex = iterator.getCurrentIndex();
			switch (token.getType()) {
			case COLON:
				lastColon = currentIndex;
				// Remove the colon by not inserting it into the result list.
				break;
			case COMMA:
				if (!iterator.isTopLevel() || lastColon == -1) {
					result.add(token);
					break;
				}
				result.add(TokenStreamUtils.createToken(token, ".", DOT));

				IToken reference = getReferenceToken(tokens, currentIndex);
				List<IToken> toRepeat = tokens.subList(lastDot + 1, lastColon);
				result.addAll(TokenStreamUtils.copyTokens(reference, toRepeat));
				break;
			case DOT:
				lastDot = currentIndex;
				lastColon = -1;
				// fall-through intended
			default:
				result.add(token);
			}
		}

		return result;
	}

	/**
	 * Gets the best possible reference token for the tokens to be duplicated.
	 * Usually the comma is at the end of the line, and the new statement starts
	 * in the next line, so we return the token following the comma, if it
	 * exists. We also take care to skip over any comment tokens.
	 */
	private static IToken getReferenceToken(List<IToken> tokens,
			int commaIndex) {
		int referenceIndex = commaIndex;
		while (referenceIndex + 1 < tokens.size()) {
			referenceIndex++;
			// skip over all comments as well
			if (tokens.get(referenceIndex).getType()
					.getTokenClass() != ETokenClass.COMMENT) {
				break;
			}
		}

		return tokens.get(referenceIndex);
	}
}
