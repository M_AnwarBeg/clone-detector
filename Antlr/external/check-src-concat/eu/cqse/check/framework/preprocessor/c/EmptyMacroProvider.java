/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

/**
 * A macro provider that holds no macros.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: 1FC6F08D7A93B1A167D900B1DFCA8BF1
 */
public class EmptyMacroProvider implements IMacroProvider {

	/** {@inheritDoc} */
	@Override
	public boolean isDefined(String name) {
		return false;
	}

	/** {@inheritDoc} */
	@Override
	public String getDefinition(String name) {
		return null;
	}
}
