/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.conqat.lib.commons.collections.Pair;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerUtils;

/**
 * An implementation of the C preprocessor that is adjusted to application in
 * static analysis. For this, various options of preprocessing can be adjusted.
 * This class adds include handling to its base classes.
 * 
 * The design of the preprocessor uses inheritance to separate the aspects macro
 * handling, include handling, and conditionals to separate classes. This is
 * meant to keep the classes small and easier to understand, while still
 * providing a final common class, that can be easily extended by subclassing
 * and overriding certain methods, which would be hard when using
 * delegation/composition.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating YELLOW Hash: 7C4E41942EF2AF148132684951C6BF31
 */
public class CPreprocessor extends ConditionalHandlingCPreprocessorBase {

	/** Patterns used for parsing include statements. */
	public static final Pattern INCLUDE_PATTERN = Pattern
			.compile("#\\s*include +[\"<](.+?)[\">]");

	/** Constructor. */
	public CPreprocessor(IMacroProvider macroProvider) {
		super(macroProvider);
	}

	/** {@inheritDoc} */
	@Override
	protected void handleInclude(String uniformPath, IToken token,
			List<IToken> result, int inclusionLimit, Set<String> seenPaths) {
		if (inclusionLimit <= 0) {
			return;
		}

		Matcher matcher = INCLUDE_PATTERN.matcher(token.getText());
		if (!matcher.find()) {
			return;
		}

		String includedName = matcher.group(1);
		IncludedTokens pathAndTokens = resolveIncludedTokens(uniformPath,
				includedName);
		result.addAll(preprocess(pathAndTokens.getFirst(),
				pathAndTokens.getSecond(), inclusionLimit - 1, seenPaths));
	}

	/**
	 * Returns the included tokens for a given include name.
	 * 
	 * @param includingUniformPath
	 *            the uniform path of the file that includes this. May be null
	 *            if no information is available.
	 * @param includedName
	 *            the name of the file to be included.
	 * @return the pair of the name of the resolved uniform path of the include
	 *         file and the file's tokens. The resolved uniform path may be
	 *         null, if it is not available.
	 */
	protected IncludedTokens resolveIncludedTokens(String includingUniformPath,
			String includedName) {
		Pair<String, String> content = resolveIncludeContent(
				includingUniformPath, includedName);

		return new IncludedTokens(content.getFirst(), ScannerUtils
				.getTokens(content.getSecond(), ELanguage.CPP, includedName));
	}

	/**
	 * Returns the resolved uniform path and content of an included file. The
	 * default implementation returns a pair of null and the empty string.
	 * 
	 * @param includingUniformPath
	 *            the uniform path of the file that includes this. May be null
	 *            if no information is available.
	 * @param includedName
	 *            the name of the file to be included.
	 * @return the pair of the name of the resolved uniform path of the include
	 *         file and the file's content. The resolved uniform path may be
	 *         null, if it is not available.
	 */
	protected Pair<String, String> resolveIncludeContent(
			String includingUniformPath, String includedName) {
		return new Pair<>(null, StringUtils.EMPTY_STRING);
	}

}
