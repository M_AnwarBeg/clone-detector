/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.preprocessor.c;

import org.conqat.lib.commons.assertion.CCSMAssert;

/**
 * Descriptor for a region in a preprocessor <code>if</code> construct. This
 * structure is used during parsing to store and communicate the regions
 * belonging to a single if directive.
 * 
 * @author $Author: streitel $
 * @version $Rev: 56430 $
 * @ConQAT.Rating GREEN Hash: DFEA51E4AFFCA1D13DF9FB202D7D2CC3
 */
/* package */class IfRegionDescriptor {

	/**
	 * The start index of the region. This is the index containing the
	 * #if/#ifdef/#elif/#else/...
	 */
	private final int startIndex;

	/**
	 * The end index of the region. This is the index containing the closing
	 * #elif/#else/#end.
	 */
	private int endIndex = 0;

	/**
	 * The condition. #ifdef and #ifndef are transparently converted to
	 * "defined()" and "!defined()", #else is converted to "true".
	 */
	private final String condition;

	/** Whether to include this region in the output. Default is false. */
	private boolean include = false;

	/** Constructor. */
	public IfRegionDescriptor(String condition, int startIndex) {
		this.condition = condition;
		this.startIndex = startIndex;
	}

	/** Closes a region by setting its end index. */
	void closeRegion(int endIndex) {
		CCSMAssert.isTrue(this.endIndex == 0, "May not close region twice!");
		CCSMAssert.isTrue(endIndex > startIndex, "Invalid end index!");
		this.endIndex = endIndex;
	}

	/**
	 * @see #include
	 */
	public void setInclude(boolean include) {
		this.include = include;
	}

	/**
	 * @see #condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @see #startIndex
	 */
	public int getStartIndex() {
		return startIndex;
	}

	/**
	 * @see #endIndex
	 */
	public int getEndIndex() {
		return endIndex;
	}

	/**
	 * @see #include
	 */
	public boolean isInclude() {
		return include;
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "[" + startIndex + "-" + endIndex + "]: " + condition;
	}
}