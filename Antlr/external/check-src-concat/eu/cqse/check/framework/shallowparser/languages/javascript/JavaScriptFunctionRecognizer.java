/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.javascript;

import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates;

/**
 * Recognizer that finds local functions and lanbdas (arrow notation) and
 * performs parsing starting from the function.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53317 $
 * @ConQAT.Rating GREEN Hash: EC4B61666678C16DEEC4C2AE6BD0D3A4
 */
/* package */class JavaScriptFunctionRecognizer extends
		RecognizerBase<EJavaScriptParserStates> {

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(
			ParserState<EJavaScriptParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		if (tokens.get(startOffset).getType() == FUNCTION) {
			return parserState.parse(EJavaScriptParserStates.ANY, tokens,
					startOffset);
		}

		// simple lambda
		if (TokenStreamUtils.tokenTypesAt(tokens, startOffset,
				ETokenType.IDENTIFIER, ETokenType.DOUBLE_ARROW)) {
			return parserState.parse(EJavaScriptParserStates.IN_LAMBDA, tokens,
					startOffset);
		}

		if (TokenStreamUtils.tokenTypesAt(tokens, startOffset,
				ETokenType.LPAREN)) {
			int closingPosition = TokenStreamUtils.findMatchingClosingToken(
					tokens, startOffset + 1, ETokenType.LPAREN,
					ETokenType.RPAREN);
			if (closingPosition == TokenStreamUtils.NOT_FOUND) {
				return NO_MATCH;
			}

			if (TokenStreamUtils.tokenTypesAt(tokens, closingPosition + 1,
					ETokenType.DOUBLE_ARROW)) {
				return parserState.parse(EJavaScriptParserStates.IN_LAMBDA,
						tokens, startOffset);
			}
		}

		return NO_MATCH;
	}
}