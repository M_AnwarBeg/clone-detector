/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.groovy;

import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.languages.base.JavaLikeAnonymousClassRecognizer;

/**
 * Recognizer for the start of closures and anonymous inner classes within
 * expressions for Groovy.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53318 $
 * @ConQAT.Rating YELLOW Hash: 28B2CB21AEB134D24442F0E73FAEEF16
 */
/* package */class GroovySubExpressionRecognizer
		extends JavaLikeAnonymousClassRecognizer<EGroovyShallowParserStates> {

	/** Constructor. */
	public GroovySubExpressionRecognizer() {
		super(EGroovyShallowParserStates.IN_EXPRESSION);
	}

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(
			ParserState<EGroovyShallowParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		if (startOffset < 0) {
			return NO_MATCH;
		}

		ETokenType tokenType = tokens.get(startOffset).getType();
		if (tokenType == LBRACE) {
			// We expect a closure.
			return parserState.parse(EGroovyShallowParserStates.IN_EXPRESSION,
					tokens, startOffset);
		}

		// No closure, so we expect an anonymous class.
		return super.matchesLocally(parserState, tokens, startOffset);
	}
}
