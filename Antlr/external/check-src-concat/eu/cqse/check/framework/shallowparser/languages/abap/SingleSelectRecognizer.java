/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.abap;

import static eu.cqse.check.framework.scanner.ETokenType.*;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.abap.AbapShallowParser.EAbapParserStates;

/**
 * Recognizer that matches single statements selects according to the rules
 * found <a href="http://help.sap.com/abapdocu_702/en/abapselect.htm">here</a>.
 * The recognizer should be called directly after finding the SELECT keyword.
 *
 * @author $Author: streitel $
 * @version $Rev: 54049 $
 * @ConQAT.Rating GREEN Hash: F6D66C889BEC3489010177BB64D41945
 */
/* package */class SingleSelectRecognizer
		extends RecognizerBase<EAbapParserStates> {

	/**
	 * Token types to be skipped from the select start to reach the result
	 * description.
	 */
	private static final EnumSet<ETokenType> SELECT_TO_RESULTS_SKIP_TOKENS = EnumSet
			.of(SINGLE, FOR, UPDATE, DISTINCT);

	/** Token types for aggregate functions. */
	private static final EnumSet<ETokenType> AGGREGATE_FUNCTIONS = EnumSet
			.of(MIN, MAX, SUM, AVG, COUNT);

	/** Token types that terminate the aggregate functions. */
	private static final EnumSet<ETokenType> AGGREGATE_TERMINATOR = EnumSet
			.of(FROM, INTO);

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EAbapParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		int dotOffset = startOffset;
		while (dotOffset < tokens.size()
				&& tokens.get(dotOffset).getType() != DOT) {
			dotOffset += 1;
		}

		// no match if closing dot was not found
		if (dotOffset >= tokens.size()) {
			return NO_MATCH;
		}

		int matchSingleSelect = dotOffset + 1;

		// the following is statements correspond directly to the rules in
		// http://help.sap.com/abapdocu_702/en/abapselect.htm, where a
		// result of matchSingleSelect means that no ENDSELECT is expected,
		// while a NO_MATCH indicates that an ENDSELECT is required
		if (!hasIntoAppendingTable(tokens, startOffset, dotOffset)) {
			if (isSingle(tokens, startOffset) || (hasOnlyAggregateFunctions(
					tokens, startOffset, dotOffset)
					&& !hasGroupBy(tokens, startOffset, dotOffset))) {
				return matchSingleSelect;
			}
			return NO_MATCH;
		}
		if (hasPackageSize(tokens, startOffset, dotOffset)) {
			return NO_MATCH;
		}
		return matchSingleSelect;
	}

	/** Returns whether the SINGLE keyword was found right at the start. */
	private static boolean isSingle(List<IToken> tokens, int startOffset) {
		return tokens.get(startOffset).getType() == SINGLE;
	}

	/** Returns whether this has the INTO|APPEND ... TABLE clause. */
	private static boolean hasIntoAppendingTable(List<IToken> tokens,
			int startOffset, int endOffset) {
		return TokenStreamUtils.containsAny(tokens, startOffset, endOffset,
				INTO, APPENDING)
				&& TokenStreamUtils.containsAny(tokens, startOffset,
						endOffset, TABLE);
	}

	/** Returns whether this has the PACKAGE SIZE clause. */
	private static boolean hasPackageSize(List<IToken> tokens,
			int startOffset,
			int endOffset) {
		return TokenStreamUtils.containsSequence(tokens, startOffset,
				endOffset, PACKAGE, SIZE);
	}

	/** Returns whether this has the GROUP BY clause. */
	private static boolean hasGroupBy(List<IToken> tokens, int startOffset,
			int endOffset) {
		return TokenStreamUtils.containsSequence(tokens, startOffset,
				endOffset, GROUP, BY);
	}

	/** Returns whether this only contains aggregate functions. */
	private static boolean hasOnlyAggregateFunctions(List<IToken> tokens,
			int startOffset, int endOffset) {

		while (startOffset < endOffset && SELECT_TO_RESULTS_SKIP_TOKENS
				.contains(tokens.get(startOffset).getType())) {
			startOffset += 1;
		}

		while (startOffset < endOffset && !AGGREGATE_TERMINATOR
				.contains(tokens.get(startOffset).getType())) {
			if (!AGGREGATE_FUNCTIONS
					.contains(tokens.get(startOffset).getType())) {
				// found non-aggregate
				return false;
			}
			startOffset = skipAggregate(tokens, startOffset + 1, endOffset);
		}
		return true;
	}

	/**
	 * Skips the remainder of an aggregate function, i.e. a block in
	 * parentheses and the optional AS part. Returns the new startOffset.
	 */
	private static int skipAggregate(List<IToken> tokens, int startOffset,
			int endOffset) {
		if (startOffset >= endOffset
				|| tokens.get(startOffset).getType() != LPAREN) {
			return startOffset;
		}
		int rparenPos = TokenStreamUtils.find(tokens, startOffset,
				endOffset, RPAREN);
		if (rparenPos == TokenStreamUtils.NOT_FOUND) {
			return startOffset;
		}

		startOffset = rparenPos + 1;

		// optionally skip AS part
		if (startOffset < endOffset
				&& tokens.get(startOffset).getType() == AS) {
			startOffset += 2;
		}

		return startOffset;
	}
}