/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.LT;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * A recognizer that skips the angle brackets of a template. A simple skip
 * nested recognizer cannot be used, because of constructs like
 * "std::vector&lt;SomeType&lt;(1&gt;2)&gt;&gt; x".
 * 
 * @author $Author: kupka $
 * @version $Rev: 56397 $
 * @ConQAT.Rating YELLOW Hash: 0548B882023EE10B8872ECFA19A71D8D
 */
public class CppSkipTemplateSpecificationRecognizer
		extends RecognizerBase<EGenericParserStates> {

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EGenericParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		// there must be at least two tokens in order to match, namely opening
		// and closing angle bracket
		if (startOffset >= tokens.size() - 1
				|| tokens.get(startOffset).getType() != LT) {
			return NO_MATCH;
		}

		int angleDepth = 1;
		int parenDepth = 0;

		for (int i = startOffset + 1; i < tokens.size(); i++) {
			switch (tokens.get(i).getType()) {
			case LPAREN:
				parenDepth++;
				break;
			case RPAREN:
				parenDepth--;
				break;
			case LT:
				if (parenDepth <= 0) {
					angleDepth++;
				}
				break;
			case GT:
				if (parenDepth <= 0) {
					angleDepth--;
					if (angleDepth == 0) {
						return i + 1;
					}
				}
				break;
			default:
				break;
			}
		}
		return NO_MATCH;
	}
}
