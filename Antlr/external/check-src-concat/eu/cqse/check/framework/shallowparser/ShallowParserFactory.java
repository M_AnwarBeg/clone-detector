/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser;

import java.util.EnumMap;
import java.util.Map;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.languages.abap.AbapShallowParser;
import eu.cqse.check.framework.shallowparser.languages.ada.AdaShallowParser;
import eu.cqse.check.framework.shallowparser.languages.cpp.CppShallowParser;
import eu.cqse.check.framework.shallowparser.languages.cs.CsShallowParser;
import eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser;
import eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser;
import eu.cqse.check.framework.shallowparser.languages.groovy.GroovyShallowParser;
import eu.cqse.check.framework.shallowparser.languages.hanasqlscript.HanaSQLScriptShallowParser;
import eu.cqse.check.framework.shallowparser.languages.iec61131.Iec61131ShallowParser;
import eu.cqse.check.framework.shallowparser.languages.java.JavaShallowParser;
import eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser;
import eu.cqse.check.framework.shallowparser.languages.magik.MagikShallowParser;
import eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser;
import eu.cqse.check.framework.shallowparser.languages.ocaml.OcamlShallowParser;
import eu.cqse.check.framework.shallowparser.languages.php.PhpShallowParser;
import eu.cqse.check.framework.shallowparser.languages.plsql.PlsqlShallowParser;
import eu.cqse.check.framework.shallowparser.languages.python.PythonShallowParser;
import eu.cqse.check.framework.shallowparser.languages.ruby.RubyShallowParser;
import eu.cqse.check.framework.shallowparser.languages.tsql.TsqlShallowParser;
import eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser;

/**
 * Factory class for creation of shallow parsers and utility methods.
 * 
 * @author $Author: ezaga $
 * @version $Rev: 57390 $
 * @ConQAT.Rating YELLOW Hash: D76EF6D176C93248C6D21B78F13F8562
 */
public class ShallowParserFactory {

	/** The map of parsers (initialized lazily). */
	private static Map<ELanguage, IShallowParser> parsers;

	static {
		parsers = new EnumMap<ELanguage, IShallowParser>(ELanguage.class);
		parsers.put(ELanguage.JAVA, new JavaShallowParser());
		parsers.put(ELanguage.ADA, new AdaShallowParser());
		parsers.put(ELanguage.CS, new CsShallowParser());
		parsers.put(ELanguage.CPP, new CppShallowParser());
		parsers.put(ELanguage.PLSQL, new PlsqlShallowParser());
		parsers.put(ELanguage.ABAP, new AbapShallowParser());
		parsers.put(ELanguage.JAVASCRIPT, new JavaScriptShallowParser());
		parsers.put(ELanguage.RUBY, new RubyShallowParser());
		parsers.put(ELanguage.PYTHON, new PythonShallowParser());
		parsers.put(ELanguage.MAGIK, new MagikShallowParser());
		parsers.put(ELanguage.DELPHI, new DelphiShallowParser());
		parsers.put(ELanguage.MATLAB, new MatlabShallowParser());
		parsers.put(ELanguage.FORTRAN, new FortranShallowParser());
		parsers.put(ELanguage.IEC61131, new Iec61131ShallowParser());
		parsers.put(ELanguage.XTEND, new XtendShallowParser());
		parsers.put(ELanguage.OCAML, new OcamlShallowParser());
		parsers.put(ELanguage.TSQL, new TsqlShallowParser());
		parsers.put(ELanguage.GROOVY, new GroovyShallowParser());
		parsers.put(ELanguage.PHP, new PhpShallowParser());
		parsers.put(ELanguage.SQLSCRIPT, new HanaSQLScriptShallowParser());
	}

	/**
	 * Returns a new parser for the given language.
	 * <p>
	 * While we call this method "create" for consistency with other factories,
	 * the parsers are actually created only once and then returned over and
	 * over again. The reason is that parser creation may be expensive,
	 * especially when many very small code fragments are to be parsed. Reusing
	 * parsers is possible as the parsers no not hold state of a specific parse
	 * and even can be used concurrently in multiple threads.
	 * 
	 * @throws ShallowParserException
	 *             if the language is not (yet) supported by our framework.
	 */
	public static IShallowParser createParser(ELanguage language)
			throws ShallowParserException {
		IShallowParser parser = parsers.get(language);
		if (parser == null) {
			throw new ShallowParserException("Shallow parsing for language "
					+ language + " not yet supported!");
		}
		return parser;
	}

	/**
	 * Returns whether the given language is supported by the parser factory.
	 */
	public static boolean supportsLanguage(ELanguage language) {
		return parsers.containsKey(language);
	}
}
