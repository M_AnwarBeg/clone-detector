/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cs;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Recognizer for array brackets as part of types, also dealing with multi-dim
 * arrays, e.g. [], [,,] etc.
 * 
 * 
 * @author $Author: streitel $
 * @version $Rev: 53626 $
 * @ConQAT.Rating GREEN Hash: D548AEF232D2B1A9B1AB16E25D234B0A
 */
public class ArrayBracketsRecognizer extends
		RecognizerBase<EGenericParserStates> {
	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EGenericParserStates> parserState,
			List<IToken> tokens, int startOffset) {

		if (tokens.get(startOffset).getType() != ETokenType.LBRACK) {
			return NO_MATCH;
		}
		for (int i = startOffset + 1; i < tokens.size(); i++) {
			ETokenType tokenType = tokens.get(i).getType();
			if (tokenType == ETokenType.RBRACK) {
				return i + 1;
			} else if (tokenType != ETokenType.COMMA) {
				return NO_MATCH;
			}
		}
		return NO_MATCH;
	}
}