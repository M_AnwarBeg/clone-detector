/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.base;

import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Base class for recognizing statements in a language whose statements are
 * primarily line-based. It also assumes that a semicolon may always be used to
 * terminate a statement.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53317 $
 * @ConQAT.Rating GREEN Hash: CA64EC05F6533BF76D74C0CDB6D2A090
 */
public abstract class LineBasedStatementRecognizerBase<STATE extends Enum<STATE>>
		extends RecognizerBase<STATE> {

	/** Matched tokens for nesting in complex statements. */
	protected final static Map<ETokenType, ETokenType> NESTING_MATCH = new EnumMap<ETokenType, ETokenType>(
			ETokenType.class);

	static {
		NESTING_MATCH.put(LPAREN, RPAREN);
		NESTING_MATCH.put(LBRACK, RBRACK);
		NESTING_MATCH.put(LBRACE, RBRACE);
	}

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<STATE> parserState,
			List<IToken> tokens, int startOffset) {
		IToken lastToken = null;
		Stack<ETokenType> expectedClosing = new Stack<ETokenType>();

		// create a node here, so we can append function nodes
		parserState.setNode(new ShallowEntity(getEntityType(),
				getEntitySubtypeName(), tokens.get(startOffset).getText(),
				tokens, startOffset));

		while (true) {
			if (startOffset >= tokens.size()) {
				return startOffset;
			}

			IToken token = tokens.get(startOffset);
			ETokenType tokenType = token.getType();

			if (!expectedClosing.isEmpty()
					&& tokenType == expectedClosing.peek()) {
				expectedClosing.pop();
			} else if (expectedClosing.isEmpty() && tokenType == SEMICOLON) {
				return startOffset + 1;
			} else if (expectedClosing.isEmpty()
					&& startsNewStatement(token, lastToken)) {
				return startOffset;
			} else if (tokenStartsSubParse(tokenType, tokens, startOffset)) {
				int next = parserState.parse(getSubParseState(), tokens,
						startOffset);
				if (next == NO_MATCH) {
					return NO_MATCH;
				}
				startOffset = next;
				lastToken = tokens.get(startOffset - 1);
				continue;
			} else if (NESTING_MATCH.containsKey(tokenType)) {
				expectedClosing.push(NESTING_MATCH.get(tokenType));
			}

			lastToken = token;
			startOffset += 1;
		}
	}

	/**
	 * @return The entity type that is to be created when matching.
	 */
	protected EShallowEntityType getEntityType() {
		return EShallowEntityType.STATEMENT;
	}

	/**
	 * @return The entity subtype name to be used when creating a node.
	 */
	protected String getEntitySubtypeName() {
		return SubTypeNames.SIMPLE_STATEMENT;
	}

	/** Returns the state to be used for a sub parse. */
	protected abstract STATE getSubParseState();

	/**
	 * Returns true if the token signals to start a sub parse (e.g. embedded
	 * classes, functions, etc.).
	 * 
	 * @param tokenType
	 *            the type of the current token.
	 * @param tokens
	 *            the entire token stream.
	 * @param offset
	 *            the offset of the current token in the token stream.
	 */
	protected abstract boolean tokenStartsSubParse(ETokenType tokenType,
			List<IToken> tokens, int offset);

	/** Returns true if the given token starts a new statement. */
	protected abstract boolean startsNewStatement(IToken token, IToken lastToken);
}