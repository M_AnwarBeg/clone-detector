/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.tsql;

//TODO (DP): Leaving this here, as it comes in handy when working on the parser. Will remove it later.
//import static eu.cqse.check.framework.scanner.ETokenType.*;
//import static eu.cqse.check.framework.shallowparser.languages.tsql.TsqlShallowParser.ETsqlParserStates.*;
//import static eu.cqse.check.framework.shallowparser.framework.EShallowEntityType.*;
//import static eu.cqse.check.framework.scanner.ETokenType.ETokenClass.*;
import static eu.cqse.check.framework.scanner.ETokenType.ALL;
import static eu.cqse.check.framework.scanner.ETokenType.BEGIN;
import static eu.cqse.check.framework.scanner.ETokenType.BY;
import static eu.cqse.check.framework.scanner.ETokenType.CREATE;
import static eu.cqse.check.framework.scanner.ETokenType.DISTINCT;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.END;
import static eu.cqse.check.framework.scanner.ETokenType.EOF;
import static eu.cqse.check.framework.scanner.ETokenType.EOL;
import static eu.cqse.check.framework.scanner.ETokenType.EXCEPT;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FROM;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.GROUP;
import static eu.cqse.check.framework.scanner.ETokenType.HAVING;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.INTERSECT;
import static eu.cqse.check.framework.scanner.ETokenType.INTO;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.OPTION;
import static eu.cqse.check.framework.scanner.ETokenType.ORDER;
import static eu.cqse.check.framework.scanner.ETokenType.PERCENT;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.RETURNS;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SELECT;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.TABLE;
import static eu.cqse.check.framework.scanner.ETokenType.TOP;
import static eu.cqse.check.framework.scanner.ETokenType.UNION;
import static eu.cqse.check.framework.scanner.ETokenType.WHERE;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WITH;
import static eu.cqse.check.framework.scanner.ETokenType.ETokenClass.KEYWORD;
import static eu.cqse.check.framework.shallowparser.framework.EShallowEntityType.META;
import static eu.cqse.check.framework.shallowparser.framework.EShallowEntityType.STATEMENT;
import static eu.cqse.check.framework.shallowparser.languages.tsql.TsqlShallowParser.ETsqlParserStates.IN_BLOCK;
import static eu.cqse.check.framework.shallowparser.languages.tsql.TsqlShallowParser.ETsqlParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.tsql.TsqlShallowParser.ETsqlParserStates.TOP_LEVEL;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;

/**
 * 
 * @author $Author: pagano $
 * @version $Rev: 55066 $
 * @ConQAT.Rating YELLOW Hash: 282EB5938E3DC7B128ED879FF88DDD70
 */
public class TsqlShallowParser
		extends ShallowParserBase<TsqlShallowParser.ETsqlParserStates> {

	/** The states used in this parser. */
	public static enum ETsqlParserStates {

		/** Top-level parsing state. */
		TOP_LEVEL,

		IN_METHOD, IN_BLOCK,

	}

	private static final EnumSet<ETokenType> SELECT_STATEMENT_KEYWORDS = EnumSet
			.of(SELECT, WITH, ORDER, FOR, OPTION, UNION, ALL, EXCEPT, INTERSECT,
					DISTINCT, TOP, PERCENT, INTO, FROM, WHERE, GROUP, BY,
					HAVING);

	private static final EnumSet<ETokenType> NO_SELECT_STATEMENT_KEYWORDS = EnumSet
			.copyOf(ETokenType.KEYWORDS);

	{
		NO_SELECT_STATEMENT_KEYWORDS.removeAll(SELECT_STATEMENT_KEYWORDS);
	}

	/**
	 * @param stateClass
	 * @param initialState
	 */
	public TsqlShallowParser() {
		super(ETsqlParserStates.class, ETsqlParserStates.TOP_LEVEL);

		createMethodRules();
		createStatementRules();
	}

	/** Creates parser rules for statements. */
	private void createStatementRules() {

		createBlockStatementRule(IF);
		createBlockStatementRule(ELSE);
		createBlockStatementRule(WHILE);

		inAnyState().sequence(BEGIN).createNode(STATEMENT, 0, new Region(1, -1))
				.parseUntil(IN_BLOCK).sequence(END).endNode();

		inAnyState().sequence(SELECT).createNode(STATEMENT, 0)
				.skipBefore(NO_SELECT_STATEMENT_KEYWORDS).endNode();

		inAnyState().markStart().sequence(KEYWORD).skipBefore(KEYWORD)
				.createNode(STATEMENT, 0).endNode();

		// TODO (DP): EOF is not recognized, so that the node is always
		// incomplete. Should be STATEMENT, not META
		inAnyState().sequence(KEYWORD).createNode(META, 0).skipBefore(EOF)
				.endNode();

	}

	/**
	 * @param blockStatementType
	 */
	private void createBlockStatementRule(ETokenType blockStatementType) {
		inAnyState().sequence(blockStatementType).skipNested(LPAREN, RPAREN)
				.skipBefore(KEYWORD).sequence(BEGIN).createNode(STATEMENT, 0)
				.parseUntil(IN_BLOCK).sequence(END).endNode();
	}

	/** Creates parser rules for functions and procedures. */
	private void createMethodRules() {
		// TODO (DP): We might need to create an Identifier set, which copes
		// with [FUNCTION] or [My cool table name]

		RecognizerBase<ETsqlParserStates> methodStart = inState(TOP_LEVEL)
				.sequence(CREATE).markStart().sequence(FUNCTION)
				.createNode(EShallowEntityType.METHOD, 0).skipTo(RETURNS);

		// T-SQL Scalar Function Syntax
		methodStart.skipTo(BEGIN).parseUntil(IN_METHOD).sequence(END).endNode();

		// T-SQL Inline Table-Valued Function Syntax
		methodStart.sequence(TABLE).skipTo(RETURN).parseUntil(IN_METHOD)
				.sequence(END).endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isFilteredToken(IToken token, IToken previousToken) {
		return super.isFilteredToken(token, previousToken)
				|| token.getType() == EOL || token.getType() == SEMICOLON;
	}

}
