/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.xtend;

import static eu.cqse.check.framework.scanner.ETokenType.ABSTRACT;
import static eu.cqse.check.framework.scanner.ETokenType.ANNOTATION;
import static eu.cqse.check.framework.scanner.ETokenType.AT_OPERATOR;
import static eu.cqse.check.framework.scanner.ETokenType.BOOLEAN;
import static eu.cqse.check.framework.scanner.ETokenType.BYTE;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CATCH;
import static eu.cqse.check.framework.scanner.ETokenType.CHAR;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CREATE;
import static eu.cqse.check.framework.scanner.ETokenType.DEF;
import static eu.cqse.check.framework.scanner.ETokenType.DEFAULT;
import static eu.cqse.check.framework.scanner.ETokenType.DISPATCH;
import static eu.cqse.check.framework.scanner.ETokenType.DO;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE_ARROW;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ENUM;
import static eu.cqse.check.framework.scanner.ETokenType.EXTENSION;
import static eu.cqse.check.framework.scanner.ETokenType.FINAL;
import static eu.cqse.check.framework.scanner.ETokenType.FINALLY;
import static eu.cqse.check.framework.scanner.ETokenType.FLOAT;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.GT;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPORT;
import static eu.cqse.check.framework.scanner.ETokenType.INT;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LONG;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.LT;
import static eu.cqse.check.framework.scanner.ETokenType.MULT;
import static eu.cqse.check.framework.scanner.ETokenType.NATIVE;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.OR;
import static eu.cqse.check.framework.scanner.ETokenType.OVERRIDE;
import static eu.cqse.check.framework.scanner.ETokenType.PACKAGE;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SHORT;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.STRICTFP;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.SYNCHRONIZED;
import static eu.cqse.check.framework.scanner.ETokenType.TEMPLATE_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.THROWS;
import static eu.cqse.check.framework.scanner.ETokenType.TRANSIENT;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.VAL;
import static eu.cqse.check.framework.scanner.ETokenType.VAR;
import static eu.cqse.check.framework.scanner.ETokenType.VOID;
import static eu.cqse.check.framework.scanner.ETokenType.VOLATILE;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_ENUM;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_LAMBDA;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_METHOD_WITH_TEMPLATE;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_SINGLE_STATEMENT;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_TOP_LEVEL;
import static eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState.IN_TYPE;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState;

/**
 * Shallow parser for Xtend.
 * <p>
 * Same features as the Java shallow parser:
 * <ul>
 * <li>The parser recognizes types (classes, enums, interfaces, annotations),
 * methods and attributes, and individual statements.</li>
 * <li>It recognizes the nesting of statements (e.g. in loops), but does not
 * parse into the statements. For example, it recognizes an if-statement and
 * provides the list of sub-statements, but does not provide direct access to
 * the if-condition.</li>
 * <li>Import and package statements are parsed as meta information.</li>
 * <li>Annotations are recognized as meta information, but only annotations at
 * types and methods. Annotations at parameters are not parsed, as the parser
 * does not parse into the parameter list of methods.</li>
 * <li>The parser does not recognize anonymous classes. These are treated as a
 * single long statement or attribute. Inner classes, however, are parsed
 * correctly.</li>
 * <li>The parser recognizes try, switch and if assignments to fields.</li>
 * <li>The parser can deal with multiple classes in a single file.</li>
 * </ul>
 *
 * @author $Author: baumeister $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: 10FE0B7EE830B7B48F3BAA3161D99732
 */
public class XtendShallowParser
		extends ShallowParserBase<EXtendShallowParserState> {

	/** The states of the xtend parser */
	public static enum EXtendShallowParserState {
		/** Top level state (e.g. at the beginning of the file). */
		IN_TOP_LEVEL,

		/** State inside a type (class, interface or annotation) definition. */
		IN_TYPE,

		/** State inside a method definition. */
		IN_METHOD,

		/** State inside a method definition with template literal. */
		IN_METHOD_WITH_TEMPLATE,

		/** State inside a region, that accepts only a single statement. */
		IN_SINGLE_STATEMENT,

		/** State inside an enum body. */
		IN_ENUM,

		/**
		 * State to match a lambda expression, the content of the lambda
		 * expression however is treated as IN_METHOD
		 */
		IN_LAMBDA
	}

	/** All modifiers that may be part of a type definition. */
	private static final EnumSet<ETokenType> TYPE_MODIFIERS = EnumSet.of(PUBLIC,
			PRIVATE, PROTECTED, PACKAGE, STRICTFP, ABSTRACT, STATIC);

	/** All possible types of a field. */
	private static final EnumSet<ETokenType> FIELD_TYPES = EnumSet.of(BOOLEAN,
			BYTE, CHAR, DOUBLE, FLOAT, INT, LONG, SHORT, VOID);

	/** All possible modifiers of a field. */
	private static final EnumSet<ETokenType> FIELD_MODIFIERS = EnumSet.of(
			PUBLIC, PRIVATE, PROTECTED, PACKAGE, STATIC, FINAL, EXTENSION,
			VOLATILE, TRANSIENT);

	/** All possible modifiers of a method. */
	private static final EnumSet<ETokenType> METHOD_MODIFIERS = EnumSet.of(
			PUBLIC, PRIVATE, PROTECTED, PACKAGE, STATIC, ABSTRACT, DISPATCH,
			FINAL, STRICTFP, NATIVE, SYNCHRONIZED);

	/** Constructor */
	public XtendShallowParser() {
		super(EXtendShallowParserState.class, IN_TOP_LEVEL);
		createMetaRules();
		createTypeDefRules();

		createEnumBodyRule();

		createMethodDefRule();
		createConstructorDefRule();

		createLoopRules();
		createSwitchCaseRules();
		createContinuationRules();

		createAnonymousBlock();
		createFieldRules();
		createSingleStatementRule();

		createLambdaExpressionRule();

		createEmptyStatementRule();
	}

	/** Creates the meta rules, such as package, annotation and import. */
	private void createMetaRules() {
		createImportRule();
		createPackageRule();
		createAnnotationRule();
	}

	/** Creates the rule for imports. */
	private void createImportRule() {
		inState(IN_TOP_LEVEL).sequence(IMPORT).optional(STATIC)
				.optional(EXTENSION).markStart().sequence(IDENTIFIER)
				.repeated(DOT, EnumSet.of(IDENTIFIER, MULT))
				.createNode(EShallowEntityType.META, SubTypeNames.IMPORT,
						new Region(0, -1))
				.optional(SEMICOLON).endNode();
	}

	/** Creates the rule for package declarations. */
	private void createPackageRule() {
		inState(IN_TOP_LEVEL).sequence(PACKAGE).sequence(IDENTIFIER)
				.repeated(DOT, IDENTIFIER)
				.createNode(EShallowEntityType.META, 0, new Region(1, -1))
				.optional(SEMICOLON).endNode();
	}

	/** Creates the rule for annotations. */
	private void createAnnotationRule() {
		inState(IN_TOP_LEVEL, IN_TYPE, IN_METHOD)
				.sequence(AT_OPERATOR, IDENTIFIER)
				.createNode(EShallowEntityType.META, SubTypeNames.ANNOTATION,
						-1)
				.skipNested(LPAREN, RPAREN).endNode();
	}

	/** Creates all rules that recognize type definitions. */
	private void createTypeDefRules() {
		createTypeDefRule(ENUM, SubTypeNames.ENUM, IN_ENUM, IN_TOP_LEVEL,
				IN_TYPE);
		createTypeDefRule(INTERFACE, SubTypeNames.INTERFACE, IN_TYPE,
				IN_TOP_LEVEL, IN_TYPE);
		createTypeDefRule(ANNOTATION, SubTypeNames.ANNOTATION, IN_TYPE,
				IN_TOP_LEVEL, IN_TYPE);
		createTypeDefRule(CLASS, SubTypeNames.CLASS, IN_TYPE, IN_TOP_LEVEL,
				IN_TYPE, IN_METHOD);
	}

	/** Creates a rule that matches a specific type definition. */
	private void createTypeDefRule(ETokenType type, String subtypeName,
			EXtendShallowParserState substate,
			EXtendShallowParserState... states) {
		inState(states).repeated(TYPE_MODIFIERS).sequence(type)
				.sequence(IDENTIFIER)
				.createNode(EShallowEntityType.TYPE, subtypeName, -1)
				.skipTo(LBRACE).parseUntil(substate).sequence(RBRACE).endNode();
	}

	/** Recognizes an enumeration inside of an enum body. */
	private void createEnumBodyRule() {
		inState(IN_ENUM).sequence(IDENTIFIER).sequenceBefore(COMMA)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ENUM_LITERAL, 0)
				.endNode();

		inState(IN_ENUM).sequence(IDENTIFIER).sequenceBefore(RBRACE)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ENUM_LITERAL, 0)
				.endNode();
	}

	/** Creates rule that recognizes method definitions. */
	private void createMethodDefRule() {
		RecognizerBase<EXtendShallowParserState> baseRecognizer = inState(
				IN_TYPE).repeated(METHOD_MODIFIERS)
						.sequence(EnumSet.of(DEF, OVERRIDE))
						.repeated(METHOD_MODIFIERS).optional(FIELD_TYPES)
						.repeated(METHOD_MODIFIERS);

		RecognizerBase<EXtendShallowParserState> baseWithSimpleCreate = baseRecognizer
				.optional(IDENTIFIER).sequence(CREATE).skipTo(NEW)
				.sequence(IDENTIFIER).skipNested(LPAREN, RPAREN);

		appendMethodNode(baseWithSimpleCreate);
		appendMethodNode(baseRecognizer);

		inState(IN_METHOD_WITH_TEMPLATE).sequence(TEMPLATE_LITERAL)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, -1)
				.endNode();
	}

	/** Appends the node for a method. */
	private static void appendMethodNode(
			RecognizerBase<EXtendShallowParserState> baseRecognizer) {
		RecognizerBase<EXtendShallowParserState> newBaseRecognizer = baseRecognizer
				.skipBefore(LPAREN)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.METHOD, -1)
				.skipNested(LPAREN, RPAREN);

		// Method header only with a "throws" clause (in interface declaration)
		RecognizerBase<EXtendShallowParserState> withThrowsException = newBaseRecognizer
				.sequence(THROWS)
				.subRecognizer(new XtendSkipToEndOfStatementRecognizer(), 0, 1);

		appendRecognizeMethodBody(newBaseRecognizer);
		appendRecognizeMethodBody(withThrowsException);

		withThrowsException.endNode(); // abstract method def
		newBaseRecognizer.endNode(); // abstract method def
	}

	/**
	 * Appends the rule to recognize the method body, which is either a
	 * statement block or a template.
	 */
	private static void appendRecognizeMethodBody(
			RecognizerBase<EXtendShallowParserState> baseRecognizer) {
		baseRecognizer.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();
		baseRecognizer.sequenceBefore(TEMPLATE_LITERAL)
				.parseOnce(IN_METHOD_WITH_TEMPLATE).endNode();
	}

	/** Creates rule for constructors. */
	private void createConstructorDefRule() {
		inState(IN_TYPE).sequence(NEW)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.CONSTRUCTOR)
				.skipTo(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();
	}

	/** Creates all rules that correspond to loops. */
	private void createLoopRules() {
		createForRule();
		createDoWhileRule();
		createWhileRule();
	}

	/** Creates rule for for statements. */
	private void createForRule() {
		RecognizerBase<EXtendShallowParserState> baseRecognizer = inState(
				IN_METHOD).sequence(FOR)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.FOR)
						.skipNested(LPAREN, RPAREN);

		appendEndNodeWithBlockOrSingleStatement(baseRecognizer);
	}

	/** Creates rule for do while statements. */
	private void createDoWhileRule() {
		RecognizerBase<EXtendShallowParserState> baseRecognizer = inState(
				IN_METHOD).sequence(DO).createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.DO);

		appendWhileRuleInDoWhile(baseRecognizer.sequence(LBRACE)
				.parseUntil(IN_METHOD).sequence(RBRACE));
		appendWhileRuleInDoWhile(baseRecognizer.parseOnce(IN_SINGLE_STATEMENT));
	}

	/** Appends the while statement to the rule after the do block. */
	private static void appendWhileRuleInDoWhile(
			RecognizerBase<EXtendShallowParserState> baseRecognizer) {
		baseRecognizer.sequence(WHILE).skipNested(LPAREN, RPAREN).endNode();
	}

	/** Creates rule for while statement. */
	private void createWhileRule() {
		RecognizerBase<EXtendShallowParserState> baseRecognizer = inState(
				IN_METHOD).sequence(WHILE)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.WHILE)
						.skipNested(LPAREN, RPAREN);
		appendEndNodeWithBlockOrSingleStatement(baseRecognizer);
	}

	/** Creates rule that matches switch case statements. */
	private void createSwitchCaseRules() {
		inState(IN_METHOD).sequence(SWITCH)
				.createNode(EShallowEntityType.STATEMENT, SubTypeNames.SWITCH)
				.skipTo(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();
		createCaseRules();
	}

	/** Creates rules that apply within a switch block. */
	private void createCaseRules() {
		inState(IN_METHOD).sequence(DEFAULT).sequence(COLON)
				.createNode(EShallowEntityType.META, SubTypeNames.DEFAULT)
				.endNode();

		// type guard (with optional case)
		inState(IN_METHOD).repeated(IDENTIFIER, DOT).sequence(IDENTIFIER)
				.skipNested(LT, GT)
				.sequenceBefore(EnumSet.of(COMMA, COLON, CASE))
				.skipTo(EnumSet.of(COMMA, COLON))
				.createNode(EShallowEntityType.META, SubTypeNames.CASE)
				.endNode();

		// case only
		inState(IN_METHOD).sequence(CASE)
				.skipToWithNesting(EnumSet.of(COMMA, COLON), ETokenType.LPAREN,
						ETokenType.RPAREN)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE)
				.endNode();
	}

	/**
	 * Creates rule for statements with continuation like if-else statements and
	 * try-catch-finally.
	 */
	private void createContinuationRules() {
		createRuleWithContinuation(IF, ELSE, ELSE, IF);
		createRuleWithContinuation(TRY, FINALLY, CATCH);
	}

	/**
	 * Create one rule for a statement with continuation.
	 */
	private void createRuleWithContinuation(ETokenType first, ETokenType last,
			ETokenType... middle) {
		RecognizerBase<EXtendShallowParserState> baseMid = inState(IN_METHOD)
				.sequence((Object[]) middle)
				.createNode(EShallowEntityType.STATEMENT, new Region(0, -1))
				.skipNested(LPAREN, RPAREN);
		appendSingleStatementAndBlockForContinuationNode(baseMid, last, middle);

		RecognizerBase<EXtendShallowParserState> baseFirst = inState(IN_METHOD)
				.sequence(EnumSet.of(first, last))
				.createNode(EShallowEntityType.STATEMENT, new Region(0, -1))
				.skipNested(LPAREN, RPAREN);
		appendSingleStatementAndBlockForContinuationNode(baseFirst, last,
				middle);

	}

	/**
	 * Appends single statement and blocks of statements rule to continuation
	 * constructs.
	 */
	private static void appendSingleStatementAndBlockForContinuationNode(
			RecognizerBase<EXtendShallowParserState> baseRecognizer,
			ETokenType last, ETokenType... middle) {
		RecognizerBase<EXtendShallowParserState> baseBlock = baseRecognizer
				.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE);
		RecognizerBase<EXtendShallowParserState> baseSingleStatement = baseRecognizer
				.parseOnce(IN_SINGLE_STATEMENT);
		appendEndOfNodeInContinuationConstructs(baseBlock, last, middle);
		appendEndOfNodeInContinuationConstructs(baseSingleStatement, last,
				middle);
	}

	/** Appends the rules for the end of a node in continuation constructs. */
	private static void appendEndOfNodeInContinuationConstructs(
			RecognizerBase<EXtendShallowParserState> baseRecognizer,
			ETokenType last, ETokenType... middle) {
		baseRecognizer.sequenceBefore(last).endNodeWithContinuation();
		baseRecognizer.sequenceBefore((Object[]) middle)
				.endNodeWithContinuation();
		baseRecognizer.optional(ETokenType.SEMICOLON).endNode();
	}

	/** Rule for anonymous blocks. */
	private void createAnonymousBlock() {
		inAnyState().sequence(LBRACE)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.ANONYMOUS_BLOCK)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();
	}

	/** Creates all rules that correspond to field definitions. */
	private void createFieldRules() {
		createFieldRule(EShallowEntityType.ATTRIBUTE, SubTypeNames.ATTRIBUTE,
				IN_TYPE);
		createFieldRule(EShallowEntityType.STATEMENT,
				SubTypeNames.LOCAL_VARIABLE, IN_METHOD);

		// Addition to attribute, as local variables cannot be defined via
		// "IDENTIFIER IDENTIFIER"
		RecognizerBase<EXtendShallowParserState> baseWithIdentifier = inState(
				IN_TYPE).repeated(FIELD_MODIFIERS).sequence(IDENTIFIER)
						.skipNested(LT, GT).repeated(LBRACK, RBRACK)
						.sequence(IDENTIFIER);

		appendFieldNodeAndSkipToEnd(baseWithIdentifier,
				EShallowEntityType.ATTRIBUTE, SubTypeNames.ATTRIBUTE);
	}

	/** Create rule for attributes. */
	private void createFieldRule(EShallowEntityType type, String subtype,
			EXtendShallowParserState... states) {

		// To declare an attribute or a variable it must include either
		// val, var or an actual type
		RecognizerBase<EXtendShallowParserState> baseWithValOrVar = inState(
				states).repeated(FIELD_MODIFIERS).sequence(EnumSet.of(VAL, VAR))
						.repeated(FIELD_MODIFIERS).optional(FIELD_TYPES)
						.repeated(LBRACK, RBRACK).sequence(IDENTIFIER)
						.skipNested(LT, GT).repeated(LBRACK, RBRACK)
						.optional(IDENTIFIER);

		RecognizerBase<EXtendShallowParserState> baseWithFieldType = inState(
				states).repeated(FIELD_MODIFIERS).sequence(FIELD_TYPES)
						.repeated(LBRACK, RBRACK).sequence(IDENTIFIER);

		// Additional rule because constructs like this are valid:
		// var (String)=>String stringToStringFunction = [ toUpperCase ]
		RecognizerBase<EXtendShallowParserState> baseWithLambda = inState(
				states).repeated(FIELD_MODIFIERS).sequence(EnumSet.of(VAL, VAR))
						.skipNested(LPAREN, RPAREN).sequence(DOUBLE_ARROW)
						.sequence(EnumSet.of(IDENTIFIER, DOUBLE, FLOAT, BYTE,
								SHORT, LONG, CHAR, INT, BOOLEAN))
						.sequence(IDENTIFIER);

		appendFieldNodeAndSkipToEnd(baseWithValOrVar, type, subtype);
		appendFieldNodeAndSkipToEnd(baseWithFieldType, type, subtype);
		appendFieldNodeAndSkipToEnd(baseWithLambda, type, subtype);
	}

	/**
	 * Creates a node of a given type and subtype and skips to the end of the
	 * statement, the name of the node is defined by the previous token.
	 */
	private static void appendFieldNodeAndSkipToEnd(
			RecognizerBase<EXtendShallowParserState> baseRecognizer,
			EShallowEntityType type, String subtype) {
		RecognizerBase<EXtendShallowParserState> alternative = baseRecognizer
				.createNode(type, subtype, -1);
		alternative.sequence(ETokenType.EQ)
				.subRecognizer(new XtendSkipToEndOfStatementRecognizer(), 0, 1)
				.endNode();
		alternative.optional(ETokenType.SEMICOLON).endNode();
	}

	/**
	 * Factory method for a recognizer that matches a single statement. Should
	 * match nearly everything, that is left.
	 */
	private void createSingleStatementRule() {
		RecognizerBase<EXtendShallowParserState> baseClasses = inState(
				IN_METHOD, IN_SINGLE_STATEMENT).sequence(
						EnumSet.of(ETokenClass.KEYWORD, ETokenClass.IDENTIFIER,
								ETokenClass.LITERAL, ETokenClass.OPERATOR));

		// this is especially needed, because a statement can start with
		// LPAREN.
		RecognizerBase<EXtendShallowParserState> baseLparen = inState(IN_METHOD,
				IN_SINGLE_STATEMENT).sequenceBefore(LPAREN);

		appendSingleStatementNode(baseClasses);
		appendSingleStatementNode(baseLparen);
	}

	/** Appends the node creation for a simple statement. */
	private static void appendSingleStatementNode(
			RecognizerBase<EXtendShallowParserState> baseRecognizer) {
		baseRecognizer
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0)
				.subRecognizer(new XtendSkipToEndOfStatementRecognizer(), 0, 1)
				.endNode();
	}

	/** Rule to recognize lambda expressions */
	private void createLambdaExpressionRule() {
		RecognizerBase<EXtendShallowParserState> alternative = inState(
				IN_LAMBDA).sequence(LBRACK).createNode(
						EShallowEntityType.METHOD,
						SubTypeNames.LAMBDA_EXPRESSION);
		alternative
				.skipBeforeWithNesting(EnumSet.of(RBRACK, OR), LBRACK, RBRACK)
				.sequence(OR).parseUntil(IN_METHOD).sequence(RBRACK).endNode();

		alternative.parseUntil(IN_METHOD).sequence(RBRACK).endNode();
	}

	/** Matches for single semicolons and adds an empty statement for them. */
	private void createEmptyStatementRule() {
		inAnyState().sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT, 0)
				.endNode();
	}

	/**
	 * Ends a rule with either multiple statements in a block or a single
	 * statement.
	 */
	private static void appendEndNodeWithBlockOrSingleStatement(
			RecognizerBase<EXtendShallowParserState> baseRecognizer) {
		baseRecognizer.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();
		baseRecognizer.parseOnce(IN_SINGLE_STATEMENT).endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isFilteredToken(IToken token, IToken previousToken) {
		return super.isFilteredToken(token, previousToken)
				|| token.getType() == ETokenType.EOL;
	}
}
