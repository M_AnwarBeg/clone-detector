/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.conqat.lib.commons.assertion.CCSMPre;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Iterator over a token list that keeps track of nesting.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57677 $
 * @ConQAT.Rating RED Hash: E5CE3A45F42B638D37BEED6349102467
 */
public class NestingAwareTokenIterator implements Iterator<IToken> {

	/** The tokens over which this iterator iterates. */
	private final List<IToken> tokens;

	/** The index of the next token that will be returned by the iterator. */
	private int position;

	/** The token types that open an new nesting level. */
	private final List<ETokenType> openingTypes;

	/** The token types that close a nesting level. */
	private final List<ETokenType> closingTypes;

	/**
	 * <code>true</code> if the last token returned by {@link #next()} was a
	 * token from {@link #closingTypes}. This information is used to report
	 * correct nesting depth for closing tokens.
	 */
	private boolean isClosingToken = false;

	/**
	 * Records the number of open nesting levels per entry in
	 * {@link #openingTypes}.
	 */
	private final int[] nestingDepths;

	/**
	 * Constructor.
	 * 
	 * @param tokens
	 *            the stream over which to iterate.
	 * @param startPosition
	 *            the index of the first token that should be returned by the
	 *            iterator.
	 * @param openingTypes
	 *            the list of tokens that open an new nesting level.
	 * @param closingTypes
	 *            the list of tokens that close a nesting level. Each entry must
	 *            correspond to the entry of openingTypes at the same position.
	 */
	public NestingAwareTokenIterator(List<IToken> tokens, int startPosition,
			List<ETokenType> openingTypes, List<ETokenType> closingTypes) {
		CCSMPre.isTrue(openingTypes.size() == closingTypes.size(),
				"Open and close tokens must have the same size");
		this.tokens = tokens;
		// TODO (LH) Naming: Please align position and startPosition
		this.position = startPosition;
		this.openingTypes = openingTypes;
		this.closingTypes = closingTypes;
		this.nestingDepths = new int[openingTypes.size()];
	}

	/** {@inheritDoc} */
	@Override
	public boolean hasNext() {
		return position < tokens.size();
	}

	/** {@inheritDoc} */
	@Override
	public IToken next() {
		if (position >= tokens.size()) {
			throw new NoSuchElementException("The iterator is exhausted");
		}
		IToken token = tokens.get(position);
		position++;
		updateNestingInfo(token);
		return token;
	}

	/**
	 * Updates the {@link #nestingDepths} array based on the type of the given
	 * token.
	 */
	private void updateNestingInfo(IToken token) {
		isClosingToken = false;
		ETokenType type = token.getType();
		int openIndex = openingTypes.indexOf(type);
		if (openIndex != -1) {
			nestingDepths[openIndex]++;
		}
		int closeIndex = closingTypes.indexOf(type);
		if (closeIndex != -1) {
			nestingDepths[closeIndex]--;
			isClosingToken = true;
		}
	}

	/**
	 * Returns <code>true</code> if the last token returned by {@link #next()}
	 * is a top-level token. Opening/closing tokens are never top-level. In case
	 * opening/closing tokens are not balanced, returns <code>true</code> for
	 * superfluous closing tokens and tokens thereafter.
	 */
	public boolean isTopLevel() {
		return getNestingDepth() <= 0;
	}

	/**
	 * Returns the depth of the current nesting. Opening/closing tokens are
	 * reported as the nesting level they introduce. In case opening/closing
	 * tokens are not balanced, returns negative numbers for superfluous closing
	 * tokens and tokens thereafter.
	 */
	public int getNestingDepth() {
		int sum = 0;
		for (int value : nestingDepths) {
			sum += value;
		}
		if (isClosingToken) {
			sum += 1;
		}
		return sum;
	}

	// TODO (LH) Align doc and method name, i.e. last vs. current
	/** Returns the index of the last token returned by {@link #next()}. */
	public int getCurrentIndex() {
		return position - 1;
	}

	/** {@inheritDoc} */
	@Override
	public void remove() {
		throw new UnsupportedOperationException(
				"This iterator does not support removal of elements");
	}
}
