/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.abap;

import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.ENDMODULE;
import static eu.cqse.check.framework.scanner.ETokenType.INPUT;
import static eu.cqse.check.framework.scanner.ETokenType.MODULE;
import static eu.cqse.check.framework.scanner.ETokenType.OUTPUT;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.abap.AbapShallowParser.EAbapParserStates;

/**
 * Precondition recognizer that matches if a module block (not a module
 * statement) starts at the current location, but does not consume any tokens. A
 * module block starts with one of:
 * 
 * <pre>
 * module x.
 * module x input.
 * module x output.
 * </pre>
 * 
 * and ends with <code>endmodule</code>.
 * <p>
 * This recognizer is used to distinguish module statements (that invoke a
 * module) from module declarations (matched by this recognizer).
 * <p>
 * Note that module statements cannot appear inside module blocks.
 * 
 * @author $Author: goeb $
 * @version $Rev: 53086 $
 * @ConQAT.Rating GREEN Hash: 55C3799D8499F0C4AB2366B90A3B38F8
 */
public class ModuleBlockStartRecognizer extends
		RecognizerBase<EAbapParserStates> {

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EAbapParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		if (tokens.size() < startOffset + 3) {
			return NO_MATCH;
		}

		if (tokens.get(startOffset).getType() != MODULE) {
			return NO_MATCH;
		}

		if (!EnumSet.of(DOT, INPUT, OUTPUT).contains(
				tokens.get(startOffset + 2).getType())) {
			return NO_MATCH;
		}

		int endModuleIndex = TokenStreamUtils.find(tokens, startOffset,
				ENDMODULE);
		if (endModuleIndex == -1) {
			return NO_MATCH;
		}

		int moduleIndex = TokenStreamUtils
				.find(tokens, startOffset + 1, MODULE);
		if (moduleIndex != -1 && moduleIndex < endModuleIndex) {
			return NO_MATCH;
		}

		return startOffset;
	}
}
