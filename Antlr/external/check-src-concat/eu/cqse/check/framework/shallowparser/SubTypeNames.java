/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * This class collects constants used for subtypes in a {@link ShallowEntity}.
 * The list of constants is not complete, as often the subtype is also taken
 * directly from the parsing context (e.g. a matched keyword). The purpose of
 * this class is only to eliminate redundancy between the parser and code
 * traversing the ASt.
 * 
 * Convention is to have the name of the constant and the content the same
 * (while respecting naming conventions). Hence, changes to a value should be
 * reflected in the name.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57529 $
 * @ConQAT.Rating YELLOW Hash: CB4BA99BF25693730693D93A00CD36B6
 */
public class SubTypeNames {

	/** Sub type. */
	public static final String ANNOTATION = "annotation";

	/** Sub type. */
	public static final String NAMESPACE = "namespace";

	/** Sub type. */
	public static final String CLASS = "class";

	/** Sub type. */
	public static final String ENUM = "enum";

	/** Sub type. */
	public static final String ENUM_LITERAL = "enum literal";

	/** Sub type. */
	public static final String STRUCT = "struct";

	/** Sub type. */
	public static final String DELEGATE = "delegate";

	/** Sub type. */
	public static final String CLASS_PUBLICATION = "class publication";

	/** Sub type. */
	public static final String CLASS_DEFINITION = "class definition";

	/** Sub type. */
	public static final String CLASS_IMPLEMENTATION = "class implementation";

	/** Sub type. */
	public static final String INTERFACE_PUBLICATION = "interface publication";

	/** Sub type. */
	public static final String INTERFACE_DEFINITION = "interface definition";

	/** Sub type. */
	public static final String METHOD_IMPLEMENTATION = "method implementation";

	/** Sub type. */
	public static final String METHOD_DECLARATION = "method declaration";

	/** Sub type. */
	public static final String FUNCTION = "function";

	/** Sub type. */
	public static final String ON_CHANGE = "on change";

	/** Sub type. */
	public static final String NATIVE_SQL = "native SQL";

	/** Sub type. */
	public static final String SELECT_BLOCK = "select block";

	/** Sub type. */
	public static final String MODULE_INPUT = "module input";

	/** Sub type. */
	public static final String MODULE_OUTPUT = "module output";

	/** Sub type. */
	public static final String GET_LATE = "get late";

	/** Sub type. */
	public static final String TOP_OF_PAGE_DURING_LINE_SELECTION = "top-of-page during line-selection";

	/** Sub type. */
	public static final String PF_EVENT = "pf event";

	/** Sub type. */
	public static final String FORM = "form";

	/** Sub type. */
	public static final String SINGLE_SELECT = "single select";

	/** Sub type. */
	public static final String MACRO = "macro";

	/** Sub type. */
	public static final String VISIBILITY = "Visibility";

	/** Sub type. */
	public static final String EMPTY_STATEMENT = "empty statement";

	/** Sub type. */
	public static final String LOCAL_VARIABLE = "local variable";

	/** Sub type. */
	public static final String INTERFACE = "interface";

	/** Sub type. */
	public static final String IF = "if";

	/** Sub type. */
	public static final String ELSE_IF = "else if";

	/** Sub type. */
	public static final String ELSE_IF_NOSPACE = "elseif";

	/** Sub type. */
	public static final String DO = "do";

	/** Sub type. */
	public static final String FOR = "for";

	/** Sub type. */
	public static final String WHILE = "while";

	/** Sub type. */
	public static final String FOREACH = "foreach";

	/** Sub type. */
	public static final String ELSIF = "elsif";

	/** Sub type. */
	public static final String LOOP = "loop";

	/** Sub type. */
	public static final String CONTINUE = "continue";

	/** Sub type. */
	public static final String CASE = "case";

	/** Sub type. */
	public static final String SWITCH = "switch";

	/** Sub type. */
	public static final String DEFAULT = "default";

	/** Sub type. */
	public static final String WHEN = "when";

	/** Sub type. */
	public static final String ELSE = "else";

	/** Sub type. */
	public static final String FINALLY = "finally";

	/** Sub type. */
	public static final String TRY = "try";

	/** Sub type. */
	public static final String CATCH = "catch";

	/** Sub type. */
	public static final String EXIT = "exit";

	/** Sub type. */
	public static final String DECLARATION = "declaration";

	/** Sub type. */
	public static final String SIMPLE_STATEMENT = "simple statement";

	/** Sub type. */
	public static final String CLEANUP = "cleanup";

	/** Sub type. */
	public static final String PROVIDE = "provide";

	/** Sub type. */
	public static final String LABEL = "label";

	/** Sub type. */
	public static final String ANONYMOUS_BLOCK = "anonymous block";

	/** Sub type. */
	public static final String ANONYMOUS_CLASS = "anonymous class";

	/** Sub type. */
	public static final String CHECKED = "checked";

	/** Sub type. */
	public static final String UNCHECKED = "unchecked";

	/** Sub type. */
	public static final String UNSAFE = "unsafe";

	/** Sub type. */
	public static final String LOCK = "lock";

	/** Sub type. */
	public static final String USING = "using";

	/** Sub type. */
	public static final String USE = "use";

	/** Sub type. */
	public static final String FIXED = "fixed";

	/** Sub type. */
	public static final String YIELD = "yield";

	/** Sub type. */
	public static final String GOTO = "goto";

	/** Sub type. */
	public static final String LAMBDA = "lambda";

	/** Sub type. */
	public static final String LAMBDA_EXPRESSION = "lambda expression";

	/** Sub type. */
	public static final String SYNCHRONIZED = "synchronized";

	/** Sub type. */
	public static final String DATA = "data";

	/** Sub type. */
	public static final String STATICS = "statics";

	/** Sub type. */
	public static final String CONSTANTS = "constants";

	/** Sub type. */
	public static final String FIELD_SYMBOLS = "field-symbols";

	/** Sub type. */
	public static final String PARAMETERS = "parameters";

	/** Sub type. */
	public static final String TABLES = "tables";

	/** Sub type. */
	public static final String NODES = "nodes";

	/** Sub type. */
	public static final String SUBMIT = "submit";

	/** Sub type. */
	public static final String LEAVE = "leave";

	/** Sub type. */
	public static final String RETURN = "return";

	/** Sub type. */
	public static final String STOP = "stop";

	/** Sub type. */
	public static final String REJECT = "reject";

	/** Sub type. */
	public static final String RAISE = "raise";

	/** Sub type. */
	public static final String TYPES = "types";

	/** Sub type. */
	public static final String INCLUDE = "include";

	/** Sub type. */
	public static final String TYPE_POOLS = "type-pools";

	/** Sub type */
	public static final String STATIC_INITIALIZER = "static initializer";

	/** Sub type */
	public static final String ATTRIBUTE = "attribute";

	/** Sub type */
	public static final String SELECT = "select";

	/** Sub type */
	public static final String PROPERTY = "property";

	/** Sub type */
	public static final String SET = "set";

	/** Sub type */
	public static final String META_CLASS = "meta class";

	/** Sub type */
	public static final String RANGE = "range";

	/** Sub type */
	public static final String ANONYMOUS_METHOD = "anonymous method";

	/** Sub type */
	public static final String ATTRIBUTE_FUNCTION = "attribute function";

	/** Sub type */
	public static final String NAMED_FUNCTION = "named function";

	/** Sub type */
	public static final String ANONYMOUS_FUNCTION = "anonymous function";

	/** Sub type */
	public static final String ASSIGNED_FUNCTION = "assigned function";

	/** Sub type */
	public static final String GLOBAL_BEGIN = "global begin";

	/** Sub type */
	public static final String FUNCTION_POINTER = "function pointer";

	/** Sub type */
	public static final String CONSTANT = "constant";

	/** Sub type */
	public static final String CONSTRUCTOR = "constructor";

	/** Sub type */
	public static final String DESTRUCTOR = "destructor";

	/** Sub type */
	public static final String OPERATOR = "operator";

	/** Sub type */
	public static final String CASE_LABEL = "case label";

	/** Sub type */
	public static final String ALIAS = "alias";

	/** Sub type */
	public static final String TYPE_POINTER = "type pointer";

	/** Sub type */
	public static final String VARIABLE = "variable";

	/** Sub type */
	public static final String CASE_ELSE = "case else";

	/** Sub type */
	public static final String IEC_SV_VARIABLE = "IEC SV Variable";

	/** Sub type */
	public static final String IEC_SV_ELEMENT = "IEC SV Element";

	/** Sub type */
	public static final String FUNCTION_DECLARATION = "function declaration";

	/** Sub type */
	public static final String IMPORT = "import";

	/** Sub type */
	public static final String STATIC_CONSTRUCTOR = "static constructor";

	/** Sub type */
	public static final String PACKAGE = "package";

	/** Sub type */
	public static final String METHOD = "method";

	/** Sub type */
	public static final String STATIC_METHOD = "static method";

	/** Sub type */
	public static final String DESTRUCTOR_DECLARATION = "destructor declaration";

	/** Sub type */
	public static final String EXECUTE = "execute";

	/** Sub type */
	public static final String GO = "go";

	/** Sub type */
	public static final String INSERT = "insert";

	/** Sub type */
	public static final String MODIFY = "modify";

	/** Sub type */
	public static final String UPDATE = "update";

	/** Sub type */
	public static final String DELETE = "delete";

	/** Sub type */
	public static final String AT_LINE_SELECTION = "at line-selection";

	/** Sub type */
	public static final String AT_SELECTION_SCREEN = "at selection-screen";

	/** Sub type */
	public static final String AT_SELECTION_OUTPUT = "at selection-output";

	/** Sub type */
	public static final String AT_USER_COMMAND = "at user-command";

	/** Sub type */
	public static final String END_OF_PAGE = "end-of-page";

	/** Sub type */
	public static final String END_OF_SELECTION = "end-of-selection";

	/** Sub type */
	public static final String INITIALIZATION = "initialization";

	/** Sub type */
	public static final String LOAD_OF_PROGRAM = "load-of-program";

	/** Sub type */
	public static final String START_OF_SELECTION = "start-of-selection";

	/** Sub type */
	public static final String TOP_OF_PAGE = "top-of-page";

	/** Sub type */
	public static final String GET = "get";

	/** Sub type */
	public static final String REPORT = "report";

	/** Sub type */
	public static final String GENERATE = "generate";

	/** Sub type */
	public static final String CALL = "call";

	/** Sub type */
	public static final String ASSERT = "assert";

	/** Sub type */
	public static final String SELECT_OPTIONS = "select-options";

	/** Sub type */
	public static final String DEFINE = "define";

	/** Sub type */
	public static final String FIELD_GROUPS = "field-groups";

	/** Sub type */
	public static final String RANGES = "ranges";

	/** Sub type */
	public static final String CLASS_DATA = "class-data";

	/** Sub type */
	public static final String DOCUMENT_ROOT = "document-root";

	/** Sub type */
	public static final String IMPORTING = "importing";

	/** Sub type */
	public static final String EXPORTING = "exporting";

	/** Sub type */
	public static final String CHANGING = "changing";

	/** Sub type */
	public static final String RETURNING = "returning";

	/** Sub type */
	public static final String PUBLIC = "public";

	/** Sub type */
	public static final String PROTECTED = "protected";

	/** Sub type */
	public static final String PRIVATE = "private";

	/** Sub type */
	public static final String ENUM_CLASS = "enum class";

	/** Sub type */
	public static final String GLOBAL_VARIABLE = "global variable";

	/** Sub type */
	public static final String STATIC_IMPORT = "static import";

	/** Sub type */
	public static final String EMPTY_GET = "empty get";

	/** Sub type */
	public static final String EMPTY_SET = "empty set";

	/** Sub type */
	public static final String TRAIT = "trait";

	/** Sub type */
	public static final String REQUIRE = "require";

	/** Sub type */
	public static final String REQUIRE_ONCE = "require_once";

	/** Sub type */
	public static final String INCLUDE_ONCE = "include_once";
}
