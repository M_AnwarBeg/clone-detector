package eu.cqse.check.framework.shallowparser;

import static eu.cqse.check.framework.shallowparser.TokenStreamUtils.NOT_FOUND;

import java.util.ArrayList;
import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Utility methods for {@link IToken} lists that work on the tokens' text.
 * 
 * @author $Author: pagano $
 * @version $Rev: 54869 $
 * @ConQAT.Rating GREEN Hash: 1E121FB6DBF18A052428CBCB94BF2174
 */
public class TokenStreamTextUtils {

	/**
	 * Returns the index of the first token whose token text equals the given
	 * text in the given token list. If the text is not found,
	 * {@link #NOT_FOUND} is returned.
	 */
	public static int findFirst(List<IToken> tokens, String text) {
		return findFirst(tokens, 0, tokens.size(), text);
	}

	/**
	 * Returns the first index of the given text not before the given start
	 * index and before the given end index. If the text is not found,
	 * {@link #NOT_FOUND} is returned.
	 */
	public static int findFirst(List<IToken> tokens, int startOffset,
			int endOffset, String text) {
		for (int i = startOffset; i < endOffset; i++) {
			if (tokens.get(i).getText().equals(text)) {
				return i;
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns all indices of the given text within the given token list. If
	 * none is found, an empty list is returned.
	 */
	public static List<Integer> findAll(List<IToken> tokens, String text) {
		List<Integer> indices = new ArrayList<>();
		for (int i = 0; i < tokens.size(); i++) {
			if (tokens.get(i).getText().equals(text)) {
				indices.add(i);
			}
		}
		return indices;
	}

	/**
	 * Returns whether the given token list contains a token with the given
	 * text.
	 */
	public static boolean contains(List<IToken> tokens, String text) {
		return findFirst(tokens, text) != NOT_FOUND;
	}

	/**
	 * Returns whether the given token list contains tokens with the given
	 * sequence of strings at the given start offset.
	 */
	public static boolean hasSequence(List<IToken> tokens, int startOffset,
			String... sequence) {
		if (startOffset < 0 || startOffset + sequence.length > tokens.size()) {
			return false;
		}

		for (int i = 0; i < sequence.length; i++) {
			if (!tokens.get(startOffset + i).getText().equals(sequence[i])) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the index of the first occurrence of the given sequence of
	 * strings in the given token list, beginning from the given start offset.
	 * The given start type is the type of the sequence's first token. If the
	 * sequence is not found, {@link #NOT_FOUND} is returned.
	 */
	public static int findSequence(List<IToken> tokens, int startOffset,
			ETokenType startType, String... sequence) {
		for (int i = startOffset; i < tokens.size() - sequence.length
				+ 1; i++) {
			if (tokens.get(i).getType().equals(startType)) {
				if (hasSequence(tokens, i, sequence)) {
					return i;
				}
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns all indices of occurrences of the given sequence of token texts
	 * in the given token list, beginning from the given offset. The given start
	 * token type is the type of the first token of the sequence. If the
	 * sequence is not found, an empty list is returned.
	 */
	public static List<Integer> findAllSequences(List<IToken> tokens,
			int startOffset, ETokenType startType, String... sequence) {
		List<Integer> indices = new ArrayList<>();

		while (startOffset < tokens.size() - sequence.length + 1) {
			startOffset = findSequence(tokens, startOffset, startType,
					sequence);
			if (startOffset == NOT_FOUND) {
				break;
			}
			indices.add(startOffset);
			startOffset++;
		}

		return indices;
	}

	/** Concatenates the token's texts and returns them as string. */
	public static String concatTokenTexts(List<IToken> tokens) {
		return concatTokenTexts(tokens, "");
	}

	/** Concatenates the token's texts and returns them as string. */
	public static String concatTokenTexts(List<IToken> tokens,
			String separator) {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < tokens.size(); i++) {
			if (i > 0) {
				builder.append(separator);
			}
			builder.append(tokens.get(i).getText());
		}
		return builder.toString();
	}

	/** Concatenates the inner lists' token texts and returns them as list. */
	public static List<String> concatAllTokenTexts(
			List<List<IToken>> tokenLists) {
		List<String> strings = new ArrayList<String>();
		for (List<IToken> tokenList : tokenLists) {
			strings.add(concatTokenTexts(tokenList));
		}
		return strings;

	}

	/**
	 * Converts the sublist of the given token list from the given start index
	 * (inclusive) to the given end index (exclusive) to a list of token texts.
	 */
	public static List<String> getTokenTexts(List<IToken> tokens,
			int startIndex, int endIndex) {
		List<String> resultStrings = new ArrayList<>();
		for (int i = startIndex; i < endIndex; i++) {
			resultStrings.add(tokens.get(i).getText());
		}
		return resultStrings;
	}

	/**
	 * Converts the sublist of the given token list to a list of token texts.
	 */
	public static List<String> getTokenTexts(List<IToken> tokens) {
		return getTokenTexts(tokens, 0, tokens.size());
	}
}
