/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.abap;

import static eu.cqse.check.framework.scanner.ETokenType.*;
import static eu.cqse.check.framework.shallowparser.languages.abap.AbapShallowParser.EAbapParserStates.DECLARATIONS;
import static eu.cqse.check.framework.shallowparser.languages.abap.AbapShallowParser.EAbapParserStates.STATEMENTS;
import static eu.cqse.check.framework.shallowparser.languages.abap.AbapShallowParser.EAbapParserStates.TOPLEVEL;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;

/**
 * Shallow parser for ABAP. The following links are useful for writing the
 * parser:
 * <ul>
 * <li><a href="http://help.sap.com/abapdocu_702/en/">ABAP Keyword
 * Documentation</a></li>
 * <li><a href=
 * "http://help.sap.com/abapdocu_702/en/abenabap_statements_overview.htm" >ABAP
 * Statements Overview</a></li>
 * </ul>
 * 
 * @author $Author: goeb $
 * @version $Rev: 56639 $
 * @ConQAT.Rating GREEN Hash: EC4210143A3F2C26C69F817994C19B95
 */
public class AbapShallowParser
		extends ShallowParserBase<AbapShallowParser.EAbapParserStates> {

	/**
	 * Since there are no reserved words in ABAP, one can use statement names as
	 * identifiers. Therefore, whenever we expect an identifier, it's not safe
	 * to rely on the Scanner's categorization of identifiers, and we should
	 * allow keywords as well.
	 */
	private static final EnumSet<ETokenClass> IDENTIFIER_LIKE = EnumSet
			.of(ETokenClass.IDENTIFIER, ETokenClass.KEYWORD);

	/** Tokens that can introduce a simple statement. */
	private static final EnumSet<ETokenType> SIMPLE_STATEMENT_START_TOKENS = EnumSet
			.of(ADD, ADD_CORRESPONDING, APPEND, ASSERT, ASSIGN, AUTHORITY_CHECK,
					BACK, BREAK_POINT, CALL, CHECK, CLEAR, CLOSE, COLLECT,
					COMMIT, COMMUNICATION, COMPUTE, CONCATENATE, CONDENSE,
					CONSTANTS, CONTEXTS, CONTINUE, CONTROLS, CONVERT, CREATE,
					DATA, DELETE, DEMAND, DESCRIBE, DETAIL, DIVIDE,
					DIVIDE_CORRESPONDING, EDITOR_CALL, ENHANCEMENT_POINT,
					EXISTS, EXIT, EXPORT, EXTRACT, FETCH, FIELDS, FIELD_GROUPS,
					FIELD_SYMBOLS, FIND, FORMAT, FREE, GENERATE, GET, HIDE,
					IDENTIFIER, IMPORT, INCLUDE, INFOTYPES, INPUT, INSERT,
					LEAVE, LOAD, LOCAL, LOG_POINT, MAXIMUM, MESSAGE, MINIMUM,
					MODIFY, MODULE, MOVE, MOVE_CORRESPONDING,
					MULTIPLY_CORRESPONDING, MULTIPLY, NAME, NEW_LINE, NEW_PAGE,
					NEW_SECTION, OPEN, OVERLAY, PACK, PACKAGE, PERFORM,
					POSITION, PRINT_CONTROL, PUT, RAISE, RANGES, READ, REFRESH,
					REJECT, REPLACE, RESERVE, RESUME, RETURN, ROLLBACK, SCROLL,
					SEARCH, SELECT_OPTIONS, SET, SHIFT, SKIP, SORT, SPLIT,
					STATICS, STOP, SUBMIT, SUBTRACT, SUBTRACT_CORRESPONDING,
					SUM, SUMMARY, SUMMING, SUPPLY, SUPPRESS, SYNTAX_CHECK,
					TRANSFER, TRANSLATE, TRUNCATE, TYPES, ULINE, UNASSIGN,
					UNPACK, UPDATE, WAIT, WINDOW, WRITE);

	/**
	 * Set of keywords that start an event block (without keywords that require
	 * a preceding "at")
	 */
	private static final EnumSet<ETokenType> EVENT_BLOCKS = EnumSet.of(
			INITIALIZATION, START_OF_SELECTION, END_OF_SELECTION, TOP_OF_PAGE,
			END_OF_PAGE, LOAD_OF_PROGRAM);

	/**
	 * Set of keywords that end an event block (possibly indicating the start of
	 * the next one)
	 */
	private static final EnumSet<ETokenType> EVENT_BLOCKS_END = EnumSet.of(AT,
			FORM, CLASS, INTERFACE);

	// all tokens that start a new event block also end the current one.
	static {
		EVENT_BLOCKS_END.addAll(EVENT_BLOCKS);
	}

	/** The states used in this parser. */
	public static enum EAbapParserStates {

		/**
		 * Top level state used for parsing constructs that are not nested in
		 * other constructs.
		 */
		TOPLEVEL,

		/**
		 * A state to recognize declarations within classes. As many constructs
		 * are allowed both top-level and in declarations, many rules are
		 * registered for both.
		 */
		DECLARATIONS,

		/**
		 * A state to recognize statements, i.e. plain code in functions, etc.
		 */
		STATEMENTS
	}

	/** Constructor. */
	public AbapShallowParser() {
		super(EAbapParserStates.class, TOPLEVEL);

		createMetaRules();
		createTopLevelRules();
		createTypeRules();
		createAttributeRules();
		createMethodRules();
		createModuleRules();
		createStatementRules();

		inAnyState().sequence(DOT).createNode(EShallowEntityType.STATEMENT,
				SubTypeNames.EMPTY_STATEMENT).endNode();
	}

	/** Rules for parsing elements that are only expected top-level. */
	private void createTopLevelRules() {

		// since the report is not really a method, its statements are still
		// parsed in the toplevel scope, not the statement scope
		RecognizerBase<EAbapParserStates> reportRecognizer = inState(TOPLEVEL)
				.sequence(EnumSet.of(REPORT, PROGRAM)).optional(COLON)
				.sequence(IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.METHOD, 0, -1).skipTo(DOT)
				.parseUntilOrEof(TOPLEVEL);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END, reportRecognizer);

		inState(TOPLEVEL).sequence(EnumSet.of(SELECTION_SCREEN))
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.endNode();

		// top-of-page during line-selection is different from "top-of-page"
		RecognizerBase<EAbapParserStates> topsRecognizer = inState(TOPLEVEL)
				.sequence(TOP_OF_PAGE, DURING, LINE_SELECTION)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.TOP_OF_PAGE_DURING_LINE_SELECTION)
				.skipTo(DOT).parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END, topsRecognizer);

		// basic event blocks without "at"
		RecognizerBase<EAbapParserStates> eventBlockRecognizer = inState(
				TOPLEVEL).sequence(EVENT_BLOCKS)
						.createNode(EShallowEntityType.METHOD, 0).skipTo(DOT)
						.parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END, eventBlockRecognizer);

		createGetBlockRules();

		createAtBlockRules();
	}

	/**
	 * Creates rules constructs starting with "GET". These can either be
	 * statements or event handlers.
	 */
	private void createGetBlockRules() {
		// "get reference of" and consorts
		inState(TOPLEVEL, STATEMENTS).sequence(GET, ETokenClass.KEYWORD)
				.createNode(EShallowEntityType.STATEMENT, new int[] { 0, 1 })
				.skipTo(DOT).endNode();

		// get ... late event handler
		RecognizerBase<EAbapParserStates> getLateEventBlockRecognizer = inState(
				TOPLEVEL)
						.sequence(GET, IDENTIFIER, LATE)
						.createNode(EShallowEntityType.METHOD,
								SubTypeNames.GET_LATE, 1)
						.skipTo(DOT).parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END,
				getLateEventBlockRecognizer);

		// get ... event handler
		RecognizerBase<EAbapParserStates> getEventBlockRecognizer = inState(
				TOPLEVEL).sequence(GET, IDENTIFIER)
						.createNode(EShallowEntityType.METHOD, 0, 1).skipTo(DOT)
						.parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END, getEventBlockRecognizer);
	}

	/** Creates parsing rules for event handlers starting with "AT". */
	private void createAtBlockRules() {
		// at selection screen event handler (named)
		RecognizerBase<EAbapParserStates> selectionScreenBlockRecognizer = inState(
				TOPLEVEL).sequence(AT, SELECTION_SCREEN)
						.skipTo(DOT)
						.createNode(EShallowEntityType.METHOD,
								SubTypeNames.AT_SELECTION_SCREEN,
								new Region(0, -2, StringUtils.SPACE))
						.parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END,
				selectionScreenBlockRecognizer);

		// at PF<..> event handler (named)
		RecognizerBase<EAbapParserStates> atPfBlockRecognizer = inState(
				TOPLEVEL)
						.sequence(AT, PF_EVENT)
						.createNode(EShallowEntityType.METHOD,
								SubTypeNames.PF_EVENT, new Region(1, 1))
						.skipTo(DOT).parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END, atPfBlockRecognizer);

		// at ... event handler (anonymous)
		RecognizerBase<EAbapParserStates> atEventBlockRecognizer = inState(
				TOPLEVEL)
						.sequence(AT, EnumSet.of(LINE_SELECTION, USER_COMMAND))
						.createNode(EShallowEntityType.METHOD,
								new int[] { 0, 1 })
						.skipTo(DOT).parseUntilOrEof(STATEMENTS);
		endMethodEntityOnEventBlock(EVENT_BLOCKS_END, atEventBlockRecognizer);
	}

	/**
	 * Ends the given recognizer if it either hits one of the given event block
	 * end tokens or a get event block or a module block.
	 */
	private static void endMethodEntityOnEventBlock(
			EnumSet<ETokenType> eventBlocksEnd,
			RecognizerBase<EAbapParserStates> eventBlockRecognizer) {
		eventBlockRecognizer.sequenceBefore(eventBlocksEnd).endNode();
		eventBlockRecognizer.sequenceBefore(GET, IDENTIFIER).endNode();
		eventBlockRecognizer.preCondition(new ModuleBlockStartRecognizer())
				.endNode();
	}

	/** Rules for parsing of meta elements. */
	private void createMetaRules() {

		inState(DECLARATIONS)
				.sequence(EnumSet.of(PUBLIC, PROTECTED, PRIVATE), SECTION, DOT)
				.createNode(EShallowEntityType.META, SubTypeNames.VISIBILITY, 0)
				.endNode();

		inAnyState().sequence(EnumSet.of(TYPE_POOLS, TABLES))
				.createNode(EShallowEntityType.META, 0).skipTo(DOT).endNode();

		inAnyState().sequence(DEFINE)
				.createNode(EShallowEntityType.META, SubTypeNames.MACRO)
				.skipTo(END_OF_DEFINITION, DOT).endNode();

		inState(DECLARATIONS).sequence(EnumSet.of(INTERFACES, ALIASES))
				.createNode(EShallowEntityType.META, 0).skipTo(DOT).endNode();
	}

	/** Rules for parsing classes, interfaces, types, and events. */
	private void createTypeRules() {

		createClassRules();

		// interfaces
		RecognizerBase<EAbapParserStates> interfaceAlternative = inState(
				TOPLEVEL, DECLARATIONS).sequence(INTERFACE, IDENTIFIER);
		interfaceAlternative.sequence(EnumSet.of(LOAD, DEFERRED, LOCAL))
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.INTERFACE_PUBLICATION, 1)
				.skipTo(DOT).endNode();
		interfaceAlternative
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.INTERFACE_DEFINITION, 1)
				.skipTo(DOT).parseUntil(DECLARATIONS)
				.sequence(ENDINTERFACE, DOT).endNode();

	}

	/** Rules for parsing ABAP OO classes. */
	private void createClassRules() {
		RecognizerBase<EAbapParserStates> classDefinitionAlternative = inState(
				TOPLEVEL, DECLARATIONS).sequence(CLASS, IDENTIFIER, DEFINITION);
		classDefinitionAlternative.sequence(EnumSet.of(LOAD, DEFERRED, LOCAL))
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.CLASS_PUBLICATION, 1)
				.skipTo(DOT).endNode();
		classDefinitionAlternative
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.CLASS_DEFINITION, 1)
				.skipTo(DOT).parseUntil(DECLARATIONS).sequence(ENDCLASS, DOT)
				.endNode();

		inState(TOPLEVEL, DECLARATIONS)
				.sequence(CLASS, IDENTIFIER, IMPLEMENTATION)
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.CLASS_IMPLEMENTATION, 1)
				.skipTo(DOT).parseUntil(DECLARATIONS).sequence(ENDCLASS, DOT)
				.endNode();
	}

	/** Rules for parsing attributes. */
	private void createAttributeRules() {
		// Match record type declaration as single entity
		inState(TOPLEVEL, DECLARATIONS)
				.sequence(EnumSet.of(CONSTANTS, DATA, STATICS, CLASS_DATA))
				.optional(COLON).sequence(BEGIN, OF, IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.ATTRIBUTE, 0, -1).skipTo(END, OF)
				.skipTo(DOT).endNode();

		// types, events, class events
		inState(TOPLEVEL, DECLARATIONS)
				.sequence(EnumSet.of(EVENTS, CLASS_EVENTS))
				.createNode(EShallowEntityType.ATTRIBUTE, 0).skipTo(DOT)
				.endNode();

		inState(TOPLEVEL, DECLARATIONS)
				.sequence(
						EnumSet.of(CONSTANTS, NODES, STATICS, DATA, CLASS_DATA))
				.optional(COLON).sequence(IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.ATTRIBUTE, 0, -1).skipTo(DOT)
				.endNode();

		inState(TOPLEVEL, DECLARATIONS).sequence(FIELD_SYMBOLS).optional(COLON)
				.sequence(LT, IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.ATTRIBUTE, 0, -1).skipTo(DOT)
				.endNode();

		inState(TOPLEVEL, DECLARATIONS)
				// The "parameter" keyword is the obsolete older form of
				// "parameters"
				.sequence(EnumSet.of(PARAMETER, PARAMETERS)).optional(COLON)
				.sequence(IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.PARAMETERS, -1)
				.skipTo(DOT).endNode();
	}

	/**
	 * Rules for parsing methods, functions, and forms.
	 * 
	 * We handle colons here to catch method declarations like <code>form:
	 * foo.</foo> as that's a declaration for form "foo" not ":foo".
	 */
	private void createMethodRules() {
		inState(DECLARATIONS).sequence(EnumSet.of(METHODS, CLASS_METHODS))
				.skipBefore(EnumSet.of(DOT, RETURNING, IMPORTING, EXPORTING,
						REDEFINITION, CHANGING, EXCEPTIONS))
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.METHOD_DECLARATION, new Region(1, -1))
				.skipTo(DOT).endNode();

		inState(DECLARATIONS).sequence(METHOD).optional(COLON).markStart()
				.skipBefore(EnumSet.of(DOT, BY))
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.METHOD_IMPLEMENTATION, new Region(0, -1))
				.skipTo(DOT).parseUntil(STATEMENTS).sequence(ENDMETHOD, DOT)
				.endNode();

		inState(TOPLEVEL, DECLARATIONS).sequence(FUNCTION).optional(COLON)
				.markStart().skipTo(DOT)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.FUNCTION,
						new Region(0, -2))
				.parseUntil(STATEMENTS).sequence(ENDFUNCTION, DOT).endNode();

		inState(TOPLEVEL).sequence(FORM).optional(COLON)
				.sequence(IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.FORM, -1)
				.skipTo(DOT).parseUntil(STATEMENTS).sequence(ENDFORM, DOT)
				.endNode();
	}

	/** Rules for parsing modules. */
	private void createModuleRules() {
		RecognizerBase<EAbapParserStates> moduleRecognizer = inState(TOPLEVEL)
				.preCondition(new ModuleBlockStartRecognizer()).sequence(MODULE)
				.markStart().skipBefore(EnumSet.of(INPUT, OUTPUT, DOT));
		moduleRecognizer.sequence(INPUT, DOT)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.MODULE_INPUT, 0)
				.parseUntil(STATEMENTS).sequence(ENDMODULE, DOT).endNode();
		moduleRecognizer.sequence(OUTPUT, DOT)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.MODULE_OUTPUT, 0)
				.parseUntil(STATEMENTS).sequence(ENDMODULE, DOT).endNode();
		moduleRecognizer.sequence(DOT)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.MODULE_INPUT, 0)
				.parseUntil(STATEMENTS).sequence(ENDMODULE, DOT).endNode();
	}

	/** Rules for parsing statements. */
	private void createStatementRules() {

		// special rule that matches assignments to variables that have the same
		// name as keywords.
		inState(STATEMENTS).sequence(ETokenClass.KEYWORD, EQ)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.endNode();

		createConditionalStatementRules();

		// on change
		RecognizerBase<EAbapParserStates> changeAlternative = inAnyState()
				.sequence(ON, CHANGE, OF)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.ON_CHANGE)
				.skipTo(DOT).parseUntil(STATEMENTS)
				.sequenceBefore(EnumSet.of(ELSE, ENDON));
		changeAlternative.sequence(ENDON, DOT).endNode();
		changeAlternative.endNodeWithContinuation();

		createLoopStatementRules();

		// try/catch
		RecognizerBase<EAbapParserStates> tryAlternative = inState(TOPLEVEL,
				STATEMENTS).sequence(EnumSet.of(TRY, CATCH, CLEANUP))
						.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
						.parseUntil(STATEMENTS).sequenceBefore(
								EnumSet.of(ENDTRY, CATCH, ENDCATCH, CLEANUP));
		tryAlternative.sequence(EnumSet.of(ENDTRY, ENDCATCH), DOT).endNode();
		tryAlternative.endNodeWithContinuation();

		createSelectRules();

		// exec
		inState(TOPLEVEL, STATEMENTS).sequence(EXEC, SQL)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.NATIVE_SQL)
				.skipTo(ENDEXEC, DOT).endNode();

		// simple statements that start with a field symbol, e.g.
		// "<fs>-foo = 12."
		inState(TOPLEVEL, STATEMENTS)
				.sequence(LT, SIMPLE_STATEMENT_START_TOKENS)
				.createNode(EShallowEntityType.STATEMENT, new Region(0, 2))
				.skipTo(DOT).endNode();

		// Match record type declarations as a single entity, although they
		// contain multiple dots.
		inState(TOPLEVEL, DECLARATIONS, STATEMENTS)
				.sequence(EnumSet.of(TYPES, CONSTANTS, DATA, STATICS))
				.optional(COLON).sequence(BEGIN, OF, IDENTIFIER_LIKE)
				.createNode(EShallowEntityType.STATEMENT, 0, -1).skipTo(END, OF)
				.skipTo(DOT).endNode();

		inState(TOPLEVEL, DECLARATIONS, STATEMENTS)
				.sequence(SIMPLE_STATEMENT_START_TOKENS)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.endNode();
	}

	/** Creates parsing rules for conditional statements. */
	private void createConditionalStatementRules() {
		// if/elseif
		RecognizerBase<EAbapParserStates> ifAlternative = inState(TOPLEVEL,
				STATEMENTS).sequence(EnumSet.of(IF, ELSEIF))
						.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
						.parseUntil(STATEMENTS)
						.sequenceBefore(EnumSet.of(ELSEIF, ELSE, ENDIF));
		ifAlternative.sequence(ENDIF, DOT).endNode();
		ifAlternative.endNodeWithContinuation();

		// else
		inState(TOPLEVEL, STATEMENTS).sequence(ELSE)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(EnumSet.of(ENDIF, ENDON), DOT)
				.endNode();

		// case/when
		inState(TOPLEVEL, STATEMENTS).sequence(CASE)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDCASE, DOT).endNode();
		// we parse when as meta, so we add no additional nesting
		inState(STATEMENTS).sequence(WHEN)
				.createNode(EShallowEntityType.META, 0).skipTo(DOT).endNode();
	}

	/** Creates parsing rules for loops and loop-like constructs. */
	private void createLoopStatementRules() {
		// loops
		inState(TOPLEVEL, STATEMENTS).sequence(LOOP)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDLOOP, DOT).endNode();
		inState(TOPLEVEL, STATEMENTS).sequence(DO)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDDO, DOT).endNode();
		inState(TOPLEVEL, STATEMENTS).sequence(WHILE)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDWHILE, DOT).endNode();
		inState(STATEMENTS).sequence(AT)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDAT, DOT).endNode();

		// loop likes
		inAnyState().sequence(PROVIDE)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDPROVIDE, DOT).endNode();
		inAnyState().sequence(ENHANCEMENT)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(ENDENHANCEMENT, DOT).endNode();
		inAnyState().sequence(ENHANCEMENT_SECTION)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DOT)
				.parseUntil(STATEMENTS).sequence(END_ENHANCEMENT_SECTION, DOT)
				.endNode();
	}

	/**
	 * Creates the parsing rules for the select clause. This is tricky, because
	 * the rules whether a select block or a single statement select is
	 * expected, are not trivial.
	 */
	private void createSelectRules() {
		RecognizerBase<EAbapParserStates> selectAlternative = inState(TOPLEVEL,
				STATEMENTS).sequence(SELECT);
		selectAlternative.sequence(LPAREN)
				.createNode(EShallowEntityType.STATEMENT, "method call")
				.skipToWithNesting(RPAREN, LPAREN, RPAREN).skipTo(DOT)
				.endNode();
		selectAlternative.subRecognizer(new SingleSelectRecognizer(), 1, 1)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SINGLE_SELECT)
				.endNode();
		selectAlternative
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SELECT_BLOCK)
				.skipTo(DOT).parseUntil(STATEMENTS).sequence(ENDSELECT, DOT)
				.endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isFilteredToken(IToken token, IToken previousToken) {
		return super.isFilteredToken(token, previousToken)
				|| token.getType() == PRAGMA_DIRECTIVE;
	}
}
