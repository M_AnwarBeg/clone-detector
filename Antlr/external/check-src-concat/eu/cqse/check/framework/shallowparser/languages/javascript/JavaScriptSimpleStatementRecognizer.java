/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.javascript;

import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.ANY;
import static eu.cqse.check.framework.scanner.ETokenType.BREAK;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSMINUS;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSPLUS;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.THROW;

import java.util.List;

import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.languages.base.LineBasedStatementRecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Recognizer for simple statements in JavaScript. We need a separate recognizer
 * as the rules for statement continuation are non-trivial due to the optional
 * semicolon. A good introduction to the topic can be found <a href=
 * "http://blog.izs.me/post/2353458699/an-open-letter-to-javascript-leaders-regarding"
 * >here</a>.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 56760 $
 * @ConQAT.Rating GREEN Hash: A669719BEE72DB09D3D22ABA161E68A6
 */
/* package */class JavaScriptSimpleStatementRecognizer extends
		LineBasedStatementRecognizerBase<EJavaScriptParserStates> {

	/** {@inheritDoc} */
	@Override
	protected EJavaScriptParserStates getSubParseState() {
		return ANY;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean tokenStartsSubParse(ETokenType tokenType,
			List<IToken> tokens, int offset) {
		switch (tokenType) {
		case FUNCTION:
			return true;
		case LPAREN:
			return startsParenthesisLambda(tokens, offset);
		case IDENTIFIER:
			return startsNamedFunction(tokens, offset)
					|| startsBareLambda(tokens, offset);
		case STRING_LITERAL:
			return startsNamedFunction(tokens, offset);
		default:
			return false;
		}
	}

	/**
	 * Returns whether at the given position there is the start of a lambda
	 * whose arguments are in parentheses.
	 */
	private static boolean startsParenthesisLambda(List<IToken> tokens, int offset) {
		int closingPosition = TokenStreamUtils.findMatchingClosingToken(tokens,
				offset + 1, ETokenType.LPAREN, ETokenType.RPAREN);
		if (closingPosition == TokenStreamUtils.NOT_FOUND) {
			return false;
		}

		return TokenStreamUtils.tokenTypesAt(tokens, closingPosition + 1,
				ETokenType.DOUBLE_ARROW);
	}

	/**
	 * Returns whether at the given position there is the start of a bare
	 * lambda, i.e. one without surrounding parentheses.
	 */
	private static boolean startsBareLambda(List<IToken> tokens, int offset) {
		return offset + 2 < tokens.size()
				&& tokens.get(offset + 1).getType() == ETokenType.DOUBLE_ARROW;
	}

	/**
	 * Returns whether an identifier or string literal at the given position
	 * starts a named function.
	 */
	private static boolean startsNamedFunction(List<IToken> tokens, int offset) {
		return offset + 2 < tokens.size()
				&& tokens.get(offset + 1).getType() == ETokenType.COLON
				&& tokens.get(offset + 2).getType() == ETokenType.FUNCTION;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean startsNewStatement(IToken token, IToken lastToken) {
		ETokenType tokenType = token.getType();
		if (tokenType == RBRACE) {
			return true;
		}

		if (lastToken == null) {
			return false;
		}

		// same line => no new statement
		if (lastToken.getLineNumber() == token.getLineNumber()) {
			return false;
		}

		ETokenType lastTokenType = lastToken.getType();

		// jump statements always end at a new line
		if (lastTokenType == RETURN || lastTokenType == BREAK
				|| lastTokenType == CONTINUE || lastTokenType == THROW) {
			return true;
		}

		// ++ and -- bind to next line
		if (tokenType == PLUSPLUS || tokenType == MINUSMINUS) {
			return true;
		}

		// continue statement if line ends with '.' or ','
		if (lastTokenType == DOT || lastTokenType == COMMA) {
			return false;
		}

		// continue statement if line ends in operator or next line starts
		// with operator or delimiter
		if (lastTokenType.getTokenClass() == ETokenClass.OPERATOR
				|| tokenType.getTokenClass() == ETokenClass.OPERATOR
				|| tokenType.getTokenClass() == ETokenClass.DELIMITER) {
			return false;
		}

		return true;
	}
}
