/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.assertion.CCSMPre;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;

/**
 * Utility methods for {@link IToken} lists.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57678 $
 * @ConQAT.Rating RED Hash: 44CBCE31485E6990165E2A9059C083A7
 */
public class TokenStreamUtils {

	/** The value returned if nothing was found in the various find methods. */
	public static final int NOT_FOUND = -1;

	/**
	 * Returns the index of the first token of given token type (or
	 * {@link #NOT_FOUND}).
	 */
	public static int find(List<IToken> tokens, ETokenType... tokenTypes) {
		return find(tokens, 0, tokenTypes);
	}

	/**
	 * Returns the index of the first token of given token type not before the
	 * start index (or {@link #NOT_FOUND}).
	 */
	public static int find(List<IToken> tokens, int startIndex,
			ETokenType... tokenTypes) {
		return find(tokens, startIndex, tokens.size(), tokenTypes);
	}

	/**
	 * Returns the index of the first token of given token type not before the
	 * start index and before the end index (or {@link #NOT_FOUND}).
	 */
	public static int find(List<IToken> tokens, int startIndex, int endIndex,
			ETokenType... tokenTypes) {
		EnumSet<ETokenType> set = toEnumSet(tokenTypes);
		startIndex = Math.max(0, startIndex);
		endIndex = Math.min(endIndex, tokens.size());

		for (int i = startIndex; i < endIndex; ++i) {
			if (set.contains(tokens.get(i).getType())) {
				return i;
			}
		}

		return NOT_FOUND;
	}

	/**
	 * Converts the given token types array into an enum set.
	 */
	private static EnumSet<ETokenType> toEnumSet(ETokenType... tokenTypes) {
		EnumSet<ETokenType> set = EnumSet.noneOf(ETokenType.class);
		set.addAll(Arrays.asList(tokenTypes));
		return set;
	}

	/**
	 * Returns the index of the last token of given token type (or
	 * {@link #NOT_FOUND}).
	 */
	public static int findLast(List<IToken> tokens, ETokenType... tokenTypes) {
		return findLast(tokens, 0, tokenTypes);
	}

	/**
	 * Returns the index of the last token of given token type (or
	 * {@link #NOT_FOUND}).
	 */
	public static int findLast(List<IToken> tokens,
			EnumSet<ETokenType> tokenTypes) {
		return findLast(tokens, 0, tokenTypes);
	}

	/**
	 * Returns the index of the last token of given token type not before the
	 * start index (or {@link #NOT_FOUND}).
	 */
	public static int findLast(List<IToken> tokens, int startIndex,
			ETokenType... tokenTypes) {
		return findLast(tokens, startIndex, tokens.size(), tokenTypes);
	}

	/**
	 * Returns the index of the last token of given token type not before the
	 * start index (or {@link #NOT_FOUND}).
	 */
	public static int findLast(List<IToken> tokens, int startIndex,
			EnumSet<ETokenType> tokenTypes) {
		return findLast(tokens, startIndex, tokens.size(), tokenTypes);
	}

	/**
	 * Returns the index of the last token of given token type not before the
	 * start index and before the end index (or {@link #NOT_FOUND}).
	 */
	public static int findLast(List<IToken> tokens, int startIndex,
			int endIndex, ETokenType... tokenTypes) {
		return findLast(tokens, startIndex, endIndex, toEnumSet(tokenTypes));
	}

	/**
	 * Returns the index of the last token of given token type not before the
	 * start index and before the end index (or {@link #NOT_FOUND}).
	 */
	public static int findLast(List<IToken> tokens, int startIndex,
			int endIndex, EnumSet<ETokenType> tokenTypes) {
		startIndex = Math.max(0, startIndex);
		endIndex = Math.min(endIndex, tokens.size());

		for (int i = endIndex - 1; i >= startIndex; --i) {
			if (tokenTypes.contains(tokens.get(i).getType())) {
				return i;
			}
		}

		return NOT_FOUND;
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a token of
	 * <code>tokenType<code>, false otherwise.
	 */
	public static boolean tokenStreamContains(List<IToken> tokens,
			ETokenType tokenType) {
		return tokenStreamContains(tokens, 0, tokens.size(), tokenType);
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a token of
	 * <code>tokenType<code> in the given range, false otherwise.
	 */
	public static boolean tokenStreamContains(List<IToken> tokens,
			int startIndex, int endIndex, ETokenType tokenType) {
		return find(tokens, startIndex, endIndex, tokenType) != NOT_FOUND;
	}

	/**
	 * Finds any of the token types in the given tokens.
	 * 
	 * @return The index of the first token that matches one of the given token
	 *         types. If no token has one of the types {@link #NOT_FOUND} is
	 *         returned.
	 */
	public static int findAny(List<IToken> tokens, int startIndex,
			ETokenType... tokenTypes) {
		return findAny(tokens, startIndex, toEnumSet(tokenTypes));
	}

	/**
	 * Finds any of the token types in the given tokens.
	 * 
	 * @return The index of the first token that matches one of the given token
	 *         types. If no token has one of the types {@link #NOT_FOUND} is
	 *         returned.
	 */
	public static int findAny(List<IToken> tokens, int startIndex,
			EnumSet<ETokenType> tokenTypes) {
		for (int i = startIndex; i < tokens.size(); ++i) {
			if (tokenTypes.contains(tokens.get(i).getType())) {
				return i;
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a any token of
	 * given <code>tokenTypes<code>, false otherwise.
	 */
	public static boolean containsAny(List<IToken> tokens,
			ETokenType... tokenTypes) {
		return containsAny(tokens, 0, tokens.size(), tokenTypes);
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a any token of
	 * given <code>tokenTypes<code> in the given range, false otherwise.
	 */
	public static boolean containsAny(List<IToken> tokens, int startIndex,
			int endIndex, ETokenType... tokenTypes) {
		return containsAny(tokens, startIndex, endIndex, toEnumSet(tokenTypes));
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a any token of
	 * given <code>tokenTypes<code> in the given range, false otherwise.
	 */
	public static boolean containsAny(List<IToken> tokens, int startIndex,
			int endIndex, EnumSet<ETokenType> tokenTypes) {
		if (tokenTypes.isEmpty()) {
			return false;
		}

		for (int i = startIndex; i < endIndex; ++i) {
			if (tokenTypes.contains(tokens.get(i).getType())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if the given token stream contains one of the
	 * tokens in the given set.
	 */
	public static boolean containsAny(List<IToken> tokens,
			EnumSet<ETokenType> tokenTypeSet) {
		return containsAny(tokens, 0, tokens.size(), tokenTypeSet);
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains at least one
	 * token of each of the given <code>tokenTypes<code>, false otherwise.
	 */
	public static boolean containsAll(List<IToken> tokens,
			ETokenType... tokenTypes) {
		return containsAll(tokens, 0, tokens.size(), tokenTypes);
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains at least one
	 * token of each of the given <code>tokenTypes<code> in the given range,
	 * false otherwise.
	 */
	public static boolean containsAll(List<IToken> tokens, int startIndex,
			int endIndex, ETokenType... tokenTypes) {
		if (tokenTypes.length == 0) {
			return true;
		}

		EnumSet<ETokenType> types = EnumSet.of(tokenTypes[0], tokenTypes);
		for (int i = startIndex; i < endIndex; ++i) {
			types.remove(tokens.get(i).getType());
			if (types.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a consecutive
	 * subsequence of tokens that match the sequence of token types., false
	 * otherwise.
	 */
	public static boolean containsSequence(List<IToken> tokens, int startIndex,
			int endIndex, ETokenType... tokenTypes) {
		OUTER: for (int i = startIndex; i <= endIndex
				- tokenTypes.length; ++i) {
			for (int j = 0; j < tokenTypes.length; ++j) {
				if (tokens.get(i + j).getType() != tokenTypes[j]) {
					continue OUTER;
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Returns <code>true</code> if the list of tokens contains a token of
	 * <code>tokenClass<code>, false otherwise.
	 */
	public static boolean tokenStreamContains(List<IToken> tokens,
			ETokenClass tokenClass) {
		for (IToken token : tokens) {
			if (token.getType().getTokenClass() == tokenClass) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns the sublist of tokens between the first occurrence of given start
	 * token type and the first occurrence of end token type (after start). If
	 * one of them is not found, an empty list is returned. The tokens for the
	 * start and end are not included in the returned sub list.
	 */
	public static List<IToken> tokensBetween(List<IToken> tokens,
			ETokenType startType, ETokenType endType) {
		int start = TokenStreamUtils.find(tokens, startType);
		if (start == NOT_FOUND) {
			return CollectionUtils.emptyList();
		}
		start += 1;

		int end = TokenStreamUtils.find(tokens, start, endType);
		if (end == NOT_FOUND) {
			return CollectionUtils.emptyList();
		}

		return tokens.subList(start, end);
	}

	/**
	 * Returns the offset of the first token of closingType, thereby counting
	 * nesting occurrences of openingType and closingType. Returns
	 * {@link RecognizerBase#NO_MATCH} if none was found.
	 * 
	 * @param currentOffset
	 *            this is the offset to start the search and must be one token
	 *            <b>after</b> the opening token for which the closing token
	 *            shall be found.
	 */
	public static int findMatchingClosingToken(List<IToken> tokens,
			int currentOffset, ETokenType openingType, ETokenType closingType) {
		int nesting = 1;
		for (; currentOffset < tokens.size(); ++currentOffset) {
			ETokenType tokenType = tokens.get(currentOffset).getType();
			if (tokenType == openingType) {
				nesting += 1;
			} else if (tokenType == closingType) {
				nesting -= 1;
				if (nesting == 0) {
					return currentOffset;
				}
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns the index of the first top-level token that has the given search
	 * type regarding the nesting introduced by the opening and closing types.
	 * If no such token is found {@link #NOT_FOUND} is returned.
	 */
	public static int findFirstTopLevel(List<IToken> tokens,
			ETokenType searchType, List<ETokenType> openingTypes,
			List<ETokenType> closingTypes) {
		return findFirstTopLevel(tokens, EnumSet.of(searchType), openingTypes,
				closingTypes);
	}

	/**
	 * Returns the index of the first top-level token that has one of the given
	 * search types regarding the nesting introduced by the opening and closing
	 * types. If no such token is found {@link #NOT_FOUND} is returned.
	 */
	public static int findFirstTopLevel(List<IToken> tokens,
			EnumSet<ETokenType> searchTypes, List<ETokenType> openingTypes,
			List<ETokenType> closingTypes) {
		return findFirstTopLevel(tokens, 0, searchTypes, openingTypes,
				closingTypes);
	}

	/**
	 * Returns the index of the first top-level token starting at the given
	 * index that has one of the given search types regarding the nesting
	 * introduced by the opening and closing types. If no such token is found
	 * {@link #NOT_FOUND} is returned.
	 */
	public static int findFirstTopLevel(List<IToken> tokens, int startIndex,
			EnumSet<ETokenType> searchTypes, List<ETokenType> openingTypes,
			List<ETokenType> closingTypes) {
		for (NestingAwareTokenIterator iterator = new NestingAwareTokenIterator(
				tokens, startIndex, openingTypes, closingTypes); iterator
						.hasNext();) {
			IToken token = iterator.next();
			if (iterator.isTopLevel()
					&& searchTypes.contains(token.getType())) {
				return iterator.getCurrentIndex();
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns the offset of the first token of openingType, thereby counting
	 * nesting occurrences of openingType and closingType. Returns
	 * {@link RecognizerBase#NO_MATCH} if none was found.
	 * 
	 * @param currentOffset
	 *            this is the offset to start the search and must be one token
	 *            <b>after</b> the opening token for which the closing token
	 *            shall be found.
	 */
	public static int findMatchingOpeningToken(List<IToken> tokens,
			int currentOffset, ETokenType openingType, ETokenType closingType) {
		int openBraces = 1;
		for (; currentOffset >= 0; currentOffset--) {
			ETokenType currentTokenType = tokens.get(currentOffset).getType();
			if (currentTokenType.equals(openingType)) {
				openBraces--;
			} else if (currentTokenType.equals(closingType)) {
				openBraces++;
			}
			if (openBraces == 0) {
				return currentOffset;
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns whether the next tokens starting at the given offset are of given
	 * type.
	 */
	public static boolean tokenTypesAt(List<IToken> tokens, int currentOffset,
			ETokenType... tokenTypes) {
		for (int i = 0; i < tokenTypes.length; ++i) {
			if (currentOffset + i >= tokens.size() || tokens
					.get(currentOffset + i).getType() != tokenTypes[i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns whether the given token list starts with given token type
	 * sequence.
	 */
	public static boolean startsWith(List<IToken> tokens, ETokenType... types) {
		return hasTokenTypeSequence(tokens, 0, types);
	}

	/**
	 * Returns whether the given token list ends with given token type sequence.
	 */
	public static boolean endsWith(List<IToken> tokens, ETokenType... types) {
		return hasTokenTypeSequence(tokens, tokens.size() - types.length,
				types);
	}

	/**
	 * Get the type of a variable, designed for JAVA. Mainly this means using
	 * the token prior to the variable as type. Handles arrays "int[]" and
	 * parameterized types "List&lt;Bar&gt;" and returns the braces including
	 * the types in the parameter.
	 * 
	 * @param index
	 *            points to the variable's name
	 */
	public static String getType(List<IToken> tokenList, int index) {
		int startIndex = index - 1;
		while (startIndex >= 2) {
			ETokenType startType = tokenList.get(startIndex).getType();
			if (startType == ETokenType.RBRACK) {
				startIndex -= 2;
			} else if (startType == ETokenType.GT) {
				startIndex = findMatchingOpeningToken(tokenList, startIndex - 1,
						ETokenType.LT, ETokenType.GT) - 1;
			} else if (startType == ETokenType.IDENTIFIER && startIndex > 0
					&& tokenList.get(startIndex - 1)
							.getType() == ETokenType.DOT) {
				startIndex -= 2;
			} else {
				break;
			}
		}

		StringBuilder returnTypeBuilder = new StringBuilder();
		for (int i = startIndex; i < index; i++) {
			returnTypeBuilder.append(tokenList.get(i).getText());
		}
		return returnTypeBuilder.toString();
	}

	/** Counts the number of tokens of a given type in the list of tokens */
	public static int count(List<IToken> tokens, ETokenType tokenType) {
		int counter = 0;
		for (IToken token : tokens) {
			if (token.getType().equals(tokenType)) {
				counter++;
			}
		}
		return counter;
	}

	/**
	 * Counts the number of tokens of a given type in the list of tokens that
	 * are not nested between the given nesting tokens.
	 */
	public static int countWithoutNesting(List<IToken> tokens,
			ETokenType tokenType, ETokenType openingType,
			ETokenType closingType) {
		int counter = 0;
		for (NestingAwareTokenIterator iterator = new NestingAwareTokenIterator(
				tokens, 0, Arrays.asList(openingType),
				Arrays.asList(closingType)); iterator.hasNext();) {
			IToken token = iterator.next();
			if (iterator.isTopLevel() && token.getType().equals(tokenType)) {
				counter++;
			}
		}
		return counter;
	}

	/**
	 * Turns a token stream into its text string. Tokens are separated by a
	 * single space. Thus, the resulting text is not guaranteed to be the same
	 * as the parsed text from which the tokens resulted.
	 */
	// TODO (LH) We should avoid the clash with toString()
	public static String toString(List<IToken> tokens) {
		StringBuilder sb = new StringBuilder();
		for (IToken token : tokens) {
			sb.append(token.getText()).append(StringUtils.SPACE_CHAR);
		}
		return sb.toString();
	}

	// TODO (LH) Align comment and method name
	/**
	 * Returns the indices into the token list which contain tokens of the given
	 * types.
	 */
	public static List<Integer> findAll(List<IToken> tokens,
			EnumSet<ETokenType> types) {
		return findAll(tokens, 0, tokens.size(), types);
	}

	/**
	 * Returns all indices of the given token types in the given token list,
	 * beginning from startOffset (inclusive) to endOffset (exclusive).
	 */
	public static List<Integer> findAll(List<IToken> tokens, int startOffset,
			int endOffset, EnumSet<ETokenType> types) {
		List<Integer> indices = new ArrayList<Integer>();
		for (int i = startOffset; i < endOffset; i++) {
			if (types.contains(tokens.get(i).getType())) {
				indices.add(i);
			}
		}
		return indices;
	}

	/** Splits the given token stream at the tokens of the given types. */
	public static List<List<IToken>> split(List<IToken> tokens,
			EnumSet<ETokenType> splitTypes) {
		return split(tokens, splitTypes, Integer.MAX_VALUE);
	}

	/** Splits the given token stream at the tokens of the given types. */
	public static List<List<IToken>> split(List<IToken> tokens,
			ETokenType... splitTypes) {
		return split(tokens, toEnumSet(splitTypes), Integer.MAX_VALUE);
	}

	/**
	 * Splits the given token stream at the tokens of the given types, but at
	 * most as often as given in the limit parameter. The resulting list will
	 * have at most <code>limit</code> entries). The limit must be bigger than
	 * 0.
	 */
	public static List<List<IToken>> split(List<IToken> tokens,
			EnumSet<ETokenType> splitTypes, int limit) {
		CCSMPre.isTrue(limit > 0, "The limit must be greater than 0");
		List<List<IToken>> parts = new ArrayList<List<IToken>>();
		int start = 0;

		for (int i = 0; i < tokens.size(); i++) {
			if (parts.size() == limit - 1) {
				break;
			}
			if (splitTypes.contains(tokens.get(i).getType())) {
				List<IToken> part = tokens.subList(start, i);
				parts.add(part);
				start = i + 1;
			}
		}

		parts.add(tokens.subList(start, tokens.size()));
		return parts;
	}

	/**
	 * Returns whether the given token list contains tokens with the given types
	 * in that order starting at the given start offset.
	 */
	public static boolean hasTokenTypeSequence(List<IToken> tokens,
			int startOffset, ETokenType... sequence) {
		if (startOffset + sequence.length > tokens.size() || startOffset < 0) {
			return false;
		}

		for (int i = 0; i < sequence.length; i++) {
			if (!tokens.get(i + startOffset).getType().equals(sequence[i])) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Returns the index of the first occurrence of the given sequence of token
	 * types in the given token list, beginning from the given start offset. If
	 * the sequence if not found, {@link #NOT_FOUND} is returned.
	 */
	public static int findTokenTypeSequence(List<IToken> tokens,
			int startOffset, ETokenType... sequence) {
		for (int i = startOffset; i < tokens.size() - sequence.length
				+ 1; i++) {
			if (hasTokenTypeSequence(tokens, i, sequence)) {
				return i;
			}
		}
		return NOT_FOUND;
	}

	/**
	 * Returns all indices of occurrences of the given sequence of token types
	 * in the given token list, beginning from the given offset. If the sequence
	 * is not found, an empty list is returned.
	 */
	public static List<Integer> findTokenTypeSequences(List<IToken> tokens,
			int offset, ETokenType... sequence) {
		List<Integer> indices = new ArrayList<>();

		while (offset < tokens.size() - sequence.length + 1) {
			offset = findTokenTypeSequence(tokens, offset, sequence);
			if (offset == NOT_FOUND) {
				break;
			}
			indices.add(offset);
			offset++;
		}

		return indices;
	}

	/**
	 * Returns whether the given list's token types equal the given token types.
	 */
	public static boolean hasTypes(List<IToken> tokens, ETokenType... types) {
		if (tokens.size() != types.length) {
			return false;
		}

		return hasTokenTypeSequence(tokens, 0, types);
	}

	/**
	 * Returns all tokens between the tokens of the opening and closing types
	 * regarding nesting.
	 */
	public static List<IToken> tokensBetweenWithNesting(List<IToken> tokens,
			ETokenType openingType, ETokenType closingType) {
		return tokensBetweenWithNesting(tokens, 0, openingType, closingType);
	}

	/**
	 * Returns all tokens between the tokens of the opening and closing types
	 * regarding nesting beginning from the given offset.
	 */
	public static List<IToken> tokensBetweenWithNesting(List<IToken> tokens,
			int offset, ETokenType openingType, ETokenType closingType) {
		int openingIndex = find(tokens, offset, openingType);
		if (openingIndex == NOT_FOUND) {
			return CollectionUtils.emptyList();
		}

		openingIndex += 1;
		int closingIndex = findMatchingClosingToken(tokens, openingIndex,
				openingType, closingType);
		if (closingIndex == NOT_FOUND) {
			return CollectionUtils.emptyList();
		}

		return tokens.subList(openingIndex, closingIndex);
	}

	/**
	 * Splits the given token list at the tokens of the given split type. If the
	 * split type is nested between the open and close tokens, it is ignored.
	 */
	public static List<List<IToken>> splitWithNesting(List<IToken> tokens,
			ETokenType splitType, ETokenType openingType,
			ETokenType closingType) {
		return splitWithNesting(tokens, splitType, Arrays.asList(openingType),
				Arrays.asList(closingType));
	}

	/**
	 * Splits the given token list at the tokens of the given split type. If the
	 * split type is nested between one of the open and close tokens, it is
	 * ignored. The open and close token type lists must have the same size.
	 * None of both must contain duplicates.
	 */
	public static List<List<IToken>> splitWithNesting(List<IToken> tokens,
			ETokenType splitType, List<ETokenType> openingTypes,
			List<ETokenType> closingTypes) {
		return splitWithNesting(tokens, EnumSet.of(splitType), openingTypes,
				closingTypes, Integer.MAX_VALUE);
	}

	/**
	 * Splits the given token list at the tokens of the given split types. If
	 * one of the split types is nested between one of the open and close
	 * tokens, it is ignored. The resulting list will have at most limit
	 * entries. Limit must be greater zero. The open and close token type lists
	 * must have the same size. None of both must contain duplicates.
	 */
	public static List<List<IToken>> splitWithNesting(List<IToken> tokens,
			EnumSet<ETokenType> splitTypes, List<ETokenType> openingTypes,
			List<ETokenType> closingTypes, int limit) {
		CCSMPre.isTrue(limit > 0, "The limit must be greater than 0");

		List<List<IToken>> parts = new ArrayList<List<IToken>>();
		int start = 0;
		for (NestingAwareTokenIterator iterator = new NestingAwareTokenIterator(
				tokens, 0, openingTypes, closingTypes); iterator.hasNext();) {
			IToken token = iterator.next();
			if (parts.size() == limit - 1) {
				break;
			}
			if (iterator.isTopLevel() && splitTypes.contains(token.getType())) {
				List<IToken> part = tokens.subList(start,
						iterator.getCurrentIndex());
				parts.add(part);
				start = iterator.getCurrentIndex() + 1;
			}
		}

		parts.add(tokens.subList(start, tokens.size()));
		return parts;
	}

	/**
	 * Creates a new token from the given referenceToken.
	 * 
	 * @param referenceToken
	 *            Reference token, whose offset, line number and origin id will
	 *            be copied to the new token.
	 * @param text
	 *            Text of the newly created token.
	 * @param type
	 *            Type of the newly created token.
	 * @return New token with the given text and type.
	 */
	public static IToken createToken(IToken referenceToken, String text,
			ETokenType type) {
		return referenceToken.newToken(type, referenceToken.getOffset(),
				referenceToken.getLineNumber(), text,
				referenceToken.getOriginId());
	}

	/**
	 * Creates a new token list copying the given list and using the given
	 * reference token.
	 * 
	 * @param referenceToken
	 *            Reference token, whose offset, line number and origin id will
	 *            be copied to the new tokens.
	 * @param tokens
	 *            List of tokens to be duplicated.
	 */
	public static List<IToken> copyTokens(IToken referenceToken,
			List<IToken> tokens) {
		return CollectionUtils.map(tokens, token -> TokenStreamUtils
				.createToken(referenceToken, token.getText(), token.getType()));
	}

	/**
	 * Adds a new token to the given token list at the given position with the
	 * given type and text. The token that is at that position currently is
	 * taken as a reference. The new token will have the same offset, origin ID
	 * and line number as the reference token.
	 * 
	 * @param tokens
	 *            The token list to modify. Must not be unmodifiable or empty.
	 * @param position
	 *            The position at which to insert the new token. Also the
	 *            position of the reference token. If this is equal to the size
	 *            of the token list, the new token will be appended at the end
	 *            and the last token will be taken as the reference token.
	 * @param type
	 *            The token type of the new token.
	 * @param text
	 *            The text of the new token.
	 */
	public static void addToken(List<IToken> tokens, int position,
			ETokenType type, String text) {
		CCSMAssert.isFalse(tokens.isEmpty(),
				"Cannot create new tokens without at least one existing token as a reference");
		int referencePosition = position;
		if (position == tokens.size()) {
			referencePosition--;
		}
		IToken referenceToken = tokens.get(referencePosition);
		IToken newToken = createToken(referenceToken, text, type);
		tokens.add(position, newToken);
	}

	/**
	 * Removes the last token of the given token list if it matches the
	 * specified token type.
	 * 
	 * @param tokens
	 *            The token list to modify.
	 * @param type
	 *            Token type, which should be removed.
	 * @return The token list without the given token type at the end.
	 */
	public static List<IToken> removeAtEnd(List<IToken> tokens,
			ETokenType type) {
		if (endsWith(tokens, type)) {
			tokens = tokens.subList(0, tokens.size() - 1);
		}
		return tokens;
	}

	/**
	 * Removes the first token of the given token list if it matches the
	 * specified token type.
	 * 
	 * @param tokens
	 *            The token list to modify.
	 * @param type
	 *            Token type, which should be removed.
	 * @return The token list without the given token type at the beginning.
	 */
	public static List<IToken> removeAtFront(List<IToken> tokens,
			ETokenType type) {
		if (startsWith(tokens, type)) {
			tokens = CollectionUtils.getRest(tokens);
		}
		return tokens;
	}
}
