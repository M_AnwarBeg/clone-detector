/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.groovy;

import static eu.cqse.check.framework.scanner.ETokenType.ABSTRACT;
import static eu.cqse.check.framework.scanner.ETokenType.ANNOTATION_INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.ARROW;
import static eu.cqse.check.framework.scanner.ETokenType.AS;
import static eu.cqse.check.framework.scanner.ETokenType.ASSERT;
import static eu.cqse.check.framework.scanner.ETokenType.AT_OPERATOR;
import static eu.cqse.check.framework.scanner.ETokenType.BOOLEAN;
import static eu.cqse.check.framework.scanner.ETokenType.BOOLEAN_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.BREAK;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CATCH;
import static eu.cqse.check.framework.scanner.ETokenType.CHAR;
import static eu.cqse.check.framework.scanner.ETokenType.CHARACTER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.DEF;
import static eu.cqse.check.framework.scanner.ETokenType.DEFAULT;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ENUM;
import static eu.cqse.check.framework.scanner.ETokenType.FINAL;
import static eu.cqse.check.framework.scanner.ETokenType.FINALLY;
import static eu.cqse.check.framework.scanner.ETokenType.FLOAT;
import static eu.cqse.check.framework.scanner.ETokenType.FLOATING_POINT_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.GOTO;
import static eu.cqse.check.framework.scanner.ETokenType.GT;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPORT;
import static eu.cqse.check.framework.scanner.ETokenType.INT;
import static eu.cqse.check.framework.scanner.ETokenType.INTEGER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LONG;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.LT;
import static eu.cqse.check.framework.scanner.ETokenType.MINUS;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSMINUS;
import static eu.cqse.check.framework.scanner.ETokenType.MULT;
import static eu.cqse.check.framework.scanner.ETokenType.NATIVE;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.NULL_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.PACKAGE;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSPLUS;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SHORT;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.STRICTFP;
import static eu.cqse.check.framework.scanner.ETokenType.STRING_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.SUPER;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.SYNCHRONIZED;
import static eu.cqse.check.framework.scanner.ETokenType.THIS;
import static eu.cqse.check.framework.scanner.ETokenType.THROW;
import static eu.cqse.check.framework.scanner.ETokenType.THROWS;
import static eu.cqse.check.framework.scanner.ETokenType.TRAIT;
import static eu.cqse.check.framework.scanner.ETokenType.TRANSIENT;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.VOID;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.shallowparser.languages.groovy.EGroovyShallowParserStates.IN_ENUM_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.groovy.EGroovyShallowParserStates.IN_EXPRESSION;
import static eu.cqse.check.framework.shallowparser.languages.groovy.EGroovyShallowParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.groovy.EGroovyShallowParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.groovy.EGroovyShallowParserStates.TOP_LEVEL;

import java.util.Arrays;
import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;

/**
 * A Groovy shallow parser following the language specifications found at:
 * <a href="http://www.groovy-lang.org/documentation.html">http://www.
 * groovy-lang.org/documentation.html</a>. <br>
 * The groovy repository can be found at:
 * <a href="https://github.com/apache/groovy/">https://github.com/apache/groovy/
 * </a>.
 * 
 * <br>
 * A few notes:
 * <ul>
 * <li>This parser recognizes all types (classes, enums, interfaces,
 * annotations, traits), methods, attributes, local variables and control
 * structures (if, try, switch, loops).</li>
 * <li>The nodes for import, package, annotations, case, default and labels are
 * reported as meta entities.</li>
 * <li>Closures are reported as anonymous functions.</li>
 * <li>Closures are parsed as well, as they are very versatile on Groovy.</li>
 * <li>Anonymous inner classes and closures are also detected within expressions
 * and arrays.</li>
 * </ul>
 * 
 * <br>
 * Known Issues:
 * <ul>
 * <li>Groovy allows optional parenthesis for method calls. This means that
 * constructs like 'println a' (which normally stands for 'println(a)') cannot
 * be distinguished from local variables. This parser creates local variables in
 * these cases.</li>
 * <li>Constructors and methods can not be reliably distinguished. E.g.
 * ACCESS_MODIFIER IDENTIFIER () {...} is only a constructor if the identifier
 * has the same name as the type. Therefore we prefer matching methods in
 * ambiguous cases.</li>
 * </ul>
 *
 * @author $Author: baumeister $
 * @version $Rev: 52657 $
 * @ConQAT.Rating YELLOW Hash: 0D801EEB5698E34927A31C7287A34846
 */
public class GroovyShallowParser
		extends ShallowParserBase<EGroovyShallowParserStates> {

	/** All primitive data types. */
	private static final EnumSet<ETokenType> PRIMITIVE_DATATYPES = EnumSet
			.of(BOOLEAN, CHAR, SHORT, INT, LONG, FLOAT, DOUBLE);

	/** All literal types. */
	private static final EnumSet<ETokenType> LITERALS = EnumSet.of(
			BOOLEAN_LITERAL, INTEGER_LITERAL, FLOATING_POINT_LITERAL,
			NULL_LITERAL, STRING_LITERAL, CHARACTER_LITERAL);

	/** All access modifiers. */
	private static final EnumSet<ETokenType> ACCESS_MODIFIERS = EnumSet
			.of(PRIVATE, PROTECTED, PUBLIC);

	/** All type modifiers. */
	private static final EnumSet<ETokenType> TYPE_MODIFIERS = EnumSet
			.of(ABSTRACT, STATIC, FINAL, STRICTFP);

	/** All method modifiers. */
	private static final EnumSet<ETokenType> METHOD_MODIFIERS = EnumSet.of(
			ABSTRACT, STATIC, FINAL, NATIVE, SYNCHRONIZED, TRANSIENT, STRICTFP);

	static {
		TYPE_MODIFIERS.addAll(ACCESS_MODIFIERS);
		METHOD_MODIFIERS.addAll(ACCESS_MODIFIERS);
	}

	/** Constructor. */
	public GroovyShallowParser() {
		super(EGroovyShallowParserStates.class, TOP_LEVEL);
		createMetaRules();
		createInitializerRules();
		createTypeRules();
		createFieldRules();
		createEnumLiteralRule();
		createMethodDefinitionRules();
		createConstructorDefinitionRule();
		createInExpressionRules();
		createLoopRules();
		createSwitchCaseRules();
		createContinuationRules();
		createStatementRule();
		createEmptyStatementRule();
	}

	/** Creates various rules that create META shallow entities. */
	private void createMetaRules() {
		createPackageRule();
		createImportRule();
		createAnnotationRule();
		createLabelRule();
	}

	/** Creates rule for the package line. */
	private void createPackageRule() {
		inState(TOP_LEVEL).sequence(PACKAGE).markStart().sequence(IDENTIFIER)
				.repeated(DOT, EnumSet.of(IDENTIFIER, MULT))
				.createNode(EShallowEntityType.META, SubTypeNames.PACKAGE,
						new Region(0, -1))
				.optional(SEMICOLON).endNode();
	}

	/** Creates rule for imports. */
	private void createImportRule() {
		inState(TOP_LEVEL).sequence(IMPORT).optional(STATIC).markStart()
				.sequence(IDENTIFIER)
				.repeated(DOT, EnumSet.of(IDENTIFIER, MULT))
				.createNode(EShallowEntityType.META, SubTypeNames.IMPORT,
						new Region(0, -1))
				.optional(AS, IDENTIFIER).optional(SEMICOLON).endNode();
	}

	/** Creates a rule for annotations. */
	private void createAnnotationRule() {
		inAnyState().sequence(AT_OPERATOR, IDENTIFIER)
				.repeated(DOT, IDENTIFIER).createNode(EShallowEntityType.META,
						SubTypeNames.ANNOTATION, new Region(1, -1))
				.skipNested(LPAREN, RPAREN).endNode();
	}

	/** Creates a rule for labels. */
	private void createLabelRule() {
		inAnyState().sequence(IDENTIFIER, COLON)
				.createNode(EShallowEntityType.META, SubTypeNames.LABEL, -2)
				.endNode();
	}

	/**
	 * Creates rules that recognize static and non-static initializer blocks.
	 */
	private void createInitializerRules() {
		inState(IN_TYPE, IN_ENUM_TYPE).sequence(STATIC).sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD, "static initializer",
						"<sinit>")
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();

		inState(IN_TYPE, IN_ENUM_TYPE).sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD, "non-static initializer",
						"<init>")
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();
	}

	/**
	 * Creates rules for all types. These are CLASS, INTERFACE,
	 * ANNOTATION_INTERFACE, TRAIT and ENUM.
	 */
	private void createTypeRules() {
		createTypeRule(CLASS, SubTypeNames.CLASS, IN_TYPE);
		createTypeRule(INTERFACE, SubTypeNames.INTERFACE, IN_TYPE);
		createTypeRule(ANNOTATION_INTERFACE, SubTypeNames.ANNOTATION, IN_TYPE);
		createTypeRule(TRAIT, SubTypeNames.TRAIT, IN_TYPE);
		createTypeRule(ENUM, SubTypeNames.ENUM, IN_ENUM_TYPE);
	}

	/** Creates rule for one given type. */
	private void createTypeRule(ETokenType type, String subTypeName,
			EGroovyShallowParserStates subState) {
		inState(TOP_LEVEL, IN_TYPE, IN_ENUM_TYPE).repeated(TYPE_MODIFIERS)
				.sequence(type).sequence(IDENTIFIER)
				.createNode(EShallowEntityType.TYPE, subTypeName, -1)
				.skipTo(LBRACE).parseUntil(subState).sequence(RBRACE).endNode();
	}

	/** Creates rules for enum literals. */
	private void createEnumLiteralRule() {
		RecognizerBase<EGroovyShallowParserStates> enumBase = inState(
				IN_ENUM_TYPE)
						.preCondition(
								new GroovyPrecedingEnumLiteralRecognizer())
						.markStart().sequence(IDENTIFIER)
						.createNode(EShallowEntityType.ATTRIBUTE,
								SubTypeNames.ENUM_LITERAL, 0)
						.skipNested(LPAREN, RPAREN,
								createSubExpressionRecognizer());

		// A(...){<IN_TYPE>}, B{<IN_TYPE>}
		enumBase.sequence(LBRACE).parseUntil(IN_TYPE).sequence(RBRACE)
				.optional(EnumSet.of(COMMA, SEMICOLON)).endNode();

		// A(...),B,C
		enumBase.optional(EnumSet.of(COMMA, SEMICOLON)).endNode();

	}

	/** Creates rules for methods. */
	private void createMethodDefinitionRules() {
		createMethodDefinitionRulesInState(TOP_LEVEL);
		createMethodDefinitionRulesInState(IN_TYPE);
		createMethodDefinitionRulesInState(IN_ENUM_TYPE);
	}

	/** Creates rules for methods in a specific state. */
	private void createMethodDefinitionRulesInState(
			EGroovyShallowParserStates state) {

		EnumSet<ETokenType> returnTypes = EnumSet.of(VOID, DEF, IDENTIFIER);
		returnTypes.addAll(PRIMITIVE_DATATYPES);

		// Methods with at least one modifier and return types.
		RecognizerBase<EGroovyShallowParserStates> baseWithModifier = inState(
				state).sequence(METHOD_MODIFIERS).repeated(METHOD_MODIFIERS)
						.skipNested(LT, GT).sequence(returnTypes)
						.repeated(DOT, returnTypes).skipNested(LT, GT)
						.skipAny(EnumSet.of(LBRACK, RBRACK));

		endMethodDefinitionRule(baseWithModifier, state);

		// Methods with at least one return type.
		RecognizerBase<EGroovyShallowParserStates> baseWithReturnType = inState(
				state).repeated(METHOD_MODIFIERS).skipNested(LT, GT)
						.sequence(returnTypes).repeated(DOT, returnTypes)
						.skipNested(LT, GT).skipAny(EnumSet.of(LBRACK, RBRACK));

		endMethodDefinitionRule(baseWithReturnType, state);

		// Methods guaranteed without return type. Need at least one modifier.
		RecognizerBase<EGroovyShallowParserStates> baseWithoutReturnType = inState(
				state).sequence(METHOD_MODIFIERS).repeated(METHOD_MODIFIERS)
						.skipNested(LT, GT);

		endMethodDefinitionRule(baseWithoutReturnType, state);
	}

	/**
	 * Ends a method definition rule. i.e. matches every method beginning with
	 * its identifier and creates the shallow entity.
	 */
	private static void endMethodDefinitionRule(
			RecognizerBase<EGroovyShallowParserStates> base,
			EGroovyShallowParserStates state) {

		// Matches 'methodIdentifier(...)'.
		RecognizerBase<EGroovyShallowParserStates> methodDef = base.markStart()
				.sequence(IDENTIFIER).sequence(LPAREN)
				.skipToWithNesting(RPAREN, LPAREN, RPAREN);

		// Matches 'throws java.io.IOException, java.io.IOError'.
		RecognizerBase<EGroovyShallowParserStates> throwsMethodDef = methodDef
				.sequence(THROWS).sequence(IDENTIFIER)
				.repeated(EnumSet.of(COMMA, DOT), IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.METHOD, 0);

		// Non abstract methods continue with '{...}'
		throwsMethodDef.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();

		// Non abstract methods continue with '{...}'
		methodDef.sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.METHOD, 0)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();

		// Abstract methods are methods without '{...}'.
		// They can only appear in types.
		if (state == EGroovyShallowParserStates.IN_TYPE) {
			methodDef.createNode(EShallowEntityType.METHOD, SubTypeNames.METHOD,
					0).optional(SEMICOLON).endNode();

			throwsMethodDef.optional(SEMICOLON).endNode();
		}
	}

	/**
	 * Creates rule for constructors that matches everything that is left after
	 * the method definition rules applied. Therefore we do not check for access
	 * modifiers (It would be recognized as method anyway).
	 */
	private void createConstructorDefinitionRule() {
		inState(IN_TYPE, IN_ENUM_TYPE).markStart().sequence(IDENTIFIER)
				.sequence(LPAREN)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.CONSTRUCTOR,
						0)
				.skipTo(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();
	}

	/** Create rules for 'for' and 'while' loops. */
	private void createLoopRules() {
		RecognizerBase<EGroovyShallowParserStates> loopBase = inState(IN_METHOD,
				TOP_LEVEL).markStart().sequence(EnumSet.of(FOR, WHILE))
						.createNode(EShallowEntityType.STATEMENT, 0)
						.skipNested(LPAREN, RPAREN,
								createSubExpressionRecognizer());

		// Loop body within braces.
		loopBase.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();

		// Single statement loop body.
		loopBase.parseOnce(IN_METHOD).endNode();

		// There is no do-while in groovy
	}

	/** Create switch case rules. */
	private void createSwitchCaseRules() {
		inState(TOP_LEVEL, IN_METHOD).sequence(SWITCH)
				.createNode(EShallowEntityType.STATEMENT, SubTypeNames.SWITCH)
				.skipNested(LPAREN, RPAREN, createSubExpressionRecognizer())
				.skipTo(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();

		// Matches 'case { it < 0 }:'.
		inState(IN_METHOD).sequence(CASE).sequenceBefore(LBRACE)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE)
				.parseOnce(IN_EXPRESSION).sequence(COLON).endNode();

		// Matches 'case [4, 5, 6, 'inList']:' and 'case ... :'.
		inState(IN_METHOD).sequence(CASE)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE)
				.skipToWithNesting(COLON, LBRACK, RBRACK).endNode();

		inState(IN_METHOD).sequence(DEFAULT)
				.createNode(EShallowEntityType.META, SubTypeNames.DEFAULT)
				.skipTo(COLON).endNode();
	}

	/**
	 * Creates rules for statements with continuation like 'if-elseif-else'
	 * statements and 'try-catch-finally'.
	 */
	private void createContinuationRules() {
		createRuleWithContinuation(IF, ELSE, ELSE, IF);
		createRuleWithContinuation(TRY, FINALLY, CATCH);
	}

	/** Creates rules for statements with continuation. */
	private void createRuleWithContinuation(ETokenType first, ETokenType last,
			ETokenType... middle) {
		RecognizerBase<EGroovyShallowParserStates> baseMid = inState(TOP_LEVEL,
				IN_METHOD).sequence((Object[]) middle);

		RecognizerBase<EGroovyShallowParserStates> baseFirstLast = inState(
				TOP_LEVEL, IN_METHOD).sequence(EnumSet.of(first, last));

		EnumSet<ETokenType> continuationTokens = EnumSet.of(last);
		continuationTokens.addAll(Arrays.asList(middle));

		endContinuationNode(baseMid, continuationTokens);
		endContinuationNode(baseFirstLast, continuationTokens);
	}

	/**
	 * Creates the node, skips the parenthesis, appends either a single
	 * statement or a block of statements and ends the node with a possible
	 * continuation.
	 */
	private void endContinuationNode(
			RecognizerBase<EGroovyShallowParserStates> baseRecognizer,
			EnumSet<ETokenType> continuationTokens) {
		baseRecognizer = baseRecognizer
				.createNode(EShallowEntityType.STATEMENT, new Region(0, -1))
				.skipNested(LPAREN, RPAREN, createSubExpressionRecognizer());

		RecognizerBase<EGroovyShallowParserStates> baseMultipleStatement = baseRecognizer
				.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE);
		RecognizerBase<EGroovyShallowParserStates> baseSingleStatement = baseRecognizer
				.parseOnce(IN_METHOD);

		endWithPossibleContinuation(baseMultipleStatement, continuationTokens);
		endWithPossibleContinuation(baseSingleStatement, continuationTokens);
	}

	/**
	 * Create rule for fields and attributes. This unfortunately also detects
	 * constructs like 'println a' (== 'println(a)') as fields, because the
	 * parenthesis in method calls are optional.
	 */
	private void createFieldRules() {
		createFieldRules(EShallowEntityType.ATTRIBUTE, SubTypeNames.ATTRIBUTE,
				IN_TYPE, IN_ENUM_TYPE);
		createFieldRules(EShallowEntityType.STATEMENT,
				SubTypeNames.LOCAL_VARIABLE, TOP_LEVEL, IN_METHOD);

	}

	/** Creates rules for a specific type of fields. */
	private void createFieldRules(EShallowEntityType shallowEntityType,
			String subtypename, EGroovyShallowParserStates... states) {

		// Fields with at least one datatype.
		RecognizerBase<EGroovyShallowParserStates> withDatatype = fieldWithDataType(
				states).preCondition(createFieldRecognizer())
						.createNode(shallowEntityType, subtypename, -1);

		// Fields with at least one modifier.
		RecognizerBase<EGroovyShallowParserStates> withModifier = inState(
				states).sequence(TYPE_MODIFIERS).repeated(TYPE_MODIFIERS)
						.sequence(IDENTIFIER)
						.preCondition(createFieldRecognizer())
						.createNode(shallowEntityType, subtypename, -1);

		// Multiple assignment fields. E.g. 'def (int i, String j) = [1, 'f']'
		RecognizerBase<EGroovyShallowParserStates> multipleAssignment = inState(
				states).sequence(DEF).skipNested(LPAREN, RPAREN)
						.preCondition(createFieldRecognizer())
						.createNode(shallowEntityType, subtypename,
								new Region(1, -1));

		skipToEndOfStatement(withDatatype);
		skipToEndOfStatement(withModifier);
		skipToEndOfStatement(multipleAssignment);

	}

	/**
	 * Creates a recognizer for starting tokens of fields that contain at least
	 * one datatype.
	 */
	private RecognizerBase<EGroovyShallowParserStates> fieldWithDataType(
			EGroovyShallowParserStates... states) {
		EnumSet<ETokenType> datatypes = EnumSet.of(IDENTIFIER, DEF, THIS);
		datatypes.addAll(PRIMITIVE_DATATYPES);

		return inState(states).repeated(TYPE_MODIFIERS).sequence(datatypes)
				.repeated(DOT, datatypes).skipNested(LT, GT)
				.sequence(IDENTIFIER);
	}

	/**
	 * Creates a recognizer that detects, if we currently parse a field (or if
	 * we are mistakenly in a simple statement). We cannot solve this with the
	 * common recognizers, as we have to check for new lines.
	 */
	private static RecognizerBase<EGroovyShallowParserStates> createFieldRecognizer() {
		return new GroovyFieldRecognizer();
	}

	/** Creates rule for simple statements. */
	private void createStatementRule() {
		EnumSet<ETokenType> statementStartTokens = EnumSet.of(IDENTIFIER, NEW,
				RETURN, THIS, CONTINUE, BREAK, GOTO, THROW, SUPER, ASSERT,
				MINUS, PLUSPLUS, MINUSMINUS);
		statementStartTokens.addAll(PRIMITIVE_DATATYPES);
		statementStartTokens.addAll(LITERALS);

		RecognizerBase<EGroovyShallowParserStates> base = inState(IN_METHOD,
				TOP_LEVEL).sequenceBefore(statementStartTokens).createNode(
						EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0);

		skipToEndOfStatement(base, true);

		// A statement may start with LPAREN for multiple assignments.
		RecognizerBase<EGroovyShallowParserStates> inParenthesis = inState(
				IN_METHOD, TOP_LEVEL).sequenceBefore(LPAREN)
						.skipNested(LPAREN, RPAREN)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.SIMPLE_STATEMENT,
								new Region(0, -1));
		skipToEndOfStatement(inParenthesis);

		// A statement may start with a LBRACK. Especially as one can omit the
		// return keyword.
		RecognizerBase<EGroovyShallowParserStates> inBrackets = inState(
				IN_METHOD, TOP_LEVEL)
						.sequenceBefore(LBRACK)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.SIMPLE_STATEMENT, 0)
						.skipNested(LBRACK, RBRACK,
								createSubExpressionRecognizer());

		skipToEndOfStatement(inBrackets);
	}

	/** Creates a rule to recognize single semicolons as empty statements. */
	private void createEmptyStatementRule() {
		inAnyState().sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT)
				.endNode();

	}

	/**
	 * Appends the {@link GroovySkipToEndOfStatementRecognizer} to the given
	 * {@link RecognizerBase} and closes the node.
	 */
	private static void skipToEndOfStatement(
			RecognizerBase<EGroovyShallowParserStates> base) {
		skipToEndOfStatement(base, false);
	}

	/**
	 * Appends the {@link GroovySkipToEndOfStatementRecognizer} to the given
	 * {@link RecognizerBase} and closes the node. Additionally set flag if at
	 * least one match should be forced.
	 */
	private static void skipToEndOfStatement(
			RecognizerBase<EGroovyShallowParserStates> base,
			boolean forceMatch) {
		GroovySkipToEndOfStatementRecognizer recognizer = new GroovySkipToEndOfStatementRecognizer();
		recognizer.setForceMatch(forceMatch);
		base.subRecognizer(recognizer, 0, 1).endNode();
	}

	/** Creates the rule for anonymous inner classes and closures. */
	private void createInExpressionRules() {

		// Anonymous inner classes
		inState(IN_EXPRESSION).sequence(NEW, IDENTIFIER)
				.repeated(DOT, IDENTIFIER).skipNested(LT, GT)
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.ANONYMOUS_CLASS, 1)
				.skipNested(LPAREN, RPAREN, createSubExpressionRecognizer())
				.sequence(LBRACE).parseUntil(IN_TYPE).sequence(RBRACE)
				.endNode();

		// Closures
		RecognizerBase<EGroovyShallowParserStates> closureBase = inState(
				IN_EXPRESSION).sequence(LBRACE).createNode(
						EShallowEntityType.STATEMENT,
						SubTypeNames.ANONYMOUS_FUNCTION);

		closureBase.skipBefore(EnumSet.of(ARROW, LBRACE)).sequence(ARROW)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode(); // with arrow

		closureBase.parseUntil(IN_METHOD).sequence(RBRACE).endNode(); // without
																		// arrow
	}

	/**
	 * Creates a recognizer, that matches anonymous classes and closures within
	 * expressions.
	 */
	private static RecognizerBase<EGroovyShallowParserStates> createSubExpressionRecognizer() {
		return new GroovySubExpressionRecognizer();
	}
}
