/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.php;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Recognizer for PHP statements that may be found within expressions.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53318 $
 * @ConQAT.Rating YELLOW Hash: 11EA36B567D3BB23694972AAF45734AD
 */
/* package */class PhpSubExpressionRecognizer
		extends RecognizerBase<EGenericParserStates> {

	/**
	 * Locally matches for either "function" (indicates an anonymous function)
	 * of "new class" (indicates an anonymous inner class). If one of them is
	 * found, the parser state is switched to IN_EXPRESSION.
	 */
	@Override
	protected int matchesLocally(ParserState<EGenericParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		if (startOffset < 0 || startOffset + 1 >= tokens.size()) {
			return NO_MATCH;
		}

		ETokenType tokenType = tokens.get(startOffset).getType();
		if (tokenType == ETokenType.FUNCTION) {
			// We expect an anonymous function.
			return parserState.parse(EGenericParserStates.IN_EXPRESSION, tokens,
					startOffset);
		}

		ETokenType tokenTypeNext = tokens.get(startOffset + 1).getType();
		if (tokenType == ETokenType.NEW && tokenTypeNext == ETokenType.CLASS) {
			// We expect an anonymous inner class.
			return parserState.parse(EGenericParserStates.IN_EXPRESSION, tokens,
					startOffset);
		}

		return NO_MATCH;
	}
}
