/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.ENUM;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.EXTERN;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MULT;
import static eu.cqse.check.framework.scanner.ETokenType.PREPROCESSOR_DIRECTIVE;
import static eu.cqse.check.framework.scanner.ETokenType.PREPROCESSOR_INCLUDE;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.STRUCT;
import static eu.cqse.check.framework.scanner.ETokenType.TEMPLATE;
import static eu.cqse.check.framework.scanner.ETokenType.UNION;
import static eu.cqse.check.framework.scanner.ETokenType.USING;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.TOP_LEVEL;

import java.util.EnumSet;

import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.CStyleShallowParserRuleProviderBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Provides the meta rules for the {@link CppShallowParser}.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56480 $
 * @ConQAT.Rating YELLOW Hash: 8F1E6A08168DA407F99A4266BA98EF06
 */
/* package */class CppShallowParserMetaRules
		extends CStyleShallowParserRuleProviderBase<CppShallowParser> {

	/** Constructor. */
	public CppShallowParserMetaRules(CppShallowParser delegateParser) {
		super(delegateParser);
	}

	/** {@inheritDoc} */
	@Override
	public void contributeRules() {
		// preprocessor directives
		inAnyState()
				.sequence(EnumSet.of(PREPROCESSOR_DIRECTIVE,
						PREPROCESSOR_INCLUDE))
				.createNode(EShallowEntityType.META, 0).endNode();

		// template declaration
		inAnyState().sequence(TEMPLATE).createNode(EShallowEntityType.META, 0)
				.subRecognizer(new CppSkipTemplateSpecificationRecognizer(), 0,
						1)
				.endNode();

		// forward declarations
		inState(TOP_LEVEL, IN_TYPE)
				.sequence(EnumSet.of(CLASS, STRUCT, ENUM, UNION))
				.optional(CLASS)
				.subRecognizer(delegateParser.createScopeRecognizer(), 0,
						Integer.MAX_VALUE)
				.markStart().sequence(IDENTIFIER)
				.subRecognizer(new CppSkipTemplateSpecificationRecognizer(), 0,
						1)
				.sequence(SEMICOLON)
				.createNode(EShallowEntityType.META, "forward declaration", 0)
				.endNode();

		// friend
		RecognizerBase<EGenericParserStates> usingAlternative = inAnyState()
				.sequence(USING);
		usingAlternative.sequence(IDENTIFIER, EQ)
				.createNode(EShallowEntityType.META, 0, 1).skipTo(SEMICOLON)
				.endNode();
		usingAlternative.createNode(EShallowEntityType.META, 0)
				.skipTo(SEMICOLON).endNode();

		inAnyState().sequence(EnumSet.of(PUBLIC, PROTECTED, PRIVATE))
				.createNode(EShallowEntityType.META, 0).skipTo(COLON).endNode();

		createExternCRules();
		createAssemblyRules();
	}

	/** Creates the rules for extern "C" constructs. */
	private void createExternCRules() {
		RecognizerBase<EGenericParserStates> externCAlternative = inState(
				TOP_LEVEL).sequence(EXTERN).sequence(new CStringMatcher());
		externCAlternative.sequence(LBRACE)
				.createNode(EShallowEntityType.META, "extern C block")
				.parseUntil(TOP_LEVEL).sequence(RBRACE).endNode();
		externCAlternative
				.createNode(EShallowEntityType.META, "extern C prefix")
				.endNode();
	}

	/** Create rules for inline assembly blocks. */
	private void createAssemblyRules() {
		inState(IN_METHOD).sequence(new ASMIdentifierMatcher(), LBRACE)
				.createNode(EShallowEntityType.META, "inline assembler block")
				.skipTo(RBRACE).endNode();
		inState(TOP_LEVEL)
				.sequence(new ASMIdentifierMatcher(),
						CppShallowParser.PRIMITIVE_TYPES)
				.optional(MULT).markStart().sequence(IDENTIFIER, LPAREN)
				.createNode(EShallowEntityType.METHOD,
						"inline assembler function", 0)
				.skipTo(RPAREN).skipTo(LBRACE).skipTo(RBRACE).endNode();
	}
}
