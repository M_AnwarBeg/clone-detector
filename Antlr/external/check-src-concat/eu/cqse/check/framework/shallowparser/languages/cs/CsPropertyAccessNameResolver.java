/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cs;

import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.INameResolver;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Resolves the name of a getter or setter for a CS property.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56492 $
 * @ConQAT.Rating YELLOW Hash: BA07D50F4E8329D085AD08F66B15DDF2
 */
public class CsPropertyAccessNameResolver
		implements INameResolver<EGenericParserStates> {

	/** {@inheritDoc} */
	@Override
	public String resolveName(ParserState<EGenericParserStates> state,
			List<IToken> tokens, int startOffset) {
		String propertyName = state.getCurrentEntityName();
		ETokenType type = tokens.get(state.getCurrentReferencePosition())
				.getType();
		switch (type) {
		case GET:
			return "getter for " + propertyName;
		case SET:
			return "setter for " + propertyName;
		default:
			CCSMAssert.fail("Illegal method type.");
			return null;
		}
	}
}
