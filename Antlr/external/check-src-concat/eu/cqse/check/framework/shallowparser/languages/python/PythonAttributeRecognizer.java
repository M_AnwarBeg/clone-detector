/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.python;

import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;

/**
 * Recognizer for class level attributes in Python.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: F9BC16219E5465AEB28871C2B4EA1FB8
 */
public class PythonAttributeRecognizer extends PythonSimpleStatementRecognizer {

	/** {@inheritDoc} */
	@Override
	protected EShallowEntityType getEntityType() {
		return EShallowEntityType.ATTRIBUTE;
	}

	/** {@inheritDoc} */
	@Override
	protected String getEntitySubtypeName() {
		return SubTypeNames.ATTRIBUTE;
	}
}
