/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.base;

import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserRuleProviderBase;

/**
 * Base class for rule providers for parsers with C-style syntax.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: DC11562ABFCE9909D64D9948E3FE737A
 */
public abstract class CStyleShallowParserRuleProviderBase<PARSER extends CStyleShallowParserBase>
		extends ShallowParserRuleProviderBase<EGenericParserStates, PARSER> {

	/** Constructor. */
	protected CStyleShallowParserRuleProviderBase(PARSER delegateParser) {
		super(delegateParser);
	}

	/**
	 * Creates a recognizer that matches all valid types, starting from the
	 * given state.
	 */
	protected RecognizerBase<EGenericParserStates> typePatternInState(
			EGenericParserStates... states) {
		return delegateParser.typePatternInState(states);
	}
}
