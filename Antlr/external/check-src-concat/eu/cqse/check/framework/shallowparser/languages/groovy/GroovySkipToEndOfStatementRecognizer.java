/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.groovy;

import static eu.cqse.check.framework.scanner.ETokenType.BACKSLASH;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSMINUS;
import static eu.cqse.check.framework.scanner.ETokenType.NOT;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSPLUS;
import static eu.cqse.check.framework.scanner.ETokenType.QUESTION;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;

/**
 * Recognizer for simple statements in Groovy. We need a separate recognizer as
 * the rules for statement continuation are non-trivial due to the optional
 * semicolon.
 * 
 * @author $Author: baumeister $
 * @version $Rev: 53318 $
 * @ConQAT.Rating YELLOW Hash: 88F19614B7EC3A04383739F632275FAB
 */
/* package */class GroovySkipToEndOfStatementRecognizer
		extends RecognizerBase<EGroovyShallowParserStates> {

	/** Token types that force a statement to continue even in a new line. */
	private static final EnumSet<ETokenType> CONTINUE_TOKENS = EnumSet
			.of(BACKSLASH, DOT, QUESTION, COLON, NOT);

	/**
	 * Token types of class OPERATOR that do not force a statement continuation.
	 */
	private static final EnumSet<ETokenType> NON_CONTINUATION_OPERATORS = EnumSet
			.of(MINUSMINUS, PLUSPLUS);

	/** Matched tokens for nesting in complex Xtend statements. */
	private final static Map<ETokenType, ETokenType> NESTING_MATCH = new EnumMap<ETokenType, ETokenType>(
			ETokenType.class);

	static {
		NESTING_MATCH.put(LPAREN, RPAREN);
		NESTING_MATCH.put(LBRACK, RBRACK);
	}

	/**
	 * Defines if we should include the lastToken into the matching decision
	 * from the start on.
	 */
	private boolean forceMatch = false;

	/** @see #forceMatch */
	public void setForceMatch(boolean forceMatch) {
		this.forceMatch = forceMatch;
	}

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(
			ParserState<EGroovyShallowParserStates> parserState,
			List<IToken> tokens, int startOffset) {

		if (startOffset < 0) {
			return NO_MATCH;
		}

		IToken lastToken = null;
		if (!forceMatch && startOffset > 0) {
			lastToken = tokens.get(startOffset - 1);
		}

		Stack<ETokenType> expectedClosing = new Stack<ETokenType>();

		while (true) {
			if (startOffset >= tokens.size()) {
				return startOffset;
			}

			IToken token = tokens.get(startOffset);
			ETokenType tokenType = token.getType();

			if (!expectedClosing.isEmpty()
					&& tokenType == expectedClosing.peek()) {
				expectedClosing.pop();
			} else if (expectedClosing.isEmpty() && tokenType == SEMICOLON) {
				return startOffset + 1;
			} else if (expectedClosing.isEmpty()
					&& startsNewStatement(token, lastToken)) {
				return startOffset;
			} else if (NESTING_MATCH.containsKey(tokenType)) {
				expectedClosing.push(NESTING_MATCH.get(tokenType));
			} else {
				int next = startSubExpressionRecognizer(tokens, startOffset,
						parserState);
				startOffset = next;
				lastToken = tokens.get(startOffset - 1);
				continue;
			}

			lastToken = token;
			startOffset += 1;
		}
	}

	/**
	 * Directly starts a subparse. Returns either the new offset from the
	 * subparsers or increments the offset, if no subparse was triggered.
	 */
	protected int startSubExpressionRecognizer(List<IToken> tokens,
			int startOffset,
			ParserState<EGroovyShallowParserStates> parserState) {
		GroovySubExpressionRecognizer subExpressionRecognizer = new GroovySubExpressionRecognizer();
		int next = subExpressionRecognizer.matches(parserState, tokens,
				startOffset);

		if (next == NO_MATCH) {
			return ++startOffset;
		}
		return next;

	}

	/**
	 * Checks if a new statement is about to start. In Groovy a new statement
	 * starts, whenever a new line begins. However this is not the case, if
	 * special tokens force the statement to continue (such as BACKSLASH).
	 */
	protected boolean startsNewStatement(IToken token, IToken lastToken) {

		if (lastToken == null) {
			return false;
		}

		if (token == null) {
			return true;
		}

		ETokenType tokenType = token.getType();
		ETokenType lastTokenType = lastToken.getType();

		if (tokenType == RBRACE) {
			return true;
		}

		// E.g.
		// a++
		// b
		// should be treated as two statements.
		// a &&
		// b
		// should be treated as one statement.

		if (!NON_CONTINUATION_OPERATORS.contains(lastTokenType)
				&& lastTokenType.getTokenClass() == ETokenClass.OPERATOR) {
			return false;
		}

		if (CONTINUE_TOKENS.contains(tokenType)
				|| CONTINUE_TOKENS.contains(lastTokenType)) {
			return false;
		}

		/*
		 * If there is no reason to continue the statement in the next line, end
		 * the statement once the new token is on a new line.
		 */
		return lastToken.getLineNumber() < token.getLineNumber();

	}
}
