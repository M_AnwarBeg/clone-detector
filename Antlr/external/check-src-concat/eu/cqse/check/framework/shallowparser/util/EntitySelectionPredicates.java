/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.util;

import java.util.EnumSet;
import java.util.List;
import java.util.function.Predicate;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.UnmodifiableList;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntityTraversalUtils;

/**
 * Defines valid primitive expressions that can be used in the
 * {@link EntitySelectionExpressionParser}. Each expression is defined by a
 * public static method of same name. Methods found here must be parameterless
 * or accept a single string parameter and must return an {@link Predicate} over
 * {@link ShallowEntity}. To avoid clashes with Java keywords, an optional
 * prefix "select" may be used.
 * 
 * Note that the comments for the public methods describe not exactly the
 * function, but rather the returned predicate.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 55575 $
 * @ConQAT.Rating GREEN Hash: F8ADD30114F3079B9E197B7EDBBEA5F2
 */
public class EntitySelectionPredicates {

	/** The visibility modifiers in C++. */
	private static final EnumSet<ETokenType> CPP_VISIBILITY_MODIFIERS = EnumSet
			.of(ETokenType.PUBLIC, ETokenType.PRIVATE, ETokenType.PROTECTED);

	/** Selects all modules/namespaces. */
	public static Predicate<ShallowEntity> module() {
		return typePredicate(EShallowEntityType.MODULE);
	}

	/** Selects all types/classes. */
	public static Predicate<ShallowEntity> type() {
		return typePredicate(EShallowEntityType.TYPE)
				.and(entity -> !entity.getSubtype().equals("typedef"));
	}

	/** Selects all methods/functions. */
	public static Predicate<ShallowEntity> method() {
		return entity -> entity.getType() == EShallowEntityType.METHOD
				&& (entity.getParent() == null || entity.getParent()
						.getType() != EShallowEntityType.ATTRIBUTE);
	}

	/** Selects all shallow entities whose subtype is a declaration. */
	public static Predicate<ShallowEntity> declaration() {
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity entity) {
				if (entity.getSubtype().contains(SubTypeNames.DECLARATION)) {
					return true;
				}

				// also consider inline methods in classes
				if (ELanguage.CPP.equals(getLanguage(entity))) {
					ShallowEntity parent = entity.getParent();
					return parent != null
							&& parent.getType() == EShallowEntityType.TYPE;
				}
				return false;
			}
		};
	}

	/** Selects all shallow entities that are internal (C# keyword). */
	public static Predicate<ShallowEntity> internal() {
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity entity) {
				if (TokenStreamUtils.containsAny(entity.ownStartTokens(),
						ETokenType.INTERNAL)) {
					return true;
				}
				ShallowEntity parent = entity.getParent();
				return parent != null && TokenStreamUtils.containsAny(
						parent.ownStartTokens(), ETokenType.INTERNAL);
			}
		};
	}

	/** Selects all attributes. */
	public static Predicate<ShallowEntity> attribute() {
		return typePredicate(EShallowEntityType.ATTRIBUTE);
	}

	/**
	 * Selects all properties, defined attributes that have only method
	 * children.
	 */
	public static Predicate<ShallowEntity> property() {
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity entity) {
				if (entity.getType() != EShallowEntityType.ATTRIBUTE) {
					return false;
				}

				for (ShallowEntity child : entity.getChildren()) {
					if (child.getType() != EShallowEntityType.METHOD) {
						return false;
					}
				}

				return !entity.getChildren().isEmpty();
			}
		};
	}

	/** Selects all statements. */
	public static Predicate<ShallowEntity> statement() {
		return typePredicate(EShallowEntityType.STATEMENT);
	}

	/** Selects all meta information (defines, annotations, etc.). */
	public static Predicate<ShallowEntity> meta() {
		return typePredicate(EShallowEntityType.META);
	}

	/** Creates a predicate for checking the type of an entity. */
	private static Predicate<ShallowEntity> typePredicate(
			final EShallowEntityType type) {
		return entity -> entity.getType() == type;
	}

	/** Matches all public entitys. */
	public static Predicate<ShallowEntity> selectPublic() {
		return modifierPredicate(ETokenType.PUBLIC);
	}

	/** Matches all protected entitys. */
	public static Predicate<ShallowEntity> selectProtected() {
		return modifierPredicate(ETokenType.PROTECTED);
	}

	/** Matches all entitys marked with override. */
	public static Predicate<ShallowEntity> selectOverride() {
		return modifierPredicate(ETokenType.OVERRIDE);
	}

	/** Matches all private entitys. */
	public static Predicate<ShallowEntity> selectPrivate() {
		return modifierPredicate(ETokenType.PRIVATE);
	}

	/** Matches all entitys with default visibility. */
	public static Predicate<ShallowEntity> selectDefault() {
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity element) {
				List<IToken> tokens = element.ownStartTokens();
				return !TokenStreamUtils.containsAny(tokens, 0, tokens.size(),
						ETokenType.PUBLIC, ETokenType.PROTECTED,
						ETokenType.PRIVATE);
			}
		};
	}

	/** Matches all final entitys. */
	public static Predicate<ShallowEntity> selectFinal() {
		return modifierPredicate(ETokenType.FINAL);
	}

	/** Matches all static entitys. */
	public static Predicate<ShallowEntity> selectStatic() {
		return modifierPredicate(ETokenType.STATIC);
	}

	/** Returns a predicate that checks for existence of the given modifier. */
	private static Predicate<ShallowEntity> modifierPredicate(
			final ETokenType modifier) {
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity element) {
				if (TokenStreamUtils.find(element.ownStartTokens(),
						modifier) != TokenStreamUtils.NOT_FOUND) {
					return true;
				}

				if (ELanguage.CPP == getLanguage(element)
						&& CPP_VISIBILITY_MODIFIERS.contains(modifier)) {
					return hasCppVisibilityModifer(element, modifier);
				}
				return false;
			}
		};
	}

	/**
	 * Determines the language of the given entity or <code>null</code> if it
	 * cannot be determined
	 */
	private static ELanguage getLanguage(ShallowEntity entity) {
		UnmodifiableList<IToken> tokens = entity.includedTokens();
		if (tokens.isEmpty()) {
			return null;
		}
		return CollectionUtils.getAny(tokens).getLanguage();
	}

	/**
	 * Checks whether the given shallow entity is affected by the specified C++
	 * visibility modifier by backwards searching the sibling shallow entities.
	 * The given modifier must be one of public, protected, or private.
	 */
	private static boolean hasCppVisibilityModifer(ShallowEntity entity,
			ETokenType modifier) {
		ShallowEntity parent = entity.getParent();
		if (parent == null) {
			return false;
		}

		UnmodifiableList<ShallowEntity> siblings = parent.getChildren();
		for (int i = siblings.indexOf(entity) - 1; i >= 0; i--) {
			ShallowEntity sibling = siblings.get(i);

			if (isCppVisibilityModifier(sibling)) {
				return TokenStreamUtils.containsAny(sibling.ownStartTokens(),
						modifier);
			}
		}

		// no explicit modifier found -> assume default
		if (parent.getType() == EShallowEntityType.TYPE) {
			if (SubTypeNames.STRUCT.equals(parent.getSubtype())) {
				return ETokenType.PUBLIC.equals(modifier);
			}
			return ETokenType.PRIVATE.equals(modifier);
		}

		return false;
	}

	/** Returns whether the given entity is a C++ visibility modifier. */
	private static boolean isCppVisibilityModifier(ShallowEntity entity) {
		return entity.getType() == EShallowEntityType.META && TokenStreamUtils
				.containsAny(entity.ownStartTokens(), CPP_VISIBILITY_MODIFIERS);
	}

	/** Matches all primitive entities (i.e. those without children). */
	public static Predicate<ShallowEntity> primitive() {
		return entity -> entity.getChildren().isEmpty();
	}

	/** Matches entities by their subtype. */
	public static Predicate<ShallowEntity> subtype(final String subtype) {
		return entity -> subtype.equals(entity.getSubtype());
	}

	/** Matches entities by their name. */
	public static Predicate<ShallowEntity> name(final String name) {
		return entity -> name.equals(entity.getName());
	}

	/**
	 * Matches all entities that are annotated with an annotation of given name
	 * (excluding the '@' sign).
	 */
	public static Predicate<ShallowEntity> annotated(String annotationName) {
		final String normalizedAnnotationName = StringUtils
				.stripPrefix(annotationName, "@");
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity entity) {
				// annotations must be part of same parent
				ShallowEntity parent = entity.getParent();
				if (parent == null) {
					return false;
				}

				List<ShallowEntity> children = parent.getChildren();
				int index = children.indexOf(entity);
				index -= 1;
				while (index >= 0 && children.get(index)
						.getType() == EShallowEntityType.META) {
					ShallowEntity metaEntity = children.get(index);
					if ("annotation".equals(metaEntity.getSubtype())
							&& normalizedAnnotationName
									.equalsIgnoreCase(metaEntity.getName())) {
						return true;
					}
					index -= 1;
				}
				return false;
			}
		};
	}

	/**
	 * Matches simple getters, i.e. methods starting with "get" or "is" and with
	 * at most one contained statement.
	 */
	public static Predicate<ShallowEntity> simpleGetter() {
		return simpleMethod("get").or(simpleMethod("is"));
	}

	/**
	 * Matches simple setters, i.e. methods starting with "set" and with at most
	 * one contained statement.
	 */
	public static Predicate<ShallowEntity> simpleSetter() {
		return simpleMethod("set");
	}

	/**
	 * Matches simple methods with the given prefix. A method is simple, if it
	 * contains as most one statement.
	 */
	public static Predicate<ShallowEntity> simpleMethod(
			final String namePrefix) {
		return new Predicate<ShallowEntity>() {
			@Override
			public boolean test(ShallowEntity entity) {
				boolean isSimpleMethod = entity
						.getType() == EShallowEntityType.METHOD
						&& getStatementCount(entity) <= 1;
				if (!isSimpleMethod) {
					return false;
				}

				if (entity.getName() == null) {
					return StringUtils.isEmpty(namePrefix);
				}
				return entity.getName().startsWith(namePrefix);
			}

			/**
			 * Returns the number of statements contained as children of the
			 * entity.
			 */
			private int getStatementCount(ShallowEntity entity) {
				return ShallowEntityTraversalUtils
						.listEntitiesOfType(entity.getChildren(),
								EShallowEntityType.STATEMENT)
						.size();
			}
		};
	}
}
