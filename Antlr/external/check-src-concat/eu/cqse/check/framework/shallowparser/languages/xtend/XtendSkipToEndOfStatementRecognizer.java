/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.xtend;

import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;

import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.xtend.XtendShallowParser.EXtendShallowParserState;

/**
 * We have to use this recognizer to match single statements in xtend, because
 * there is no indicator like semicolon or end of line, that marks the end of a
 * statement. Additionally xtend allows multiple statements in one single line.
 * <br>
 * 
 * The recognizer recognizes specific constructs within a simple statement,
 * namely: try, switch and if statements.<br>
 * 
 * @author $Author: baumeister $
 * @version $Rev: $
 * @ConQAT.Rating GREEN Hash: 9ADD58A74AA74955897F127CA425FADC
 */
public class XtendSkipToEndOfStatementRecognizer
		extends RecognizerBase<EXtendShallowParserState> {

	/** Contains the token classes LITERAL and IDENTIFIER. */
	private final static EnumSet<ETokenClass> LITERAL_OR_IDENTIFIER = EnumSet
			.of(ETokenClass.LITERAL, ETokenClass.IDENTIFIER);

	/** Contains the token types VAL and VAR. */
	private final static EnumSet<ETokenType> VAL_OR_VAR = EnumSet
			.of(ETokenType.VAL, ETokenType.VAR);

	/** Contains closing parenthesis token types. */
	private static final EnumSet<ETokenType> CLOSING_PARENTHESIS = EnumSet
			.of(ETokenType.RPAREN, ETokenType.RBRACK, ETokenType.RBRACE);

	/** Contains token types THROW and NEW. */
	private static final EnumSet<ETokenType> THROW_OR_NEW = EnumSet
			.of(ETokenType.THROW, ETokenType.NEW);

	/** Contains the token classes IDENTIFIER and KEYWORD. */
	private final static EnumSet<ETokenClass> IDENTIFIER_OR_KEYWORD = EnumSet
			.of(ETokenClass.IDENTIFIER, ETokenClass.KEYWORD);

	/**
	 * Contains token types that are allowed to be used in front of a keyword.
	 * Those are EQ, AS and DOT.
	 */
	private static final EnumSet<ETokenType> ALLOWED_IN_FRONT_OF_KEYWORD = EnumSet
			.of(ETokenType.EQ, ETokenType.AS_OPERATOR, ETokenType.DOT);

	/** Matched tokens for nesting in complex Xtend statements. */
	private final static Map<ETokenType, ETokenType> XTEND_NESTING_MATCH = new EnumMap<ETokenType, ETokenType>(
			ETokenType.class);

	static {
		XTEND_NESTING_MATCH.put(LPAREN, RPAREN);
		XTEND_NESTING_MATCH.put(LBRACE, RBRACE);
	}

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EXtendShallowParserState> parserState,
			List<IToken> tokens, int offset) {
		if (offset <= 0) {
			return NO_MATCH;
		}

		IToken lastToken = tokens.get(offset - 1);
		Stack<ETokenType> expectedClosing = new Stack<ETokenType>();

		while (true) {
			if (offset >= tokens.size()) {
				return offset;
			}

			IToken token = tokens.get(offset);
			ETokenType tokenType = token.getType();

			if (!expectedClosing.isEmpty()
					&& tokenType == expectedClosing.peek()) {
				expectedClosing.pop();
			} else if (expectedClosing.isEmpty() && tokenType == SEMICOLON) {
				return offset + 1;
			} else if (expectedClosing.isEmpty()
					&& startsNewStatement(token, lastToken)) {
				return offset;
			} else if (XTEND_NESTING_MATCH.containsKey(tokenType)) {
				expectedClosing.push(XTEND_NESTING_MATCH.get(tokenType));
			} else if (tokenType == ETokenType.LBRACK
					&& lastToken.getType() == ETokenType.HASH_OPERATOR) {
				expectedClosing.push(ETokenType.RBRACK);
			} else {
				int next = startSubParse(parserState, tokens, offset,
						tokenType);
				if (next == NO_MATCH) {
					return NO_MATCH;
				}
				if (next != offset) {
					offset = next;
					lastToken = tokens.get(offset - 1);
					continue;
				}
			}

			lastToken = token;
			offset += 1;
		}
	}

	/**
	 * Checks if a new statement is about to start. This means, that
	 * <code>lastToken</code> is part of the statement and <code>token</code> is
	 * not. <br>
	 * Defaults to false.
	 */
	private static boolean startsNewStatement(IToken token, IToken lastToken) {
		ETokenType tokenType = token.getType();
		ETokenType lastTokenType = lastToken.getType();

		ETokenClass tokenClass = tokenType.getTokenClass();
		ETokenClass lastTokenClass = lastTokenType.getTokenClass();

		if (VAL_OR_VAR.contains(tokenType)) {
			return true;
		}

		if (VAL_OR_VAR.contains(lastTokenType)) {
			return false;
		}

		if (LITERAL_OR_IDENTIFIER.contains(tokenClass)
				&& LITERAL_OR_IDENTIFIER.contains(lastTokenClass)) {
			return true;
		}

		if (CLOSING_PARENTHESIS.contains(lastTokenType)
				&& LITERAL_OR_IDENTIFIER.contains(tokenClass)) {
			return true;
		}

		if (CLOSING_PARENTHESIS.contains(tokenType)) {
			return true;
		}

		if (LITERAL_OR_IDENTIFIER.contains(lastTokenClass)
				&& tokenType == ETokenType.LBRACE) {
			return true;
		}

		// Treat return explicitly, to allow cases like 'return null'
		if (lastTokenType == ETokenType.RETURN
				&& (LITERAL_OR_IDENTIFIER.contains(tokenClass)
						|| tokenClass == ETokenClass.KEYWORD)) {
			return false;
		}

		if (THROW_OR_NEW.contains(lastTokenType)
				&& (IDENTIFIER_OR_KEYWORD.contains(tokenClass))) {
			return false;
		}

		if (lastTokenClass == ETokenClass.KEYWORD
				&& LITERAL_OR_IDENTIFIER.contains(tokenClass)) {
			return true;
		}

		// RETURN, THROW, and NEW have already been handled.
		return !ALLOWED_IN_FRONT_OF_KEYWORD.contains(lastTokenType)
				&& lastTokenClass != ETokenClass.OPERATOR
				&& tokenClass == ETokenClass.KEYWORD;
	}

	/**
	 * Starts a subparser, if tokenType equals TRY, SWITCH or IF. Also starts to
	 * subparse in case of a lambda expression.
	 * 
	 * @return new startOffset
	 */
	private static int startSubParse(ParserState<EXtendShallowParserState> parserState,
			List<IToken> tokens, int startOffset, ETokenType tokenType) {
		if (tokenType == TRY || tokenType == SWITCH || tokenType == IF) {
			return parserState.parse(EXtendShallowParserState.IN_METHOD, tokens,
					startOffset);
		} else if (tokenType == ETokenType.LBRACK
				&& tokens.get(startOffset - 1)
						.getType() != ETokenType.HASH_OPERATOR
				&& tokens.get(startOffset + 1).getType() != ETokenType.RBRACK) {
			return parserState.parse(EXtendShallowParserState.IN_LAMBDA, tokens,
					startOffset);
		}
		return startOffset;
	}
}
