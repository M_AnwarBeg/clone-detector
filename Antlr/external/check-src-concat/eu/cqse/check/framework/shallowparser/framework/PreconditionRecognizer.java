/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.framework;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;

/**
 * A recognizer that allows to match a prepared recognizer once but does not
 * advance the parsing state.
 * 
 * @author $Author: goeb $
 * @version $Rev: 52714 $
 * @ConQAT.Rating GREEN Hash: 1EC8014988112861BE2FF76DE6FF5611
 */
public class PreconditionRecognizer<STATE extends Enum<STATE>> extends
		RecognizerBase<STATE> {

	/** The sub-recognizer that is applied. */
	private final RecognizerBase<STATE> subRecognizer;

	/** Constructor. */
	public PreconditionRecognizer(RecognizerBase<STATE> subRecognizer) {
		this.subRecognizer = subRecognizer;
	}

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<STATE> parserState,
			List<IToken> tokens, int startOffset) {
		int newOffset = subRecognizer.matches(parserState, tokens,
				startOffset);
		if (newOffset == NO_MATCH) {
			return NO_MATCH;
		}
		return startOffset;
	}
}
