/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.matlab;

import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_CLASSDEF;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_ENUMERATION;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_EVENTS;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_METHODS;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_PROPERTIES;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.IN_SWITCH;
import static eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates.TOP_LEVEL;
import static eu.cqse.check.framework.scanner.ETokenType.AND;
import static eu.cqse.check.framework.scanner.ETokenType.BREAK;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CATCH;
import static eu.cqse.check.framework.scanner.ETokenType.CLASSDEF;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSEIF;
import static eu.cqse.check.framework.scanner.ETokenType.END;
import static eu.cqse.check.framework.scanner.ETokenType.ENUMERATION;
import static eu.cqse.check.framework.scanner.ETokenType.EOL;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.EVENTS;
import static eu.cqse.check.framework.scanner.ETokenType.EXCLAMATION;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.GLOBAL;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.LT;
import static eu.cqse.check.framework.scanner.ETokenType.METHODS;
import static eu.cqse.check.framework.scanner.ETokenType.OTHERWISE;
import static eu.cqse.check.framework.scanner.ETokenType.PARFOR;
import static eu.cqse.check.framework.scanner.ETokenType.PERSISTENT;
import static eu.cqse.check.framework.scanner.ETokenType.PROPERTIES;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;

import java.util.EnumSet;

import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.matlab.MatlabShallowParser.EMatlabParserStates;
import eu.cqse.check.framework.scanner.ETokenType;

/**
 * A shallow parser for Matlab.
 * 
 * This parser currently recognizes class definitions (with events, enumeration,
 * methods and properties). Within functions, control structures and simple
 * statements can be parsed. Nested methods and shell escapes are supported.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: C77E011D0D5505B458B8798B1A989D30
 */
public class MatlabShallowParser extends ShallowParserBase<EMatlabParserStates> {

	/** All possible states of a MatlabShallowParser. */
	public static enum EMatlabParserStates {
		/** Top-level state. */
		TOP_LEVEL,

		/** Inside a class definition. */
		IN_CLASSDEF,

		/** Inside a properties section. */
		IN_PROPERTIES,

		/** Inside a events section. */
		IN_EVENTS,

		/** Inside a enumeration section. */
		IN_ENUMERATION,

		/** Inside a methods section. */
		IN_METHODS,

		/** Inside a method's body. */
		IN_METHOD,

		/** Inside a switch block. */
		IN_SWITCH
	}

	/** All tokens that are valid statement separators. */
	private static final EnumSet<ETokenType> STATEMENT_SEPARATORS = EnumSet.of(
			EOL, SEMICOLON, COMMA);

	/** All states in which statements can be parsed. */
	private static final EMatlabParserStates[] STATEMENT_STATES = { TOP_LEVEL,
			IN_METHOD };

	/** Constructor. */
	public MatlabShallowParser() {
		super(EMatlabParserStates.class, TOP_LEVEL);
		createTopLevelRules();
		createInClassDefRules();
		createInPropertiesRules();
		createInEventsRules();
		createInEnumerationRules();
		createInMethodsRules();
		createStatementRules();
	}

	/** Creates rules for the top-level state. */
	private void createTopLevelRules() {
		// class definition
		inState(TOP_LEVEL).sequence(CLASSDEF)
				.skipToWithNesting(IDENTIFIER, LPAREN, RPAREN)
				.createNode(EShallowEntityType.TYPE, "class", -1)
				.optional(LT, IDENTIFIER).repeated(AND, IDENTIFIER)
				.sequence(EOL).parseUntil(IN_CLASSDEF).sequence(END).endNode();
	}

	/** Creates rules for parsing class definitions. */
	private void createInClassDefRules() {
		// properties section
		createInClassDefSectionRule(PROPERTIES, IN_PROPERTIES);

		// methods section
		createInClassDefSectionRule(METHODS, IN_METHODS);

		// events section
		createInClassDefSectionRule(EVENTS, IN_EVENTS);

		// enumeration section
		inState(IN_CLASSDEF).sequence(ENUMERATION, EOL)
				.createNode(EShallowEntityType.META, 0)
				.parseUntil(IN_ENUMERATION).sequence(END).endNode();
	}

	/**
	 * Creates a rule for parsing a section within a class definition. The
	 * section must start with the given start token. The section's content is
	 * parsed in the given sub-state.
	 */
	private void createInClassDefSectionRule(ETokenType startTokenType,
			EMatlabParserStates subState) {
		inState(IN_CLASSDEF).sequence(startTokenType)
				.skipToWithNesting(EOL, LPAREN, RPAREN)
				.createNode(EShallowEntityType.META, 0).parseUntil(subState)
				.sequence(END).endNode();
	}

	/** Creates rules for parsing class properties. */
	private void createInPropertiesRules() {
		inState(IN_PROPERTIES).sequence(IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE, "attribute", 0)
				.skipTo(STATEMENT_SEPARATORS).endNode();
	}

	/** Creates rules for parsing class events. */
	private void createInEventsRules() {
		inState(IN_EVENTS).sequence(IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE, 0).sequence(EOL)
				.endNode();
	}

	/** Creates rules for parsing enumerations. */
	private void createInEnumerationRules() {
		inState(IN_ENUMERATION).sequence(IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE, "enum-literal", 0)
				.skipToWithNesting(EnumSet.of(COMMA, EOL), LPAREN, RPAREN)
				.endNode();
	}

	/** Creates rules for parsing function definitions/declarations. */
	private void createInMethodsRules() {
		// recognizer for the beginning of function definitions
		// function definitions can also appear in top-level state or within
		// another function's body
		RecognizerBase<EMatlabParserStates> functionAlternative = inState(
				TOP_LEVEL, IN_METHOD, IN_METHODS).sequence(FUNCTION);

		// function with one return value
		continueMethodHeadRule(functionAlternative.sequence(IDENTIFIER, EQ),
				true);

		// function with no return value
		continueMethodHeadRule(functionAlternative, true);

		// function with multiple return values
		continueMethodHeadRule(
				functionAlternative.skipToWithNesting(EQ, LBRACK, RBRACK), true);

		// function declaration
		continueMethodHeadRule(inState(IN_METHODS), false);
	}

	/** Appends rules for parsing a method's head to the given recognizer. */
	private void continueMethodHeadRule(
			RecognizerBase<EMatlabParserStates> functionRecognizer,
			boolean isDefinition) {
		String subtype = "function declaration";
		if (isDefinition) {
			subtype = "function";
		}

		functionRecognizer = functionRecognizer.sequence(IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, subtype, -1)
				.skipToWithNesting(EOL, LPAREN, RPAREN);
		// if it is a function definition, we have to parse the function's body,
		// as well
		if (isDefinition) {
			functionRecognizer = functionRecognizer.parseUntilOrEof(IN_METHOD)
					.sequence(END);
		}
		functionRecognizer.endNode();
	}

	/** Create rules for parsing all kinds of statements within function bodies. */
	private void createStatementRules() {
		// create rule for if/elseif/else
		createBlockRulesWithContinuation(EnumSet.of(IF, ELSEIF, ELSE),
				EnumSet.of(ELSEIF, ELSE));

		// create rule try/catch
		createBlockRulesWithContinuation(EnumSet.of(TRY, CATCH),
				EnumSet.of(CATCH));

		createLoopRules();
		createSwitchCaseRules();
		createShellEscapeRules();
		createSimpleStatementRule();
	}

	/**
	 * Creates a rules for parsing statements that begin with one of the given
	 * start tokens, followed by a block. The statement can be continued if one
	 * of the given continuation tokens is encountered after the block.
	 */
	private void createBlockRulesWithContinuation(
			EnumSet<ETokenType> startTokens,
			EnumSet<ETokenType> continuationTokens) {
		RecognizerBase<EMatlabParserStates> alternative = inState(
				STATEMENT_STATES).sequence(startTokens)
				.createNode(EShallowEntityType.STATEMENT, 0)
				.skipTo(STATEMENT_SEPARATORS)
				.parseUntil(IN_METHOD);

		alternative.sequence(END).endNode();

		alternative.sequenceBefore(continuationTokens)
				.endNodeWithContinuation();
	}

	/** Creates rules for parsing for/parfor and while loops. */
	private void createLoopRules() {
		// create rule for for/parfor/while loop
		inState(STATEMENT_STATES).sequence(EnumSet.of(FOR, PARFOR, WHILE))
				.createNode(EShallowEntityType.STATEMENT, 0)
				.skipTo(STATEMENT_SEPARATORS)
				.parseUntil(IN_METHOD).sequence(END).endNode();
	}

	/** Creates rules for parsing switch/case structures. */
	private void createSwitchCaseRules() {
		inState(STATEMENT_STATES).sequence(SWITCH)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(EOL)
				.parseUntil(IN_SWITCH).sequence(END).endNode();

		inState(IN_SWITCH).sequence(EnumSet.of(CASE, OTHERWISE))
				.createNode(EShallowEntityType.META, 0).skipTo(EOL)
				.parseUntil(IN_METHOD)
				.sequenceBefore(EnumSet.of(CASE, OTHERWISE, END)).endNode();
	}

	/** Returns a set of token types, that can start a simple statement. */
	private EnumSet<ETokenType> getSimpleStatementTokenTypes() {
		return EnumSet.of(IDENTIFIER, BREAK, CONTINUE, RETURN, LBRACK,
				PERSISTENT, GLOBAL);
	}

	/** Creates rules for parsing shell escapes. */
	private void createShellEscapeRules() {
		inState(STATEMENT_STATES).sequence(EXCLAMATION)
				.createNode(EShallowEntityType.STATEMENT, "shell-escape")
				.skipTo(EOL).endNode();
	}

	/** Creates rules for parsing simple statements. */
	private void createSimpleStatementRule() {
		inState(STATEMENT_STATES)
				.sequence(getSimpleStatementTokenTypes())
				.createNode(EShallowEntityType.STATEMENT, "simple-statement", 0)
				.skipTo(STATEMENT_SEPARATORS).endNode();
	}
}
