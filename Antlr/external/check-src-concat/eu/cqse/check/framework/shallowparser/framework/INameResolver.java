/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.framework;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;

/**
 * A name resolver can be passed to
 * {@link RecognizerBase#createNode(EShallowEntityType, Object, Object)} and
 * resolves the name of the new created entity.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56492 $
 * @ConQAT.Rating YELLOW Hash: F27D37BB8166309CE428DF7B0E55B3A3
 */
public interface INameResolver<STATE extends Enum<STATE>> {

	/** Returns a name that will be set in a newly create entity. */
	public String resolveName(ParserState<STATE> state, List<IToken> tokens,
			int startOffset);
}
