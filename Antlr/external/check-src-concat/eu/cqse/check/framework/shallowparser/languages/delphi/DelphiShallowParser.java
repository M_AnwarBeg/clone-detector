/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.delphi;

import static eu.cqse.check.framework.scanner.ETokenType.ABSTRACT;
import static eu.cqse.check.framework.scanner.ETokenType.ARRAY;
import static eu.cqse.check.framework.scanner.ETokenType.AT;
import static eu.cqse.check.framework.scanner.ETokenType.BEGIN;
import static eu.cqse.check.framework.scanner.ETokenType.BOOLEAN_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.BREAK;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CDECL;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONST;
import static eu.cqse.check.framework.scanner.ETokenType.CONSTRUCTOR;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.DESTRUCTOR;
import static eu.cqse.check.framework.scanner.ETokenType.DISPINTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.DO;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE_DOT;
import static eu.cqse.check.framework.scanner.ETokenType.DYNAMIC;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.END;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.EXCEPT;
import static eu.cqse.check.framework.scanner.ETokenType.EXIT;
import static eu.cqse.check.framework.scanner.ETokenType.EXTERNAL;
import static eu.cqse.check.framework.scanner.ETokenType.FILE;
import static eu.cqse.check.framework.scanner.ETokenType.FINALIZATION;
import static eu.cqse.check.framework.scanner.ETokenType.FINALLY;
import static eu.cqse.check.framework.scanner.ETokenType.FLOATING_POINT_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FORWARD;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPLEMENTATION;
import static eu.cqse.check.framework.scanner.ETokenType.INDEX;
import static eu.cqse.check.framework.scanner.ETokenType.INHERITED;
import static eu.cqse.check.framework.scanner.ETokenType.INITIALIZATION;
import static eu.cqse.check.framework.scanner.ETokenType.INTEGER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.OF;
import static eu.cqse.check.framework.scanner.ETokenType.ON;
import static eu.cqse.check.framework.scanner.ETokenType.OPERATOR;
import static eu.cqse.check.framework.scanner.ETokenType.OVERLOAD;
import static eu.cqse.check.framework.scanner.ETokenType.OVERRIDE;
import static eu.cqse.check.framework.scanner.ETokenType.PACKED;
import static eu.cqse.check.framework.scanner.ETokenType.PASCAL;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROCEDURE;
import static eu.cqse.check.framework.scanner.ETokenType.PROGRAM;
import static eu.cqse.check.framework.scanner.ETokenType.PROPERTY;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLISHED;
import static eu.cqse.check.framework.scanner.ETokenType.RAISE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RECORD;
import static eu.cqse.check.framework.scanner.ETokenType.REFERENCE;
import static eu.cqse.check.framework.scanner.ETokenType.REGISTER;
import static eu.cqse.check.framework.scanner.ETokenType.REPEAT;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SAFECALL;
import static eu.cqse.check.framework.scanner.ETokenType.SELF;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SET;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.STDCALL;
import static eu.cqse.check.framework.scanner.ETokenType.STRICT;
import static eu.cqse.check.framework.scanner.ETokenType.STRING_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.THEN;
import static eu.cqse.check.framework.scanner.ETokenType.TO;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.TYPE;
import static eu.cqse.check.framework.scanner.ETokenType.UNIT;
import static eu.cqse.check.framework.scanner.ETokenType.UNTIL;
import static eu.cqse.check.framework.scanner.ETokenType.USES;
import static eu.cqse.check.framework.scanner.ETokenType.VAR;
import static eu.cqse.check.framework.scanner.ETokenType.VARARGS;
import static eu.cqse.check.framework.scanner.ETokenType.VIRTUAL;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WITH;
import static eu.cqse.check.framework.scanner.ETokenType.WRITE;
import static eu.cqse.check.framework.scanner.ETokenType.XOR;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_CASE;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_CONST;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_ENUM;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_IMPLEMENTATION;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_INTERFACE;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_TYPE_CASE;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_TYPE_CASE_LABEL;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_TYPE_DECL;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.IN_VAR;
import static eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates.TOP_LEVEL;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.delphi.DelphiShallowParser.EDelphiParserStates;

/**
 * A shallow parser for Delphi.
 * 
 * This parser currently recognizes types (classes, records, enumerations,
 * ranges, sets), procedures, functions and global directives like
 * initialization and finalization sections. Within methods, control structures
 * and simple statements can be parsed. The parser does not parse into the
 * statements. Nested methods are supported.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 53669 $
 * @ConQAT.Rating GREEN Hash: E437DA99738951AAED40E71762336564
 */
public class DelphiShallowParser
		extends ShallowParserBase<EDelphiParserStates> {

	/** All possible states of the DelphiShallowParser. */
	public static enum EDelphiParserStates {

		/** Top-level state. */
		TOP_LEVEL,

		/** Inside a interface state. */
		IN_INTERFACE,

		/**
		 * Inside a implementation section (and initialization and
		 * finalization).
		 */
		IN_IMPLEMENTATION,

		/** Inside a type-section. */
		IN_TYPE,

		/** Inside a type declaration, for example a class or a record. */
		IN_TYPE_DECL,

		/** Inside a case statement within a type declaration. */
		IN_TYPE_CASE,

		/** Inside the label of a case statement within a type declaration. */
		IN_TYPE_CASE_LABEL,

		/** Inside an enum declaration. */
		IN_ENUM,

		/** Inside a variable-section. */
		IN_VAR,

		/** Inside a const-section. */
		IN_CONST,

		/** Inside a case-of statement. */
		IN_CASE,

		/** Inside a method's head/variable declarations. */
		IN_METHOD_HEAD,

		/** Inside a method's implementation. */
		IN_METHOD
	}

	/** A set of all token types, that indicate a new top-level section. */
	private static final EnumSet<ETokenType> TOP_LEVEL_KEYWORDS = EnumSet
			.of(IMPLEMENTATION, INTERFACE, BEGIN, FINALIZATION, INITIALIZATION);

	/** States that are either implementation or interface. */
	private static final EDelphiParserStates[] SECTION_STATES = { IN_INTERFACE,
			IN_IMPLEMENTATION };

	/**
	 * A set of all token types that either indicate a new top-level section, or
	 * a new declaration within an implementation or interface section.
	 */
	private static final EnumSet<ETokenType> TOP_LEVEL_OR_SECTION_KEYWORDS = EnumSet
			.of(PROGRAM, UNIT, INTERFACE, IMPLEMENTATION, TYPE, VAR, CONST,
					PROCEDURE, FUNCTION, BEGIN, END, INITIALIZATION,
					FINALIZATION);

	/**
	 * A set of all token types that mark the beginning of a
	 * method/function/procedure declaration.
	 */
	private static final EnumSet<ETokenType> METHOD_KEYWORDS = EnumSet
			.of(FUNCTION, PROCEDURE, CONSTRUCTOR, DESTRUCTOR, OPERATOR);

	/**
	 * A set of all token types that mark the beginning of a simple statement.
	 */
	private static final EnumSet<ETokenType> SIMPLE_STATEMENT_TOKENS = EnumSet
			.of(IDENTIFIER, INHERITED, INDEX, RAISE, EXIT, CONTINUE, BREAK, AT,
					WRITE, LPAREN, SELF);

	/**
	 * A set of all token types that can be used as modifiers for
	 * methods/functions/procedures.
	 */
	private static final EnumSet<ETokenType> METHOD_MODIFIERS = EnumSet.of(
			ABSTRACT, CDECL, DYNAMIC, FORWARD, OVERLOAD, OVERRIDE, PASCAL,
			REGISTER, SAFECALL, STATIC, STDCALL, VARARGS, VIRTUAL);

	/** A set of all token types that can be used as access modifiers. */
	private static final EnumSet<ETokenType> ACCESS_MODIFIERS = EnumSet
			.of(PUBLIC, PROTECTED, PRIVATE, PUBLISHED);

	/** A set of all token types that can be used as case literals. */
	private static final EnumSet<ETokenType> CASE_LITERALS = EnumSet.of(
			IDENTIFIER, INTEGER_LITERAL, FLOATING_POINT_LITERAL,
			BOOLEAN_LITERAL, STRING_LITERAL);

	/**
	 * A set of all tokens that indicate a new member within or the end of a
	 * type declaration.
	 */
	private static final EnumSet<ETokenType> TYPE_MEMBER_TOKENS;

	/** A set of all token types that mark the end of a method's head. */
	private static final EnumSet<ETokenType> METHOD_SECTION_TOKENS;

	static {
		TYPE_MEMBER_TOKENS = EnumSet.of(STRICT, CASE, CLASS, PROPERTY,
				IDENTIFIER, END);
		TYPE_MEMBER_TOKENS.addAll(ACCESS_MODIFIERS);
		TYPE_MEMBER_TOKENS.addAll(METHOD_KEYWORDS);

		METHOD_SECTION_TOKENS = EnumSet.of(BEGIN, VAR, CONST, TYPE);
		METHOD_SECTION_TOKENS.addAll(METHOD_KEYWORDS);
	}

	/** Constructor. */
	public DelphiShallowParser() {
		super(EDelphiParserStates.class, TOP_LEVEL);
		createTopLevelRules();
		createTypeRules();
		createEnumRules();
		createTypeDeclRules();
		createMethodSectionRules();
		createVarRules();
		createConstRules();
		createMethodRules();
		createClassAttributeRules();
	}

	/** Create rules for all top-level structures. */
	private void createTopLevelRules() {
		// program or unit declaration
		inState(TOP_LEVEL).sequence(EnumSet.of(PROGRAM, UNIT)).skipTo(SEMICOLON)
				.createNode(EShallowEntityType.META, 0, 1).endNode();

		// uses declaration
		inAnyState().sequence(USES).skipTo(SEMICOLON)
				.createNode(EShallowEntityType.META, 0).endNode();

		// interface section
		inState(TOP_LEVEL).sequence(INTERFACE)
				.createNode(EShallowEntityType.META, 0)
				.parseUntilOrEof(IN_INTERFACE)
				.sequenceBefore(TOP_LEVEL_KEYWORDS).endNode();

		// implementation section
		inState(TOP_LEVEL).sequence(IMPLEMENTATION)
				.createNode(EShallowEntityType.META, 0)
				.parseUntilOrEof(IN_IMPLEMENTATION)
				.sequenceBefore(TOP_LEVEL_KEYWORDS).endNode();

		// list of type declarations
		inState(IN_INTERFACE).sequence(TYPE).parseUntilOrEof(IN_TYPE)
				.sequenceBefore(TOP_LEVEL_OR_SECTION_KEYWORDS);

		// variable declaration
		inState(SECTION_STATES).sequence(VAR).parseUntilOrEof(IN_VAR)
				.sequenceBefore(TOP_LEVEL_OR_SECTION_KEYWORDS);

		// const declaration
		inState(SECTION_STATES).sequence(CONST).parseUntilOrEof(IN_CONST)
				.sequenceBefore(TOP_LEVEL_OR_SECTION_KEYWORDS);

		// declaration of methods
		inState(IN_INTERFACE).sequence(METHOD_KEYWORDS, IDENTIFIER)
				.repeated(DOT, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, new Region(1, -1))
				.skipNested(LPAREN, RPAREN).skipTo(SEMICOLON).endNode();

		// method implementation (nested functions are
		// processed in IN_METHOD state)
		RecognizerBase<EDelphiParserStates> methodRecognizer = inState(
				IN_IMPLEMENTATION, IN_METHOD)
						.sequence(METHOD_KEYWORDS, IDENTIFIER)
						.repeated(DOT, IDENTIFIER)
						.createNode(EShallowEntityType.METHOD, 0,
								new Region(1, -1))
						.skipNested(LPAREN, RPAREN).skipTo(SEMICOLON)
						.parseUntilOrEof(IN_METHOD);
		methodRecognizer.sequence(END).optional(SEMICOLON).endNode();

		// global begin
		inState(TOP_LEVEL, IN_IMPLEMENTATION).sequence(BEGIN)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.GLOBAL_BEGIN)
				.parseUntil(IN_METHOD).sequence(END).optional(SEMICOLON)
				.endNode();

		// initialization/finalization
		inState(TOP_LEVEL).sequence(EnumSet.of(INITIALIZATION, FINALIZATION))
				.createNode(EShallowEntityType.METHOD, 0).parseUntil(IN_METHOD)
				.sequenceBefore(TOP_LEVEL_OR_SECTION_KEYWORDS).endNode();
	}

	/**
	 * Create rules for parsing types like classes, records, enumerations,
	 * function pointers, ranges, sets and meta-classes.
	 */
	private void createTypeRules() {
		RecognizerBase<EDelphiParserStates> typeAlternative = inState(IN_TYPE)
				.sequence(IDENTIFIER).skipTo(EQ);

		// existing type alias
		typeAlternative.optional(TYPE).sequence(IDENTIFIER, SEMICOLON)
				.createNode(EShallowEntityType.TYPE, SubTypeNames.ALIAS, 0)
				.endNode();

		// type pointer
		typeAlternative.sequence(XOR, IDENTIFIER, SEMICOLON)
				.createNode(EShallowEntityType.TYPE, SubTypeNames.TYPE_POINTER,
						0)
				.endNode();

		// array or file of existing types
		typeAlternative.sequence(EnumSet.of(ARRAY, FILE))
				.skipToWithNesting(SEMICOLON, LPAREN, RPAREN)
				.createNode(EShallowEntityType.TYPE, 2, 0).endNode();

		// function pointer declaration
		typeAlternative.sequence(EnumSet.of(PROCEDURE, FUNCTION))
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.FUNCTION_POINTER, 0)
				.skipNested(LPAREN, RPAREN).skipTo(SEMICOLON).endNode();

		// anonymous method declaration
		typeAlternative.sequence(REFERENCE, TO, EnumSet.of(PROCEDURE, FUNCTION))
				.createNode(EShallowEntityType.TYPE,
						SubTypeNames.ANONYMOUS_METHOD, 0)
				.skipNested(LPAREN, RPAREN).skipTo(SEMICOLON).endNode();

		// enum declaration
		typeAlternative.sequence(LPAREN)
				.createNode(EShallowEntityType.TYPE, SubTypeNames.ENUM, 0)
				.parseUntil(IN_ENUM).sequence(RPAREN, SEMICOLON).endNode();

		// range declaration
		EnumSet<ETokenType> rangeTypes = EnumSet.of(INTEGER_LITERAL,
				STRING_LITERAL);
		typeAlternative.sequence(rangeTypes, DOUBLE_DOT, rangeTypes, SEMICOLON)
				.createNode(EShallowEntityType.TYPE, SubTypeNames.RANGE, 0)
				.endNode();

		// set declaration
		typeAlternative.sequence(SET, OF)
				.createNode(EShallowEntityType.TYPE, SubTypeNames.SET, 0)
				.skipTo(SEMICOLON).endNode();

		// meta-class type
		typeAlternative.sequence(CLASS, OF, CLASS, TYPE, SEMICOLON)
				.createNode(EShallowEntityType.TYPE, SubTypeNames.META_CLASS, 0)
				.endNode();

		// class/record/interface/dispinterface declaration with possible
		// inheritance
		RecognizerBase<EDelphiParserStates> declAlternative = typeAlternative
				.optional(PACKED)
				.sequence(EnumSet.of(CLASS, INTERFACE, RECORD, DISPINTERFACE))
				.createNode(EShallowEntityType.TYPE, -1, 0)
				.skipNested(LPAREN, RPAREN);
		// helper declaration
		endTypeDeclaration(declAlternative.sequence(IDENTIFIER)
				.skipNested(LPAREN, RPAREN).sequence(FOR, IDENTIFIER));
		endTypeDeclaration(declAlternative);
	}

	/**
	 * End a type declaration recognizer. Either a semicolon or a type body is
	 * expected.
	 */
	private static void endTypeDeclaration(
			RecognizerBase<EDelphiParserStates> recognizer) {
		recognizer.sequence(SEMICOLON).endNode();
		recognizer.parseUntil(IN_TYPE_DECL).sequence(END).optional(SEMICOLON)
				.endNode();
	}

	/** Create rules for parsing enumerations. */
	private void createEnumRules() {
		// beginning enum-literal
		inState(IN_ENUM).sequence(IDENTIFIER).optional(EQ, INTEGER_LITERAL)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ENUM_LITERAL, 0)
				.endNode();

		// enum-literal separated with comma
		inState(IN_ENUM).sequence(COMMA, IDENTIFIER)
				.optional(EQ, INTEGER_LITERAL)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ENUM_LITERAL, 1)
				.endNode();
	}

	/** Create rules for parsing classes and records. */
	private void createTypeDeclRules() {
		// access modifiers
		inState(IN_TYPE_DECL).optional(STRICT).sequence(ACCESS_MODIFIERS)
				.createNode(EShallowEntityType.META, new Region(0, -1))
				.endNode();

		// function/procedure declaration
		RecognizerBase<EDelphiParserStates> methodBase = inState(IN_TYPE_DECL)
				.optional(CLASS).sequence(METHOD_KEYWORDS, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, -2, -1)
				.skipNested(LPAREN, RPAREN).skipTo(SEMICOLON);
		appendMethodModifierRules(methodBase);

		// property declaration
		inState(IN_TYPE_DECL).sequence(PROPERTY).skipTo(SEMICOLON)
				.createNode(EShallowEntityType.ATTRIBUTE, SubTypeNames.PROPERTY,
						1)
				.endNode();

		// attribute declaration
		inState(IN_TYPE_DECL).sequence(IDENTIFIER).skipTo(SEMICOLON)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ATTRIBUTE, 0)
				.endNode();

		// case statement
		inState(IN_TYPE_DECL).sequence(CASE).skipTo(OF)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE)
				.parseUntil(IN_TYPE_CASE).sequenceBefore(TYPE_MEMBER_TOKENS)
				.endNode();

		createTypeCaseRules();
	}

	/** Create rules for parsing case statements within type declarations. */
	private void createTypeCaseRules() {
		inState(IN_TYPE_CASE).sequence(CASE_LITERALS).skipTo(COLON)
				.sequence(LPAREN)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE_LABEL, 0)
				.parseUntil(IN_TYPE_CASE_LABEL).sequence(RPAREN, SEMICOLON)
				.endNode();

		inState(IN_TYPE_CASE_LABEL).optional(SEMICOLON).markStart()
				.sequence(IDENTIFIER, COLON)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ATTRIBUTE, 0)
				.skipBefore(EnumSet.of(SEMICOLON, RPAREN)).endNode();
	}

	/**
	 * Create rules for parsing sections within a method. Possible sections are
	 * variable, constant, type declarations or the method's body.
	 */
	private void createMethodSectionRules() {
		// variable declaration within method
		inState(IN_METHOD).sequence(VAR).parseUntil(IN_VAR)
				.sequenceBefore(METHOD_SECTION_TOKENS);

		// const declaration within method
		inState(IN_METHOD).sequence(CONST).parseUntil(IN_CONST)
				.sequenceBefore(METHOD_SECTION_TOKENS);

		// type declaration within method
		inState(IN_METHOD).sequence(TYPE)
				.parseUntil(EDelphiParserStates.IN_TYPE)
				.sequenceBefore(METHOD_SECTION_TOKENS);

		// begin of method body
		inState(IN_METHOD).sequence(BEGIN).parseUntil(IN_METHOD)
				.sequenceBefore(END).optional(SEMICOLON);
	}

	/** Create rules for parsing variable declarations within a var-section. */
	private void createVarRules() {
		// variable declaration
		inState(IN_VAR).sequence(IDENTIFIER, COLON).skipTo(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.LOCAL_VARIABLE, 0)
				.endNode();
	}

	/** Create rules for parsing const declarations within a const-section. */
	private void createConstRules() {
		// const declaration
		inState(IN_CONST).sequence(IDENTIFIER).optional(COLON).skipTo(EQ)
				.skipTo(SEMICOLON).createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.CONSTANT, 0)
				.endNode();
	}

	/** Create rules for parsing the body of a method/procedure/function. */
	private void createMethodRules() {
		// anonymous block
		inState(IN_METHOD).sequence(BEGIN)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.ANONYMOUS_BLOCK)
				.parseUntil(IN_METHOD).sequence(END).optional(SEMICOLON)
				.endNode();

		createIfElseRules();
		createTryExceptFinallyRules();
		createLoopRules();
		createCaseRules();
		createWithRules();
		createSimpleStatementRules();
	}

	/**
	 * Append rules for parsing method/function/procedure modifiers to the given
	 * recognizer.
	 */
	private static void appendMethodModifierRules(
			RecognizerBase<EDelphiParserStates> recognizer) {
		RecognizerBase<EDelphiParserStates> modifierAlternative = recognizer
				.repeated(METHOD_MODIFIERS, SEMICOLON);
		// parse external directive
		modifierAlternative.sequence(EXTERNAL).skipTo(SEMICOLON).endNode();
		// the last modifier was the end
		modifierAlternative.endNode();
	}

	/** Create rules for parsing if/else structures. */
	private void createIfElseRules() {
		RecognizerBase<EDelphiParserStates> ifAlternative = inState(IN_METHOD)
				.sequence(IF).createNode(EShallowEntityType.STATEMENT, 0)
				.skipTo(THEN);
		endWithPossibleContinuation(ifAlternative.sequence(BEGIN)
				.parseUntil(IN_METHOD).sequence(END).optional(SEMICOLON),
				EnumSet.of(ELSE));
		endWithPossibleContinuation(ifAlternative.parseOnce(IN_METHOD),
				EnumSet.of(ELSE));

		RecognizerBase<EDelphiParserStates> elseIfAlternative = inState(
				IN_METHOD).sequence(ELSE, IF)
						.createNode(EShallowEntityType.STATEMENT,
								new int[] { 0, 1 })
						.skipTo(THEN);
		endWithPossibleContinuation(elseIfAlternative.sequence(BEGIN)
				.parseUntil(IN_METHOD).sequence(END).optional(SEMICOLON),
				EnumSet.of(ELSE));
		endWithPossibleContinuation(elseIfAlternative.parseOnce(IN_METHOD),
				EnumSet.of(ELSE));

		RecognizerBase<EDelphiParserStates> elseAlternative = inState(IN_METHOD)
				.sequence(ELSE).createNode(EShallowEntityType.STATEMENT, 0);
		elseAlternative.sequence(BEGIN).parseUntil(IN_METHOD).sequence(END)
				.optional(SEMICOLON).endNode();
		elseAlternative.parseOnce(IN_METHOD).endNode();
	}

	/** Create rules for parsing try, except and finally structures. */
	private void createTryExceptFinallyRules() {
		RecognizerBase<EDelphiParserStates> tryAlternative = inState(IN_METHOD)
				.sequence(TRY).createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(IN_METHOD);
		endWithContinuationOrEnd(tryAlternative, EnumSet.of(EXCEPT, FINALLY));

		RecognizerBase<EDelphiParserStates> exceptAlternative = inState(
				IN_METHOD).sequence(EXCEPT)
						.createNode(EShallowEntityType.STATEMENT, 0)
						.parseUntil(IN_METHOD);
		endWithContinuationOrEnd(exceptAlternative,
				EnumSet.of(EXCEPT, FINALLY));

		inState(IN_METHOD).sequence(FINALLY)
				.createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(IN_METHOD).sequence(END).optional(SEMICOLON)
				.endNode();

		// on clause
		RecognizerBase<EDelphiParserStates> onAlternative = inState(IN_METHOD)
				.sequence(ON, IDENTIFIER, COLON, IDENTIFIER, DO)
				.createNode(EShallowEntityType.META, 0, 1);
		endWithPossibleContinuation(
				onAlternative.sequence(BEGIN).parseUntil(IN_METHOD)
						.sequence(END).optional(SEMICOLON),
				EnumSet.of(ON, ELSE));
		endWithPossibleContinuation(onAlternative.parseOnce(IN_METHOD),
				EnumSet.of(ON, ELSE));
	}

	/**
	 * End the given recognizer's current node with continuation, if one of the
	 * given continuation tokens is recognized and sequence before it. Otherwise
	 * expect an end and a semicolon token and end the current node.
	 */
	private static void endWithContinuationOrEnd(
			RecognizerBase<EDelphiParserStates> recognizer,
			EnumSet<ETokenType> continuationTokens) {
		recognizer.sequenceBefore(continuationTokens).endNodeWithContinuation();
		recognizer.sequence(END).optional(SEMICOLON).endNode();
	}

	/** Create rules for parsing loops. */
	private void createLoopRules() {
		// repeat until loop
		inState(IN_METHOD).sequence(REPEAT)
				.createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(IN_METHOD).sequence(UNTIL).skipTo(SEMICOLON)
				.endNode();

		// while/for loop
		RecognizerBase<EDelphiParserStates> whileForAlternative = inState(
				IN_METHOD).sequence(EnumSet.of(WHILE, FOR))
						.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DO);
		whileForAlternative.sequence(BEGIN).parseUntil(IN_METHOD).sequence(END)
				.optional(SEMICOLON).endNode();
		whileForAlternative.parseOnce(IN_METHOD).endNode();
	}

	/** Create rules for parsing case structures. */
	private void createCaseRules() {
		inState(IN_METHOD).sequence(CASE).skipTo(OF)
				.createNode(EShallowEntityType.STATEMENT, 0).parseUntil(IN_CASE)
				.sequence(END).optional(SEMICOLON).endNode();

		appendCaseLabelBody(inState(IN_CASE).sequence(CASE_LITERALS)
				.skipTo(COLON)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE, 0));

		RecognizerBase<EDelphiParserStates> elseRecognizer = inState(IN_CASE)
				.sequence(ELSE).createNode(EShallowEntityType.META, 0);
		elseRecognizer.parseUntil(IN_METHOD).sequenceBefore(END).endNode();
	}

	/**
	 * Appends rules for parsing the body of a case label to the given
	 * recognizer.
	 */
	private static void appendCaseLabelBody(
			RecognizerBase<EDelphiParserStates> recognizer) {
		endCaseLabelRecognizer(recognizer.sequence(BEGIN).parseUntil(IN_METHOD)
				.sequence(END).optional(SEMICOLON));
		endCaseLabelRecognizer(recognizer.parseOnce(IN_METHOD));

	}

	/**
	 * Ends a case-label recognizer, which must sequence before another
	 * case-label, else or end.
	 */
	private static void endCaseLabelRecognizer(
			RecognizerBase<EDelphiParserStates> recognizer) {
		recognizer.sequenceBefore(
				EnumSet.of(IDENTIFIER, INTEGER_LITERAL, FLOATING_POINT_LITERAL,
						STRING_LITERAL, BOOLEAN_LITERAL, ELSE, END))
				.endNode();
	}

	/** Create rules for parsing with statements. */
	private void createWithRules() {
		RecognizerBase<EDelphiParserStates> withRecognizer = inState(IN_METHOD)
				.sequence(WITH).skipTo(DO)
				.createNode(EShallowEntityType.STATEMENT, 0);
		withRecognizer.sequence(BEGIN).parseUntil(IN_METHOD).sequence(END)
				.optional(SEMICOLON).endNode();
		withRecognizer.parseOnce(IN_METHOD).endNode();
	}

	/** Create rules for parsing simple statements within methods. */
	private void createSimpleStatementRules() {
		// simple statement
		inState(IN_METHOD).sequence(SIMPLE_STATEMENT_TOKENS)
				.skipBefore(EnumSet.of(SEMICOLON, UNTIL, ELSE, END))
				.optional(SEMICOLON).createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0)
				.endNode();

		// empty statement
		inState(IN_METHOD).sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT)
				.endNode();

	}

	/** Create rules for parsing attributes at any place. */
	private void createClassAttributeRules() {
		inAnyState().sequence(LBRACK).skipToWithNesting(RBRACK, LBRACK, RBRACK)
				.createNode(EShallowEntityType.META, SubTypeNames.ATTRIBUTE, 1)
				.endNode();
	}
}