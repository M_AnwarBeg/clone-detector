/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.UnmodifiableList;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntityTraversalUtils;

/**
 * Utility methods used for dealing with {@link ShallowEntity}s.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57690 $
 * @ConQAT.Rating RED Hash: 3EEFC8E7EBA7723F559CF0BF6D81DF19
 */
public class ShallowParsingUtils {

	/**
	 * Entity subtypes that are part of a branching statement but not the first
	 * option of the branches. Those statements must not be separated from the
	 * first part of the branching statement when we consider refactoring
	 * suggestions. DO is part of this set because a last entity of a
	 * refactoring suggestion covering a do-while loop (where while is the
	 * branching statement) can be of type DO.
	 */
	private final static Set<String> SUBPART_BRANCHING_STATEMENTS = new HashSet<String>(
			Arrays.asList(new String[] { SubTypeNames.ELSE_IF,
					SubTypeNames.ELSE, SubTypeNames.CATCH, SubTypeNames.FINALLY,
					SubTypeNames.CASE, SubTypeNames.DEFAULT, SubTypeNames.USING,
					SubTypeNames.DO }));

	/**
	 * Entity subtypes that represent looping statements
	 */
	private final static Set<String> LOOP_SUBTYPES = new HashSet<String>(
			Arrays.asList(new String[] { SubTypeNames.FOR, SubTypeNames.FOREACH,
					SubTypeNames.WHILE }));

	/** Lists all primitive statements (i.e. statements without children). */
	public static List<ShallowEntity> listPrimitiveStatements(
			Collection<ShallowEntity> entities) {
		return new ShallowEntityTraversalUtils.CollectingVisitorBase() {
			@Override
			protected boolean collect(ShallowEntity entity) {
				return entity.getType() == EShallowEntityType.STATEMENT
						&& entity.getChildren().isEmpty();
			}
		}.apply(entities);
	}

	/** Lists all nested statements (i.e. statements with children). */
	public static List<ShallowEntity> listNestedStatements(
			Collection<ShallowEntity> entities) {
		return new ShallowEntityTraversalUtils.CollectingVisitorBase() {
			@Override
			protected boolean collect(ShallowEntity entity) {
				UnmodifiableList<ShallowEntity> children = entity.getChildren();
				return entity.getType() == EShallowEntityType.STATEMENT
						&& !children.isEmpty()
				// only include this, if nested is also code and not an
				// anonymous function, class, etc.
						&& children.get(0)
								.getType() == EShallowEntityType.STATEMENT;
			}
		}.apply(entities);
	}

	/**
	 * Returns the tokens of a method entity that correspond to parameter names.
	 */
	public static List<IToken> extractParameterNameTokens(
			ShallowEntity entity) {
		return extractVariableNameTokens(
				TokenStreamUtils.tokensBetween(entity.ownStartTokens(),
						ETokenType.LPAREN, ETokenType.RPAREN),
				true);
	}

	/** Returns the tokens corresponding to variable names. */
	public static List<IToken> extractVariableNameTokens(List<IToken> tokens) {
		return extractVariableNameTokens(tokens, false);
	}

	/**
	 * Returns the tokens corresponding to variable names. Heuristic is to look
	 * at identifier tokens outside of parenthesis/braces and directly before a
	 * comma, equals sign, or closing parenthesis. The equals sign is needed to
	 * deal with C++ default parameters and assignments in local variables.
	 * Additionally, we drop brackets, as they can obscure the type/name
	 * separation.
	 * 
	 * @param ignoreParameterTypes
	 *            if this is true, identifiers at the beginning, or after a
	 *            comma or a double-colon are ignored, as they are likely type
	 *            names in a parameter list (C++ allows to skip the parameter
	 *            name in some cases).
	 */
	private static List<IToken> extractVariableNameTokens(List<IToken> tokens,
			boolean ignoreParameterTypes) {
		// TODO (LH) This method is getting more and more complicated. Split or
		// even better transform into class with state?
		List<IToken> result = new ArrayList<IToken>();
		int parenthesisNesting = 0;
		boolean waitForComma = false;
		IToken previousToken = null;
		ETokenType beforePreviousType = null;
		for (IToken token : tokens) {
			switch (token.getType()) {
			case LBRACK:
			case LT:
			case LPAREN:
			case LBRACE:
				parenthesisNesting += 1;
				break;

			case RBRACK:
			case GT:
			case RPAREN:
			case RBRACE:
				parenthesisNesting -= 1;
				// do not update previousToken
				continue;

			case COMMA:
			case ARRAY_SEPARATOR:
				// TODO (LH) This sounds like we should actually fix the parser,
				// shouldn't we?
				// the commas in matlab methods are parsed as array separators
				if (parenthesisNesting == 0) {
					if (!waitForComma && isParameterName(previousToken,
							beforePreviousType, ignoreParameterTypes)) {
						result.add(previousToken);
					}
					waitForComma = false;
				}
				break;

			case EQ:
			case SEMICOLON:
				if (parenthesisNesting == 0 && !waitForComma
						&& isParameterName(previousToken, beforePreviousType,
								ignoreParameterTypes)) {
					result.add(previousToken);
					waitForComma = true;
				}
				break;

			case COLON:
				// In TypeScript you can add type information after colon
				if (token.getLanguage() == ELanguage.JAVASCRIPT
						&& parenthesisNesting == 0 && !waitForComma
						&& isParameterName(previousToken, beforePreviousType,
								false)) {
					result.add(previousToken);
					waitForComma = true;
				}
				break;

			default:
				break;
			}

			if (parenthesisNesting == 0) {
				if (previousToken != null) {
					beforePreviousType = previousToken.getType();
				}
				previousToken = token;
			}
		}

		if (parenthesisNesting == 0 && !waitForComma && isParameterName(
				previousToken, beforePreviousType, ignoreParameterTypes)) {
			result.add(previousToken);
		}

		return result;
	}

	/** Returns whether the previous token could be a parameter name. */
	private static boolean isParameterName(IToken previousToken,
			ETokenType beforePreviousType, boolean ignoreParameterTypes) {
		return previousToken != null
				&& previousToken.getType() == ETokenType.IDENTIFIER
				&& (!ignoreParameterTypes
						|| (beforePreviousType != ETokenType.COMMA
								&& beforePreviousType != null
								&& beforePreviousType != ETokenType.SCOPE));
	}

	/**
	 * Returns the list of tokens corresponding to the names of variables newly
	 * declared within a for loop.
	 */
	public static List<IToken> extractVariablesDeclaredInFor(
			ShallowEntity entity) {
		List<IToken> forLoopInitTokens = TokenStreamUtils.tokensBetween(
				entity.ownStartTokens(), ETokenType.LPAREN,
				ETokenType.SEMICOLON);
		List<IToken> variableNameTokens = ShallowParsingUtils
				.extractVariableNameTokens(forLoopInitTokens);

		// handle the case where only existing variables are initialized
		if (!variableNameTokens.isEmpty()
				&& variableNameTokens.get(0) == forLoopInitTokens.get(0)) {
			variableNameTokens.clear();
		}

		return variableNameTokens;
	}

	/** Returns whether the entity is a local variable. */
	public static boolean isLocalVariable(ShallowEntity entity) {
		return entity.getType() == EShallowEntityType.STATEMENT
				&& SubTypeNames.LOCAL_VARIABLE.equals(entity.getSubtype());
	}

	/** Returns whether the entity is a global variable. */
	public static boolean isGlobalVariable(ShallowEntity entity) {
		return entity.getType() == EShallowEntityType.ATTRIBUTE
				&& (entity.getParent() == null || entity.getParent()
						.getType() == EShallowEntityType.MODULE);
	}

	/** Returns whether the given attributes denotes a constant. */
	public static boolean isConstant(ShallowEntity entity) {
		if (entity.getType() != EShallowEntityType.ATTRIBUTE) {
			return false;
		}

		List<IToken> tokens = entity.ownStartTokens();

		// C++: const keyword, but no '*', as this indicates (variable) pointer
		// to const
		if (TokenStreamUtils.containsAny(tokens, ETokenType.CONST)
				&& !TokenStreamUtils.containsAny(tokens, ETokenType.MULT)) {
			return true;
		}

		// In java we require "static final", as "final" alone only indicates
		// immutable field but not "classic" constants
		if (TokenStreamUtils.containsAll(tokens, ETokenType.FINAL,
				ETokenType.STATIC)) {
			return true;
		}

		// If the attribute is part of an interface, it is always constant
		ShallowEntity parent = entity.getParent();

		return parent != null
				&& parent.getType().equals(EShallowEntityType.TYPE)
				&& parent.getSubtype().equals(SubTypeNames.INTERFACE);
	}

	/**
	 * Checks if <code>continue</code> or <code>break</code> are contained in a
	 * <code>for</code> or <code>while</code> loop in the given block. If there
	 * is <code>continue</code>, <code>break</code> or <code>return;</code> in a
	 * code segment, it can not be extracted. (Note: <code>return sth;</code>
	 * may be extractable.)
	 *
	 * @param block
	 *            the {@link ShallowEntity ShallowEntities} that will be
	 *            checked.
	 * @return <code>true</code> if there is <code>continue</code> or
	 *         <code>break</code>
	 */
	public static boolean containsLoopReferenceWithoutLoop(
			List<ShallowEntity> block) {
		for (ShallowEntity statement : block) {
			if (statement.getSubtype().equals(SubTypeNames.FOR)
					|| statement.getSubtype().equals(SubTypeNames.WHILE)) {
				continue;
			}
			List<IToken> startTokens = statement.ownStartTokens();
			IToken firstToken = startTokens.get(0);
			if (firstToken.getType() == ETokenType.CONTINUE
					|| firstToken.getType() == ETokenType.BREAK) {
				return true;
			} else if (firstToken.getType() == ETokenType.RETURN) {
				// return statements are not extractable (for us), even if
				// return obj;
				// may be extractable if it is at the very end of the original
				// method
				return true;
			}
		}
		for (ShallowEntity statement : block) {
			if (containsLoopReferenceWithoutLoop(statement.getChildren())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns for a given entity that contains an <code>if</code> or
	 * <code>try</code> block a list of {@link ShallowEntity ShallowEntities}
	 * that represents the corresponding <code>else if</code> or
	 * <code>catch</code> and <code>else</code> or <code>finally</code> block.
	 * That means that the complete statement is determined and returned.
	 *
	 * @param shallowEntity
	 *            the entity that contains an <code>if</code> or
	 *            <code>try</code> block
	 * @return The list of {@link ShallowEntity ShallowEntities} that contains
	 *         the corresponding block with all <code>else if</code> or
	 *         <code>catch</code> and <code>else</code> or <code>finally</code>
	 *         entities belonging to the given shallowEntity
	 */
	public static List<ShallowEntity> getCompleteStatement(
			ShallowEntity shallowEntity) {

		String secondKeyword = "";
		String thirdKeyword = "";

		if (shallowEntity.getSubtype().equals(SubTypeNames.IF)) {
			secondKeyword = SubTypeNames.ELSE_IF;
			thirdKeyword = SubTypeNames.ELSE;
		} else if (shallowEntity.getSubtype().equals(SubTypeNames.TRY)) {
			secondKeyword = SubTypeNames.CATCH;
			thirdKeyword = SubTypeNames.FINALLY;
		} else if (shallowEntity.getSubtype().equals(SubTypeNames.SWITCH)) {
			secondKeyword = SubTypeNames.CASE;
			thirdKeyword = SubTypeNames.DEFAULT;
		} else {
			return CollectionUtils.emptyList();
		}

		return resembleCompleteStatement(shallowEntity, secondKeyword,
				thirdKeyword);
	}

	/**
	 * For a given {@link ShallowEntity} of subtype <code>if</code>,
	 * <code>try</code> or <code>switch</code> and the corresponding keywords
	 * the complete statement is resembled
	 *
	 * @param shallowEntity
	 *            A {@link ShallowEntity} of subtype <code>if</code>,
	 *            <code>try</code> or <code>switch</code>.
	 * @param secondKeyword
	 *            The corresponding second keyword, for <code>if</code> that is
	 *            <code>else if</code>.
	 * @param thirdKeyword
	 *            The corresponding third keyword, for <code>if</code> that is
	 *            <code>else</code>.
	 * @return the complete statement
	 */
	private static List<ShallowEntity> resembleCompleteStatement(
			ShallowEntity shallowEntity, String secondKeyword,
			String thirdKeyword) {
		List<ShallowEntity> completeStatement = new ArrayList<ShallowEntity>();
		completeStatement.add(shallowEntity);
		List<ShallowEntity> children = shallowEntity.getParent().getChildren();

		boolean firstKeywordFound = false;
		// Construct the complete statement: First if/try, then as many else
		// if/catch as you need and finally (optionally) one else/finally
		// statement.
		for (ShallowEntity child : children) {
			if (!firstKeywordFound) {
				if (child.equals(shallowEntity)) {
					firstKeywordFound = true;
				}
			} else if (child.getSubtype().equals(secondKeyword)) {
				completeStatement.add(child);

			} else if (child.getSubtype().equals(thirdKeyword)) {
				completeStatement.add(child);
				return completeStatement;
			} else {
				break;
			}
		}

		return completeStatement;
	}

	/**
	 * Returns the entity which is directly after the given
	 * {@link ShallowEntity}.
	 *
	 * @param candidate
	 *            the last entity of the candidate
	 * @return The first entity which begins after the the given
	 *         {@link ShallowEntity}. If there is no such entity,
	 *         <code>null</code> will be returned.
	 */
	public static ShallowEntity getFirstEntityAfter(ShallowEntity candidate) {
		List<ShallowEntity> childrenOfParent = candidate.getParent()
				.getChildrenOfType(EShallowEntityType.STATEMENT);

		int indexOfNextCandidate = childrenOfParent.indexOf(candidate) + 1;
		if (indexOfNextCandidate < childrenOfParent.size()) {
			return childrenOfParent.get(indexOfNextCandidate);
		}
		return null;
	}

	/**
	 * Returns the entity which is directly before the given
	 * {@link ShallowEntity}.
	 *
	 * @param candidate
	 *            the first entity of the candidate
	 * @return The last entity which ends before the the given
	 *         {@link ShallowEntity}. If there is no such entity,
	 *         <code>null</code> will be returned.
	 */
	public static ShallowEntity getLastEntityBefore(ShallowEntity candidate) {
		List<ShallowEntity> childrenOfParent = candidate.getParent()
				.getChildrenOfType(EShallowEntityType.STATEMENT);

		int indexOfPreviousCandidate = childrenOfParent.indexOf(candidate) - 1;
		if (indexOfPreviousCandidate >= 0) {
			return childrenOfParent.get(indexOfPreviousCandidate);
		}
		return null;
	}

	/**
	 * Returns the nesting area of a list of {@link ShallowEntity
	 * ShallowEntities}. The nesting area is the sum of the nesting depth of all
	 * {@link ShallowEntity ShallowEntities}.
	 *
	 * @param shallowEntities
	 *            the {@link ShallowEntity ShallowEntities}.
	 * @return The nesting area of the given entities.
	 */
	public static int getNestingArea(List<ShallowEntity> shallowEntities) {
		int currentDepth = 0;
		int nestingArea = 0;
		for (ShallowEntity shallowEntity : shallowEntities) {
			nestingArea += getNestingArea(shallowEntity, currentDepth);
		}
		return nestingArea;
	}

	/**
	 * Returns the nesting area of a {@link ShallowEntity}. The nesting area is
	 * the sum of the nesting depth of all {@link ShallowEntity ShallowEntities}
	 * .
	 *
	 * @param shallowEntity
	 *            the {@link ShallowEntity}.
	 * @return The nesting area of the given entities.
	 */
	private static int getNestingArea(ShallowEntity shallowEntity,
			int currentDepth) {
		int nestingArea = currentDepth;
		for (ShallowEntity child : shallowEntity
				.getChildrenOfType(EShallowEntityType.STATEMENT)) {
			nestingArea += getNestingArea(child, currentDepth + 1);
		}
		return nestingArea;
	}

	/**
	 * Returns the maximal nesting depth of the given block.
	 *
	 * @param block
	 *            the {@link ShallowEntity ShallowEntities} that are taken into
	 *            account for the maximal nesting depth.
	 * @return The maximal nesting depth
	 */
	public static int getNestingDepth(List<ShallowEntity> block) {
		int maxNestingDepth = 0;
		for (ShallowEntity shallowEntity : block) {
			int entityNestingDepth = getNestingDepth(shallowEntity);

			if (entityNestingDepth > maxNestingDepth) {
				maxNestingDepth = entityNestingDepth;
			}
		}
		return maxNestingDepth;
	}

	/**
	 * Returns the maximal nesting depth of the given entity.
	 *
	 * @param shallowEntity
	 *            the {@link ShallowEntity}.
	 * @return The maximal nesting depth
	 */
	private static int getNestingDepth(ShallowEntity shallowEntity) {
		int maxNestingDepth = 0;
		for (ShallowEntity child : shallowEntity
				.getChildrenOfType(EShallowEntityType.STATEMENT)) {
			int childNestingDepth = getNestingDepth(child);

			if (maxNestingDepth <= childNestingDepth) {
				maxNestingDepth = childNestingDepth + 1;
			}
		}
		return maxNestingDepth;
	}

	/**
	 * Returns the number of blank or commented lines after the given
	 * {@link ShallowEntity}.
	 *
	 * @param candidate
	 *            if a refactoring candidate should be considered, take its last
	 *            {@link ShallowEntity}.
	 * @return The number of blank or commented lines
	 */
	public static int getNumberOfBlankLinesOrCommentsAfter(
			ShallowEntity candidate) {
		int lineDifference = 1;
		ShallowEntity firstEntityAfter = getFirstEntityAfter(candidate);
		if (firstEntityAfter == null) {
			if (candidate.getParent().getEndLine()
					- candidate.getEndLine() > 1) {
				lineDifference = candidate.getParent().getEndLine()
						- candidate.getEndLine();
			}
			return lineDifference - 1;
		}
		lineDifference = firstEntityAfter.getStartLine()
				- candidate.getEndLine();
		return lineDifference - 1;
	}

	/**
	 * Returns the number of blank or commented lines before the given
	 * {@link ShallowEntity}.
	 *
	 * @param candidate
	 *            if a refactoring candidate should be considered, take its
	 *            first {@link ShallowEntity}.
	 * @return The number of blank or commented lines
	 */
	public static int getNumberOfBlankLinesOrCommentsBefore(
			ShallowEntity candidate) {
		int lineDifference = 1;
		ShallowEntity lastEntityBefore = getLastEntityBefore(candidate);
		if (lastEntityBefore == null) {
			if (candidate.getStartLine()
					- candidate.getParent().getStartLine() > 1) {
				lineDifference = candidate.getStartLine()
						- candidate.getParent().getStartLine();
			}
			return lineDifference - 1;
		}
		lineDifference = candidate.getStartLine()
				- lastEntityBefore.getEndLine();
		return lineDifference - 1;
	}

	/**
	 * Returns the nesting depth of the remainder (all statements in the method
	 * without those of the candidate)
	 *
	 * @param method
	 *            the method (all {@link ShallowEntity} are taken into account
	 *            but the ones that are contained in the candidate).
	 * @param candidate
	 *            {@link ShallowEntity ShallowEntities} that are covered by the
	 *            candidate are not taken into account for the nesting depth.
	 * @return The nesting depth of the remainder
	 */
	public static int getRemainderNestingDepth(List<ShallowEntity> method,
			List<ShallowEntity> candidate) {
		int maxNestingDepth = 0;
		for (ShallowEntity shallowEntity : method) {
			// TODO (LH) Calls contains on list. Potential performance issue?
			if (!candidate.contains(shallowEntity)) {
				int entityNestingDepth = getRemainderNestingDepth(shallowEntity,
						candidate);

				if (entityNestingDepth > maxNestingDepth) {
					maxNestingDepth = entityNestingDepth;
				}
			}
		}
		return maxNestingDepth;
	}

	/**
	 * Returns the nesting depth of the remainder (all statements in the method
	 * without those of the candidate)
	 *
	 * @param shallowEntity
	 *            the method (all {@link ShallowEntity} are taken into account
	 *            but the ones that are contained in the candidate).
	 * @param candidate
	 *            {@link ShallowEntity ShallowEntities} that are covered by the
	 *            candidate are not taken into account for the nesting depth.
	 * @return The nesting depth of the remainder
	 */
	private static int getRemainderNestingDepth(ShallowEntity shallowEntity,
			List<ShallowEntity> candidate) {
		int maxNestingDepth = 0;
		for (ShallowEntity child : shallowEntity
				.getChildrenOfType(EShallowEntityType.STATEMENT)) {
			// TODO (LH) Calls contains on list. Potential performance issue?
			if (!candidate.contains(child)) {
				int childNestingDepth = getNestingDepth(child);

				if (childNestingDepth >= maxNestingDepth) {
					maxNestingDepth = childNestingDepth + 1;
				}
			}
		}
		return maxNestingDepth;
	}

	/**
	 * Returns a list of variable names (identifiers) that must be returned from
	 * the specified block if it is extracted into a new method
	 *
	 * @param block
	 *            {@link ShallowEntity ShallowEntities} that might be extracted
	 * @return List of identifiers
	 */
	public static List<String> getReturnParameters(List<ShallowEntity> block) {
		List<String> returnParameters = new ArrayList<String>();
		List<ShallowEntity> allEntities = ShallowEntityTraversalUtils
				.listEntitiesOfType(block, EShallowEntityType.STATEMENT);
		for (ShallowEntity statement : allEntities) {
			if (statement.includedTokens().get(0)
					.getType() == ETokenType.RETURN) {
				returnParameters.add(statement.getName());
			}
		}

		return returnParameters;
	}

	/** Returns all statements in the given entity */
	public static List<ShallowEntity> getStatements(ShallowEntity entity) {
		List<ShallowEntity> list = new ArrayList<ShallowEntity>();
		list.add(entity);
		return ShallowEntityTraversalUtils.listEntitiesOfType(list,
				EShallowEntityType.STATEMENT);
	}

	/**
	 * Obtain all classes (including anonymous classes) that contain directly or
	 * indirectly the method corresponding to the given line numbers.
	 *
	 * @param shallowEntities
	 *            the {@link ShallowEntity ShallowEntities} that represent the
	 *            file in which the finding was found
	 * @param startLine
	 *            the start line of the code area that must be covered by the
	 *            classes
	 * @param endLine
	 *            the end line of the code area that must be covered by the
	 *            classes
	 * @return A list that contains at least one {@link ShallowEntity} with type
	 *         TYPE.
	 */
	public static List<ShallowEntity> getClassEntities(
			List<ShallowEntity> shallowEntities, int startLine, int endLine) {

		List<ShallowEntity> classEntities = ShallowEntityTraversalUtils
				.listEntitiesOfType(shallowEntities, EShallowEntityType.TYPE);

		List<ShallowEntity> classEntitiesCoveringCodeArea = new ArrayList<ShallowEntity>();
		for (ShallowEntity classEntity : classEntities) {
			if (classEntity.getStartLine() <= startLine
					&& classEntity.getEndLine() >= endLine) {
				classEntitiesCoveringCodeArea.add(classEntity);
			}
		}
		return classEntitiesCoveringCodeArea;
	}

	/**
	 * Extracts from a given token (that represents a file) all the attributes
	 * (variables) that are declared directly in a type (class) and not within a
	 * method etc.
	 *
	 * @param classEntities
	 *            the {@link ShallowEntity} of the class that contains the
	 *            method for which suggestions are to be generated
	 * @return A list containing all the names (identifiers) of the global
	 *         variables
	 */
	public static List<String> getClassAttributes(
			List<ShallowEntity> classEntities) {

		List<String> globalVariables = new ArrayList<String>();

		for (ShallowEntity classEntity : classEntities) {

			List<ShallowEntity> attributes = classEntity
					.getChildrenOfType(EShallowEntityType.ATTRIBUTE);

			for (ShallowEntity attribute : attributes) {
				globalVariables.add(attribute.getName());
			}
		}

		return globalVariables;
	}

	/** Checks if the entity is a loop (for, foreach, while). */
	public static boolean isLoop(ShallowEntity entity) {
		return LOOP_SUBTYPES.contains(entity.getSubtype());
	}

	/**
	 * Checks if the given entity is part of a branching statement but not the
	 * first option of the branches. Those statements must not be separated from
	 * the first part of the branching statement when we consider refactoring
	 * suggestions.
	 * 
	 * @return For entities of {@link SubTypeNames subtype} ELSE IF, ELSE,
	 *         CATCH, FINALLY, CASE, DEFAULT, USING, DO will return
	 *         <code>true</code> but for other subtypes (including IF, SWITCH,
	 *         TRY) return <code>false</code>.
	 */
	public static boolean isSubpartOfBranchingStatement(ShallowEntity entity) {
		return SUBPART_BRANCHING_STATEMENTS.contains(entity.getSubtype());
	}

	/**
	 * Checks whether the given method is a C++ constructor.
	 */
	public static boolean isCppConstructor(ELanguage language,
			ShallowEntity entity) {
		return language.equals(ELanguage.CPP)
				&& entity.getType().equals(EShallowEntityType.METHOD)
				&& SubTypeNames.CONSTRUCTOR.equals(entity.getSubtype());

	}
}
