/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.fortran;

import static eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates.IN_ENUM;
import static eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates.IN_MODULE;
import static eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates.IN_SELECT;
import static eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates.TOP_LEVEL;
import static eu.cqse.check.framework.scanner.ETokenType.ALLOCATE;
import static eu.cqse.check.framework.scanner.ETokenType.ASYNCHRONOUS;
import static eu.cqse.check.framework.scanner.ETokenType.BLOCK;
import static eu.cqse.check.framework.scanner.ETokenType.CALL;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.CLOSE;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.COMMON;
import static eu.cqse.check.framework.scanner.ETokenType.CONCURRENT;
import static eu.cqse.check.framework.scanner.ETokenType.CONTAINS;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.CRITICAL;
import static eu.cqse.check.framework.scanner.ETokenType.CYCLE;
import static eu.cqse.check.framework.scanner.ETokenType.DATA;
import static eu.cqse.check.framework.scanner.ETokenType.DEALLOCATE;
import static eu.cqse.check.framework.scanner.ETokenType.DEFAULT;
import static eu.cqse.check.framework.scanner.ETokenType.DO;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE_COLON;
import static eu.cqse.check.framework.scanner.ETokenType.ELEMENTAL;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSEIF;
import static eu.cqse.check.framework.scanner.ETokenType.ELSEWHERE;
import static eu.cqse.check.framework.scanner.ETokenType.END;
import static eu.cqse.check.framework.scanner.ETokenType.ENDFILE;
import static eu.cqse.check.framework.scanner.ETokenType.ENTRY;
import static eu.cqse.check.framework.scanner.ETokenType.ENUM;
import static eu.cqse.check.framework.scanner.ETokenType.ENUMERATOR;
import static eu.cqse.check.framework.scanner.ETokenType.EOL;
import static eu.cqse.check.framework.scanner.ETokenType.EQUIVALENCE;
import static eu.cqse.check.framework.scanner.ETokenType.EXIT;
import static eu.cqse.check.framework.scanner.ETokenType.EXTERNAL;
import static eu.cqse.check.framework.scanner.ETokenType.FORALL;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.GOTO;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPLICIT;
import static eu.cqse.check.framework.scanner.ETokenType.INQUIRE;
import static eu.cqse.check.framework.scanner.ETokenType.INTEGER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.IS;
import static eu.cqse.check.framework.scanner.ETokenType.LOCK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MODULE;
import static eu.cqse.check.framework.scanner.ETokenType.NAMELIST;
import static eu.cqse.check.framework.scanner.ETokenType.NULLIFY;
import static eu.cqse.check.framework.scanner.ETokenType.OPEN;
import static eu.cqse.check.framework.scanner.ETokenType.PARAMETER;
import static eu.cqse.check.framework.scanner.ETokenType.PAUSE;
import static eu.cqse.check.framework.scanner.ETokenType.PRINT;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROCEDURE;
import static eu.cqse.check.framework.scanner.ETokenType.PROGRAM;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.PURE;
import static eu.cqse.check.framework.scanner.ETokenType.RECURSIVE;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.REWIND;
import static eu.cqse.check.framework.scanner.ETokenType.REWRITE;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SELECT;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SEQUENCE;
import static eu.cqse.check.framework.scanner.ETokenType.STOP;
import static eu.cqse.check.framework.scanner.ETokenType.SUBMODULE;
import static eu.cqse.check.framework.scanner.ETokenType.SUBROUTINE;
import static eu.cqse.check.framework.scanner.ETokenType.THEN;
import static eu.cqse.check.framework.scanner.ETokenType.TYPE;
import static eu.cqse.check.framework.scanner.ETokenType.UNLOCK;
import static eu.cqse.check.framework.scanner.ETokenType.USE;
import static eu.cqse.check.framework.scanner.ETokenType.VOLATILE;
import static eu.cqse.check.framework.scanner.ETokenType.WAIT;
import static eu.cqse.check.framework.scanner.ETokenType.WHERE;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WRITE;

import java.util.EnumSet;

import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.fortran.FortranShallowParser.EFortranParserStates;
import org.conqat.lib.commons.region.Region;
import eu.cqse.check.framework.scanner.ETokenType;

/**
 * A shallow parser for Fortran.
 * 
 * This parser supports the free form of Fortran 2008.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: B96028DDFA354B5E339BE8C0159D1736
 */
public class FortranShallowParser extends
		ShallowParserBase<EFortranParserStates> {

	/** All possible states of a FortranShallowParser. */
	public static enum EFortranParserStates {
		/** Top-level state. */
		TOP_LEVEL,

		/** Inside a module. */
		IN_MODULE,

		/** Inside a type declaration. */
		IN_TYPE,

		/** Inside a enum declaration. */
		IN_ENUM,

		/** Inside a method's body. */
		IN_METHOD,

		/** Inside a select statement. */
		IN_SELECT,
	}

	/** All tokens that indicate a simple statement. */
	private static final EnumSet<ETokenType> STATEMENT_TOKENS = EnumSet.of(
			ALLOCATE, ASYNCHRONOUS, CALL, CLOSE, COMMON, CONTINUE, CYCLE, DATA,
			DEALLOCATE, ENDFILE, ENTRY, EQUIVALENCE, EXIT, EXTERNAL, GOTO,
			IDENTIFIER, INTEGER_LITERAL, INQUIRE, LOCK, NAMELIST, NULLIFY,
			OPEN, PAUSE, PARAMETER, PRINT, RETURN, REWIND, REWRITE, STOP,
			UNLOCK, WAIT, WRITE, VOLATILE);

	/** All parser states in which statements can occur. */
	private static final EFortranParserStates[] STATEMENT_STATES = { IN_MODULE,
			IN_METHOD };

	/** All modifiers that can be used in front of a function declaration. */
	private static final EnumSet<ETokenType> FUNCTION_MODIFIERS = EnumSet.of(
			MODULE, ELEMENTAL, PURE, RECURSIVE);

	/** All modifiers that can be used in front of a procedure declaration. */
	private static final EnumSet<ETokenType> PROCEDURE_MODIFIERS = EnumSet
			.of(MODULE);

	/** All token types that can be used as a type. */
	private static final EnumSet<ETokenType> TYPE_TOKENS = EnumSet.of(
			IDENTIFIER, TYPE, CLASS);

	/** Constructor. */
	public FortranShallowParser() {
		super(EFortranParserStates.class, TOP_LEVEL);
		createTopLevelRules();
		createInModuleRules();
		createInTypeRules();
		createInEnumRule();
		createMethodRules();
		createStatementRules();
		createAnyStateRules();
	}

	/** Create rules for parsing top-level constructs like modules or program. */
	private void createTopLevelRules() {
		// module
		inState(TOP_LEVEL).sequence(MODULE, IDENTIFIER)
				.createNode(EShallowEntityType.MODULE, 0, 1).sequence(EOL)
				.parseUntil(IN_MODULE).sequence(END).optional(MODULE)
				.optional(IDENTIFIER).endNode();

		// program
		inState(TOP_LEVEL).sequence(PROGRAM, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, -1).sequence(EOL)
				.parseUntil(IN_MODULE).sequence(END).optional(PROGRAM)
				.optional(IDENTIFIER).endNode();

		// submodule
		inState(TOP_LEVEL).sequence(SUBMODULE)
				.skipToWithNesting(IDENTIFIER, LPAREN, RPAREN)
				.createNode(EShallowEntityType.MODULE, 0, -1).sequence(EOL)
				.parseUntil(IN_MODULE).sequence(END).optional(SUBMODULE)
				.optional(IDENTIFIER).endNode();
	}

	/** Create rules for parsing some meta information. */
	private void createAnyStateRules() {
		inAnyState().sequence(IMPLICIT).createNode(EShallowEntityType.META, 0)
				.skipTo(EOL).endNode();

		inAnyState().sequence(CONTAINS).createNode(EShallowEntityType.META, 0)
				.endNode();

		inState(IN_MODULE, IN_METHOD).sequence(USE)
				.createNode(EShallowEntityType.META, 0).skipTo(EOL).endNode();
	}

	/** Create rules for parsing the body of a module. */
	private void createInModuleRules() {
		// visibility
		inState(IN_MODULE).sequence(EnumSet.of(PUBLIC, PRIVATE))
				.createNode(EShallowEntityType.META, 0).skipTo(EOL).endNode();

		// interface with name
		inState(IN_MODULE).sequence(INTERFACE, IDENTIFIER)
				.createNode(EShallowEntityType.TYPE, 0, 1)
				.parseUntil(IN_MODULE).sequence(END).optional(INTERFACE)
				.sequence(IDENTIFIER).endNode();

		// interface with no name
		inState(IN_MODULE).sequence(INTERFACE)
				.createNode(EShallowEntityType.TYPE, 0).parseUntil(IN_MODULE)
				.sequence(END).optional(INTERFACE).endNode();

		// type declaration
		RecognizerBase<EFortranParserStates> typeAlternative = inState(
				IN_MODULE).sequence(TYPE);
		typeAlternative.optional(DOUBLE_COLON).sequence(IDENTIFIER, EOL)
				.createNode(EShallowEntityType.TYPE, 0, -2).parseUntil(IN_TYPE)
				.sequence(END).optional(TYPE).optional(IDENTIFIER).endNode();
		typeAlternative.sequence(COMMA)
				.skipBefore(EnumSet.of(DOUBLE_COLON, EOL))
				.sequence(DOUBLE_COLON, IDENTIFIER, EOL)
				.createNode(EShallowEntityType.TYPE, 0, -2).parseUntil(IN_TYPE)
				.sequence(END).optional(TYPE).optional(IDENTIFIER).endNode();

		// enum declaration
		inState(IN_MODULE).sequence(ENUM)
				.skipBefore(EnumSet.of(DOUBLE_COLON, EOL))
				.sequence(DOUBLE_COLON, IDENTIFIER, EOL)
				.createNode(EShallowEntityType.TYPE, 0, -2).parseUntil(IN_ENUM)
				.sequence(END).optional(ENUM).optional(IDENTIFIER).endNode();
	}

	/** Create rules for parsing type bodies. */
	private void createInTypeRules() {
		// sequence
		inState(IN_TYPE).sequence(SEQUENCE, EOL)
				.createNode(EShallowEntityType.META, 0).endNode();

		// attribute
		inState(IN_TYPE)
				.sequence(TYPE_TOKENS)
				.skipBefore(EnumSet.of(DOUBLE_COLON, EOL))
				.sequence(DOUBLE_COLON, IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ATTRIBUTE, -1).skipTo(EOL).endNode();

		// procedure
		inState(IN_TYPE).sequence(PROCEDURE, DOUBLE_COLON, IDENTIFIER)
				.skipTo(EOL).createNode(EShallowEntityType.METHOD, 0, 2)
				.endNode();
	}

	/** Create rule for parsing an enum body. */
	private void createInEnumRule() {
		inState(IN_ENUM).sequence(ENUMERATOR).optional(DOUBLE_COLON)
				.sequence(IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE, 0, -1).skipTo(EOL)
				.endNode();
	}

	/** Create rules for parsing method declarations. */
	private void createMethodRules() {
		// subroutine
		inState(TOP_LEVEL, IN_MODULE, IN_METHOD)
				.sequence(SUBROUTINE, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, 1).skipTo(EOL)
				.parseUntil(IN_METHOD).sequence(END).optional(SUBROUTINE)
				.optional(IDENTIFIER).endNode();

		// function
		inState(TOP_LEVEL, IN_MODULE, IN_METHOD)
				.optional(FUNCTION_MODIFIERS, TYPE_TOKENS)
				.skipBefore(EnumSet.of(FUNCTION, EOL))
				.sequence(FUNCTION, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, -2, -1).skipTo(EOL)
				.parseUntil(IN_METHOD).sequence(END).optional(FUNCTION)
				.optional(IDENTIFIER).endNode();

		// variable declaration
		inState(IN_MODULE, IN_METHOD)
				.sequence(TYPE_TOKENS)
				.skipBefore(EnumSet.of(DOUBLE_COLON, EOL))
				.sequence(DOUBLE_COLON, IDENTIFIER)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.LOCAL_VARIABLE, -1).skipTo(EOL).endNode();

		// procedure
		inState(IN_MODULE, IN_TYPE).repeated(PROCEDURE_MODIFIERS)
				.sequence(PROCEDURE, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, -2, -1).skipTo(EOL)
				.endNode();
	}

	/**
	 * Create rules for parsing statements. All constructs are taken from
	 * http://de.wikibooks.org/wiki/Fortran:_Fortran_95:
	 * _Verzweigungen_und_Schleifen.
	 */
	private void createStatementRules() {
		createArithmeticIfRule();

		createSingleLineConstructs(IF, WHERE, FORALL);

		createLoopRules();
		createSelectRules();

		createElseIfRule();
		createContinuedConstructRules(IF, ELSEIF, ELSE, THEN);
		createContinuedConstructRules(WHERE, ELSEWHERE, ELSEWHERE, null);

		createConcurrentRules();

		createSimpleStatementRules();
	}

	/**
	 * Create rule for parsing the arithmetic if construct (e.g. 'if
	 * (&lt;arithmetic expression&gt;) 1, 2, 3')
	 */
	private void createArithmeticIfRule() {
		inState(STATEMENT_STATES)
				.sequence(IF)
				.skipNested(LPAREN, RPAREN)
				.sequence(INTEGER_LITERAL, COMMA, INTEGER_LITERAL, COMMA,
						INTEGER_LITERAL, EOL)
				.createNode(EShallowEntityType.STATEMENT, 0).endNode();
	}

	/**
	 * Create rules for parsing constructs with a condition in parenthesis and
	 * one statement in the same line for all given token types. A single line
	 * construct is e.g. 'if (&lt;condition&gt;) &lt;statement&gt;'
	 */
	private void createSingleLineConstructs(ETokenType... constructTokens) {
		for (ETokenType constructToken : constructTokens) {
			inState(STATEMENT_STATES).sequence(constructToken, LPAREN)
					.skipToWithNesting(RPAREN, LPAREN, RPAREN)
					.sequenceBefore(STATEMENT_TOKENS)
					.createNode(EShallowEntityType.STATEMENT, 0)
					.parseOnce(IN_METHOD).sequence(EOL).endNode();
		}
	}

	/** Create rules for parsing block and critical sections. */
	private void createConcurrentRules() {
		inState(STATEMENT_STATES).sequence(BLOCK)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(EOL)
				.parseUntil(IN_METHOD).sequence(END).optional(BLOCK).endNode();

		inState(STATEMENT_STATES).sequence(CRITICAL)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(EOL)
				.parseUntil(IN_METHOD).sequence(END).optional(CRITICAL)
				.endNode();
	}

	/** Create rules for parsing a select statement. */
	private void createSelectRules() {
		finishConstructWithOptionalName(
				inState(STATEMENT_STATES)
						.optional(IDENTIFIER, COLON)
						.markStart()
						.sequence(SELECT)
						.optional(EnumSet.of(CASE, TYPE))
						.sequence(LPAREN)
						.skipToWithNesting(RPAREN, LPAREN, RPAREN)
						.sequence(EOL)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.SELECT).parseUntil(IN_SELECT),
				SELECT);

		RecognizerBase<EFortranParserStates> labelAlternative = inState(
				IN_SELECT).sequence(EnumSet.of(CASE, TYPE, CLASS)).optional(IS)
				.skipNested(LPAREN, RPAREN).optional(IDENTIFIER).sequence(EOL)
				.createNode(EShallowEntityType.META, 0).parseUntil(IN_METHOD);
		labelAlternative.sequenceBefore(END).endNode();
		labelAlternative.sequenceBefore(EnumSet.of(CASE, TYPE, CLASS))
				.endNodeWithContinuation();

		inState(IN_SELECT).sequence(EnumSet.of(CASE, CLASS), DEFAULT)
				.optional(IDENTIFIER).sequence(EOL)
				.createNode(EShallowEntityType.META, 0).parseUntil(IN_METHOD)
				.sequenceBefore(END).endNode();
	}

	/** Create rules for parsing do and forall loops. */
	private void createLoopRules() {
		// do/do concurrent/do while loop
		finishConstructWithOptionalName(
				inState(STATEMENT_STATES)
						.optional(IDENTIFIER, COLON)
						.markStart()
						.sequence(DO)
						.optional(EnumSet.of(CONCURRENT, WHILE))
						.createNode(EShallowEntityType.STATEMENT,
								new Region(0, -1)).skipTo(EOL)
						.parseUntil(IN_METHOD), DO);

		// forall loop
		finishConstructWithOptionalName(
				inState(STATEMENT_STATES).optional(IDENTIFIER, COLON)
						.markStart().sequence(FORALL, LPAREN)
						.skipToWithNesting(RPAREN, LPAREN, RPAREN)
						.createNode(EShallowEntityType.STATEMENT, 0)
						.sequence(EOL).parseUntil(IN_METHOD), FORALL);
	}

	/**
	 * Create a special rule for handling 'else if' that consists of two
	 * separate tokens.
	 */
	private void createElseIfRule() {
		RecognizerBase<EFortranParserStates> recognizer = inState(
				STATEMENT_STATES)
				.sequence(ELSE, IF, LPAREN)
				.skipToWithNesting(RPAREN, LPAREN, RPAREN)
				.sequence(THEN)
				.optional(IDENTIFIER)
				.sequence(EOL)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.ELSE_IF_NOSPACE).parseUntil(IN_METHOD);
		finishConstructWithOptionalName(recognizer, IF);
		recognizer.sequenceBefore(EnumSet.of(ELSE, ELSEIF))
				.endNodeWithContinuation();
	}

	/**
	 * Create rules for parsing constructs than can be continued. The given
	 * begin token is the token that begins the construct for e.g. 'if'. The
	 * continuation token can continue a construct, e.g. 'else if'. The final
	 * token is the last unconditional continuation token, e.g. 'else'. The
	 * complete construct is ended with 'end &lt;beginToken&gt;'. The given
	 * suffix token must be recognized after the parenthesis of the construct
	 * e.g. 'then' in an 'if' construct. The suffix token can be null, if there
	 * is none. The construct can be prefixed by a construct name, e.g. 'name:if
	 * (...)'
	 */
	private void createContinuedConstructRules(ETokenType beginToken,
			ETokenType continuationToken, ETokenType finalToken,
			ETokenType suffixToken) {
		createContinuedConstructRuleWithName(beginToken, beginToken,
				EnumSet.of(continuationToken, finalToken), suffixToken, true);
		createContinuedConstructRuleWithName(beginToken, continuationToken,
				EnumSet.of(continuationToken, finalToken), suffixToken, false);
		createEndContinuedConstructRule(beginToken, finalToken);
	}

	/**
	 * Create a rule for parsing a statement of a construct that can be
	 * continued. The construct token is the token of the construct type, e.g.
	 * 'if'. The statement token is the beginning token of the construct
	 * statement, e.g. 'if' or 'else if'. The given continuation tokens are
	 * those, that can continue the current construct statement. The given
	 * suffix token must be recognized after the parenthesis of the construct
	 * e.g. 'then' in an 'if' construct. The suffix token can be null, if there
	 * is none. If nameInFront is true, an optional name label can prefix the
	 * construct. If it is false, the construct can be followed by a name.
	 */
	private void createContinuedConstructRuleWithName(
			ETokenType constructToken, ETokenType partToken,
			EnumSet<ETokenType> continuationTokens, ETokenType suffixToken,
			boolean nameInFront) {
		RecognizerBase<EFortranParserStates> recognizer = inState(STATEMENT_STATES);
		if (nameInFront) {
			recognizer = recognizer.optional(IDENTIFIER, COLON);
		}
		recognizer = recognizer.markStart().sequence(partToken, LPAREN)
				.skipToWithNesting(RPAREN, LPAREN, RPAREN);
		if (suffixToken != null) {
			recognizer = recognizer.sequence(suffixToken);
		}
		if (!nameInFront) {
			recognizer = recognizer.optional(IDENTIFIER);
		}
		recognizer = recognizer.sequence(EOL)
				.createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(IN_METHOD);

		finishConstructWithOptionalName(recognizer, constructToken);
		recognizer.sequenceBefore(continuationTokens).endNodeWithContinuation();
	}

	/**
	 * Create a rule for parsing the end of a continued construct. The construct
	 * token is the token of the construct type, e.g. 'if'. The final token is
	 * the beginning token of the statement, e.g. 'else'. An optional identifier
	 * can follow for named constructs.
	 */
	private void createEndContinuedConstructRule(ETokenType constructToken,
			ETokenType finalToken) {
		RecognizerBase<EFortranParserStates> finalAlternative = inState(
				STATEMENT_STATES).sequence(finalToken).optional(IDENTIFIER)
				.sequence(EOL).createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(IN_METHOD);

		finishConstructWithOptionalName(finalAlternative, constructToken);
	}

	/**
	 * Finishes the given recognizer with 'end' and the optional given token
	 * type. An optional identifier can follow for named constructs.
	 */
	private void finishConstructWithOptionalName(
			RecognizerBase<EFortranParserStates> recognizer,
			ETokenType constructType) {
		recognizer.sequence(END).optional(constructType).optional(IDENTIFIER)
				.sequence(EOL).endNode();
	}

	/** Creates rules for parsing simple statements. */
	private void createSimpleStatementRules() {
		inState(STATEMENT_STATES)
				.sequence(STATEMENT_TOKENS)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0)
				.skipBefore(EnumSet.of(EOL, SEMICOLON)).optional(SEMICOLON)
				.endNode();
	}
}
