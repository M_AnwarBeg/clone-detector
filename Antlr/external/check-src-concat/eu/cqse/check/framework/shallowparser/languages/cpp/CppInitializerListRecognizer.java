/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * A recognizer that skips the initializer list of a C++ constructor.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56397 $
 * @ConQAT.Rating YELLOW Hash: EC6DD8C36C8F984822027828FB366C53
 */
public class CppInitializerListRecognizer
		extends RecognizerBase<EGenericParserStates> {

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EGenericParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		while (true) {
			startOffset = matchInitializer(tokens, startOffset);
			if (startOffset == NO_MATCH) {
				return NO_MATCH;
			}

			if (startOffset >= tokens.size()
					|| tokens.get(startOffset).getType() != COMMA) {
				return startOffset;
			}

			startOffset++;
		}
	}

	/**
	 * Matches one initializer within the initalizer list (e.g.
	 * "member{value}").
	 */
	private static int matchInitializer(List<IToken> tokens, int startOffset) {
		// 4 is the minimum length of an initializer
		if (startOffset > tokens.size() - 4
				|| tokens.get(startOffset).getType() != IDENTIFIER) {
			return NO_MATCH;
		}

		ETokenType openingType = tokens.get(startOffset + 1).getType();
		ETokenType closingType = getClosingType(openingType);
		if (closingType == null) {
			return NO_MATCH;
		}

		int depth = 1;
		for (int i = startOffset + 2; i < tokens.size(); i++) {
			ETokenType type = tokens.get(i).getType();
			if (type == openingType) {
				depth++;
			} else if (type == closingType) {
				depth--;
				if (depth == 0) {
					return i + 1;
				}
			}
		}
		return NO_MATCH;
	}

	/**
	 * Returns the corresponding closing type for the given opening type, namely
	 * RPAREN for LPAREN and RBRACE for LBRACE.
	 */
	private static ETokenType getClosingType(ETokenType openingType) {
		switch (openingType) {
		case LPAREN:
			return RPAREN;
		case LBRACE:
			return RBRACE;
		default:
			return null;
		}
	}
}
