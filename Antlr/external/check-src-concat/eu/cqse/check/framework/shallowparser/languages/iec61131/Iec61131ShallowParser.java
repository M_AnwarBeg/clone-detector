/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.iec61131;

import static eu.cqse.check.framework.scanner.ETokenType.ACTION;
import static eu.cqse.check.framework.scanner.ETokenType.ALGORITHM;
import static eu.cqse.check.framework.scanner.ETokenType.ALGORITHM_BLOCK;
import static eu.cqse.check.framework.scanner.ETokenType.AUTOSTART;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONSTANT;
import static eu.cqse.check.framework.scanner.ETokenType.DO;
import static eu.cqse.check.framework.scanner.ETokenType.ELEMENT;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSIF;
import static eu.cqse.check.framework.scanner.ETokenType.END;
import static eu.cqse.check.framework.scanner.ETokenType.END_ACTION;
import static eu.cqse.check.framework.scanner.ETokenType.END_ALGORITHM;
import static eu.cqse.check.framework.scanner.ETokenType.END_ALGORITHM_BLOCK;
import static eu.cqse.check.framework.scanner.ETokenType.END_CASE;
import static eu.cqse.check.framework.scanner.ETokenType.END_FOR;
import static eu.cqse.check.framework.scanner.ETokenType.END_FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.END_FUNCTION_BLOCK;
import static eu.cqse.check.framework.scanner.ETokenType.END_IF;
import static eu.cqse.check.framework.scanner.ETokenType.END_IMPORT;
import static eu.cqse.check.framework.scanner.ETokenType.END_OBJECT;
import static eu.cqse.check.framework.scanner.ETokenType.END_STEP;
import static eu.cqse.check.framework.scanner.ETokenType.END_TRANSITION;
import static eu.cqse.check.framework.scanner.ETokenType.END_VAR;
import static eu.cqse.check.framework.scanner.ETokenType.END_WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.EVENT_ALGORITHM;
import static eu.cqse.check.framework.scanner.ETokenType.EXIT;
import static eu.cqse.check.framework.scanner.ETokenType.EXIT_TRANSITION;
import static eu.cqse.check.framework.scanner.ETokenType.FLOATING_POINT_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION_BLOCK;
import static eu.cqse.check.framework.scanner.ETokenType.GO_ON_EXIT_TRANSITION;
import static eu.cqse.check.framework.scanner.ETokenType.GO_ON_TRANSITION;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPORT;
import static eu.cqse.check.framework.scanner.ETokenType.INITIAL_STEP;
import static eu.cqse.check.framework.scanner.ETokenType.INTEGER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.OF;
import static eu.cqse.check.framework.scanner.ETokenType.ON;
import static eu.cqse.check.framework.scanner.ETokenType.PLAUSIBILITY_FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.POSTUPDATE_ALGORITHM;
import static eu.cqse.check.framework.scanner.ETokenType.PROCESS_ALGORITHM;
import static eu.cqse.check.framework.scanner.ETokenType.REPEAT;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.STEP;
import static eu.cqse.check.framework.scanner.ETokenType.SYSTEM_OBJECT;
import static eu.cqse.check.framework.scanner.ETokenType.SYSTEM_VAR;
import static eu.cqse.check.framework.scanner.ETokenType.SYSTEM_VAR_DECL;
import static eu.cqse.check.framework.scanner.ETokenType.SYSTEM_VAR_IN;
import static eu.cqse.check.framework.scanner.ETokenType.THEN;
import static eu.cqse.check.framework.scanner.ETokenType.TRANSITION;
import static eu.cqse.check.framework.scanner.ETokenType.UNTIL;
import static eu.cqse.check.framework.scanner.ETokenType.VAR;
import static eu.cqse.check.framework.scanner.ETokenType.VAR_EXTERNAL;
import static eu.cqse.check.framework.scanner.ETokenType.VAR_INPUT;
import static eu.cqse.check.framework.scanner.ETokenType.VAR_IN_OUT;
import static eu.cqse.check.framework.scanner.ETokenType.VAR_TEMP;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WITH;
import static eu.cqse.check.framework.scanner.ETokenType.XOR;

import java.util.EnumSet;

import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.iec61131.Iec61131ShallowParser.EIec61131ParserStates;
import eu.cqse.check.framework.scanner.ETokenType;

/**
 * Parser for the language IEC 61131-3 Structured Text.
 *
 * This parser does not (yet) support PROGRAM and CONFIGURATION (see
 * https://en.wikipedia.org/wiki/Structured_text), as we have no example code
 * for this.
 *
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: F4EC08559C41F1CFEC6B61E0C9D9C1D4
 */
public class Iec61131ShallowParser extends
		ShallowParserBase<EIec61131ParserStates> {

	/** The states used in this parser. */
	public static enum EIec61131ParserStates {

		/** Top-level state. */
		TOP_LEVEL,

		/** In variable declarations. */
		IN_VARS,

		/** In methods, algorithms, etc. */
		IN_METHOD,

		/** In case statement. */
		IN_CASE,

		/** System var decl of SV Files. */
		IN_SYSTEM_VAR_DECL
	}

	/** A set of all token types that mark the beginning of a simple statement. */
	private static final EnumSet<ETokenType> SIMPLE_STATEMENT_KEYWORDS = EnumSet
			.of(IDENTIFIER, RETURN, EXIT);

	/** A set of all token types that can be used as case literals. */
	private static final EnumSet<ETokenType> CASE_LITERALS = EnumSet.of(
			IDENTIFIER, INTEGER_LITERAL, FLOATING_POINT_LITERAL);

	/** Constructor. */
	public Iec61131ShallowParser() {
		super(EIec61131ParserStates.class, EIec61131ParserStates.TOP_LEVEL);

		createTopLevelRules();
		createInVariableRules();
		createInMethodRules();
		createInSystemVarDeclRules();
	}

	/** Creates the rules for parsing top-level elements. */
	private void createTopLevelRules() {
		inState(EIec61131ParserStates.TOP_LEVEL).sequence(IMPORT)
				.createNode(EShallowEntityType.META, 0).skipTo(END_IMPORT)
				.endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(ALGORITHM_BLOCK, IDENTIFIER)
				.optional(WITH, INTERFACE, IDENTIFIER)
				.createNode(EShallowEntityType.MODULE, 0, 1)
				.parseUntil(EIec61131ParserStates.TOP_LEVEL)
				.sequence(END_ALGORITHM_BLOCK).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(
						EnumSet.of(VAR, SYSTEM_VAR, SYSTEM_VAR_IN, VAR_EXTERNAL))
				.createNode(EShallowEntityType.META, 0)
				.parseUntil(EIec61131ParserStates.IN_VARS).sequence(END_VAR)
				.optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL).sequence(SYSTEM_OBJECT)
				.createNode(EShallowEntityType.META, 0)
				.parseUntil(EIec61131ParserStates.IN_VARS).sequence(END_OBJECT)
				.endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(ALGORITHM, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, 1)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_ALGORITHM).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(FUNCTION_BLOCK, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, 1)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_FUNCTION_BLOCK).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(PROCESS_ALGORITHM, IDENTIFIER, ON, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, 1)
				.optional(AUTOSTART)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_ALGORITHM).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(EVENT_ALGORITHM, IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, 0, 1)
				.skipTo(WITH, IDENTIFIER)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_ALGORITHM).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(EnumSet.of(EVENT_ALGORITHM, POSTUPDATE_ALGORITHM),
						IDENTIFIER).createNode(EShallowEntityType.METHOD, 0, 1)
				.skipTo(WITH, IDENTIFIER).optional(XOR)
				.repeated(COMMA, IDENTIFIER)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_ALGORITHM).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL)
				.sequence(EnumSet.of(FUNCTION, PLAUSIBILITY_FUNCTION),
						IDENTIFIER).createNode(EShallowEntityType.METHOD, 0, 1)
				.skipTo(COLON).sequence(IDENTIFIER)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_FUNCTION).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.TOP_LEVEL).sequence(SYSTEM_VAR_DECL)
				.createNode(EShallowEntityType.TYPE, 0)
				.parseUntil(EIec61131ParserStates.IN_SYSTEM_VAR_DECL)
				.sequence(END).endNode();
	}

	/** Creates the rules for parsing within variable blocks. */
	private void createInVariableRules() {
		inState(EIec61131ParserStates.IN_VARS)
				.sequence(IDENTIFIER, COLON)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.VARIABLE, 0).skipTo(SEMICOLON).endNode();
	}

	/** Creates the rules for parsing within methods (statements). */
	private void createInMethodRules() {
		inState(EIec61131ParserStates.IN_METHOD)
				.sequence(EnumSet.of(VAR, VAR_INPUT, VAR_TEMP, VAR_IN_OUT))
				.createNode(EShallowEntityType.META, 0).optional(CONSTANT)
				.parseUntil(EIec61131ParserStates.IN_VARS).sequence(END_VAR)
				.endNode();

		createCaseRule();
		createLoopRules();
		createIfElseRules();

		inState(EIec61131ParserStates.IN_METHOD)
				.sequence(
						EnumSet.of(GO_ON_TRANSITION, TRANSITION,
								EXIT_TRANSITION, GO_ON_EXIT_TRANSITION))
				.createNode(EShallowEntityType.STATEMENT, 0)
				.skipTo(END_TRANSITION).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.IN_METHOD)
				.sequence(ACTION, IDENTIFIER, COLON)
				.createNode(EShallowEntityType.STATEMENT, 0, 1)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_ACTION).optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.IN_METHOD)
				.sequence(EnumSet.of(STEP, INITIAL_STEP), IDENTIFIER, COLON)
				.createNode(EShallowEntityType.STATEMENT, 0, 1)
				.parseUntil(EIec61131ParserStates.IN_METHOD).sequence(END_STEP)
				.optional(SEMICOLON).endNode();

		createSimpleStatementRules();
	}

	/** Creates the rules for parsing the case statement. */
	private void createCaseRule() {
		inStatementStates().sequence(CASE)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(OF)
				.parseUntil(EIec61131ParserStates.IN_CASE).sequence(END_CASE)
				.optional(SEMICOLON).endNode();

		inState(EIec61131ParserStates.IN_CASE).repeated(CASE_LITERALS, COMMA)
				.sequence(CASE_LITERALS, COLON)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE_LABEL)
				.endNode();
		inState(EIec61131ParserStates.IN_CASE).sequence(ELSE)
				.createNode(EShallowEntityType.META, SubTypeNames.CASE_ELSE)
				.endNode();
	}

	/** Returns the states that expect "plain" statements. */
	private RecognizerBase<EIec61131ParserStates> inStatementStates() {
		return inState(EIec61131ParserStates.IN_METHOD,
				EIec61131ParserStates.IN_CASE);
	}

	/** Creates the rules for parsing loops. */
	private void createLoopRules() {
		inStatementStates().sequence(FOR)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DO)
				.parseUntil(EIec61131ParserStates.IN_METHOD).sequence(END_FOR)
				.optional(SEMICOLON).endNode();

		inStatementStates().sequence(WHILE)
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(DO)
				.parseUntil(EIec61131ParserStates.IN_METHOD)
				.sequence(END_WHILE).optional(SEMICOLON).endNode();

		inStatementStates().sequence(REPEAT)
				.createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(EIec61131ParserStates.IN_METHOD).sequence(UNTIL)
				.skipTo(SEMICOLON).endNode();
	}

	/** Create rules for parsing if/else structures. */
	private void createIfElseRules() {
		RecognizerBase<EIec61131ParserStates> ifAlternative = inStatementStates()
				.sequence(EnumSet.of(IF, ELSIF))
				.createNode(EShallowEntityType.STATEMENT, 0).skipTo(THEN)
				.parseUntil(EIec61131ParserStates.IN_METHOD);
		ifAlternative.sequenceBefore(EnumSet.of(ELSE, ELSIF))
				.endNodeWithContinuation();
		ifAlternative.sequence(END_IF).optional(SEMICOLON).endNode();

		// else is used in "if" (only use IN_METHOD here, as IN_CASE case is
		// handled elsewhere)
		RecognizerBase<EIec61131ParserStates> elseAlternative = inState(
				EIec61131ParserStates.IN_METHOD).sequence(ELSE)
				.createNode(EShallowEntityType.STATEMENT, 0)
				.parseUntil(EIec61131ParserStates.IN_METHOD);
		elseAlternative.sequenceBefore(END_CASE).endNode();
		elseAlternative.sequence(END_IF, SEMICOLON).endNode();
	}

	/** Create rules for parsing simple statements within methods. */
	private void createSimpleStatementRules() {
		// simple statement
		inStatementStates()
				.sequence(SIMPLE_STATEMENT_KEYWORDS)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0).skipTo(SEMICOLON)
				.endNode();

		// empty statement
		inStatementStates()
				.sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT).endNode();
	}

	/** Creates the rules for the system var decl section of SV files. */
	private void createInSystemVarDeclRules() {
		// we parse these parts as attributes, as this makes it less likely to
		// interface with structuring metrics.
		inState(EIec61131ParserStates.IN_SYSTEM_VAR_DECL)
				.sequence(IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.IEC_SV_VARIABLE, 0)
				.skipBefore(EnumSet.of(SEMICOLON, ELEMENT)).optional(SEMICOLON)
				.endNode();
		inState(EIec61131ParserStates.IN_SYSTEM_VAR_DECL)
				.sequence(ELEMENT)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.IEC_SV_ELEMENT)
				.skipBefore(EnumSet.of(SEMICOLON, ELEMENT)).optional(SEMICOLON)
				.endNode();
	}

}
