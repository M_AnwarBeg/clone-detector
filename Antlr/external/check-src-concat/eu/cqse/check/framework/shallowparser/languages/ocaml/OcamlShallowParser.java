/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.ocaml;

import static eu.cqse.check.framework.scanner.ETokenType.AND;
import static eu.cqse.check.framework.scanner.ETokenType.BEGIN;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLONGREATER;
import static eu.cqse.check.framework.scanner.ETokenType.DO;
import static eu.cqse.check.framework.scanner.ETokenType.DONE;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.END;
import static eu.cqse.check.framework.scanner.ETokenType.EOF;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.EXCEPTION;
import static eu.cqse.check.framework.scanner.ETokenType.EXCLAMATION;
import static eu.cqse.check.framework.scanner.ETokenType.EXTERNAL;
import static eu.cqse.check.framework.scanner.ETokenType.FALSE;
import static eu.cqse.check.framework.scanner.ETokenType.FLOATING_POINT_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FUN;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IN;
import static eu.cqse.check.framework.scanner.ETokenType.INCLUDE;
import static eu.cqse.check.framework.scanner.ETokenType.INHERIT;
import static eu.cqse.check.framework.scanner.ETokenType.INTEGER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LET;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MATCH;
import static eu.cqse.check.framework.scanner.ETokenType.METHOD;
import static eu.cqse.check.framework.scanner.ETokenType.MINUS;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSGREATER;
import static eu.cqse.check.framework.scanner.ETokenType.MODULE;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.OBJECT;
import static eu.cqse.check.framework.scanner.ETokenType.OPEN;
import static eu.cqse.check.framework.scanner.ETokenType.OR;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.REC;
import static eu.cqse.check.framework.scanner.ETokenType.REF;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SHARP;
import static eu.cqse.check.framework.scanner.ETokenType.STRUCT;
import static eu.cqse.check.framework.scanner.ETokenType.THEN;
import static eu.cqse.check.framework.scanner.ETokenType.TO;
import static eu.cqse.check.framework.scanner.ETokenType.TRUE;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.TYPE;
import static eu.cqse.check.framework.scanner.ETokenType.UNDERSCORE;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WITH;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_EXPRESSION;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.TOP_LEVEL;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * 
 * @author $Author: popeea $
 * @version $Rev: 55035 $
 * @ConQAT.Rating RED Hash:
 */

/** A shallow parser for OCaml. */
public class OcamlShallowParser
		extends ShallowParserBase<EGenericParserStates> {

	/** Constructor. */
	public OcamlShallowParser() {
		super(EGenericParserStates.class, TOP_LEVEL);
		createDefinitionRules();
		createTypeRules();
		createClassElementsRules();
		createNestingExpressionRules();
		createNonNestingExpressionRules();
		createSimplifiedRecExpressionRules();
		// Create an incomplete node if something is still unrecognized.
		inAnyState().markStart().sequence(EnumSet.allOf(ETokenType.class))
				.createNode(EShallowEntityType.META, 0, null);
	}

	/** Definition rules. */
	private void createDefinitionRules() {
		inState(TOP_LEVEL).sequence(OPEN)
				.createNode(EShallowEntityType.META, "open", null)
				.sequence(IDENTIFIER).repeated(DOT, IDENTIFIER)
				.endNode();
		inState(TOP_LEVEL).sequence(MODULE).markStart().sequence(IDENTIFIER)
				.skipTo(EQ).sequence(STRUCT)
				.createNode(EShallowEntityType.META, "module-def", 0)
				.skipTo(END).endNode();
		inState(TOP_LEVEL).sequence(MODULE).markStart().sequence(IDENTIFIER)
				.skipTo(EQ).sequence(IDENTIFIER)
				.createNode(EShallowEntityType.META, "module-def", 0)
				.repeated(DOT, IDENTIFIER).endNode();
		inState(TOP_LEVEL).sequence(EXCEPTION, IDENTIFIER);
		inState(TOP_LEVEL).sequence(LET).optional(REC).markStart()
				.sequence(IDENTIFIER)
				.createNode(EShallowEntityType.METHOD, "let-decl", 0).skipTo(EQ)
				.parseOnce(IN_EXPRESSION).endNode();
	}

	/** Class definition rule */
	private void createTypeRules() {
		inState(TOP_LEVEL).sequence(CLASS).markStart()
				.createNode(EShallowEntityType.TYPE, SubTypeNames.CLASS, 0)
				.sequence(IDENTIFIER).skipTo(EQ)
				.sequence(OBJECT).optional(LPAREN, IDENTIFIER, RPAREN)
				.parseUntil(IN_TYPE).sequence(END)
				.endNode();
	}

	/** Create rules for class elements, i.e., methods. */
	private void createClassElementsRules() {
		inState(IN_TYPE).sequence(METHOD).optional(PRIVATE).markStart()
				.sequence(IDENTIFIER) // method name
				.createNode(EShallowEntityType.METHOD, SubTypeNames.METHOD, 0)
				.skipTo(EQ)
				.parseOnce(IN_EXPRESSION) // method body
				.endNode();
		inState(IN_TYPE).sequence(INHERIT, IDENTIFIER).repeated(DOT,
				IDENTIFIER);
	}

	/** Rules to handle nesting expressions, i.e., if, while, for, match, .. */
	private void createNestingExpressionRules() {

		inState(IN_EXPRESSION).sequence(IF) // 4a: if with else branch
				.createNode(EShallowEntityType.STATEMENT, SubTypeNames.IF)
				.parseOnce(IN_EXPRESSION).sequence(THEN)
				.parseOnce(IN_EXPRESSION).sequence(ELSE)
				.parseOnce(IN_EXPRESSION).endNode();

		inState(IN_EXPRESSION).sequence(IF) // 4b: if without else branch
				.createNode(EShallowEntityType.STATEMENT, SubTypeNames.IF)
				.parseOnce(IN_EXPRESSION).sequence(THEN)
				.parseOnce(IN_EXPRESSION).endNode();

		inState(IN_EXPRESSION).sequence(WHILE) // 4c
				.createNode(EShallowEntityType.STATEMENT, SubTypeNames.WHILE)
				.parseOnce(IN_EXPRESSION).sequence(DO).parseOnce(IN_EXPRESSION)
				.sequence(DONE).endNode();

		inState(IN_EXPRESSION).sequence(FOR, IDENTIFIER, EQ) // 4d
				.createNode(EShallowEntityType.STATEMENT, SubTypeNames.FOR)
				.parseOnce(IN_EXPRESSION).sequence(TO).parseOnce(IN_EXPRESSION)
				.sequence(DO).parseOnce(IN_EXPRESSION).sequence(DONE).endNode();

		// rule 4e: match expression
		RecognizerBase<EGenericParserStates> recognizedMatch = inState(IN_EXPRESSION)
				.sequence(MATCH)
				.createNode(EShallowEntityType.STATEMENT, "match-expr")
				.parseOnce(IN_EXPRESSION).sequence(WITH);
		patternMatchingRecognizer(recognizedMatch).endNode();

		RecognizerBase<EGenericParserStates> recognizedFunction = // 4f
		inState(IN_EXPRESSION).sequence(FUNCTION)
				.createNode(EShallowEntityType.STATEMENT, "function-expr");
		patternMatchingRecognizer(recognizedFunction).endNode();

		inState(IN_EXPRESSION).sequence(FUN) // 4g
				.createNode(EShallowEntityType.STATEMENT, "fun-expr")
				.skipTo(MINUSGREATER).parseOnce(IN_EXPRESSION).endNode();

		patternMatchingRecognizer(inState(IN_EXPRESSION).sequence(TRY) // 4h
				.parseOnce(IN_EXPRESSION).sequence(WITH)).endNode();

		inState(IN_EXPRESSION).sequence(OBJECT) // rule 4i
				.createNode(EShallowEntityType.STATEMENT, "object-expr")
				.parseUntil(IN_TYPE).sequence(END).endNode();
	}

	/** Rules to handle non-nesting expressions */
	private void createNonNestingExpressionRules() {

		inState(IN_EXPRESSION).sequence(BEGIN).parseOnce(IN_EXPRESSION) // 4v
				.sequence(END);

		inState(IN_EXPRESSION).sequence(LBRACK) // rule 4n: list expressions
				.createNode(EShallowEntityType.STATEMENT, "list-expr")
				.skipToWithNesting(RBRACK, LBRACK, RBRACK).endNode();
		
		inState(IN_EXPRESSION).sequence(LET).optional(REC).markStart() // 4q
				.sequence(IDENTIFIER)
				.createNode(EShallowEntityType.STATEMENT, "let-expr", 0)
				.skipTo(EQ).parseOnce(IN_EXPRESSION).sequence(IN)
				.parseOnce(IN_EXPRESSION).endNode();

		inState(IN_EXPRESSION).sequence(NEW).markStart()
				.sequence(IDENTIFIER).repeated(DOT, IDENTIFIER)
				.createNode(EShallowEntityType.STATEMENT, "new-expr", 0)
				.endNode();

	}

	/**
	 * Simplified expression rules. (TODO: general rules would be
	 * left-recursive.)
	 */
	private void createSimplifiedRecExpressionRules() {

		inState(IN_EXPRESSION).sequence(IDENTIFIER) // 4s
				.sequence(SHARP, IDENTIFIER)
				.createNode(EShallowEntityType.STATEMENT, "method-call-expr", 0)
				.sequence(IDENTIFIER).endNode();

		inState(IN_EXPRESSION).sequence(getUnaryConstructors()) // rule 4u
				.parseOnce(IN_EXPRESSION);

		inState(IN_EXPRESSION).sequence(IDENTIFIER) // rule 4j
				.repeated(DOT, IDENTIFIER)
				.createNode(EShallowEntityType.STATEMENT, "id-expr",
						new Region(0, -1))
				.endNode();

		inState(IN_EXPRESSION).sequence(getConstantTokens()) // rule 4k
				.createNode(EShallowEntityType.STATEMENT, "const-expr", 0)
				.endNode();

		inState(IN_EXPRESSION).markStart().sequence(LPAREN, RPAREN) // rule 4k
				.createNode(EShallowEntityType.STATEMENT, "const-expr",
						new Region(0, -1))
				.endNode();

		inState(IN_EXPRESSION).sequence(LPAREN).parseOnce(IN_EXPRESSION) // 4l
				.sequence(RPAREN);

		inState(IN_EXPRESSION).sequence(LPAREN).parseOnce(IN_EXPRESSION) // 4t
				.sequence(COLONGREATER, IDENTIFIER).repeated(DOT, IDENTIFIER)
				.sequence(RPAREN);

	}

	/** Recognizes a pattern-matching construct */
	private static RecognizerBase<EGenericParserStates> patternMatchingRecognizer(
			RecognizerBase<EGenericParserStates> currentState) {
		return currentState.skipTo(MINUSGREATER).parseOnce(IN_EXPRESSION);
	}

	/**
	 * Returns a set of tokens representing constant values, except () and [].
	 */
	private static EnumSet<ETokenType> getConstantTokens() {
		return EnumSet.of(INTEGER_LITERAL, FLOATING_POINT_LITERAL, IDENTIFIER,
				FALSE, TRUE);
	}

	/** Returns a set of unary constructors */
	private static EnumSet<ETokenType> getUnaryConstructors() {
		return EnumSet.of(REF, EXCLAMATION, MINUS);
	}

	/** Redundant method, collecting all token names. Remove it. */
	private void bull() {
		inAnyState().skipBefore(AND, CLASS, COLONGREATER, DO, DONE, END,
				EXCEPTION, ELSE, EOF, EXCLAMATION, EXTERNAL, FOR, FUN, FUNCTION,
				IN, INCLUDE, INHERIT, LBRACK, LPAREN, LET, MATCH, MINUS,
				MINUSGREATER, MODULE, NEW, OBJECT, OPEN, OR, PRIVATE,
				RBRACK, REC, REF, RPAREN, STRUCT, TO, TYPE, UNDERSCORE,
				WITH, WHILE);
	}
}
