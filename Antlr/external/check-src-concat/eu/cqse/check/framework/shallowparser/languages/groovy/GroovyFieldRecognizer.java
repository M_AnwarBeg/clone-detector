/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.groovy;

import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;

/**
 * Recognizer that helps matching fields in Groovy. An extra recognizer is
 * necessary because we have to check for a new line to begin.
 * 
 * @author $Author: baumeister $
 * @version $Rev: 53318 $
 * @ConQAT.Rating YELLOW Hash: 083DD3162D9F41C5B41C9DCEAB2DF3F8
 */
/* package */ class GroovyFieldRecognizer
		extends RecognizerBase<EGroovyShallowParserStates> {

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(
			ParserState<EGroovyShallowParserStates> parserState,
			List<IToken> tokens, int startOffset) {

		if (startOffset < 1) {
			return NO_MATCH;
		}

		IToken token = tokens.get(startOffset);
		ETokenType tokenType = token.getType();

		IToken lastToken = tokens.get(startOffset - 1);

		EnumSet<ETokenType> allowedTypes = EnumSet.of(EQ, COMMA, SEMICOLON);

		if (allowedTypes.contains(tokenType)
				|| lastToken.getLineNumber() < token.getLineNumber()) {
			return ++startOffset;
		}

		return NO_MATCH;

	}
}
