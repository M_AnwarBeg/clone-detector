/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.javascript;

import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CATCH;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONST;
import static eu.cqse.check.framework.scanner.ETokenType.CONSTRUCTOR;
import static eu.cqse.check.framework.scanner.ETokenType.DECLARE;
import static eu.cqse.check.framework.scanner.ETokenType.DEFAULT;
import static eu.cqse.check.framework.scanner.ETokenType.DO;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE_ARROW;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ENUM;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.EXPORT;
import static eu.cqse.check.framework.scanner.ETokenType.EXTENDS;
import static eu.cqse.check.framework.scanner.ETokenType.FINALLY;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIERS;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPLEMENTS;
import static eu.cqse.check.framework.scanner.ETokenType.IMPORT;
import static eu.cqse.check.framework.scanner.ETokenType.INSTANCEOF;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LITERALS;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MODULE;
import static eu.cqse.check.framework.scanner.ETokenType.NAMESPACE;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PROTOTYPE;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.QUESTION;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.STRING_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.TYPE;
import static eu.cqse.check.framework.scanner.ETokenType.VAR;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WITH;
import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.ANY;
import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.BASE_JS_EXTEND;
import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.CLOSURE_CONSTRUCTOR;
import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.IN_LAMBDA;
import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates.TOP_LEVEL;

import java.util.EnumSet;
import java.util.HashSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.AllLowercaseIdentifierMatcher;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ExactIdentifierMatcher;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.framework.UpperCaseStartIdentifierMatcher;
import eu.cqse.check.framework.shallowparser.languages.javascript.JavaScriptShallowParser.EJavaScriptParserStates;

/**
 * Shallow parser for JavaScript. The parser also supports various newer
 * language features from EcmaScript as well as TypeScript. Additionally,
 * library specific constructs from multiple libraries (e.g., Google Closure,
 * Base.js, AngularJS) are handled explicitly.
 *
 * @author $Author: heinemann $
 * @version $Rev: 56881 $
 * @ConQAT.Rating RED Hash: 5111E736B465993C219CCD01C55DB458
 */
public class JavaScriptShallowParser
		extends ShallowParserBase<EJavaScriptParserStates> {

	/** The states used in this parser. */
	public static enum EJavaScriptParserStates {

		/** Top-level parsing state. */
		TOP_LEVEL,

		/** Generic state, as many constructs can occur at any place. */
		ANY,

		/** Parsing within a type, such as a TypeScript class. */
		IN_TYPE,

		/** Parses a lambda (arrow notation) construct. */
		IN_LAMBDA,

		/**
		 * Special state that is only valid in the constructor of a google
		 * closure type
		 */
		CLOSURE_CONSTRUCTOR,

		/** Special state used inside of the base.js extend method */
		BASE_JS_EXTEND;
	}

	/**
	 * All possible identifiers, including new keywords, which can be used as
	 * identifiers in older JavaScript files.
	 */
	private static final EnumSet<ETokenType> ALL_IDENTIFIERS = EnumSet.of(
			IDENTIFIER, CLASS, IMPLEMENTS, DECLARE, ENUM, EXPORT, EXTENDS,
			IMPLEMENTS, IMPORT, INSTANCEOF, INTERFACE, MODULE, NAMESPACE,
			PRIVATE, PROTECTED, PUBLIC, STATIC, TYPE);

	/** Valid modifiers in TypeScript. */
	private static final EnumSet<ETokenType> TYPESCRIPT_MODIFIERS = EnumSet
			.of(STATIC, PUBLIC, PRIVATE, PROTECTED);

	/** Constructor. */
	public JavaScriptShallowParser() {
		super(EJavaScriptParserStates.class, EJavaScriptParserStates.TOP_LEVEL);

		createLambdaRules();
		createMetaRules();
		createAngularRules();
		createModuleRules();
		createBaseJSRules();
		createClosureRecognizers();
		createTypeRules();
		createTypeMemberRules();
		createFunctionRules();
		createStatementRules();
	}

	/** Creates the rules for handling lanbda expressions (arrow notation). */
	private void createLambdaRules() {
		completeLambda(inState(IN_LAMBDA, ANY).sequence(IDENTIFIER));
		completeLambda(inState(IN_LAMBDA, ANY).sequence(LPAREN).skipTo(RPAREN));

		// additional rule for parsing lambda expressions (without braces). see
		// completeLambda() for details
		// the node start is moved one token to the right so the shallow
		// entities produced by this rule don't include the double arrow
		// (instead it will be included in the parent entity)
		inState(IN_LAMBDA).sequence(DOUBLE_ARROW)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.LAMBDA_EXPRESSION, null, 1)
				.skipBeforeWithNesting(
						EnumSet.of(RPAREN, SEMICOLON, RBRACE, RBRACK, COMMA),
						LPAREN, RPAREN)
				.endNode();
	}

	/** Completes a rule for parsing lambda expressions. */
	private static void completeLambda(
			RecognizerBase<EJavaScriptParserStates> ruleStart) {
		RecognizerBase<EJavaScriptParserStates> lambdaAlternative = ruleStart
				.sequenceBefore(DOUBLE_ARROW)
				.createNode(EShallowEntityType.METHOD, "lambda");
		lambdaAlternative.sequence(DOUBLE_ARROW, LBRACE).parseUntil(ANY)
				.sequence(RBRACE).endNode();

		// we start parsing before the double arrow (=>) as this allows our
		// special statement rule to capture this case. This is required, as
		// this kind of expression is not terminated by a semicolon.
		lambdaAlternative.parseOnce(IN_LAMBDA).endNode();
	}

	/** Creates parsing rules for meta elements. */
	private void createMetaRules() {
		// Closure special commands (require, provide, inherits)
		inState(TOP_LEVEL)
				.sequence(identifiers("goog"), DOT,
						identifiers("provide", "require", "inherits"), LPAREN)
				.skipTo(RPAREN).createNode(EShallowEntityType.META,
						new Region(0, 2), new Region(4, -2))
				.optional(SEMICOLON).endNode();

		// Support for namespace.js
		inState(TOP_LEVEL).sequence(NAMESPACE, LPAREN)
				.createNode(EShallowEntityType.META, SubTypeNames.NAMESPACE, 2)
				.skipTo(RPAREN).optional(SEMICOLON).endNode();
	}

	/** Returns a matcher for exact identifiers of given names. */
	private static ExactIdentifierMatcher identifiers(String... names) {
		return new ExactIdentifierMatcher(names);
	}

	/**
	 * Creates type rules specific to the Base.JS Framework
	 * http://dean.edwards.name/weblog/2006/03/base/
	 */
	private void createBaseJSRules() {
		RecognizerBase<EJavaScriptParserStates> baseJSTypeRecognizer = inState(
				TOP_LEVEL).repeated(ALL_IDENTIFIERS, DOT).markStart()
						.sequence(new UpperCaseStartIdentifierMatcher(), EQ)
						.repeated(new AllLowercaseIdentifierMatcher(), DOT)
						.sequence(new UpperCaseStartIdentifierMatcher(), DOT,
								new ExactIdentifierMatcher("extend"), LPAREN,
								LBRACE)
						.createNode(EShallowEntityType.TYPE, SubTypeNames.CLASS,
								0)
						.parseUntil(BASE_JS_EXTEND).sequence(RBRACE);

		baseJSTypeRecognizer.sequence(RPAREN).optional(SEMICOLON).endNode();

		baseJSTypeRecognizer.sequence(COMMA, LBRACE).parseUntil(BASE_JS_EXTEND)
				.sequence(RBRACE, RPAREN).optional(SEMICOLON).endNode();
	}

	/** Creates recognizers for the google closure framework */
	private void createClosureRecognizers() {
		// Google closure constructor, displayed as method here, as that is what
		// it technically is
		inState(TOP_LEVEL).repeated(ALL_IDENTIFIERS, DOT).markStart()
				.sequence(new UpperCaseStartIdentifierMatcher(), EQ, FUNCTION)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.CONSTRUCTOR,
						new Region(0, -3))
				.skipTo(LBRACE).parseUntil(CLOSURE_CONSTRUCTOR).sequence(RBRACE)
				.optional(SEMICOLON).endNode();

		RecognizerBase<EJavaScriptParserStates> attributeBeginning = inState(
				CLOSURE_CONSTRUCTOR)
						.sequence(ETokenType.THIS, DOT, ALL_IDENTIFIERS)
						.createNode(EShallowEntityType.ATTRIBUTE,
								SubTypeNames.ATTRIBUTE, new Region(0, 2));
		attributeBeginning.sequence(SEMICOLON).endNode();

		RecognizerBase<EJavaScriptParserStates> attributeAssignment = attributeBeginning
				.sequence(EQ);
		attributeAssignment.sequenceBefore(LBRACE)
				.skipToWithNesting(SEMICOLON, LBRACE, RBRACE).endNode();

		attributeAssignment.skipToWithNesting(SEMICOLON, LPAREN, RPAREN)
				.endNode();

	}

	/** Creates parsing rules specifically for AngularJS. */
	private void createAngularRules() {
		// angular modules
		inState(TOP_LEVEL).optional(VAR, ALL_IDENTIFIERS, EQ)
				.sequence(identifiers("angular"), DOT, MODULE, LPAREN)
				.createNode(EShallowEntityType.META,
						"AngularJS module declaration")
				.skipToWithNesting(RPAREN, LPAREN, ETokenType.RPAREN)
				.optional(SEMICOLON).endNode();

		// angular style for unnamed and named functions
		inState(TOP_LEVEL).optional(ALL_IDENTIFIERS).markStart()
				.sequence(DOT, identifiers("config"), LPAREN, FUNCTION)
				.createNode(EShallowEntityType.METHOD,
						new Object[] { "AngularJS", 1 })
				.skipTo(LBRACE).parseUntil(ANY).sequence(RBRACE, RPAREN)
				.optional(SEMICOLON).endNode();
		inState(TOP_LEVEL).optional(ALL_IDENTIFIERS).markStart()
				.sequence(DOT,
						identifiers("controller", "filter", "factory",
								"directive", "constant"),
						LPAREN, STRING_LITERAL, COMMA, FUNCTION)
				.createNode(EShallowEntityType.METHOD,
						new Object[] { "AngularJS", 1 }, 3)
				.skipTo(LBRACE).parseUntil(ANY).sequence(RBRACE, RPAREN)
				.optional(SEMICOLON).endNode();
	}

	/**
	 * Creates rules for parsing namespace/module constructs. These are used to
	 * handle those constructs provided by well-known libraries, as JavaScript
	 * itself does not provide a direct namespace concept.
	 */
	private void createModuleRules() {
		// YUI library, see http://yuilibrary.com/yui/docs/yui/
		completeModuleRecognizer(inAnyState().ensureTopLevel()
				.subRecognizer(new YuiCallRecognizer("add"), 1, 1)
				.skipTo(STRING_LITERAL)
				.createNode(EShallowEntityType.MODULE, "YUI module", -1));
		completeModuleRecognizer(inAnyState().ensureTopLevel()
				.subRecognizer(new YuiCallRecognizer("use"), 1, 1)
				.createNode(EShallowEntityType.MODULE, "YUI code", -1));

		// TypeScript namespace
		inState(TOP_LEVEL).repeated(EnumSet.of(DECLARE, EXPORT)).markStart()
				.sequence(EnumSet.of(NAMESPACE, MODULE)).sequence(IDENTIFIER)
				.repeated(DOT, IDENTIFIER).sequence(LBRACE)
				.createNode(EShallowEntityType.MODULE, 0, new Region(1, -2))
				.parseUntil(TOP_LEVEL).sequence(RBRACE).endNode();
	}

	/** Completes a recognizer for modules. */
	private static void completeModuleRecognizer(
			RecognizerBase<EJavaScriptParserStates> recognizer) {
		recognizer.skipTo(FUNCTION).skipTo(LBRACE).parseUntil(ANY)
				.sequence(RBRACE).skipToWithNesting(RPAREN, LPAREN, RPAREN)
				.optional(SEMICOLON).endNode();
	}

	/** Creates rules for TypeScript types. */
	private void createTypeRules() {
		inAnyState().repeated(EnumSet.of(DECLARE, EXPORT)).markStart()
				.sequence(EnumSet.of(CLASS, INTERFACE), IDENTIFIER)
				.createNode(EShallowEntityType.TYPE, 0, 1).skipTo(LBRACE)
				.parseUntil(IN_TYPE).sequence(RBRACE).endNode();

		// enums
		inAnyState().repeated(EnumSet.of(DECLARE, EXPORT, CONST)).markStart()
				.sequence(ENUM, IDENTIFIER, LBRACE)
				.createNode(EShallowEntityType.TYPE, 0, 1)
				.skipToWithNesting(RBRACE, ETokenType.LBRACE, RBRACE).endNode();
	}

	/** Creates rules for TypeScript type members. */
	private void createTypeMemberRules() {
		// constructor
		inState(IN_TYPE).repeated(TYPESCRIPT_MODIFIERS).markStart()
				.sequence(CONSTRUCTOR)
				.createNode(EShallowEntityType.METHOD, SubTypeNames.CONSTRUCTOR)
				.skipNested(ETokenType.LPAREN, RPAREN).sequence(LBRACE)
				.parseUntil(ANY).sequence(RBRACE).endNode();

		// "normal" methods
		RecognizerBase<EJavaScriptParserStates> methodAlternative = inState(
				IN_TYPE).repeated(TYPESCRIPT_MODIFIERS).markStart()
						.sequence(IDENTIFIER, LPAREN).skipTo(RPAREN)
						.skipBefore(EnumSet.of(LBRACE, SEMICOLON));
		methodAlternative.sequence(SEMICOLON)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.METHOD_DECLARATION, 0)
				.endNode();
		methodAlternative.sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD, "method", 0)
				.parseUntil(ANY).sequence(RBRACE).endNode();

		// attributes
		inState(IN_TYPE).repeated(TYPESCRIPT_MODIFIERS).markStart()
				.sequence(IDENTIFIER, EnumSet.of(EQ, COLON, QUESTION))
				.createNode(EShallowEntityType.ATTRIBUTE, "attribute", 0)
				.skipToWithNesting(SEMICOLON, LBRACE, RBRACE).endNode();
	}

	/** Creates parsing rules for functions. */
	private void createFunctionRules() {

		// TODO (LH) Extract methods for sub-cases?

		// assigned function
		inAnyState().repeated(EnumSet.of(DECLARE, EXPORT)).optional(VAR)
				.markStart().sequence(ALL_IDENTIFIERS)
				.repeated(DOT, extendAllIdentifiers(PROTOTYPE))
				.sequence(EQ, FUNCTION)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.ASSIGNED_FUNCTION, new Region(0, -3))
				.skipNested(LPAREN, RPAREN).skipTo(LBRACE).parseUntil(ANY)
				.sequence(RBRACE).optional(SEMICOLON).endNode();

		// named function
		RecognizerBase<EJavaScriptParserStates> namedFunctionAlternative = inAnyState()
				.repeated(EnumSet.of(DECLARE, EXPORT)).markStart()
				.sequence(FUNCTION, ALL_IDENTIFIERS).skipNested(LPAREN, RPAREN)
				.skipBefore(EnumSet.of(LBRACE, SEMICOLON));
		namedFunctionAlternative.sequence(SEMICOLON)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.FUNCTION_DECLARATION, 1)
				.endNode();
		namedFunctionAlternative.sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.NAMED_FUNCTION, 1)
				.parseUntil(ANY).sequence(RBRACE).endNode();

		// attribute function
		inAnyState()
				.sequence(extendAllIdentifiers(STRING_LITERAL), COLON, FUNCTION)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.ATTRIBUTE_FUNCTION, 0)
				.skipNested(LPAREN, RPAREN).skipTo(LBRACE).parseUntil(ANY)
				.sequence(RBRACE).optional(COMMA).endNode();

		// anonymous function
		inAnyState().sequence(FUNCTION)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.ANONYMOUS_FUNCTION)
				.skipNested(LPAREN, RPAREN).skipTo(LBRACE).parseUntil(ANY)
				.sequence(RBRACE).endNode();
	}

	/**
	 * Returns a copy of {@link #ALL_IDENTIFIERS} extended by the given type.
	 */
	private static EnumSet<ETokenType> extendAllIdentifiers(
			ETokenType extension) {
		EnumSet<ETokenType> result = EnumSet.copyOf(ALL_IDENTIFIERS);
		result.add(extension);
		return result;
	}

	/** Creates parsing rules for statements. */
	private void createStatementRules() {

		// TODO (LH) Extract methods for sub-cases?

		// empty statement
		inAnyState().sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT)
				.endNode();

		// Attributes
		RecognizerBase<EJavaScriptParserStates> baseJsAttribute = inState(
				BASE_JS_EXTEND).sequence(ALL_IDENTIFIERS, COLON).createNode(
						EShallowEntityType.ATTRIBUTE, SubTypeNames.ATTRIBUTE,
						0);
		baseJsAttribute.skipToWithNesting(COMMA, LBRACE, RBRACE).endNode();
		// Might be the last one in the constructor
		baseJsAttribute.skipBeforeWithNesting(RBRACE, LBRACE, RBRACE).endNode();

		// filter out labels as meta as they do not increase statement count
		inAnyState().sequence(ALL_IDENTIFIERS, COLON)
				.createNode(EShallowEntityType.META, SubTypeNames.LABEL, 0)
				.endNode();

		// else if
		RecognizerBase<EJavaScriptParserStates> elseIfAlternative = inAnyState()
				.sequence(ELSE, IF)
				.skipNested(LPAREN, RPAREN, new JavaScriptFunctionRecognizer())
				.createNode(EShallowEntityType.STATEMENT, new int[] { 0, 1 });
		endWithPossibleContinuation(elseIfAlternative.sequence(LBRACE)
				.parseUntil(ANY).sequence(RBRACE), EnumSet.of(ELSE));
		endWithPossibleContinuation(elseIfAlternative.parseOnce(ANY),
				EnumSet.of(ELSE));

		// simple block constructs
		createBlockRuleWithContinuation(EnumSet.of(WHILE, FOR, SWITCH, WITH),
				null, true);
		createBlockRuleWithContinuation(EnumSet.of(ELSE, FINALLY), null, false);
		createBlockRuleWithContinuation(EnumSet.of(IF), EnumSet.of(ELSE), true);
		createBlockRuleWithContinuation(EnumSet.of(TRY, CATCH),
				EnumSet.of(CATCH, FINALLY), true);

		createSwitchCaseRules();
		createDoWhileRules();

		// anonymous block
		inAnyState().sequence(LBRACE)
				.createNode(EShallowEntityType.STATEMENT, "anonymous block")
				.parseUntil(ANY).sequence(RBRACE).endNode();

		// simple statement
		inAnyState()
				.subRecognizer(new JavaScriptSimpleStatementRecognizer(), 1, 1)
				.endNode();
	}

	/** Creates rules for do/while. */
	private void createDoWhileRules() {
		RecognizerBase<EJavaScriptParserStates> doWhileAlternative = inState(
				ANY).sequence(DO).createNode(EShallowEntityType.STATEMENT, 0);
		doWhileAlternative.sequence(LBRACE).parseUntil(ANY)
				.sequence(RBRACE, WHILE)
				.skipNested(LPAREN, RPAREN, new JavaScriptFunctionRecognizer())
				.endNode();
		doWhileAlternative.parseOnce(ANY).sequence(WHILE)
				.skipNested(LPAREN, RPAREN, new JavaScriptFunctionRecognizer())
				.endNode();
	}

	/** Creates rules for switch/case. */
	private void createSwitchCaseRules() {
		HashSet<ETokenType> literalsAndIdentifiers = new HashSet<ETokenType>(
				LITERALS);
		literalsAndIdentifiers.addAll(IDENTIFIERS);
		inAnyState().sequence(CASE, literalsAndIdentifiers, COLON)
				.createNode(EShallowEntityType.META, new int[] { 0, 1 })
				.endNode();
		inAnyState().sequence(CASE).skipToWithNesting(COLON, LPAREN, RPAREN)
				.createNode(EShallowEntityType.META, 0).endNode();

		inAnyState().sequence(DEFAULT, COLON)
				.createNode(EShallowEntityType.META, 0).endNode();
	}

	/**
	 * Creates a rule for recognizing a statement starting with a single
	 * keyword, optionally followed by an expression in parentheses, and
	 * followed by a block or a single statement.
	 *
	 * @param continuationTokens
	 *            list of tokens that indicate a continued statement if
	 *            encountered after the block. May be null.
	 */
	private void createBlockRuleWithContinuation(
			EnumSet<ETokenType> startTokens,
			EnumSet<ETokenType> continuationTokens,
			boolean canBeFollowedByParentheses) {
		RecognizerBase<EJavaScriptParserStates> alternative = inAnyState()
				.sequence(startTokens)
				.createNode(EShallowEntityType.STATEMENT, 0);
		if (canBeFollowedByParentheses) {
			alternative = alternative.skipNested(LPAREN, RPAREN,
					new JavaScriptFunctionRecognizer());
		}

		endWithPossibleContinuation(
				alternative.sequence(LBRACE).parseUntil(ANY).sequence(RBRACE),
				continuationTokens);
		endWithPossibleContinuation(alternative.parseOnce(ANY),
				continuationTokens);
	}
}
