/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.ALIGNAS;
import static eu.cqse.check.framework.scanner.ETokenType.AND;
import static eu.cqse.check.framework.scanner.ETokenType.ANDAND;
import static eu.cqse.check.framework.scanner.ETokenType.ASSERT;
import static eu.cqse.check.framework.scanner.ETokenType.AUTO;
import static eu.cqse.check.framework.scanner.ETokenType.BOOL;
import static eu.cqse.check.framework.scanner.ETokenType.BREAK;
import static eu.cqse.check.framework.scanner.ETokenType.BYTE;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CHAR;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.CONST;
import static eu.cqse.check.framework.scanner.ETokenType.CONSTEXPR;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.DECLTYPE;
import static eu.cqse.check.framework.scanner.ETokenType.DELETE;
import static eu.cqse.check.framework.scanner.ETokenType.DOUBLE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ENUM;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.EXTERN;
import static eu.cqse.check.framework.scanner.ETokenType.FAR;
import static eu.cqse.check.framework.scanner.ETokenType.FINAL;
import static eu.cqse.check.framework.scanner.ETokenType.FLOAT;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.GOTO;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.INT;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LONG;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSMINUS;
import static eu.cqse.check.framework.scanner.ETokenType.MULT;
import static eu.cqse.check.framework.scanner.ETokenType.MUTABLE;
import static eu.cqse.check.framework.scanner.ETokenType.NAMESPACE;
import static eu.cqse.check.framework.scanner.ETokenType.NEAR;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.NOEXCEPT;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSPLUS;
import static eu.cqse.check.framework.scanner.ETokenType.POINTERTO;
import static eu.cqse.check.framework.scanner.ETokenType.PREPROCESSOR_DIRECTIVE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SCOPE;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.SHORT;
import static eu.cqse.check.framework.scanner.ETokenType.SIGNED;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.STRUCT;
import static eu.cqse.check.framework.scanner.ETokenType.SUPER;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.THIS;
import static eu.cqse.check.framework.scanner.ETokenType.THROW;
import static eu.cqse.check.framework.scanner.ETokenType.TYPEDEF;
import static eu.cqse.check.framework.scanner.ETokenType.TYPENAME;
import static eu.cqse.check.framework.scanner.ETokenType.UNION;
import static eu.cqse.check.framework.scanner.ETokenType.UNSIGNED;
import static eu.cqse.check.framework.scanner.ETokenType.USING;
import static eu.cqse.check.framework.scanner.ETokenType.VIRTUAL;
import static eu.cqse.check.framework.scanner.ETokenType.VOID;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_EXPRESSION;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.TOP_LEVEL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.CStyleShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Shallow parser for C/C++14.
 * <p>
 * What this parser does and does not:
 * <ul>
 * <li>The parser recognizes types (classes, enums, interfaces), methods and
 * attributes, individual statements and lambdas.</li>
 * <li>It recognizes the nesting of statements (e.g. in loops), but does not
 * parse into the statements. For example, it recognizes an if-statement and
 * provides the list of sub-statements, but does not provide direct access to
 * the if-condition.</li>
 * <li>All preprocessor statements are parsed as meta information.</li>
 * <li>Template declarations are parsed as preceding meta information.</li>
 * <li>Forward declarations are handled as meta information.</li>
 * <li>We heuristically filter code generating macros, such as
 * "CREATE_STUFF(MyClass)".</li>
 * </ul>
 *
 * @author $Author: kupka $
 * @version $Rev: 56426 $
 * @ConQAT.Rating YELLOW Hash: 4F557FE5FAE84EAB27D32BA971849ED7
 */
public class CppShallowParser extends CStyleShallowParserBase {

	/** Keywords used for primitive types. */
	/* package */static final EnumSet<ETokenType> PRIMITIVE_TYPES = EnumSet.of(
			VOID, BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, SIGNED, UNSIGNED, CHAR,
			BOOL);

	/** Token types for types or identifiers. */
	/* package */static final EnumSet<ETokenType> TYPE_OR_IDENTIFIER = EnumSet
			.of(AUTO, DECLTYPE, IDENTIFIER);

	/** Token types that are used for type declarations. */
	/* package */static final EnumSet<ETokenType> TYPE_KEYWORDS = EnumSet
			.of(CLASS, STRUCT, UNION, ENUM);

	/** Modifiers that can be specified after a lambda. */
	/* package */static final EnumSet<ETokenType> LAMBDA_MODIFIERS = EnumSet
			.of(CONSTEXPR, MUTABLE);

	static {
		TYPE_OR_IDENTIFIER.addAll(PRIMITIVE_TYPES);
	}

	/**
	 * Set of common "keywords" that are not actually part of the language but
	 * rather used by certain compilers and implicitly defined using macros. The
	 * solution used here is to filter them out.
	 */
	private static final Set<String> PSEUDO_KEYWORDS = new HashSet<String>(
			Arrays.asList(
					// typically found in Windows compilers
					"__fastcall", "__export", "__forceinline", "_cdecl",
					"_stdcall", "__stdcall", "WINAPI", "APIENTRY", "CALLBACK",
					// used by the common Qt library
					"Q_EXPORT",
					// keywords found in ISA dialog manager
					"DML_c", "DM_CALLBACK", "__1", "__2", "__3", "__4", "__5",
					"__6", "__7", "DM_ENTRY", "DML_pascal", "DML_default",
					// project specific keywords
					"IGVPWORD_API"));

	/** Constructor. */
	public CppShallowParser() {
		createNamespaceRules();
	}

	/** {@inheritDoc} */
	@Override
	protected void createMetaRules() {
		new CppShallowParserMetaRules(this).contributeRules();
		super.createMetaRules();
	}

	/** Creates namespace specific rules. */
	private void createNamespaceRules() {
		// using
		inAnyState().sequence(USING).createNode(EShallowEntityType.META, 0)
				.skipTo(SEMICOLON).endNode();

		// namespace
		RecognizerBase<EGenericParserStates> namespaceAlternative = inAnyState()
				.sequence(NAMESPACE).skipBefore(EnumSet.of(SEMICOLON, LBRACE));
		namespaceAlternative.sequence(LBRACE)
				.createNode(EShallowEntityType.MODULE, 0, new Region(1, -2))
				.parseUntil(TOP_LEVEL).sequence(RBRACE).endNode();
		namespaceAlternative.sequence(SEMICOLON)
				.createNode(EShallowEntityType.META, 0, new Region(1, -2))
				.endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected void createTypeRules() {
		// typedef
		RecognizerBase<EGenericParserStates> typeInTypedefAlternative = inAnyState()
				.sequence(TYPEDEF).optional(CONST);
		typeInTypedefAlternative
				.sequenceBefore(getTypeKeywords(), IDENTIFIER, LBRACE)
				.createNode(EShallowEntityType.TYPE, 0).parseOnce(TOP_LEVEL)
				.skipTo(IDENTIFIER, SEMICOLON).endNodeWithName(-2);
		typeInTypedefAlternative.sequenceBefore(getTypeKeywords(), LBRACE)
				.createNode(EShallowEntityType.TYPE, 0).parseOnce(TOP_LEVEL)
				.skipTo(IDENTIFIER, SEMICOLON).endNodeWithName(-2);

		RecognizerBase<EGenericParserStates> simpleTypedefAlternative = inAnyState()
				.sequence(TYPEDEF).createNode(EShallowEntityType.TYPE, 0)
				.skipBefore(IDENTIFIER, EnumSet.of(SEMICOLON, RPAREN, LBRACK));
		simpleTypedefAlternative.markStart()
				.sequenceBefore(IDENTIFIER, EnumSet.of(SEMICOLON, LBRACK))
				.skipToWithNesting(SEMICOLON, LBRACK, RBRACK)
				.endNodeWithName(0);
		simpleTypedefAlternative.markStart().sequence(IDENTIFIER, RPAREN)
				.skipTo(SEMICOLON).endNodeWithName(0);

		createEnumRules();

		// types; we have to ensure when skipping to the LBRACE, that there is
		// no earlier SEMICOLON or EQ, as in these cases it is a forward
		// declaration or a variable.
		inAnyState().sequence(getTypeKeywords(), getValidIdentifiers())
				.skipBefore(EnumSet.of(SEMICOLON, LBRACE, EQ)).sequence(LBRACE)
				.createNode(EShallowEntityType.TYPE, 0, 1).parseUntil(IN_TYPE)
				.sequence(RBRACE).optional(SEMICOLON).endNode();

		// anonymous types
		inAnyState().sequence(getTypeKeywords(), LBRACE)
				.createNode(EShallowEntityType.TYPE, 0, "<anonymous>")
				.parseUntil(IN_TYPE).sequence(RBRACE).optional(SEMICOLON)
				.endNode();
	}

	/** Creates rules for parsing enums and enum classes. */
	private void createEnumRules() {
		// enum (both anonymous and named)
		finishEnumDeclaration(inAnyState().sequence(ENUM), SubTypeNames.ENUM,
				1);
		finishEnumDeclaration(inAnyState().sequence(ENUM, CLASS),
				SubTypeNames.ENUM_CLASS, 2);

		RecognizerBase<EGenericParserStates> enumLiteralAlternative = inState(
				IN_TYPE).sequence(IDENTIFIER);
		finishEnumLiteral(enumLiteralAlternative
				.sequenceBefore(EnumSet.of(COMMA, RBRACE)));
		finishEnumLiteral(enumLiteralAlternative.sequence(EQ)
				.skipBefore(EnumSet.of(COMMA, RBRACE)));
	}

	/** Finishes the rule for enum or enum class declarations. */
	private static void finishEnumDeclaration(
			RecognizerBase<EGenericParserStates> enumAlternative,
			String subType, Object name) {
		enumAlternative.sequence(LBRACE)
				.createNode(EShallowEntityType.TYPE, subType)
				.parseUntil(IN_TYPE).sequence(RBRACE).optional(SEMICOLON)
				.endNode();
		enumAlternative.sequence(IDENTIFIER).skipTo(LBRACE)
				.createNode(EShallowEntityType.TYPE, subType, name)
				.parseUntil(IN_TYPE).sequence(RBRACE).optional(SEMICOLON)
				.endNode();
	}

	/** Finishes the rule for a enum literal. */
	private static void finishEnumLiteral(
			RecognizerBase<EGenericParserStates> enumLiteralAlternative) {
		enumLiteralAlternative.createNode(EShallowEntityType.ATTRIBUTE,
				SubTypeNames.ENUM_LITERAL, 0).endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected EnumSet<ETokenType> getTypeKeywords() {
		return TYPE_KEYWORDS;
	}

	/** {@inheritDoc} */
	@Override
	protected void createClassElementsRules() {
		new CppShallowParserClassElementRules(this).contributeRules();
	}

			/**
			 * Creates a new recognizer that can match a scope prefix for a
			 * method-like construct. This includes sequences of identifiers
			 * with double colon, possibly intermixed with template arguments.
			 */
			/* package */RecognizerBase<EGenericParserStates> createScopeRecognizer() {
		// remember the start of the recognizer chain (we can not used the
		// result of the method chain, as this would be the last recognizer)
		RecognizerBase<EGenericParserStates> result = emptyRecognizer();
		result.sequence(IDENTIFIER)
				.subRecognizer(new CppSkipTemplateSpecificationRecognizer(), 0,
						1)
				.sequence(SCOPE);
		return result;
	}

	/** {@inheritDoc} */
	@Override
	protected void createCaseRule() {
		super.createCaseRule();

		// C/C++ also allows parentheses here and type casts (hence two sets of
		// nested parentheses).
		inState(IN_METHOD).markStart().sequence(CASE).skipTo(COLON)
				.createNode(EShallowEntityType.META, 0).endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected void createSimpleStatementRule() {

		EnumSet<ETokenType> separators = EnumSet.of(LBRACE, RBRACE);
		separators.addAll(ETokenType.KEYWORDS);

		inState(IN_METHOD).sequence(new UppercaseIdentifierMatcher())
				.skipNested(LPAREN, RPAREN).optional(PREPROCESSOR_DIRECTIVE)
				.sequenceBefore(separators)
				.createNode(EShallowEntityType.STATEMENT, "Expanded macro", 0)
				.endNode();

		super.createSimpleStatementRule();
	}

	/** {@inheritDoc} */
	@Override
	protected EnumSet<ETokenType> getSimpleBlockKeywordsWithParentheses() {
		return EnumSet.of(WHILE, FOR, SWITCH);
	}

	/** {@inheritDoc} */
	@Override
	protected EnumSet<ETokenType> getSimpleBlockKeywordsWithoutParentheses() {
		return EnumSet.of(ELSE);
	}

	/** {@inheritDoc} */
	@Override
	protected EnumSet<ETokenType> getStatementStartTokens() {
		return EnumSet.of(AUTO, NEW, DELETE, BREAK, CONTINUE, RETURN, ASSERT,
				FINAL, GOTO, SUPER, THIS, THROW, MULT, LPAREN, PLUSPLUS,
				MINUSMINUS, SCOPE, IDENTIFIER);
	}

	/** {@inheritDoc} */
	@Override
	protected RecognizerBase<EGenericParserStates> typePattern(
			RecognizerBase<EGenericParserStates> currentState) {

		EnumSet<ETokenType> extendedTypeKeywords = EnumSet
				.copyOf(getTypeKeywords());
		extendedTypeKeywords.add(TYPENAME);

		return currentState
				.repeated(EnumSet.of(CONST, CONSTEXPR, STATIC, VIRTUAL, EXTERN,
						NEAR, FAR, ALIGNAS, MUTABLE))
				.skipNested(LPAREN, RPAREN).optional(extendedTypeKeywords)
				.skipNested(LPAREN, RPAREN)
				.subRecognizer(createScopeRecognizer(), 0, Integer.MAX_VALUE)
				.optional(EnumSet.of(SIGNED, UNSIGNED))
				.sequence(TYPE_OR_IDENTIFIER).skipNested(LPAREN, RPAREN)
				.subRecognizer(new CppSkipTemplateSpecificationRecognizer(), 0,
						1)
				.skipAny(EnumSet.of(MULT, AND, ANDAND, CONST, CONSTEXPR))
				.skipNested(LBRACK, RBRACK).skipAny(EnumSet.of(MULT, AND,
						ANDAND, CONST, CONSTEXPR, LBRACK, RBRACK, NEAR, FAR));
	}

	/** {@inheritDoc} */
	@Override
	protected void createSubExpressionRules() {
		RecognizerBase<EGenericParserStates> lambdaAlternative = inState(
				IN_EXPRESSION).skipNested(LBRACK, RBRACK)
						.skipNested(LPAREN, RPAREN).repeated(LAMBDA_MODIFIERS)
						.optional(EnumSet.of(NOEXCEPT, THROW))
						.skipNested(LPAREN, RPAREN);

		finishLambdaRule(lambdaAlternative.sequence(POINTERTO)
				.repeated(getTypeKeywords())
				.optional(EnumSet.of(SIGNED, UNSIGNED))
				.optional(TYPE_OR_IDENTIFIER).skipNested(LPAREN, RPAREN));
		finishLambdaRule(lambdaAlternative);

	}

	/** Finishes a rule that indicates a lambda. */
	private static void finishLambdaRule(
			RecognizerBase<EGenericParserStates> recognizer) {
		recognizer.sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD,
						SubTypeNames.LAMBDA_EXPRESSION)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected RecognizerBase<EGenericParserStates> getSubExpressionRecognizer() {
		return new CppLambdaRecognizer();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isFilteredToken(IToken token, IToken previousToken) {
		if (token.getType() == IDENTIFIER
				&& PSEUDO_KEYWORDS.contains(token.getText())) {
			return true;
		}

		return super.isFilteredToken(token, previousToken);
	}

	/** {@inheritDoc} */
	@Override
	protected List<IToken> filterTokens(List<IToken> tokens) {
		return filterCpp11Attributes(
				filterGCCAttributes(super.filterTokens(tokens)));
	}

	/**
	 * Filters GCC attributes. See e.g.
	 * http://gcc.gnu.org/onlinedocs/gcc/Type-Attributes.html
	 */
	private static List<IToken> filterGCCAttributes(List<IToken> tokens) {
		List<IToken> result = new ArrayList<>();
		boolean inAttribute = false;
		int openBraces = 0;
		for (int i = 0; i < tokens.size(); i++) {
			IToken token = tokens.get(i);
			if (token.getText().equals("__attribute__")) {
				inAttribute = true;
			} else if (inAttribute && token.getType() == LPAREN) {
				openBraces++;
			} else if (inAttribute && token.getType() == RPAREN) {
				openBraces--;
				if (openBraces == 0) {
					inAttribute = false;
				}
			} else if (!inAttribute) {
				result.add(token);
			}
		}
		return result;
	}

	/**
	 * Filters C++11 attributes. See e.g.
	 * http://en.cppreference.com/w/cpp/language/attributes
	 */
	private static List<IToken> filterCpp11Attributes(List<IToken> tokens) {
		List<IToken> result = new ArrayList<>();
		boolean inAttribute = false;
		for (int i = 0; i < tokens.size(); i++) {
			ETokenType searchType = LBRACK;
			if (inAttribute) {
				searchType = RBRACK;
			}

			if (TokenStreamUtils.tokenTypesAt(tokens, i, searchType,
					searchType)) {
				inAttribute = !inAttribute;
				i++;
			} else if (!inAttribute) {
				result.add(tokens.get(i));
			}
		}
		return result;
	}
}
