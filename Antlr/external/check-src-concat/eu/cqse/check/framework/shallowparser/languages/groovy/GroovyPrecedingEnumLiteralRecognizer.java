/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.groovy;

import static eu.cqse.check.framework.scanner.ETokenType.AT_OPERATOR;
import static eu.cqse.check.framework.scanner.ETokenType.COMMA;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;

/**
 * Checks the preceding sequence from the token at startOffset. It matches for
 * sequences like:<br>
 * <b>[{,] (@Identifier (...) )?</b> to catch enum literals.
 * 
 * @author $Author: $
 * @version $Rev: $
 * @ConQAT.Rating YELLOW Hash: 70625D1BB83728584EB58F30D9788027
 */
/* package */ class GroovyPrecedingEnumLiteralRecognizer
		extends RecognizerBase<EGroovyShallowParserStates> {

	/**
	 * Types that are allowed to precede a (possibly annotated) enum literal.
	 * These are:
	 * <ul>
	 * <li>COMMA (enum literal after another)</li>
	 * <li>LBRACE (enum literal right at the beginning of the enum type
	 * declaration).</li>
	 */
	private static final EnumSet<ETokenType> ALLOWED_TYPES = EnumSet.of(COMMA,
			LBRACE);

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(
			ParserState<EGroovyShallowParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		if (startOffset < 1) {
			return NO_MATCH;
		}

		int offset = startOffset - 1;
		if (TokenStreamUtils.tokenTypesAt(tokens, offset, RPAREN)) {
			offset = TokenStreamUtils.findMatchingOpeningToken(tokens,
					offset - 1, LPAREN, RPAREN);
			if (offset == NO_MATCH) {
				return NO_MATCH;
			}
			// The parentheses have to be preceded with
			// @ IDENTIFIER, otherwise we have no match.
			if (!TokenStreamUtils.tokenTypesAt(tokens, offset - 2, AT_OPERATOR,
					IDENTIFIER)) {
				return NO_MATCH;
			}
			offset -= 3; // Skip '@Annotation('
		} else {
			// Although there are no parentheses, we might have an annotation
			// nevertheless. Therefore the next two preceding tokens may be
			// '@ IDENTIFIER'
			if (TokenStreamUtils.tokenTypesAt(tokens, offset - 1, AT_OPERATOR,
					IDENTIFIER)) {
				offset -= 2;
			}
		}

		IToken token = tokens.get(offset);
		if (token == null || !ALLOWED_TYPES.contains(token.getType())) {
			return NO_MATCH;
		}
		return startOffset;
	}
}
