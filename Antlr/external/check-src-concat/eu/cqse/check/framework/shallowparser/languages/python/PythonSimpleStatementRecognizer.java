/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.python;

import static eu.cqse.check.framework.shallowparser.languages.python.PythonShallowParser.EPythonParserStates.ANY;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.DEDENT;
import static eu.cqse.check.framework.scanner.ETokenType.DEF;

import java.util.List;

import eu.cqse.check.framework.shallowparser.languages.base.LineBasedStatementRecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.python.PythonShallowParser.EPythonParserStates;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Recognizer for simple statements in Python.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: 12CF0B46DC2827E84D27AF60368A5978
 */
public class PythonSimpleStatementRecognizer extends
		LineBasedStatementRecognizerBase<EPythonParserStates> {

	/** {@inheritDoc} */
	@Override
	protected EPythonParserStates getSubParseState() {
		return ANY;
	}

	/** {@inheritDoc} */
	@Override
	protected boolean tokenStartsSubParse(ETokenType tokenType,
			List<IToken> tokens, int offset) {
		return (tokenType == DEF || tokenType == CLASS);
	}

	/** {@inheritDoc} */
	@Override
	protected boolean startsNewStatement(IToken token, IToken lastToken) {
		ETokenType tokenType = token.getType();
		return tokenType == ETokenType.EOL || tokenType == DEDENT;
	}
}
