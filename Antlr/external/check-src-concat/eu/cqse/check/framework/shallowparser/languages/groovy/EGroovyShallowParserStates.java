/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.groovy;

/**
 * Enum type for all shallow parser states for the groovy language. We cannot
 * use the generic parser states, as we need a state to match enum literals.
 * 
 * @author $Author: $
 * @version $Rev: $
 * @ConQAT.Rating YELLOW Hash: F494698D6D7CCF15BFB6E0231FF0983D
 */
/* package */ enum EGroovyShallowParserStates {
	/** Top level state. */
	TOP_LEVEL,

	/** Currently within enum type definition. */
	IN_ENUM_TYPE,

	/** Currently within type definition (class, etc.). */
	IN_TYPE,

	/** Currently within method/procedure/function definition. */
	IN_METHOD,

	/**
	 * Currently within an expression. Typically, we expect anonymous classes,
	 * and closures in this state.
	 */
	IN_EXPRESSION;
}
