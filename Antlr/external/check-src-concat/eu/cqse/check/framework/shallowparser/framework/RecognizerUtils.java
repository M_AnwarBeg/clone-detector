/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.framework;

import java.lang.reflect.Array;
import java.util.List;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Utility methods for writing recognizers.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56492 $
 * @ConQAT.Rating YELLOW Hash: 8A3257F21ACFB0005221FF0978260E13
 */
public class RecognizerUtils {

	/**
	 * Resolves a name based on the token stream and the parser state. The
	 * "name" can either be null, a constant name (String), an index into the
	 * token stream (int), a {@link Region} of the token stream, an array
	 * (indicating multiple of those mentioned before) or an
	 * {@link INameResolver}.
	 * 
	 * @param extractStringContent
	 *            if this is true and the token is a string literal, the
	 *            surrounding quotes are removed.
	 */
	public static <STATE extends Enum<STATE>> String resolveName(
			List<IToken> tokens, ParserState<STATE> parserState,
			int startOffset, Object name, boolean extractStringContent) {
		if (name == null) {
			return null;
		}

		if (name instanceof String) {
			return (String) name;
		}

		if (name instanceof Number) {
			int index = ((Number) name).intValue();
			return normalizeText(
					tokens.get(resolveIndex(parserState, startOffset, index)),
					extractStringContent);
		}

		if (name instanceof Region) {
			return resolveRegionName(tokens, parserState, startOffset,
					(Region) name, extractStringContent);
		}

		if (name.getClass().isArray()) {
			return resolveArrayName(tokens, parserState, startOffset, name,
					extractStringContent);
		}

		if (name instanceof INameResolver) {
			@SuppressWarnings("unchecked")
			INameResolver<STATE> resolver = (INameResolver<STATE>) name;
			return resolver.resolveName(parserState, tokens, startOffset);
		}

		throw new AssertionError(
				"Unexpected type in resolving of name: " + name.getClass());
	}

	/** Handles name resolution for arrays. */
	private static <STATE extends Enum<STATE>> String resolveArrayName(
			List<IToken> tokens, ParserState<STATE> parserState,
			int startOffset, Object name, boolean extractStringContent) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Array.getLength(name); ++i) {
			if (i > 0) {
				sb.append(" ");
			}
			sb.append(resolveName(tokens, parserState, startOffset,
					Array.get(name, i), extractStringContent));
		}
		return sb.toString();
	}

	/** Handles name resolution for regions. */
	private static <STATE extends Enum<STATE>> String resolveRegionName(
			List<IToken> tokens, ParserState<STATE> parserState,
			int startOffset, Region region, boolean extractStringContent) {
		int start = resolveIndex(parserState, startOffset, region.getStart());
		int end = resolveIndex(parserState, startOffset, region.getEnd());
		StringBuilder sb = new StringBuilder();
		for (int i = start; i <= end; ++i) {
			if (i > start
					&& !region.getOrigin().equals(Region.UNKNOWN_ORIGIN)) {
				sb.append(region.getOrigin());
			}
			sb.append(normalizeText(tokens.get(i), extractStringContent));
		}
		return sb.toString();
	}

	/** Resolves the index relative to the parser state. */
	private static <STATE extends Enum<STATE>> int resolveIndex(
			ParserState<STATE> parserState, int startOffset, int index) {
		if (index >= 0) {
			index = parserState.getCurrentReferencePosition() + index;
		} else {
			// we need addition here, as index is negative
			index = startOffset + index;
		}
		return index;
	}

	/**
	 * Returns the token text. For case-insensitive languages, it is converted
	 * to lower case.
	 * 
	 * @param extractStringContent
	 *            if this is true and the token is a string literal, the
	 *            surrounding quotes are removed.
	 */
	private static String normalizeText(IToken token,
			boolean extractStringContent) {
		String text = token.getText();
		if (extractStringContent
				&& token.getType() == ETokenType.STRING_LITERAL) {
			return text.substring(1, text.length() - 1);
		}

		if (token.getLanguage().isCaseSensitive()) {
			return text;
		}
		return text.toLowerCase();
	}
}
