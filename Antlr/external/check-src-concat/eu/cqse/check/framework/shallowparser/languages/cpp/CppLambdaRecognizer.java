/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_EXPRESSION;

import java.util.List;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Recognizer that finds C++ lambdas and continues parsing within these.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56397 $
 * @ConQAT.Rating YELLOW Hash: 4B6F35E9B138B7EF0105DA56F56C0129
 */
public class CppLambdaRecognizer extends RecognizerBase<EGenericParserStates> {

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<EGenericParserStates> parserState,
			List<IToken> tokens, int startOffset) {
		int currentOffset = skipCaptureList(tokens, startOffset);
		if (currentOffset == NO_MATCH) {
			return NO_MATCH;
		}

		currentOffset = skipOptionalParameters(tokens, currentOffset);
		if (currentOffset == NO_MATCH) {
			return NO_MATCH;
		}

		// At this point we cannot be sure if this is really a lambda, but the
		// rule in the parser does the final check whether this is a lambda or
		// not.
		return parserState.parse(IN_EXPRESSION, tokens, startOffset);
	}

	/**
	 * Skips over the capture list and returns the new offset or
	 * {@link RecognizerBase#NO_MATCH} if none is found.
	 */
	private static int skipCaptureList(List<IToken> tokens, int currentOffset) {
		if (!TokenStreamUtils.tokenTypesAt(tokens, currentOffset, LBRACK)) {
			return NO_MATCH;
		}

		int closingBracket = TokenStreamUtils.findMatchingClosingToken(tokens,
				currentOffset + 1, LBRACK, RBRACK);
		if (closingBracket == TokenStreamUtils.NOT_FOUND) {
			return NO_MATCH;
		}
		return closingBracket + 1;
	}

	/**
	 * Skips over the optional parameters and returns the new offset or
	 * {@link RecognizerBase#NO_MATCH} if it is malformed.
	 */
	private static int skipOptionalParameters(List<IToken> tokens,
			int currentOffset) {
		if (!TokenStreamUtils.tokenTypesAt(tokens, currentOffset, LPAREN)) {
			return currentOffset;
		}

		int closingParenthesis = TokenStreamUtils.findMatchingClosingToken(
				tokens, currentOffset + 1, LPAREN, RPAREN);
		if (closingParenthesis == TokenStreamUtils.NOT_FOUND) {
			return NO_MATCH;
		}
		return closingParenthesis + 1;
	}
}
