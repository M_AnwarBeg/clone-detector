/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.SequenceRecognizer.ITokenMatcher;

/**
 * Matches the keyword "asm" or the strings "_asm" and "__asm".
 * 
 * @author $Author: kupka $
 * @version $Rev: 56284 $
 * @ConQAT.Rating YELLOW Hash: 072D11B0B639FDAF01CE75E81E14D648
 */
/* package */class ASMIdentifierMatcher implements ITokenMatcher {

	/** {@inheritDoc} */
	@Override
	public boolean matches(IToken token) {
		return token.getType() == ETokenType.ASM
				|| (token.getType().getTokenClass() == ETokenClass.IDENTIFIER
						&& ("_asm".equals(token.getText())
								|| "__asm".equals(token.getText())));
	}
}