/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.php;

import static eu.cqse.check.framework.scanner.ETokenType.ABSTRACT;
import static eu.cqse.check.framework.scanner.ETokenType.AND;
import static eu.cqse.check.framework.scanner.ETokenType.BACKSLASH;
import static eu.cqse.check.framework.scanner.ETokenType.BREAK;
import static eu.cqse.check.framework.scanner.ETokenType.CASE;
import static eu.cqse.check.framework.scanner.ETokenType.CATCH;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.CLONE;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.CONST;
import static eu.cqse.check.framework.scanner.ETokenType.CONSTRUCTOR;
import static eu.cqse.check.framework.scanner.ETokenType.CONTINUE;
import static eu.cqse.check.framework.scanner.ETokenType.DECLARE;
import static eu.cqse.check.framework.scanner.ETokenType.DEFAULT;
import static eu.cqse.check.framework.scanner.ETokenType.DESTRUCTOR;
import static eu.cqse.check.framework.scanner.ETokenType.ECHO;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.ELSEIF;
import static eu.cqse.check.framework.scanner.ETokenType.ENDDECLARE;
import static eu.cqse.check.framework.scanner.ETokenType.ENDFOR;
import static eu.cqse.check.framework.scanner.ETokenType.ENDFOREACH;
import static eu.cqse.check.framework.scanner.ETokenType.ENDIF;
import static eu.cqse.check.framework.scanner.ETokenType.ENDSWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.ENDWHILE;
import static eu.cqse.check.framework.scanner.ETokenType.EQ;
import static eu.cqse.check.framework.scanner.ETokenType.FINAL;
import static eu.cqse.check.framework.scanner.ETokenType.FINALLY;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FOREACH;
import static eu.cqse.check.framework.scanner.ETokenType.FUNCTION;
import static eu.cqse.check.framework.scanner.ETokenType.GLOBAL;
import static eu.cqse.check.framework.scanner.ETokenType.GOTO;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.INCLUDE;
import static eu.cqse.check.framework.scanner.ETokenType.INCLUDE_ONCE;
import static eu.cqse.check.framework.scanner.ETokenType.INTEGER_LITERAL;
import static eu.cqse.check.framework.scanner.ETokenType.INTERFACE;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.MINUSMINUS;
import static eu.cqse.check.framework.scanner.ETokenType.NAMESPACE;
import static eu.cqse.check.framework.scanner.ETokenType.NEW;
import static eu.cqse.check.framework.scanner.ETokenType.PLUSPLUS;
import static eu.cqse.check.framework.scanner.ETokenType.PRINT;
import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.REQUIRE;
import static eu.cqse.check.framework.scanner.ETokenType.REQUIRE_ONCE;
import static eu.cqse.check.framework.scanner.ETokenType.RETURN;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.STATIC;
import static eu.cqse.check.framework.scanner.ETokenType.SWITCH;
import static eu.cqse.check.framework.scanner.ETokenType.TEMPLATE_CODE_END;
import static eu.cqse.check.framework.scanner.ETokenType.THIS;
import static eu.cqse.check.framework.scanner.ETokenType.THROW;
import static eu.cqse.check.framework.scanner.ETokenType.TRAIT;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.USE;
import static eu.cqse.check.framework.scanner.ETokenType.VAR;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.YIELD;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_EXPRESSION;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.TOP_LEVEL;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * PHP shallow parser.
 * 
 * Some special cases for this parser:
 * <ul>
 * <li>Beware, that in most cases field definitions / variable declarations are
 * recognized as simple statements.</li>
 * <li>As of PHP 5.4.0, writing CONTINUE without a semicolon raises a compile
 * error. However this parser accepts CONTINUE from the prior versions as well.
 * </li>
 * </ul>
 * 
 * @author $Author: $
 * @version $Rev: $
 * @ConQAT.Rating YELLOW Hash: 934C25314FA0A814F86E612B328389FA
 */
public class PhpShallowParser extends ShallowParserBase<EGenericParserStates> {

	/** All token types that may stand at the beginning of new statement. */
	private static final EnumSet<ETokenType> STATEMENT_START_TOKENS = EnumSet
			.of(BACKSLASH, BREAK, CLONE, ECHO, GOTO, IDENTIFIER, MINUSMINUS,
					NAMESPACE, NEW, PLUSPLUS, PRINT, RETURN, STATIC, THIS,
					THROW, YIELD);
	/** A set of access modifiers. */
	private static final EnumSet<ETokenType> ACCESS_MODIFIERS = EnumSet.of(
			PRIVATE, PUBLIC, PROTECTED, STATIC, GLOBAL, CONST, ABSTRACT, FINAL,
			VAR);

	/** Constructor. */
	public PhpShallowParser() {
		super(EGenericParserStates.class, TOP_LEVEL);
		createInExpressionRules();
		createConstructorRule();
		createMethodRule();
		createTypeRule();
		createIfElseRule();
		createTryCatchRule();
		createSwitchCaseRule();
		createLoopRules();
		createNamespaceRule();
		createMetaRules();
		createVariableRule();
		createAnonymousBlockRule();
		createSingleStatementRule();
		createEmptyStatementRule();
	}

	/**
	 * Creates the rule for anonymous inner classes and anonymous functions.
	 * They are typically found within other statements.
	 */
	private void createInExpressionRules() {

		// Anonymous inner classes
		RecognizerBase<EGenericParserStates> innerClassBase = inState(
				IN_EXPRESSION)
						.sequence(NEW, CLASS)
						.createNode(EShallowEntityType.TYPE,
								SubTypeNames.ANONYMOUS_CLASS, 1)
						.skipBefore(LBRACE);
		endTypeRule(innerClassBase);

		// Anonymous functions
		RecognizerBase<EGenericParserStates> closureBase = inState(
				IN_EXPRESSION)
						.sequence(FUNCTION)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.ANONYMOUS_FUNCTION)
						.skipNested(LPAREN, RPAREN,
								createSubexpressionRecognizer());

		RecognizerBase<EGenericParserStates> closureBaseWithUse = closureBase
				.sequence(USE)
				.skipNested(LPAREN, RPAREN, createSubexpressionRecognizer());

		endMethodRule(closureBaseWithUse);
		endMethodRule(closureBase);
	}

	/**
	 * Create a rule that matches try catch finally statements.
	 */
	private void createTryCatchRule() {
		RecognizerBase<EGenericParserStates> tryBase = inState(TOP_LEVEL,
				IN_METHOD).sequence(TRY).createNode(
						EShallowEntityType.STATEMENT, SubTypeNames.TRY);

		RecognizerBase<EGenericParserStates> catchBase = inState(TOP_LEVEL,
				IN_METHOD).sequence(CATCH)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.CATCH)
						.skipNested(LPAREN, RPAREN);

		RecognizerBase<EGenericParserStates> finallyBase = inState(TOP_LEVEL,
				IN_METHOD).sequence(FINALLY).createNode(
						EShallowEntityType.STATEMENT, SubTypeNames.FINALLY);

		continueTryCatchRule(tryBase);
		continueTryCatchRule(catchBase);
		continueTryCatchRule(finallyBase);

	}

	/**
	 * Continue the try catch finally rule.
	 */
	private static void continueTryCatchRule(
			RecognizerBase<EGenericParserStates> base) {

		RecognizerBase<EGenericParserStates> multilineBase = base
				.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE);
		RecognizerBase<EGenericParserStates> singlelineBase = base
				.parseOnce(IN_METHOD);

		endTryCatchRule(multilineBase);
		endTryCatchRule(singlelineBase);
	}

	/**
	 * End the try catch finally rule.
	 */
	private static void endTryCatchRule(
			RecognizerBase<EGenericParserStates> base) {
		base.sequenceBefore(EnumSet.of(CATCH, FINALLY))
				.endNodeWithContinuation();
		base.endNode();
	}

	/**
	 * Create a rule that matches constructors and destructors.
	 */
	private void createConstructorRule() {
		createConstructorRule(CONSTRUCTOR, SubTypeNames.CONSTRUCTOR);
		createConstructorRule(DESTRUCTOR, SubTypeNames.DESTRUCTOR);
	}

	/**
	 * Create a rule that matches constructors or destructors.
	 */
	private void createConstructorRule(ETokenType tokenType,
			String subtypeName) {
		RecognizerBase<EGenericParserStates> base = inState(IN_TYPE)
				.repeated(ACCESS_MODIFIERS).sequence(FUNCTION)
				.sequence(tokenType)
				.createNode(EShallowEntityType.METHOD, subtypeName)
				.skipNested(LPAREN, RPAREN);
		base.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE).endNode();
		base.sequence(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END)).endNode();
	}

	/**
	 * Create a rule that matches modules (e.g. namespaces).
	 */
	private void createNamespaceRule() {

		RecognizerBase<EGenericParserStates> base = inState(TOP_LEVEL)
				.sequence(NAMESPACE)
				// We have to make sure to not match the sequence NAMESPACE -
				// BACKSLASH here. These can be used to call functions etc.
				.sequenceBefore(EnumSet.complementOf(EnumSet.of(BACKSLASH)))
				.skipBefore(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END, LBRACE))
				.createNode(EShallowEntityType.MODULE, SubTypeNames.NAMESPACE,
						new Region(1, -1));

		base.sequence(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END)).endNode();
		base.sequence(LBRACE).parseUntil(TOP_LEVEL).sequence(RBRACE).endNode();
	}

	/**
	 * Create a rule that matches an empty statement (the semicolon).
	 */
	private void createEmptyStatementRule() {
		inAnyState().sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT)
				.endNode();

	}

	/**
	 * Create a rule that matches anonymous blocks.
	 */
	private void createAnonymousBlockRule() {
		inState(TOP_LEVEL, IN_METHOD).sequence(LBRACE)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.ANONYMOUS_BLOCK)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();
	}

	/**
	 * Create rules that match labels, use and declare statements.
	 */
	private void createMetaRules() {
		// Declare
		RecognizerBase<EGenericParserStates> baseDeclare = inState(TOP_LEVEL,
				IN_METHOD)
						.sequence(DECLARE)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.DECLARATION)
						.skipNested(LPAREN, RPAREN);
		baseDeclare.sequence(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END))
				.endNode();
		endControlFlowStatement(baseDeclare, ENDDECLARE);

		// Use
		RecognizerBase<EGenericParserStates> baseUse = inAnyState()
				.sequence(USE)
				.createNode(EShallowEntityType.META, SubTypeNames.USE,
						new Region(1, -1))
				.skipBefore(EnumSet.of(TEMPLATE_CODE_END, SEMICOLON, LBRACE));

		baseUse.sequence(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END)).endNode();
		baseUse.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.endNode();

		createRequireRule(REQUIRE, SubTypeNames.REQUIRE);
		createRequireRule(REQUIRE_ONCE, SubTypeNames.REQUIRE_ONCE);
		createRequireRule(INCLUDE, SubTypeNames.INCLUDE);
		createRequireRule(INCLUDE_ONCE, SubTypeNames.INCLUDE_ONCE);

		// Label
		inAnyState().sequence(IDENTIFIER, COLON)
				.createNode(EShallowEntityType.META, SubTypeNames.LABEL, 0)
				.endNode();
	}

	/** Creates a rule for include/require */
	private void createRequireRule(ETokenType requireToken, String subtype) {
		inAnyState().sequence(requireToken)
				.createNode(EShallowEntityType.META, subtype, new Region(1, -1))
				.skipTo(EnumSet.of(TEMPLATE_CODE_END, SEMICOLON)).endNode();
	}

	/**
	 * Create a rule that matches switch case.
	 */
	private void createSwitchCaseRule() {
		inState(IN_METHOD).sequence(EnumSet.of(CASE, DEFAULT))
				.createNode(EShallowEntityType.META, 0).optional(IDENTIFIER)
				.repeated(COLON, COLON, IDENTIFIER)
				// "case 1;" is allowed.
				.skipTo(EnumSet.of(COLON, SEMICOLON, TEMPLATE_CODE_END))
				.endNode();

		RecognizerBase<EGenericParserStates> base = inState(TOP_LEVEL,
				IN_METHOD)
						.sequence(SWITCH)
						.createNode(EShallowEntityType.STATEMENT,
								SubTypeNames.SWITCH)
						.skipNested(LPAREN, RPAREN,
								createSubexpressionRecognizer());
		endControlFlowStatement(base, ENDSWITCH);
	}

	/**
	 * Create a rule that matches type declarations.
	 */
	private void createTypeRule() {
		createTypeRule(CLASS, SubTypeNames.CLASS);
		createTypeRule(INTERFACE, SubTypeNames.INTERFACE);
		createTypeRule(TRAIT, SubTypeNames.TRAIT);
	}

	/**
	 * Create a rule that matches a specific type declaration.
	 */
	private void createTypeRule(ETokenType type, String subtypeName) {
		RecognizerBase<EGenericParserStates> base = inState(TOP_LEVEL,
				IN_METHOD).repeated(ACCESS_MODIFIERS).sequence(type).markStart()
						.sequence(IDENTIFIER)
						.createNode(EShallowEntityType.TYPE, subtypeName, 0)
						.skipBefore(LBRACE);
		endTypeRule(base);
	}

	/**
	 * End type rule creation with a parse within IN_TYPE surrounded by braces.
	 */
	private static void endTypeRule(RecognizerBase<EGenericParserStates> base) {
		base.sequence(LBRACE).parseUntil(IN_TYPE).sequence(RBRACE).endNode();
	}

	/**
	 * Create a rule that matches variables and attributes.
	 */
	private void createVariableRule() {
		inState(TOP_LEVEL, IN_METHOD).repeated(ACCESS_MODIFIERS).markStart()
				.sequence(IDENTIFIER)
				.sequence(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END))
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.LOCAL_VARIABLE, 0)
				.endNode();

		inState(TOP_LEVEL, IN_METHOD).sequence(ACCESS_MODIFIERS)
				.repeated(ACCESS_MODIFIERS).markStart().sequence(IDENTIFIER)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.LOCAL_VARIABLE, 0)
				.skipToWithNesting(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END),
						LPAREN, RPAREN, createSubexpressionRecognizer())
				.endNode();

		inState(IN_TYPE).repeated(ACCESS_MODIFIERS).markStart()
				.sequence(IDENTIFIER).sequenceBefore(EQ)
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ATTRIBUTE, 0)
				.skipToWithNesting(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END),
						LPAREN, RPAREN, createSubexpressionRecognizer())
				.endNode();

		inState(IN_TYPE).repeated(ACCESS_MODIFIERS).markStart()
				.sequence(IDENTIFIER, EnumSet.of(SEMICOLON, TEMPLATE_CODE_END))
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.ATTRIBUTE, 0)
				.endNode();
	}

	/**
	 * Create rules that match all kinds of loop statements.
	 */
	private void createLoopRules() {
		createLoopRule(FOREACH, ENDFOREACH);
		createLoopRule(FOR, ENDFOR);
		createLoopRule(WHILE, ENDWHILE);

		// Do-While
		RecognizerBase<EGenericParserStates> base = inState(IN_METHOD,
				TOP_LEVEL).sequence(ETokenType.DO).createNode(
						EShallowEntityType.STATEMENT, SubTypeNames.DO);
		base.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE, WHILE)
				.skipNested(LPAREN, RPAREN, createSubexpressionRecognizer())
				.optional(SEMICOLON).endNode();
		base.parseOnce(IN_METHOD).sequence(WHILE)
				.skipNested(LPAREN, RPAREN, createSubexpressionRecognizer())
				.optional(SEMICOLON).endNode();
	}

	/**
	 * Create a rule that matches the normal version as well as the alternative
	 * versions of loop statements.
	 */
	private void createLoopRule(ETokenType startToken, ETokenType endToken) {
		RecognizerBase<EGenericParserStates> base = inState(IN_METHOD,
				TOP_LEVEL)
						.sequence(startToken)
						.skipNested(LPAREN, RPAREN,
								createSubexpressionRecognizer())
						.createNode(EShallowEntityType.STATEMENT, 0);
		endControlFlowStatement(base, endToken);
	}

	/**
	 * Create a rule that matches if statements.
	 */
	private void createIfElseRule() {
		RecognizerBase<EGenericParserStates> base = inState(IN_METHOD,
				TOP_LEVEL)
						.sequence(EnumSet.of(IF, ELSEIF))
						.skipNested(LPAREN, RPAREN,
								createSubexpressionRecognizer())
						.createNode(EShallowEntityType.STATEMENT, 0);
		continueIfElseRule(base);

		RecognizerBase<EGenericParserStates> elseBase = inState(IN_METHOD,
				TOP_LEVEL).sequence(ELSE)
						.createNode(EShallowEntityType.STATEMENT, 0);
		continueIfElseRule(elseBase);
	}

	/**
	 * Continues an if else rule possibly with continuation.
	 */
	private static void continueIfElseRule(
			RecognizerBase<EGenericParserStates> base) {
		RecognizerBase<EGenericParserStates> multiLineBase = base
				.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE);

		RecognizerBase<EGenericParserStates> multiLineBaseAlternative = base
				.sequence(COLON).parseUntil(IN_METHOD)
				.sequenceBefore(EnumSet.of(ELSE, ELSEIF, ENDIF));

		RecognizerBase<EGenericParserStates> singleLineBase = base
				.parseOnce(IN_METHOD);

		endIfElseRule(singleLineBase);
		endIfElseRule(multiLineBase);
		endIfElseRule(multiLineBaseAlternative);
	}

	/**
	 * Ends an if else rule possibly with continuation.
	 */
	private static void endIfElseRule(
			RecognizerBase<EGenericParserStates> base) {
		base.sequenceBefore(EnumSet.of(ELSE, ELSEIF)).endNodeWithContinuation();
		base.optional(ENDIF, SEMICOLON).endNode();
		base.optional(ENDIF).endNode();
	}

	/**
	 * Create a rule that matches method declarations. In PHP there is the
	 * concept of conditional functions, therefore we recognize functions within
	 * IN_METHOD, too.
	 */
	private void createMethodRule() {
		RecognizerBase<EGenericParserStates> base = inState(TOP_LEVEL, IN_TYPE,
				IN_METHOD).repeated(ACCESS_MODIFIERS).sequence(FUNCTION)
						.optional(AND).sequence(IDENTIFIER)
						.createNode(EShallowEntityType.METHOD,
								SubTypeNames.METHOD, -1)
						// we need this skip because of strict typing
						.skipBeforeWithNesting(EnumSet.of(LBRACE, SEMICOLON,
								TEMPLATE_CODE_END), LPAREN, RPAREN);

		endMethodRule(base);
		// abstract method in e.g. interfaces.
		base.sequence(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END)).endNode();
	}

	/**
	 * Ends a standard method rule with a parse within IN_METHOD surrounded by
	 * braces.
	 */
	private static void endMethodRule(
			RecognizerBase<EGenericParserStates> base) {
		base.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE).endNode();
	}

	/**
	 * Create a rule that matches single statements.
	 */
	private void createSingleStatementRule() {
		inState(TOP_LEVEL, IN_METHOD).sequenceBefore(STATEMENT_START_TOKENS)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0)
				.skipToWithNesting(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END),
						LPAREN, RPAREN, createSubexpressionRecognizer())
				.endNode();

		// Statements that are enclosed in parenthesis.
		inState(TOP_LEVEL, IN_METHOD).sequenceBefore(LPAREN)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0)
				.skipToWithNesting(EnumSet.of(SEMICOLON, TEMPLATE_CODE_END),
						LPAREN, RPAREN, createSubexpressionRecognizer())
				.endNode();

		// Continue is a special case, as one could omit the
		// semicolon prior to PHP 5.4.0.
		RecognizerBase<EGenericParserStates> continueBase = inState(TOP_LEVEL,
				IN_METHOD).sequence(CONTINUE).createNode(
						EShallowEntityType.STATEMENT,
						SubTypeNames.SIMPLE_STATEMENT, 0);
		continueBase
				.sequence(EnumSet.of(INTEGER_LITERAL, IDENTIFIER), SEMICOLON)
				.endNode();
		continueBase.optional(SEMICOLON).endNode();
	}

	/**
	 * Ends a typical control flow statement with.
	 */
	private static void endControlFlowStatement(
			RecognizerBase<EGenericParserStates> base,
			ETokenType alternativeEndToken) {
		base.sequence(COLON).parseUntil(IN_METHOD).sequence(alternativeEndToken)
				.optional(SEMICOLON).endNode();
		base.sequence(LBRACE).parseUntil(IN_METHOD).sequence(RBRACE)
				.optional(SEMICOLON).endNode();
		base.sequence(SEMICOLON).endNode();
		base.parseOnce(IN_METHOD).optional(SEMICOLON).endNode();
	}

	/**
	 * Create a new sub expression recognizer.
	 */
	public static RecognizerBase<EGenericParserStates> createSubexpressionRecognizer() {
		return new PhpSubExpressionRecognizer();
	}
}
