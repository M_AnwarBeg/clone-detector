/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.COMP;
import static eu.cqse.check.framework.scanner.ETokenType.GT;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.LT;
import static eu.cqse.check.framework.scanner.ETokenType.OPERATOR;
import static eu.cqse.check.framework.scanner.ETokenType.POINTERTO;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACE;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.VIRTUAL;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_METHOD;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.IN_TYPE;
import static eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates.TOP_LEVEL;

import java.util.EnumSet;

import org.conqat.lib.commons.region.Region;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.languages.base.CStyleShallowParserRuleProviderBase;
import eu.cqse.check.framework.shallowparser.languages.base.EGenericParserStates;

/**
 * Provides the class element rules for the {@link CppShallowParser}.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56397 $
 * @ConQAT.Rating YELLOW Hash: 682A1EC4522E00E8450E1FFF09209F84
 */
/* package */class CppShallowParserClassElementRules
		extends CStyleShallowParserRuleProviderBase<CppShallowParser> {

	/** Constructor. */
	public CppShallowParserClassElementRules(CppShallowParser delegateParser) {
		super(delegateParser);
	}

	/** {@inheritDoc} */
	@Override
	public void contributeRules() {
		createOperatorRules();
		createMethodRules();

		// heuristic for preprocessor additions; we specify it here although we
		// parse it as META, as we want it to match between the constructor and
		// the attribute
		inState(TOP_LEVEL, IN_TYPE)
				.sequence(new UppercaseIdentifierMatcher(), LPAREN)
				.skipToWithNesting(RPAREN, LPAREN, RPAREN)
				.createNode(EShallowEntityType.META,
						"preprocessor generated code", 0)
				.endNode();

		// attributes and global variables
		createVariableRule(TOP_LEVEL, SubTypeNames.GLOBAL_VARIABLE);
		createVariableRule(IN_TYPE, SubTypeNames.ATTRIBUTE);

		// global variables that directly follow a struct/class/enum.
		// Example: struct { int x, y; } myPair = { 16, 17};
		// In this case we want to parse the struct as type and the part from
		// myPair as variable. As these are only common in plain C code, we only
		// use top-level and no namespaces and generics. Heuristic is to look
		// for = or [].
		inState(TOP_LEVEL)
				.sequence(IDENTIFIER,
						EnumSet.of(ETokenType.EQ, ETokenType.LBRACK))
				.createNode(EShallowEntityType.ATTRIBUTE,
						SubTypeNames.GLOBAL_VARIABLE, 0)
				.skipToWithNesting(SEMICOLON, LBRACE, RBRACE,
						createSubExpressionRecognizer())
				.endNode();
	}

	/** Creates a rule for detecting variables. */
	private void createVariableRule(EGenericParserStates state, String name) {
		typePatternInState(state)
				.subRecognizer(createScopeRecognizer(), 0, Integer.MAX_VALUE)
				.markStart().sequence(IDENTIFIER)
				.createNode(EShallowEntityType.ATTRIBUTE, name, 0)
				.skipToWithNesting(SEMICOLON, LBRACE, RBRACE,
						createSubExpressionRecognizer())
				.endNode();
	}

	/** Creates parsing rules for operator overloading. */
	private void createOperatorRules() {
		// operator overloading
		completeMethod(SubTypeNames.OPERATOR,
				typePatternInState(IN_TYPE, TOP_LEVEL)
						.subRecognizer(createScopeRecognizer(), 0,
								Integer.MAX_VALUE)
						.markStart().sequence(OPERATOR).skipTo(LPAREN),
				new Region(0, 1));

		// operator overloading without type (conversion operators)
		completeMethod("cast operator",
				inState(IN_TYPE, TOP_LEVEL)
						.subRecognizer(createScopeRecognizer(), 0,
								Integer.MAX_VALUE)
						.markStart().sequence(OPERATOR).skipTo(LPAREN),
				new int[] { 0, 1 });
	}

	/**
	 * Creates parsing rules for methods (including constructors and
	 * destructors).
	 */
	private void createMethodRules() {
		// functions, procedures, methods
		completeMethod("function",
				typePatternInState(IN_TYPE, TOP_LEVEL)
						.subRecognizer(createScopeRecognizer(), 0,
								Integer.MAX_VALUE)
						.markStart().sequence(IDENTIFIER).sequence(LPAREN));

		// destructor
		completeMethod("destructor",
				inState(IN_TYPE, TOP_LEVEL).optional(VIRTUAL)
						.subRecognizer(createScopeRecognizer(), 0,
								Integer.MAX_VALUE)
						.sequence(COMP).markStart().sequence(IDENTIFIER)
						.skipNested(LT, GT).sequence(LPAREN));

		// constructor
		completeMethod("constructor",
				inState(IN_TYPE, TOP_LEVEL)
						.subRecognizer(createScopeRecognizer(), 0,
								Integer.MAX_VALUE)
						.markStart().sequence(IDENTIFIER).skipNested(LT, GT)
						.sequence(LPAREN));
	}

	/**
	 * Completes a method-like construct. This begins with skipping the
	 * parameter list (i.e. the construct has to already match the left
	 * parenthesis). This end either in a complete method with a body, or with a
	 * semicolon and thus is just a declaration.
	 */
	private static void completeMethod(String name,
			RecognizerBase<EGenericParserStates> start) {
		completeMethod(name, start, 0);
	}

	/**
	 * Completes a method-like construct. This begins with skipping the
	 * parameter list (i.e. the construct has to already match the left
	 * parenthesis). This end either in a complete method with a body, or with a
	 * semicolon and thus is just a declaration.
	 * 
	 * @param methodName
	 *            the name of the method. This object accepts the same types as
	 *            the name object in createNode.
	 */
	private static void completeMethod(String name,
			RecognizerBase<EGenericParserStates> start, Object methodName) {

		// the keywords we break on to check for K&R style
		EnumSet<ETokenType> krCheck = EnumSet.of(LBRACE, SEMICOLON, IDENTIFIER,
				COLON, POINTERTO);
		krCheck.addAll(CppShallowParser.PRIMITIVE_TYPES);

		// the next check is a bit more involved, as it is part of our heuristic
		// to recognize code generating preprocessor statements and also should
		// support K&R style functions
		RecognizerBase<EGenericParserStates> krStyleAlternative = start
				.skipToWithNesting(RPAREN, LPAREN, RPAREN)
				// 1.) go to first LBRACE, SEMICOLON, IDENTIFIER,
				// COLON, or primitive type keyword
				.skipBeforeWithNesting(krCheck, LPAREN, RPAREN);

		// 2.) If we find a type keyword first, this must be K&R style and we
		// continue at the brace
		krStyleAlternative.sequence(CppShallowParser.PRIMITIVE_TYPES)
				.skipTo(LBRACE)
				.createNode(EShallowEntityType.METHOD, name, methodName)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();

		RecognizerBase<EGenericParserStates> declarationAlternative = krStyleAlternative
				// 3.) without K&R style, this is either a constructor (COLON),
				// a function definition (LBRACE) or a function declaration
				// (SEMICOLON)
				.sequenceBefore(
						EnumSet.of(LBRACE, SEMICOLON, COLON, POINTERTO));

		// 4.1.) in case of a COLON, skip the initializer list
		finishMethod(name,
				declarationAlternative.sequence(COLON)
						.subRecognizer(new CppInitializerListRecognizer(), 1, 1)
						.sequenceBefore(EnumSet.of(LBRACE, SEMICOLON)),
				methodName);
		// 4.2.) in case of a POINTERTO, skip the return type
		finishMethod(name, declarationAlternative.sequence(POINTERTO)
				.skipBefore(EnumSet.of(LBRACE, SEMICOLON)), methodName);
		// 4.3.) otherwise just finish the method
		finishMethod(name, declarationAlternative, methodName);
	}

	/**
	 * Finishes either a method or a method declaration depending on the next
	 * token (either LBRACE or SEMICOLON).
	 */
	private static void finishMethod(String name,
			RecognizerBase<EGenericParserStates> recognizer,
			Object methodName) {
		recognizer.sequence(LBRACE)
				.createNode(EShallowEntityType.METHOD, name, methodName)
				.parseUntil(IN_METHOD).sequence(RBRACE).endNode();

		recognizer.sequence(SEMICOLON).createNode(EShallowEntityType.METHOD,
				name + " declaration", methodName).endNode();
	}

	/**
	 * Creates a new recognizer that can match a scope prefix for a method-like
	 * construct. This includes sequences of identifiers with double colon,
	 * possibly intermixed with template arguments.
	 */
	private RecognizerBase<EGenericParserStates> createScopeRecognizer() {
		return delegateParser.createScopeRecognizer();
	}

	/** Creates a recognizer that matches lambdas within expressions. */
	protected RecognizerBase<EGenericParserStates> createSubExpressionRecognizer() {
		return delegateParser.getSubExpressionRecognizer();
	}
}
