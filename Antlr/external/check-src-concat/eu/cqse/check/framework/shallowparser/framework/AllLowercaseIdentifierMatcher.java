/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.framework;

import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;

import eu.cqse.check.framework.shallowparser.framework.SequenceRecognizer.ITokenMatcher;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Matches identifiers that contains only lowercase letters
 *
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: B072D347E09412F6A557A89C47AF0DB4
 */
public class AllLowercaseIdentifierMatcher implements ITokenMatcher {

	/** {@inheritDoc} */
	@Override
	public boolean matches(IToken token) {
		return token.getType() == IDENTIFIER
				&& token.getText().toLowerCase().equals(token.getText());
	}
}