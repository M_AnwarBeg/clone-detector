/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.base;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ParserState;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;

/**
 * Recognizer that finds anonymous classes as they would appear in java and
 * performs parsing within this class.
 * 
 * @author $Author: baumeister $
 * @version $Rev: 52657 $
 * @ConQAT.Rating YELLOW Hash: 578CD22020BE88832667E5CD3E734C32
 */
public class JavaLikeAnonymousClassRecognizer<STATE extends Enum<STATE>>
		extends RecognizerBase<STATE> {

	/**
	 * The state that will be used to parse, once the anonymous class has been
	 * verified.
	 */
	private final STATE subParseState;

	/**
	 * Constructor.
	 * 
	 * @param subParseState
	 *            {@link #subParseState}
	 */
	public JavaLikeAnonymousClassRecognizer(STATE subParseState) {
		this.subParseState = subParseState;
	}

	/** {@inheritDoc} */
	@Override
	protected int matchesLocally(ParserState<STATE> parserState,
			List<IToken> tokens, int startOffset) {

		int currentOffset = startOffset;
		if (!TokenStreamUtils.tokenTypesAt(tokens, currentOffset,
				ETokenType.NEW, ETokenType.IDENTIFIER)) {
			return NO_MATCH;
		}
		currentOffset += 2;

		// skip fully qualified names
		while (TokenStreamUtils.tokenTypesAt(tokens, currentOffset,
				ETokenType.DOT, ETokenType.IDENTIFIER)) {
			currentOffset += 2;
		}

		// skip generics specification
		if (TokenStreamUtils.tokenTypesAt(tokens, currentOffset,
				ETokenType.LT)) {
			currentOffset = TokenStreamUtils.findMatchingClosingToken(tokens,
					currentOffset + 1, ETokenType.LT, ETokenType.GT);
			if (currentOffset == TokenStreamUtils.NOT_FOUND) {
				return NO_MATCH;
			}
			currentOffset += 1;
		}

		// expect and skip parentheses
		if (!TokenStreamUtils.tokenTypesAt(tokens, currentOffset,
				ETokenType.LPAREN)) {
			return NO_MATCH;
		}
		currentOffset = TokenStreamUtils.findMatchingClosingToken(tokens,
				currentOffset + 1, ETokenType.LPAREN, ETokenType.RPAREN);
		if (currentOffset == TokenStreamUtils.NOT_FOUND) {
			return NO_MATCH;
		}
		currentOffset += 1;

		if (TokenStreamUtils.tokenTypesAt(tokens, currentOffset,
				ETokenType.LBRACE)) {
			return parserState.parse(subParseState, tokens, startOffset);
		}

		return NO_MATCH;
	}

}
