/*-------------------------------------------------------------------------+
|                                                                          |
Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.shallowparser.languages.python;

import static eu.cqse.check.framework.shallowparser.languages.python.PythonShallowParser.EPythonParserStates.ANY;
import static eu.cqse.check.framework.shallowparser.languages.python.PythonShallowParser.EPythonParserStates.IN_CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.AT;
import static eu.cqse.check.framework.scanner.ETokenType.CLASS;
import static eu.cqse.check.framework.scanner.ETokenType.COLON;
import static eu.cqse.check.framework.scanner.ETokenType.DEDENT;
import static eu.cqse.check.framework.scanner.ETokenType.DEF;
import static eu.cqse.check.framework.scanner.ETokenType.DOT;
import static eu.cqse.check.framework.scanner.ETokenType.ELIF;
import static eu.cqse.check.framework.scanner.ETokenType.ELSE;
import static eu.cqse.check.framework.scanner.ETokenType.EOL;
import static eu.cqse.check.framework.scanner.ETokenType.EXCEPT;
import static eu.cqse.check.framework.scanner.ETokenType.FINALLY;
import static eu.cqse.check.framework.scanner.ETokenType.FOR;
import static eu.cqse.check.framework.scanner.ETokenType.FROM;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.IF;
import static eu.cqse.check.framework.scanner.ETokenType.IMPORT;
import static eu.cqse.check.framework.scanner.ETokenType.INDENT;
import static eu.cqse.check.framework.scanner.ETokenType.LBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.LPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.RBRACK;
import static eu.cqse.check.framework.scanner.ETokenType.RPAREN;
import static eu.cqse.check.framework.scanner.ETokenType.SEMICOLON;
import static eu.cqse.check.framework.scanner.ETokenType.TRY;
import static eu.cqse.check.framework.scanner.ETokenType.WHILE;
import static eu.cqse.check.framework.scanner.ETokenType.WITH;

import java.util.EnumSet;

import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.RecognizerBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowParserBase;
import eu.cqse.check.framework.shallowparser.languages.python.PythonShallowParser.EPythonParserStates;
import org.conqat.lib.commons.region.Region;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Shallow parser for Python.
 *
 * @author $Author: hummelb $
 * @version $Rev: 52657 $
 * @ConQAT.Rating GREEN Hash: 21D8CE326C9F28F3B89B6B6B68151786
 */
public class PythonShallowParser extends ShallowParserBase<EPythonParserStates> {

	/** The states used in this parser. */
	public static enum EPythonParserStates {

		/**
		 * Any state apart from in-class states. Typically, any construct can
		 * occur at any place.
		 */
		ANY,
		
		/** State applying within a class scope. */
		IN_CLASS
	}

	/** Constructor. */
	public PythonShallowParser() {
		super(EPythonParserStates.class, EPythonParserStates.ANY);

		createErrorRules();
		createImportRules();
		createAnnotationRules();
		createClassRules();
		createFunctionRules();
		createStatementRules();
	}

	/** Create rules for handling error handling. */
	private void createErrorRules() {
		// unmatched indent/dedent: no endNode to keep the node incomplete
		inAnyState().sequence(INDENT).createNode(EShallowEntityType.META,
				"Unmatched indent");
		inAnyState().sequence(DEDENT).createNode(EShallowEntityType.META,
				"Unmatched dedent");
	}

	/** Creates parsing rules for imports. */
	private void createImportRules() {
		inAnyState().sequence(EnumSet.of(IMPORT, FROM))
				.createNode(EShallowEntityType.META, 0).skipTo(EOL).endNode();
	}

	/** Creates parsing rules for annotations. */
	private void createAnnotationRules() {
		inAnyState()
				.sequence(AT, IDENTIFIER)
				.repeated(DOT, IDENTIFIER)
				.createNode(EShallowEntityType.META, "annotation",
						new Region(1, -1)).skipNested(LPAREN, RPAREN).endNode();
	}

	/** Creates parsing rules for classes. */
	private void createClassRules() {
		RecognizerBase<EPythonParserStates> classAlternative = inAnyState()
				.sequence(CLASS, IDENTIFIER).skipNested(LPAREN, RPAREN)
				.sequence(COLON)
				.createNode(EShallowEntityType.TYPE, "class", 1);
		addBlockClosingAlternatives(classAlternative, IN_CLASS);
	}

	/** Creates parsing rules for functions. */
	private void createFunctionRules() {
		RecognizerBase<EPythonParserStates> functionAlternative = inAnyState()
				.sequence(DEF, IDENTIFIER).skipNested(LPAREN, RPAREN)
				.sequence(COLON)
				.createNode(EShallowEntityType.METHOD, "method", 1);
		addBlockClosingAlternatives(functionAlternative, ANY);
	}

	/** Creates parsing rules for statements. */
	private void createStatementRules() {
		// empty statement
		inAnyState()
				.sequence(SEMICOLON)
				.createNode(EShallowEntityType.STATEMENT,
						SubTypeNames.EMPTY_STATEMENT).endNode();

		// block statements
		RecognizerBase<EPythonParserStates> ifAlternative = inAnyState()
				.sequence(
						EnumSet.of(IF, ELIF, ELSE, TRY, EXCEPT, FINALLY, WHILE,
								FOR, WITH))
				.createNode(EShallowEntityType.STATEMENT, 0)
				.skipToWithNesting(COLON, LBRACK, RBRACK);
		addBlockClosingAlternatives(ifAlternative, ANY);

		// remove any isolated EOLs
		inAnyState().sequence(EOL);

		// in class attributes
		inState(IN_CLASS).sequenceBefore(IDENTIFIER)
				.subRecognizer(new PythonAttributeRecognizer(), 1, 1).endNode();

		// simple statement
		inAnyState()
				.sequenceBefore(EnumSet.complementOf(EnumSet.of(DEF, CLASS)))
				.subRecognizer(new PythonSimpleStatementRecognizer(), 1, 1)
				.endNode();
	}

	/**
	 * Adds two different rules for closing a block:
	 * <ul>
	 * <li>Closing a block by finding a dedent</li>
	 * <li>Single line that ends with EOL, typically this means multiple
	 * statements on one line</li>
	 * </ul>
	 * 
	 * @param matchingAlternative
	 *            The block recognizer to be closed.
	 * @param innerBlockState
	 *            The {@link EPythonParserStates} used within the block this
	 *            method is closing.
	 */
	private void addBlockClosingAlternatives(
			RecognizerBase<EPythonParserStates> matchingAlternative,
			EPythonParserStates innerBlockState) {
		matchingAlternative.sequence(EOL, INDENT).parseUntil(innerBlockState)
				.sequence(DEDENT).endNode();
		matchingAlternative.parseUntil(innerBlockState).sequence(EOL).endNode();
	}

	/** {@inheritDoc} */
	@Override
	protected boolean isFilteredToken(IToken token, IToken previousToken) {
		if (super.isFilteredToken(token, previousToken)) {
			return true;
		}
		// Don't allow double EOLs
		return previousToken != null && previousToken.getType() == EOL
				&& token.getType() == EOL;
	}

}
