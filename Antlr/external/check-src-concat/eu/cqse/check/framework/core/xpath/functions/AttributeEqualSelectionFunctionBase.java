/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: AttributeEqualSelectionFunctionBase.java 52960 2015-06-18 11:26:01Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import java.util.List;

import org.jaxen.FunctionCallException;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * Base class for functions that compare an attribute of a node to a given input
 * string.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 7C39A05966E23D1A1D4ABEBCEA700B84
 *//*
public abstract class AttributeEqualSelectionFunctionBase extends
		FunctionBase<String> {

	*//** {@inheritDoc} *//*
	@Override
	protected String parseArguments(List<?> args) throws FunctionCallException {
		if (args.size() != 1) {
			throw new FunctionCallException(
					getArgumentSizeInvalidErrorMessage());
		}
		return args.get(0).toString();
	}

	*//** {@inheritDoc} *//*
	@Override
	protected boolean checkNode(ShallowEntity node, String arguments) {
		return getComparisonAttributeValue(node).equals(arguments);
	}

	*//** Returns the value of the attribute that is used for comparison. *//*
	protected abstract String getComparisonAttributeValue(ShallowEntity node);

	*//**
	 * Returns the error message that is displayed if the number of arguments
	 * does not match.
	 *//*
	protected abstract String getArgumentSizeInvalidErrorMessage();
}
*/