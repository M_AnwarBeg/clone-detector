package eu.cqse.check.framework.core.util;

import java.util.EnumSet;

import eu.cqse.check.framework.scanner.ETokenType;

/**
 * Utilities for the custom check framework.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 54015 $
 * @ConQAT.Rating YELLOW Hash: 1EA5D1CEA82D4ECFF38841A5EDE71E7D
 */
public class CheckUtils {

	/** Boolean operator types. */
	public static final EnumSet<ETokenType> BOOLEAN_OPERATOR_TYPES = EnumSet.of(
			ETokenType.ANDAND, ETokenType.OROR, ETokenType.AND, ETokenType.OR,
			ETokenType.XOR);

	/**
	 * String that is used to separate single parts of unique check identifiers.
	 */
	private static final String SEPARATOR = "#:#";

	/** Constructs a unique identifier out of the given group name and name. */
	public static String buildIdentifier(String groupName, String name) {
		StringBuilder builder = new StringBuilder(groupName);
		builder.append(SEPARATOR);
		builder.append(name);
		return builder.toString();
	}
}
