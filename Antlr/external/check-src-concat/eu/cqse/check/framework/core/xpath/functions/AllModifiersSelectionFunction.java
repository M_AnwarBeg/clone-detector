/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: AllModifiersSelectionFunction.java 53939 2015-08-24 08:55:55Z kupka $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function that is querying for nodes, that contain all given modifiers. At
 * the moment this function is similar to {@link AllTokensSelectionFunction} but
 * in the future it can be extended to support different modifier semantics for
 * different languages instead of just checking if a token with the modifier
 * name exists.
 * 
 * @author $Author: kupka $
 * @version $Rev: 53939 $
 * @ConQAT.Rating YELLOW Hash: 937469C7A40B8CF22AD5B5C78E2DDC9C
 *//*
public class AllModifiersSelectionFunction
		extends ModifierSelectionFunctionBase {

	*//** The function's name. *//*
	public static final String NAME = "modifiers";

	*//** Constructor. *//*
	public AllModifiersSelectionFunction() {
		super(NAME);
	}

	*//** {@inheritDoc} *//*
	@Override
	protected boolean checkNode(ShallowEntity node, ETokenType[] arguments) {
		return TokenStreamUtils.containsAll(node.ownStartTokens(), arguments);
	}
}
*/