package eu.cqse.check.framework.core.option;

import java.lang.reflect.Field;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.reflect.ReflectionUtils;

import eu.cqse.check.framework.core.CheckImplementationBase;

/**
 * An option of custom check. Points to an attribute of the check, that
 * represents the option.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 56673 $
 * @ConQAT.Rating GREEN Hash: 7D2985F9FC06FFC32D5906FC99A4EEEB
 */
public class CheckOption<T> {

	/** The annotation the marks the option's field. */
	private final ACheckOption annotation;

	/** The option field. */
	private final Field field;

	/** The option's default value. */
	private final T defaultValue;

	/** The option's type. */
	private final Class<T> type;

	/** Constructor. */
	public CheckOption(ACheckOption annotation, Field field, T defaultValue,
			Class<T> type) {
		CCSMAssert.isNotNull(annotation);
		CCSMAssert.isNotNull(field);
		CCSMAssert.isTrue(ReflectionUtils.isAssignable(type, field.getType()),
				"The given field must be assignable from the given type.");
		this.annotation = annotation;
		this.field = field;
		this.defaultValue = defaultValue;
		this.type = type;
	}

	/** Returns the option's name. */
	public String getName() {
		return annotation.name();
	}

	/** Returns the option's description. */
	public String getDescription() {
		return annotation.description();
	}

	/** Returns whether this option is multiline text. */
	public boolean isMultilineText() {
		return annotation.multilineText();
	}

	/** Returns the option's field. */
	public Field getField() {
		return field;
	}

	/** Returns the option's type. */
	public Class<T> getType() {
		return type;
	}

	/** Returns the option's default value. */
	public T getDefaultValue() {
		return defaultValue;
	}

	/**
	 * Set the option to the given value in the given check implementation
	 * instance.
	 */
	public void setOption(CheckImplementationBase implementation, T value) {
		try {
			field.set(implementation, value);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			CCSMAssert.fail("Failed to set option: " + e.getMessage());
		}
	}
}
