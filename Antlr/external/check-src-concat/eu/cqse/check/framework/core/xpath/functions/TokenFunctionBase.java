/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: TokenFunctionBase.java 52960 2015-06-18 11:26:01Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.enums.EnumUtils;
import org.jaxen.FunctionCallException;

import eu.cqse.check.framework.scanner.ETokenType;

*//**
 * Base class for functions that use an array of {@link ETokenType} as
 * arguments.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: B580BA32AA560C5ECC882FE995DCC362
 *//*
public abstract class TokenFunctionBase extends FunctionBase<ETokenType[]> {

	*//** {@inheritDoc} *//*
	@Override
	protected ETokenType[] parseArguments(List<?> args)
			throws FunctionCallException {
		if (args.size() < 1) {
			throw new FunctionCallException(
					getArgumentSizeInvalidErrorMessage());
		}

		List<ETokenType> tokenTypes = new ArrayList<>();
		for (Object tokenTypeName : args) {
			ETokenType tokenType = EnumUtils.valueOf(ETokenType.class,
					(tokenTypeName.toString()).toUpperCase());
			if (tokenType == null) {
				throw new FunctionCallException("Argument " + tokenTypeName
						+ " is no valid token.");
			}
			tokenTypes.add(tokenType);
		}
		return tokenTypes.toArray(new ETokenType[tokenTypes.size()]);
	}

	*//**
	 * Returns the error message that is displayed if the number of arguments
	 * does not match.
	 *//*
	protected abstract String getArgumentSizeInvalidErrorMessage();
}
*/