/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function that is querying for nodes, that contain any of the given token
 * types.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53930 $
 * @ConQAT.Rating GREEN Hash: C205A466E0C41CA5818A5BD1D11D80CC
 *//*
public class AnyTokenSelectionFunction extends ModifierSelectionFunctionBase {

	*//** The function's name. *//*
	public static final String NAME = "anytoken";

	*//** Constructor. *//*
	public AnyTokenSelectionFunction() {
		super(NAME);
	}

	*//** {@inheritDoc} *//*
	@Override
	protected boolean checkNode(ShallowEntity node, ETokenType[] arguments) {
		return TokenStreamUtils.containsAny(node.ownStartTokens(), arguments);
	}
}*/