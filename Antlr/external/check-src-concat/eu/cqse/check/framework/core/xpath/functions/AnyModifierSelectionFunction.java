/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: AnyModifierSelectionFunction.java 53941 2015-08-24 09:35:55Z kinnen $
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function that is querying for nodes, that contain any of the given
 * modifiers. At the moment this function is similar to
 * {@link AnyTokenSelectionFunction} but in the future it can be extended to
 * support different modifier semantics for different languages instead of just
 * checking if a token with the modifier name exists.
 *
 * @author $Author: kinnen $
 * @version $Rev: 53941 $
 * @ConQAT.Rating GREEN Hash: 3421ABFB3BDFD83B7453F14F75426788
 *//*
public class AnyModifierSelectionFunction
		extends ModifierSelectionFunctionBase {

	*//** The function's name. *//*
	public static final String NAME = "anymodifier";

	*//** Constructor. *//*
	public AnyModifierSelectionFunction() {
		super(NAME);
	}

	*//** {@inheritDoc} *//*
	@Override
	protected boolean checkNode(ShallowEntity node, ETokenType[] arguments) {
		return TokenStreamUtils.containsAny(node.ownStartTokens(), arguments);
	}
}*/