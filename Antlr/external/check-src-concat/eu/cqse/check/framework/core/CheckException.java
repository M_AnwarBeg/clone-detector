package eu.cqse.check.framework.core;


/**
 * An exception that is thrown within the custom check framework.
 *
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 1EF807A906249296E527C39C426BAAE8
 */
public class CheckException extends Exception {

	/** The serial version UID. */
	private static final long serialVersionUID = 1L;

	/** Creates a new CheckException with the given message and cause. */
	public CheckException(String message, Exception cause) {
		super(message, cause);
	}

	/** Creates a new CheckException with the given cause. */
	public CheckException(Exception cause) {
		super(cause);
	}

	/** Creates a new CheckException with the given message. */
	public CheckException(String message) {
		super(message);
	}
}
