package eu.cqse.check.framework.core.registry;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.reflect.ReflectionUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.CheckInfo;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.core.option.CheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.ShallowParserFactory;
import eu.cqse.check.framework.typetracker.TypeTrackerFactory;

/**
 * Class for loading custom checks.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 55558 $
 * @ConQAT.Rating GREEN Hash: F730CF1CC119D0EE64EFEDF34B2F860A
 */
public class CheckLoader {

	/** The class to load a check from. */
	private final Class<?> checkClass;

	/** Constructor. */
	private CheckLoader(Class<?> checkClass) {
		CCSMAssert.isNotNull(checkClass);
		this.checkClass = checkClass;
	}

	/**
	 * Tries to load a check info from the check class that was set in the
	 * constructor. Returns null, if the given check class is not annotated with
	 * {@link ACheck}.
	 * 
	 * @throws CheckException
	 *             if loading fails
	 */
	private CheckInfo load() throws CheckException {
		ACheck checkAnnotation = checkClass.getAnnotation(ACheck.class);
		if (checkAnnotation == null) {
			return null;
		}

		CheckImplementationBase dummyInstance = CheckImplementationBase
				.createInstance(checkClass);

		if (!languagesSupportParameters(checkAnnotation.languages(),
				checkAnnotation.parameters())) {
			throw new CheckException("Unsupported parameters!");
		}

		return new CheckInfo(checkClass, checkAnnotation.name(),
				checkAnnotation.description(), checkAnnotation.groupName(),
				checkAnnotation.defaultEnablement(),
				checkAnnotation.languages(), checkAnnotation.parameters(),
				loadOptions(dummyInstance));
	}

	/** Returns whether the given parameters support the given languages. */
	private static boolean languagesSupportParameters(ELanguage[] languages,
			ECheckParameter[] parameters) {
		for (ELanguage language : languages) {
			for (ECheckParameter parameter : parameters) {
				if (!languageSupportParameter(language, parameter)) {
					return false;
				}
			}
		}
		return true;
	}

	/** Returns whether the given parameter supports the given language. */
	private static boolean languageSupportParameter(ELanguage language,
			ECheckParameter parameter) {
		if (ECheckParameter.ABSTRACT_SYNTAX_TREE.equals(parameter)) {
			return ShallowParserFactory.supportsLanguage(language);
		} else if (ECheckParameter.TYPE_RESOLUTION.equals(parameter)) {
			return TypeTrackerFactory.supportsLanguage(language);
		}
		return false;
	}

	/**
	 * Load options for a check. The given dummy instance is used to retrieve
	 * default values.
	 */
	private Map<String, CheckOption<?>> loadOptions(
			CheckImplementationBase dummyInstance) throws CheckException {
		Map<String, CheckOption<?>> options = new HashMap<>();
		for (Field attribute : ReflectionUtils.getAllFields(checkClass)) {
			CheckOption<?> option = getOptionFromField(attribute, dummyInstance);

			if (option == null) {
				continue;
			}

			if (options.put(option.getName(), option) != null) {
				throw new CheckException("Check " + checkClass.getName()
						+ " has duplicated option: " + option.getName());
			}

		}
		return options;
	}

	/**
	 * Returns a CheckOption for the given field. If the given field is not
	 * annotated with ACheckOption null is returned.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private CheckOption<?> getOptionFromField(Field field,
			CheckImplementationBase dummyInstance) throws CheckException {
		field.setAccessible(true);
		ACheckOption annotation = field.getAnnotation(ACheckOption.class);
		if (annotation == null) {
			return null;
		}

		try {
			Object defaultValue = field.get(dummyInstance);
			if (defaultValue == null) {
				throw new CheckException("Check " + checkClass.getName()
						+ " must have default values for options.");
			}

			Class<?> type = field.getType();
			if (type.isPrimitive()) {
				// If the type is primitive we wrap it into a the
				// corresponding wrapper class, as the whole custom check
				// option framework works with non-primitive types only
				// Writing values back to the attribute works due to
				// auto-boxing.
				type = ReflectionUtils.resolvePrimitiveClass(type);
			}

			return new CheckOption(annotation, field, defaultValue, type);
		} catch (IllegalAccessException e) {
			CCSMAssert
					.fail("Could not access default value: " + e.getMessage());
		}
		return null;
	}

	/**
	 * Tries to load a check info from the given check class. Returns null, if
	 * the given check class is not annotated with {@link ACheck}.
	 * 
	 * @throws CheckException
	 *             if loading fails
	 */
	public static CheckInfo loadFromClass(Class<?> checkClass)
			throws CheckException {
		return new CheckLoader(checkClass).load();
	}
}
