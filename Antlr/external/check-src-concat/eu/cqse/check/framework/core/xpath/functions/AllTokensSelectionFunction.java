/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: AllTokensSelectionFunction.java 53930 2015-08-24 07:42:03Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function that is querying for nodes, that contain all given token types.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53930 $
 * @ConQAT.Rating GREEN Hash: FFD641E0C384B46E51F117B295075DD5
 *//*
public class AllTokensSelectionFunction extends TokenSelectionFunctionBase {

	*//** The function's name. *//*
	public static final String NAME = "tokens";

	*//** Constructor. *//*
	public AllTokensSelectionFunction() {
		super(NAME);
	}

	*//** {@inheritDoc} *//*
	@Override
	protected boolean checkNode(ShallowEntity node, ETokenType[] arguments) {
		return TokenStreamUtils.containsAll(node.ownStartTokens(), arguments);
	}
}
*/