/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.core;

/*import org.conqat.lib.commons.assessment.ETrafficLightColor;*/

/**
 * The possible values for enabling/controlling a finding.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 070E1333221E10AFF38274B98FF1E327
 */
public enum EFindingEnablement {

	/** The findings are not shown. */
	OFF,

	/** The findings are rated as {@link ETrafficLightColor#YELLOW}. */
	YELLOW,

	/** The findings are rated as {@link ETrafficLightColor#RED}. */
	RED,

	/** Enabled and the color is determined by other means, such as thresholds. */
	AUTO;
}
