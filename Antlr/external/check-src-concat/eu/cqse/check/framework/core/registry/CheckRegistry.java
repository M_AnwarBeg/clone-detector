package eu.cqse.check.framework.core.registry;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import org.apache.log4j.Logger;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.filesystem.ClassPathUtils;
import org.conqat.lib.commons.filesystem.FileExtensionFilter;
import org.conqat.lib.commons.filesystem.FileSystemUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.CheckInfo;
import eu.cqse.check.framework.core.CheckInstance;

/**
 * Registry singleton that manages custom checks.
 *
 * @author $Author: kinnen $
 * @version $Rev: 55127 $
 * @ConQAT.Rating YELLOW Hash: 906DDC4A55D2ABE11B406490C215553C
 */
public class CheckRegistry {

	/** Logger instance. */
	//private static final Logger LOGGER = Logger.getLogger(CheckRegistry.class);

	/** The singleton instance. */
	private static CheckRegistry instance = null;

	/** Mapping from check identifiers to the check information. */
	private final Map<String, CheckInfo> checksInfo = new HashMap<>();

	/** Private constructor to avoid multiple instantiation. */
	private CheckRegistry() {
		// do nothing
	}

	/** Returns the singleton instance. */
	public static synchronized CheckRegistry getInstance() {
		if (instance == null) {
			instance = new CheckRegistry();
		}
		return instance;
	}

	/**
	 * Registers the given bundle and loads all classes from the given bundle
	 * that implement {@link CheckImplementationBase} and are annotated with
	 * {@link ACheck}.
	 */
	public void registerCheckClassDirectory(File classesDirectory) {
		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		List<String> classNames = ClassPathUtils
				.getClassNames(classesDirectory);
		for (String className : classNames) {
			try {
				loadCheckFromClass(classLoader.loadClass(className));
			} catch (ClassNotFoundException e) {
				/*LOGGER.error("Failed to load check from class: " + className,
						e);*/
			}
		}
	}

	/**
	 * Creates and registers a custom check instance from the given class. If
	 * the given class does not contain a custom check (class not annotated with
	 * {@link ACheck}), this method does nothing.
	 */
	private void loadCheckFromClass(Class<?> clazz) {
		CheckInfo checkInfo = null;
		try {
			checkInfo = CheckLoader.loadFromClass(clazz);
		} catch (CheckException e) {
			/*LOGGER.error(e.getMessage(), e);*/
			return;
		}

		if (checkInfo == null) {
			return;
		}

		if (checksInfo.containsKey(checkInfo.getIdentifier())) {
			/*LOGGER.error("Check " + checkInfo.getGroupName() + ":"
					+ checkInfo.getName() + " is already registered.");*/
			return;
		}
		checksInfo.put(checkInfo.getIdentifier(), checkInfo);
	}

	/**
	 * Creates check instances for the given check identifiers.
	 *
	 * @throws CheckException
	 *             if there is no check for one of the given identifiers
	 */
	public Collection<CheckInstance> instantiateChecks(List<String> identifiers)
			throws CheckException {
		List<CheckInstance> instances = new ArrayList<>();
		for (String identifier : identifiers) {
			CheckInfo checkInfo = checksInfo.get(identifier);
			if (checkInfo == null) {
				throw new CheckException("No check with identifier '"
						+ identifier + "' exists.");
			}
			instances.add(new CheckInstance(checkInfo));
		}
		return instances;
	}

	/** Returns a list of all registered checks. */
	public Collection<CheckInfo> getChecksInfos() {
		return CollectionUtils.asUnmodifiable(checksInfo.values());
	}

	/** Registers the given directory of custom checks bundled as JAR files. */
	public void registerCheckDirectory(File customChecksDirectory) {
		if (!customChecksDirectory.canRead()) {
			/*LOGGER.error("Cannot read from custom check directory: "
					+ customChecksDirectory.getAbsolutePath());*/
			return;
		}
		List<File> jarFiles = FileSystemUtils.listFilesRecursively(
				customChecksDirectory, new FileExtensionFilter("jar"));
		for (File jarFile : jarFiles) {
			if (jarFile.isFile()) {
				loadChecksFromJAR(jarFile);
			}
		}

	}

	/** Loads custom checks from the given JAR file. */
	private void loadChecksFromJAR(File jarFile) {
		ClassLoader contextClassLoader = Thread.currentThread()
				.getContextClassLoader();
		try (URLClassLoader classLoader = new URLClassLoader(
				new URL[] { jarFile.toURI().toURL() }, contextClassLoader)) {
			List<String> classes = FileSystemUtils
					.listTopLevelClassesInJarFile(jarFile);
			for (String clazz : classes) {
				loadCheckFromClass(classLoader.loadClass(clazz));
			}
		} catch (IOException | ClassNotFoundException e) {
			/*LOGGER.error("Error loading custom checks from '" + jarFile + "': "
					+ e.toString(), e);*/
		}
	}
}
