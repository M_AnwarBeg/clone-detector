/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: NameMatchesFunction.java 53154 2015-07-02 14:50:26Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.jaxen.FunctionCallException;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function for matching an {@link ShallowEntity}'s name against a set of
 * regular expressions.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53154 $
 * @ConQAT.Rating GREEN Hash: 10B973DF552E3ED858DF6B59684A4765
 *//*
public class NameMatchesFunction extends FunctionBase<Pattern> {

	*//** The function's name. *//*
	public final static String NAME = "name-matches";

	*//** {@inheritDoc} *//*
	@Override
	protected Pattern parseArguments(List<?> args) throws FunctionCallException {
		if (args.size() != 1) {
			throw new FunctionCallException(NAME
					+ "(<regex>) expects exactly one argument.");
		}
		String regex = args.get(0).toString();
		try {
			return Pattern.compile(regex);
		} catch (PatternSyntaxException e) {
			throw new FunctionCallException("'" + regex
					+ "' is not a valid regular expression.");
		}
	}

	*//** {@inheritDoc} *//*
	@Override
	protected boolean checkNode(ShallowEntity node, Pattern arguments) {
		return node.getName() != null
				&& arguments.matcher(node.getName()).matches();
	}
}
*/