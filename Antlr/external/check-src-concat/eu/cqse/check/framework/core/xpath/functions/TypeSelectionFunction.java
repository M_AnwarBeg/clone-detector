/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: TypeSelectionFunction.java 52960 2015-06-18 11:26:01Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function for querying the type of a {@link ShallowEntity}.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 2C42DDFB45093D9387A6B6CD6E1ECBFE
 *//*
public class TypeSelectionFunction extends AttributeEqualSelectionFunctionBase {

	*//** The function's name. *//*
	public final static String NAME = "type";

	*//** {@inheritDoc} *//*
	@Override
	protected String getComparisonAttributeValue(ShallowEntity node) {
		return node.getType().name();
	}

	*//** {@inheritDoc} *//*
	@Override
	protected String getArgumentSizeInvalidErrorMessage() {
		return NAME + "(<typeName>) expects exactly one argument.";
	}
}
*/