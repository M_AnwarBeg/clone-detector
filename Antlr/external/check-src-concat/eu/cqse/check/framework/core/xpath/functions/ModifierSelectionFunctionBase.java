/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: ModifierSelectionFunctionBase.java 52960 2015-06-18 11:26:01Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import org.conqat.lib.commons.assertion.CCSMAssert;

*//**
 * Base class for functions that use modifiers for node selection.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: F2E5CCE44CE6BDB7A3388E9EAD79D89C
 *//*
public abstract class ModifierSelectionFunctionBase extends TokenFunctionBase {

	*//** The function's name. *//*
	private final String name;

	*//** Constructor. *//*
	public ModifierSelectionFunctionBase(String name) {
		CCSMAssert.isNotNull(name);
		this.name = name;
	}

	*//** {@inheritDoc} *//*
	@Override
	protected String getArgumentSizeInvalidErrorMessage() {
		return name + "(modifier,[modifier]*) expects at least one argument.";
	}
}
*/