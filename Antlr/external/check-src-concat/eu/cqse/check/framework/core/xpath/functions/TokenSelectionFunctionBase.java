/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import org.conqat.lib.commons.assertion.CCSMAssert;

*//**
 * Base class for functions that use tokens for node selection.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53930 $
 * @ConQAT.Rating GREEN Hash: C7460D02985E1AFD89F36BD12F0E700A
 *//*
public abstract class TokenSelectionFunctionBase extends TokenFunctionBase {

	*//** The function's name. *//*
	private final String name;

	*//** Constructor. *//*
	public TokenSelectionFunctionBase(String name) {
		CCSMAssert.isNotNull(name);
		this.name = name;
	}

	*//** {@inheritDoc} *//*
	@Override
	protected String getArgumentSizeInvalidErrorMessage() {
		return name + "(token,[token]*) expects at least one argument.";
	}
}*/