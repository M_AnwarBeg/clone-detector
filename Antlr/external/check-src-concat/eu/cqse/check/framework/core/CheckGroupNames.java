/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.core;

/**
 * A collection of group names that are used by the built-in checks.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57424 $
 * @ConQAT.Rating GREEN Hash: 10525C545754325AC7FCE048CDE2BF22
 */
public class CheckGroupNames {

	/** The bad practice group name */
	public static final String BAD_PRACTICE = "Bad practice";

	/** The correctness group name */
	public static final String CORRECTNESS = "Correctness";

	/** The Unused code group name */
	public static final String UNUSED_CODE = "Unused code";

	/** The Performance code group name */
	public static final String PERFORMANCE = "Performance";

	/** The Test conventions group name */
	public static final String TEST_CONVENTIONS = "Test Conventions";
}
