package eu.cqse.check.framework.core;

/**
 * An enumeration of all parameters that can be passed to a custom check.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 55558 $
 * @ConQAT.Rating GREEN Hash: AED7C96EE5F0EA0375D64F389684B357
 */
public enum ECheckParameter {

	/**
	 * Parameter for a list of shallow entities that represent the abstract
	 * syntax tree of the token element that is currently analyzed.
	 */
	ABSTRACT_SYNTAX_TREE,

	/**
	 * Parameter for type resolution that can be applied to shallow entities.
	 */
	TYPE_RESOLUTION;
}
