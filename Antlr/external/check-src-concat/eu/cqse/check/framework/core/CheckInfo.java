package eu.cqse.check.framework.core;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.core.option.CheckOption;
import eu.cqse.check.framework.core.util.CheckUtils;
import eu.cqse.check.framework.scanner.ELanguage;

/**
 * A check info contains information about a custom check.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53089 $
 * @ConQAT.Rating GREEN Hash: 3414EADC0BC13568D33484A4CD475B29
 */
public class CheckInfo {

	/** The check's class. */
	private final Class<?> checkClass;

	/** The name. */
	private final String name;

	/** The description. */
	private final String description;

	/** The analysis-group name. */
	private final String groupName;

	/** The default enablement. */
	private final EFindingEnablement defaultEnablement;

	/** The supported languages. */
	private final Set<ELanguage> languages;

	/** The parameters that must be provided to this check. */
	private final Set<ECheckParameter> parameters;

	/** The options that can be configured. */
	private final Map<String, CheckOption<?>> options;

	/**
	 * Creates a new check info with the given parameters. None of them must be
	 * null.
	 */
	public CheckInfo(Class<?> checkClass, String name, String description,
			String groupName, EFindingEnablement defaultEnablement,
			ELanguage[] languages, ECheckParameter[] parameters,
			Map<String, CheckOption<?>> options) {
		CCSMAssert.isNotNull(checkClass);
		CCSMAssert.isNotNull(name);
		CCSMAssert.isNotNull(description);
		CCSMAssert.isNotNull(groupName);
		CCSMAssert.isNotNull(defaultEnablement);
		CCSMAssert.isNotNull(languages);
		CCSMAssert.isNotNull(parameters);
		CCSMAssert.isNotNull(options);

		this.checkClass = checkClass;

		this.name = name;
		this.description = description;
		this.groupName = groupName;
		this.defaultEnablement = defaultEnablement;
		this.languages = CollectionUtils.asHashSet(languages);
		this.parameters = EnumSet.noneOf(ECheckParameter.class);
		this.parameters.addAll(Arrays.asList(parameters));
		this.options = new HashMap<String, CheckOption<?>>(options);
	}

	/**
	 * Creates a new instance of the underlying check implementation.
	 */
	public CheckImplementationBase instantiateCheckImplementation()
			throws CheckException {
		return CheckImplementationBase.createInstance(checkClass);
	}

	/** Returns the name. */
	public String getName() {
		return name;
	}

	/** Returns the description. */
	public String getDescription() {
		return description;
	}

	/** Returns the analysis-group name. */
	public String getGroupName() {
		return groupName;
	}

	/** Returns the default enablement. */
	public EFindingEnablement getDefaultEnablement() {
		return defaultEnablement;
	}

	/** Returns the supported languages. */
	public Set<ELanguage> getSupportedLanguages() {
		return languages;
	}

	/** Returns the parameters that must be provided to this check. */
	public Set<ECheckParameter> getParameters() {
		return parameters;
	}

	/** Returns all options that can be provided to this check. */
	public Map<String, CheckOption<?>> getOptions() {
		return CollectionUtils.asUnmodifiable(options);
	}

	/** Returns a unique identifier for this check. */
	public String getIdentifier() {
		return CheckUtils.buildIdentifier(groupName, name);
	}

	/** Returns the simple class name of the check. */
	public String getSimpleClassName() {
		return checkClass.getSimpleName();
	}
}