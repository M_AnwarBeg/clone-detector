package eu.cqse.check.framework.core.option;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that is used to mark attributes of custom checks as options.
 * 
 * @author $Author: kupka $
 * @version $Rev: 56579 $
 * @ConQAT.Rating GREEN Hash: ED630BA81EE7AF8523E6F50DF7529DC9
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ACheckOption {

	/** The option's name. */
	String name();

	/** The option's description. */
	String description();

	/**
	 * Whether this option is multiline text (rendering advice for configuration
	 * UI).
	 */
	boolean multilineText() default false;
}
