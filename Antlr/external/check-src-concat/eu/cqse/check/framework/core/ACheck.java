package eu.cqse.check.framework.core;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import eu.cqse.check.framework.scanner.ELanguage;

/**
 * Annotation that marks a class as custom check and provides some meta-data for
 * it.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 9DE2892E27CFFEC330877FE0C866AFC9
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ACheck {

	/** Returns the name. */
	String name();

	/** Returns the description. */
	String description();

	/** Returns the analysis-group name. */
	String groupName();

	/** Returns the default enablement. */
	EFindingEnablement defaultEnablement() default EFindingEnablement.YELLOW;

	/** Returns all supported languages. */
	ELanguage[] languages();

	/** Returns all parameters that must be provided to the check. */
	ECheckParameter[] parameters() default {};
}
