/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: SubTypeSelectionFunction.java 52960 2015-06-18 11:26:01Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * A function for querying the subType of a {@link ShallowEntity}.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 867D347A2305F42F4CA59E627686228C
 *//*
public class SubTypeSelectionFunction extends
		AttributeEqualSelectionFunctionBase {

	*//** The function's name. *//*
	public final static String NAME = "subtype";

	*//** {@inheritDoc} *//*
	@Override
	protected String getComparisonAttributeValue(ShallowEntity node) {
		return node.getSubtype();
	}

	*//** {@inheritDoc} *//*
	@Override
	protected String getArgumentSizeInvalidErrorMessage() {
		return NAME + "(<subTypeName>) expects exactly one argument.";
	}
}
*/