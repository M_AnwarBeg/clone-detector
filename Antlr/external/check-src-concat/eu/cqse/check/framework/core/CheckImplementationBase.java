package eu.cqse.check.framework.core;

import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
/*import org.jaxen.BaseXPath;
import org.jaxen.JaxenException;
*/
//import eu.cqse.check.framework.core.xpath.CheckXPathUtils;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Abstract base class for custom checks. A check is instantiated and
 * initialized with a check context when its executing processor is set up. It
 * is destroyed, when the executing processor has finished execution.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 55561 $
 * @ConQAT.Rating GREEN Hash: 3C32B17AF80D764EA1705ED77F68A769
 */
public abstract class CheckImplementationBase {

	/**
	 * The check context that is used to access custom check specific
	 * functionality.
	 */
	protected ICheckContext context = null;

	/**
	 * Sets the context. Must not be called if the context has already been set.
	 */
	public void setContext(ICheckContext context) {
		CCSMAssert.isTrue(this.context == null,
				"The context has already been set.");
		this.context = context;
	}

	/** Initializes the custom check. */
	public void initialize() {
		// empty dummy implementation
	}

	/** Executes the custom check with the given context. */
	public abstract void execute() throws CheckException;

	/**
	 * Selects all shallow entities from the abstract syntax tree, that match
	 * the given xpath-expression. This method can be used, if the executing
	 * check requests the {@link ECheckParameter#ABSTRACT_SYNTAX_TREE}
	 * parameter.
	 */
	/*protected List<ShallowEntity> select(String xPathExpression)
			throws CheckException {
		return select(context.getRootEntity(), xPathExpression);
	}*/

	/**
	 * Selects all shallow entities within the given entity, that match the
	 * given xpath-expression.
	 */
	/*@SuppressWarnings("unchecked")
	protected List<ShallowEntity> select(ShallowEntity entity,
			String xPathExpression) throws CheckException {
		BaseXPath xPath = CheckXPathUtils.getXPath(xPathExpression);
		try {
			Object result = xPath.evaluate(entity);
			if (result instanceof List<?>) {
				return (List<ShallowEntity>) result;
			}
			throw new CheckException(
					"Evaluating the xPath expression '" + xPathExpression
							+ "' returned unexpected result: " + result);
		} catch (JaxenException e) {
			throw new CheckException(e);
		}
	}*/

	/**
	 * Creates a finding with the given message for the whole analyzed token
	 * element.
	 */
	protected void createFinding(String message) throws CheckException {
		context.createFindingForElement(message);
	}

	/** Creates a finding with the given message at the given token. */
	protected void createFinding(String message, IToken token)
			throws CheckException {
		createFinding(message, token, token);
	}

	/**
	 * Creates a finding with the given message beginning from startToken to
	 * endToken.
	 */
	protected void createFinding(String message, IToken startToken,
			IToken endToken) throws CheckException {
		context.createFindingForOffsets(message, startToken.getOffset(),
				endToken.getEndOffset());

	}

	/** Creates a finding with the given message for the given entity. */
	protected void createFinding(String message, ShallowEntity entity)
			throws CheckException {
		context.createFindingForOffsets(message, entity.getStartOffset(),
				entity.getEndOffset());
	}

	/**
	 * Creates a finding with the given message on the first line of the given
	 * entity.
	 */
	protected void createFindingOnFirstLine(String message,
			ShallowEntity entity) throws CheckException {
		createFinding(message, entity.getStartLine());
	}

	/** Creates a finding with the given message at the given line. */
	protected void createFinding(String message, int line)
			throws CheckException {
		context.createFindingForLines(message, line, line);
	}

	/**
	 * Creates a finding with the given message from the given start line
	 * (inclusive) to the given end line (inclusive).
	 */
	protected void createFinding(String message, int startLine, int endLine)
			throws CheckException {
		context.createFindingForLines(message, startLine, endLine);
	}

	/** Creates an instance of the given check class. */
	public static CheckImplementationBase createInstance(Class<?> checkClass)
			throws CheckException {
		CheckImplementationBase implementation = null;
		try {
			implementation = (CheckImplementationBase) checkClass.newInstance();
		} catch (InstantiationException e) {
			throw new CheckException("Check class " + checkClass.getName()
					+ " does not provide a default constructor.", e);
		} catch (IllegalAccessException e) {
			throw new CheckException("Check class " + checkClass.getName()
					+ "'s default constructor is not visible.", e);
		} catch (ClassCastException e) {
			throw new CheckException("Check class " + checkClass.getName()
					+ " does not implement ICheckImplementation.", e);
		}
		return implementation;
	}
}
