package eu.cqse.check.framework.core;

import java.util.List;

import org.conqat.lib.commons.logging.ILogger;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.ITypeResolution;

/**
 * Context interface for custom checks, that provides custom check related
 * functionality to a custom check.
 *
 * @author $Author: hummelb $
 * @version $Rev: 56672 $
 * @ConQAT.Rating GREEN Hash: 6536FBB282FD74A674C0F9EBBBEBEF02
 */
public interface ICheckContext {

	/** Returns the language of the analyzed token element. */
	public ELanguage getLanguage();

	/** Returns the uniform path of the analyzed token element. */
	public String getUniformPath();

	/** Returns the tokens of the analyzed token element. */
	public List<IToken> getTokens() throws CheckException;

	/**
	 * Returns the text content of the analyzed token element. The returned
	 * content is filtered, e.g. generated code is <b>not</b> included. For most
	 * checks this will be the preferred form of the content, as usually no
	 * findings shall be generated for filtered code.
	 */
	public String getTextContent() throws CheckException;

	/**
	 * Returns the <b>unfiltered</b> text context of the analyzed token element.
	 * The returned content may include generated code. In general, checks
	 * should use {@link #getTextContent()} and only very specific checks should
	 * need to access the raw content.
	 */
	public String getUnfilteredTextContent() throws CheckException;

	/**
	 * Returns the synthetic root entity of the abstract syntax tree. This
	 * method can be used, if the executing check requests the
	 * {@link ECheckParameter#ABSTRACT_SYNTAX_TREE} parameter.
	 */
	public ShallowEntity getRootEntity() throws CheckException;

	/**
	 * Returns the synthetic root entity of the abstract syntax tree based on
	 * the unfiltered content (see also {@link #getUnfilteredTextContent()}).
	 * This method can be used, if the executing check requests the
	 * {@link ECheckParameter#ABSTRACT_SYNTAX_TREE} parameter.
	 */
	public ShallowEntity getUnfilteredRootEntity() throws CheckException;

	/**
	 * Returns the abstract syntax tree of the analyzed token element as list of
	 * shallow entities. This method can be used, if the executing check
	 * requests the {@link ECheckParameter#ABSTRACT_SYNTAX_TREE} parameter.
	 */
	public List<ShallowEntity> getAbstractSyntaxTree() throws CheckException;

	/**
	 * Returns a type resolution, which contains information about known
	 * variables and their types. This method can be used, if the executing
	 * check request the {@link ECheckParameter#TYPE_RESOLUTION} parameter.
	 */
	public ITypeResolution getTypeResolution() throws CheckException;

	/**
	 * Returns a logger, which may be used to indicate problems in situations
	 * where the check may continue. In more severe cases, a
	 * {@link CheckException} should be thrown instead.
	 */
	public ILogger getLogger();

	/**
	 * Creates a finding with the given message for the whole analyzed token
	 * element.
	 */
	public void createFindingForElement(String message) throws CheckException;

	/**
	 * Creates a finding with the given message from the given start line
	 * (inclusive) to the given end line (inclusive).
	 */
	public void createFindingForLines(String message, int startLine,
			int endLine) throws CheckException;

	/**
	 * Creates a finding with the given message from the given start offset
	 * (inclusive) to the given end offset (inclusive).
	 */
	public void createFindingForOffsets(String message, int startOffset,
			int endOffset) throws CheckException;

	/**
	 * Returns the abstract syntax tree based on the unfiltered text content.
	 * (see also {@link #getUnfilteredTextContent()})
	 */
	public List<ShallowEntity> getUnfilteredAbstractSyntaxTree()
			throws CheckException;

	/**
	 * Returns the type resolution based on the unfiltered text content. (see
	 * also {@link #getUnfilteredTextContent()})
	 */
	public ITypeResolution getUnfilteredTypeResolution() throws CheckException;

	/**
	 * Returns the list of tokens based on the unfiltered text content. (see
	 * also {@link #getUnfilteredTextContent()})
	 */
	public List<IToken> getUnfilteredTokens();
}
