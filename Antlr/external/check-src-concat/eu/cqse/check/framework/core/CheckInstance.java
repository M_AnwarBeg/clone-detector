package eu.cqse.check.framework.core;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.reflect.ReflectionUtils;

import eu.cqse.check.framework.core.option.CheckOption;

/**
 * A instance of a custom check. Multiple instances of one check can exist in
 * order to be executed in parallel by multiple worker threads.
 * 
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 3266AD358A170CFCE472D68343FEA493
 */
public class CheckInstance {

	/** The check's implementation. */
	private final CheckImplementationBase checkImplementation;

	/** The check info. */
	private final CheckInfo checkInfo;

	/**
	 * Creates a new check instance with the given check info. This given check
	 * info must not be null.
	 */
	public CheckInstance(CheckInfo checkInfo) throws CheckException {
		CCSMAssert.isNotNull(checkInfo);
		this.checkInfo = checkInfo;
		this.checkImplementation = checkInfo.instantiateCheckImplementation();
	}

	/** Initializes the check. */
	public void initialize() {
		checkImplementation.initialize();
	}

	/** Executes this check with the given context. */
	public void execute() throws CheckException {
		checkImplementation.execute();
	}

	/** @see #checkInfo */
	public CheckInfo getCheckInfo() {
		return checkInfo;
	}

	/**
	 * Sets the check context of the underlying check implementation to the
	 * given one.
	 */
	public void setContext(ICheckContext context) {
		CCSMAssert.isNotNull(context);
		checkImplementation.setContext(context);
	}

	/** Sets the option with the given name to the given value. */
	public <T> void setOption(String name, T value) {
		CheckOption<?> option = checkInfo.getOptions().get(name);
		if (option != null) {
			CCSMAssert
					.isTrue(ReflectionUtils.isAssignable(value.getClass(),
							option.getType()),
							"The given value must have the same type as the option to set.");
			@SuppressWarnings("unchecked")
			CheckOption<T> castedOption = (CheckOption<T>) option;
			castedOption.setOption(checkImplementation, value);
		}
	}
}
