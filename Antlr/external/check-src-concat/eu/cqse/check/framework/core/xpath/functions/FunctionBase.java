/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: FunctionBase.java 52960 2015-06-18 11:26:01Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------
package eu.cqse.check.framework.core.xpath.functions;

import java.util.ArrayList;
import java.util.List;

import org.jaxen.Context;
import org.jaxen.Function;
import org.jaxen.FunctionCallException;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

*//**
 * Base class for functions that convert input arguments to a given type and
 * then check for each {@link ShallowEntity}-node, if it should be kept in the
 * node set.
 * 
 * @param <T>
 *            the converted argument type
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52960 $
 * @ConQAT.Rating GREEN Hash: 720E01317AADF168A2B43E9D83A7061D
 *//*
public abstract class FunctionBase<T> implements Function {

	*//** {@inheritDoc} *//*
	@SuppressWarnings("unchecked")
	@Override
	public Object call(Context context, @SuppressWarnings("rawtypes") List args)
			throws FunctionCallException {
		T parsedArguments = parseArguments(args);

		List<ShallowEntity> elements = new ArrayList<>();
		for (ShallowEntity node : (List<ShallowEntity>) context.getNodeSet()) {
			if (checkNode(node, parsedArguments)) {
				elements.add(node);
			}
		}
		return elements;
	}

	*//** Converts the input arguments. *//*
	protected abstract T parseArguments(List<?> args)
			throws FunctionCallException;

	*//** Checks if the given node should be kept in the node set. *//*
	protected abstract boolean checkNode(ShallowEntity node, T arguments)
			throws FunctionCallException;
}
*/