/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * A scoped type lookup is an associative data structure that maps variable
 * names to their type information. A scoped type lookup can create child
 * lookups, that can shadow variables from the parent. Further imported
 * namespaces are stored.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52958 $
 * @ConQAT.Rating YELLOW Hash: 0EBEC6D9DE07057FADBF34ED55B5847D
 */
public class ScopedTypeLookup {

	/** Set of imported namespaces. */
	private Set<String> importedNamespaces;

	/** An internal lookup, that maps variable names to their type information. */
	private Map<String, TypedVariable> variableTypeInfos;

	/** The parent lookup or null if this is the root lookup. */
	private ScopedTypeLookup parent;

	/** Creates a new lookup with no parent. */
	public ScopedTypeLookup() {
		this(null);
	}

	/**
	 * Creates a new lookup with the given parent. This constructor is only used
	 * internally. Child-creation is done with {@link #createChildScope()}.
	 */
	private ScopedTypeLookup(ScopedTypeLookup parent) {
		this.parent = parent;
		this.importedNamespaces = new HashSet<String>();
		this.variableTypeInfos = new HashMap<>();
	}

	/** Returns the parent lookup or null if this is the root lookup. */
	public ScopedTypeLookup getParent() {
		return parent;
	}

	/** Creates a new lookup, with this lookup as parent. */
	public ScopedTypeLookup createChildScope() {
		return new ScopedTypeLookup(this);
	}

	/** Returns a set of all existing keys. */
	public Set<String> getAllVariables() {
		Set<String> variableSet = new HashSet<>(variableTypeInfos.keySet());
		if (parent != null) {
			variableSet.addAll(parent.getAllVariables());
		}
		return variableSet;
	}

	/**
	 * Associates the given variable with the given type information in this
	 * lookup. Returns the old type information or null if none was set before.
	 */
	public TypedVariable putTypeInfo(String variableName, TypedVariable typeInfo) {
		return variableTypeInfos.put(variableName, typeInfo);
	}

	/**
	 * Returns the type information for the given key. If the key is not found
	 * in this scoped lookup, the parent lookups will be searched recursively.
	 * If the key does not exist, null is returned.
	 */
	public TypedVariable getTypeInfo(String variableName) {
		TypedVariable value = variableTypeInfos.get(variableName);
		if (value == null && parent != null) {
			value = parent.getTypeInfo(variableName);
		}
		return value;
	}

	/** Adds an imported namespace to the scoped lookup. */
	public boolean addImportedNamespace(String namespace) {
		return importedNamespaces.add(namespace);
	}

	/** Returns a set of all imported namespaces. */
	public Set<String> getImportedNamespaces() {
		Set<String> namespaces = new HashSet<String>(importedNamespaces);
		if (parent != null) {
			namespaces.addAll(parent.getImportedNamespaces());
		}
		return namespaces;
	}

	/**
	 * Returns whether this lookup contains information about the given
	 * variable.
	 */
	public boolean containsVariable(String variableName) {
		return getTypeInfo(variableName) != null;
	}

	/**
	 * Returns whether this lookup contains information about a variable with
	 * the given variable that was defined in the given entity.
	 */
	public boolean containsVariableWithEntity(String variableName,
			ShallowEntity definitionEntity) {
		TypedVariable typeInfo = variableTypeInfos.get(variableName);
		if (typeInfo != null
				&& typeInfo.getDeclaringEntity().equals(definitionEntity)) {
			return true;
		}
		if (parent != null) {
			return parent.containsVariableWithEntity(variableName,
					definitionEntity);
		}
		return false;
	}

	/**
	 * Returns whether the variable with the given name is shadowing the
	 * variable that was defined by the given entity.
	 */
	public boolean isShadowingVariable(String variableName,
			ShallowEntity variableEntity) {
		if (parent == null) {
			return false;
		}
		if (variableTypeInfos.containsKey(variableName)) {
			return parent.containsVariableWithEntity(variableName,
					variableEntity);
		}
		return parent.isShadowingVariable(variableName, variableEntity);
	}

	/**
	 * Returns whether the given variable has the type with the given full
	 * qualified type name.
	 */
	public boolean hasFullQualifiedTypeName(String variableName,
			String fullQualifiedTypeName) {
		TypedVariable info = getTypeInfo(variableName);
		if (info == null) {
			throw new IllegalArgumentException(
					"Type information for the given variableName does not exist.");
		}

		String typeName = info.getTypeName();
		if (typeName.equals(fullQualifiedTypeName)) {
			return true;
		}

		Set<String> namespaces = getImportedNamespaces();
		for (String namespace : namespaces) {
			String potentialFullQualifiedTypeName = namespace + "." + typeName;
			if (potentialFullQualifiedTypeName.equals(fullQualifiedTypeName)) {
				return true;
			}
		}

		return false;
	}
}
