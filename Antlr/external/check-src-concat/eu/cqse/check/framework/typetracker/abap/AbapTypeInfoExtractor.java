/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker.abap;

import static eu.cqse.check.framework.scanner.ETokenType.BEGIN;
import static eu.cqse.check.framework.scanner.ETokenType.GT;
import static eu.cqse.check.framework.scanner.ETokenType.IDENTIFIER;
import static eu.cqse.check.framework.scanner.ETokenType.LT;
import static eu.cqse.check.framework.scanner.ETokenType.OF;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.Pair;
import org.conqat.lib.commons.logging.ILogger;

import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.TypeInfoExtractorBase;
import eu.cqse.check.framework.typetracker.TypedVariable;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * Extracts type information from ABAP shallow entities that are either
 * statements, methods or attributes. Other shallow entity types don't introduce
 * any type information in ABAP.
 * 
 * The {@link AbapTypeInfoExtractor} requires a reference to the rootEntities,
 * because it needs to have access to the whole file in order to extract all
 * type infos. Methods in ABAP are the reason for this, because their parameters
 * are not declared in the same shallow entity as the method implementation. If
 * no rootEntities are given the corresponding methods return an empty list of
 * types when called.
 * 
 * Please note that the extractor does not handle colon-statements (e.g.
 * <code>data: foo type i, bar type i.</code>) at all. These need to be
 * "rolled out" into separate statements on the token level before parsing and
 * subsequently feeding the obtained shallow entities to the extractor.
 * 
 * @author $Author: streitel $
 * @version $Rev: 57634 $
 * @ConQAT.Rating GREEN Hash: D3A93C671DCD347E60DEB54EBBA82522
 */
public class AbapTypeInfoExtractor extends TypeInfoExtractorBase {

	/**
	 * Return value used as type for a structured datatype, aka record datatype.
	 */
	public static final String STRUCTURED_DATATYPE = "Structured Datatype";

	/**
	 * Names of sub types in which variables can be declared.
	 */
	private static final Set<String> VARIABLE_DECLARATION_SUBTYPES = CollectionUtils
			.asHashSet(SubTypeNames.CLASS_DATA, SubTypeNames.CONSTANTS,
					SubTypeNames.DATA, SubTypeNames.FIELD_SYMBOLS,
					SubTypeNames.PARAMETERS, SubTypeNames.STATICS);

	/**
	 * Holds all shallow entities of the current file. May be null e.g. when
	 * called from the refactoring detection.
	 */
	private List<ShallowEntity> rootEntities;

	/** Logger instance for printing warnings. */
	private ILogger logger;

	/**
	 * @param rootEntities
	 *            All shallow entities of the current file(see
	 *            {@link #rootEntities})
	 * @param logger
	 *            Logger object that is used to communicate warnings.
	 */
	public AbapTypeInfoExtractor(List<ShallowEntity> rootEntities,
			ILogger logger) {
		this.rootEntities = rootEntities;
		this.logger = logger;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Extracts type information from declaration entities of type statement,
	 * e.g. like DATA or FIELD-SYMBOL blocks.
	 */
	@Override
	public List<TypedVariable> extractFromStatement(
			ShallowEntity statementEntity) {
		if (VARIABLE_DECLARATION_SUBTYPES
				.contains(statementEntity.getSubtype())) {
			return extractFromVariableDeclarationEntity(statementEntity);
		}

		return super.extractFromStatement(statementEntity);
	}

	/** {@inheritDoc} */
	@Override
	public List<TypedVariable> extractFromAttribute(
			ShallowEntity attributeEntity) {
		if (VARIABLE_DECLARATION_SUBTYPES
				.contains(attributeEntity.getSubtype())) {
			return extractFromVariableDeclarationEntity(attributeEntity);
		}

		return super.extractFromAttribute(attributeEntity);
	}

	/** {@inheritDoc} */
	@Override
	public List<TypedVariable> extractFromMethod(ShallowEntity entity) {
		switch (entity.getSubtype()) {
		case SubTypeNames.FORM:
			// Forms and functions work the same way.
		case SubTypeNames.FUNCTION:
			return extractFromForm(entity, entity.ownStartTokens());
		case SubTypeNames.METHOD_DECLARATION:
			// METHOD_DECLARATION is handled the same way as
			// METHOD_IMPLEMENTATION
		case SubTypeNames.METHOD_IMPLEMENTATION:
			return extractFromMethodImplementation(entity);
		default:
			return super.extractFromMethod(entity);
		}
	}

	/**
	 * Extracts type information from a METHOD subroutine.
	 * 
	 * @return A list of {@link TypedVariable}s belonging to the parameters of
	 *         the method. If no rootEnities are set for the method an empty
	 *         list is returned.
	 */
	private List<TypedVariable> extractFromMethodImplementation(
			ShallowEntity methodEntity) {
		if (rootEntities == null) {
			return CollectionUtils.emptyList();
		}

		List<IToken> declaration = LanguageFeatureParser.ABAP
				.getDeclarationForMethod(rootEntities, methodEntity);

		// If no declaration has been found for various reasons return no types.
		// This may happen when called with a non complete list of
		// #rootEntities. Maybe even for a complete list when the method
		// implements an interface. The TypeInfoExtractor also is called from
		// the MethodRefactoringDetector base with a non complete list of root
		// entities.
		if (declaration == null) {
			return CollectionUtils.emptyList();
		}

		return extractFromForm(methodEntity, declaration);
	}

	/** Extracts type information from a FORM subroutine. */
	private List<TypedVariable> extractFromForm(ShallowEntity entity,
			List<IToken> formTokens) {
		return LanguageFeatureParser.ABAP.getTypeInfoForMethodParameters(entity,
				formTokens.subList(2, formTokens.size()), logger);
	}

	/** Extracts type information from variable tokens. */
	private List<TypedVariable> extractFromVariableDeclarationEntity(
			ShallowEntity entity) {
		List<TypedVariable> typeInfo = new ArrayList<>();
		List<IToken> tokens = entity.ownStartTokens();

		if (TokenStreamUtils.hasTokenTypeSequence(tokens, 1, BEGIN, OF)) {
			String variableName = tokens.get(3).getText();
			typeInfo.add(new TypedVariable(variableName, STRUCTURED_DATATYPE,
					CollectionUtils.emptyList(), entity));
		} else {
			int startOffsetOfType;
			String variableName;
			// Handle field symbol
			if (TokenStreamUtils.hasTokenTypeSequence(tokens, 1, LT, IDENTIFIER,
					GT)) {
				variableName = TokenStreamTextUtils
						.concatTokenTexts(tokens.subList(1, 4));
				startOffsetOfType = 4;
			} else {
				variableName = tokens.get(1).getText();
				startOffsetOfType = 2;
			}
			Pair<String, Integer> type = LanguageFeatureParser.ABAP
					.getNextTypeName(tokens, startOffsetOfType, logger);
			typeInfo.add(new TypedVariable(variableName, type.getFirst(),
					CollectionUtils.emptyList(), entity));
		}

		return typeInfo;
	}

}
