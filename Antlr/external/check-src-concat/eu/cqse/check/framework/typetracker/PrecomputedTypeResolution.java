/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.Map;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * A type resolution that internally stores precomputed type lookups.
 * 
 * @author $Author: kupka$
 * @version $Rev: 53994 $
 * @ConQAT.Rating YELLOW Hash: 99F870EBAAB7829B1F48B37F9F531EF0
 */
public class PrecomputedTypeResolution implements ITypeResolution {

	/** The internal mapping from shallow entities to type lookups. */
	private final Map<ShallowEntity, ScopedTypeLookup> lookupMap;

	/** Constructor. */
	public PrecomputedTypeResolution(
			Map<ShallowEntity, ScopedTypeLookup> lookupMap) {
		CCSMAssert.isNotNull(lookupMap);
		this.lookupMap = lookupMap;
	}

	/** {@inheritDoc} */
	@Override
	public ScopedTypeLookup getTypeLookup(ShallowEntity entity) {
		return lookupMap.get(entity);
	}

}
