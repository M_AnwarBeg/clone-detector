/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * A typed variable is an immutable object that contains information about the
 * type of a variable. It stores the variable's name, the variable's type and
 * further modifiers. The type is stored as string, that represents the
 * concatenation of all contained token texts. Thus for field
 * <code> private static java.util.List&lt;String&gt; names; </code> the
 * variable name "names", the modifiers ["private", "static"] and the type name
 * "java.util.List&lt;String&gt;" will be stored.
 * 
 * @author $Author: dreier $
 * @version $Rev: 55064 $
 * @ConQAT.Rating YELLOW Hash: 0DA1BB8720D2E27FE5CDFF1E8AEC919E
 */
public class TypedVariable implements Serializable {

	/** Id used for serialization. */
	private static final long serialVersionUID = -7915139441846763832L;

	/** Type name that is used if no type name was given. */
	private static final String UNKNOWN_TYPE_NAME = "<unknown type>";

	/** The variable's name. */
	private final String variableName;

	/** Name of the variable's type. */
	private String typeName;

	/** A list of variable modifiers. */
	// TODO (LH) Why is this a list of Strings? Can't we be more precise and use
	// an existing enum or a new one for the possible modifiers?
	// TODO (AK) It is hard to create a generic enum, because this class shall
	// be used for different languages. Probably defining an interface
	// ITypedVariable and subclassing it for different languages with different
	// modifier enums would be possible.
	// TODO (LH) I thought of using ETokenType, it should "know" all modifiers,
	// right?
	// TODO (AK) This will work for Java/C#, but i am not sure whether we
	// guarantee, that every modifier in every language uses an extra token
	// type.
	private final List<String> modifiers;

	/** The entity that defines this variable. */
	private transient final ShallowEntity declaringEntity;

	/**
	 * Creates a new type info object, the variable name and the modifier list
	 * must not be null.
	 */
	public TypedVariable(String variableName, String typeName,
			List<String> modifiers, ShallowEntity definitionEntity) {
		CCSMAssert.isNotNull(variableName);
		CCSMAssert.isNotNull(modifiers);
		CCSMAssert.isNotNull(definitionEntity);
		this.variableName = variableName;
		this.typeName = typeName;
		if (typeName == null) {
			this.typeName = UNKNOWN_TYPE_NAME;
		}
		this.modifiers = new ArrayList<String>(modifiers);
		this.declaringEntity = definitionEntity;
	}

	/** Returns the variable's name. */
	public String getVariableName() {
		return variableName;
	}

	/** Returns the variable's type name. */
	public String getTypeName() {
		return typeName;
	}

	/** Returns whether this type info contains the given modifier. */
	public boolean hasModifier(String modifier) {
		return modifiers.contains(modifier);
	}

	/** Returns an unmodifiable list of variable modifiers. */
	public List<String> getModifiers() {
		return CollectionUtils.asUnmodifiable(modifiers);
	}

	/** Returns the entity that declares this variable. */
	public ShallowEntity getDeclaringEntity() {
		return declaringEntity;
	}

	/** Creates string representation of the type info object. */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		if (modifiers.size() > 0) {
			builder.append(StringUtils.concat(modifiers, " "));
			builder.append(" ");
		}
		builder.append(typeName);
		builder.append(" ");
		builder.append(variableName);
		return builder.toString();
	}
}
