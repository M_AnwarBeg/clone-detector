/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import static eu.cqse.check.framework.scanner.ELanguage.ABAP;
import static eu.cqse.check.framework.scanner.ELanguage.CS;
import static eu.cqse.check.framework.scanner.ELanguage.JAVA;

import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMPre;
import org.conqat.lib.commons.assertion.PreconditionException;
import org.conqat.lib.commons.logging.ILogger;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.abap.AbapTypeInfoExtractor;
import eu.cqse.check.framework.typetracker.cs.CsTypeInfoExtractor;
import eu.cqse.check.framework.typetracker.java.JavaTypeInfoExtractor;

/**
 * Factory class for creating type info extractors.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57206 $
 * @ConQAT.Rating YELLOW Hash: AB5D0F8908715DEB06B61485C9F5C22B
 */
public class TypeInfoExtractorFactory {

	/**
	 * List of all languages that do have a TypeInfoExtractor.
	 */
	private static final EnumSet<ELanguage> SUPPORTED_LANGUAGES = EnumSet
			.of(ABAP, CS, JAVA);

	/**
	 * Returns a type info extractor instance for the given language.
	 * {@link #supportsLanguage(ELanguage)}) has to be called before, because
	 * for unsupported languages a {@link PreconditionException} will be thrown.
	 * 
	 * @param rootEntities
	 *            All shallow entities of the current file. May be null e.g.
	 *            when called from the refactoring detection.
	 */
	public static ITypeInfoExtractor getTypeInfoExtractor(ELanguage language,
			List<ShallowEntity> rootEntities, ILogger logger) {
		CCSMPre.isTrue(supportsLanguage(language),
				"No type info extractor is defined for "
						+ language.getReadableName());
		switch (language) {
		case ABAP:
			return new AbapTypeInfoExtractor(rootEntities, logger);
		case CS:
			return new CsTypeInfoExtractor();
		case JAVA:
			return new JavaTypeInfoExtractor();
		default:
			return null;
		}
	}

	/** Determines if language is supported. */
	public static boolean supportsLanguage(ELanguage lanugage) {
		return SUPPORTED_LANGUAGES.contains(lanugage);
	}
}
