/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker.java;

import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.typetracker.ScopedTypeLookup;
import eu.cqse.check.framework.typetracker.clike.CLikeTypeTracker;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * A Java type tracker attaches type information to Java shallow entities.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: B1289A5D04C77F73FAF8659E9F642240
 */
public class JavaTypeTracker extends CLikeTypeTracker {

	/** Name of the java.lang package. */
	private static final String JAVA_LANG = "java.lang";

	/** All sub-types that need a separate lookup scope. */
	private static final Set<String> SEPARATE_SCOPE_SUBTYPES = CollectionUtils
			.asHashSet(SubTypeNames.FOR, SubTypeNames.CATCH, SubTypeNames.TRY);

	/** Constructor. */
	public JavaTypeTracker() {
		super(ELanguage.JAVA, LanguageFeatureParser.JAVA);
	}

	/** {@inheritDoc} */
	@Override
	protected void addDefaultImports(ScopedTypeLookup variableLookup) {
		variableLookup.addImportedNamespace(JAVA_LANG);
	}

	/** {@inheritDoc} */
	@Override
	protected Set<String> getSeparateScopeSubtypes() {
		return SEPARATE_SCOPE_SUBTYPES;
	}
}
