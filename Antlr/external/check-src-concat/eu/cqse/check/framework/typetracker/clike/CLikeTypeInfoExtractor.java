/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker.clike;

import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.collections.Pair;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.TypeInfoExtractorBase;
import eu.cqse.check.framework.typetracker.TypedVariable;
import eu.cqse.check.framework.util.CLikeLanguageFeatureParserBase;

/**
 * The clike type info extractor is an abstract base class for type info
 * extractors for clike languages. Type information in clike languages is only
 * introduces by shallow entities that are either statements, methods or
 * attributes. Other shallow entity types don't introduce any type information.
 * 
 * @author $Author: kupka $
 * @version $Rev: 57172 $
 * @ConQAT.Rating YELLOW Hash: 32FD6126CF959EF67A26542CEC0FF88F
 */
public abstract class CLikeTypeInfoExtractor extends TypeInfoExtractorBase {

	/** The internally used token utilities. */
	protected final CLikeLanguageFeatureParserBase languageUtils;

	/** Constructor. */
	public CLikeTypeInfoExtractor(
			CLikeLanguageFeatureParserBase languageUtils) {
		CCSMAssert.isNotNull(languageUtils);
		this.languageUtils = languageUtils;
	}

	/** {@inheritDoc} */
	@Override
	public List<TypedVariable> extractFromAttribute(
			ShallowEntity attributeEntity) {
		String subtype = attributeEntity.getSubtype();
		if (subtype.equals(SubTypeNames.ATTRIBUTE)) {
			return extractFromVariableTokens(attributeEntity,
					attributeEntity.ownStartTokens());
		}
		return super.extractFromAttribute(attributeEntity);
	}

	/** {@inheritDoc} */
	@Override
	public List<TypedVariable> extractFromMethod(ShallowEntity methodEntity) {
		List<List<IToken>> splitParameterTokens = languageUtils
				.getSplitParameterTokens(methodEntity);

		List<TypedVariable> typeInfo = new ArrayList<>();
		for (List<IToken> parameter : splitParameterTokens) {
			Pair<List<String>, String> modifiersAndType = languageUtils
					.getModifiersAndTypeFromTokens(parameter);
			String typeName = modifiersAndType.getSecond();
			List<String> modifiers = modifiersAndType.getFirst();

			IToken variableName = languageUtils
					.getVariableNameFromTokens(parameter);
			if (variableName != null) {
				typeInfo.add(new TypedVariable(variableName.getText(), typeName,
						modifiers, methodEntity));
			}
		}

		return typeInfo;
	}

	/**
	 * Extracts type information from the start tokens of for, foreach and using
	 * statements. The given end token type is used to determine the end of the
	 * variable declaration within the loop. For loops use SEMICOLON, while
	 * foreach loops use IN and using statements use RPAREN.
	 */
	protected List<TypedVariable> extractFromForLikeTokens(ShallowEntity entity,
			ETokenType endToken) {
		List<IToken> variableTokens = languageUtils
				.getVariableTokensFromForLikeTokens(entity.ownStartTokens(),
						endToken);
		return extractFromVariableTokens(entity, variableTokens);
	}

	/** Extracts type information from variable tokens. */
	protected List<TypedVariable> extractFromVariableTokens(
			ShallowEntity entity, List<IToken> variableTokens) {
		List<List<IToken>> splitTokens = languageUtils
				.splitVariableTokens(variableTokens);
		CCSMAssert.isFalse(splitTokens.isEmpty(),
				"splitTokens must have at least one element");

		List<IToken> firstTokens = splitTokens.get(0);
		if (!languageUtils.isVariableDeclaration(firstTokens)) {
			return CollectionUtils.emptyList();
		}

		Pair<List<String>, String> modifiersAndType = languageUtils
				.getModifiersAndTypeFromTokens(firstTokens);
		String typeName = modifiersAndType.getSecond();
		List<String> modifiers = modifiersAndType.getFirst();

		List<TypedVariable> typeInfo = new ArrayList<>();
		for (List<IToken> tokens : splitTokens) {
			IToken variableName = languageUtils
					.getVariableNameFromTokens(tokens);
			if (variableName != null) {
				typeInfo.add(new TypedVariable(variableName.getText(), typeName,
						modifiers, entity));
			}
		}

		return typeInfo;
	}

	/** Extracts type information from the given catch statement. */
	protected List<TypedVariable> extractFromCatchTokens(ShallowEntity entity) {
		List<IToken> exceptionTokens = TokenStreamUtils.tokensBetween(
				entity.ownStartTokens(), ETokenType.LPAREN, ETokenType.RPAREN);
		List<TypedVariable> typeInfo = new ArrayList<>();
		if (exceptionTokens.size() <= 1) {
			return typeInfo;
		}

		Pair<List<String>, String> modifiersAndType = languageUtils
				.getModifiersAndTypeFromTokens(exceptionTokens);
		String typeName = modifiersAndType.getSecond();
		List<String> modifiers = modifiersAndType.getFirst();

		IToken variableName = languageUtils
				.getVariableNameFromTokens(exceptionTokens);
		if (variableName != null) {
			typeInfo.add(new TypedVariable(variableName.getText(), typeName,
					modifiers, entity));
		}

		return typeInfo;
	}
}
