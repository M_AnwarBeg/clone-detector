/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.conqat.lib.commons.logging.ILogger;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Abstract base class for type trackers.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: 75C32F296D555D8587F2D4244D042706
 */
public abstract class TypeTrackerBase implements ITypeTracker {

	/**
	 * A set of all entity types that provide an ordered introduction of type
	 * information.
	 */
	private static final EnumSet<EShallowEntityType> ORDERED_ENTITY_TYPES = EnumSet
			.of(EShallowEntityType.METHOD, EShallowEntityType.STATEMENT);

	/** Mapping from shallow entities to type lookup maps. */
	private final Map<ShallowEntity, ScopedTypeLookup> typeLookups = new HashMap<>();

	/**
	 * Language of the type tracker.
	 */
	private ELanguage language;

	/** The internally used type info extractor. */
	protected ITypeInfoExtractor typeInfoExtractor;

	/** Constructor. */
	public TypeTrackerBase(ELanguage language) {
		this.language = language;
	}

	/** {@inheritDoc} */
	@Override
	public ITypeResolution createTypeResolution(
			List<ShallowEntity> rootEntities, ILogger logger) {
		ScopedTypeLookup variableLookup = new ScopedTypeLookup();
		return createTypeResolution(rootEntities, logger, variableLookup);
	}

	/** {@inheritDoc} */
	@Override
	public ITypeResolution createTypeResolution(
			List<ShallowEntity> rootEntities, ILogger logger,
			ScopedTypeLookup variableLookup) {
		typeInfoExtractor = TypeInfoExtractorFactory
				.getTypeInfoExtractor(language, rootEntities, logger);
		addDefaultImports(variableLookup);
		createTypeInformation(rootEntities, variableLookup, false);
		return new PrecomputedTypeResolution(
				new HashMap<ShallowEntity, ScopedTypeLookup>(typeLookups));
	}

	/**
	 * Recursive method that is called to traverse all given entities. The
	 * parent tracked entity can be null, if the given entities are top-level
	 * entities. The given variable lookup contains type information for all
	 * known variables. Ordered specifies whether an ordered scope should be
	 * used.
	 */
	private void createTypeInformation(List<ShallowEntity> entities,
			ScopedTypeLookup variableLookup, boolean ordered) {
		for (ShallowEntity entity : entities) {
			processImport(entity, variableLookup);

			List<TypedVariable> typeInfo = typeInfoExtractor.extract(entity);
			boolean newTypeInfo = !typeInfo.isEmpty();

			if (ordered && newTypeInfo) {
				variableLookup = variableLookup.createChildScope();
			}

			ScopedTypeLookup tmpVariableLookup = variableLookup;
			if (newTypeInfo) {
				if (needsSeparateScope(entity)) {
					tmpVariableLookup = tmpVariableLookup.createChildScope();
				}
				for (TypedVariable info : typeInfo) {
					tmpVariableLookup.putTypeInfo(info.getVariableName(), info);
				}
			}

			typeLookups.put(entity, tmpVariableLookup);

			List<ShallowEntity> children = entity.getChildren();
			if (!children.isEmpty()) {
				createTypeInformation(children,
						tmpVariableLookup.createChildScope(),
						needsOrderedScope(entity));
			}
		}
	}

	/** Adds default namespaces to the initial variable lookup. */
	protected void addDefaultImports(
			@SuppressWarnings("unused") ScopedTypeLookup variableLookup) {
		// do nothing by default
	}

	/**
	 * Checks whether the given entity is an import of a namespace and adds the
	 * namespace to the variable lookup.
	 */
	protected void processImport(
			@SuppressWarnings("unused") ShallowEntity entity,
			@SuppressWarnings("unused") ScopedTypeLookup variableLookup) {
		// do nothing by default
	}

	/**
	 * Returns whether the given entity needs an ordered scope. This is used e.
	 * g. for methods or statements, where variables are only accessible if they
	 * have been introduced in an entity earlier in the code. Other entities
	 * like types don't need an ordered scope, because introduced attributes are
	 * visible everywhere within the class.
	 */
	protected boolean needsOrderedScope(ShallowEntity entity) {
		return ORDERED_ENTITY_TYPES.contains(entity.getType());
	}

	/**
	 * Returns whether this entity needs a separate variable scope. This is the
	 * case for methods and some statements that are determined by
	 * {@link #getSeparateScopeSubtypes()}, as otherwise those would introduce
	 * type information into the wrong scope.
	 */
	private boolean needsSeparateScope(ShallowEntity entity) {
		if (entity.getType().equals(EShallowEntityType.METHOD)
				|| getSeparateScopeSubtypes().contains(entity.getSubtype())) {
			return true;
		}
		return false;
	}

	/** Returns all statement sub-types that need an separate scope. */
	protected abstract Set<String> getSeparateScopeSubtypes();

}