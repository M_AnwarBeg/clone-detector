/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * 
 * An interface that provides type lookups for shallow entities.
 * 
 * @author $Author: kupka$
 * @version $Rev: 53994 $
 * @ConQAT.Rating YELLOW Hash: 1D792E92F7178D315AB64CB3F2AEAE44
 */
public interface ITypeResolution {

	/** Returns a type lookup for the given shallow entity. */
	public ScopedTypeLookup getTypeLookup(ShallowEntity entity);
}
