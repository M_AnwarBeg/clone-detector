/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker.java;

import java.util.List;

import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.TypedVariable;
import eu.cqse.check.framework.typetracker.clike.CLikeTypeInfoExtractor;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * A Java type info extractor extracts type information from Java shallow
 * entities that are either statements, methods or attributes. Other shallow
 * entity types don't introduce any type information in Java.
 * 
 * 
 * @author $Author: dreier $
 * @version $Rev: 55360 $
 * @ConQAT.Rating YELLOW Hash: DF192AFD62FC0352193F34F390FEB0FE
 */
public class JavaTypeInfoExtractor extends CLikeTypeInfoExtractor {

	/** Constructor. */
	public JavaTypeInfoExtractor() {
		super(LanguageFeatureParser.JAVA);
	}

	/** {@inheritDoc} */
	@Override
	public List<TypedVariable> extractFromStatement(
			ShallowEntity statementEntity) {
		switch (statementEntity.getSubtype()) {
		case SubTypeNames.LOCAL_VARIABLE:
			return extractFromVariableTokens(statementEntity,
					statementEntity.ownStartTokens());
		case SubTypeNames.FOR:
			ETokenType endToken = ETokenType.SEMICOLON;
			if (LanguageFeatureParser.JAVA.isForEachLoop(statementEntity)) {
				endToken = ETokenType.COLON;
			}
			return extractFromForLikeTokens(statementEntity, endToken);
		case SubTypeNames.CATCH:
			return extractFromCatchTokens(statementEntity);
		case SubTypeNames.TRY:
			return extractFromForLikeTokens(statementEntity, ETokenType.RPAREN);
		default:
			return super.extractFromStatement(statementEntity);
		}

	}

}
