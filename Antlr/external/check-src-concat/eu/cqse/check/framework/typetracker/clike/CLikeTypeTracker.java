/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker.clike;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.ScopedTypeLookup;
import eu.cqse.check.framework.typetracker.TypeTrackerBase;
import eu.cqse.check.framework.util.CLikeLanguageFeatureParserBase;

/**
 * Abstract base class for type trackers for C-like languages.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: E3EA3E0E5C32C5AC08F4C4B9FE007BC2
 */
public abstract class CLikeTypeTracker extends TypeTrackerBase {

	/** The internally used language utilities. */
	protected final CLikeLanguageFeatureParserBase languageUtils;

	/** Constructor. */
	protected CLikeTypeTracker(ELanguage language,
			CLikeLanguageFeatureParserBase languageUtils) {
		super(language);
		this.languageUtils = languageUtils;
	}

	/** {@inheritDoc} */
	@Override
	protected void processImport(ShallowEntity entity,
			ScopedTypeLookup variableLookup) {
		if (languageUtils.isImport(entity)) {
			String namespace = languageUtils.getImportName(entity);
			if (namespace != null) {
				variableLookup.addImportedNamespace(namespace);
			}
		}
	}
}
