/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.EnumMap;
import java.util.Map;

import org.conqat.lib.commons.assertion.CCSMAssert;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.typetracker.abap.AbapTypeTracker;
import eu.cqse.check.framework.typetracker.cs.CsTypeTracker;
import eu.cqse.check.framework.typetracker.java.JavaTypeTracker;

/**
 * Factory class for creating type trackers.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: CBD997DD2C6D771B261472686C7FE248
 */
public class TypeTrackerFactory {

	/** Mapping from languages to the corresponding type tracker class. */
	private static final Map<ELanguage, Class<? extends ITypeTracker>> TYPE_TRACKERS = new EnumMap<>(
			ELanguage.class);

	static {
		registerTypeTrackerClass(ELanguage.ABAP, AbapTypeTracker.class);
		registerTypeTrackerClass(ELanguage.CS, CsTypeTracker.class);
		registerTypeTrackerClass(ELanguage.JAVA, JavaTypeTracker.class);
	}

	/** Registers the given type tracker class for the given language. */
	private static void registerTypeTrackerClass(ELanguage language,
			Class<? extends ITypeTracker> typeTrackerClass) {
		TYPE_TRACKERS.put(language, typeTrackerClass);
	}

	/** Creates a new type tracker for the given language. */
	public static ITypeTracker createTypeTracker(ELanguage language) {
		Class<? extends ITypeTracker> typeTrackerClass = TYPE_TRACKERS
				.get(language);
		CCSMAssert.isNotNull(typeTrackerClass,
				"No type tracker is defined for " + language.getReadableName());
		try {
			return typeTrackerClass.newInstance();
		} catch (InstantiationException e) {
			CCSMAssert.fail("Type tracker has no default constructor.");
		} catch (IllegalAccessException e) {
			CCSMAssert.fail("Type tracker default constructor is not visible.");
		}
		return null;
	}

	/** Returns whether the given language has type tracking support. */
	public static boolean supportsLanguage(ELanguage language) {
		return TYPE_TRACKERS.containsKey(language);
	}
}
