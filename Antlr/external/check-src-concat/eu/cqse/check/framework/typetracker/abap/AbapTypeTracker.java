/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker.abap;

import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.typetracker.TypeTrackerBase;

/**
 * An ABAP type tracker attaches type information to ABAP shallow entities.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: DB33E2EB142186BBA15741185D7AB306
 */
public class AbapTypeTracker extends TypeTrackerBase {

	/** Constructor. */
	public AbapTypeTracker() {
		super(ELanguage.ABAP);
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * In ABAP, there are no entities, which open a new variable scope. All
	 * variables are visible everywhere within the method.
	 */
	@Override
	protected Set<String> getSeparateScopeSubtypes() {
		return CollectionUtils.emptySet();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * In ABAP you can even access local variables before you define them.
	 */
	@Override
	protected boolean needsOrderedScope(ShallowEntity entity) {
		return false;
	}
}
