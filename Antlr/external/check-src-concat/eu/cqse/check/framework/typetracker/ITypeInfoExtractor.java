/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.List;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * A type info extractor extracts type information of declared variables within
 * shallow entities. A type info extractor must not hold any internal state, as
 * the same instance will be returned by the {@link TypeInfoExtractorFactory}
 * multiple times.
 *
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: 7ABF2D38FE10FC8FA2C216353BCA90E2
 */
public interface ITypeInfoExtractor {

	/**
	 * Extracts type information for all variables that are newly declared
	 * within the given entity. Child entities are not included.
	 */
	public List<TypedVariable> extract(ShallowEntity entity);
}
