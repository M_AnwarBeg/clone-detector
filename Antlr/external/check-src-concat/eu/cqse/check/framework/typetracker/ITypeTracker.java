/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.List;

import org.conqat.lib.commons.logging.ILogger;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * A type tracker attaches type information about known variables to shallow
 * entities. Therefore it creates a {@link ITypeResolution} that provides a
 * mapping from shallow entities to type lookups.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: 5AD6FF7BAE07ACF41FBF2938CD358890
 */
public interface ITypeTracker {

	/**
	 * Creates type information for all given shallow entities and their
	 * children. A type resolution is returned.
	 */
	public ITypeResolution createTypeResolution(
			List<ShallowEntity> rootEntities, ILogger logger);

	/**
	 * Creates type information for all given shallow entities and their
	 * children and uses the given {@link ScopedTypeLookup} as root lookup. A
	 * type resolution is returned.
	 */
	ITypeResolution createTypeResolution(List<ShallowEntity> rootEntities,
			ILogger logger, ScopedTypeLookup scopedTypeLookup);
}
