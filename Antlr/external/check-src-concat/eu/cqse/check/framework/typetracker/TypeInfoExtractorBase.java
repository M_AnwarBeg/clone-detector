/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.framework.typetracker;

import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * An abstract base class for type info extractors. This base class implements
 * the {@link ITypeInfoExtractor#extract(ShallowEntity)}-method and passes the
 * given entity to a method corresponding to the entity's type.
 * 
 * @author $Author: dreier $
 * @version $Rev: 57205 $
 * @ConQAT.Rating YELLOW Hash: 6DA42D053C7CE6EBC73C8639C603E011
 */
public abstract class TypeInfoExtractorBase implements ITypeInfoExtractor {

	/** {@inheritDoc} */
	@Override
	public List<TypedVariable> extract(ShallowEntity entity) {
		switch (entity.getType()) {
		case MODULE:
			return extractFromModule(entity);
		case META:
			return extractFromMeta(entity);
		case TYPE:
			return extractFromType(entity);
		case ATTRIBUTE:
			return extractFromAttribute(entity);
		case METHOD:
			return extractFromMethod(entity);
		case STATEMENT:
			return extractFromStatement(entity);
		default:
			CCSMAssert.fail("No valid entity type.");
			return null;
		}
	}

	/** Extracts type information from module entities. */
	public List<TypedVariable> extractFromModule(
			@SuppressWarnings("unused") ShallowEntity moduleEntity) {
		return CollectionUtils.emptyList();
	}

	/** Extracts type information from meta entities. */
	public List<TypedVariable> extractFromMeta(
			@SuppressWarnings("unused") ShallowEntity metaEntity) {
		return CollectionUtils.emptyList();
	}

	/** Extracts type information from type entities. */
	public List<TypedVariable> extractFromType(
			@SuppressWarnings("unused") ShallowEntity typeEntity) {
		return CollectionUtils.emptyList();
	}

	/** Extracts type information from attribute entities. */
	public List<TypedVariable> extractFromAttribute(
			@SuppressWarnings("unused") ShallowEntity attributeEntity) {
		return CollectionUtils.emptyList();
	}

	/** Extracts type information from method entities. */
	public List<TypedVariable> extractFromMethod(
			@SuppressWarnings("unused") ShallowEntity methodEntity) {
		return CollectionUtils.emptyList();
	}

	/** Extracts type information from statement entities. */
	public List<TypedVariable> extractFromStatement(
			@SuppressWarnings("unused") ShallowEntity statementEntity) {
		return CollectionUtils.emptyList();
	}
}
