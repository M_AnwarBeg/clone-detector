/*-----------------------------------------------------------------------+
 | eu.cqse.conqat.engine.sourcecode
 |                                                                       |
   $Id: TokenEquator.java 52656 2015-05-19 11:31:34Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2012 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.framework.scanner;

import org.conqat.lib.commons.equals.IEquator;

/**
 * A class for testing tokens for equality with respect to type and content.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52656 $
 * @ConQAT.Rating GREEN Hash: B4F248A7DC29385CFF5202295E53E5D6
 */
public class TokenEquator implements IEquator<IToken> {

	/** Reusable instance. */
	public static final TokenEquator INSTANCE = new TokenEquator();

	/** {@inheritDoc} */
	@Override
	public boolean equals(IToken token1, IToken token2) {
		return token1.getType() == token2.getType()
				&& token1.getText().equals(token2.getText());
	}
}