/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.abap;

import static eu.cqse.check.framework.scanner.ETokenType.ESCAPE;
import static eu.cqse.check.framework.scanner.ETokenType.LIKE;
import static eu.cqse.check.framework.scanner.ETokenType.WHERE;
import static eu.cqse.check.framework.shallowparser.SubTypeNames.SELECT_BLOCK;
import static eu.cqse.check.framework.shallowparser.SubTypeNames.SINGLE_SELECT;

import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * This check is off by default, since it may have false positives.
 * 
 * @author $Author: goeb $
 * @version $Rev: 57106 $
 * @ConQAT.Rating YELLOW Hash: 3D4015096371F97A2B705FCCB942448C
 */
@ACheck(name = "Unescaped WHERE ... LIKE", description = "SELECTs with non-literal WHERE ... LIKE should always specify ESCAPE "
		+ "to avoid unintended wildcards.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.OFF, languages = {
				ELanguage.ABAP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UnescapedWhereLikeCheck extends EntityCheckBase {

	/**
	 * Select single SELECT statements or SELECT ... ENDSELECT blocks that
	 * contain both WHERE and LIKE.
	 */
	private static final String SELECTOR = String.format(
			"//STATEMENT[(subtype('%s') or subtype('%s')) and tokens('%s', '%s')]",
			SINGLE_SELECT, SELECT_BLOCK, WHERE, LIKE);

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return SELECTOR;
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<List<IToken>> parts = TokenStreamUtils
				.split(entity.ownStartTokens(), LIKE);
		// The first element will be everything before the LIKE, so skip this
		for (List<IToken> likeClause : parts.subList(1, parts.size())) {
			if (isUnescapedNonLiteral(likeClause)) {
				createFinding(
						"Non-literal LIKE without ESCAPE in a WHERE clause.",
						likeClause.get(0));
			}
		}
	}

	/**
	 * Checks whether the given tokens following LIKE start with a non-literal
	 * but no ESCAPE.
	 */
	private static boolean isUnescapedNonLiteral(List<IToken> likeClause) {
		if (likeClause.isEmpty() || likeClause.get(0).getType().isLiteral()) {
			return false;
		}
		return !TokenStreamUtils.tokenStreamContains(likeClause, ESCAPE);
	}
}
