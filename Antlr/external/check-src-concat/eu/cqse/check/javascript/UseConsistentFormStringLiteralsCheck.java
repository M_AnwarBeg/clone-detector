/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.javascript;

import java.util.EnumSet;
import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * This work is a step to encourage developers to maintain a consistent use of
 * either single or double-quoted string literals in Javascript. The check is
 * configurable so it can be turned on or off.
 * 
 * @author $Author: ezaga$
 * @version $Rev: 57565 $
 * @ConQAT.Rating GREEN Hash: 3C7AA1CEC60745BBC1DADF70111A75D2
 */
@ACheck(name = "Use Consistent Form for String Literals", description = "Use consistent form for String literals, i.e. only one of '' or \"\"", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVASCRIPT }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class UseConsistentFormStringLiteralsCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Use Single-Quoted String Literals", description = "Use Single-Quoted String Literals in Javascript code")
	private boolean useSingleQuotedLiterals = true;

	/** The default literal quotes for Javascript. */
	private String quotationCharacter = "'";

	/** The default finding message when checking for single-quoted literals */
	private String findingMessage = "Use Single-Quoted String Literals";

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT";
	}

	/** {@inheritDoc} */
	@Override
	public void initialize() {
		if (!useSingleQuotedLiterals) {
			quotationCharacter = "\"";
			findingMessage = "Use Double-Quoted String Literals";
		}
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> tokens = entity.ownStartTokens();
		List<Integer> indices = TokenStreamUtils.findAll(tokens,
				EnumSet.of(ETokenType.STRING_LITERAL));
		for (int index : indices) {
			IToken token = tokens.get(index);
			if (!token.getText().startsWith(quotationCharacter)) {
				createFinding(findingMessage, token);
			}
		}
	}

}
