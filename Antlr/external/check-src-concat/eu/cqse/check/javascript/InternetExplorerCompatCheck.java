/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.javascript;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * This check checks if JavaScript method calls are supported by (a) Internet
 * Explorer in general and (b) - if enabled - Internet Explorer version 8. Since
 * we cannot use type resolution in JavaScript, a heuristic is used to check
 * suspicious method calls. Specifically, this check will create a finding if
 * the JS method is called with the expected number of arguments and is no
 * JQuery or Closure method call.
 * 
 * @author $Author: veihelmann $
 * @version $Rev: 57292 $
 * @ConQAT.Rating YELLOW Hash: 989BD8EB16AF4C3BBE9979AA7C0EFA58
 */
@ACheck(name = "Internet Explorer Compatibility Check", description = "Some method calls on global objects are not be supported by all browsers. Please refer to online resources like the Mozilla Developer Network ( https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference ) for details.", groupName = "Cross-Browser Compatibility", defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVASCRIPT }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class InternetExplorerCompatCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Check method compatibility for IE8", description = "Checks if method calls of global objects are available on Internet Explorer 8.")
	private boolean checkIE8Compatibiltiy;

	/** The prefix of the Google Closure namespace. */
	private static final String CLOSURE_NAMESPACE_PREFIX = "goog.";

	/**
	 * Maps method names to their expected number of parameters (min and max).
	 */
	private final static Map<String, Integer[]> SUSPICIOUS_METHODS = new HashMap<>();

	/** The value to indicate no method arguments. */
	protected final static Integer[] NO_ARGS = new Integer[] {};

	/** The identifier token for JQuery methods. */
	private final static String JQUERY_IDENTIFIER = "$";

	/** {@inheritDoc} */
	@Override
	public void initialize() {
		super.initialize();

		buildMethodMap();
	}

	/** Fills the method map with candidates that may create a finding. */
	private void buildMethodMap() {
		// String methods
		addSuspicousMethod("startsWith", 1, 2);
		addSuspicousMethod("endsWith", 1, 2);
		addSuspicousMethod("includes", 1, 2);

		// Number methods
		addSuspicousMethod("isFinite", 1);
		addSuspicousMethod("isNaN", 1);
		addSuspicousMethod("isInteger", 1);
		addSuspicousMethod("isSafeInteger", 1);

		// Arrays
		addSuspicousMethod("copyWithin", 2, 3);
		addSuspicousMethod("find", 1, 2);
		addSuspicousMethod("findIndex", 1, 2);

		if (checkIE8Compatibiltiy) {
			addIE8Methods();
		}
	}

	/**
	 * Adds method calls that are not available in Internet Explorer 8 to the
	 * exclusion map.
	 */
	private static void addIE8Methods() {
		// String methods
		addSuspicousMethod("trim", NO_ARGS);

		// Date methods
		addSuspicousMethod("toISOString", NO_ARGS);
		addSuspicousMethod("toJSON", NO_ARGS);

		// Arrays
		addSuspicousMethod("every", 1, 2);
		addSuspicousMethod("filter", 1, 3);
		addSuspicousMethod("forEach", 1, 2);
		addSuspicousMethod("isArray", 1);
		addSuspicousMethod("map", 1, 2);
		addSuspicousMethod("reduce", 1, 2);
		addSuspicousMethod("reduceRight", 1, 2);
		addSuspicousMethod("some", 1, 2);
	}

	/**
	 * Adds a suspicious method to the exclusion map.
	 * 
	 * @param methodName
	 *            The name of the suspicious method.
	 * @param argsCountIntervall
	 *            The (optional) minimal and (required) maximal number of
	 *            arguments this method is expected to be called with. Use an
	 *            empty array for no expected method arguments.
	 */
	private static void addSuspicousMethod(String methodName,
			Integer... argsCountIntervall) {
		SUSPICIOUS_METHODS.put(methodName, argsCountIntervall);
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> tokens = entity.includedTokens();
		List<Integer> indices = TokenStreamUtils.findTokenTypeSequences(tokens,
				0, ETokenType.DOT, ETokenType.IDENTIFIER, ETokenType.LPAREN);

		for (int index : indices) {

			int methodNameIndex = ++index;
			IToken token = tokens.get(methodNameIndex);
			String methodName = token.getText();

			if (shouldReportMethod(methodName, tokens, methodNameIndex)) {
				createFinding(
						methodName + "() is not available on Internet Explorer",
						token);
			}

		}

	}

	/**
	 * Determines whether the given method should be reported as finding.
	 * 
	 * @param methodName
	 *            the name of the method.
	 * @param tokens
	 *            the tokens of the current statement.
	 * @param methodNameIndex
	 *            the index of the method name within the tokens.
	 */
	private static boolean shouldReportMethod(String methodName,
			List<IToken> tokens, int methodNameIndex) {
		return SUSPICIOUS_METHODS.containsKey(methodName)
				&& matchesArgumentsCount(methodName, methodNameIndex, tokens)
				&& !isClosureMethod(tokens, methodNameIndex)
				&& !isJQueryMethod(tokens, methodNameIndex);
	}

	/**
	 * Checks whether the method call is part of a JQuery expression.
	 * 
	 * @param tokens
	 *            the tokens of the current statement.
	 * @param methodNameIndex
	 *            the index of the method name within the tokens.
	 */
	private static boolean isJQueryMethod(List<IToken> tokens,
			int methodNameIndex) {
		int openParenthesisCount = 0;
		for (int i = methodNameIndex; i >= 0; i--) {
			IToken token = tokens.get(i);
			if (token.getType() == ETokenType.RPAREN) {
				openParenthesisCount++;
			} else if (token.getType() == ETokenType.LPAREN) {
				openParenthesisCount--;
			}

			if (token.getText().equals(JQUERY_IDENTIFIER)
					&& openParenthesisCount == 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Determines if the actual <b>number</b> of method arguments is within the
	 * expected range as defined in the exlusion map.
	 * 
	 * @param methodName
	 *            the name of the method.
	 * @param methodNameIndex
	 *            the index of the method name within the tokens.
	 * @param tokens
	 *            the tokens of the current statement.
	 */
	private static boolean matchesArgumentsCount(String methodName,
			int methodNameIndex, List<IToken> tokens) {
		Integer[] expectedArgsCount = SUSPICIOUS_METHODS.get(methodName);

		int actualArgs = countMethodArgs(tokens, methodNameIndex);

		if (expectedArgsCount.length == 0) {
			return actualArgs == 0;
		}

		int minArgs = expectedArgsCount[0];
		int maxArgs = minArgs;
		if (expectedArgsCount.length > 1) {
			maxArgs = expectedArgsCount[1];
		}
		return actualArgs >= minArgs && actualArgs <= maxArgs;
	}

	/**
	 * Counts the number of arguments for the given method.
	 * 
	 * @param tokens
	 *            the tokens of the current statement.
	 * @param methodNameIndex
	 *            the index of the method name within the tokens.
	 */
	private static int countMethodArgs(List<IToken> tokens,
			int methodNameIndex) {
		int commaCount = 0;
		int innerBracketsCounter = 0;

		if (tokens.get(methodNameIndex + 2).getType() == ETokenType.RPAREN) {
			// Method is immediately followed by ')', so it has no arguments.
			return 0;
		}

		for (int j = methodNameIndex + 2; j < tokens.size(); j++) {
			IToken token = tokens.get(j);

			if (innerBracketsCounter == 0
					&& token.getType() == ETokenType.COMMA) {
				commaCount++;
				continue;
			}

			if (token.getType() == ETokenType.LPAREN) {
				innerBracketsCounter++;
			} else if (token.getType() == ETokenType.RPAREN) {
				innerBracketsCounter--;

				if (innerBracketsCounter < 0) {
					break;
				}
			}
		}

		return commaCount + 1;
	}

	/**
	 * Determines if the current method call if part of a Google closure call,
	 * i.e. starts with 'goog.'
	 * 
	 * @param tokens
	 *            the tokens of the current statement.
	 * @param methodNameIndex
	 *            the index of the method name within the tokens.
	 */
	private static boolean isClosureMethod(List<IToken> tokens,
			int methodNameIndex) {
		if (methodNameIndex - 4 < 0) {
			return false;
		}
		String namespacePrefix = tokens.get(methodNameIndex - 4).getText()
				+ tokens.get(methodNameIndex - 3).getText();

		return CLOSURE_NAMESPACE_PREFIX.equals(namespacePrefix);
	}
}
