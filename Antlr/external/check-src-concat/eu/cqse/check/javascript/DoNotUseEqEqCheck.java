/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: DoNotUseEqEqCheck.java 56503 2016-04-05 14:59:57Z heinemann $
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.javascript;

import java.util.EnumSet;
import java.util.List;

import org.conqat.lib.commons.collections.UnmodifiableList;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 *
 * @author $Author: heinemann $
 * @version $Rev: 56503 $
 * @ConQAT.Rating GREEN Hash: 024E42FF6EFC46FC945A4EB5E3E1E22F
 */
@ACheck(name = "Do not use == and !=", description = "The comparison operators == and != should be avoided. Their behaviour is inconsistent and the performance of === and !== is better.", groupName = "Javascript (built-in)", defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVASCRIPT }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class DoNotUseEqEqCheck extends EntityCheckBase {

	/** Set of tokens, which should be avoided while programming JavaScript. */
	public static final EnumSet<ETokenType> UNACCEPTED_TOKENS = EnumSet
			.of(ETokenType.EQEQ, ETokenType.NOTEQ);

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		UnmodifiableList<IToken> tokens = entity.ownStartTokens();
		List<Integer> indexes = TokenStreamUtils.findAll(tokens,
				UNACCEPTED_TOKENS);
		for (Integer index : indexes) {
			createFinding("Do not use == or !=, use === or !== instead.",
					tokens.get(index));
		}
	}
}
