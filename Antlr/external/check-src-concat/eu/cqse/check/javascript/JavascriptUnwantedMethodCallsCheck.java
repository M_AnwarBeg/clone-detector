/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: JavascriptUnwantedMethodCallsCheck.java 57098 2016-05-27 12:44:15Z heinemann $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.javascript;

import java.util.HashSet;
import java.util.Set;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.base.UnwantedMethodCallsCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;

/**
 * Detects unwanted JavaScript method calls in applications.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 57098 $
 * @ConQAT.Rating GREEN Hash: 94CE05DD7CDEB5872AB513BF50C5C592
 */
@ACheck(name = "Javascript Unwanted Method Calls", description = UnwantedMethodCallsCheckBase.CHECK_DESCRIPTION, groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
		ELanguage.JAVASCRIPT }, parameters = {
				ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class JavascriptUnwantedMethodCallsCheck
		extends UnwantedMethodCallsCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Javascript Unwanted Methods", description = "Names of methods that should not be called.")
	private Set<String> unwantedMethods = new HashSet<String>(
			CollectionUtils.asHashSet("alert", "console.log"));

	/** {@inheritDoc} */
	@Override
	protected Set<String> getUnwantedMethods() {
		return unwantedMethods;
	}

}
