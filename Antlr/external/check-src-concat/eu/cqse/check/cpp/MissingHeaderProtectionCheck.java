/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.conqat.lib.commons.collections.CollectionUtils;
import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.EShallowEntityType;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: goeb $
 * @version $Rev: 56640 $
 * @ConQAT.Rating YELLOW Hash: 00A4870EB4E391A6CAD14290571F6D81
 */
// TODO (AG) GREEN for the changes in CR#9654, still YELLOW for #8617
@ACheck(name = "Missing header protection", description = "Header files must protect from multiple inclusion using the preprocessor. The macro variable used "
		+ "for this must match the filename.", groupName = CheckGroupNames.CORRECTNESS, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class MissingHeaderProtectionCheck extends CheckImplementationBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Header extensions", description = "File extensions that are used to identify header files.")
	private Set<String> headerExtensions = CollectionUtils.asHashSet("h", "hh",
			"hpp", "hxx");

	/** {@inheritDoc} */
	@Override
	public void initialize() {
		headerExtensions = headerExtensions.stream().map(x -> x.toLowerCase())
				.collect(Collectors.toSet());
	}

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		String extension = getExtension(context.getUniformPath());
		if (extension == null || (!headerExtensions.isEmpty()
				&& !headerExtensions.contains(extension.toLowerCase()))) {
			// skip non-headers
			return;
		}

		analyzeForMissingHeaderProtection(
				context.getUnfilteredAbstractSyntaxTree());
	}

	/** Analyzes the given entities for missing header protection. */
	protected void analyzeForMissingHeaderProtection(
			List<ShallowEntity> entities) throws CheckException {

		if (entities.size() < 3) {
			createHeaderProtectFinding();
			return;
		}

		String checkedConstant = checkAndExtractIfndef(entities.get(0));
		String definedConstant = extractDefineConstant(entities.get(1));

		if (checkedConstant == null || definedConstant == null
				|| !checkClosingEndIf(CollectionUtils.getLast(entities))) {
			createHeaderProtectFinding();
		} else if (!checkedConstant.equals(definedConstant)) {
			createFindingOnFirstLine(
					"Header protection defines different constant than the one checked for!",
					entities.get(1));
		}
	}

	/**
	 * The first macro must be either "#if !defined" or "#ifndef". Returns the
	 * constant checked for or null if constraint not satisfied.
	 */
	private static String checkAndExtractIfndef(ShallowEntity entity) {
		IToken token = entity.includedTokens().get(0);
		if (token.getType() != ETokenType.PREPROCESSOR_DIRECTIVE) {
			return null;
		}

		String[] parts = token.getText().trim().split("(\\s|[()])+");
		if (parts.length >= 2 && parts[0].equals("#ifndef")) {
			return parts[1];
		}

		if (parts.length < 3 || !parts[0].equals("#if")) {
			return null;
		}

		if (parts[1].equals("!defined")) {
			return parts[2];
		}

		if (parts[1].equals("!") && parts[2].equals("defined")
				&& parts.length > 3) {
			return parts[3];
		}

		return null;
	}

	/**
	 * The second macro must be a "#define". Returns the name of the defined
	 * constant or null if constraint not satisfied.
	 */
	private static String extractDefineConstant(ShallowEntity entity) {
		IToken token = entity.includedTokens().get(0);
		if (token.getType() != ETokenType.PREPROCESSOR_DIRECTIVE) {
			return null;
		}

		String[] parts = token.getText().trim().split("\\s+");
		if (parts.length < 2 || !parts[0].equals("#define")) {
			return null;
		}

		return parts[1];
	}

	/** Last non-comment list must be "#endif". */
	private static boolean checkClosingEndIf(ShallowEntity last) {
		return last.getType() == EShallowEntityType.META && last
				.includedTokens().get(0).getText().trim().startsWith("#endif");
	}

	/** Creates the finding. */
	private void createHeaderProtectFinding() throws CheckException {
		createFinding(
				"Header protection missing or not implemented correctly!");
	}

	/**
	 * TODO (AK) this is a 1:1 copy from UniformPathUtils and shall be removed
	 * when UniformPathUtils is available for custom checks
	 * 
	 * Returns the extension of the uniform path.
	 * 
	 * @return File extension, i.e. "java" for "FileSystemUtils.java", or
	 *         <code>null</code>, if the path has no extension (i.e. if a path
	 *         contains no '.'), returns the empty string if the '.' is the
	 *         path's last character.
	 */
	private static String getExtension(String uniformPath) {
		String name = getElementName(uniformPath);
		int posLastDot = name.lastIndexOf('.');
		if (posLastDot < 0) {
			return null;
		}
		return name.substring(posLastDot + 1);
	}

	/**
	 * TODO (AK) this is a 1:1 copy from UniformPathUtils and shall be removed
	 * when UniformPathUtils is available for custom checks
	 * 
	 * Returns the element name for a uniform path, which is the everything
	 * starting from the last '/'.
	 */
	private static String getElementName(String uniformPath) {
		return StringUtils.getLastPart(uniformPath, '/');
	}
}
