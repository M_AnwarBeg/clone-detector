/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import java.util.List;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kupka $
 * @version $Rev: 55962 $
 * @ConQAT.Rating YELLOW Hash: D0EC438F7A76FAD8F8FD3E72E689B157
 */
@ACheck(name = "Multiple classes in one file (C++)", description = "Defining more than one class in a file makes "
		+ "finding the file for a given class very hard. Therefore each file should contain only one top-level "
		+ "class.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class CppMultipleTopLevelClassesCheck extends CppTopLevelClassCheckBase {

	/** {@inheritDoc} */
	@Override
	protected void analyzeTopLevelTypes(
			List<ShallowEntity> topLevelTypeEntities) throws CheckException {
		if (topLevelTypeEntities.size() > 1) {
			for (ShallowEntity entity : topLevelTypeEntities) {
				createFindingOnFirstLine(
						"Multiple top-level class definitions in one file.",
						entity);
			}
		}
	}
}
