/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import static eu.cqse.check.framework.scanner.ETokenType.PRIVATE;
import static eu.cqse.check.framework.scanner.ETokenType.PROTECTED;
import static eu.cqse.check.framework.scanner.ETokenType.PUBLIC;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kupka $
 * @version $Rev: 55971 $
 * @ConQAT.Rating YELLOW Hash: 46970B13E6B5C429FCF66CB455F7A519
 */
@ACheck(name = "Public class attribute (C++)", description = "Classes should not have public attributes as this violates encapsulation. "
		+ "Instead access should be provided using public methods. This rule does not apply to constants and to attributes in "
		+ "structs.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class CppPublicClassAttributeCheck extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//TYPE[subtype('class')]";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		// private is the default visibility within C++ classes.
		ETokenType currentVisibility = PRIVATE;
		for (ShallowEntity child : entity.getChildren()) {
			switch (child.getType()) {
			case META:
				currentVisibility = updateVisibility(child, currentVisibility);
				break;
			case ATTRIBUTE:
				if (currentVisibility == PUBLIC
						&& !LanguageFeatureParser.CPP.isStaticConst(child)) {
					createFindingOnFirstLine("Attribute declared public",
							child);
				}
				break;
			default:
				// ignore other entity types
				break;
			}
		}
	}

	/**
	 * Returns the new member visibility within a C++ class depending on the
	 * given meta entity and the old visibility.
	 */
	private static ETokenType updateVisibility(ShallowEntity metaEntity,
			ETokenType oldVisibility) {
		switch (metaEntity.getSubtype()) {
		case SubTypeNames.PUBLIC:
			return PUBLIC;
		case SubTypeNames.PROTECTED:
			return PROTECTED;
		case SubTypeNames.PRIVATE:
			return PRIVATE;
		default:
			return oldVisibility;

		}
	}
}
