/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.core.option.ACheckOption;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: dreier $
 * @version $Rev: 55360 $
 * @ConQAT.Rating GREEN Hash: 142B16956E6A3FB01CEA08834733B62A
 */
@ACheck(name = "Missing virtual destructor", description = "If a class has no virtual destructor and some other class inherits from it, this can lead "
		+ "to memory not being freed correctly.", groupName = CheckGroupNames.CORRECTNESS, defaultEnablement = EFindingEnablement.RED, languages = {
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class VirtualDestructorCheck extends EntityCheckBase {

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Include final classes", description = "Reports final classes that have no virtual destructor as finding.")
	private boolean includeFinalClasses = false;

	/** {@ConQAT.Doc} */
	@ACheckOption(name = "Include structs", description = "Reports structs that have no virtual destructor as finding.")
	private boolean includeStructs = false;

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		StringBuilder selection = new StringBuilder(
				"//TYPE[(subtype('" + SubTypeNames.CLASS + "')");
		if (!includeFinalClasses) {
			selection.append("and not(modifiers('final'))");
		}
		selection.append(")");
		if (includeStructs) {
			selection.append("or subtype('" + SubTypeNames.STRUCT + "')");
		}
		selection.append("]");
		return selection.toString();
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		/*List<ShallowEntity> destructors = select(entity,
				"METHOD[subtype('" + SubTypeNames.DESTRUCTOR + "') or subtype('"
						+ SubTypeNames.DESTRUCTOR_DECLARATION + "')]");
		if (destructors.isEmpty()) {
			createFindingOnFirstLine("Class without destructor", entity);
		}

		for (ShallowEntity destructor : destructors) {
			if (!LanguageFeatureParser.CPP.isVirtual(destructor)) {
				createFindingOnFirstLine("Non-virtual destructor", destructor);
			}
		}*/
	}
}
