/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import java.util.List;

import org.conqat.lib.commons.string.StringUtils;

import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * {@ConQAT.Doc}
 * 
 * @author $Author: kupka $
 * @version $Rev: 55962 $
 * @ConQAT.Rating YELLOW Hash: 90CEB921DA0B8CD799D203703F0F83B4
 */
@ACheck(name = "Top-level class name (C++)", description = "Reports if the top-level class name does not match the file "
		+ "name.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class CppTopLevelClassNameCheck extends CppTopLevelClassCheckBase {

	/** {@inheritDoc} */
	@Override
	protected void analyzeTopLevelTypes(
			List<ShallowEntity> topLevelTypeEntities) throws CheckException {
		for (ShallowEntity entity : topLevelTypeEntities) {
			if (!entity.getName().equals(getFileNameWithoutExtension())) {
				createFindingOnFirstLine("Class name \"" + entity.getName()
						+ "\" does not match file name.", entity);
			}
		}
	}

	/**
	 * Returns the analyzed elements file name without its extension. TODO (AK)
	 * this is an intermediate solution, until UniformPathUtils become available
	 * in the custom check library.
	 */
	private String getFileNameWithoutExtension() {
		String fileName = StringUtils.getLastPart(context.getUniformPath(),
				'/');
		return StringUtils.removeLastPart(fileName, '.');
	}
}
