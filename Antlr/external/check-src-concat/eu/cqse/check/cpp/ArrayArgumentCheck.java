/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import eu.cqse.check.base.EntityCheckBase;
import eu.cqse.check.framework.core.ACheck;
import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckGroupNames;
import eu.cqse.check.framework.core.ECheckParameter;
import eu.cqse.check.framework.core.EFindingEnablement;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Checks if an array appears in the argument list of methods and additionally
 * checks if 'sizeof' is used on this array.
 *
 * @author $Author: baumeister $
 * @version $Rev: $
 * @ConQAT.Rating GREEN Hash: 6100BD8C5F0492F4E233FEAC49DE5554
 */
@ACheck(name = "Methods should not have arrays as arguments.", description = "Having "
		+ "arrays as method argument should be avoided. Using 'sizeof' on the array "
		+ "argument returns the size of the pointer instead of the byte size of the "
		+ "array as they are treated as pointers by the "
		+ "compiler.", groupName = CheckGroupNames.BAD_PRACTICE, defaultEnablement = EFindingEnablement.YELLOW, languages = {
				ELanguage.CPP }, parameters = {
						ECheckParameter.ABSTRACT_SYNTAX_TREE })
public class ArrayArgumentCheck extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//METHOD";
	}

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		List<IToken> tokens = findArrayIdentifiersInArguments(entity);
		createFindingsForArguments(tokens);
		createFindingsForSubStatements(entity, tokens);
	}

	/** Finds all array identifiers that appear in the arguments. */
	private static List<IToken> findArrayIdentifiersInArguments(
			ShallowEntity entity) {
		List<IToken> tokens = entity.ownStartTokens();

		int offset = TokenStreamUtils.findFirstTopLevel(tokens,
				ETokenType.LPAREN, Arrays.asList(ETokenType.LT),
				Arrays.asList(ETokenType.GT));

		List<IToken> identifiers = new ArrayList<>();
		if (offset == TokenStreamUtils.NOT_FOUND) {
			return identifiers;
		}

		List<Integer> offsets = TokenStreamUtils.findTokenTypeSequences(tokens,
				offset, ETokenType.IDENTIFIER, ETokenType.LBRACK);

		for (Integer i : offsets) {
			identifiers.add(tokens.get(i));
		}
		return identifiers;
	}

	/** Creates a finding for every array in the method argument list. */
	private void createFindingsForArguments(List<IToken> tokens)
			throws CheckException {
		for (IToken token : tokens) {
			createFinding("Array in argument list.", token);
		}
	}

	/**
	 * Creates a finding every time 'sizeof' is called on an array from the
	 * method arguments.
	 */
	private void createFindingsForSubStatements(ShallowEntity entity,
			List<IToken> tokens) throws CheckException {
		/*if (!tokens.isEmpty()) {
			List<ShallowEntity> subEntities = select(entity,
					"descendant::STATEMENT");
			List<String> tokenTexts = toText(tokens);
			for (ShallowEntity subEntity : subEntities) {
				createFindingsForUseOfSizeof(subEntity, tokenTexts);
			}
		}*/
	}

	/**
	 * Converts a list of <code>IToken</code>s to a list containing its text
	 * representations.
	 */
	private static List<String> toText(List<IToken> tokens) {
		List<String> tokenTexts = new ArrayList<>();
		for (IToken token : tokens) {
			tokenTexts.add(token.getText());
		}
		return tokenTexts;
	}

	/**
	 * Creates a finding for every use of <code>sizeof</code> on any token in
	 * <code>tokens</code>.
	 */
	private void createFindingsForUseOfSizeof(ShallowEntity entity,
			List<String> tokenTexts) throws CheckException {
		List<IToken> tokens = entity.ownStartTokens();
		List<Integer> offsets = TokenStreamUtils.findTokenTypeSequences(tokens,
				0, ETokenType.SIZEOF, ETokenType.LPAREN, ETokenType.IDENTIFIER);

		for (Integer i : offsets) {
			IToken potentialMatch = tokens.get(i + 2);
			if (tokenTexts.contains(potentialMatch.getText())) {
				createFinding("Use of 'sizeof' on array from argument list.",
						potentialMatch);
			}
		}
	}

}
