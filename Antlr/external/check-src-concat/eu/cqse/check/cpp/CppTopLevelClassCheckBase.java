/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.cpp;

import eu.cqse.check.base.TopLevelTypeCheckBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;
import eu.cqse.check.framework.util.LanguageFeatureParser;

/**
 * Base class for checks that analyze C++ top-level classes and structs.
 * 
 * @author $Author: kupka $
 * @version $Rev: 55962 $
 * @ConQAT.Rating YELLOW Hash: 3EC91DFCC820FA29BE23C213F4AF3BDB
 */
public abstract class CppTopLevelClassCheckBase extends TopLevelTypeCheckBase {

	/** {@inheritDoc} */
	@Override
	protected boolean isTopLevelType(ShallowEntity typeEntity) {
		return LanguageFeatureParser.CPP.isTopLevelClassOrStruct(typeEntity);
	}
}
