/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.base;

import java.util.List;
import java.util.Set;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.SubTypeNames;
import eu.cqse.check.java.StaticMethodCallRecognizer;

/**
 * Parent class that provides the basic functionality for detecting
 * language-specific unwanted calls.
 * 
 * @author $Author: ezaga$
 * @version $Rev: 57096 $
 * @ConQAT.Rating GREEN Hash: 4E88B91197B62BEE6C823A59CA12FBB0
 */
public abstract class UnwantedMethodCallsCheckBase
		extends EntityTokenCheckBase {

	/** A description for this check applicable to all extenders. */
	public static final String CHECK_DESCRIPTION = "Certain methods like console or debugging methods should be avoided in code"
			+ " because they could introduce unwanted side effects or reduce performance.";

	/** Template method for obtaining the list of unwanted methods. */
	protected abstract Set<String> getUnwantedMethods();

	/** {@inheritDoc} */
	@Override
	protected void processTokens(List<IToken> tokens) throws CheckException {
		for (String unwantedMethod : getUnwantedMethods()) {
			createFindingForIndices(
					new StaticMethodCallRecognizer(unwantedMethod), tokens);
		}
	}

	/**
	 * Creates findings for all calls found in the indices.
	 */
	private void createFindingForIndices(StaticMethodCallRecognizer recognizer,
			List<IToken> tokens) throws CheckException {
		for (int callIndex : recognizer.findCallsInTokens(tokens)) {
			createFinding(
					"Method " + recognizer.getFullQualifiedMethodName()
							+ " must not be called",
					tokens.get(callIndex),
					tokens.get(callIndex + recognizer.getTokenLength()));
		}
	}

	/** {@inheritDoc} */
	@Override
	protected String getXPathSelectionString() {
		return "//STATEMENT[subtype('" + SubTypeNames.SIMPLE_STATEMENT + "')]";
	}

}
