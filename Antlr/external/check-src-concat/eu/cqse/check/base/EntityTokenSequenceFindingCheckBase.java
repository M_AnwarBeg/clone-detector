/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.base;

import java.util.List;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;

/**
 * Base class for checks that create findings on occurrences of token sequences.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 56642 $
 * @ConQAT.Rating GREEN Hash: F94EBBB41A1ACB41DF85A4389EE1F672
 */
public abstract class EntityTokenSequenceFindingCheckBase
		extends EntityTokenCheckBase {

	/** {@inheritDoc} */
	@Override
	protected void processTokens(List<IToken> tokens) throws CheckException {
		ETokenType[] findingSequence = getFindingSequence();
		List<Integer> indices = TokenStreamUtils.findTokenTypeSequences(tokens,
				0, findingSequence);
		for (int index : indices) {
			int matchEnd = index + findingSequence.length;
			createFinding(getFindingMessage(tokens, index, matchEnd),
					tokens.get(index), tokens.get(matchEnd - 1));
		}
	}

	/** Returns the token type sequence for which findings should be created. */
	protected abstract ETokenType[] getFindingSequence();

	/**
	 * Returns a finding message for the given token type sequence. The sequence
	 * contains the tokens from matchStart inclusively to matchEnd exclusively.
	 */
	protected abstract String getFindingMessage(List<IToken> tokens,
			int matchStart, int matchEnd);
}
