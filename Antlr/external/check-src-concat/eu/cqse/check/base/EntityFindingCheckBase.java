/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: EntityFindingCheckBase.java 53185 2015-07-03 13:54:40Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.base;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Base class for checks that select a set of entities via xPath and create a
 * finding for each selected entity.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53185 $
 * @ConQAT.Rating GREEN Hash: F5A24E8EC1977EB238A3868AC926DD56
 */
public abstract class EntityFindingCheckBase extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		createFinding(getFindingMessage(entity), entity);
	}

	/** Creates a finding message for the given entity. */
	protected abstract String getFindingMessage(ShallowEntity entity);
}
