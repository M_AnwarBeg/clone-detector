/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: EntityFirstLineFindingCheckBase.java 53935 2015-08-24 08:36:00Z kinnen $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.base;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Base class for checks that select a set of entities via xPath and create a
 * finding on the first line for each selected entity.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 53935 $
 * @ConQAT.Rating GREEN Hash: 210EB488EF995588FAF109015855E1B8
 */
public abstract class EntityFirstLineFindingCheckBase extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		createFindingOnFirstLine(getFindingMessage(entity), entity);
	}

	/** Creates a finding message for the given entity. */
	protected abstract String getFindingMessage(ShallowEntity entity);
}
