/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: EntityCheckBase.java 57564 2016-07-08 10:04:15Z ezaga $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.base;

import java.util.List;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Base class for checks, that select a set of entities via xPath and inspect
 * each of these.
 * 
 * @author $Author: ezaga $
 * @version $Rev: 57564 $
 * @ConQAT.Rating YELLOW Hash: 9CAE504576D6F5D59185D049E421AD55
 */
public abstract class EntityCheckBase extends CheckImplementationBase {

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		/*initialize();
		for (ShallowEntity entity : filterEntities(
				select(getXPathSelectionString()))) {
			processEntity(entity);
		}*/
	}

	/**
	 * Filters the selected entities. The default implementation returns all
	 * selected entities.
	 */
	protected List<ShallowEntity> filterEntities(List<ShallowEntity> entities) {
		return entities;
	}

	/** Returns the xPath string for selecting entities. */
	protected abstract String getXPathSelectionString();

	/** Processes a single selected entity. */
	protected abstract void processEntity(ShallowEntity entity)
			throws CheckException;
}
