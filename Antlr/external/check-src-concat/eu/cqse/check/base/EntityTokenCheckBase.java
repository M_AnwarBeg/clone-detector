/*-----------------------------------------------------------------------+
 | com.teamscale.checks
 |                                                                       |
   $Id: EntityTokenCheckBase.java 52968 2015-06-18 12:03:48Z hummelb $            
 |                                                                       |
 | Copyright (c)  2009-2015 CQSE GmbH                                 |
 +-----------------------------------------------------------------------*/
package eu.cqse.check.base;

import java.util.List;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Base class for checks that select entities via xPath and process the tokens
 * of each selected entity.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52968 $
 * @ConQAT.Rating GREEN Hash: F973E21B7008F6976B4AD44513FED2E5
 */
public abstract class EntityTokenCheckBase extends EntityCheckBase {

	/** {@inheritDoc} */
	@Override
	protected void processEntity(ShallowEntity entity) throws CheckException {
		processTokens(entity.ownStartTokens());
		if (entity.hasChildren()) {
			processTokens(entity.ownEndTokens());
		}
	}

	/** Process the start or end tokens for a selected entity. */
	protected abstract void processTokens(List<IToken> tokens)
			throws CheckException;
}
