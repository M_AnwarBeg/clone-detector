/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.base;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.shallowparser.TokenStreamTextUtils;
import eu.cqse.check.framework.shallowparser.TokenStreamUtils;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Base class for checks creating a finding for certain token text sequences.
 *
 * @author $Author: heinemann $
 * @version $Rev: 54012 $
 * @ConQAT.Rating YELLOW Hash: E15761A5E4A7DF1D41D0D58B6CCF8058
 */
public abstract class TokenTextSequenceCheckBase extends EntityCheckBase {

	/** Checks the given entity */
	protected void checkEntity(ShallowEntity entity, String findingMessage,
			String... tokenTexts) throws CheckException {
		int callIndex = TokenStreamTextUtils.findSequence(
				entity.ownStartTokens(), 0, ETokenType.IDENTIFIER, tokenTexts);
		if (callIndex != TokenStreamUtils.NOT_FOUND) {
			createFindingOnFirstLine(findingMessage, entity);
		}
	}

}
