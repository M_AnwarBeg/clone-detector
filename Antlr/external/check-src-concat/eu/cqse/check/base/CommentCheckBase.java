package eu.cqse.check.base;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.scanner.ETokenType.ETokenClass;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.util.TokenUtils;

/**
 * Base class for checks which inspects all comments in a resource.
 *
 * @author $Author: kinnen $
 * @version $Rev: 53928 $
 * @ConQAT.Rating GREEN Hash: 97EAABB881380F4333037195917A5B1A
 */
public abstract class CommentCheckBase extends CheckImplementationBase {

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		for (int i = 0; i < context.getTokens().size(); i++) {
			IToken token = context.getTokens().get(i);

			if (token.getType().getTokenClass() == ETokenClass.COMMENT) {
				int commentEndLine = 0;

				if (i == context.getTokens().size() - 1) {
					commentEndLine = TokenUtils
							.calculateEndLineByCountingLines(token);
				} else {
					// Estimate the end line for better performance whenever possible.
					commentEndLine = TokenUtils.estimateEndLineByLookahead(
							token, context.getTokens().get(i + 1));
				}

				processComment(token, token.getLineNumber() + 1,
						commentEndLine + 1);
			}
		}
	}

	/**
	 * When implemented in a subclass, processes the specified comment token,
	 * which runs from the provided start line to the provided end line.
	 *
	 * @param token
	 *            the comment token to process
	 * @param startLine
	 *            the 1-based start line of the comment token in the analyzed
	 *            resource.
	 * @param endLine
	 *            the 1-based end line of the comment token in the analyzed
	 *            resource.
	 */
	protected abstract void processComment(IToken token, int startLine,
			int endLine) throws CheckException;
}