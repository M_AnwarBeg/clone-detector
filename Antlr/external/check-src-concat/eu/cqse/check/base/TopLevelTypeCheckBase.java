/*-------------------------------------------------------------------------+
|                                                                          |
| Copyright 2005-2011 the ConQAT Project                                   |
|                                                                          |
| Licensed under the Apache License, Version 2.0 (the "License");          |
| you may not use this file except in compliance with the License.         |
| You may obtain a copy of the License at                                  |
|                                                                          |
|    http://www.apache.org/licenses/LICENSE-2.0                            |
|                                                                          |
| Unless required by applicable law or agreed to in writing, software      |
| distributed under the License is distributed on an "AS IS" BASIS,        |
| WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. |
| See the License for the specific language governing permissions and      |
| limitations under the License.                                           |
+-------------------------------------------------------------------------*/
package eu.cqse.check.base;

import java.util.List;

import org.conqat.lib.commons.collections.CollectionUtils;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.core.CheckImplementationBase;
import eu.cqse.check.framework.shallowparser.framework.ShallowEntity;

/**
 * Base class for checks that analyze top-level types.
 * 
 * @author $Author: kupka $
 * @version $Rev: 55962 $
 * @ConQAT.Rating YELLOW Hash: 6360192C0B61BA78F0937A384E56B60E
 */
public abstract class TopLevelTypeCheckBase extends CheckImplementationBase {

	/** {@inheritDoc} */
	@Override
	public void execute() throws CheckException {
		/*List<ShallowEntity> typeEntities = select(
				getXPathTypeSelectionString());
		analyzeTopLevelTypes(
				CollectionUtils.filter(typeEntities, this::isTopLevelType));*/
	}

	/** Returns the xPath selection string that is used to select all types. */
	protected String getXPathTypeSelectionString() {
		return "//TYPE";
	}

	/** Analyzes the given top-level entities. */
	protected abstract void analyzeTopLevelTypes(
			List<ShallowEntity> topLevelTypeEntities) throws CheckException;

	/** Returns whether the given entity represents a top-level type. */
	protected abstract boolean isTopLevelType(ShallowEntity typeEntity);
}
