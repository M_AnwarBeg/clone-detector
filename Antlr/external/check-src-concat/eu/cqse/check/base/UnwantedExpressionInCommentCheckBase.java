package eu.cqse.check.base;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eu.cqse.check.framework.core.CheckException;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Base class for checks which find unwanted expressions in any comment in a
 * resource.
 *
 * @author $Author: kinnen $
 * @version $Rev: 53936 $
 * @ConQAT.Rating GREEN Hash: 64580A2E415DE0D0A9CB40A3F4DEB502
 */
public abstract class UnwantedExpressionInCommentCheckBase
		extends CommentCheckBase {

	/** Stores the unwanted expression regex. */
	private Pattern unwantedRegex;

	/** {@inheritDoc} */
	@Override
	public void initialize() {
		super.initialize();
		unwantedRegex = getUnwantedRegex();
	}

	/** {@inheritDoc} */
	@Override
	protected void processComment(IToken token, int startLine, int endLine)
			throws CheckException {
		Matcher commentMatcher = unwantedRegex.matcher(token.getText());

		// The pattern might appear anywhere in the token text. Therefore, we
		// use find() instead of matches() to search for it.
		if (commentMatcher.find()) {
			createFinding(getFindingText(token, commentMatcher.toMatchResult()),
					startLine, endLine);
		}
	}

	/**
	 * Provides the Regex pattern that specifies which expressions are unwanted
	 * in comments. Note that this method is only called once at the
	 * initialization of the check for performance reasons.
	 */
	protected abstract Pattern getUnwantedRegex();

	/**
	 * Gets the text for the finding.
	 *
	 * @param token
	 *            The token where the unwanted pattern was found.
	 * @param matchResult
	 *            The {@link MatchResult} which allows providing more details in
	 *            the findings text.
	 * @return The finding text.
	 */
	protected abstract String getFindingText(IToken token,
			MatchResult matchResult);

}
