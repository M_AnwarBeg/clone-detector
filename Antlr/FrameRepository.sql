SET GLOBAL max_allowed_packet = 1024*1024*1024;


CREATE DATABASE IF NOT EXISTS FrameRepository;

USE FrameRepository;

/*Table structure for table `directory` */

CREATE TABLE IF NOT EXISTS `frames` (
  `fid` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `path` varchar(1000) NOT NULL,
  `level` int(11) NOT NULL,
  `links` int(11) NOT NULL,
  `isSynced` int(1) NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `SccFrames` (
  `sccid` int(11) NOT NULL,
  `fid` varchar(1000) NOT NULL,
  PRIMARY KEY (`sccid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `MccFrames` (
  `mccid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  PRIMARY KEY (`mccid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `FccFrames` (
  `fccid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  PRIMARY KEY (`fccid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `McsFrames` (
  `mcsid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  PRIMARY KEY (`mcsid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `FrameDescription` (
  `leveltype` int(11) NOT NULL,
  `leveldescription` varchar(1000) NOT NULL,
  PRIMARY KEY (`leveltype`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `FrameHierarchy` (
  `adaptor` int(11) NOT NULL,
  `adaptee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

insert into framedescription select t.* from ((select 1 as leveltype,'Code Fragment (Simple Clone)' as leveldescription) union all (select 2,'Method Fragment') union all (select 3,'Method SPC') union all (select 4,'Method Template') union all (select 5,'File SPC') union all (select 6,'File Template') union all (select 7,'MCS SPC') union all (select 8,'MCS Template'))t where not exists (select * from framedescription);