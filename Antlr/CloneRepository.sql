/*
SQLyog Enterprise - MySQL GUI v7.12 
MySQL - 8.0.11 : Database - clonedatabase
*********************************************************************
*/


SET GLOBAL max_allowed_packet = 1024*1024*1024*1024*1024;
/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`clonedatabase` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `clonedatabase`;

/*Table structure for table `groupinfo` */

DROP TABLE IF EXISTS `groupinfo`;

CREATE TABLE `groupinfo` (
  `gid` int(11) NOT NULL,
  `gname` varchar(1000) NOT NULL,
  `gpath` varchar(1000) NOT NULL,
  PRIMARY KEY (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `groupinfo` */

/*Table structure for table `directory` */

DROP TABLE IF EXISTS `directory`;

CREATE TABLE `directory` (
  `did` int(11) NOT NULL,
  `dname` varchar(1000) NOT NULL,
  PRIMARY KEY (`did`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `directory` */

/*Table structure for table `fcc` */

DROP TABLE IF EXISTS `fcc`;

CREATE TABLE `fcc` (
  `fcc_id` int(11) NOT NULL,
  `atc` double NOT NULL,
  `apc` double NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`fcc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcc` */

/*Table structure for table `fcc_dir` */

DROP TABLE IF EXISTS `fcc_dir`;

CREATE TABLE `fcc_dir` (
  `fcc_id` int(11) NOT NULL,
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcc_dir` */

/*Table structure for table `fcc_group` */

DROP TABLE IF EXISTS `fcc_group`;

CREATE TABLE `fcc_group` (
  `fcc_id` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcc_group` */

/*Table structure for table `fcc_instance` */

DROP TABLE IF EXISTS `fcc_instance`;

CREATE TABLE `fcc_instance` (
  `fcc_instance_id` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `tc` double NOT NULL,
  `pc` double NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcc_instance` */

/*Table structure for table `fcc_scc` */

DROP TABLE IF EXISTS `fcc_scc`;

CREATE TABLE `fcc_scc` (
  `scc_id` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcc_scc` */

/*Table structure for table `fcs_crossdir` */

DROP TABLE IF EXISTS `fcs_crossdir`;

CREATE TABLE `fcs_crossdir` (
  `fcs_crossdir_id` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`fcs_crossdir_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcs_crossdir` */

/*Table structure for table `fcs_crossgroup` */

DROP TABLE IF EXISTS `fcs_crossgroup`;

CREATE TABLE `fcs_crossgroup` (
  `fcs_crossgroup_id` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`fcs_crossgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcs_crossgroup` */

/*Table structure for table `fcs_indir` */

DROP TABLE IF EXISTS `fcs_indir`;

CREATE TABLE `fcs_indir` (
  `fcs_indir_id` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`fcs_indir_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcs_indir` */

/*Table structure for table `fcs_ingroup` */

DROP TABLE IF EXISTS `fcs_ingroup`;

CREATE TABLE `fcs_ingroup` (
  `fcs_ingroup_id` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`fcs_ingroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcs_ingroup` */

/*Table structure for table `fcscrossdir_dir` */

DROP TABLE IF EXISTS `fcscrossdir_dir`;

CREATE TABLE `fcscrossdir_dir` (
  `fcs_crossdir_id` int(11) NOT NULL,
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcscrossdir_dir` */

/*Table structure for table `fcscrossdir_fcc` */

DROP TABLE IF EXISTS `fcscrossdir_fcc`;

CREATE TABLE `fcscrossdir_fcc` (
  `fcs_crossdir_id` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcscrossdir_fcc` */

/*Table structure for table `fcscrossdir_files` */

DROP TABLE IF EXISTS `fcscrossdir_files`;

CREATE TABLE `fcscrossdir_files` (
  `fcs_crossdir_id` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcscrossdir_files` */

/*Table structure for table `fcscrossgroup_fcc` */

DROP TABLE IF EXISTS `fcscrossgroup_fcc`;

CREATE TABLE `fcscrossgroup_fcc` (
  `fcs_crossgroup_id` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcscrossgroup_fcc` */

/*Table structure for table `fcscrossgroup_files` */

DROP TABLE IF EXISTS `fcscrossgroup_files`;

CREATE TABLE `fcscrossgroup_files` (
  `fcs_crossgroup_id` int(11) NOT NULL,
  `gid` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcscrossgroup_files` */

/*Table structure for table `fcscrossgroup_group` */

DROP TABLE IF EXISTS `fcscrossgroup_group`;

CREATE TABLE `fcscrossgroup_group` (
  `fcs_crossgroup_id` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcscrossgroup_group` */

/*Table structure for table `fcsindir_dir` */

DROP TABLE IF EXISTS `fcsindir_dir`;

CREATE TABLE `fcsindir_dir` (
  `fcs_indir_id` int(11) NOT NULL,
  `did` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcsindir_dir` */

/*Table structure for table `fcsindir_fcc` */

DROP TABLE IF EXISTS `fcsindir_fcc`;

CREATE TABLE `fcsindir_fcc` (
  `fcs_indir_id` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcsindir_fcc` */

/*Table structure for table `fcsindir_files` */

DROP TABLE IF EXISTS `fcsindir_files`;

CREATE TABLE `fcsindir_files` (
  `fcs_indir_id` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL,
  `fcsindir_instance_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcsindir_files` */

/*Table structure for table `fcsingroup_fcc` */

DROP TABLE IF EXISTS `fcsingroup_fcc`;

CREATE TABLE `fcsingroup_fcc` (
  `fcs_ingroup_id` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcsingroup_fcc` */

/*Table structure for table `fcsingroup_files` */

DROP TABLE IF EXISTS `fcsingroup_files`;

CREATE TABLE `fcsingroup_files` (
  `fcs_ingroup_id` int(11) NOT NULL,
  `gid` int(11) NOT NULL,
  `fcc_id` int(11) NOT NULL,
  `fcsingroup_instance_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcsingroup_files` */

/*Table structure for table `fcsingroup_group` */

DROP TABLE IF EXISTS `fcsingroup_group`;

CREATE TABLE `fcsingroup_group` (
  `fcs_ingroup_id` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `fcsingroup_group` */

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `fid` int(11) NOT NULL,
  `fname` varchar(1000) NOT NULL,
  `gid` int(11) NOT NULL,
  PRIMARY KEY (`fid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `file` */

/*Table structure for table `file_directory` */

DROP TABLE IF EXISTS `file_directory`;

CREATE TABLE `file_directory` (
  `fid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `endLine` int(100) NOT NULL,
  KEY `FK_file_directory` (`fid`),
  KEY `FK_file_directory02` (`did`),
  CONSTRAINT `FK_file_directory` FOREIGN KEY (`fid`) REFERENCES `file` (`fid`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_file_directory02` FOREIGN KEY (`did`) REFERENCES `directory` (`did`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `file_directory` */

/*Table structure for table `file_structures` */

DROP TABLE IF EXISTS `file_structures`;

CREATE TABLE `file_structures` (
  `fcs_id` int(11) NOT NULL,
  `instances` int(11) NOT NULL,
  `members` varchar(300) NOT NULL,
  PRIMARY KEY (`fcs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `file_structures` */

/*Table structure for table `file_structures_files` */

DROP TABLE IF EXISTS `file_structures_files`;

CREATE TABLE `file_structures_files` (
  `fcs_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `tc` int(11) NOT NULL,
  `pc` double NOT NULL,
  `support` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `file_structures_files` */

/*Table structure for table `file_structures_scc` */

DROP TABLE IF EXISTS `file_structures_scc`;

CREATE TABLE `file_structures_scc` (
  `fid` int(11) NOT NULL,
  `sccid` int(11) NOT NULL,
  `scc_instance_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `file_structures_scc` */

/*Table structure for table `mcc` */

DROP TABLE IF EXISTS `mcc`;

CREATE TABLE `mcc` (
  `mcc_id` int(11) NOT NULL,
  `atc` double NOT NULL,
  `apc` double NOT NULL,
  `members` int(11) NOT NULL,
  `framed` int(11) DEFAULT NULL,
  `art_files_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`mcc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcc` */

/*Table structure for table `mcc_file` */

DROP TABLE IF EXISTS `mcc_file`;

CREATE TABLE `mcc_file` (
  `mcc_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcc_file` */

/*Table structure for table `mcc_instance` */

DROP TABLE IF EXISTS `mcc_instance`;

CREATE TABLE `mcc_instance` (
  `mcc_instance_id` int(11) NOT NULL,
  `mcc_id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `tc` double NOT NULL,
  `pc` double NOT NULL,
  `fid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL,
  `synced` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcc_instance` */

/*Table structure for table `mcc_scc` */

DROP TABLE IF EXISTS `mcc_scc`;

CREATE TABLE `mcc_scc` (
  `mcc_id` int(11) NOT NULL,
  `scc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcc_scc` */

/*Table structure for table `mcs_crossfile` */

DROP TABLE IF EXISTS `mcs_crossfile`;

CREATE TABLE `mcs_crossfile` (
  `mcs_crossfile_id` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`mcs_crossfile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcs_crossfile` */

/*Table structure for table `mcscrossfile_file` */

DROP TABLE IF EXISTS `mcscrossfile_file`;

CREATE TABLE `mcscrossfile_file` (
  `mcs_crossfile_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcscrossfile_file` */

/*Table structure for table `mcscrossfile_mcc` */

DROP TABLE IF EXISTS `mcscrossfile_mcc`;

CREATE TABLE `mcscrossfile_mcc` (
  `mcs_crossfile_id` int(11) NOT NULL,
  `mcc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcscrossfile_mcc` */

/*Table structure for table `mcscrossfile_methods` */

DROP TABLE IF EXISTS `mcscrossfile_methods`;

CREATE TABLE `mcscrossfile_methods` (
  `mcs_crossfile_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `mcc_id` int(11) NOT NULL,
  `mid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mcscrossfile_methods` */

/*Table structure for table `method` */

DROP TABLE IF EXISTS `method`;

CREATE TABLE `method` (
  `mid` int(11) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `tokens` int(11) NOT NULL,
  `startline` int(11) NOT NULL,
  `endline` int(11) NOT NULL,
  PRIMARY KEY (`mid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `method` */

/*Table structure for table `method_file` */

DROP TABLE IF EXISTS `method_file`;

CREATE TABLE `method_file` (
  `mid` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `startline` int(11) NOT NULL,
  `endline` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `method_file` */

/*Table structure for table `scc` */

DROP TABLE IF EXISTS `scc`;

CREATE TABLE `scc` (
  `scc_id` int(11) NOT NULL,
  `length` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  `benefit` int(11) NOT NULL,
  `RNR` varchar(5) NOT NULL,
  PRIMARY KEY (`scc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scc` */

/*Table structure for table `scc_file` */

DROP TABLE IF EXISTS `scc_file`;

CREATE TABLE `scc_file` (
  `scc_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scc_file` */

/*Table structure for table `scc_instance` */

DROP TABLE IF EXISTS `scc_instance`;

CREATE TABLE `scc_instance` (
  `scc_instance_id` int(11) NOT NULL,
  `scc_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `startline` int(11) NOT NULL,
  `startcol` int(11) NOT NULL,
  `endline` int(11) NOT NULL,
  `endcol` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scc_instance` */

/*Table structure for table `scc_method` */

DROP TABLE IF EXISTS `scc_method`;

CREATE TABLE `scc_method` (
  `scc_id` int(11) NOT NULL,
  `mid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scc_method` */

/*Table structure for table `scs_crossfile` */

DROP TABLE IF EXISTS `scs_crossfile`;

CREATE TABLE `scs_crossfile` (
  `scs_crossfile_id` int(11) NOT NULL,
  `atc` double NOT NULL,
  `apc` double NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`scs_crossfile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scs_crossfile` */

/*Table structure for table `scs_crossmethod` */

DROP TABLE IF EXISTS `scs_crossmethod`;

CREATE TABLE `scs_crossmethod` (
  `scs_crossmethod_id` int(11) NOT NULL,
  `atc` double NOT NULL,
  `apc` double NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`scs_crossmethod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scs_crossmethod` */

/*Table structure for table `scs_infile` */

DROP TABLE IF EXISTS `scs_infile`;

CREATE TABLE `scs_infile` (
  `scs_infile_id` int(11) NOT NULL,
  `members` int(11) NOT NULL,
  PRIMARY KEY (`scs_infile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scs_infile` */

/*Table structure for table `scscrossfile_file` */

DROP TABLE IF EXISTS `scscrossfile_file`;

CREATE TABLE `scscrossfile_file` (
  `scs_crossfile_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `tc` double NOT NULL,
  `pc` double NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scscrossfile_file` */

/*Table structure for table `scscrossfile_scc` */

DROP TABLE IF EXISTS `scscrossfile_scc`;

CREATE TABLE `scscrossfile_scc` (
  `scc_id` int(11) NOT NULL,
  `scs_crossfile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scscrossfile_scc` */

/*Table structure for table `scscrossmethod_method` */

DROP TABLE IF EXISTS `scscrossmethod_method`;

CREATE TABLE `scscrossmethod_method` (
  `scs_crossmethod_id` int(11) NOT NULL,
  `mid` int(11) NOT NULL,
  `tc` double NOT NULL,
  `pc` double NOT NULL,
  `fid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scscrossmethod_method` */

/*Table structure for table `scscrossmethod_scc` */

DROP TABLE IF EXISTS `scscrossmethod_scc`;

CREATE TABLE `scscrossmethod_scc` (
  `scs_crossmethod_id` int(11) NOT NULL,
  `scc_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scscrossmethod_scc` */

/*Table structure for table `scsinfile_file` */

DROP TABLE IF EXISTS `scsinfile_file`;

CREATE TABLE `scsinfile_file` (
  `scs_infile_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `did` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scsinfile_file` */

/*Table structure for table `scsinfile_fragments` */

DROP TABLE IF EXISTS `scsinfile_fragments`;

CREATE TABLE `scsinfile_fragments` (
  `scs_infile_id` int(11) NOT NULL,
  `fid` int(11) NOT NULL,
  `scc_id` int(11) NOT NULL,
  `scsinfile_instance_id` int(11) NOT NULL,
  `scc_instance_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scsinfile_fragments` */

/*Table structure for table `scsinfile_scc` */

DROP TABLE IF EXISTS `scsinfile_scc`;

CREATE TABLE `scsinfile_scc` (
  `scc_id` int(11) NOT NULL,
  `scs_infile_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `scsinfile_scc` */

/*Table structure for table `sysconf` */

DROP TABLE IF EXISTS `sysconf`;

CREATE TABLE `sysconf` (
  `languageselected` varchar(10) DEFAULT NULL,
  `projectname` varchar(50) DEFAULT NULL,
  `projectdiscrp` varchar(150) DEFAULT NULL,
  `threshold` int(11) DEFAULT NULL,
  `groupcheck` int(11) DEFAULT NULL,
  `min_fileclusp` int(11) DEFAULT NULL,
  `min_fileclust` int(11) DEFAULT NULL,
  `min_methodclusp` int(11) DEFAULT NULL,
  `min_methodclust` int(11) DEFAULT NULL,
  `createdate` varchar(20) DEFAULT NULL,
  `lastupdate` varchar(20) DEFAULT NULL,
  `enableframing` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysconf` */

/*Table structure for table `sysconfig_equate_tokens` */

DROP TABLE IF EXISTS `sysconfig_equate_tokens`;

CREATE TABLE `sysconfig_equate_tokens` (
  `rule_number` int(11) DEFAULT NULL,
  `token_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysconfig_equate_tokens` */

/*Table structure for table `sysconfig_supresstoken` */

DROP TABLE IF EXISTS `sysconfig_supresstoken`;

CREATE TABLE `sysconfig_supresstoken` (
  `token_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sysconfig_supresstoken` */

/*Table structure for table `tokens` */

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `number` int(11) DEFAULT NULL,
  `fid` int(11) DEFAULT NULL,
  `line` int(11) DEFAULT NULL,
  `col` int(11) DEFAULT NULL,
  `cls` int(11) DEFAULT NULL,
  `mthd` int(11) DEFAULT NULL,
  `rep` int(11) DEFAULT NULL,
  `tokenId` int(11) DEFAULT NULL,
  `text` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tokens` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;