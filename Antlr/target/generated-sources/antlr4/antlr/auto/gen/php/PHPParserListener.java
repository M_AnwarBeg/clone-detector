// Generated from PHPParser.g4 by ANTLR 4.4

    package antlr.auto.gen.php;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PHPParserParser}.
 */
public interface PHPParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#arrayItem}.
	 * @param ctx the parse tree
	 */
	void enterArrayItem(@NotNull PHPParserParser.ArrayItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#arrayItem}.
	 * @param ctx the parse tree
	 */
	void exitArrayItem(@NotNull PHPParserParser.ArrayItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#arrayItemList}.
	 * @param ctx the parse tree
	 */
	void enterArrayItemList(@NotNull PHPParserParser.ArrayItemListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#arrayItemList}.
	 * @param ctx the parse tree
	 */
	void exitArrayItemList(@NotNull PHPParserParser.ArrayItemListContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ChainExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterChainExpression(@NotNull PHPParserParser.ChainExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ChainExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitChainExpression(@NotNull PHPParserParser.ChainExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArrayCreationExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArrayCreationExpression(@NotNull PHPParserParser.ArrayCreationExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArrayCreationExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArrayCreationExpression(@NotNull PHPParserParser.ArrayCreationExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#constantArrayItemList}.
	 * @param ctx the parse tree
	 */
	void enterConstantArrayItemList(@NotNull PHPParserParser.ConstantArrayItemListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#constantArrayItemList}.
	 * @param ctx the parse tree
	 */
	void exitConstantArrayItemList(@NotNull PHPParserParser.ConstantArrayItemListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#innerStatement}.
	 * @param ctx the parse tree
	 */
	void enterInnerStatement(@NotNull PHPParserParser.InnerStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#innerStatement}.
	 * @param ctx the parse tree
	 */
	void exitInnerStatement(@NotNull PHPParserParser.InnerStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#phpBlock}.
	 * @param ctx the parse tree
	 */
	void enterPhpBlock(@NotNull PHPParserParser.PhpBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#phpBlock}.
	 * @param ctx the parse tree
	 */
	void exitPhpBlock(@NotNull PHPParserParser.PhpBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#traitMethodReference}.
	 * @param ctx the parse tree
	 */
	void enterTraitMethodReference(@NotNull PHPParserParser.TraitMethodReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#traitMethodReference}.
	 * @param ctx the parse tree
	 */
	void exitTraitMethodReference(@NotNull PHPParserParser.TraitMethodReferenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#qualifiedNamespaceNameList}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedNamespaceNameList(@NotNull PHPParserParser.QualifiedNamespaceNameListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#qualifiedNamespaceNameList}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedNamespaceNameList(@NotNull PHPParserParser.QualifiedNamespaceNameListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#traitPrecedence}.
	 * @param ctx the parse tree
	 */
	void enterTraitPrecedence(@NotNull PHPParserParser.TraitPrecedenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#traitPrecedence}.
	 * @param ctx the parse tree
	 */
	void exitTraitPrecedence(@NotNull PHPParserParser.TraitPrecedenceContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#namespaceDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterNamespaceDeclaration(@NotNull PHPParserParser.NamespaceDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#namespaceDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitNamespaceDeclaration(@NotNull PHPParserParser.NamespaceDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#declareList}.
	 * @param ctx the parse tree
	 */
	void enterDeclareList(@NotNull PHPParserParser.DeclareListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#declareList}.
	 * @param ctx the parse tree
	 */
	void exitDeclareList(@NotNull PHPParserParser.DeclareListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#attributeNamedArgList}.
	 * @param ctx the parse tree
	 */
	void enterAttributeNamedArgList(@NotNull PHPParserParser.AttributeNamedArgListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#attributeNamedArgList}.
	 * @param ctx the parse tree
	 */
	void exitAttributeNamedArgList(@NotNull PHPParserParser.AttributeNamedArgListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#keyedFieldName}.
	 * @param ctx the parse tree
	 */
	void enterKeyedFieldName(@NotNull PHPParserParser.KeyedFieldNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#keyedFieldName}.
	 * @param ctx the parse tree
	 */
	void exitKeyedFieldName(@NotNull PHPParserParser.KeyedFieldNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#namespaceNameList}.
	 * @param ctx the parse tree
	 */
	void enterNamespaceNameList(@NotNull PHPParserParser.NamespaceNameListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#namespaceNameList}.
	 * @param ctx the parse tree
	 */
	void exitNamespaceNameList(@NotNull PHPParserParser.NamespaceNameListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void enterExpressionStatement(@NotNull PHPParserParser.ExpressionStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#expressionStatement}.
	 * @param ctx the parse tree
	 */
	void exitExpressionStatement(@NotNull PHPParserParser.ExpressionStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#squareCurlyExpression}.
	 * @param ctx the parse tree
	 */
	void enterSquareCurlyExpression(@NotNull PHPParserParser.SquareCurlyExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#squareCurlyExpression}.
	 * @param ctx the parse tree
	 */
	void exitSquareCurlyExpression(@NotNull PHPParserParser.SquareCurlyExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void enterForUpdate(@NotNull PHPParserParser.ForUpdateContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#forUpdate}.
	 * @param ctx the parse tree
	 */
	void exitForUpdate(@NotNull PHPParserParser.ForUpdateContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#emptyStatement}.
	 * @param ctx the parse tree
	 */
	void enterEmptyStatement(@NotNull PHPParserParser.EmptyStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#emptyStatement}.
	 * @param ctx the parse tree
	 */
	void exitEmptyStatement(@NotNull PHPParserParser.EmptyStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(@NotNull PHPParserParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(@NotNull PHPParserParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#numericConstant}.
	 * @param ctx the parse tree
	 */
	void enterNumericConstant(@NotNull PHPParserParser.NumericConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#numericConstant}.
	 * @param ctx the parse tree
	 */
	void exitNumericConstant(@NotNull PHPParserParser.NumericConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#traitAdaptationStatement}.
	 * @param ctx the parse tree
	 */
	void enterTraitAdaptationStatement(@NotNull PHPParserParser.TraitAdaptationStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#traitAdaptationStatement}.
	 * @param ctx the parse tree
	 */
	void exitTraitAdaptationStatement(@NotNull PHPParserParser.TraitAdaptationStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code NewExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNewExpression(@NotNull PHPParserParser.NewExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code NewExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNewExpression(@NotNull PHPParserParser.NewExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#gotoStatement}.
	 * @param ctx the parse tree
	 */
	void enterGotoStatement(@NotNull PHPParserParser.GotoStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#gotoStatement}.
	 * @param ctx the parse tree
	 */
	void exitGotoStatement(@NotNull PHPParserParser.GotoStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#qualifiedNamespaceName}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedNamespaceName(@NotNull PHPParserParser.QualifiedNamespaceNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#qualifiedNamespaceName}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedNamespaceName(@NotNull PHPParserParser.QualifiedNamespaceNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#htmlElementOrPhpBlock}.
	 * @param ctx the parse tree
	 */
	void enterHtmlElementOrPhpBlock(@NotNull PHPParserParser.HtmlElementOrPhpBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#htmlElementOrPhpBlock}.
	 * @param ctx the parse tree
	 */
	void exitHtmlElementOrPhpBlock(@NotNull PHPParserParser.HtmlElementOrPhpBlockContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PrefixIncDecExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrefixIncDecExpression(@NotNull PHPParserParser.PrefixIncDecExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PrefixIncDecExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrefixIncDecExpression(@NotNull PHPParserParser.PrefixIncDecExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ComparisonExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterComparisonExpression(@NotNull PHPParserParser.ComparisonExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ComparisonExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitComparisonExpression(@NotNull PHPParserParser.ComparisonExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void enterExpressionList(@NotNull PHPParserParser.ExpressionListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#expressionList}.
	 * @param ctx the parse tree
	 */
	void exitExpressionList(@NotNull PHPParserParser.ExpressionListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#foreachStatement}.
	 * @param ctx the parse tree
	 */
	void enterForeachStatement(@NotNull PHPParserParser.ForeachStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#foreachStatement}.
	 * @param ctx the parse tree
	 */
	void exitForeachStatement(@NotNull PHPParserParser.ForeachStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCall(@NotNull PHPParserParser.FunctionCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#functionCall}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCall(@NotNull PHPParserParser.FunctionCallContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#staticVariableStatement}.
	 * @param ctx the parse tree
	 */
	void enterStaticVariableStatement(@NotNull PHPParserParser.StaticVariableStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#staticVariableStatement}.
	 * @param ctx the parse tree
	 */
	void exitStaticVariableStatement(@NotNull PHPParserParser.StaticVariableStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#baseCtorCall}.
	 * @param ctx the parse tree
	 */
	void enterBaseCtorCall(@NotNull PHPParserParser.BaseCtorCallContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#baseCtorCall}.
	 * @param ctx the parse tree
	 */
	void exitBaseCtorCall(@NotNull PHPParserParser.BaseCtorCallContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LambdaFunctionExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLambdaFunctionExpression(@NotNull PHPParserParser.LambdaFunctionExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LambdaFunctionExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLambdaFunctionExpression(@NotNull PHPParserParser.LambdaFunctionExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BitwiseExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBitwiseExpression(@NotNull PHPParserParser.BitwiseExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BitwiseExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBitwiseExpression(@NotNull PHPParserParser.BitwiseExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#classConstant}.
	 * @param ctx the parse tree
	 */
	void enterClassConstant(@NotNull PHPParserParser.ClassConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#classConstant}.
	 * @param ctx the parse tree
	 */
	void exitClassConstant(@NotNull PHPParserParser.ClassConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#newExpr}.
	 * @param ctx the parse tree
	 */
	void enterNewExpr(@NotNull PHPParserParser.NewExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#newExpr}.
	 * @param ctx the parse tree
	 */
	void exitNewExpr(@NotNull PHPParserParser.NewExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#identifierInititalizer}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierInititalizer(@NotNull PHPParserParser.IdentifierInititalizerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#identifierInititalizer}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierInititalizer(@NotNull PHPParserParser.IdentifierInititalizerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void enterImportStatement(@NotNull PHPParserParser.ImportStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#importStatement}.
	 * @param ctx the parse tree
	 */
	void exitImportStatement(@NotNull PHPParserParser.ImportStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void enterReturnStatement(@NotNull PHPParserParser.ReturnStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#returnStatement}.
	 * @param ctx the parse tree
	 */
	void exitReturnStatement(@NotNull PHPParserParser.ReturnStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeHint}.
	 * @param ctx the parse tree
	 */
	void enterTypeHint(@NotNull PHPParserParser.TypeHintContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeHint}.
	 * @param ctx the parse tree
	 */
	void exitTypeHint(@NotNull PHPParserParser.TypeHintContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#keyedSimpleFieldName}.
	 * @param ctx the parse tree
	 */
	void enterKeyedSimpleFieldName(@NotNull PHPParserParser.KeyedSimpleFieldNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#keyedSimpleFieldName}.
	 * @param ctx the parse tree
	 */
	void exitKeyedSimpleFieldName(@NotNull PHPParserParser.KeyedSimpleFieldNameContext ctx);
	/**
	 * Enter a parse tree produced by the {@code LogicalExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLogicalExpression(@NotNull PHPParserParser.LogicalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code LogicalExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLogicalExpression(@NotNull PHPParserParser.LogicalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#actualArguments}.
	 * @param ctx the parse tree
	 */
	void enterActualArguments(@NotNull PHPParserParser.ActualArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#actualArguments}.
	 * @param ctx the parse tree
	 */
	void exitActualArguments(@NotNull PHPParserParser.ActualArgumentsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#catchClause}.
	 * @param ctx the parse tree
	 */
	void enterCatchClause(@NotNull PHPParserParser.CatchClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#catchClause}.
	 * @param ctx the parse tree
	 */
	void exitCatchClause(@NotNull PHPParserParser.CatchClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#innerStatementList}.
	 * @param ctx the parse tree
	 */
	void enterInnerStatementList(@NotNull PHPParserParser.InnerStatementListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#innerStatementList}.
	 * @param ctx the parse tree
	 */
	void exitInnerStatementList(@NotNull PHPParserParser.InnerStatementListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#lambdaFunctionUseVars}.
	 * @param ctx the parse tree
	 */
	void enterLambdaFunctionUseVars(@NotNull PHPParserParser.LambdaFunctionUseVarsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#lambdaFunctionUseVars}.
	 * @param ctx the parse tree
	 */
	void exitLambdaFunctionUseVars(@NotNull PHPParserParser.LambdaFunctionUseVarsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#constantArrayItem}.
	 * @param ctx the parse tree
	 */
	void enterConstantArrayItem(@NotNull PHPParserParser.ConstantArrayItemContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#constantArrayItem}.
	 * @param ctx the parse tree
	 */
	void exitConstantArrayItem(@NotNull PHPParserParser.ConstantArrayItemContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#attribute}.
	 * @param ctx the parse tree
	 */
	void enterAttribute(@NotNull PHPParserParser.AttributeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#attribute}.
	 * @param ctx the parse tree
	 */
	void exitAttribute(@NotNull PHPParserParser.AttributeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#elseIfColonStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseIfColonStatement(@NotNull PHPParserParser.ElseIfColonStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#elseIfColonStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseIfColonStatement(@NotNull PHPParserParser.ElseIfColonStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameter(@NotNull PHPParserParser.FormalParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#formalParameter}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameter(@NotNull PHPParserParser.FormalParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void enterVariableInitializer(@NotNull PHPParserParser.VariableInitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#variableInitializer}.
	 * @param ctx the parse tree
	 */
	void exitVariableInitializer(@NotNull PHPParserParser.VariableInitializerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#lambdaFunctionUseVar}.
	 * @param ctx the parse tree
	 */
	void enterLambdaFunctionUseVar(@NotNull PHPParserParser.LambdaFunctionUseVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#lambdaFunctionUseVar}.
	 * @param ctx the parse tree
	 */
	void exitLambdaFunctionUseVar(@NotNull PHPParserParser.LambdaFunctionUseVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#chain}.
	 * @param ctx the parse tree
	 */
	void enterChain(@NotNull PHPParserParser.ChainContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#chain}.
	 * @param ctx the parse tree
	 */
	void exitChain(@NotNull PHPParserParser.ChainContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#castOperation}.
	 * @param ctx the parse tree
	 */
	void enterCastOperation(@NotNull PHPParserParser.CastOperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#castOperation}.
	 * @param ctx the parse tree
	 */
	void exitCastOperation(@NotNull PHPParserParser.CastOperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#unsetStatement}.
	 * @param ctx the parse tree
	 */
	void enterUnsetStatement(@NotNull PHPParserParser.UnsetStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#unsetStatement}.
	 * @param ctx the parse tree
	 */
	void exitUnsetStatement(@NotNull PHPParserParser.UnsetStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#throwStatement}.
	 * @param ctx the parse tree
	 */
	void enterThrowStatement(@NotNull PHPParserParser.ThrowStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#throwStatement}.
	 * @param ctx the parse tree
	 */
	void exitThrowStatement(@NotNull PHPParserParser.ThrowStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#traitAdaptations}.
	 * @param ctx the parse tree
	 */
	void enterTraitAdaptations(@NotNull PHPParserParser.TraitAdaptationsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#traitAdaptations}.
	 * @param ctx the parse tree
	 */
	void exitTraitAdaptations(@NotNull PHPParserParser.TraitAdaptationsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#topStatement}.
	 * @param ctx the parse tree
	 */
	void enterTopStatement(@NotNull PHPParserParser.TopStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#topStatement}.
	 * @param ctx the parse tree
	 */
	void exitTopStatement(@NotNull PHPParserParser.TopStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void enterForStatement(@NotNull PHPParserParser.ForStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#forStatement}.
	 * @param ctx the parse tree
	 */
	void exitForStatement(@NotNull PHPParserParser.ForStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ArithmeticExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArithmeticExpression(@NotNull PHPParserParser.ArithmeticExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ArithmeticExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArithmeticExpression(@NotNull PHPParserParser.ArithmeticExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#elseIfStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseIfStatement(@NotNull PHPParserParser.ElseIfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#elseIfStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseIfStatement(@NotNull PHPParserParser.ElseIfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#assignmentList}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentList(@NotNull PHPParserParser.AssignmentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#assignmentList}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentList(@NotNull PHPParserParser.AssignmentListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#finallyStatement}.
	 * @param ctx the parse tree
	 */
	void enterFinallyStatement(@NotNull PHPParserParser.FinallyStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#finallyStatement}.
	 * @param ctx the parse tree
	 */
	void exitFinallyStatement(@NotNull PHPParserParser.FinallyStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#qualifiedStaticTypeRef}.
	 * @param ctx the parse tree
	 */
	void enterQualifiedStaticTypeRef(@NotNull PHPParserParser.QualifiedStaticTypeRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#qualifiedStaticTypeRef}.
	 * @param ctx the parse tree
	 */
	void exitQualifiedStaticTypeRef(@NotNull PHPParserParser.QualifiedStaticTypeRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#chainList}.
	 * @param ctx the parse tree
	 */
	void enterChainList(@NotNull PHPParserParser.ChainListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#chainList}.
	 * @param ctx the parse tree
	 */
	void exitChainList(@NotNull PHPParserParser.ChainListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#yieldExpression}.
	 * @param ctx the parse tree
	 */
	void enterYieldExpression(@NotNull PHPParserParser.YieldExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#yieldExpression}.
	 * @param ctx the parse tree
	 */
	void exitYieldExpression(@NotNull PHPParserParser.YieldExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#functionCallName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionCallName(@NotNull PHPParserParser.FunctionCallNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#functionCallName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionCallName(@NotNull PHPParserParser.FunctionCallNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#genericDynamicArgs}.
	 * @param ctx the parse tree
	 */
	void enterGenericDynamicArgs(@NotNull PHPParserParser.GenericDynamicArgsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#genericDynamicArgs}.
	 * @param ctx the parse tree
	 */
	void exitGenericDynamicArgs(@NotNull PHPParserParser.GenericDynamicArgsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#attributes}.
	 * @param ctx the parse tree
	 */
	void enterAttributes(@NotNull PHPParserParser.AttributesContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#attributes}.
	 * @param ctx the parse tree
	 */
	void exitAttributes(@NotNull PHPParserParser.AttributesContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#tryCatchFinally}.
	 * @param ctx the parse tree
	 */
	void enterTryCatchFinally(@NotNull PHPParserParser.TryCatchFinallyContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#tryCatchFinally}.
	 * @param ctx the parse tree
	 */
	void exitTryCatchFinally(@NotNull PHPParserParser.TryCatchFinallyContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#globalStatement}.
	 * @param ctx the parse tree
	 */
	void enterGlobalStatement(@NotNull PHPParserParser.GlobalStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#globalStatement}.
	 * @param ctx the parse tree
	 */
	void exitGlobalStatement(@NotNull PHPParserParser.GlobalStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#memberModifier}.
	 * @param ctx the parse tree
	 */
	void enterMemberModifier(@NotNull PHPParserParser.MemberModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#memberModifier}.
	 * @param ctx the parse tree
	 */
	void exitMemberModifier(@NotNull PHPParserParser.MemberModifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDeclaration(@NotNull PHPParserParser.FunctionDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#functionDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDeclaration(@NotNull PHPParserParser.FunctionDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#nonEmptyStatement}.
	 * @param ctx the parse tree
	 */
	void enterNonEmptyStatement(@NotNull PHPParserParser.NonEmptyStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#nonEmptyStatement}.
	 * @param ctx the parse tree
	 */
	void exitNonEmptyStatement(@NotNull PHPParserParser.NonEmptyStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeParameterListInBrackets}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameterListInBrackets(@NotNull PHPParserParser.TypeParameterListInBracketsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeParameterListInBrackets}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameterListInBrackets(@NotNull PHPParserParser.TypeParameterListInBracketsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(@NotNull PHPParserParser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(@NotNull PHPParserParser.ConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#echoStatement}.
	 * @param ctx the parse tree
	 */
	void enterEchoStatement(@NotNull PHPParserParser.EchoStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#echoStatement}.
	 * @param ctx the parse tree
	 */
	void exitEchoStatement(@NotNull PHPParserParser.EchoStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code BackQuoteStringExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBackQuoteStringExpression(@NotNull PHPParserParser.BackQuoteStringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code BackQuoteStringExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBackQuoteStringExpression(@NotNull PHPParserParser.BackQuoteStringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentOperator(@NotNull PHPParserParser.AssignmentOperatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#assignmentOperator}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentOperator(@NotNull PHPParserParser.AssignmentOperatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#modifier}.
	 * @param ctx the parse tree
	 */
	void enterModifier(@NotNull PHPParserParser.ModifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#modifier}.
	 * @param ctx the parse tree
	 */
	void exitModifier(@NotNull PHPParserParser.ModifierContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#htmlElements}.
	 * @param ctx the parse tree
	 */
	void enterHtmlElements(@NotNull PHPParserParser.HtmlElementsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#htmlElements}.
	 * @param ctx the parse tree
	 */
	void exitHtmlElements(@NotNull PHPParserParser.HtmlElementsContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#elseColonStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseColonStatement(@NotNull PHPParserParser.ElseColonStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#elseColonStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseColonStatement(@NotNull PHPParserParser.ElseColonStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#doWhileStatement}.
	 * @param ctx the parse tree
	 */
	void enterDoWhileStatement(@NotNull PHPParserParser.DoWhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#doWhileStatement}.
	 * @param ctx the parse tree
	 */
	void exitDoWhileStatement(@NotNull PHPParserParser.DoWhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#globalVar}.
	 * @param ctx the parse tree
	 */
	void enterGlobalVar(@NotNull PHPParserParser.GlobalVarContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#globalVar}.
	 * @param ctx the parse tree
	 */
	void exitGlobalVar(@NotNull PHPParserParser.GlobalVarContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#interpolatedStringPart}.
	 * @param ctx the parse tree
	 */
	void enterInterpolatedStringPart(@NotNull PHPParserParser.InterpolatedStringPartContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#interpolatedStringPart}.
	 * @param ctx the parse tree
	 */
	void exitInterpolatedStringPart(@NotNull PHPParserParser.InterpolatedStringPartContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#propertyModifiers}.
	 * @param ctx the parse tree
	 */
	void enterPropertyModifiers(@NotNull PHPParserParser.PropertyModifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#propertyModifiers}.
	 * @param ctx the parse tree
	 */
	void exitPropertyModifiers(@NotNull PHPParserParser.PropertyModifiersContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CloneExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCloneExpression(@NotNull PHPParserParser.CloneExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CloneExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCloneExpression(@NotNull PHPParserParser.CloneExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#memberAccess}.
	 * @param ctx the parse tree
	 */
	void enterMemberAccess(@NotNull PHPParserParser.MemberAccessContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#memberAccess}.
	 * @param ctx the parse tree
	 */
	void exitMemberAccess(@NotNull PHPParserParser.MemberAccessContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#useDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterUseDeclaration(@NotNull PHPParserParser.UseDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#useDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitUseDeclaration(@NotNull PHPParserParser.UseDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#magicMethod}.
	 * @param ctx the parse tree
	 */
	void enterMagicMethod(@NotNull PHPParserParser.MagicMethodContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#magicMethod}.
	 * @param ctx the parse tree
	 */
	void exitMagicMethod(@NotNull PHPParserParser.MagicMethodContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ConditionalExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalExpression(@NotNull PHPParserParser.ConditionalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ConditionalExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalExpression(@NotNull PHPParserParser.ConditionalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeParameterList}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameterList(@NotNull PHPParserParser.TypeParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeParameterList}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameterList(@NotNull PHPParserParser.TypeParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#parenthesis}.
	 * @param ctx the parse tree
	 */
	void enterParenthesis(@NotNull PHPParserParser.ParenthesisContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#parenthesis}.
	 * @param ctx the parse tree
	 */
	void exitParenthesis(@NotNull PHPParserParser.ParenthesisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IndexerExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIndexerExpression(@NotNull PHPParserParser.IndexerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IndexerExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIndexerExpression(@NotNull PHPParserParser.IndexerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ScalarExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterScalarExpression(@NotNull PHPParserParser.ScalarExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ScalarExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitScalarExpression(@NotNull PHPParserParser.ScalarExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#formalParameterList}.
	 * @param ctx the parse tree
	 */
	void enterFormalParameterList(@NotNull PHPParserParser.FormalParameterListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#formalParameterList}.
	 * @param ctx the parse tree
	 */
	void exitFormalParameterList(@NotNull PHPParserParser.FormalParameterListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#elseStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseStatement(@NotNull PHPParserParser.ElseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#elseStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseStatement(@NotNull PHPParserParser.ElseStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#globalConstantDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterGlobalConstantDeclaration(@NotNull PHPParserParser.GlobalConstantDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#globalConstantDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitGlobalConstantDeclaration(@NotNull PHPParserParser.GlobalConstantDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeParameterDecl}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameterDecl(@NotNull PHPParserParser.TypeParameterDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeParameterDecl}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameterDecl(@NotNull PHPParserParser.TypeParameterDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#chainBase}.
	 * @param ctx the parse tree
	 */
	void enterChainBase(@NotNull PHPParserParser.ChainBaseContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#chainBase}.
	 * @param ctx the parse tree
	 */
	void exitChainBase(@NotNull PHPParserParser.ChainBaseContext ctx);
	/**
	 * Enter a parse tree produced by the {@code CastExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCastExpression(@NotNull PHPParserParser.CastExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code CastExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCastExpression(@NotNull PHPParserParser.CastExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code InstanceOfExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInstanceOfExpression(@NotNull PHPParserParser.InstanceOfExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code InstanceOfExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInstanceOfExpression(@NotNull PHPParserParser.InstanceOfExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#htmlElement}.
	 * @param ctx the parse tree
	 */
	void enterHtmlElement(@NotNull PHPParserParser.HtmlElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#htmlElement}.
	 * @param ctx the parse tree
	 */
	void exitHtmlElement(@NotNull PHPParserParser.HtmlElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#inlineHtml}.
	 * @param ctx the parse tree
	 */
	void enterInlineHtml(@NotNull PHPParserParser.InlineHtmlContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#inlineHtml}.
	 * @param ctx the parse tree
	 */
	void exitInlineHtml(@NotNull PHPParserParser.InlineHtmlContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#htmlDocument}.
	 * @param ctx the parse tree
	 */
	void enterHtmlDocument(@NotNull PHPParserParser.HtmlDocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#htmlDocument}.
	 * @param ctx the parse tree
	 */
	void exitHtmlDocument(@NotNull PHPParserParser.HtmlDocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#classEntryType}.
	 * @param ctx the parse tree
	 */
	void enterClassEntryType(@NotNull PHPParserParser.ClassEntryTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#classEntryType}.
	 * @param ctx the parse tree
	 */
	void exitClassEntryType(@NotNull PHPParserParser.ClassEntryTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#methodBody}.
	 * @param ctx the parse tree
	 */
	void enterMethodBody(@NotNull PHPParserParser.MethodBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#methodBody}.
	 * @param ctx the parse tree
	 */
	void exitMethodBody(@NotNull PHPParserParser.MethodBodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code SpecialWordExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterSpecialWordExpression(@NotNull PHPParserParser.SpecialWordExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code SpecialWordExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitSpecialWordExpression(@NotNull PHPParserParser.SpecialWordExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#namespaceStatement}.
	 * @param ctx the parse tree
	 */
	void enterNamespaceStatement(@NotNull PHPParserParser.NamespaceStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#namespaceStatement}.
	 * @param ctx the parse tree
	 */
	void exitNamespaceStatement(@NotNull PHPParserParser.NamespaceStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#declareStatement}.
	 * @param ctx the parse tree
	 */
	void enterDeclareStatement(@NotNull PHPParserParser.DeclareStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#declareStatement}.
	 * @param ctx the parse tree
	 */
	void exitDeclareStatement(@NotNull PHPParserParser.DeclareStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(@NotNull PHPParserParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(@NotNull PHPParserParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#magicConstant}.
	 * @param ctx the parse tree
	 */
	void enterMagicConstant(@NotNull PHPParserParser.MagicConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#magicConstant}.
	 * @param ctx the parse tree
	 */
	void exitMagicConstant(@NotNull PHPParserParser.MagicConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeParameterWithDefaultDecl}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameterWithDefaultDecl(@NotNull PHPParserParser.TypeParameterWithDefaultDeclContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeParameterWithDefaultDecl}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameterWithDefaultDecl(@NotNull PHPParserParser.TypeParameterWithDefaultDeclContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeParameterWithDefaultsList}.
	 * @param ctx the parse tree
	 */
	void enterTypeParameterWithDefaultsList(@NotNull PHPParserParser.TypeParameterWithDefaultsListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeParameterWithDefaultsList}.
	 * @param ctx the parse tree
	 */
	void exitTypeParameterWithDefaultsList(@NotNull PHPParserParser.TypeParameterWithDefaultsListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#interfaceList}.
	 * @param ctx the parse tree
	 */
	void enterInterfaceList(@NotNull PHPParserParser.InterfaceListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#interfaceList}.
	 * @param ctx the parse tree
	 */
	void exitInterfaceList(@NotNull PHPParserParser.InterfaceListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#constantInititalizer}.
	 * @param ctx the parse tree
	 */
	void enterConstantInititalizer(@NotNull PHPParserParser.ConstantInititalizerContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#constantInititalizer}.
	 * @param ctx the parse tree
	 */
	void exitConstantInititalizer(@NotNull PHPParserParser.ConstantInititalizerContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#memberModifiers}.
	 * @param ctx the parse tree
	 */
	void enterMemberModifiers(@NotNull PHPParserParser.MemberModifiersContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#memberModifiers}.
	 * @param ctx the parse tree
	 */
	void exitMemberModifiers(@NotNull PHPParserParser.MemberModifiersContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#switchStatement}.
	 * @param ctx the parse tree
	 */
	void enterSwitchStatement(@NotNull PHPParserParser.SwitchStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#switchStatement}.
	 * @param ctx the parse tree
	 */
	void exitSwitchStatement(@NotNull PHPParserParser.SwitchStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#indirectTypeRef}.
	 * @param ctx the parse tree
	 */
	void enterIndirectTypeRef(@NotNull PHPParserParser.IndirectTypeRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#indirectTypeRef}.
	 * @param ctx the parse tree
	 */
	void exitIndirectTypeRef(@NotNull PHPParserParser.IndirectTypeRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(@NotNull PHPParserParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(@NotNull PHPParserParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PrintExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrintExpression(@NotNull PHPParserParser.PrintExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PrintExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrintExpression(@NotNull PHPParserParser.PrintExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code AssignmentExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentExpression(@NotNull PHPParserParser.AssignmentExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code AssignmentExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentExpression(@NotNull PHPParserParser.AssignmentExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PostfixIncDecExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfixIncDecExpression(@NotNull PHPParserParser.PostfixIncDecExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PostfixIncDecExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfixIncDecExpression(@NotNull PHPParserParser.PostfixIncDecExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#stringConstant}.
	 * @param ctx the parse tree
	 */
	void enterStringConstant(@NotNull PHPParserParser.StringConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#stringConstant}.
	 * @param ctx the parse tree
	 */
	void exitStringConstant(@NotNull PHPParserParser.StringConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull PHPParserParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull PHPParserParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#traitAlias}.
	 * @param ctx the parse tree
	 */
	void enterTraitAlias(@NotNull PHPParserParser.TraitAliasContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#traitAlias}.
	 * @param ctx the parse tree
	 */
	void exitTraitAlias(@NotNull PHPParserParser.TraitAliasContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#keyedVariable}.
	 * @param ctx the parse tree
	 */
	void enterKeyedVariable(@NotNull PHPParserParser.KeyedVariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#keyedVariable}.
	 * @param ctx the parse tree
	 */
	void exitKeyedVariable(@NotNull PHPParserParser.KeyedVariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#assignmentListElement}.
	 * @param ctx the parse tree
	 */
	void enterAssignmentListElement(@NotNull PHPParserParser.AssignmentListElementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#assignmentListElement}.
	 * @param ctx the parse tree
	 */
	void exitAssignmentListElement(@NotNull PHPParserParser.AssignmentListElementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void enterPrimitiveType(@NotNull PHPParserParser.PrimitiveTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#primitiveType}.
	 * @param ctx the parse tree
	 */
	void exitPrimitiveType(@NotNull PHPParserParser.PrimitiveTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#useDeclarationContentList}.
	 * @param ctx the parse tree
	 */
	void enterUseDeclarationContentList(@NotNull PHPParserParser.UseDeclarationContentListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#useDeclarationContentList}.
	 * @param ctx the parse tree
	 */
	void exitUseDeclarationContentList(@NotNull PHPParserParser.UseDeclarationContentListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#attributeNamedArg}.
	 * @param ctx the parse tree
	 */
	void enterAttributeNamedArg(@NotNull PHPParserParser.AttributeNamedArgContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#attributeNamedArg}.
	 * @param ctx the parse tree
	 */
	void exitAttributeNamedArg(@NotNull PHPParserParser.AttributeNamedArgContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void enterTypeRef(@NotNull PHPParserParser.TypeRefContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#typeRef}.
	 * @param ctx the parse tree
	 */
	void exitTypeRef(@NotNull PHPParserParser.TypeRefContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void enterBlockStatement(@NotNull PHPParserParser.BlockStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#blockStatement}.
	 * @param ctx the parse tree
	 */
	void exitBlockStatement(@NotNull PHPParserParser.BlockStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code UnaryOperatorExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryOperatorExpression(@NotNull PHPParserParser.UnaryOperatorExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code UnaryOperatorExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryOperatorExpression(@NotNull PHPParserParser.UnaryOperatorExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ParenthesisExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParenthesisExpression(@NotNull PHPParserParser.ParenthesisExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ParenthesisExpression}
	 * labeled alternative in {@link PHPParserParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParenthesisExpression(@NotNull PHPParserParser.ParenthesisExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#useDeclarationContent}.
	 * @param ctx the parse tree
	 */
	void enterUseDeclarationContent(@NotNull PHPParserParser.UseDeclarationContentContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#useDeclarationContent}.
	 * @param ctx the parse tree
	 */
	void exitUseDeclarationContent(@NotNull PHPParserParser.UseDeclarationContentContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void enterBreakStatement(@NotNull PHPParserParser.BreakStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#breakStatement}.
	 * @param ctx the parse tree
	 */
	void exitBreakStatement(@NotNull PHPParserParser.BreakStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#classStatement}.
	 * @param ctx the parse tree
	 */
	void enterClassStatement(@NotNull PHPParserParser.ClassStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#classStatement}.
	 * @param ctx the parse tree
	 */
	void exitClassStatement(@NotNull PHPParserParser.ClassStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(@NotNull PHPParserParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(@NotNull PHPParserParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#actualArgument}.
	 * @param ctx the parse tree
	 */
	void enterActualArgument(@NotNull PHPParserParser.ActualArgumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#actualArgument}.
	 * @param ctx the parse tree
	 */
	void exitActualArgument(@NotNull PHPParserParser.ActualArgumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#attributesGroup}.
	 * @param ctx the parse tree
	 */
	void enterAttributesGroup(@NotNull PHPParserParser.AttributesGroupContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#attributesGroup}.
	 * @param ctx the parse tree
	 */
	void exitAttributesGroup(@NotNull PHPParserParser.AttributesGroupContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStatement(@NotNull PHPParserParser.WhileStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#whileStatement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStatement(@NotNull PHPParserParser.WhileStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#attributeArgList}.
	 * @param ctx the parse tree
	 */
	void enterAttributeArgList(@NotNull PHPParserParser.AttributeArgListContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#attributeArgList}.
	 * @param ctx the parse tree
	 */
	void exitAttributeArgList(@NotNull PHPParserParser.AttributeArgListContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#scriptTextPart}.
	 * @param ctx the parse tree
	 */
	void enterScriptTextPart(@NotNull PHPParserParser.ScriptTextPartContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#scriptTextPart}.
	 * @param ctx the parse tree
	 */
	void exitScriptTextPart(@NotNull PHPParserParser.ScriptTextPartContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#switchBlock}.
	 * @param ctx the parse tree
	 */
	void enterSwitchBlock(@NotNull PHPParserParser.SwitchBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#switchBlock}.
	 * @param ctx the parse tree
	 */
	void exitSwitchBlock(@NotNull PHPParserParser.SwitchBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#forInit}.
	 * @param ctx the parse tree
	 */
	void enterForInit(@NotNull PHPParserParser.ForInitContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#forInit}.
	 * @param ctx the parse tree
	 */
	void exitForInit(@NotNull PHPParserParser.ForInitContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#literalConstant}.
	 * @param ctx the parse tree
	 */
	void enterLiteralConstant(@NotNull PHPParserParser.LiteralConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#literalConstant}.
	 * @param ctx the parse tree
	 */
	void exitLiteralConstant(@NotNull PHPParserParser.LiteralConstantContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void enterContinueStatement(@NotNull PHPParserParser.ContinueStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#continueStatement}.
	 * @param ctx the parse tree
	 */
	void exitContinueStatement(@NotNull PHPParserParser.ContinueStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link PHPParserParser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(@NotNull PHPParserParser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PHPParserParser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(@NotNull PHPParserParser.ArgumentsContext ctx);
}