package cmt.cvac.treemapvisualization;

import java.util.ArrayList;

public class DummyMethodNode {
	
	public String name;
	public int size;
	int color;
	public int mid;
	
	public DummyMethodNode(String n,int s)
	{
		name=n;
		size=s;
		color=0;
		mid=-1;
	}
	public void setColor(int red)
	{
		color=red;
	}
	public void setMid(int id)
	{
		mid=id;
	}
}

