package cmt.cvac.treemapvisualization;

import java.util.ArrayList;

public class DummyNode {
	
	public String name;
	public String title;
	public int size;
	public ArrayList<Integer> children;
	int color;
	boolean isdirectory;
	
	public DummyNode(String n,int s)
	{
		name=n;
		size=s;
		children=new ArrayList();
		color=0;
		title="";
	}
	public void setColor(int red)
	{
		color=red;
	}
	public void setTitle(String t)
	{
		this.title=t;
	}

}

