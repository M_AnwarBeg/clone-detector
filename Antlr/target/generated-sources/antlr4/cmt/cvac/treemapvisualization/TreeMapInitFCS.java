package cmt.cvac.treemapvisualization;



import javax.swing.*;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import com.macrofocus.treemap.AlgorithmFactory;
import com.macrofocus.treemap.DefaultTreeMapController;
import com.macrofocus.treemap.LeafTreeMapNode;
import com.macrofocus.treemap.TreeMap;
import com.macrofocus.treemap.TreeMapNode;

import cmt.cvac.views.MccView;
import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonedetectioninitializer.SysConfig;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;

public class TreeMapInitFCS {
	
	public static JFrame frame;
	public static TreeMap treeMap;
	
    public static void init(ArrayList<String> fname, final int sccID,final ArrayList<Integer> mids) {
    	
    	final ArrayList<File> list=new ArrayList();
    	ArrayList<DummyNode> allNodes=new ArrayList();
    	ArrayList<DefaultMutableTreeNode> nodes = new ArrayList();
    	ArrayList<String> allFiles=new ArrayList<>();
    	
    	int checkSize=0;
    	try
    	{
    		CloneRepository.openConnection();
	    	Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+CloneRepository.getDBName()+";");
		    ResultSet rs = stmt.executeQuery("select dname from directory");
		    
		    while(rs.next())
		    {
		    	File directory = new File(rs.getString(1));
		     	list.add(directory);
		     	//System.out.println(directory.getName());
		    }
		    
		    Statement stmt2 = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+CloneRepository.getDBName()+";");
		    ResultSet rs2 = stmt.executeQuery("select fname from file");
		    
		    while(rs2.next())
		    {
		    	allFiles.add(rs2.getString(1));
		    }
		    checkSize=list.size();
    	
     	/*File directory = new File("D:\\Eclipse Workspace\\runtime-EclipseApplication\\treeMap");
     	list.add(directory);*/
     	
		    DummyNode newNode=new DummyNode("root",1);
		    newNode.setTitle("root");
     	allNodes.add(newNode);
     	boolean check=true;
     	
     	for(int i=0;i<list.size();i++)
     	{
	     		DummyNode newNode2=new DummyNode(list.get(i).getName(),(int)list.get(i).length());
			    newNode2.setTitle(list.get(i).getName());
			    
	     		allNodes.add(newNode2);
	     		allNodes.get(0).children.add(allNodes.size()-1);
		    
     	}
     	
     	for(int i=0;i<list.size();i++)
     	{
	        // get all the files from a directory
     		if (list.get(i).isDirectory())
     		{
		        File[] fList = list.get(i).listFiles();
		        boolean present=false;
		        
		        for(int k=0;k<fList.length;k++)
		        {
		        	present=false;
		        	for(int l=0;l<allFiles.size();l++)
		        	{
		        		if(fList[k].getName().equals(allFiles.get(l)))
		        		{
		        			present=true;
		        			break;
		        		}
		        	}
		        	if(!present)
		        	{
		        		fList[k]=null;
		        	}
		        }
		
		        for (File file : fList) 
		        {
		        	check=true;
		        	for(int j=0;j<checkSize;j++)
		        	{
		        		if(list.get(j).equals(file))
		        		{
		        			
				        	allNodes.get(i+1).children.add(j+1);
				        	for(int k=0;k<allNodes.get(0).children.size();k++)
				        	{
				        		if(allNodes.get(0).children.get(k)==j+1)
				        		{
				        			allNodes.get(0).children.remove(k);
				        		}
				        	}
				        	check=false;
		        		}
		        	}
		        	if(check && file!=null)
		        	{
	        			list.add(file);
	        			
	        			DummyNode newNode2=new DummyNode(file.getName(),(int)file.length());
	    			    if(file.isDirectory())
	    			    {
	    			    	newNode2.setTitle(list.get(i).getName());
	    			    }
	    			    else
	    			    {
	    			    	newNode2.setTitle("");
	    			    }
	    			    
			        	allNodes.add(newNode2);
			        	allNodes.get(i+1).children.add(allNodes.size()-1);
	        		}
		        }
     		}
     	}
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
     	
     	for(int i=0;i<allNodes.size();i++)
     	{
     		nodes.add(new DefaultMutableTreeNode(allNodes.get(i).name));
     		
     		for(int j=0;j<fname.size();j++)
     		{
     			if(fname.get(j).equals(allNodes.get(i).name))
     			{
     				allNodes.get(i).setColor(16711680);
     			}
     		}
     	}
     	
     	for(int i=0;i<allNodes.size();i++)
     	{
     		for(int j=0;j<allNodes.get(i).children.size();j++)
     		{
     			nodes.get(i).add(nodes.get(allNodes.get(i).children.get(j)));
     		}
     	}

        DefaultTreeModel treeModel = new DefaultTreeModel(nodes.get(0));

        final String[] columnNames = new String[]{"Title", "Path", "Size","Color","Label"};
        final Class[] columnClasses = new Class[]{String.class, Object[].class, Double.class,Integer.class,String.class};

        DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0) {
            public Class<?> getColumnClass(int columnIndex) {
                return columnClasses[columnIndex];
            }
        };
        
        
        final Enumeration enumeration = nodes.get(0).depthFirstEnumeration();
        while (enumeration.hasMoreElements()) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) enumeration.nextElement();
            if (node.isLeaf()) {
            	String title="";
                final Object label = node.getUserObject();
                final Object[] userObjectPath = node.getUserObjectPath();
                final Object[] path = Arrays.copyOfRange(userObjectPath, 0, userObjectPath.length - 1);
                Double size=1.0 ;
                int nodeColor=0;
                for(int i=0;i<allNodes.size();i++)
                {
                	if(allNodes.get(i).name.equals(label))
                	{
                		title=allNodes.get(i).title;
                		size=(double)allNodes.get(i).size;
                		nodeColor=allNodes.get(i).color;
                	}
                }
                Object[] row = new Object[]{title,path, size,nodeColor,label};
                tableModel.addRow(row);
            }
        }
        
        treeMap = new TreeMap(tableModel);
        
        /*treeMap.getView().addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				
				ArrayList<DummyMethodNode> allMethodNodes=new ArrayList();
		    	ArrayList<DefaultMutableTreeNode> methodNodes = new ArrayList();
		    	TreeMap treeMapSecond;
		    	final JFrame frameSecond = new JFrame();
				
				TreeMapNode getNode =(TreeMapNode)treeMap.getModel().getProbing();
				if(getNode != null && getNode.isLeaf() )
				{
					LeafTreeMapNode selectedNode=(LeafTreeMapNode)getNode;
					
					for(int i=0;i<list.size();i++)
					{
						if(list.get(i).getName().equals(selectedNode.getLabelName()))
						{
							try
							{
								String fileName=list.get(i).getName();
								final int fileToOpenIndex=i;
								
								Statement stmt2 = CloneRepository.getConnection().createStatement();
								Statement stmt3 = CloneRepository.getConnection().createStatement();
							    stmt2.execute("use "+CloneRepository.getDBName());
							    stmt3.execute("use "+CloneRepository.getDBName());
							    ResultSet rs2 = stmt2.executeQuery("select fid from file where fname='"+fileName+"'");
								
							    rs2.next();
								int fid=rs2.getInt(1);
								
								//methods the selected file in which the selected simple clone instances are
								ResultSet methodIDs=stmt3.executeQuery("select mid from scc_method where scc_id="+sccID);
								
								// all the methods of the selected file.
								ResultSet methodDetails=stmt2.executeQuery("select mid,mname,tokens from method where mid IN (select mid from method_file where fid="+fid+")");
								
								//Creating the new tree Map
								allMethodNodes.add(new DummyMethodNode(selectedNode.getLabelName(),1)); // root i.e the selected file
								methodNodes.add(new DefaultMutableTreeNode(selectedNode.getLabelName()));
								while(methodDetails.next())
								{
									DummyMethodNode newNode =new DummyMethodNode(methodDetails.getString(2),methodDetails.getInt(3));
									newNode.setMid(methodDetails.getInt(1));
									if(sccID!=-1)
									{
										while(methodIDs.next())
										{
											if(methodIDs.getInt(1)==methodDetails.getInt(1))
											{
												newNode.setColor(16711680);
												break;
											}
										}
										methodIDs.beforeFirst();
									}
									else
									{
										for(int j=0;j<mids.size();j++)
										{
											if(mids.get(j)==methodDetails.getInt(1))
											{
												newNode.setColor(16711680);
												break;
											}
										}
									}
									allMethodNodes.add(newNode);
									
									methodNodes.add(new DefaultMutableTreeNode(methodDetails.getString(2)));
								}
								
								for(int k=1;k<allMethodNodes.size();k++)
								{
									methodNodes.get(0).add(methodNodes.get(k));
								}
								
								DefaultTreeModel treeModel2 = new DefaultTreeModel(methodNodes.get(0));

						        final String[] columnN = new String[]{"Name","Path", "Size","Color"};
						        final Class[] columnC = new Class[]{String.class,Object.class, Integer.class, Integer.class};

						        DefaultTableModel tableModel2 = new DefaultTableModel(columnN, 0) {
						            public Class<?> getColumnClass(int columnIndex) {
						                return columnC[columnIndex];
						            }
						        };
						        
						        
						        final Enumeration enum2 = methodNodes.get(0).depthFirstEnumeration();
						        while (enum2.hasMoreElements()) {
						            DefaultMutableTreeNode node2 = (DefaultMutableTreeNode) enum2.nextElement();
						            if (node2.isLeaf()) {
						            	String name2="";
						            	Object Label=node2.getUserObject();
						            	final Object[] userObjectPath2 = node2.getUserObjectPath();
						                final Object[] path2 = Arrays.copyOfRange(userObjectPath2, 0, userObjectPath2.length - 1);
						                Double size2=1.0 ;
						                int nodeColor2=0;
						                for(int u=0;u<allMethodNodes.size();u++)
						                {
						                	if(allMethodNodes.get(u).name.equals(Label))
						                	{
						                		name2=allMethodNodes.get(u).name;
						                		size2=(double)allMethodNodes.get(u).size;
						                		nodeColor2=allMethodNodes.get(u).color;
						                	}
						                }
						                Object[] row2 = new Object[]{name2,path2,size2,nodeColor2};
						                tableModel2.addRow(row2);
						            }
						        }
								treeMapSecond=new TreeMap(tableModel2);
								
								treeMapSecond.setGroupBy(1);
								treeMapSecond.setLabels(0);
						        
								treeMapSecond.setSize(2);
								treeMapSecond.setColor(3);
						        
						        //Font size is set to ZERO because of hiding the title and show it on the tooltip only
						        treeMapSecond.getModel().getSettings().getDefaultFieldSettings().setLabelingFont(new Font("Calibri", Font.BOLD, 0));
						        treeMapSecond.getModel().getSettings().setShowPopup(treeMapSecond.getModel().getTreeMapField(3),false);
						        treeMapSecond.getModel().getSettings().setShowPopup(treeMapSecond.getModel().getTreeMapField(2),false);
						        treeMapSecond.getModel().getSettings().setShowPopup(treeMapSecond.getModel().getTreeMapField(0),true);
						        /*treeMapSecond.getModel().getSettings().setSelectionColor(Color.red);
						        
						        JTabbedPane tabbedPane2 = new JTabbedPane();
						        tabbedPane2.add("TreeMap", treeMapSecond);
						        
						        frameSecond.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						        frameSecond.getContentPane().add(tabbedPane2);
						        frameSecond.setSize(new Dimension(800, 600));
						        
						        frameSecond.setVisible(true);
								
							}
							catch(Exception e1)
							{
								
							}
						}
					}
				}
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				
			}
        	
        });*/
        
        treeMap.setGroupBy(1);
        treeMap.setLabels(4);
        
        treeMap.setSize(2);
        treeMap.setColor(3);
        
        //Font size is set to ZERO because of hiding the title and show it on the tooltip only
        treeMap.getModel().getSettings().getDefaultFieldSettings().setLabelingFont(new Font("Calibri", Font.BOLD, 0));
        treeMap.getModel().getSettings().setShowPopup(treeMap.getModel().getTreeMapField(3),false);
        treeMap.getModel().getSettings().setShowPopup(treeMap.getModel().getTreeMapField(2),false);
        treeMap.getModel().getSettings().setShowPopup(treeMap.getModel().getTreeMapField(4),true);
        //treeMap.getModel().getSettings().setSelectionColor(Color.red);*/
        
        ////////////////////////////////////////////////////////////////////////
        
        final DefaultTreeMapController controller = new DefaultTreeMapController();
        controller.setMultipleSelectionEnabled(false);
        controller.setSelectOnPopupTrigger(true);

        treeMap.setController(controller);

        // Add custom entries to the context menu
        
        controller.getPopupMenu().insert(new AbstractAction("Open in Eclipse") {
             public void actionPerformed(ActionEvent e) {
                 final java.util.List selection = treeMap.getModel().getSelection();
                 int fileToOpenIndex=0;
                 for(int i=0;i<list.size();i++)
                 {
                	 if(list.get(i).getName().equals(selection.get(0).toString()))
                	 {
                		 fileToOpenIndex=i;
                		 break;
                	 }
                 }
                 File fileToOpen = new File(list.get(fileToOpenIndex).getAbsolutePath());
					// String text = getFileContent(fileToOpen);
					IFile ifile = MccView.getIFile(fileToOpen);
					if (fileToOpen.exists() && fileToOpen.isFile())
					{
						final IFileStore fileStore = EFS.getLocalFileSystem().getStore(fileToOpen.toURI());
					    Display.getDefault().asyncExec(new Runnable() {
					        @Override
					        public void run() {
					            IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					            try 
								{
					            	
					            	final IWorkbenchPage page=iw.getActivePage();
									IDE.openEditorOnFileStore( page, fileStore );
								} 
								catch (PartInitException e1) 
								{
									e1.printStackTrace();
								}
					        }
					    });
						
					}
             }
        }, 0);
        
        ///////////////////////////////////////////////////////////////////////
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.add("TreeMap", treeMap);
        
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().add(tabbedPane);
        frame.setSize(new Dimension(800, 600));
        
        frame.setVisible(true);
    }
}

