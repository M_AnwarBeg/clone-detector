package cmt.cvac.views;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.part.FileEditorInput;
import cmt.cfc.mccframe.RenamingWizard;
import cmt.cfc.mccframe.WizardPage1;
import cmt.cfc.mccframe.WizardPage2;

import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.dialogs.ViewComparator;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.ITextEditor;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cddc.structures.sMethod;
import cmt.cfc.fccframe.FcFrameRepo;
import cmt.cfc.mccframe.CliqueSelectionWizard;
import cmt.cfc.mccframe.CliqueSelectionWizardPage1;
import cmt.cfc.mccframe.DeleteFrame;
import cmt.cfc.mccframe.EditFrameWizard;
import cmt.cfc.mccframe.MCCTemplate;
import cmt.cfc.mccframe.MCCsAnalyser;
import cmt.cfc.mccframe.McFrameRepo;
import cmt.cfc.utility.FileHandler;
import cmt.common.Directories;
import cmt.common.ProjectImages;
import cmt.cvac.clonecomparsion.OpenCloneInspectionViewerAction;
import cmt.cvac.contentprovidor.MccInstanceListContentProvider;
import cmt.cvac.contentprovidor.MccListContentProvider;
import cmt.cvac.labelprovidor.MccCloneInstanceListLabelProvider;
import cmt.cvac.labelprovidor.MccListLabelProvider;
import cmt.cvac.treemapvisualization.TreeMapInit;
import cmt.cvac.viewobjects.MccCloneInstanceList;
import cmt.cvac.viewobjects.MccList;
import cmt.cvac.viewobjects.PrimaryMccObject;
import cmt.cvac.viewobjects.SecondaryMccObject;

public class MccView extends ViewPart {
	
	    public TableViewer primaryMccViewer;
	    private TableViewer secondaryMccViewer;
	    private TableCursor mccListTableCursor ; 
		private TableCursor mccInstanceListTableCursor; 
	    private MccList mccList;
	    private MccList filteredMccList;
	    private MccCloneInstanceList cloneList;
	    private MccCloneInstanceList filteredCloneList;
	    public Action cliqueSelectionWizardAction;
	    public Action sideBysideViewAction;
	    public Action editFrameWizardAction;
	    public Action deleteFrameAction;
	    public Action visualizationAction;
	    public Action filterMethodClones;
	    public Action unFilterMethodClones;
	    public Action goToLocationAction;
	    private Action doubleClickAction;
	    Table mccListTable;
	    Table mccInstanceListTable;
	    int mccIndex;
	    int mccInstanceId;
	    static int ASCENDING = 1;
	    static int DECENDING = 2;
	    int structureOrder;
	    int mccIdOrder;
	    int idOrder;
	    int sortOrder;
	    IFile ifile;
	    ArrayList<MethodCloneInstance> instances;
	    public static MethodClones mccAfterVisualization;
	    
	    public Action deletionAction;
	    public static ArrayList<McFrameRepo> repo = new ArrayList<McFrameRepo>();
	    public static int totalFrames=0;
	    private ArrayList<String> existingMcc;
	    public static ArrayList<String> fname = new ArrayList();
	    public static ArrayList<Integer> mids = new ArrayList();
		public static boolean filterFlag = false;
		public static boolean filtered = false;
		
	
	
	    public MccView()
	    {
			super();
			primaryMccViewer = null;
			secondaryMccViewer = null;
			mccList = new MccList();
			filteredMccList = new MccList();
			cloneList = new MccCloneInstanceList();
			filteredCloneList = new MccCloneInstanceList();
			mccListTable = null;
			mccInstanceListTable = null;
			structureOrder = DECENDING;
			mccIdOrder = DECENDING;
			idOrder = ASCENDING;
			sortOrder = ASCENDING;
			ifile = null;
			existingMcc = new ArrayList<String>();
			mccListTableCursor = null;
			mccInstanceListTableCursor = null;
	    }

	@Override
	public void createPartControl(final Composite parent) {
		
		
	    GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		layout.makeColumnsEqualWidth = false;
		parent.setLayout(layout);
		
		createMccTables(parent);
		
		mccListTable.setHeaderVisible(true);
		mccListTable.setLinesVisible(true);
		mccInstanceListTable.setHeaderVisible(true);
		mccInstanceListTable.setLinesVisible(true);
		makeActions();
		contributeToActionBars();
		hookDoubleClickAction();
		if(mccList.getList().isEmpty())
		{
			filterMethodClones.setEnabled(false);
		}
		else
		{
			filterMethodClones.setEnabled(true);
		}
		if(!filtered)
		{
			unFilterMethodClones.setEnabled(false);
		}
		else
		{
			unFilterMethodClones.setEnabled(true);
		}
	  mccListTableCursor = new TableCursor(mccListTable, SWT.NULL);
	  mccInstanceListTableCursor = new TableCursor(mccInstanceListTable, SWT.NULL);

		/*primaryMccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryMccObject object1 = (PrimaryMccObject) e1;
				PrimaryMccObject object2 = (PrimaryMccObject) e2;
				int result = 0;
				if (object1.getStructure().size() == object2.getStructure().size())
				{
				    if (object1.getStructure().get(0) > object2.getStructure().get(0))
				    {
					result = 1;
				    } else
				    {
					result = -1;
				    }
				} else
				{
				    result = object1.getStructure().size() > (object2.getStructure().size()) ? -1 : 1;
				}
				if (structureOrder == 0)
				{
				    return result;
				}
				if (structureOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (structureOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();
			if (structureOrder == ASCENDING)
			{
			    structureOrder = DECENDING;

			} else if (structureOrder == DECENDING)
			{
			    structureOrder = ASCENDING;
			}
		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});*/

		primaryMccViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryMccObject object1 = (PrimaryMccObject) e1;
				PrimaryMccObject object2 = (PrimaryMccObject) e2;
				int result = 0;
				result = object1.getMccId() > (object2.getMccId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		
		primaryMccViewer.getTable().getColumn(5).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryMccObject object1 = (PrimaryMccObject) e1;
				PrimaryMccObject object2 = (PrimaryMccObject) e2;
				int result = 0;
				result = object1.getApc() > (object2.getApc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		
		primaryMccViewer.getTable().getColumn(4).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryMccObject object1 = (PrimaryMccObject) e1;
				PrimaryMccObject object2 = (PrimaryMccObject) e2;
				int result = 0;
				result = object1.getAtc() > (object2.getAtc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});

		primaryMccViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryMccObject object1 = (PrimaryMccObject) e1;
				PrimaryMccObject object2 = (PrimaryMccObject) e2;
				int result = 0;
				result = object1.getId() > (object2.getId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		
		primaryMccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryMccObject object1 = (PrimaryMccObject) e1;
				PrimaryMccObject object2 = (PrimaryMccObject) e2;
				int result = 0;
				result = object1.getNumberOfInstance() > (object2.getNumberOfInstance()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		secondaryMccViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getId() > (object2.getId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		
		secondaryMccViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getTc() > (object2.getTc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		
		secondaryMccViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getPc() > (object2.getPc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		secondaryMccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getMId() > (object2.getMId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		/*secondaryMccViewer.getTable().getColumn(4).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getMethodName() > (object2.getMethodName()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});*/
		secondaryMccViewer.getTable().getColumn(5).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getFId() > (object2.getFId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});
		/*secondaryMccViewer.getTable().getColumn(6).addSelectionListener(new SelectionListener() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;

			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryMccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryMccObject object1 = (SecondaryMccObject) e1;
				SecondaryMccObject object2 = (SecondaryMccObject) e2;
				int result = 0;
				result = object1.getFileName() > (object2.getFileName()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			primaryMccViewer.refresh();
		//	markExistingMcc();

		    }

		    @Override
		    public void widgetDefaultSelected(SelectionEvent e)
		    {
			// TODO Auto-generated method stub

		    }
		});*/

		primaryMccViewer.getTable().addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseDown(MouseEvent e)
		    {
		    	if(filterFlag) {
		    		int row = Integer.parseInt((mccListTableCursor.getRow().getText()));
					mccIndex = row;

					if (ClonesReader.getFilteredMethodClones().get(mccIndex).isFramed() )
					{
					   //editFrameWizardAction.setEnabled(true);
					   // deleteFrameAction.setEnabled(true);
					    cliqueSelectionWizardAction.setEnabled(false);
				
					    sideBysideViewAction.setEnabled(true);
					} 
					else
					{
					   // editFrameWizardAction.setEnabled(false);
					   // deleteFrameAction.setEnabled(false);
					    if(CloneRepository.getDBName().equals(LoadList.framingBaseline) && (validForFraming()==true))
						{
					    	cliqueSelectionWizardAction.setEnabled(true);
						}
						else
						{
							cliqueSelectionWizardAction.setEnabled(false);
						}
					    sideBysideViewAction.setEnabled(true);
					    cliqueSelectionWizardAction.setEnabled(false);
					}
					
					// int column=(mccListTableCursor.getColumn());
					// if(column==1)
					// {
					MccInstanceListContentProvider mccCloneInstanceListContentProvider = new MccInstanceListContentProvider();
					mccCloneInstanceListContentProvider.setList(filteredMccList.getList().get(row).getCloneList().getList());
					secondaryMccViewer.setContentProvider(mccCloneInstanceListContentProvider);
					secondaryMccViewer.setLabelProvider(new MccCloneInstanceListLabelProvider());
					secondaryMccViewer.setInput(filteredMccList.getList().get(row).getCloneList().getList());
					secondaryMccViewer.refresh();
					
					fname.clear();
					mids.clear();
					for(int i=0;i<filteredMccList.getList().get(row).getCloneList().getList().size();i++)
					{
						fname.add(filteredMccList.getList().get(row).getCloneList().getList().get(i).getFileName());
						mids.add(filteredMccList.getList().get(row).getCloneList().getList().get(i).getMId());
					}
					
					deletionAction.setEnabled(false);
					
					for(int t=0;t<repo.size();t++)
					{
						if(repo.get(t).getMccId() == ClonesReader.getMethodClones().get(mccIndex).getClusterID())
						{
							deletionAction.setEnabled(true);
						}
					}
					visualizationAction.setEnabled(true);
		    		
		    	}
		    	else {
		    		int row = Integer.parseInt((mccListTableCursor.getRow().getText()));
					mccIndex = row;

					if (ClonesReader.getMethodClones().get(mccIndex).isFramed())
					{
					   //editFrameWizardAction.setEnabled(true);
					   // deleteFrameAction.setEnabled(true);
					    cliqueSelectionWizardAction.setEnabled(false);
					    sideBysideViewAction.setEnabled(true);
					} 
					else
					{
					   // editFrameWizardAction.setEnabled(false);
					   // deleteFrameAction.setEnabled(false);
						if(CloneRepository.getDBName().equals(LoadList.framingBaseline) && (validForFraming()==true))
						{
					    	cliqueSelectionWizardAction.setEnabled(true);
						}
						else
						{
							cliqueSelectionWizardAction.setEnabled(false);
						}
					    sideBysideViewAction.setEnabled(true);
					}
					cliqueSelectionWizardAction.setEnabled(false);
					// int column=(mccListTableCursor.getColumn());
					// if(column==1)
					// {
					MccInstanceListContentProvider mccCloneInstanceListContentProvider = new MccInstanceListContentProvider();
					mccCloneInstanceListContentProvider.setList(mccList.getList().get(row).getCloneList().getList());
					secondaryMccViewer.setContentProvider(mccCloneInstanceListContentProvider);
					secondaryMccViewer.setLabelProvider(new MccCloneInstanceListLabelProvider());
					secondaryMccViewer.setInput(mccList.getList().get(row).getCloneList().getList());
					secondaryMccViewer.refresh();
					
					fname.clear();
					mids.clear();
					for(int i=0;i<mccList.getList().get(row).getCloneList().getList().size();i++)
					{
						fname.add(mccList.getList().get(row).getCloneList().getList().get(i).getFileName());
						mids.add(mccList.getList().get(row).getCloneList().getList().get(i).getMId());
					}
					
					visualizationAction.setEnabled(true);
					
					deletionAction.setEnabled(false);
					
					for(int t=0;t<repo.size();t++)
					{
						if(repo.get(t).getMccId() == ClonesReader.getMethodClones().get(mccIndex).getClusterID())
						{
							deletionAction.setEnabled(true);
						}
					}
		    	}
		    	goToLocationAction.setEnabled(false);
		    }
		});

		secondaryMccViewer.getTable().addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseDown(MouseEvent e)
		    {
		    	goToLocationAction.setEnabled(true);		    	
		    }
		});

	    }

	@Override
	public void setFocus() {
		primaryMccViewer.getControl().setFocus();
		secondaryMccViewer.getControl().setFocus();		
	}
	
	public boolean validForFraming()
	{
		boolean flag=false;
		IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
		for(int i=0; i< editorlist.length;i++) {
		if(editorlist[i].getTitle().equals("Method Clone Editor")) {
			flag=true;
		break;
		}
		}
		return flag;
	}
	
	public static void navigateToLine(IFile file, Integer line)
    {
	HashMap<String, Object> map = new HashMap<String, Object>();
	map.put(IMarker.LINE_NUMBER, line);
	IMarker marker = null;
	try
	{
	    marker = file.createMarker("com.plugin.clonemanager.customtextmarker");
	    marker.setAttributes(map);
	    //marker.setAttribute(IMarker.CHAR_START, 0);
	    try
	    {
		IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), marker);
	    } catch (PartInitException e)
	    {
		// complain
	    }
	} catch (CoreException e1)
	{
	    // complain
	} finally
	{
	    try
	    {
		if (marker != null)
		{
		    marker.delete();
		}
	    } catch (CoreException e)
	    {
		// whatever
	    }
	}
    }
	
	

    private void printInPopup(String in)
    {
	MessageBox box = new MessageBox(getSite().getWorkbenchWindow().getShell(), SWT.ICON_INFORMATION);
	box.setMessage(in);
	box.open();
    }
	
	public static IFile getIFile(File fileToOpen)
    {
	IPath location = Path.fromOSString(fileToOpen.getAbsolutePath());
	IWorkspace workspace = ResourcesPlugin.getWorkspace();
	return workspace.getRoot().getFileForLocation(location);
    }

    private String getFileContent(File fileToOpen)
    {
	String content = "";
	try
	{
	    FileInputStream fis = new FileInputStream(fileToOpen);
	    byte[] data = new byte[(int) fileToOpen.length()];
	    fis.read(data);
	    fis.close();
	    content = new String(data, "UTF-8");
	} catch (IOException e2)
	{
	    e2.printStackTrace();
	}
	return content;
    }
	
	
	 private void contributeToActionBars()
	 {
		IActionBars bars = getViewSite().getActionBars();
		// fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	 }

	    private void fillLocalToolBar(IToolBarManager manager)
	    {


	    	manager.add(goToLocationAction);
	    	manager.add(new Separator());
	    	
	    	manager.add(new Separator());
			manager.add(sideBysideViewAction);
			manager.add(new Separator());
			manager.add(cliqueSelectionWizardAction);
			manager.add(new Separator());
			manager.add(deletionAction);
			/*manager.add(new Separator());
			manager.add(editFrameWizardAction);
			manager.add(new Separator());
			manager.add(deleteFrameAction);*/
			manager.add(new Separator());
			manager.add(visualizationAction);
			manager.add(new Separator());
			manager.add(filterMethodClones);
			manager.add(new Separator());
			manager.add(unFilterMethodClones);

	    }
	    
	    private void hookDoubleClickAction()
	    {
		primaryMccViewer.addDoubleClickListener(new IDoubleClickListener() {
		    @Override
		    public void doubleClick(DoubleClickEvent event)
		    {
			doubleClickAction.run();
		    }
		});
	    }
	
	 private void makeActions(){
  
		 deletionAction = new Action() {
				
				public void run()
				{
					int result = deleteMCC(mccIndex);
					if(result == 1) {
						JOptionPane.showMessageDialog(null, "Selected Clone Class has been deleted.", "Error" , JOptionPane.INFORMATION_MESSAGE);
					}
					else if(result == 2) {
						JOptionPane.showMessageDialog(null, "Selected Clone Class has not framed yet.", "Error" , JOptionPane.INFORMATION_MESSAGE);
					}
					deletionAction.setEnabled(false);
				}
			};
			
			deletionAction.setText("Delete Frame");
			deletionAction.setToolTipText("Delete Frame");
			deletionAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.DELETE_FRAME)));
			deletionAction.setEnabled(false);
		 
		 
	    	visualizationAction= new Action() {
	    		@Override
	    	    public void run()
	    	    {
	    			TreeMapInit.init(fname,-1,mids);
	    			visualizationAction.setEnabled(false);
	    	    }
	    	};
	    	visualizationAction.setText("Run TreeMap Visualization");
	    	visualizationAction.setToolTipText("Run TreeMap Visualization");
	    	visualizationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_TREEMAPVISUALIZATION)));
	    	visualizationAction.setEnabled(false);
	    	
	    	filterMethodClones = new Action() {
	    		public void run() {
					Shell shell01 = new Shell();
					FilterMethodClones filter = new FilterMethodClones();
					
					filter.open();
				}
			};
			filterMethodClones.setEnabled(true);
			filterMethodClones.setText("Filter Method Clones");
			filterMethodClones.setToolTipText("Filter Method Clones");
			filterMethodClones.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FILTERATION)));
			
			unFilterMethodClones = new Action() {
	    		public void run() {
	    			IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	 			   try
	 				{
	 				
	 				    
	 				   
	 				   	MccView.filterFlag = false;
	 				   	MccView.filtered = false;
	 				    IViewPart mccView = workbenchPage.findView("mccView");
	 				    if(mccView != null) {
	 				    	workbenchPage.hideView(mccView);
	 				    }
	 				   	IViewPart p = workbenchPage.showView("mccView");
	 				    p.setFocus();
	 				   

	 				}
	 			   catch (Exception e)
	 				{
	 				    e.printStackTrace();
	 				}
					
				}
			};
			unFilterMethodClones.setEnabled(false);
			unFilterMethodClones.setText("Unfilter Method Clones");
			unFilterMethodClones.setToolTipText("Unfilter Method Clones");
			unFilterMethodClones.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_UNFILTERATION)));
			
			
	    	sideBysideViewAction=new Action() {
	    		
	    		public void run()
			    {
				int mccId = ClonesReader.getMethodClones().get(mccIndex).getClusterID();
				MethodClones mcc = ClonesReader.getMethodClones().get(mccIndex);
				mccAfterVisualization = mcc;
				instances = new ArrayList<MethodCloneInstance>();
				instances = mcc.getMCCInstances();
				cliqueSelectionWizardAction.setEnabled(true);
				//add the instances to the function
				new OpenCloneInspectionViewerAction(mcc.getMCCInstances().get(0), mcc.getMCCInstances().get(1), instances,0,1).run();
				
			
			    }
			};
			sideBysideViewAction.setText("Analyze Clone");
			sideBysideViewAction.setToolTipText("Analyze Clone");
			sideBysideViewAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONE_COMPARISON)));
			sideBysideViewAction.setEnabled(false);

			
			
			cliqueSelectionWizardAction = new Action() {
		    @Override
		    public void run()
		    {
		    	try {
				/*	CliqueSelectionWizrdPage page = new CliqueSelectionWizrdPage();
					page.setMCCID(ClonesReader.getMethodClones().get(mccIndex).getClusterID());
					page.setMCCIndex(mccIndex);
					page.open();
					*/
		    		
		    		//ubaid
		    		
				}
				catch(Exception e) {
					e.printStackTrace();
				}
				
		    	cliqueSelectionWizardAction.setEnabled(false);
			int mccId = mccAfterVisualization.getClusterID();
			MethodClones mcc = mccAfterVisualization;
			McFrameRepo newMc=new McFrameRepo();
			try {
			MCCsAnalyser.analyze(mcc);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				
			}
		// printInPopup("MCC Id Selected: "+ mccId);
			CliqueSelectionWizard cliqueWizard = new CliqueSelectionWizard();
			cliqueWizard.setMCCID(mccId);
		cliqueWizard.setMccIndex(mccIndex);
		ISelection selection = getSite().getWorkbenchWindow().getSelectionService().getSelection();
			IStructuredSelection selectionToPass = StructuredSelection.EMPTY;
			if (selection instanceof IStructuredSelection)
			{
		    selectionToPass = (IStructuredSelection) selection;
		}
			cliqueWizard.init(getSite().getWorkbenchWindow().getWorkbench(), selectionToPass);
			Shell shell = getSite().getWorkbenchWindow().getWorkbench().getActiveWorkbenchWindow().getShell();
			WizardDialog dialog = new WizardDialog(shell, cliqueWizard);
			try
			{
			    dialog.create();
			 // following two lines are commented because of the new flow of framing.
				//int res = dialog.open();
			    //notifyResult(res == Window.OK);
		} catch (Exception e)
			{
			    e.printStackTrace();
			    System.err.println(e.getMessage());
			}
			
			// code of the new flow of framing.
		
		//CloneDB.loadResultsForFraming(SampleHandler.f.mccList.get(mccIndex));

		try
		{
			System.out.println("Hello");
			MCCsAnalyser.modifymcc(ClonesReader.getMethodClones().get(mccIndex), CliqueSelectionWizardPage1.SccsSelected);
		    for (MethodCloneInstance instance :ClonesReader.getMethodClones().get(mccIndex).getMCCInstances())
		    {
			instance.DisplayAnalysisDetails();
		    }
		    System.out.println(ClonesReader.getMethodClones().get(mccIndex).getClusterID());
		    
		    
		    
		    newMc.setMccId((ClonesReader.getMethodClones().get(mccIndex).getClusterID()));
			repo.add(newMc);
			
			//Ubaid
			//new OpenCloneInspectionViewerAction(mcc.getMCCInstances().get(0), mcc.getMCCInstances().get(1), instances,0,1).run();
			 MCCTemplate.genMCCTemplate(ClonesReader.getMethodClones().get(mccIndex).getClusterID());
			 //Wizard Position
			
			
		   
		 
		    // open SPC file in the editor (Saud)
		    FileHandler.fileHandler=new FileHandler("MCC_SPC");
		    
		    FileHandler.fileHandler.getARTFilesList();
		    FileHandler.fileHandler.getARTFilesPaths();
		    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
		    {
		    	if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(mccId+"_MCC_SPC"))
		    	{
			    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
			    	boolean test  =  file.isFile();
			 
				    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
				    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				    IDE.openEditorOnFileStore(page,fileStore);
		    	}
		    }
		    
		    // open template file in the editor (Saud)
		    FileHandler.fileHandler=new FileHandler("MCC_Template");
		    FileHandler.fileHandler.getARTFilesList();
		    FileHandler.fileHandler.getARTFilesPaths();
		    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
		    {
		    	if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(mccId+"_MCC_template"))
		    	{
			    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
			    	boolean test  =  file.isFile();
			 
				    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
				    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				    IDE.openEditorOnFileStore(page,fileStore);
		    	}
		    }
		    
		 // open SCC Frames in the editor (Saud)
		    
		    ArrayList<Integer> scc = new ArrayList<>();
		    Statement st = CloneRepository.getConnection().createStatement();
		    st.execute("use "+CloneRepository.getDBName()+";");
		    ResultSet rs = st.executeQuery("select scc_id from mcc_scc where mcc_id = "+ mccId);
		    
		    while (rs.next())
		    {
		    	scc.add(rs.getInt(1));
		    }
		    FileHandler.fileHandler=new FileHandler("SCC_Frames");
		    FileHandler.fileHandler.getARTFilesList();
		    FileHandler.fileHandler.getARTFilesPaths();
		    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
		    {
		    	for(int j=0; j<scc.size(); j++)
				{
		    		if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(scc.get(j)+"_SCC_Frame"))
				    	{
					    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
					    	boolean test  =  file.isFile();
					 
						    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
						    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						    IDE.openEditorOnFileStore(page,fileStore);
				    	}
				}
		    }
		    CliqueSelectionWizard.markMethods(mccId);
			    
		} catch (Exception e)
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		
		    }
		};

		cliqueSelectionWizardAction.setText("Frame");
		cliqueSelectionWizardAction.setToolTipText("Frame Method Clone Class");
		
				
		cliqueSelectionWizardAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FRAMING)));
		cliqueSelectionWizardAction.setEnabled(false);
		
		

		editFrameWizardAction = new Action() {
		    @Override
		    public void run()
		    {
			int mccId = ClonesReader.getMethodClones().get(mccIndex).getClusterID();
			FileHandler fileHandler = new FileHandler("MCC_Template//MCC_"+mccId);
			
			EditFrameWizard wizard = new EditFrameWizard();
			wizard.mccID = mccId;
			wizard.mccIndex = mccIndex;
			ISelection selection = getSite().getWorkbenchWindow().getSelectionService().getSelection();
			IStructuredSelection selectionToPass = StructuredSelection.EMPTY;
			if (selection instanceof IStructuredSelection)
			{
			    selectionToPass = (IStructuredSelection) selection;
			}
			wizard.init(getSite().getWorkbenchWindow().getWorkbench(), selectionToPass);
			Shell shell = getSite().getWorkbenchWindow().getWorkbench().getActiveWorkbenchWindow().getShell();
			WizardDialog dialog = new WizardDialog(shell, wizard);
			
			
			try
			{
			    dialog.create();
			    int res = dialog.open();
			    notifyResult(res == Window.OK);
			} catch (Exception e)
			{
			    e.printStackTrace();
			    System.err.println(e.getMessage());
			}
		    }
		};
		deleteFrameAction = new Action() {
		    @Override
		    public void run()
		    {
			int mccId = ClonesReader.getMethodClones().get(mccIndex).getClusterID();
			FileHandler fileHandler = new FileHandler("MCC_Template//MCC_"+mccId);
			DeleteFrame wizard = new DeleteFrame(mccId);
		
			
		    }
		};
		

		editFrameWizardAction.setText("Edit Frame");
		editFrameWizardAction.setToolTipText("Edit Frame");
		editFrameWizardAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FRAMEEDIT)));
		editFrameWizardAction.setEnabled(false);
		
		deleteFrameAction.setText("Delete Frame");
		deleteFrameAction.setToolTipText("Delete Frame");
		deleteFrameAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FRAMEDELETION)));
		deleteFrameAction.setEnabled(false);	    
		
    	goToLocationAction = new Action() {
    	public void run() {
			int row = Integer.parseInt((mccInstanceListTableCursor.getRow().getText()));
			mccInstanceListTableCursor.getColumn();
			// if(column==5){
			sMethod currentMethod = ClonesReader.getMethodClones().get(mccIndex).getMCCInstances().get(--row).getMethod();
			File fileToOpen = new File(currentMethod.getFilePath());
			// String text = getFileContent(fileToOpen);
			IFile ifile = getIFile(fileToOpen);
			if (fileToOpen.exists() && fileToOpen.isFile())
			{
				try
				{
				    // IFileStore fileStore = EFS.getLocalFileSystem().getStore(fileToOpen.toURI());
				    // IWorkbenchPage page =
				    // PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				    navigateToLine(ifile, currentMethod.getStartToken());
				    /*
				     * try { /* IEditorPart editorPart = IDE.openEditorOnFileStore( page, fileStore
				     * ); ITextEditor sourceEditor = (ITextEditor)editorPart; int startChar =
				     * text.indexOf(currentMethod.getCodeSegment()); int offset =
				     * currentMethod.getCodeSegment().length(); //highlightCode(ifile, sourceEditor,
				     * startChar, offset); sourceEditor.selectAndReveal(startChar, 0); IMarker[]
				     * markers = findMarkers(ifile);
				     * //printInPopup(Integer.toString(markers.length)); } catch (Exception ex ) {
				     * ex.printStackTrace(); }
				     */
				}
				catch(Exception e)
				{
					MessageConsole myConsole = new MessageConsole("My Console", null);
				    ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ myConsole });
				    ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
				    final MessageConsoleStream stream = myConsole.newMessageStream();
				    stream.setActivateOnWrite(true);
				    stream.println("File Not available");
					    try 
					    {
							stream.close();
						}
					    catch (IOException e2) 
					    {
							e2.printStackTrace();
						}
				}
			} else
			{
			    printInPopup(fileToOpen.getName() + " doesn't Exist!");
			}
			// }
		    }
    	};
		
    	goToLocationAction.setText("Go to Method Location");
    	goToLocationAction.setToolTipText("Go to Method Location");
    	goToLocationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.GO_TO_FILE_LOCATION)));
    	goToLocationAction.setEnabled(false);
		
		 }
	
	
	
   public void getExistingMcc()
   {
	/*File f = new File(CloneManagementPlugin.getAbsolutePath("\\ART_Output\\MCC_Template\\"));
	existingMcc = new ArrayList<String>(Arrays.asList(f.list()));
	for (int i = 0; i < existingMcc.size(); i++)
	{
	    System.out.println("Name : " + existingMcc.get(i));
	    String[] parts = existingMcc.get(i).split("_");
	    int index = Integer.parseInt(parts[parts.length - 1]);
	    System.out.println("Index : " + index);
	    for (int j = 0; j < mccList.getList().size(); j++)
	    {
		// if(index==mccList.getList().get(j).getMccId())
		if (index == Integer.parseInt(primaryMccViewer.getTable().getItem(j).getText(1)))
		{
		    SampleHandler.f.mccList.get(j).setFramed(true);
		    markMethods(j);
		    setBackground(j);
		    setForeground(j);
		}
	    }
	}*/
   }
   
   	
   	public static int deleteMCC(int mccID) {
   		
   		int index=-1;
		for(int j=0; j<ClonesReader.getMethodClones().size(); j++)
		{
			if(ClonesReader.getMethodClones().get(j).getClusterID() == mccID)
			{
				index=j;
			}
		}
   		for(int t=0;t<repo.size();t++)
		{
			if(repo.get(t).getMccId() == ClonesReader.getMethodClones().get(index).getClusterID())
			{
				try 
				{
					String SPC_PATH = repo.get(t).getMccId() + "_MCC_SPC.art";
				    String templatePath = repo.get(t).getMccId() + "_MCC_template.art";
					/*Files.deleteIfExists(Paths.get(Directories.getAbsolutePath("ART_Output/FCC_Template/"+SPC_PATH)));
					Files.deleteIfExists(Paths.get(Directories.getAbsolutePath("ART_Output/FCC_Template/"+templatePath)));*/
				    
				    int frameid;
				    Statement stmt = CloneRepository.conn.createStatement();
				    stmt.execute("use framerepository;");
				    ResultSet rs=stmt.executeQuery("select fid from mccframes where mccid = "+repo.get(t).getMccId());
				    rs.next();
				    frameid=rs.getInt(1);
				    
				    ResultSet rs6= stmt.executeQuery("select links from frames where fid = "+frameid);
					rs6.next();
					int mccLinks = rs6.getInt(1);
					
					File f;
					
					if(mccLinks>1) 
					{
						stmt.execute("update frames set links="+(mccLinks-1)+"where fid="+frameid);
					}
					else
					{
						stmt.execute("delete from mccframes where mccid = "+repo.get(t).getMccId());
					    stmt.execute("delete from frames where fid = "+frameid);
					    stmt.execute("delete from frames where fid = "+(frameid+1));
					    
					    f=new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccSPC+SPC_PATH));
					    f.delete();
					    
					    f=new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccTemplate+templatePath));
					    f.delete();
					}
					
					for(int y=0;y<ClonesReader.getMethodClones().get(index).getMCCInstances().get(0).getSCCs().size();y++)
					{
						ResultSet rs2=stmt.executeQuery("select fid from sccframes where sccid = "+ClonesReader.getMethodClones().get(index).getMCCInstances().get(0).getSCCs().get(y).getSCCID());
						rs2.next();
						frameid=rs2.getInt(1);
						
						ResultSet rs3= stmt.executeQuery("select links from frames where fid = "+frameid);
						rs3.next();
						int links = rs3.getInt(1);
						
						if(links>1)
						{
							stmt.execute("update frames set links="+(links-1)+"where fid="+frameid);
						}
						else
						{
							ResultSet rs5=stmt.executeQuery("select sccid from sccframes where fid = "+frameid);
						    rs5.next();
							int sccFrameId = rs5.getInt(1);
							stmt.execute("delete from frames where fid = "+frameid);
							stmt.execute("delete from sccframes where fid = "+frameid);
							String ART_PATH =  sccFrameId + "_SCC_Frame.art";
							f=new File(Directories.getAbsolutePath(Directories.SccFrames+ART_PATH));
							f.delete();
						}
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				repo.remove(t);
				return 1;
				
			}
			else if(t==repo.size()-1)
			{
				return 2;
			}
		}
		if(repo.size()==0)
		{
			return 2;
		}
		return 0;
   	}
	
	public void createMccTables(Composite parent)
    {
	primaryMccViewer = new TableViewer(parent,
		SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createMccTableColumns(parent, primaryMccViewer);
	// setMccInput();
	if(filterFlag) {
		populateFilteredMcclist();
		MccListContentProvider mccListContentProvider = new MccListContentProvider();
		mccListContentProvider.setList(filteredMccList.getList());
		primaryMccViewer.setContentProvider(mccListContentProvider);
		primaryMccViewer.setLabelProvider(new MccListLabelProvider());
		primaryMccViewer.setInput(filteredMccList.getList());/*
		
		GridData gridData  = new GridData();
		gridData.widthHint = 200;
	    gridData.heightHint = 200;*/
		
		primaryMccViewer.refresh();
		
	}
	else {
		populateMccList();
		MccListContentProvider mccListContentProvider = new MccListContentProvider();
		mccListContentProvider.setList(mccList.getList());
		primaryMccViewer.setContentProvider(mccListContentProvider);
		primaryMccViewer.setLabelProvider(new MccListLabelProvider());
		primaryMccViewer.setInput(mccList.getList());/*
		
		GridData gridData  = new GridData();
		gridData.widthHint = 200;
	    gridData.heightHint = 200;*/
		
		primaryMccViewer.refresh();
		
	}
	/*populateMccList();
	MccListContentProvider mccListContentProvider = new MccListContentProvider();
	mccListContentProvider.setList(mccList.getList());
	primaryMccViewer.setContentProvider(mccListContentProvider);
	primaryMccViewer.setLabelProvider(new MccListLabelProvider());
	primaryMccViewer.setInput(mccList.getList());
	
	GridData gridData  = new GridData();
	gridData.widthHint = 200;
    gridData.heightHint = 200;*/
	
	primaryMccViewer.refresh();
	secondaryMccViewer = new TableViewer(parent,
		SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createMccCloneListTableColumns(parent, secondaryMccViewer);
	//markExistingMcc();
    }

	
	 private void createMccTableColumns(Composite parent, TableViewer primaryMccViewer)
	    {
		
		 String[] titles = {"#", "MCCID", "Structure", "Number of Instance(s)", "ATC ", "APC" };
		int[] bounds = {50, 50, 150, 50, 50, 50 };
		mccListTable = this.primaryMccViewer.getTable();
		
		mccListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		createMccTableColumn(titles[0], bounds[0], 0);
		createMccTableColumn(titles[1], bounds[1], 1);
		createMccTableColumn(titles[2], bounds[2], 2);
		createMccTableColumn(titles[3], bounds[3], 3);
		createMccTableColumn(titles[4], bounds[4], 4);
		createMccTableColumn(titles[5], bounds[5], 5);
		mccListTable.getColumn(0).setToolTipText("Clone Number");
		mccListTable.getColumn(1).setToolTipText("Method Clone Class ID");
		mccListTable.getColumn(2).setToolTipText("List of Simple-Clone-ID's of which this method clone is composed of");
		mccListTable.getColumn(3).setToolTipText("Total number of instances of this clone");
		mccListTable.getColumn(4).setToolTipText("Average Token Count");
		mccListTable.getColumn(5).setToolTipText("Average Percentage Count");
	    }
	 
	 
	 private TableViewerColumn createMccTableColumn(String title, int bound, final int colNumber)
	    {
		final TableViewerColumn viewerColumn = new TableViewerColumn(primaryMccViewer, SWT.MULTI | SWT.FULL_SELECTION);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	    }

	 
	 
	 private void createMccCloneListTableColumns(Composite parent, TableViewer secondartMccViewer)
	 {
		String[] titles = {"ID", "TC", "PC", "MID", "Method Name", "FID", "File Name"};
		int[] bounds = { 50,  50, 150, 50, 150, 50, 50 };
		mccInstanceListTable = this.secondaryMccViewer.getTable();
		
		mccInstanceListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		createMccCloneListTableColumn(titles[0], bounds[0], 0);
		createMccCloneListTableColumn(titles[1], bounds[1], 1);
		createMccCloneListTableColumn(titles[2], bounds[2], 2);
		createMccCloneListTableColumn(titles[3], bounds[3], 3);
		createMccCloneListTableColumn(titles[4], bounds[4], 4);
		createMccCloneListTableColumn(titles[5], bounds[5], 5);
		createMccCloneListTableColumn(titles[6], bounds[6], 6);
		mccInstanceListTable.getColumn(0).setToolTipText("Method Clone Instance ID");
		mccInstanceListTable.getColumn(1).setToolTipText("Token Count");
		mccInstanceListTable.getColumn(2).setToolTipText("Percentage Count");
		mccInstanceListTable.getColumn(3).setToolTipText("Method ID");
		mccInstanceListTable.getColumn(4).setToolTipText("Name of method that contains this clone");
		mccInstanceListTable.getColumn(5).setToolTipText("File ID");		
		mccInstanceListTable.getColumn(6).setToolTipText("Name of file that contains this clone");
	 }
	 
	 
	 
	 
	 private TableViewerColumn createMccCloneListTableColumn(String title, int bound, final int colNumber)
	    {
		final TableViewerColumn viewerColumn = new TableViewerColumn(secondaryMccViewer, SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	    }
	 
	 private void populateMccList()
	    {
	//	if (ClonesReader != null)
	//	{
		    for (int i = 0; i < ClonesReader.getMethodClones().size(); i++)
		    {
			int mccid =ClonesReader.getMethodClones().get(i).getClusterID();
			int inctancecount =ClonesReader.getMethodClones().get(i).getMCCInstanceCount();
			ArrayList<Integer> integerList = ClonesReader.getMethodClones().get(i).getvCloneClasses();
			double averageTokenCount = ClonesReader.getMethodClones().get(i).getAverageTokenCount();
			double averagePercentageCount = ClonesReader.getMethodClones().get(i).getAveragePercentageCount();
			
			PrimaryMccObject temp = new PrimaryMccObject(i, mccid, integerList, inctancecount, averageTokenCount, averagePercentageCount);
			mccList.addToList(temp);
			populate(mccid);
		    }
	//	}
	    }
	 private void populateFilteredMcclist() {
		 for (int i = 0; i < ClonesReader.getFilteredMethodClones().size(); i++)
		    {
			int mccid =ClonesReader.getFilteredMethodClones().get(i).getClusterID();
			int inctancecount =ClonesReader.getFilteredMethodClones().get(i).getMCCInstanceCount();
			ArrayList<Integer> integerList = ClonesReader.getFilteredMethodClones().get(i).getvCloneClasses();
			double averageTokenCount = ClonesReader.getFilteredMethodClones().get(i).getAverageTokenCount();
			double averagePercentageCount = ClonesReader.getFilteredMethodClones().get(i).getAveragePercentageCount();
			
			PrimaryMccObject temp = new PrimaryMccObject(i, mccid, integerList, inctancecount, averageTokenCount, averagePercentageCount);
			filteredMccList.addToList(temp);
			populateFiltered(mccid);
		    }
		 
	 }
	 protected void populateFiltered(int mccid) {
		 filteredCloneList = new MccCloneInstanceList();
			for (int i = 0; i < ClonesReader.getFilteredMethodClones().size(); i++)
			{
			    if (mccid ==  ClonesReader.getFilteredMethodClones().get(i).getClusterID())
			    {

				for (int j = 0; j <  ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().size(); j++)
				{
				    String filePath = ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().get(j).getMethod().getFilePath();
				    String[] parts = filePath.split("[//// ]");
				    //parts=parts[parts.length-1].split("[//// ]");
				    String fileName = parts[parts.length - 1];
				    String methodName = ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().get(j).getMethod()
					    .getMethodName();
				    int mId = (ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().get(j).getMethod().getMethodId());
				    int fid = ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().get(j).getMethod().getFileID();
				    int tokenCoverage = ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().get(j).getMethod()
					    .getTokenCount();
				    double percentageCoverage = ClonesReader.getFilteredMethodClones().get(i).getMCCInstances().get(j).getCoverage();
				    SecondaryMccObject temp = new SecondaryMccObject(j + 1, fid, mId, tokenCoverage, percentageCoverage,
					    methodName, fileName);
				    filteredCloneList.addToList(temp);

				}
				filteredMccList.getList().get(i).setCloneList(filteredCloneList);
			    }
			}
		 
	 }
	 
	    protected void populate(int mccid)
	    {
		cloneList = new MccCloneInstanceList();
		for (int i = 0; i < ClonesReader.getMethodClones().size(); i++)
		{
		    if (mccid ==  ClonesReader.getMethodClones().get(i).getClusterID())
		    {

			for (int j = 0; j <  ClonesReader.getMethodClones().get(i).getMCCInstances().size(); j++)
			{
			    String filePath = ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getMethod().getFilePath();
			    String[] parts = filePath.split("[//// ]");
			    //parts=parts[parts.length-1].split("[//// ]");
			    String fileName = parts[parts.length - 1];
			    String methodName = ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getMethod()
				    .getMethodName();
			    int mId = (ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getMethod().getMethodId());
			    int fid = ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getMethod().getFileID();
			    int tokenCoverage = ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getMethod()
				    .getTokenCount();
			    double percentageCoverage = ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getCoverage();
			    SecondaryMccObject temp = new SecondaryMccObject(j + 1, fid, mId, tokenCoverage, percentageCoverage,
				    methodName, fileName);
			    cloneList.addToList(temp);

			}
			mccList.getList().get(i).setCloneList(cloneList);
		    }
		}

	    }
	 
	 

}


