package cmt.cvac.views;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.internal.dialogs.ViewComparator;
import org.eclipse.ui.part.ViewPart;

import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.common.Directories;
import cmt.common.ProjectImages;
import cmt.cvac.contentprovidor.PrimaryFcsObjectCrossDirsContentProvider;
import cmt.cvac.contentprovidor.PrimaryFcsObjectInDirsContentProvider;
import cmt.cvac.contentprovidor.SecondaryFcsObjectCrossDirsContentProvider;
import cmt.cvac.contentprovidor.SecondaryFcsObjectInDirsContentProvider;
import cmt.cvac.labelprovidor.PrimaryFcsObjectCrossDirsLabelProvider;
import cmt.cvac.labelprovidor.PrimaryFcsObjectInDirsLabelProvider;
import cmt.cvac.labelprovidor.SecondaryFcsObjectCrossDirsLabelProvider;
import cmt.cvac.labelprovidor.SecondaryFcsObjectInDirsLabelProvider;
import cmt.cvac.treemapvisualization.TreeMapInit;
import cmt.cvac.treemapvisualization.TreeMapInitFCS;
import cmt.cvac.viewobjects.FcsList;
import cmt.cvac.viewobjects.FcsListInDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectInDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectInDirs;

public class FcsViewInDirs extends ViewPart {

 	public static boolean filterFlag;
	public static boolean filtered;
	public TableViewer primaryFcsViewer;
    private TableViewer secondaryFcsViewer;
    Table fcsListTable;
    Table fcsInstanceListTable;
    private int fcsIndex;
    private FcsListInDirs list;
    private FcsList filteredList;
    
    public Action treeMapVisualizationAction;
    public Action fileFrameAction;
    public Action filterFileClone;
    int fccId;
    int idOrder;
    static int ASCENDING = 1;
    static int DECENDING = 2;
    
    public static ArrayList<String> fname = new ArrayList();
    public static int fcsID=-1;
    
    
    public FcsViewInDirs()
    {
    	super();
		primaryFcsViewer = null;
		secondaryFcsViewer = null;
		fcsListTable = null;
		fcsInstanceListTable = null;
		list=new FcsListInDirs();
		idOrder = ASCENDING;
    }
@Override
public void createPartControl(final Composite parent) {
	
	GridLayout layout = new GridLayout(2, false);
	parent.setLayout(layout);
	createFccTables(parent);
	fcsListTable.setHeaderVisible(true);
	fcsListTable.setLinesVisible(true);
	fcsInstanceListTable.setHeaderVisible(true);
	fcsInstanceListTable.setLinesVisible(true);
	makeActions();
	contributeToActionBars();
	
	if(list == null)
	{
		filterFileClone.setEnabled(false);
	}
	else
	{
		filterFileClone.setEnabled(true);
	}
	

	final TableCursor fccListTableCursor = new TableCursor(fcsListTable, SWT.NULL);
	final TableCursor fccInstanceListTableCursor = new TableCursor(fcsInstanceListTable, SWT.NULL);
	primaryFcsViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryFcsViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryFcsObjectInDirs object1 = (PrimaryFcsObjectInDirs) e1;
				PrimaryFcsObjectInDirs object2 = (PrimaryFcsObjectInDirs) e2;
				int result = 0;
				result = object1.getFcsId() > (object2.getFcsId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			primaryFcsViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	
	primaryFcsViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryFcsViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
			    	PrimaryFcsObjectInDirs object1 = (PrimaryFcsObjectInDirs) e1;
			    	PrimaryFcsObjectInDirs object2 = (PrimaryFcsObjectInDirs) e2;
				int result = 0;
				result = object1.getNumberOfInstance() > (object2.getNumberOfInstance()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			primaryFcsViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFcsViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFcsViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFcsObjectInDirs object1 = (SecondaryFcsObjectInDirs) e1;
				SecondaryFcsObjectInDirs object2 = (SecondaryFcsObjectInDirs) e2;
				int result = 0;
				result = object1.getInstanceId() > (object2.getInstanceId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFcsViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFcsViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFcsViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
			    	SecondaryFcsObjectInDirs object1 = (SecondaryFcsObjectInDirs) e1;
			    	SecondaryFcsObjectInDirs object2 = (SecondaryFcsObjectInDirs) e2;
				int result = 0;
				result = object1.getFId() > (object2.getFId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFcsViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFcsViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFcsViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
			    	SecondaryFcsObjectInDirs object1 = (SecondaryFcsObjectInDirs) e1;
			    	SecondaryFcsObjectInDirs object2 = (SecondaryFcsObjectInDirs) e2;
				int result = 0;
				result = object1.getInstanceId() > (object2.getInstanceId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFcsViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFcsViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFcsViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
			    	SecondaryFcsObjectInDirs object1 = (SecondaryFcsObjectInDirs) e1;
			    	SecondaryFcsObjectInDirs object2 = (SecondaryFcsObjectInDirs) e2;
				int result = 0;
				result = object1.getFId() > (object2.getFId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFcsViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	primaryFcsViewer.getTable().addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseDown(MouseEvent e)
	    {
		int row = Integer.parseInt((fccListTableCursor.getRow().getText()));
		fcsIndex = row;
		
		fileFrameAction.setEnabled(true);
		/*if (ClonesReader.getMethodClones().get(mccIndex).isFramed())
		{
		    editFrameWizardAction.setEnabled(true);
		    deleteFrameAction.setEnabled(true);
		    cliqueSelectionWizardAction.setEnabled(false);
		} else
		{
		    editFrameWizardAction.setEnabled(false);
		    deleteFrameAction.setEnabled(false);
		    cliqueSelectionWizardAction.setEnabled(true);
		}*/
		// int column=(mccListTableCursor.getColumn());
		// if(column==1)
		// {
		SecondaryFcsObjectInDirsContentProvider secondaryFcsObjectContentProvider = new SecondaryFcsObjectInDirsContentProvider();
		secondaryFcsObjectContentProvider.setList(list.getList().get(row).getCloneList());
		secondaryFcsViewer.setContentProvider(secondaryFcsObjectContentProvider);
		secondaryFcsViewer.setLabelProvider(new SecondaryFcsObjectInDirsLabelProvider());
		secondaryFcsViewer.setInput(list.getList().get(row).getCloneList());
		secondaryFcsViewer.refresh();
		
		fname.clear();
		fcsID=-1;
		for(int i=0;i<list.getList().get(row).getCloneList().size();i++)
		{
			fname.add(list.getList().get(row).getCloneList().get(i).getFileName());
		}
		fcsID=list.getList().get(row).getFcsId();
		
		treeMapVisualizationAction.setEnabled(true);
		
		// }
	    }
	});
}

@Override
public void setFocus() {
	
	primaryFcsViewer.getControl().setFocus();
	secondaryFcsViewer.getControl().setFocus();
}

public void createFccTables(Composite parent)
{
	primaryFcsViewer = new TableViewer(parent,
		SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createFccTableColumns(parent, primaryFcsViewer);
	// setMccInput();
	populateFcsListInDirs();
	PrimaryFcsObjectInDirsContentProvider primaryFcsObjectContentProvider = new PrimaryFcsObjectInDirsContentProvider();
	primaryFcsObjectContentProvider.setList(list.getList());
	primaryFcsViewer.setContentProvider(primaryFcsObjectContentProvider);
	primaryFcsViewer.setLabelProvider(new PrimaryFcsObjectInDirsLabelProvider());
	primaryFcsViewer.setInput(list);
	primaryFcsViewer.refresh();
	secondaryFcsViewer = new TableViewer(parent,
		SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createFccCloneListTableColumns(parent, secondaryFcsViewer);
	//markExistingMcc();
}



 private void contributeToActionBars()
 {
	IActionBars bars = getViewSite().getActionBars();
	// fillLocalPullDown(bars.getMenuManager());
	fillLocalToolBar(bars.getToolBarManager());
 }
 private void createFccTableColumns(Composite parent, TableViewer primaryFccViewer)
 {
	String[] titles = { "FCS-ID", "Fcc-Structure", "Dir-ID", "Number of Instance(s)" };
	int[] bounds = { 60, 150, 50, 150};
	fcsListTable = this.primaryFcsViewer.getTable();
	fcsListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
	createFccTableColumn(titles[0], bounds[0], 0);
	createFccTableColumn(titles[1], bounds[1], 1);
	createFccTableColumn(titles[2], bounds[2], 2);
	createFccTableColumn(titles[3], bounds[3], 3);
	fcsListTable.getColumn(0).setToolTipText("File Clone Structure ID's");
	fcsListTable.getColumn(1).setToolTipText("List of File-Clone-ID's of which this clone is composed of");
	fcsListTable.getColumn(1).setToolTipText("Directory-ID in which this clone is present");
	fcsListTable.getColumn(3).setToolTipText("Total number of instances this clone has");
 } 
 
 private TableViewerColumn createFccTableColumn(String title, int bound, final int colNumber)
 {
	final TableViewerColumn viewerColumn = new TableViewerColumn(primaryFcsViewer, SWT.MULTI | SWT.FULL_SELECTION);
	final TableColumn column = viewerColumn.getColumn();
	column.setText(title);
	column.setWidth(bound);
	column.setResizable(true);
	column.setMoveable(true);
	return viewerColumn;
 }

 private void createFccCloneListTableColumns(Composite parent, TableViewer secondaryFccViewer)
 {
	String[] titles = { "Instance ID", "FID", "Fcc-ID", "File Name" };
	int[] bounds = { 50, 50, 50, 150 };
	fcsInstanceListTable = this.secondaryFcsViewer.getTable();
	fcsInstanceListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
	createFccCloneListTableColumn(titles[0], bounds[0], 0);
	createFccCloneListTableColumn(titles[1], bounds[1], 1);
	createFccCloneListTableColumn(titles[2], bounds[2], 2);
	createFccCloneListTableColumn(titles[3], bounds[3], 3);
	
	fcsInstanceListTable.getColumn(0).setToolTipText("ID");
	fcsInstanceListTable.getColumn(1).setToolTipText("File ID");
	fcsInstanceListTable.getColumn(2).setToolTipText("Fcc ID");
	fcsInstanceListTable.getColumn(3).setToolTipText("Name of file that contains this clone");

  }
 
 private TableViewerColumn createFccCloneListTableColumn(String title, int bound, final int colNumber)
 {
	final TableViewerColumn viewerColumn = new TableViewerColumn(secondaryFcsViewer, SWT.NONE);
	final TableColumn column = viewerColumn.getColumn();
	column.setText(title);
	column.setWidth(bound);
	column.setResizable(true);
	column.setMoveable(true);
	return viewerColumn;
  }
 
private void fillLocalToolBar(IToolBarManager manager)
{
	//manager.add(fileFrameAction);
	//manager.add(new Separator());
	//manager.manager.add(filterFileClone);
	manager.add(treeMapVisualizationAction);
	manager.add(new Separator());
}

public void makeActions(){
	
	treeMapVisualizationAction= new Action() {
		@Override
	    public void run()
	    {
			ArrayList<Integer> nullList=new ArrayList<>();
			TreeMapInitFCS.init(fname,fcsID,nullList);
			treeMapVisualizationAction.setEnabled(false);
	    }
	};
	
	treeMapVisualizationAction.setText("Run TreeMap Visualization");
	treeMapVisualizationAction.setToolTipText("Run TreeMap Visualization");
	treeMapVisualizationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_TREEMAPVISUALIZATION)));
	treeMapVisualizationAction.setEnabled(false);
	
	fileFrameAction = new Action() {
	    @Override
	    public void run()
	    {
	    	// framing commented
		/*try {
			MCSTemplate.generateTemplate(fcsIndex);
			
			FileHandler.fileHandler=new FileHandler("MCS_Template//MCS_"+fcsIndex);
		    FileHandler.fileHandler.getARTFilesList();
		    FileHandler.fileHandler.getARTFilesPaths();
			for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
		    {
		    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
		 
			    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
			    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			    IDE.openEditorOnFileStore(page,fileStore);
		    }
		}
		catch (Exception e)
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}*/
	    
	    }
	
	};
	
	
	ImageDescriptor desc = ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FRAMING));
	
	
	fileFrameAction.setText("Frame");
	fileFrameAction.setToolTipText("Frame File Clone Instance");
	fileFrameAction.setImageDescriptor(desc);
	fileFrameAction.setEnabled(false);
	filterFileClone = new Action() {
		public void run() {
			Shell shell01 = new Shell();
			FilterFileClones filter = new FilterFileClones();
			
			filter.open();
		}
	};
	filterFileClone.setEnabled(false);
	filterFileClone.setToolTipText("Filter File Clones");
	filterFileClone.setText("Filter File Clones");
	filterFileClone.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FILTERATION)));

	
	
	
}

 private void populateFcsListInDirs()
 {
	 if(!(FileClonesReader.getFcsListInDirs()==null)) {
		 for(int i=0;i<FileClonesReader.getFcsListInDirs().size();i++) {
			 list.addToList(FileClonesReader.getFcsListInDirs().get(i));
		 }
	 }
	 
	
 }


}
