package cmt.cvac.views;

import java.awt.Color;
import java.util.ArrayList;

import antlr.auto.gen.java.JavaLexer;

public class TempClassJavaKeyWords {

	public static ArrayList<Integer> javaComments = new ArrayList<Integer>();
	public static ArrayList<Integer> javaDocument = new ArrayList<Integer>();
	public static ArrayList<Integer> javaKeyword = new ArrayList<Integer>();
	public static ArrayList<Integer> javaLiteralColor = new ArrayList<Integer>();

	
	
	
	public static void setJavaGrammer(){
	
		javaComments.add(JavaLexer.COMMENT);
		javaComments.add(JavaLexer.LINE_COMMENT);
		
		javaDocument.add(JavaLexer.PUBLIC);
		javaDocument.add(JavaLexer.VOID);
		javaDocument.add(JavaLexer.IMPORT);
		javaDocument.add(JavaLexer.PACKAGE);
		javaDocument.add(JavaLexer.CLASS);
		javaDocument.add(JavaLexer.PROTECTED);
		
		javaKeyword.add(JavaLexer.INT);
		javaKeyword.add(JavaLexer.ABSTRACT);
		javaKeyword.add(JavaLexer.ASSERT);
		javaKeyword.add(JavaLexer.BOOLEAN);
		javaKeyword.add(JavaLexer.BREAK);
		javaKeyword.add(JavaLexer.BYTE);
		javaKeyword.add(JavaLexer.CASE);
		javaKeyword.add(JavaLexer.CATCH);
		javaKeyword.add(JavaLexer.CHAR);
		javaKeyword.add(JavaLexer.CLASS);
		javaKeyword.add(JavaLexer.CONST);
		javaKeyword.add(JavaLexer.CONTINUE);
		javaKeyword.add(JavaLexer.DEFAULT);
		javaKeyword.add(JavaLexer.DO);
		javaKeyword.add(JavaLexer.DOUBLE);
		javaKeyword.add(JavaLexer.ELSE);
		javaKeyword.add(JavaLexer.ENUM);
		javaKeyword.add(JavaLexer.EXTENDS);
		javaKeyword.add(JavaLexer.FINAL);
		javaKeyword.add(JavaLexer.FINALLY);
		javaKeyword.add(JavaLexer.FLOAT);
		javaKeyword.add(JavaLexer.FOR);
		javaKeyword.add(JavaLexer.GOTO);
		javaKeyword.add(JavaLexer.IF);
		javaKeyword.add(JavaLexer.IMPLEMENTS);
		javaKeyword.add(JavaLexer.IMPORT);
		javaKeyword.add(JavaLexer.INSTANCEOF);
		javaKeyword.add(JavaLexer.INTERFACE);
		javaKeyword.add(JavaLexer.LONG);
		javaKeyword.add(JavaLexer.NATIVE);
		javaKeyword.add(JavaLexer.NEW);
		javaKeyword.add(JavaLexer.PACKAGE);
		javaKeyword.add(JavaLexer.PRIVATE);
		javaKeyword.add(JavaLexer.PROTECTED);
		javaKeyword.add(JavaLexer.PUBLIC);
		javaKeyword.add(JavaLexer.RETURN);
		javaKeyword.add(JavaLexer.SHORT);
		javaKeyword.add(JavaLexer.STATIC);
		javaKeyword.add(JavaLexer.STRICTFP);
		javaKeyword.add(JavaLexer.SUPER);
		javaKeyword.add(JavaLexer.SWITCH);
		javaKeyword.add(JavaLexer.SYNCHRONIZED);
		javaKeyword.add(JavaLexer.THIS);
		javaKeyword.add(JavaLexer.THROW);
		javaKeyword.add(JavaLexer.THROWS);
		javaKeyword.add(JavaLexer.TRANSIENT);
		javaKeyword.add(JavaLexer.TRY);
		javaKeyword.add(JavaLexer.VOID);
		javaKeyword.add(JavaLexer.VOLATILE);
		javaKeyword.add(JavaLexer.WHILE);
		javaKeyword.add(JavaLexer.INT);
		javaKeyword.add(JavaLexer.StringLiteral);
		javaKeyword.add(JavaLexer.NullLiteral);
		
		
		
	
		
	}
	
	
	
}




/*
 private static final Color JAVA_COMMENT_COLOR = new Color(63, 127, 95);
	private static final Color JAVA_DOCCOMMENT_COLOR = new Color(63, 95, 191);
	private static final Color JAVA_KEYWORD_COLOR = new Color(127, 0, 85);

	private static final Color JAVA_LITERAL_COLOR = new Color(42, 0, 255);
	*/
 