package cmt.cvac.views;

import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.SWTResourceManager;

import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.simpleclones.SimpleClonesReader;

public class FilterSimpleClones {
	
	

	protected Object result;
	protected Shell shell;
	private Text minMember;
	private Text maxMember;
	private Text minLength;
	private Text maxLength;
	private Text locationIDs;
	private Text minBenefit;
	private Text maxBenefit;
	private Button btnFind;
	public static Composite container;

	
	/**
	 * Open the window.
	 */public void open() {
			Display display = Display.getDefault();
			createContents();
			Monitor primary = display.getPrimaryMonitor();
		    Rectangle bounds = primary.getBounds();
		    Rectangle rect = shell.getBounds();
		    int x = bounds.x + (bounds.width - rect.width) / 2;
		    int y = bounds.y + (bounds.height - rect.height) / 2;
		    
		    shell.setLocation(x, y);
			shell.open();
			shell.layout();
			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		}
	 
	 public void close() {
		 shell.close();
	 }
	
	 /**
		 * Create contents of the window.
		 */
		protected void createContents() {
			shell = new Shell(SWT.CLOSE | SWT.TITLE | SWT.MIN);
			shell.setSize(392, 256);
			shell.setText("Filter Simple Clones");
			
			
			
			Group grpLocation = new Group(shell, SWT.NONE);
			grpLocation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			grpLocation.setText("Location");
			grpLocation.setBounds(197, 21, 178, 82);
			
			Label lblLocation = new Label(grpLocation, SWT.NONE);
			lblLocation.setBounds(10, 24, 65, 15);
			lblLocation.setText("Location");
			
			Combo location = new Combo(grpLocation, SWT.NONE);
			location.setBounds(81, 21, 78, 23);
			location.setItems(new String[] {"Method", "File"});
			location.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					if(!location.getText().equals("")) {
						locationIDs.setEnabled(true);
					}
					
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
				}
			});
			
			
			Label lblLocationIds = new Label(grpLocation, SWT.NONE);
			lblLocationIds.setBounds(10, 58, 65, 15);
			lblLocationIds.setText("Location IDs");
			
			locationIDs = new Text(grpLocation, SWT.BORDER);
			locationIDs.setBounds(81, 50, 78, 21);
			locationIDs.setEnabled(false);
			locationIDs.addListener(SWT.MouseDown, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
					 Color white = new Color(Display.getCurrent(), new RGB(255,255,255));
					 locationIDs.setBackground(white);
				        /*char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9' || chars[i] ==',')) {
				            e.doit = false;
				            return;
				          }
				        }
					 if(!chkText(string)) {
						 e.doit = false;
				            return;
					 }*/
					
				}
				
			});
			
			Group grpBenefit = new Group(shell, SWT.NONE);
			grpBenefit.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			grpBenefit.setText("Benefit");
			grpBenefit.setBounds(197, 109, 178, 82);
			
			Label lblMinBenefit = new Label(grpBenefit, SWT.NONE);
			lblMinBenefit.setBounds(10, 23, 68, 15);
			lblMinBenefit.setText("Min Benefit");
			
			minBenefit = new Text(grpBenefit, SWT.BORDER);
			minBenefit.setBounds(84, 20, 76, 21);
			minBenefit.addListener(SWT.Verify, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
				        char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9')) {
				            e.doit = false;
				            return;
				          }
				        }
					
				}
			});
			
			Label lblMaxBenefit = new Label(grpBenefit, SWT.NONE);
			lblMaxBenefit.setBounds(10, 50, 68, 15);
			lblMaxBenefit.setText("Max Benefit");
			
			maxBenefit = new Text(grpBenefit, SWT.BORDER);
			maxBenefit.setBounds(84, 47, 76, 21);
			maxBenefit.addListener(SWT.Verify, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
				        char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9')) {
				            e.doit = false;
				            return;
				          }
				        }
					
				}
			});
			
			Group grpMember = new Group(shell, SWT.NONE);
			grpMember.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			grpMember.setText("Member");
			grpMember.setBounds(10, 21, 181, 82);
			
			Label lblMinMember = new Label(grpMember, SWT.NONE);
			lblMinMember.setBounds(10, 21, 77, 15);
			lblMinMember.setText("Min Member");
			
			minMember = new Text(grpMember, SWT.BORDER);
			minMember.setBounds(93, 18, 78, 21);
			minMember.addListener(SWT.Verify, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
				        char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9')) {
				            e.doit = false;
				            return;
				          }
				        }
					
				}
			});
			
			Label lblMaxMember = new Label(grpMember, SWT.NONE);
			lblMaxMember.setBounds(9, 45, 78, 15);
			lblMaxMember.setText("Max Member");
			
			maxMember = new Text(grpMember, SWT.BORDER);
			maxMember.setBounds(93, 42, 78, 21);
			maxMember.addListener(SWT.Verify, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
				        char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9')) {
				            e.doit = false;
				            return;
				          }
				        }
					
				}
			});
			
			Group grpLength = new Group(shell, SWT.NONE);
			grpLength.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
			grpLength.setText("Length");
			grpLength.setBounds(13, 109, 178, 82);
			
			Label lblMinLength = new Label(grpLength, SWT.NONE);
			lblMinLength.setBounds(10, 25, 78, 15);
			lblMinLength.setText("Min Length");
			
			minLength = new Text(grpLength, SWT.BORDER);
			minLength.setBounds(94, 22, 76, 21);
			minLength.addListener(SWT.Verify, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
				        char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9')) {
				            e.doit = false;
				            return;
				          }
				        }
					
				}
			});
			
			Label lblMaxLength = new Label(grpLength, SWT.NONE);
			lblMaxLength.setBounds(10, 46, 78, 15);
			lblMaxLength.setText("Max Length");
			
			maxLength = new Text(grpLength, SWT.BORDER);
			maxLength.setBounds(94, 49, 76, 21);
			maxLength.addListener(SWT.Verify, new Listener() {
				
				@Override
				public void handleEvent(Event e) {
					// TODO Auto-generated method stub
					 String string = e.text;
				        char[] chars = new char[string.length()];
				        string.getChars(0, chars.length, chars, 0);
				        for (int i = 0; i < chars.length; i++) {
				          if (!('0' <= chars[i] && chars[i] <= '9')) {
				            e.doit = false;
				            return;
				          }
				        }
					
				}
			});
			btnFind = new Button(shell, SWT.NONE);
			btnFind.setBounds(300, 197, 75, 25);
			btnFind.setText("Find");
			
			btnFind.addSelectionListener(new SelectionListener() {
				
				@Override
				public void widgetSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					if(!location.getText().toString().isEmpty() ||  !locationIDs.getText().isEmpty() || !maxBenefit.getText().isEmpty() || !minBenefit.getText().isEmpty() || !minLength.getText().isEmpty() || !maxLength.getText().isEmpty() ||!minMember.getText().isEmpty() || !maxMember.getText().isEmpty()) {
						if(!locationIDs.getText().isEmpty()) {
							if(chkText(locationIDs.getText())) {
								SimpleClonesReader.filterClones(location.getText().toString() , locationIDs.getText(), maxBenefit.getText(), minBenefit.getText(), minLength.getText(), maxLength.getText(),minMember.getText(), maxMember.getText());
							}
							else{
								Color pink = new Color(Display.getDefault(), new RGB(255,182,193));
								locationIDs.setBackground(pink);
								
								return;
								
							}
							
						}
						SimpleClonesReader.filterClones(location.getText().toString() , locationIDs.getText(), maxBenefit.getText(), minBenefit.getText(), minLength.getText(), maxLength.getText(),minMember.getText(), maxMember.getText());
					}
				
					
					SccView.filterFlag = true;
					
					
					IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					   try
						{
						   	
						   IViewPart sccView = workbenchPage.findView("sccView");
						   if (sccView != null) {
								workbenchPage.hideView(sccView);
							}
							
						   
						    SccView.filterFlag = true;
						   	SccView.filtered = true;
						    IViewPart p = workbenchPage.showView("sccView");
						    p.setFocus();
						    

						}
					   catch (Exception e)
						{
						    e.printStackTrace();
						}

					close();
					
					//SccView.applyFilter();
					
				}
				
				@Override
				public void widgetDefaultSelected(SelectionEvent arg0) {
					// TODO Auto-generated method stub
					
					
					
				}
			});

			
			
		}
		public boolean chkText(String input) {
			String regex = "[0-9]+(,[0-9]+)*,?";
			Pattern pattern = Pattern.compile(regex);
			  boolean flag = pattern.matches(regex, input);
			  if(flag) {
				  return true;
			  }
			  else {
				  return false;
			  }
		}
		

}
