package cmt.cvac.views;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Locale;

import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.dialogs.ViewComparator;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.ITextEditor;

import com.macrofocus.igraphics.Rectangle;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleClonesReader;
import cmt.common.Directories;
import cmt.common.ProjectImages;
import cmt.cvac.clonecomparsion.OpenCloneInspectionViewerAction;
import cmt.cvac.clonecomparsion.OpenSimpleCloneInspectionViewerAction;
import cmt.cvac.contentprovidor.SccInstanceListContentProvider;
import cmt.cvac.contentprovidor.SccListContentProvider;
import cmt.cvac.labelprovidor.SccCloneInstanceListLabelProvider;
import cmt.cvac.labelprovidor.SccListLabelProvider;
import cmt.cvac.treemapvisualization.TreeMapInit;
import cmt.cvac.viewobjects.PrimarySccObject;
import cmt.cvac.viewobjects.SccCloneInstanceList;
import cmt.cvac.viewobjects.SccList;
import cmt.cvac.viewobjects.SecondarySccObject;



public class SccView extends ViewPart {

    private TableViewer primarySccViewer;
    TableCursor sccListTableCursor ;
	TableCursor sccInstanceListTableCursor; 
    public static boolean filterFlag = false;
    public static boolean filtered = false;
    private TableViewer secondarySccViewer;
    private SccList sccList;
    private SccList primarySCCFilteredList;
    private SccCloneInstanceList cloneList;
    private SccCloneInstanceList secondarySCCFilteredList;
    public Action goToLocationAction;
    public Action treeMapVisualizationAction;
    public Action filterSimpleClonesAction;
    public Action unFilterSimpleClones;
    public Action sideBysideViewAction;
    Table sccListTable;
    Table sccInstanceListTable;
    static int ASCENDING = 1;
    static int DECENDING = 2;
    int cloneNumberOrder;
    int sccIndex = 0;
    public static ArrayList<String> fname = new ArrayList();
    public ArrayList<SimpleCloneInstance> instances;
    public static int sccID=-1;
    Listener sortPrimaryTableListener;

    public SccView()
    {
	// TODO Auto-generated constructor stub
	super();
	primarySccViewer = null;
	secondarySccViewer = null;
	sccList = new SccList();
	primarySCCFilteredList = new SccList();
	cloneList = new SccCloneInstanceList();
	secondarySCCFilteredList = new SccCloneInstanceList();
	sccListTable = null;
	sccInstanceListTable = null;
	cloneNumberOrder = ASCENDING;
	sccListTableCursor = null;
	sccInstanceListTableCursor = null;
	

    }
    @Override
    public void createPartControl(Composite parent)
    {
    	
	GridLayout layout = new GridLayout(2, true);
	parent.setLayout(layout);
	createSccTables(parent);
	sccListTable.setHeaderVisible(true);
	sccListTable.setLinesVisible(true);
	sccInstanceListTable.setHeaderVisible(true);
	sccInstanceListTable.setLinesVisible(true);
	makeActions();
	contributeToActionBars();
	
	if(sccList.getList().isEmpty())
	{
		filterSimpleClonesAction.setEnabled(false);
		sideBysideViewAction.setEnabled(false);
	}
	else
	{
		filterSimpleClonesAction.setEnabled(true);
		sideBysideViewAction.setEnabled(true);
	}
	if(!filtered) {
		unFilterSimpleClones.setEnabled(false);
		sideBysideViewAction.setEnabled(false);
	}
	else {
		unFilterSimpleClones.setEnabled(true);
		sideBysideViewAction.setEnabled(true);
	}
	
	sccListTableCursor = new TableCursor(sccListTable, SWT.NULL);
	sccInstanceListTableCursor = new TableCursor(sccInstanceListTable, SWT.NULL);
	
	primarySccViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		if (cloneNumberOrder == ASCENDING)
		{
		    cloneNumberOrder = DECENDING;
		} else if (cloneNumberOrder == DECENDING)
		{
		    cloneNumberOrder = ASCENDING;
		}
		primarySccViewer.setComparator(new ViewComparator() {
		    @Override
		    public int compare(Viewer viewer, Object e1, Object e2)
		    {
			PrimarySccObject object1 = (PrimarySccObject) e1;
			PrimarySccObject object2 = (PrimarySccObject) e2;
			int result = 0;
			if (object1.getId() == object2.getId())
			{
			    // result=1;
			} else
			{
			    result = object1.getId() >= (object2.getId()) ? -1 : 1;
			}
			if (cloneNumberOrder == ASCENDING)
			{
			    result = -result;
			    return result;
			}
			if (cloneNumberOrder == DECENDING)
			{
			    return result;
			}
			return result;
		    };
		});

		primarySccViewer.refresh();

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});
	primarySccViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		if (cloneNumberOrder == ASCENDING)
		{
		    cloneNumberOrder = DECENDING;
		} else if (cloneNumberOrder == DECENDING)
		{
		    cloneNumberOrder = ASCENDING;
		}
		primarySccViewer.setComparator(new ViewComparator() {
		    @Override
		    public int compare(Viewer viewer, Object e1, Object e2)
		    {
			PrimarySccObject object1 = (PrimarySccObject) e1;
			PrimarySccObject object2 = (PrimarySccObject) e2;
			int result = 0;
			if (object1.getSccId() == object2.getSccId())
			{
			    // result=1;
			} else
			{
			    result = object1.getSccId() >= (object2.getSccId()) ? -1 : 1;
			}
			if (cloneNumberOrder == ASCENDING)
			{
			    result = -result;
			    return result;
			}
			if (cloneNumberOrder == DECENDING)
			{
			    return result;
			}
			return result;
		    };
		});

		primarySccViewer.refresh();

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});
	primarySccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		if (cloneNumberOrder == ASCENDING)
		{
		    cloneNumberOrder = DECENDING;
		} else if (cloneNumberOrder == DECENDING)
		{
		    cloneNumberOrder = ASCENDING;
		}
		primarySccViewer.setComparator(new ViewComparator() {
		    @Override
		    public int compare(Viewer viewer, Object e1, Object e2)
		    {
			PrimarySccObject object1 = (PrimarySccObject) e1;
			PrimarySccObject object2 = (PrimarySccObject) e2;
			int result = 0;
			if (object1.getBenefit() == object2.getBenefit())
			{
			    // result=1;
			} else
			{
			    result = object1.getBenefit() >= (object2.getBenefit()) ? -1 : 1;
			}
			if (cloneNumberOrder == ASCENDING)
			{
			    result = -result;
			    return result;
			}
			if (cloneNumberOrder == DECENDING)
			{
			    return result;
			}
			return result;
		    };
		});

		primarySccViewer.refresh();

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});

	primarySccViewer.getTable().getColumn(4).addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		if (cloneNumberOrder == ASCENDING)
		{
		    cloneNumberOrder = DECENDING;
		} else if (cloneNumberOrder == DECENDING)
		{
		    cloneNumberOrder = ASCENDING;
		}
		primarySccViewer.setComparator(new ViewComparator() {
		    @Override
		    public int compare(Viewer viewer, Object e1, Object e2)
		    {
			PrimarySccObject object1 = (PrimarySccObject) e1;
			PrimarySccObject object2 = (PrimarySccObject) e2;
			int result = 0;
			if (object1.getNumberOfClone() == object2.getNumberOfClone())
			{
			    // result=1;
			} else
			{
			    result = object1.getNumberOfClone() >= (object2.getNumberOfClone()) ? -1 : 1;
			}
			if (cloneNumberOrder == ASCENDING)
			{
			    result = -result;
			    return result;
			}
			if (cloneNumberOrder == DECENDING)
			{
			    return result;
			}
			return result;
		    };
		});

		primarySccViewer.refresh();

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});
	secondarySccViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub

			if (cloneNumberOrder == ASCENDING)
			{
			    cloneNumberOrder = DECENDING;

			} else if (cloneNumberOrder == DECENDING)
			{
			    cloneNumberOrder = ASCENDING;
			}
			secondarySccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondarySccObject object1 = (SecondarySccObject) e1;
				SecondarySccObject object2 = (SecondarySccObject) e2;
				int result = 0;
				if (object1.getId() == object2.getId())
				{
				    // result=1;
				} else
				{
				    result = object1.getId() >= (object2.getId()) ? -1 : 1;
				}
				if (cloneNumberOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (cloneNumberOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			secondarySccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	secondarySccViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub

			if (cloneNumberOrder == ASCENDING)
			{
			    cloneNumberOrder = DECENDING;

			} else if (cloneNumberOrder == DECENDING)
			{
			    cloneNumberOrder = ASCENDING;
			}
			secondarySccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondarySccObject object1 = (SecondarySccObject) e1;
				SecondarySccObject object2 = (SecondarySccObject) e2;
				int result = 0;
				if (object1.getStartLine() == object2.getStartLine())
				{
				    // result=1;
				} else
				{
				    result = object1.getStartLine() >= (object2.getStartLine()) ? -1 : 1;
				}
				if (cloneNumberOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (cloneNumberOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			secondarySccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	secondarySccViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub

			if (cloneNumberOrder == ASCENDING)
			{
			    cloneNumberOrder = DECENDING;

			} else if (cloneNumberOrder == DECENDING)
			{
			    cloneNumberOrder = ASCENDING;
			}
			secondarySccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondarySccObject object1 = (SecondarySccObject) e1;
				SecondarySccObject object2 = (SecondarySccObject) e2;
				int result = 0;
				if (object1.getEndLine() == object2.getEndLine())
				{
				    // result=1;
				} else
				{
				    result = object1.getEndLine() >= (object2.getEndLine()) ? -1 : 1;
				}
				if (cloneNumberOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (cloneNumberOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			secondarySccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	secondarySccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub

			if (cloneNumberOrder == ASCENDING)
			{
			    cloneNumberOrder = DECENDING;

			} else if (cloneNumberOrder == DECENDING)
			{
			    cloneNumberOrder = ASCENDING;
			}
			secondarySccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondarySccObject object1 = (SecondarySccObject) e1;
				SecondarySccObject object2 = (SecondarySccObject) e2;
				int result = 0;
				if (object1.getStartColumn() == object2.getStartColumn())
				{
				    // result=1;
				} else
				{
				    result = object1.getStartColumn() >= (object2.getStartColumn()) ? -1 : 1;
				}
				if (cloneNumberOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (cloneNumberOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			secondarySccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	secondarySccViewer.getTable().getColumn(4).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub

			if (cloneNumberOrder == ASCENDING)
			{
			    cloneNumberOrder = DECENDING;

			} else if (cloneNumberOrder == DECENDING)
			{
			    cloneNumberOrder = ASCENDING;
			}
			secondarySccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondarySccObject object1 = (SecondarySccObject) e1;
				SecondarySccObject object2 = (SecondarySccObject) e2;
				int result = 0;
				if (object1.getEndColumn() == object2.getEndColumn())
				{
				    // result=1;
				} else
				{
				    result = object1.getEndColumn() >= (object2.getEndColumn()) ? -1 : 1;
				}
				if (cloneNumberOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (cloneNumberOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			secondarySccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondarySccViewer.getTable().getColumn(5).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub

			if (cloneNumberOrder == ASCENDING)
			{
			    cloneNumberOrder = DECENDING;

			} else if (cloneNumberOrder == DECENDING)
			{
			    cloneNumberOrder = ASCENDING;
			}
			secondarySccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondarySccObject object1 = (SecondarySccObject) e1;
				SecondarySccObject object2 = (SecondarySccObject) e2;
				int result = 0;
				if (object1.getFId() == object2.getFId())
				{
				    // result=1;
				} else
				{
				    result = object1.getFId() >= (object2.getFId()) ? -1 : 1;
				}
				if (cloneNumberOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (cloneNumberOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});

			secondarySccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
//	secondarySccViewer.getTable().getColumn(6).addSelectionListener(new SelectionListener() {
//		
//		@Override
//		public void widgetSelected(SelectionEvent arg0) {
//			// TODO Auto-generated method stub
//
//			if (cloneNumberOrder == ASCENDING)
//			{
//			    cloneNumberOrder = DECENDING;
//
//			} else if (cloneNumberOrder == DECENDING)
//			{
//			    cloneNumberOrder = ASCENDING;
//			}
//			secondarySccViewer.setComparator(new ViewComparator() {
//			    @Override
//			    public int compare(Viewer viewer, Object e1, Object e2)
//			    {
//				SecondarySccObject object1 = (SecondarySccObject) e1;
//				SecondarySccObject object2 = (SecondarySccObject) e2;
//				int result = 0;
//				if (object1.getFileName() == object2.getFileName())
//				{
//				    // result=1;
//				} else
//				{
//				    result = object1.getFileName() >= (object2.getFileName()) ? -1 : 1;
//				}
//				if (cloneNumberOrder == ASCENDING)
//				{
//				    result = -result;
//				    return result;
//				}
//				if (cloneNumberOrder == DECENDING)
//				{
//				    return result;
//				}
//				return result;
//			    };
//			});
//
//			secondarySccViewer.refresh();
//			
//		}
//		
//		@Override
//		public void widgetDefaultSelected(SelectionEvent arg0) {
//			// TODO Auto-generated method stub
//			
//		}
//	});
	
	primarySccViewer.getTable().addMouseListener(new MouseAdapter() {
		@Override
		public void mouseDown(MouseEvent e)
		{
			if(sccList.getList().isEmpty())
			{
				sideBysideViewAction.setEnabled(false);
			}
			else {
				sideBysideViewAction.setEnabled(true);
			}
		}
	
	});
	primarySccViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		if (cloneNumberOrder == ASCENDING)
		{
		    cloneNumberOrder = DECENDING;

		} else if (cloneNumberOrder == DECENDING)
		{
		    cloneNumberOrder = ASCENDING;
		}
		primarySccViewer.setComparator(new ViewComparator() {
		    @Override
		    public int compare(Viewer viewer, Object e1, Object e2)
		    {
			PrimarySccObject object1 = (PrimarySccObject) e1;
			PrimarySccObject object2 = (PrimarySccObject) e2;
			int result = 0;
			if (object1.getLength() == object2.getLength())
			{
			    // result=1;
			} else
			{
			    result = object1.getLength() >= (object2.getLength()) ? -1 : 1;
			}
			if (cloneNumberOrder == ASCENDING)
			{
			    result = -result;
			    return result;
			}
			if (cloneNumberOrder == DECENDING)
			{
			    return result;
			}
			return result;
		    };
		});

		primarySccViewer.refresh();
	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});
	primarySccViewer.getTable().addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseDown(MouseEvent e)
	    {
	    	if(filterFlag) {
	    		
	    		secondarySccViewer.refresh();
	    		int row = Integer.parseInt((sccListTableCursor.getRow().getText()));
	    		sccListTableCursor.getColumn();
	    		// System.out.println(row);
	    		sccIndex = row;
	    		SccInstanceListContentProvider sccCloneInstanceListContentProvider = new SccInstanceListContentProvider();
	    		sccCloneInstanceListContentProvider.setList(primarySCCFilteredList.getList().get(row).getCloneList().getList());
	    		secondarySccViewer.setContentProvider(sccCloneInstanceListContentProvider);
	    		secondarySccViewer.setLabelProvider(new SccCloneInstanceListLabelProvider());
	    		//System.out.println(sccList.getList().get(row).getCloneList().getList().size());
	    		secondarySccViewer.setInput(primarySCCFilteredList.getList().get(row).getCloneList().getList());
	    		// secondarySccViewer.setInput(cloneList.getList());
	    		
	    		//add all the File names in fname and pass it into the treeMapInit for visualization
	    		fname.clear();
	    		sccID=-1;
	    		for(int i=0;i<primarySCCFilteredList.getList().get(row).getCloneList().getList().size();i++)
	    		{
	    			fname.add(primarySCCFilteredList.getList().get(row).getCloneList().getList().get(i).getFileName());
	    		}
	    		sccID=primarySCCFilteredList.getList().get(row).getSccId();
	    		treeMapVisualizationAction.setEnabled(true);
	    		
	    	}
	    	else {
	    		int row = Integer.parseInt((sccListTableCursor.getRow().getText()));
	    		sccListTableCursor.getColumn();
	    		// System.out.println(row);
	    		sccIndex = row;
	    		SccInstanceListContentProvider sccCloneInstanceListContentProvider = new SccInstanceListContentProvider();
	    		sccCloneInstanceListContentProvider.setList(sccList.getList().get(row).getCloneList().getList());
	    		secondarySccViewer.setContentProvider(sccCloneInstanceListContentProvider);
	    		secondarySccViewer.setLabelProvider(new SccCloneInstanceListLabelProvider());
	    		//System.out.println(sccList.getList().get(row).getCloneList().getList().size());
	    		secondarySccViewer.setInput(sccList.getList().get(row).getCloneList().getList());
	    		// secondarySccViewer.setInput(cloneList.getList());
	    		secondarySccViewer.refresh();
	    		
	    		//add all the File names in fname and pass it into the treeMapInit for visualization
	    		fname.clear();
	    		sccID=-1;
	    		for(int i=0;i<sccList.getList().get(row).getCloneList().getList().size();i++)
	    		{
	    			fname.add(sccList.getList().get(row).getCloneList().getList().get(i).getFileName());
	    		}
	    		sccID=sccList.getList().get(row).getSccId();
	    		treeMapVisualizationAction.setEnabled(true);
	    	}
		
	    	goToLocationAction.setEnabled(false);


	    }
	});

	secondarySccViewer.getTable().addMouseListener(new MouseAdapter() {
	    @Override
	    public void mouseDown(MouseEvent e)
	    {	    		
	    	goToLocationAction.setEnabled(true);
	    }
	});
	
   }

    private void makeActions()
    {
    	goToLocationAction = new Action() {
    	public void run() {

    			int row = Integer.parseInt((sccInstanceListTableCursor.getRow().getText()));
    			sccInstanceListTableCursor.getColumn();
    			// if(column==0){
    			SimpleCloneInstance sccInstance = ClonesReader.getSimpleClones().get(sccIndex).getSCCInstances().get(--row);
    			File fileToOpen = new File(sccInstance.getFilePath());
    			String content = getFileContent(fileToOpen);
    			getIFile(fileToOpen);
    			if (fileToOpen.exists() && fileToOpen.isFile())
    				{
    				    /*IFileStore fileStore = EFS.getLocalFileSystem().getStore(fileToOpen.toURI());
    				    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();*/
    				    try
    				    {
	    					/*ITextEditor sourceEditor = (ITextEditor) IDE.openEditorOnFileStore(page, fileStore);
	    					content=content.replace("\r", "");
	    					content=content.replace("\t", "        ");
	    					int startChar = content.indexOf(sccInstance.getCodeSegment());
	    					int offset = sccInstance.getCodeSegment().length();*/
	    					// highlightCode(ifile, sourceEditor, startChar, offset);
	    					//sourceEditor.selectAndReveal(8, offset);
	    					// printInPopup(sccInstance.getCodeSegment());
	    					// printInPopup("CharStart: "+startChar+"\nOffset: "+ offset);
	    					MccView.navigateToLine(MccView.getIFile(fileToOpen), sccInstance.getStartingIndex());
    		
    				    } 
    				    catch (Exception ex)
    				    {
    				    	//ex.printStackTrace();
    				    	MessageConsole myConsole = new MessageConsole("My Console", null);
    					    ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ myConsole });
    					    ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
    					    final MessageConsoleStream stream = myConsole.newMessageStream();
    					    stream.setActivateOnWrite(true);
    					    stream.println("File Not available");
    						    try 
    						    {
    								stream.close();
    							}
    						    catch (IOException e2) 
    						    {
    								e2.printStackTrace();
    							}
    				    }
    				}
    			else
    				{
    				    printInPopup(fileToOpen.getName() + " doesn't Exist!");
    				}
    	    
			}
    	};
    	goToLocationAction.setText("Go to Clone Location");
    	goToLocationAction.setToolTipText("Go to Clone Location");
    	goToLocationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.GO_TO_FILE_LOCATION)));
    	goToLocationAction.setEnabled(false);
    	
    	
    	treeMapVisualizationAction= new Action() {
    		@Override
    	    public void run()
    	    {
    			ArrayList<Integer> nullList=new ArrayList<>();
    			TreeMapInit.init(fname,sccID,nullList);
    			treeMapVisualizationAction.setEnabled(false);
    	    }
    	};
    	
    	treeMapVisualizationAction.setText("Run TreeMap Visualization");
    	treeMapVisualizationAction.setToolTipText("Run TreeMap Visualization");
    	treeMapVisualizationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_TREEMAPVISUALIZATION)));
    	treeMapVisualizationAction.setEnabled(false);
    	
    	
    	filterSimpleClonesAction = new Action() {
			public void run() {
				Shell shell01 = new Shell();
				FilterSimpleClones filter = new FilterSimpleClones();
				
				filter.open();
			}
		};
		filterSimpleClonesAction.setText("Filter Clones");
		filterSimpleClonesAction.setToolTipText("FilterClones");
		filterSimpleClonesAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FILTERATION)));
		filterSimpleClonesAction.setEnabled(false);
		
		sideBysideViewAction = new Action() {
			public void run() {
				int sccId = ClonesReader.getSimpleClones().get(sccIndex).getSSCId();//getClusterID();
				SimpleClone scc = ClonesReader.getSimpleClones().get(sccIndex);//getMethodClones().get(sccIndex);
				instances = new ArrayList<SimpleCloneInstance>();
				instances = scc.getSCCInstances();
				//add the instances to the function
				new OpenSimpleCloneInspectionViewerAction(scc.getSCCInstances().get(0), scc.getSCCInstances().get(1), instances,0,1
						).run() ;
				
			}
		};
		sideBysideViewAction.setText("Analyze Clone");
		sideBysideViewAction.setToolTipText("Analyze Clone");
		sideBysideViewAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONE_COMPARISON)));
		sideBysideViewAction.setEnabled(false);
		
		
		
		unFilterSimpleClones = new Action() {
			public void run() {
				IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				   try
					{
					   SccView.filterFlag = false;
					   SccView.filtered = false;
					   IViewPart sccView = workbenchPage.findView("sccView");
					   if (sccView != null) {
							workbenchPage.hideView(sccView);
						}
						
					   
					    SccView.filterFlag = false;
					   	SccView.filtered = false;
					    IViewPart p = workbenchPage.showView("sccView");
					    p.setFocus();
					   // workbenchPage.
					    

					}
				   catch (Exception e)
					{
					    e.printStackTrace();
					}
			}
		};
		unFilterSimpleClones.setText("Filter Clones");
		unFilterSimpleClones.setToolTipText("FilterClones");
		unFilterSimpleClones.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_UNFILTERATION)));
		unFilterSimpleClones.setEnabled(false);
    	
    }
    private void contributeToActionBars()
    {
		IActionBars bars = getViewSite().getActionBars();
		// fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
    }

    private void fillLocalToolBar(IToolBarManager manager) 
    {
    	
    	manager.add(goToLocationAction);
    	manager.add(new Separator());
    	
    	manager.add(treeMapVisualizationAction);
    	manager.add(new Separator());

    	manager.add(filterSimpleClonesAction);
    	manager.add(new Separator());
    	
    	manager.add(unFilterSimpleClones);
    	manager.add(new Separator());
    	
    	manager.add(sideBysideViewAction);
    	manager.add(new Separator());

    	/*manager.add(filterClones);
    	manager.add(new Separator());*/

    }
    public void createSccTables(Composite parent)
    {
	primarySccViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createSccTableColumns(parent, primarySccViewer);
	
	if(filterFlag) {
		applyFilter();
		SccListContentProvider sccListContentProvider = new SccListContentProvider();
		sccListContentProvider.setList(primarySCCFilteredList.getList());
		primarySccViewer.setContentProvider(sccListContentProvider);
		primarySccViewer.setLabelProvider(new SccListLabelProvider());
		primarySccViewer.setInput(sccList.getList());
		primarySccViewer.refresh();
	}
	else {
		setSccInput();
		SccListContentProvider sccListContentProvider = new SccListContentProvider();
		sccListContentProvider.setList(sccList.getList());
		primarySccViewer.setContentProvider(sccListContentProvider);
		primarySccViewer.setLabelProvider(new SccListLabelProvider());
		primarySccViewer.setInput(sccList.getList());
		primarySccViewer.refresh();
	}
	

	secondarySccViewer = new TableViewer(parent, SWT.MULTI | SWT.FULL_SELECTION | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
	createSccCloneListTableColumns(parent, secondarySccViewer);
	//filterClones.setEnabled(true);

    }
    

    private void createSccTableColumns(Composite parent, TableViewer primarySccViewer)
    {
	String[] titles = {"#", "SCCID", "Length", "Benefit", "Number of Instance(s)" };
	int[] bounds = {50, 100, 150, 150, 150, 250 };
	sccListTable = primarySccViewer.getTable();
	GridData data = new GridData(GridData.FILL_BOTH); 
	
	
	sccListTable.setLayoutData(data);
	createSccTableColumn(titles[0], bounds[0], 0);
	createSccTableColumn(titles[1], bounds[1], 1);
	createSccTableColumn(titles[2], bounds[2], 2);
	createSccTableColumn(titles[3], bounds[3], 3);
	createSccTableColumn(titles[4], bounds[4], 4);
	
	sccListTable.getColumn(0).setToolTipText("Clone Number");
	sccListTable.getColumn(1).setToolTipText("Simple Clone Class ID");
	sccListTable.getColumn(2).setToolTipText("Total number of tokens");
	sccListTable.getColumn(3).setToolTipText("Benefit");
	sccListTable.getColumn(4).setToolTipText("Total number of Instances this clone class has");
	//org.eclipse.swt.graphics.Rectangle columnWidth = sccListTable.computeSize(wHint, hHint)
	
	int columnCount = sccListTable.getColumnCount();
	
	
	
    }

    private TableViewerColumn createSccTableColumn(String title, int bound, final int colNumber)
    {
	final TableViewerColumn viewerColumn = new TableViewerColumn(primarySccViewer, SWT.NONE);
	final TableColumn column = viewerColumn.getColumn();
	column.setText(title);
	
	column.setWidth(bound);
	column.setResizable(true);
	column.setMoveable(true);
	
	return viewerColumn;
    }

    private void createSccCloneListTableColumns(Composite parent, TableViewer secondartSccViewer)
    {
	String[] titles = { "ID", "Start Line", "End Line","Start Column","End Column","FID", " File Name " };
	int[] bounds = { 50, 50, 50, 50,50,50, 200 };
	sccInstanceListTable = secondarySccViewer.getTable();
	sccInstanceListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
	createSccCloneListTableColumn(titles[0], bounds[0], 0);
	createSccCloneListTableColumn(titles[1], bounds[1], 1);
	createSccCloneListTableColumn(titles[2], bounds[2], 2);
	createSccCloneListTableColumn(titles[3], bounds[3], 3);
	createSccCloneListTableColumn(titles[4], bounds[4], 4);
	createSccCloneListTableColumn(titles[5], bounds[5], 5);
	createSccCloneListTableColumn(titles[6], bounds[6], 6);

	
	
	
	sccInstanceListTable.getColumn(0).setToolTipText("Simple Clone Instance ID");
	sccInstanceListTable.getColumn(1).setToolTipText("Start line of this clone");
	sccInstanceListTable.getColumn(2).setToolTipText("End line of this clone");
	sccInstanceListTable.getColumn(3).setToolTipText("Start column of this clone");
	sccInstanceListTable.getColumn(4).setToolTipText("Start column of this clone");
	sccInstanceListTable.getColumn(5).setToolTipText("File ID");
	sccInstanceListTable.getColumn(6).setToolTipText("Name of the file that contains this clone");
	
	

    }

    private TableViewerColumn createSccCloneListTableColumn(String title, int bound, final int colNumber)
    {
	final TableViewerColumn viewerColumn = new TableViewerColumn(secondarySccViewer, SWT.NONE);
	final TableColumn column = viewerColumn.getColumn();
	column.setText(title);
	column.setWidth(bound);
	column.setResizable(true);
	column.setMoveable(true);
	return viewerColumn;
    }

    @Override
    public void setFocus()
    {
	primarySccViewer.getControl().setFocus();
	secondarySccViewer.getControl().setFocus();
    }

    public void setSccInput()
    {
	//if (SampleHandler.f != null)
	//{
	    populateSccList();
	    
	//}

	// sccList.addToList(new PrimarySccObject(1,1,1,1,1,cloneList));
	// sccList.addToList(new PrimarySccObject(2,2,2,2,2,cloneList));
	// sccList.addToList(new PrimarySccObject(3,3,3,3,3,cloneList));
    }
    
    

    public void setSccCloneListInput(int sscid)
    {

	cloneList = new SccCloneInstanceList();
	for (int i = 0; i < ClonesReader.getSimpleClones().size(); i++)
	{
	    if (sscid == ClonesReader.getSimpleClones().get(i).getSSCId())
	    {

		for (int j = 0; j < ClonesReader.getSimpleClones().get(i).getSCCInstances().size(); j++)
		{
		    // String filePath =
		    // handler.f.sscList.get(i).getSCCInstances().get(j).get.getFilePath();
		    // String[] parts = filePath.split("[\\\\ ]");

		    String filePath = ClonesReader.getSimpleClones().get(i).getSCCInstances().get(j).getFilePath();
		    String[] parts = filePath.split("[//// ]");
		    String fileName = parts[parts.length - 1];
		    // String fileName=
		    // SampleHandler.f.sscList.get(i).getSCCInstances().get(j).getFilePath();
		    // String
		    // fileName=handler.f.sscList.get(i).getSCCInstances().get(j).getFileName();
		    // String
		    // methodName=handler.f.mccList.get(i).getMCCInstances().get(j).getMethod().getMethodName();
		    int fId = ClonesReader.getSimpleClones().get(i).getSCCInstances().get(j).getFileId();
		    int startLine = ClonesReader.getSimpleClones().get(i).getSCCInstances().get(j).getStartingIndex();
		    int endLine = ClonesReader.getSimpleClones().get(i).getSCCInstances().get(j).getEndingIndex();
		    int startColoumn = ClonesReader.getSimpleClones().get(i).getSCCInstances().get(j).getStartCol();
		    int endColoumn = ClonesReader.getSimpleClones().get(i).getSCCInstances().get(j).getEndCol();
		    SecondarySccObject temp = new SecondarySccObject(j + 1, fId, fileName, startLine, endLine,
			    startColoumn, endColoumn);
		    cloneList.addToList(temp);

		}
		sccList.getList().get(i).setSccCloneList(cloneList);
		/*
		 * String[] Methds = new String[Method_Names.size()]; Methds =
		 * Method_Names.toArray(Methds); combo.setItems(Methds);
		 * combo.setText(Methds[0]); String[] parts = Methds[0].split("[\\s+]"); int MID
		 * = Integer.parseInt(parts[1]); Fill_Code(MID);
		 */
	    }
	}

	/*
	 * cloneList.addToList(new SecondarySccObject(1,1,"file1",10,25));
	 * cloneList.addToList(new SecondarySccObject(2,2,"file1",35,67));
	 * cloneList.addToList(new SecondarySccObject(3,3,"file1",70,95));
	 */
    }
   /* private void filterSccList(int minMembers, int maxMembers, int minLenght, int maxLength, String location, int locationIds, int minBenefit, int maxBenefit) {
    	
    	
    }*/
    public void setFilteredSccCloneListInput(int sscid)
    {

	secondarySCCFilteredList = new SccCloneInstanceList();
	for (int i = 0; i < ClonesReader.getFilteredSimpleClones().size(); i++)
	{
	    if (sscid == ClonesReader.getFilteredSimpleClones().get(i).getSSCId())
	    {

		for (int j = 0; j < ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().size(); j++)
		{
		    // String filePath =
		    // handler.f.sscList.get(i).getSCCInstances().get(j).get.getFilePath();
		    // String[] parts = filePath.split("[\\\\ ]");

		    String filePath = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().get(j).getFilePath();
		    String[] parts = filePath.split("[\\\\ ]");
		    String fileName = parts[parts.length - 1];
		    // String fileName=
		    // SampleHandler.f.sscList.get(i).getSCCInstances().get(j).getFilePath();
		    // String
		    // fileName=handler.f.sscList.get(i).getSCCInstances().get(j).getFileName();
		    // String
		    // methodName=handler.f.mccList.get(i).getMCCInstances().get(j).getMethod().getMethodName();
		    int fId = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().get(j).getFileId();
		    int startLine = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().get(j).getStartingIndex();
		    int endLine = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().get(j).getEndingIndex();
		    int startColoumn = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().get(j).getStartCol();
		    int endColoumn = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstances().get(j).getEndCol();
		    SecondarySccObject temp = new SecondarySccObject(j + 1, fId, fileName, startLine, endLine,
			    startColoumn, endColoumn);
		    secondarySCCFilteredList.addToList(temp);

		}
		primarySCCFilteredList.getList().get(i).setSccCloneList(secondarySCCFilteredList);
		/*
		 * String[] Methds = new String[Method_Names.size()]; Methds =
		 * Method_Names.toArray(Methds); combo.setItems(Methds);
		 * combo.setText(Methds[0]); String[] parts = Methds[0].split("[\\s+]"); int MID
		 * = Integer.parseInt(parts[1]); Fill_Code(MID);
		 */
	    }
	}

	/*
	 * cloneList.addToList(new SecondarySccObject(1,1,"file1",10,25));
	 * cloneList.addToList(new SecondarySccObject(2,2,"file1",35,67));
	 * cloneList.addToList(new SecondarySccObject(3,3,"file1",70,95));
	 */
    }
   /* private void filterSccList(int minMembers, int maxMembers, int minLenght, int maxLength, String location, int locationIds, int minBenefit, int maxBenefit) {
    	
    	
    }*/

    private void populateSccList()
    {
	// TODO Auto-generated method stub
	for (int i = 0; i <ClonesReader.getSimpleClones().size(); i++)
	{
	    int sccid = ClonesReader.getSimpleClones().get(i).getSSCId();
	    int length = ClonesReader.getSimpleClones().get(i).getTokenCount();
	    int benefit = ClonesReader.getSimpleClones().get(i).getBenefit();
	    int instanceCount = ClonesReader.getSimpleClones().get(i).getSCCInstanceCount();
	    // ArrayList <Integer> integerList=SampleHandler.f.sscList.get(i);
	    // MccCloneInstanceList cloneInstanceList=new MccCloneInstanceList();
	    PrimarySccObject temp = new PrimarySccObject(i, sccid, length, benefit, instanceCount);
	    sccList.addToList(temp);
	    setSccCloneListInput(sccid);
	    // Populate_Drop_Down(sccid);
	}

	// for (int i = 0; i < SampleHandler.f.mccList.size(); i++)
	{
	    // Populate_Drop_Down(SampleHandler.f.mccList.get(i).getMCCID()-1);
	}
	/*http://marketplace.eclipse.org/marketplace-client-intro?mpc_install=25744
	 * setMccCloneListInput(); mccList.getList().get(0).setCloneList(cloneList);
	 * mccList.getList().get(1).setCloneList(cloneList);
	 * mccList.getList().get(2).setCloneList(cloneList);
	 */
    }
    
    public void applyFilter() {
    	for (int i = 0; i <SimpleClonesReader.getFilteredSimpleClone().size(); i++)
    	{
    	    int sccid = ClonesReader.getFilteredSimpleClones().get(i).getSSCId();
    	    int length = ClonesReader.getFilteredSimpleClones().get(i).getTokenCount();
    	    int benefit = ClonesReader.getFilteredSimpleClones().get(i).getBenefit();
    	    int instanceCount = ClonesReader.getFilteredSimpleClones().get(i).getSCCInstanceCount();
    	    // ArrayList <Integer> integerList=SampleHandler.f.sscList.get(i);
    	    // MccCloneInstanceList cloneInstanceList=new MccCloneInstanceList();
    	    PrimarySccObject temp = new PrimarySccObject(i, sccid, length, benefit, instanceCount);
    	    primarySCCFilteredList.addToList(temp);
    	    setFilteredSccCloneListInput(sccid);
    	    // Populate_Drop_Down(sccid);
    	}
    	
    }

    private void printInPopup(String in)
    {
	MessageBox box = new MessageBox(getSite().getWorkbenchWindow().getShell(), SWT.ICON_INFORMATION);
	box.setMessage(in);
	box.open();
    }

    public static String getFileContent(File fileToOpen)
    {
	String text = "";
	try
	{
	    FileInputStream fis = new FileInputStream(fileToOpen);
	    byte[] data = new byte[(int) fileToOpen.length()];
	    fis.read(data);
	    fis.close();
	    text = new String(data, "UTF-8");
	} catch (IOException e2)
	{
	    e2.printStackTrace();
	}
	return text;
    }

    public static IFile getIFile(File fileToOpen)
    {
	IPath location = Path.fromOSString(fileToOpen.getAbsolutePath());
	IWorkspace workspace = ResourcesPlugin.getWorkspace();
	return workspace.getRoot().getFileForLocation(location);
    }
}
