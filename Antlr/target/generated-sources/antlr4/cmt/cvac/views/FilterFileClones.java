package cmt.cvac.views;

import java.util.regex.Pattern;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.SWTResourceManager;

import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.methodclones.MethodClonesReader;

public class FilterFileClones {


	protected Object result;
protected Shell shell;
private Text minMember;
private Text maxMember;
private Text minATC;
private Text maxATC;
private Text locationIDs;
private Text minAPC;
private Text maxAPC;
private Combo location;



/**
 * Open the dialog.
 * @return the result
 */
public Object open() {
	Display display = Display.getDefault();
	createContents();
	Monitor primary = display.getPrimaryMonitor();
    Rectangle bounds = primary.getBounds();
    Rectangle rect = shell.getBounds();
    int x = bounds.x + (bounds.width - rect.width) / 2;
    int y = bounds.y + (bounds.height - rect.height) / 2;
    
    shell.setLocation(x, y);
	shell.open();
	shell.layout();
	
	while (!shell.isDisposed()) {
		if (!display.readAndDispatch()) {
			display.sleep();
		}
	}
	return result;
}
public void close() {
	 shell.close();
}

/**
 * Create contents of the dialog.
 */
private void createContents() {
	shell = new Shell(SWT.CLOSE | SWT.TITLE | SWT.MIN);
	shell.setSize(392, 256);
	shell.setText("Filter File Clones");
	
	Button btnFind = new Button(shell, SWT.NONE);
	btnFind.setBounds(300, 197, 75, 25);
	btnFind.setText("Find");
	btnFind.addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			if(!(minMember.getText().isEmpty()) || !maxMember.getText().isEmpty() || !locationIDs.getText().isEmpty() || !location.getText().isEmpty()|| 
					!minATC.getText().isEmpty()|| !maxATC.getText().isEmpty()|| !minAPC.getText().isEmpty()|| !(maxAPC.getText().isEmpty())) {
				if(!locationIDs.getText().isEmpty()) {
					if(chkText(locationIDs.getText())) {
						FileClonesReader.filterFileClones(minAPC.getText(), maxAPC.getText(), minATC.getText(), maxATC.getText(), locationIDs.getText(), location.getText(), minMember.getText(), maxMember.getText()  );
					}
					else {
						/*btnFind.setEnabled(false);
						JOptionPane.showMessageDialog(null, "Please Enter Valid Id's. \n E.g. 12,3,5 etc", "Error", JOptionPane.ERROR_MESSAGE);
						showMessageDialog(frame,
							    "Eggs are not supposed to be green.",
							    "Inane error",
							    JOptionPane.ERROR_MESSAGE);*/
						Color pink = new Color(Display.getDefault(), new RGB(255,182,193));
						locationIDs.setBackground(pink);
						
						return;
					}
				}
				FileClonesReader.filterFileClones(minAPC.getText(), maxAPC.getText(), minATC.getText(), maxATC.getText(), locationIDs.getText(), location.getText(), minMember.getText(), maxMember.getText()  );
				
			}
			
			FccView.filterFlag = true;
			IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			   try
				{
				   IViewPart fccView = workbenchPage.findView("fccView");
				   if (fccView != null) {
						workbenchPage.hideView(fccView);
					}
				    FccView.filterFlag = true;
				    FccView.filtered = true;
				   
				    IViewPart p = workbenchPage.showView("fccView");
				    p.setFocus();
				   

				}
			   catch (Exception e)
				{
				    e.printStackTrace();
				}

			close();
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	Group grpLocation = new Group(shell, SWT.NONE);
	grpLocation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
	grpLocation.setText("Location");
	grpLocation.setBounds(197, 21, 178, 82);
	
	Label lblLocation = new Label(grpLocation, SWT.NONE);
	lblLocation.setBounds(10, 24, 65, 15);
	lblLocation.setText("Location");
	
	location = new Combo(grpLocation, SWT.NONE);
	location.setBounds(81, 21, 78, 23);
	location.setItems(new String[] {"Directory", "Group"});
	location.addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			if(!location.getText().equals("")) {
				locationIDs.setEnabled(true);
			}
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	
	Label lblLocationIds = new Label(grpLocation, SWT.NONE);
	lblLocationIds.setBounds(10, 58, 65, 15);
	lblLocationIds.setText("Location IDs");
	
	locationIDs = new Text(grpLocation, SWT.BORDER);
	locationIDs.setBounds(81, 50, 78, 21);
	locationIDs.setEnabled(false);
locationIDs.addListener(SWT.MouseDown, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
			 Color white = new Color(Display.getCurrent(), new RGB(255,255,255));
			 locationIDs.setBackground(white);
		       /* char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9' || chars[i] ==',')) {
		            e.doit = false;
		            return;
		          }
		        }
			 if(!chkText(string)) {
				 e.doit = false;
		            return;
			 }*/
			
		}
		
	});
	
	Group grpBenefit = new Group(shell, SWT.NONE);
	grpBenefit.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
	grpBenefit.setText("APC");
	grpBenefit.setBounds(197, 109, 178, 82);
	
	Label lblMinBenefit = new Label(grpBenefit, SWT.NONE);
	lblMinBenefit.setBounds(10, 23, 68, 15);
	lblMinBenefit.setText("Min APC");
	
	minAPC = new Text(grpBenefit, SWT.BORDER);
	minAPC.setBounds(84, 20, 76, 21);
	minAPC.addListener(SWT.Verify, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9')) {
		            e.doit = false;
		            return;
		          }
		        }
			
		}
	});
	
	Label lblMaxBenefit = new Label(grpBenefit, SWT.NONE);
	lblMaxBenefit.setBounds(10, 50, 68, 15);
	lblMaxBenefit.setText("Max APC");
	
	maxAPC = new Text(grpBenefit, SWT.BORDER);
	maxAPC.setBounds(84, 47, 76, 21);
	maxAPC.addListener(SWT.Verify, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9')) {
		            e.doit = false;
		            return;
		          }
		        }
			
		}
	});
	
	Group grpMember = new Group(shell, SWT.NONE);
	grpMember.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
	grpMember.setText("Member");
	grpMember.setBounds(10, 21, 181, 82);
	
	Label lblMinMember = new Label(grpMember, SWT.NONE);
	lblMinMember.setBounds(10, 21, 77, 15);
	lblMinMember.setText("Min Member");
	
	minMember = new Text(grpMember, SWT.BORDER);
	minMember.setBounds(93, 18, 78, 21);
	minMember.addListener(SWT.Verify, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9')) {
		            e.doit = false;
		            return;
		          }
		        }
			
		}
	});
	
	Label lblMaxMember = new Label(grpMember, SWT.NONE);
	lblMaxMember.setBounds(9, 45, 78, 15);
	lblMaxMember.setText("Max Member");
	
	maxMember = new Text(grpMember, SWT.BORDER);
	maxMember.setBounds(93, 42, 78, 21);
	maxMember.addListener(SWT.Verify, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9')) {
		            e.doit = false;
		            return;
		          }
		        }
			
		}
	});
	
	Group grpLength = new Group(shell, SWT.NONE);
	grpLength.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
	grpLength.setText("ATC");
	grpLength.setBounds(13, 109, 178, 82);
	
	Label lblMinLength = new Label(grpLength, SWT.NONE);
	lblMinLength.setBounds(10, 25, 78, 15);
	lblMinLength.setText("Min ATC");
	
	minATC = new Text(grpLength, SWT.BORDER);
	minATC.setBounds(94, 22, 76, 21);
	minATC.addListener(SWT.Verify, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9')) {
		            e.doit = false;
		            return;
		          }
		        }
			
		}
	});
	
	Label lblMaxLength = new Label(grpLength, SWT.NONE);
	lblMaxLength.setBounds(10, 46, 78, 15);
	lblMaxLength.setText("Max ATC");
	
	maxATC = new Text(grpLength, SWT.BORDER);
	maxATC.setBounds(94, 49, 76, 21);
	maxATC.addListener(SWT.Verify, new Listener() {
		
		@Override
		public void handleEvent(Event e) {
			// TODO Auto-generated method stub
			 String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9')) {
		            e.doit = false;
		            return;
		          }
		        }
			
		}
	});

}
public boolean chkText(String input) {
	String regex = "[0-9]+(,[0-9]+)*,?";
	Pattern pattern = Pattern.compile(regex);
	  boolean flag = pattern.matches(regex, input);
	  if(flag) {
		  return true;
	  }
	  else {
		  return false;
	  }
}
}