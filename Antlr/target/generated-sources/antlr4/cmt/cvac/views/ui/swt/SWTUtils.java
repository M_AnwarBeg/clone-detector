package cmt.cvac.views.ui.swt;

import java.awt.image.BufferedImage;
import java.awt.image.DirectColorModel;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.conqat.lib.commons.assertion.CCSMAssert;
import org.conqat.lib.commons.string.StringUtils;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

/**
 * Provides a bunch of commonly used SWT constructs.
 * 
 * @author $Author: heinemann $
 * @version $Rev: 50099 $
 * @ConQAT.Rating GREEN Hash: 47FDEA76E2F3576FFF2FD6B876F54118
 */
public class SWTUtils {

	/**
	 * Creates and returns a {@link Label} in the given parent with the given
	 * text.
	 */
	public static Label createLabel(Composite parent, String text) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(text);
		layout(label);
		return label;
	}

	/**
	 * Creates and returns a single line {@link Text} editor in the given parent
	 * initially with the given default text and with the given {@link Listener}
	 * set as {@link SWT#Modify} listener.
	 * <p>
	 * If the modify listener is <code>null</code> it is not added.
	 */
	public static Text createText(Composite parent, String defaultText,
			Listener modifyListener) {
		Text text = new Text(parent, SWT.BORDER);
		text.setText(defaultText);
		if (modifyListener != null) {
			text.addListener(SWT.Modify, modifyListener);
		}
		layout(text);
		return text;
	}

	/**
	 * Creates and returns a {@link Button} in the given parent with the given
	 * text, the given style and with the given selection listener. A default
	 * (grid) layout is applied to the button.
	 * <p>
	 * If the selection listener is <code>null</code> it is not added.
	 */
	public static Button createButton(Composite parent, String text, int style,
			SelectionListener selectionListener) {
		Button button = new Button(parent, style);
		button.setText(text);
		if (selectionListener != null) {
			button.addSelectionListener(selectionListener);
		}
		layout(button);
		return button;
	}

	/** Creates a {@link Group}. */
	public static Group createGroup(Composite parent, String text, Layout layout) {
		Group group = new Group(parent, SWT.NONE);
		group.setText(text);
		group.setLayout(layout);
		SWTUtils.layout(group);
		return group;
	}

	/** Layouts a control using default settings of grid layout. */
	public static void layout(Control control) {
		GridDataFactory.defaultsFor(control).applyTo(control);
	}

	/** Convert BufferedImage to SWT ImageData */
	public static ImageData convertToSWT(BufferedImage bufferedImage) {
		boolean typeCheck = bufferedImage.getColorModel() instanceof DirectColorModel;
		CCSMAssert.isTrue(typeCheck, "The buffered image must use "
				+ "DirectColorModel");

		DirectColorModel colorModel = (DirectColorModel) bufferedImage
				.getColorModel();
		PaletteData palette = new PaletteData(colorModel.getRedMask(),
				colorModel.getGreenMask(), colorModel.getBlueMask());
		ImageData data = new ImageData(bufferedImage.getWidth(),
				bufferedImage.getHeight(), colorModel.getPixelSize(), palette);
		WritableRaster raster = bufferedImage.getRaster();

		int[] pixelArray = new int[4];
		for (int y = 0; y < data.height; y++) {
			for (int x = 0; x < data.width; x++) {
				raster.getPixel(x, y, pixelArray);
				int pixel = palette.getPixel(new RGB(pixelArray[0],
						pixelArray[1], pixelArray[2]));
				data.setPixel(x, y, pixel);
			}
		}

		return data;
	}

	/** Shows the busy cursor on the active shell of the default display. */
	public static void showBusyCursor() {
		Shell shell = Display.getDefault().getActiveShell();
		if (shell == null) {
			return;
		}
		shell.setCursor(Display.getDefault().getSystemCursor(SWT.CURSOR_WAIT));
	}

	/**
	 * Shows the busy cursor on the active shell of the default display using a
	 * asynchronous thread
	 */
	public static void showBusyCursorAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				showBusyCursor();
			}
		});
	}

	/** Shows the default cursor on the active shell of the default display. */
	public static void showDefaultCursor() {
		Shell shell = Display.getDefault().getActiveShell();
		if (shell == null) {
			return;
		}
		shell.setCursor(null);
	}

	/**
	 * Shows the default cursor on the active shell of the default display using
	 * a asynchronous thread.
	 */
	public static void showDefaultCursorAsync() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				showDefaultCursor();
			}
		});
	}

	/** Creates an empty label as separator. */
	public static void createSeparator(Composite parent) {
		createLabel(parent, StringUtils.EMPTY_STRING);
	}

	/**
	 * Exports a table to a text-representation where each row is a line and
	 * columns are separated by the specified separator. The table content is
	 * written to the file exactly like it is shown on the screen, i.e. sorting
	 * and label formatting are respected. The first line contains the column
	 * headers.
	 * 
	 * This method does not do any error checking on the provided writer.
	 * 
	 * @throws IOException
	 *             if the writing caused IO problems
	 */
	public static void exportTable(Table table, Writer writer, String separator)
			throws IOException {
		TableColumn[] columns = table.getColumns();

		List<String> items = new ArrayList<String>();
		for (int i = 0; i < columns.length; i++) {
			items.add(columns[i].getText());
		}

		writeLine(writer, separator, items);

		for (TableItem item : table.getItems()) {
			for (int i = 0; i < columns.length; i++) {
				items.add(item.getText(i));
			}
			writeLine(writer, separator, items);
		}

	}

	/**
	 * Write a single line and clear the items list.
	 */
	private static void writeLine(Writer writer, String separator,
			List<String> items) throws IOException {
		writer.write(StringUtils.concat(items, separator));
		writer.write(StringUtils.CR);
		items.clear();
	}

	/** Returns the maximal amount of scrolling that is possible (in pixels). */
	public static int getTextWidthInPixels(StyledText text) {
		return text.getTextBounds(0, text.getCharCount() - 1).width;
	}

	/** Creates a basic menu item for the given menu. */
	public static MenuItem createMenuItem(Menu menu, String caption,
			SelectionListener handler) {
		MenuItem item = new MenuItem(menu, SWT.PUSH);
		item.setText(caption);
		item.addSelectionListener(handler);
		return item;
	}
}