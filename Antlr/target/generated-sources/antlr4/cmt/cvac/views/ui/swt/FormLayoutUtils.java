package cmt.cvac.views.ui.swt;

import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Control;

/**
 * Utility code for dealing with {@link FormLayout} and {@link FormData}.
 * 
 * @author $Author: kinnen $
 * @version $Rev: 48477 $
 * @ConQAT.Rating GREEN Hash: EFCF50756670C5BD15051693045B06CE
 */
public class FormLayoutUtils {

	/**
	 * Sets up the given controls <code>left</code> and <code>right</code>
	 * horizontally next to each other with a third separator Control in the
	 * middle.
	 * 
	 * @param separatorWidth
	 *            the width of the sash.
	 */
	public static void layoutHorizontallyWithSeparatorControl(Control left,Control separator, Control right, int separatorWidth) {
		left.setLayoutData(createFormData(0, 0, new FormAttachment(0, 0),
				new FormAttachment(separator), new FormAttachment(0, 0),
				new FormAttachment(100, 0)));
		

		FormData sashData = createFormData(separatorWidth, separatorWidth,
				new FormAttachment(50, 0), null, new FormAttachment(0, 0),
				new FormAttachment(100, 0));
		separator.setLayoutData(sashData);

		right.setLayoutData(createFormData(0, 0, new FormAttachment(separator),
				new FormAttachment(100, 0), new FormAttachment(0, 0),
				new FormAttachment(100, 0)));
	}

	/** Creates a new {@link FormData} object. */
	private static FormData createFormData(int width, int height,
			FormAttachment left, FormAttachment right, FormAttachment top,
			FormAttachment bottom) {
		FormData formData = new FormData(width, height);
		formData.left = left;
		formData.right = right;
		formData.top = top;
		formData.bottom = bottom;
		return formData;
	}
}

