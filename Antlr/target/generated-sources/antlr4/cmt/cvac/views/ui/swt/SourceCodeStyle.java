package cmt.cvac.views.ui.swt;


import java.awt.Font;
import org.conqat.lib.commons.collections.ImmutablePair;
import org.conqat.lib.commons.error.NeverThrownRuntimeException;
import org.conqat.lib.commons.factory.IParameterizedFactory;
import org.conqat.lib.commons.cache4j.BasicCache;
import org.conqat.lib.commons.cache4j.ICache;
import org.conqat.lib.commons.cache4j.backend.ICacheBackend;
import org.conqat.lib.commons.cache4j.backend.ECachingStrategy;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.graphics.Color;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ETokenType;
import eu.cqse.check.framework.scanner.IToken;

/**
 * Class that determines the style used when displaying source code. This is
 * just a wrapper for
 * {@link org.conqat.lib.scanner.highlighting.SourceCodeStyle} that converts
 * from AWT to SWT.
 * 
 * @author $Author: hummelb $
 * @version $Rev: 52663 $
 * @ConQAT.Rating YELLOW Hash: DE5B55D79C59E97ABF08FEE6A4B31F08
 */
public class SourceCodeStyle {

	/**
	 * Cache that converts from AWT to SWT colors. We never free the colors
	 * created.
	 */
	
	
	/*
	 * 	public BasicCache(String name, IParameterizedFactory<V, K, X> factory,
			ICacheBackend<K, V> backend) {*/
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ICache<java.awt.Color, Color, NeverThrownRuntimeException> colorCache = new BasicCache<java.awt.Color, Color, NeverThrownRuntimeException>(
			null,
			new IParameterizedFactory<Color, java.awt.Color, NeverThrownRuntimeException>() {
				@Override
				public Color create(java.awt.Color awtColor) {
					return new Color(null, awtColor.getRed(), awtColor
							.getGreen(), awtColor.getBlue());
				}
			}, (ICacheBackend) ECachingStrategy.UNLIMITED.getBackend(0));

	/** The wrapped style. */
	private final eu.cqse.check.framework.scanner.highlighting.SourceCodeStyle awtStyle;

	/** Constructor. */
	private SourceCodeStyle(
			eu.cqse.check.framework.scanner.highlighting.SourceCodeStyle awtStyle) {
		this.awtStyle = awtStyle;
	}

	/**
	 * Returns the color/font style pair for a given token type. The default
	 * implementation handles the base case of text.
	 */
	public ImmutablePair<Color, Integer> getStyle(ETokenType tokenType) {
		return convertStyle(awtStyle.getStyle(tokenType));
	}

	/** Returns the color/font style pair for a given token. */
	public ImmutablePair<Color, Integer> getStyle(IToken token) {
		return convertStyle(awtStyle.getStyle(token));
	}

	/** Converts AWT style information to SWT style info. */
	private ImmutablePair<Color, Integer> convertStyle(
			ImmutablePair<java.awt.Color, Integer> format) {

		int fontStyle = SWT.NONE;
		if ((format.getSecond() & Font.BOLD) != 0) {
			fontStyle |= SWT.BOLD;
		}
		if ((format.getSecond() & Font.ITALIC) != 0) {
			fontStyle |= SWT.ITALIC;
		}

		return new ImmutablePair<Color, Integer>(colorCache.obtain(format
				.getFirst()), fontStyle);
	}

	/** Fills a style range. */
	public StyleRange getStyleRange(ETokenType tokenType, int offset,
			int length, Color background) {
		ImmutablePair<Color, Integer> style = getStyle(tokenType);
		return new StyleRange(offset, length, style.getFirst(), background,
				style.getSecond());
	}

	/** Returns the style for a given language. */
	public static SourceCodeStyle get(ELanguage language) {
		return new SourceCodeStyle(
				eu.cqse.check.framework.scanner.highlighting.SourceCodeStyle
						.get(language));
	}
}
