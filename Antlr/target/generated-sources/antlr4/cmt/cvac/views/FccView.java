package cmt.cvac.views;


import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.dialogs.ViewComparator;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.texteditor.ITextEditor;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sMethod;
import cmt.cfc.fccframe.CliqueSelectionWizardFcc;
import cmt.cfc.fccframe.CliqueSelectionWizrdPage;
import cmt.cfc.fccframe.FCCAnayser;
import cmt.cfc.fccframe.FCCTemplate;
import cmt.cfc.fccframe.FcFrameRepo;
import cmt.cfc.mccframe.CliqueSelectionWizard;
import cmt.cfc.mccframe.CliqueSelectionWizardPage1;
import cmt.cfc.mccframe.MCCTemplate;
import cmt.cfc.mccframe.MCCsAnalyser;
import cmt.cfc.utility.FileHandler;
import cmt.common.Directories;
import cmt.common.ProjectImages;
import cmt.cvac.clonecomparsion.OpenFileCloneInspectionViewerAction;
import cmt.cvac.contentprovidor.FccInstanceListContentProvider;
import cmt.cvac.contentprovidor.FccListContentProvider;
import cmt.cvac.labelprovidor.FccCloneInstanceListLabelProvider;
import cmt.cvac.labelprovidor.FccListLabelProvider;
import cmt.cvac.treemapvisualization.TreeMapInit;
import cmt.cvac.viewobjects.FccList;
import cmt.cvac.viewobjects.PrimaryFccObject;
import cmt.cvac.viewobjects.SecondaryFccObject;
import cmt.cvac.viewobjects.SecondaryMccObject;

public class FccView extends ViewPart {

	 	public TableViewer primaryFccViewer;
	    private TableViewer secondaryFccViewer;
		TableCursor fccListTableCursor;
		TableCursor fccInstanceListTableCursor;
	    public static boolean filterFlag = false;
	    public static boolean filtered = false;
	    Table fccListTable;
	    Table fccInstanceListTable;
	    private int fccIndex;
	    private FccList list;
	    private FccList filteredFccList;
	    public Action unFilterFileClone;
	    public Action fileFrameAction;
	    public Action filterFileClones;
	    public Action sideBysideViewAction;
	    public Action visualizationAction;
	    public Action deletionAction;
	    public Action goToLocationAction;
	    public static ArrayList<String> fname = new ArrayList<String>();
	    public static ArrayList<Integer> mids = new ArrayList<Integer>();
	    public static ArrayList<FcFrameRepo> repo = new ArrayList<FcFrameRepo>();
	    public static int totalFrames=0;
	    int fccId;
	    int idOrder;
	    static int ASCENDING = 1;
	    static int DECENDING = 2;
	    ArrayList<FileCloneInstance> instances;
	    HashMap<Integer,ArrayList<SimpleCloneInstance>> fileToSimpleCloneMapping = new HashMap<Integer,ArrayList<SimpleCloneInstance>>();
	    
	    
	    public FccView()
	    {
	    	super();
			primaryFccViewer = null;
			secondaryFccViewer = null;
			fccListTable = null;
			fccInstanceListTable = null;
			list=null;
			idOrder = ASCENDING;
			filteredFccList = null;
			fccListTableCursor = null;
			fccInstanceListTableCursor = null;
			
			
			
	    }
	@Override
	public void createPartControl(final Composite parent) {
		
		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);
		createFccTables(parent);
		fccListTable.setHeaderVisible(true);
		fccListTable.setLinesVisible(true);
		fccInstanceListTable.setHeaderVisible(true);
		fccInstanceListTable.setLinesVisible(true);
		makeActions();
		contributeToActionBars();
		if(!filterFlag)
		
		if(list.getList().isEmpty())
		{
			filterFileClones.setEnabled(false);
		}
		else
		{
			filterFileClones.setEnabled(true);
		}
		if(!filtered)
		{
			unFilterFileClone.setEnabled(false);
		}
		else
		{
			unFilterFileClone.setEnabled(true);
		}
		

		fccListTableCursor = new TableCursor(fccListTable, SWT.NULL);
		fccInstanceListTableCursor = new TableCursor(fccInstanceListTable, SWT.NULL);
		
		primaryFccViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				

				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;

				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryFccViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					PrimaryFccObject object1 = (PrimaryFccObject) e1;
					PrimaryFccObject object2 = (PrimaryFccObject) e2;
					int result = 0;
					result = object1.getId() > (object2.getId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});

				primaryFccViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		primaryFccViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				

				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;

				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryFccViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
				    	PrimaryFccObject object1 = (PrimaryFccObject) e1;
				    	PrimaryFccObject object2 = (PrimaryFccObject) e2;
					int result = 0;
					result = object1.getFccId() > (object2.getFccId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});

				primaryFccViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		primaryFccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				

				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;

				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryFccViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					PrimaryFccObject object1 = (PrimaryFccObject) e1;
					PrimaryFccObject object2 = (PrimaryFccObject) e2;
					int result = 0;
					result = object1.getNumberOfInstance() > (object2.getNumberOfInstance()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});

				primaryFccViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		primaryFccViewer.getTable().getColumn(4).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				

				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;

				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryFccViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					PrimaryFccObject object1 = (PrimaryFccObject) e1;
					PrimaryFccObject object2 = (PrimaryFccObject) e2;
					int result = 0;
					result = object1.getAtc() > (object2.getAtc()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});

				primaryFccViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});

	primaryFccViewer.getTable().getColumn(5).addSelectionListener(new SelectionListener() {
	
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			primaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				PrimaryFccObject object1 = (PrimaryFccObject) e1;
				PrimaryFccObject object2 = (PrimaryFccObject) e2;
				int result = 0;
				result = object1.getApc() > (object2.getApc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			primaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFccViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFccObject object1 = (SecondaryFccObject) e1;
				SecondaryFccObject object2 = (SecondaryFccObject) e2;
				int result = 0;
				result = object1.getId() > (object2.getId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFccViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFccObject object1 = (SecondaryFccObject) e1;
				SecondaryFccObject object2 = (SecondaryFccObject) e2;
				int result = 0;
				result = object1.GId() > (object2.GId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFccViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFccObject object1 = (SecondaryFccObject) e1;
				SecondaryFccObject object2 = (SecondaryFccObject) e2;
				int result = 0;
				result = object1.getDId() > (object2.getDId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFccViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFccObject object1 = (SecondaryFccObject) e1;
				SecondaryFccObject object2 = (SecondaryFccObject) e2;
				int result = 0;
				result = object1.getFId() > (object2.getFId()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	secondaryFccViewer.getTable().getColumn(4).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFccObject object1 = (SecondaryFccObject) e1;
				SecondaryFccObject object2 = (SecondaryFccObject) e2;
				int result = 0;
				result = object1.getTc() > (object2.getTc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	secondaryFccViewer.getTable().getColumn(5).addSelectionListener(new SelectionListener() {
		
		@Override
		public void widgetSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
	
			if (idOrder == ASCENDING)
			{
			    idOrder = DECENDING;
	
			} else if (idOrder == DECENDING)
			{
			    idOrder = ASCENDING;
			}
			secondaryFccViewer.setComparator(new ViewComparator() {
			    @Override
			    public int compare(Viewer viewer, Object e1, Object e2)
			    {
				SecondaryFccObject object1 = (SecondaryFccObject) e1;
				SecondaryFccObject object2 = (SecondaryFccObject) e2;
				int result = 0;
				result = object1.getPc() > (object2.getPc()) ? -1 : 1;
				if (idOrder == ASCENDING)
				{
				    result = -result;
				    return result;
				}
				if (idOrder == DECENDING)
				{
				    return result;
				}
				return result;
			    };
			});
	
			secondaryFccViewer.refresh();
			
		}
		
		@Override
		public void widgetDefaultSelected(SelectionEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	});
		
		
		
		primaryFccViewer.getTable().addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseDown(MouseEvent e)
		    {
			  /*int row = Integer.parseInt((fccListTableCursor.getRow().getText()));
              fccIndex = row;*/
               
             //fileFrameAction.setEnabled(true);
			
			/*if (ClonesReader.getMethodClones().get(mccIndex).isFramed())
			{
			    editFrameWizardAction.setEnabled(true);
			    deleteFrameAction.setEnabled(true);
			    cliqueSelectionWizardAction.setEnabled(false);
			} else
			{
			    editFrameWizardAction.setEnabled(false);
			    deleteFrameAction.setEnabled(false);
			    cliqueSelectionWizardAction.setEnabled(true);
			}*/
			// int column=(mccListTableCursor.getColumn());
			// if(column==1)
			// {
		    	int row=-1;
		    	
			if(filterFlag) {
				row = Integer.parseInt((fccListTableCursor.getRow().getText()));
				fccIndex = row;
				
				if(CloneRepository.getDBName().equals(LoadList.framingBaseline) && validForFraming()==true)
				{
					fileFrameAction.setEnabled(true);
				}
				else
				{
					fileFrameAction.setEnabled(false);
				}
				sideBysideViewAction.setEnabled(true);
				FccInstanceListContentProvider fccCloneInstanceListContentProvider = new FccInstanceListContentProvider();
				fccCloneInstanceListContentProvider.setList(filteredFccList.getList().get(row).getCloneList().getList());
				secondaryFccViewer.setContentProvider(fccCloneInstanceListContentProvider);
				secondaryFccViewer.setLabelProvider(new FccCloneInstanceListLabelProvider());
				secondaryFccViewer.setInput(filteredFccList.getList().get(row).getCloneList().getList());
				
				secondaryFccViewer.refresh();
				
			}
			else {
				row = Integer.parseInt((fccListTableCursor.getRow().getText()));
				fccIndex = row;
				
				if(CloneRepository.getDBName().equals(LoadList.framingBaseline) && validForFraming()==true)
				{
					fileFrameAction.setEnabled(true);
				}
				else
				{
					fileFrameAction.setEnabled(false);
				}
				sideBysideViewAction.setEnabled(true);
				FccInstanceListContentProvider fccCloneInstanceListContentProvider = new FccInstanceListContentProvider();
				fccCloneInstanceListContentProvider.setList(list.getList().get(row).getCloneList().getList());
				secondaryFccViewer.setContentProvider(fccCloneInstanceListContentProvider);
				secondaryFccViewer.setLabelProvider(new FccCloneInstanceListLabelProvider());
				secondaryFccViewer.setInput(list.getList().get(row).getCloneList().getList());
				
				secondaryFccViewer.refresh();
				
			}
			
			deletionAction.setEnabled(true);
			fname.clear();
			mids.clear();
			try
			{
				Statement stmt = CloneRepository.getConnection().createStatement();
				stmt.execute("use "+CloneRepository.getDBName());
				ResultSet rs = stmt.executeQuery("select * from scc_method;");
				ArrayList<Integer> sccId=new ArrayList<>();
				ArrayList<Integer> sccId_mid=new ArrayList<>();
				
				while(rs.next())
				{
					sccId.add(rs.getInt(1));
					sccId_mid.add(rs.getInt(2));
				}
				
				
				for(int i=0;i<list.getList().get(row).getCloneList().getList().size();i++)
				{
					fname.add(list.getList().get(row).getCloneList().getList().get(i).getFileName());
					
					for(int k=0;k<list.getList().get(row).getStructure().size();k++)
					{
						for(int b=0;b<sccId.size();b++)
						{
							if(list.getList().get(row).getStructure().get(k)==sccId.get(b))
							{
								mids.add(sccId_mid.get(b));
							}
						}
					}
				}
				visualizationAction.setEnabled(true);
		    	goToLocationAction.setEnabled(false);

			}
			catch(Exception f)
			{
				f.printStackTrace();
			}
			
			// }
		    }
		});
		
		secondaryFccViewer.getTable().addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseDown(MouseEvent e)
		    {
		    	goToLocationAction.setEnabled(true);		    	
		    	
		    }
		});
	}

	private void printInPopup(String in)
    {
	MessageBox box = new MessageBox(getSite().getWorkbenchWindow().getShell(), SWT.ICON_INFORMATION);
	box.setMessage(in);
	box.open();
    }

	
	
	
	
	
	
	
	@Override
	public void setFocus() {
		
		primaryFccViewer.getControl().setFocus();
		secondaryFccViewer.getControl().setFocus();
	}
	
	public void createFccTables(Composite parent)
    {
	primaryFccViewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createFccTableColumns(parent, primaryFccViewer);
	if(filterFlag) {
		applyFilters();
		FccListContentProvider fccListContentProvider = new FccListContentProvider();
		fccListContentProvider.setList(filteredFccList.getList());
		primaryFccViewer.setContentProvider(fccListContentProvider);
		primaryFccViewer.setLabelProvider(new FccListLabelProvider());
		primaryFccViewer.setInput(filteredFccList.getList());
		primaryFccViewer.refresh();
		
		
	}
	else {
		populateFccList();
		FccListContentProvider fccListContentProvider = new FccListContentProvider();
		fccListContentProvider.setList(list.getList());
		primaryFccViewer.setContentProvider(fccListContentProvider);
		primaryFccViewer.setLabelProvider(new FccListLabelProvider());
		primaryFccViewer.setInput(list.getList());
		
		primaryFccViewer.refresh();
		
	}
	// setMccInput();
	
	secondaryFccViewer = new TableViewer(parent,
		SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createFccCloneListTableColumns(parent, secondaryFccViewer);
	//markExistingMcc();
    }

	
	
	 private void contributeToActionBars()
	 {
		IActionBars bars = getViewSite().getActionBars();
		// fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	 }
	 private void createFccTableColumns(Composite parent, TableViewer primaryFccViewer)
	 {
		String[] titles = {"#", "FCC-ID", "Structure", "Number of Instance(s)", "ATC ", "APC" };
		int[] bounds = {50, 50, 150, 50, 50, 50 };
		fccListTable = this.primaryFccViewer.getTable();
		fccListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		createFccTableColumn(titles[0], bounds[0], 0);
		createFccTableColumn(titles[1], bounds[1], 1);
		createFccTableColumn(titles[2], bounds[2], 2);
		createFccTableColumn(titles[3], bounds[3], 3);
		createFccTableColumn(titles[4], bounds[4], 4);
		createFccTableColumn(titles[5], bounds[5], 5);
		fccListTable.getColumn(0).setToolTipText("File Clone ID");
		fccListTable.getColumn(1).setToolTipText("File Clone Class ID");
		fccListTable.getColumn(2).setToolTipText("List of Simple-Clone-Class-ID's of which this file clone is composed of");
		fccListTable.getColumn(3).setToolTipText("Total number of instances of this clone");
		fccListTable.getColumn(4).setToolTipText("Average Tokken Count");
		fccListTable.getColumn(5).setToolTipText("Average Percentage Count");
		//createFccTableColumn(titles[5], bounds[5], 5);
	 } 
	 
	 private TableViewerColumn createFccTableColumn(String title, int bound, final int colNumber)
	 {
		final TableViewerColumn viewerColumn = new TableViewerColumn(primaryFccViewer, SWT.MULTI | SWT.FULL_SELECTION);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	 }

	 private void createFccCloneListTableColumns(Composite parent, TableViewer secondaryFccViewer)
	 {
		String[] titles = { "ID", "GID", "DID", "FID", "TC ", "PC", "File Name" };
		int[] bounds = { 50, 50, 50, 50, 50, 50, 150 };
		fccInstanceListTable = this.secondaryFccViewer.getTable();
		fccInstanceListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		createFccCloneListTableColumn(titles[0], bounds[0], 0);
		createFccCloneListTableColumn(titles[1], bounds[1], 1);
		createFccCloneListTableColumn(titles[2], bounds[2], 2);
		createFccCloneListTableColumn(titles[3], bounds[3], 3);
		createFccCloneListTableColumn(titles[4], bounds[4], 4);
		createFccCloneListTableColumn(titles[5], bounds[5], 5);
		createFccCloneListTableColumn(titles[6], bounds[6], 6);
		fccInstanceListTable.getColumn(0).setToolTipText("File Clone Instance ID");
		fccInstanceListTable.getColumn(1).setToolTipText("Group ID.");
		fccInstanceListTable.getColumn(2).setToolTipText("Directroy ID.");
		fccInstanceListTable.getColumn(3).setToolTipText("File ID.");
		fccInstanceListTable.getColumn(4).setToolTipText("Tokken Count");
		fccInstanceListTable.getColumn(5).setToolTipText("Percentage Count");
		fccInstanceListTable.getColumn(6).setToolTipText("Name of file that contains this clone");
		//createFccCloneListTableColumn(titles[6], bounds[6], 6);

	  }
	 
	 private TableViewerColumn createFccCloneListTableColumn(String title, int bound, final int colNumber)
	 {
		final TableViewerColumn viewerColumn = new TableViewerColumn(secondaryFccViewer, SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	  }
	 
	private void fillLocalToolBar(IToolBarManager manager)
	{
		manager.add(goToLocationAction);
    	manager.add(new Separator());
		
		manager.add(new Separator());
		manager.add(sideBysideViewAction);
		
		manager.add(new Separator());
		manager.add(fileFrameAction);
		
		manager.add(new Separator());
		manager.add(deletionAction);
		
		manager.add(new Separator());
		manager.add(filterFileClones);
		
		manager.add(new Separator());
		manager.add(unFilterFileClone);
		
		manager.add(new Separator());
		manager.add(visualizationAction);
	}
	
	public void makeActions(){
		
		deletionAction = new Action() {
			
			public void run()
			{
				int result = deleteFCC(fccIndex);
				if(result == 1) {
					JOptionPane.showMessageDialog(null, "Selected Clone Class has been deleted.", "Error" , JOptionPane.INFORMATION_MESSAGE);
				}
				else if(result == 1) {
					JOptionPane.showMessageDialog(null, "Selected Clone Class has not framed yet.", "Error" , JOptionPane.INFORMATION_MESSAGE);
				}
				deletionAction.setEnabled(false);
			}
		};
		
		deletionAction.setText("Delete Frame");
		deletionAction.setToolTipText("Delete Frame");
		deletionAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.DELETE_FRAME)));
		deletionAction.setEnabled(false);
		
		visualizationAction= new Action() {
    		@Override
    	    public void run()
    	    {
    			TreeMapInit.init(fname,-1,mids);
    			visualizationAction.setEnabled(false);
    	    }
    	};
    	visualizationAction.setText("Run TreeMap Visualization");
    	visualizationAction.setToolTipText("Run TreeMap Visualization");
    	visualizationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_TREEMAPVISUALIZATION)));
    	visualizationAction.setEnabled(false);
    	
		fileFrameAction = new Action() {
		    @Override
		    public void run()
		    {
		    int fccId = ClonesReader.getFileCloneList().get(fccIndex).getClusterID();
		    
		    FcFrameRepo newFc=new FcFrameRepo();
			FileCluster fcc = ClonesReader.getFileCloneList().get(fccIndex);
			try {
				FCCAnayser.analyze(fcc);
			}
			catch(Exception e)
			{
				e.printStackTrace();
				
			}
		    
			try
			{
			    /*MCCsAnalyser.modifymcc(ClonesReader.getMethodClones().get(fccIndex), CliqueSelectionWizardPage1.SccsSelected);
			    for (MethodCloneInstance instance :ClonesReader.getMethodClones().get(fccIndex).getMCCInstances())
			    {
				instance.DisplayAnalysisDetails();
			    }*/
			   // System.out.println(ClonesReader.getFileClones().getList().get(fccIndex).getClusterID());
				newFc.setFccId(ClonesReader.getFileCloneList().get(fccId).getClusterID());
				repo.add(newFc);
			    
				FCCTemplate.genFCCTemplate(ClonesReader.getFileCloneList().get(fccId).getClusterID());
			    
				// open SPC file in the editor (Saud)
				
			    FileHandler.fileHandler=new FileHandler("FCC_SPC");
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
				for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
					if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(fccId+"_FCC_SPC"))
					{
				    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
				 
					    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
					    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					    IDE.openEditorOnFileStore(page,fileStore);
					}
			    }
				
				// open template file in the editor (Saud)
				
			    FileHandler.fileHandler=new FileHandler("FCC_Template");
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
				for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
					if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(fccId+"_FCC_template"))
					{
				    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
				 
					    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
					    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					    IDE.openEditorOnFileStore(page,fileStore);
					}
			    }
				
				// open SCC Frames in the editor (Saud)
			    
			    ArrayList<Integer> scc = new ArrayList<>();
			    Statement st = CloneRepository.getConnection().createStatement();
			    st.execute("use "+CloneRepository.getDBName()+";");
			    ResultSet rs = st.executeQuery("select scc_id from fcc_scc where fcc_id = "+ fccId);
			    
			    while (rs.next())
			    {
			    	scc.add(rs.getInt(1));
			    }
			    FileHandler.fileHandler=new FileHandler("SCC_Frames");
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
			    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
			    	for(int j=0; j<scc.size(); j++)
					{
			    		if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(scc.get(j)+"_SCC_Frame"))
					    	{
						    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
						    	boolean test  =  file.isFile();
						 
							    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
							    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
							    IDE.openEditorOnFileStore(page,fileStore);
					    	}
					}
			    }
				
			    // open the file in the editor
			    //FileHandler.fileHandler=new FileHandler(fccIndex);
			    //FileHandler.fileHandler.getARTFilesList();
			    //FileHandler.fileHandler.getARTFilesPaths();
			    //for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    //{
			    	//File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
			    	//boolean test  =  file.isFile();
			 
				//    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
				//    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				//    IDE.openEditorOnFileStore(page,fileStore);
			    //}
				    
			} 
			catch (Exception e)
			{
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    
		    }
		    
		    
		    
		    
		
		};
		
		
		ImageDescriptor desc = ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FRAMING));
		
		
		fileFrameAction.setText("Frame");
		fileFrameAction.setToolTipText("Frame File Clone Instance");
		fileFrameAction.setImageDescriptor(desc);
		fileFrameAction.setEnabled(false);
		/**/
		
		filterFileClones = new Action() {
			public void run() {
				Shell shell01 = new Shell();
				FilterFileClones filter = new FilterFileClones();
				
				filter.open();
			}
			
		};
		filterFileClones.setEnabled(false);
		
		filterFileClones.setToolTipText("Filter File Clones");
		filterFileClones.setText("Filter File Clones");
		filterFileClones.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FILTERATION)));
		
		unFilterFileClone = new Action() {
			public void run() {
				IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				   try
					{
					   	
					    FccView.filterFlag = false;
					    FccView.filtered = false;
					    IViewPart fccView = workbenchPage.findView("fccView");
					    if(fccView != null) {
					    	workbenchPage.hideView(fccView);
					    }
					    IViewPart p = workbenchPage.showView("fccView");
					    p.setFocus();
					   

					}
				   catch (Exception e)
					{
					    e.printStackTrace();
					}
				
			}
				
		};
		//unFilterFileClone.setEnabled(false);
		unFilterFileClone.setToolTipText("Unfilter File Clones");
		unFilterFileClone.setText("Unfilter File Clones");
		unFilterFileClone.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_UNFILTERATION)));

		sideBysideViewAction=new Action() {
    		
    		public void run()
		    {
    			fileToSimpleCloneMapping.clear();
    			
    			int check=0;
			//int mccId = ClonesReader.getMethodClones().get(fccIndex).getClusterID();
			int fccId = ClonesReader.getFileCloneList().get(fccIndex).getClusterID();
			FccList fcc = ClonesReader.getFileClones();//.get(fccIndex);
			
			instances = new ArrayList<FileCloneInstance>();
			//instances = fcc.getList().get(fccIndex).getCloneList().getList();//getFCCInstance();
			//new OpenFileCloneInspectionViewerAction(instances.get(0), instances.get(1), instances, 0, 1);
			PrimaryFccObject pFCCObj = fcc.getList().get(fccIndex);
			ArrayList<SimpleClone> requiredSCC = new ArrayList<SimpleClone>();
			ArrayList<Integer> fccStructure = fcc.getList().get(fccIndex).getStructure();
			
			for(SimpleClone scc: ClonesReader.getSimpleClones()) {
				if(fccStructure.contains(scc.getSSCId())) {
					requiredSCC.add(scc);
				}
			}
			for(int i=0;i < pFCCObj.getCloneList().getList().size(); i++) {
				int fileId = pFCCObj.getCloneList().getList().get(i).getFId();
				for(SimpleClone scc : requiredSCC) {
					for(SimpleCloneInstance sccInstance : scc.getInstances()) {
						if(sccInstance.getFileId() == fileId) {
							if(fileToSimpleCloneMapping.get(fileId) == null) {
								ArrayList<SimpleCloneInstance> requiredsccInstances = new ArrayList<SimpleCloneInstance>();
								requiredsccInstances.add(sccInstance);
								fileToSimpleCloneMapping.put(fileId, requiredsccInstances);
							}
							else {
								fileToSimpleCloneMapping.get(fileId).add(sccInstance);
							}
						}
					}
				}
			}
			
			new OpenFileCloneInspectionViewerAction(fileToSimpleCloneMapping, requiredSCC).run();
			
			
			
			
		
		    }
		};
		sideBysideViewAction.setText("Analyze Clone");
		sideBysideViewAction.setToolTipText("Analyze Clone");
		sideBysideViewAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONE_COMPARISON)));
		sideBysideViewAction.setEnabled(true);
		
		
		
		goToLocationAction = new Action() {
	    	public void run() {
		    	try
		    	{
					int row = Integer.parseInt((fccInstanceListTableCursor.getRow().getText()));
					fccInstanceListTableCursor.getColumn();
					// if(column==0){
					FileCloneInstance fccInstance = ClonesReader.getFileCloneList().get(fccIndex).getFCCInstance().get(row);//ClonesReader.getSimpleClones().get(fccIndex).getSCCInstances().get(--row);
					File fileToOpen = new File(fccInstance.getSCCs().get(0).getFilePath());
					String content = SccView.getFileContent(fileToOpen);
					if (fileToOpen.exists() && fileToOpen.isFile())
						{
						    IFileStore fileStore = EFS.getLocalFileSystem().getStore(fileToOpen.toURI());
						    IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						    IDE.openEditor(page, SccView.getIFile(fileToOpen));
						}
					else
						{
						    printInPopup(fileToOpen.getName() + " doesn't Exist!");
						}
		    	}
		    	catch(Exception i)
		    	{
		    		i.printStackTrace();
		    	}
		    }
	    	};
			
	    	goToLocationAction.setText("Go to File Location");
	    	goToLocationAction.setToolTipText("Go to File Location");
	    	goToLocationAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.GO_TO_FILE_LOCATION)));
	    	goToLocationAction.setEnabled(false);
		
		
	}
	
	public boolean validForFraming()
	{
		boolean flag=false;
		IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
		for(int i=0; i< editorlist.length;i++) {
		if(editorlist[i].getTitle().equals("File Clone Editor")) {
			flag=true;
		break;
		}
		}
		return flag;
	}
	
	
	
	
	
	
	
	
	
	public static int deleteFCC(int fccID)
	{
		for(int t=0;t<repo.size();t++)
		{
			if(repo.get(t).getFccId() == ClonesReader.getFileCloneList().get(fccID).getClusterID())
			{
				try 
				{
					String SPC_PATH = "FCC_" + repo.get(t).getFccId() + "\\" + repo.get(t).getFccId() + "_FCC_SPC.art";
				    String templatePath="FCC_" + repo.get(t).getFccId() + "\\" + repo.get(t).getFccId() + "_FCC_template.art";
					/*Files.deleteIfExists(Paths.get(Directories.getAbsolutePath("ART_Output/FCC_Template/"+SPC_PATH)));
					Files.deleteIfExists(Paths.get(Directories.getAbsolutePath("ART_Output/FCC_Template/"+templatePath)));*/
				    
				    int frameid;
				    Statement stmt = CloneRepository.conn.createStatement();
				    stmt.execute("use framerepository;");
				    ResultSet rs=stmt.executeQuery("select fid from fccframes where fccid = "+repo.get(t).getFccId());
				    rs.next();
				    frameid=rs.getInt(1);
				    stmt.execute("delete from fccframes where fccid = "+repo.get(t).getFccId());
				    stmt.execute("delete from frames where fid = "+frameid);
				    stmt.execute("delete from frames where fid = "+(frameid+1));
				    
				    File f=new File(Directories.getAbsolutePath("ART_Output\\FCC_Template\\"+SPC_PATH));
				    f.delete();
				    
				    f=new File(Directories.getAbsolutePath("ART_Output\\FCC_Template\\"+templatePath));
				    f.delete();
					
					for(int y=0;y<repo.get(t).getSccs().size();y++)
					{
						ResultSet rs2=stmt.executeQuery("select fid from sccframes where sccid = "+repo.get(t).getSccs().get(y));
						rs2.next();
						frameid=rs2.getInt(1);
						
						ResultSet rs3= stmt.executeQuery("select links from frames where fid = "+frameid);
						rs3.next();
						int links = rs3.getInt(1);
						
						if(links>1)
						{
							stmt.execute("update frames set links="+(links-1)+"where fid="+frameid);
						}
						else
						{
							stmt.execute("delete from frames where fid = "+frameid);
							String ART_PATH = "SCC_Frames\\" + repo.get(t).getSccs().get(y) + "_SCC_Frame.art";
							f=new File(Directories.getAbsolutePath("ART_Output\\"+ART_PATH));
							f.delete();
						}
					}
				} 
				catch (Exception e) 
				{
					e.printStackTrace();
				}
				
				return 1;
			}
			else if(t==repo.size()-1)
			{
				return 2;
			}
		}
		if(repo.size()==0)
		{
			return 2;
		}
		return 0;
	}
	
	 private void populateFccList()
	 {    
		list=ClonesReader.getFileClones();
		 
		/*	private int id;
			private int fccId;
			private ArrayList <Integer> structure;
			private int numberOfInstance;
			private double atc;
			private double apc;
			private FccCloneInstanceList cloneList;
			
		
		 
		 
		 for (int i = 0; i < ClonesReader.getFileCloneList().size(); i++)
		 {
			 
			 
			int fccId =ClonesReader.getFileCloneList().get(i).getClusterID();
			int instanceCount =ClonesReader.getFileCloneList().get(i).getNumberOfInstance();
			ArrayList<Integer> structure = ClonesReader.getFileCloneList().get(i).getvCloneClasses();
			double  atc = ClonesReader.getFileCloneList().get(i).getAverageTokenCount();
			double  ptc = ClonesReader.getFileCloneList().get(i).getAveragePercentageCount();
			
			
			ArrayList<Integer> integerList = ClonesReader.getMethodClones().get(i).getvCloneClasses();
			PrimaryMccObject temp = new PrimaryMccObject(i, mccid, integerList, inctancecount, 0, 0);
			mccList.addToList(temp);
			populate(mccid);
		 }
		 
		 
		 
		 
		 for (int i = 0; i < ClonesReader.getMethodClones().size(); i++)
		 {
					int mccid =ClonesReader.getMethodClones().get(i).getClusterID();
					int inctancecount =ClonesReader.getMethodClones().get(i).getMCCInstanceCount();
					ArrayList<Integer> integerList = ClonesReader.getMethodClones().get(i).getvCloneClasses();
					PrimaryMccObject temp = new PrimaryMccObject(i, mccid, integerList, inctancecount, 0, 0);
					mccList.addToList(temp);
					populate(mccid);
				    }
			//	}
			 
		 
		 
		 
		 
		
		 *  */
		 
	 }
	 private void applyFilters() {
		 filteredFccList=ClonesReader.getFilteredFileClones();
	 }
	

}
