package cmt.cvac.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.SWTResourceManager;

import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.methodclones.MethodClonesReader;

public class FilterFCS {

	
	protected Object result;
	protected Shell shell;
	private Text minMembers;
	private Text maxMembers;
	private Text locationIds;
	private Text mccId;
	
	public void close() {
		 shell.close();
	}
	
	private void createContents() {
		shell = new Shell(SWT.CLOSE | SWT.TITLE);
		shell.setSize(277, 306);
		shell.setText("Filter Method Clone Structure");
		
		Group grpMember = new Group(shell, SWT.NONE);
		grpMember.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpMember.setText("Member");
		grpMember.setBounds(10, 10, 252, 84);
		
		Label lblMinMembers = new Label(grpMember, SWT.NONE);
		lblMinMembers.setBounds(10, 20, 92, 15);
		lblMinMembers.setText("Min Members");
		
		Label lblMaxMembers = new Label(grpMember, SWT.NONE);
		lblMaxMembers.setText("Max Members");
		lblMaxMembers.setBounds(10, 50, 92, 15);
		
		minMembers = new Text(grpMember, SWT.BORDER);
		minMembers.setBounds(108, 17, 62, 21);
		minMembers.addListener(SWT.Verify, new Listener() {
			
			@Override
			public void handleEvent(Event e) {
				// TODO Auto-generated method stub
				 String string = e.text;
			        char[] chars = new char[string.length()];
			        string.getChars(0, chars.length, chars, 0);
			        for (int i = 0; i < chars.length; i++) {
			          if (!('0' <= chars[i] && chars[i] <= '9')) {
			            e.doit = false;
			            return;
			          }
			        }
				
			}
		});
		
		maxMembers = new Text(grpMember, SWT.BORDER);
		maxMembers.setBounds(108, 47, 62, 21);
		maxMembers.addListener(SWT.Verify, new Listener() {
			
			@Override
			public void handleEvent(Event e) {
				// TODO Auto-generated method stub
				 String string = e.text;
			        char[] chars = new char[string.length()];
			        string.getChars(0, chars.length, chars, 0);
			        for (int i = 0; i < chars.length; i++) {
			          if (!('0' <= chars[i] && chars[i] <= '9')) {
			            e.doit = false;
			            return;
			          }
			        }
				
			}
		});
		
		Group grpLocation = new Group(shell, SWT.NONE);
		grpLocation.setText("Location");
		grpLocation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpLocation.setBounds(10, 101, 252, 84);
		
		Label lblLocation = new Label(grpLocation, SWT.NONE);
		lblLocation.setText("Location");
		lblLocation.setBounds(10, 20, 92, 15);
		
		Label label_1 = new Label(grpLocation, SWT.NONE);
		label_1.setText("Location ID");
		label_1.setBounds(10, 50, 92, 15);
		
		locationIds = new Text(grpLocation, SWT.BORDER);
		locationIds.setBounds(108, 47, 131, 21);
		locationIds.addListener(SWT.Verify, new Listener() {
			
			@Override
			public void handleEvent(Event e) {
				// TODO Auto-generated method stub
				String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9'|| chars[i] == ',')) {
		            e.doit = false;
		            return;
		          }
		        }
				
			}
		});
		
		Combo combo = new Combo(grpLocation, SWT.NONE);
		combo.setItems(new String[] {"Group", "Directory", "File"});
		combo.setBounds(108, 17, 131, 21);
		/*combo.*/
		
		Group grpStructure = new Group(shell, SWT.NONE);
		grpStructure.setText("Structure");
		grpStructure.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpStructure.setBounds(10, 191, 252, 51);
		
		Label label = new Label(grpStructure, SWT.NONE);
		label.setText("MCC ID");
		label.setBounds(10, 20, 92, 15);
		
		mccId = new Text(grpStructure, SWT.BORDER);
		mccId.setBounds(108, 17, 131, 21);
		mccId.addListener(SWT.Verify, new Listener() {
			
			@Override
			public void handleEvent(Event e) {
				// TODO Auto-generated method stub
				String string = e.text;
		        char[] chars = new char[string.length()];
		        string.getChars(0, chars.length, chars, 0);
		        for (int i = 0; i < chars.length; i++) {
		          if (!('0' <= chars[i] && chars[i] <= '9'|| chars[i] == ',')) {
		            e.doit = false;
		            return;
		          }
		        }
				
			}
		});
		
		Button btnFilter = new Button(shell, SWT.NONE);
		btnFilter.setBounds(187, 248, 75, 25);
		btnFilter.setText("Find");
		btnFilter.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				if(!(minMembers.getText().equals(null))|| !(maxMembers.getText().equals(null))|| !(String.valueOf(combo.getSelectionIndex()).equals(null))|| 
						!(locationIds.getText().equals(null))|| !(mccId.getText().equals(null))) {
					FileClonesReader.filterFCS(minMembers.getText(), maxMembers.getText(), String.valueOf(combo.getSelectionIndex()), locationIds.getText(), mccId.getText());
					
				}
				
				
				FcsViewCrossDirs.filterFlag = true;
				FcsViewCrossDirs.filtered = true;
				IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				   try
					{
					   
					   
					   IViewPart mcsView = workbenchPage.findView("fcsView");
					   if (mcsView != null) {
							workbenchPage.hideView(mcsView);
						}
						
					   
					    FcsViewCrossDirs.filterFlag = true;
					    FcsViewCrossDirs.filtered = true;
					    IViewPart p = workbenchPage.showView("fcsView");
					    p.setFocus();
					    

					}
				   catch (Exception e)
					{
					    e.printStackTrace();
					}

				close();
			
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});

	}
	public Object open() {
		Display display = Display.getDefault();
		createContents();
		createContents();
		Monitor primary = display.getPrimaryMonitor();
	    Rectangle bounds = primary.getBounds();
	    Rectangle rect = shell.getBounds();
	    int x = bounds.x + (bounds.width - rect.width) / 2;
	    int y = bounds.y + (bounds.height - rect.height) / 2;
	    
	    shell.setLocation(x, y);
		
		shell.open();
		shell.layout();
		
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

}
