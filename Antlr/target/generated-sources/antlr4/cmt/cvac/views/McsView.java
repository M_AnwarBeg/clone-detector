package cmt.cvac.views;


import java.io.File;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.internal.dialogs.ViewComparator;
import org.eclipse.ui.part.ViewPart;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cfc.mccframe.MCSTemplate;
import cmt.cfc.utility.FileHandler;
import cmt.common.Directories;
import cmt.common.ProjectImages;
import cmt.cvac.clonecomparsion.OpenMCSInspectionViewerAction;
import cmt.cvac.contentprovidor.PrimaryMcsObjectContentProvider;
import cmt.cvac.contentprovidor.SecondaryMcsObjectContentProvider;
import cmt.cvac.labelprovidor.PrimaryMcsObjectLabelProvider;
import cmt.cvac.labelprovidor.SecondaryMcsObjectLabelProvider;
import cmt.cvac.viewobjects.McsList;
import cmt.cvac.viewobjects.PrimaryFccObject;
import cmt.cvac.viewobjects.PrimaryMcsObject;
import cmt.cvac.viewobjects.SecondaryMcsObject;

public class McsView extends ViewPart {

	 	public TableViewer primaryMcsViewer;
	    private TableViewer secondaryMcsViewer;
	    Table mcsListTable;
	    Table mcsInstanceListTable;
	    private int mcsIndex;
	   /* private ArrayList<PrimaryMcsObject> list;
	    private ArrayList<PrimaryMcsObject> filteredList;*/
	    private McsList list;
	    private McsList filteredList;
	    
	    public Action fileFrameAction;
	    public Action filterMethodCloneStructure;
	    public Action unfilterMethodCloneStructure;
	    public Action deletionAction;
	    public Action sideBysideViewAction;
	    int fccId;
	    public static boolean filterFlag = false;
		public static boolean filtered = false;
		private ArrayList<MethodCloneStructure> instances;
		
		 int idOrder;
		    static int ASCENDING = 1;
		    static int DECENDING = 2;
	    
	    
	    public McsView()
	    {
	    	super();
			primaryMcsViewer = null;
			secondaryMcsViewer = null;
			mcsListTable = null;
			mcsInstanceListTable = null;
			list= new McsList();
			idOrder = ASCENDING;
			filteredList = new McsList();
	    }
	@Override
	
	
	
	
	
	
	public void createPartControl(final Composite parent) {
		
		GridLayout layout = new GridLayout(2, false);
		parent.setLayout(layout);
		createFccTables(parent);
		mcsListTable.setHeaderVisible(true);
		mcsListTable.setLinesVisible(true);
		mcsInstanceListTable.setHeaderVisible(true);
		mcsInstanceListTable.setLinesVisible(true);
		makeActions();
		contributeToActionBars();
		
		if(list.getList().isEmpty())
		{
			filterMethodCloneStructure.setEnabled(false);
			
		}
		else
		{
			filterMethodCloneStructure.setEnabled(true);
		}
		if(filtered) {
			unfilterMethodCloneStructure.setEnabled(true);
		}
		else {
			unfilterMethodCloneStructure.setEnabled(false);
		}

		final TableCursor fccListTableCursor = new TableCursor(mcsListTable, SWT.NULL);
		final TableCursor fccInstanceListTableCursor = new TableCursor(mcsInstanceListTable, SWT.NULL);
		
		primaryMcsViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					PrimaryMcsObject object1 = (PrimaryMcsObject) e1;
					PrimaryMcsObject object2 = (PrimaryMcsObject) e2;
					int result = 0;
					result = object1.getId() > (object2.getId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		primaryMcsViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					PrimaryMcsObject object1 = (PrimaryMcsObject) e1;
					PrimaryMcsObject object2 = (PrimaryMcsObject) e2;
					int result = 0;
					result = object1.getMcsId() > (object2.getMcsId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		primaryMcsViewer.getTable().getColumn(3).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				primaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					PrimaryMcsObject object1 = (PrimaryMcsObject) e1;
					PrimaryMcsObject object2 = (PrimaryMcsObject) e2;
					int result = 0;
					result = object1.getNumberOfInstance() > (object2.getNumberOfInstance()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		secondaryMcsViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				secondaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					SecondaryMcsObject object1 = (SecondaryMcsObject) e1;
					SecondaryMcsObject object2 = (SecondaryMcsObject) e2;
					int result = 0;
					result = object1.getId() > (object2.getId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		secondaryMcsViewer.getTable().getColumn(1).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				secondaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					SecondaryMcsObject object1 = (SecondaryMcsObject) e1;
					SecondaryMcsObject object2 = (SecondaryMcsObject) e2;
					int result = 0;
					result = object1.GId() > (object2.GId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		secondaryMcsViewer.getTable().getColumn(2).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				secondaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					SecondaryMcsObject object1 = (SecondaryMcsObject) e1;
					SecondaryMcsObject object2 = (SecondaryMcsObject) e2;
					int result = 0;
					result = object1.getDId() > (object2.getDId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		secondaryMcsViewer.getTable().getColumn(0).addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
		
				if (idOrder == ASCENDING)
				{
				    idOrder = DECENDING;
		
				} else if (idOrder == DECENDING)
				{
				    idOrder = ASCENDING;
				}
				secondaryMcsViewer.setComparator(new ViewComparator() {
				    @Override
				    public int compare(Viewer viewer, Object e1, Object e2)
				    {
					SecondaryMcsObject object1 = (SecondaryMcsObject) e1;
					SecondaryMcsObject object2 = (SecondaryMcsObject) e2;
					int result = 0;
					result = object1.getFId() > (object2.getFId()) ? -1 : 1;
					if (idOrder == ASCENDING)
					{
					    result = -result;
					    return result;
					}
					if (idOrder == DECENDING)
					{
					    return result;
					}
					return result;
				    };
				});
		
				primaryMcsViewer.refresh();
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		primaryMcsViewer.getTable().addMouseListener(new MouseAdapter() {
		    @Override
		    public void mouseDown(MouseEvent e)
		    {
		    	if(filterFlag) {
		    		int row = Integer.parseInt((fccListTableCursor.getRow().getText()));
					mcsIndex = row;
					sideBysideViewAction.setEnabled(true);
					if(CloneRepository.getDBName().equals(LoadList.framingBaseline) && validForFraming()==true) // change it to LoadLIst.framingBaseline after creation of gui for baseline
					{
						fileFrameAction.setEnabled(true);
						//sideBysideViewAction.setEnabled(false);
					}
					else
					{
						fileFrameAction.setEnabled(false);
						//sideBysideViewAction.setEnabled(true);
					}
					/*if (ClonesReader.getMethodClones().get(mccIndex).isFramed())
					{
					    editFrameWizardAction.setEnabled(true);
					    deleteFrameAction.setEnabled(true);
					    cliqueSelectionWizardAction.setEnabled(false);
					} else
					{
					    editFrameWizardAction.setEnabled(false);
					    deleteFrameAction.setEnabled(false);
					    cliqueSelectionWizardAction.setEnabled(true);
					}*/
					// int column=(mccListTableCursor.getColumn());
					// if(column==1)
					// {
					SecondaryMcsObjectContentProvider secondaryMcsObjectContentProvider = new SecondaryMcsObjectContentProvider();
					secondaryMcsObjectContentProvider.setList(filteredList.getList().get(row).getCloneList());
					secondaryMcsViewer.setContentProvider(secondaryMcsObjectContentProvider);
					secondaryMcsViewer.setLabelProvider(new SecondaryMcsObjectLabelProvider());
					secondaryMcsViewer.setInput(filteredList.getList().get(row).getCloneList());
					secondaryMcsViewer.refresh();
		    		
		    	}
		    	else {
		    		int row = Integer.parseInt((fccListTableCursor.getRow().getText()));
					mcsIndex = row;
					sideBysideViewAction.setEnabled(true);
					if(CloneRepository.getDBName().equals(LoadList.framingBaseline) && validForFraming()==true) // change it to LoadLIst.framingBaseline after creation of gui for baseline
					{
						fileFrameAction.setEnabled(true);
						//sideBysideViewAction.setEnabled(false);
					}
					else
					{
						fileFrameAction.setEnabled(false);
						//sideBysideViewAction.setEnabled(true);
					}
					/*if (ClonesReader.getMethodClones().get(mccIndex).isFramed())
					{
					    editFrameWizardAction.setEnabled(true);
					    deleteFrameAction.setEnabled(true);
					    cliqueSelectionWizardAction.setEnabled(false);
					} else
					{
					    editFrameWizardAction.setEnabled(false);
					    deleteFrameAction.setEnabled(false);
					    cliqueSelectionWizardAction.setEnabled(true);
					}*/
					// int column=(mccListTableCursor.getColumn());
					// if(column==1)
					// {
					SecondaryMcsObjectContentProvider secondaryMcsObjectContentProvider = new SecondaryMcsObjectContentProvider();
					secondaryMcsObjectContentProvider.setList(list.getList().get(row).getCloneList());
					secondaryMcsViewer.setContentProvider(secondaryMcsObjectContentProvider);
					secondaryMcsViewer.setLabelProvider(new SecondaryMcsObjectLabelProvider());
					secondaryMcsViewer.setInput(list.getList().get(row).getCloneList());
					secondaryMcsViewer.refresh();
		    		
		    	}
		    	
		    	deletionAction.setEnabled(true);
			
			
			// }
		    }
		});
	}

	@Override
	public void setFocus() {
		
		primaryMcsViewer.getControl().setFocus();
		secondaryMcsViewer.getControl().setFocus();
	}
	
	public void createFccTables(Composite parent)
    {
	primaryMcsViewer = new TableViewer(parent,
		SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
	createFccTableColumns(parent, primaryMcsViewer);
	if(filterFlag) {
		populateFilteredMCSList();
		PrimaryMcsObjectContentProvider primaryMcsObjectContentProvider = new PrimaryMcsObjectContentProvider();
		primaryMcsObjectContentProvider.setList(filteredList.getList());
		primaryMcsViewer.setContentProvider(primaryMcsObjectContentProvider);
		primaryMcsViewer.setLabelProvider(new PrimaryMcsObjectLabelProvider());
		primaryMcsViewer.setInput(filteredList.getList());
		primaryMcsViewer.refresh();
		secondaryMcsViewer = new TableViewer(parent,
			SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		createFccCloneListTableColumns(parent, secondaryMcsViewer);
		
	}
	else {
		populateMcsList();
		PrimaryMcsObjectContentProvider primaryMcsObjectContentProvider = new PrimaryMcsObjectContentProvider();
		primaryMcsObjectContentProvider.setList(list.getList());
		primaryMcsViewer.setContentProvider(primaryMcsObjectContentProvider);
		primaryMcsViewer.setLabelProvider(new PrimaryMcsObjectLabelProvider());
		primaryMcsViewer.setInput(list.getList());
		primaryMcsViewer.refresh();
		secondaryMcsViewer = new TableViewer(parent,
			SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER | SWT.FULL_SELECTION);
		createFccCloneListTableColumns(parent, secondaryMcsViewer);
		
	}
	// setMccInput();
	
	//markExistingMcc();
    }

	
	
	 private void contributeToActionBars()
	 {
		IActionBars bars = getViewSite().getActionBars();
		// fillLocalPullDown(bars.getMenuManager());
		fillLocalToolBar(bars.getToolBarManager());
	 }
	 private void createFccTableColumns(Composite parent, TableViewer primaryFccViewer)
	 {
		String[] titles = { "# ","MCS-ID", "Structure", "Number of Instance(s)" };
		int[] bounds = { 40,60, 150, 150};
		mcsListTable = this.primaryMcsViewer.getTable();
		mcsListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		createFccTableColumn(titles[0], bounds[0], 0);
		createFccTableColumn(titles[1], bounds[1], 1);
		createFccTableColumn(titles[2], bounds[2], 2);
		createFccTableColumn(titles[3], bounds[3], 3);
		mcsListTable.getColumn(0).setToolTipText("ID");
		mcsListTable.getColumn(1).setToolTipText("Method Clone Structure ID's");
		mcsListTable.getColumn(2).setToolTipText("List of Simple-Clone-ID's of which this clone is composed of");
		mcsListTable.getColumn(3).setToolTipText("Total number of instances of this clone");
	 } 
	 
	 private TableViewerColumn createFccTableColumn(String title, int bound, final int colNumber)
	 {
		final TableViewerColumn viewerColumn = new TableViewerColumn(primaryMcsViewer, SWT.MULTI | SWT.FULL_SELECTION);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	 }

	 private void createFccCloneListTableColumns(Composite parent, TableViewer secondaryFccViewer)
	 {
		String[] titles = { "ID", "GID", "DID", "FID", "File Name" };
		int[] bounds = { 50, 50, 50, 50, 150 };
		mcsInstanceListTable = this.secondaryMcsViewer.getTable();
		mcsInstanceListTable.setLayoutData(new GridData(GridData.FILL_BOTH));
		createFccCloneListTableColumn(titles[0], bounds[0], 0);
		createFccCloneListTableColumn(titles[1], bounds[1], 1);
		createFccCloneListTableColumn(titles[2], bounds[2], 2);
		createFccCloneListTableColumn(titles[3], bounds[3], 3);
		createFccCloneListTableColumn(titles[4], bounds[4], 4);
		mcsInstanceListTable.getColumn(0).setToolTipText("Method Clone Structure Instance ID");
		mcsInstanceListTable.getColumn(1).setToolTipText("Group ID");
		mcsInstanceListTable.getColumn(2).setToolTipText("Directory ID");
		mcsInstanceListTable.getColumn(3).setToolTipText("File ID");
		mcsInstanceListTable.getColumn(4).setToolTipText("Name of file that contains this clone");
		

	  }
	 
	 private TableViewerColumn createFccCloneListTableColumn(String title, int bound, final int colNumber)
	 {
		final TableViewerColumn viewerColumn = new TableViewerColumn(secondaryMcsViewer, SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	  }
	 
	private void fillLocalToolBar(IToolBarManager manager)
	{
		manager.add(fileFrameAction);
		manager.add(new Separator());
	 	manager.add(filterMethodCloneStructure);
	 	manager.add(new Separator());
	 	manager.add(unfilterMethodCloneStructure);
	 	manager.add(new Separator());
	 	manager.add(deletionAction);
	 	manager.add(new Separator());
	 	manager.add(sideBysideViewAction);
	}
	
	public void makeActions(){
		boolean inputMapping = false;
		HashMap<Integer,ArrayList<MethodCloneInstance>> fileToMethodMapping = new HashMap<Integer,ArrayList<MethodCloneInstance>>();
		
		
		sideBysideViewAction = new Action() {
			public void run() {
				fileToMethodMapping.clear();
				PrimaryMcsObject pMcsObj = MethodClonesReader.getMcsList().get(mcsIndex);
				ArrayList<Integer> mccStructure = pMcsObj.getStructure();
				ArrayList<MethodClones> requiredMCCs =  new ArrayList<MethodClones>();
				for(MethodClones mcc : ClonesReader.getMethodClones()) {
						if(mccStructure.contains(mcc.getClusterID())){
								requiredMCCs.add(mcc);
						}
					}
				for(int i = 0; i < pMcsObj.getCloneList().size(); i++) {
					int fileId = pMcsObj.getCloneList().get(i).getFId();
					// ArrayList<MethodClones> methodClones =  ClonesReader.getMethodClones();
					for(MethodClones mcc : requiredMCCs) {
						
						for(MethodCloneInstance mccInstance : mcc.getMCCInstances()) {
						
							if(mccInstance.getFID() == fileId) {
								if(fileToMethodMapping.get(fileId) == null) {
									ArrayList<MethodCloneInstance> requiredMCCInstance = new ArrayList<MethodCloneInstance>();
									requiredMCCInstance.add(mccInstance);
									fileToMethodMapping.put(fileId, requiredMCCInstance);
									
								}
								else 
									fileToMethodMapping.get(fileId).add(mccInstance);
							}
						}
					}
				}
				
				
				
				System.out.println("Testing");
				
				
				new OpenMCSInspectionViewerAction(fileToMethodMapping, requiredMCCs).run() ;
				
				

				
			}
			

//			public void run() {
//				int mcsId = ClonesReader.getMethodCloneStructure(mcsId).getMcsId();//MethodClones().get(mccIndex).getClusterID();
//				MethodCloneStructure mcs = ClonesReader.getMethodCloneStructure(mcsIndex);
//				instances = new ArrayList<MethodCloneStructureInstance>();
//				instances = mcs.getMCCInstances();
//				//add the instances to the function
//				new OpenCloneInspectionViewerAction(mcs.getMCCInstances().get(0), mcs.getMCCInstances().get(1), instances,0,1
//						).run() ;
//				
//				
//			}
			
		};
		sideBysideViewAction.setText("Analyze Clone");
		sideBysideViewAction.setToolTipText("Analyze Clone");
		sideBysideViewAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONE_COMPARISON)));
		sideBysideViewAction.setEnabled(true);
		
		deletionAction = new Action() {
			
			public void run()
			{
				int result = deleteMCS(mcsIndex);
				if(result == 1) {
					JOptionPane.showMessageDialog(null, "Selected Clone Class has been deleted.", "Error" , JOptionPane.INFORMATION_MESSAGE);
				}
				else if(result == 2) {
					JOptionPane.showMessageDialog(null, "Selected Clone Class has not framed yet.", "Error" , JOptionPane.INFORMATION_MESSAGE);
				}
				deletionAction.setEnabled(false);
				
				
			}
		};
		
		deletionAction.setText("Delete Frame");
		deletionAction.setToolTipText("Delete Frame");
		deletionAction.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.DELETE_FRAME)));
		deletionAction.setEnabled(false);
		
		fileFrameAction = new Action() {
		    @Override
		    public void run()
		    {
		    //int fccId = ClonesReader.getFileCloneList().get(mcsIndex).getClusterID();
		    //int mcsId=MethodClonesReader.getMcsList().get(mcsIndex);
			//FileCluster fcc = ClonesReader.getFileCloneList().get(mcsIndex);
			try {
				MCSTemplate.generateTemplate(mcsIndex);
				
				// open SPC file in editor (Saud)
				FileHandler.fileHandler=new FileHandler("MCS_SPC");
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
				for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
					if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(mcsIndex+"_MCS_SPC"))
					{
				    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
				 
					    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
					    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					    IDE.openEditorOnFileStore(page,fileStore);
					}
			    }
				
				 // open MCC-SPC files that are in this MCS file in the editor (Saud)
			    
			    ArrayList<Integer> scc = new ArrayList<>();
			    ArrayList<Integer> mcc = new ArrayList<>();
			    Statement st = CloneRepository.getConnection().createStatement();
			    st.execute("use "+CloneRepository.getDBName()+";");
			    ResultSet rs = st.executeQuery("select mcc_id from mcscrossfile_mcc where mcs_crossfile_id = "+ mcsIndex);
			    
			    while (rs.next())
			    {
			    	mcc.add(rs.getInt(1));
			    }
			    
			    for(int t=0; t<mcc.size(); t++)
			    {
			    	ResultSet rs2 = st.executeQuery("select scc_id from mcc_scc where mcc_id = "+ mcc.get(t));
			    	
			    	while(rs2.next())
			    	{
			    		scc.add(rs2.getInt(1));
			    	}
			    	
			    	rs2.close();
			    }
			    
			    FileHandler.fileHandler=new FileHandler("MCC_SPC");
			    
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
			    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
			    	for(int k=0; k<mcc.size(); k++)
			    	{
				    	if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(mcc.get(k)+"_MCC_SPC"))
				    	{
					    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
					    	boolean test  =  file.isFile();
					 
						    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
						    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						    IDE.openEditorOnFileStore(page,fileStore);
						    break;
				    	}
			    	}
			    }
			    
			    // open MCC-template files that are in this MCS in the editor (Saud)
			    FileHandler.fileHandler=new FileHandler("MCC_Template");
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
			    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
			    	for(int k=0; k<mcc.size(); k++)
			    	{
				    	if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(mcc.get(k)+"_MCC_template"))
				    	{
					    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
					    	boolean test  =  file.isFile();
					 
						    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
						    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
						    IDE.openEditorOnFileStore(page,fileStore);
						    break;
				    	}
			    	}
			    }
			    
			    // open SCC Frames in the editor (Saud)
			    
			    FileHandler.fileHandler=new FileHandler("SCC_Frames");
			    FileHandler.fileHandler.getARTFilesList();
			    FileHandler.fileHandler.getARTFilesPaths();
			    for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    {
			    	for(int j=0; j<scc.size(); j++)
					{
			    		if(FileHandler.fileHandler.listOfARTFiles.get(i).contains(scc.get(j)+"_SCC_Frame"))
					    	{
						    	File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
						    	boolean test  =  file.isFile();
						 
							    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
							    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
							    IDE.openEditorOnFileStore(page,fileStore);
					    	}
					}
			    }
				
				//FCCAnayser.analyze(fcc);
			    /*MCCsAnalyser.modifymcc(ClonesReader.getMethodClones().get(fccIndex), CliqueSelectionWizardPage1.SccsSelected);
			    for (MethodCloneInstance instance :ClonesReader.getMethodClones().get(fccIndex).getMCCInstances())
			    {
				instance.DisplayAnalysisDetails();
			    }*/
			   // System.out.println(ClonesReader.getFileClones().getList().get(fccIndex).getClusterID());
			    //FCCTemplate.genFCCTemplate(ClonesReader.getFileCloneList().get(fccId).getClusterID());
			    // open the file in the editor
			    //FileHandler.fileHandler=new FileHandler(fccIndex);
			    //FileHandler.fileHandler.getARTFilesList();
			    //FileHandler.fileHandler.getARTFilesPaths();
			    //for(int i=0;i<FileHandler.fileHandler.listOfARTFiles.size();i++)
			    //{
			    	//File file = new File(FileHandler.fileHandler.artFilesPaths.get(i));
			    	//boolean test  =  file.isFile();
			 
				//    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
				//    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				//    IDE.openEditorOnFileStore(page,fileStore);
			    //}
			}
			catch (Exception e)
			{
			    // TODO Auto-generated catch block
			    e.printStackTrace();
			}
		    
		    }
		
		};
		
		
		ImageDescriptor desc = ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FRAMING));
		
		
		fileFrameAction.setText("Frame");
		fileFrameAction.setToolTipText("Frame File Clone Instance");
		fileFrameAction.setImageDescriptor(desc);
		fileFrameAction.setEnabled(false);
		filterMethodCloneStructure = new Action() {
			public void run() {
				Shell shell01 = new Shell();
				//list.clear();
				FilterMCS filter = new FilterMCS();
				
				filter.open();
			}
		};
		filterMethodCloneStructure.setEnabled(false);
		filterMethodCloneStructure.setToolTipText("Filter Method Clones Structure");
		filterMethodCloneStructure.setText("Filter Method Clones Structure");
		filterMethodCloneStructure.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_FILTERATION)));

	 	unfilterMethodCloneStructure = new Action() {
	 		public void run() {
	 			filterFlag = false;
	 			filtered = false;
	 			filteredList.getList().clear();
	 			 try {
	 		         //    workbench.showPerspective(perspectiveID, workbench.getActiveWorkbenchWindow());
	 		         	 IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	 		         	  
	 		         		   	
	 						   IViewPart mcsView = workbenchPage.findView("mcsView");
	 						   if (mcsView != null) {
	 								workbenchPage.hideView(mcsView);
	 							}
	 							
	 						   
	 						    McsView.filterFlag = false;
	 						   	McsView.filtered = false;
	 						    IViewPart p = workbenchPage.showView("mcsView");
	 						    p.setFocus();
	 		         		   
	 		         	}
	 		         	   catch (Exception e)
	 		         		{
	 		         		    e.printStackTrace();
	 		         		} 
	 		}
	 	};
	 	unfilterMethodCloneStructure.setEnabled(false);
		unfilterMethodCloneStructure.setToolTipText("Unfilter Method Clone Structure");
		unfilterMethodCloneStructure.setText("Unfilter Method Clone Structure");
		unfilterMethodCloneStructure.setImageDescriptor(ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.CLONES_UNFILTERATION)));
		
		
	}
	
	private void populateFilteredMCSList() {
		for(int i =0;i<MethodClonesReader.getFilteredMcsList().size();i++) {
			filteredList.addToList(MethodClonesReader.getFilteredMcsList().get(i));
			
		}/*
		filteredList.s = MethodClonesReader.getFilteredMcsList();*/
	}
	private void populateMcsList()
	 {
		if(!(MethodClonesReader.getMcsList()==(null))) {
			for(int i =0;i<MethodClonesReader.getMcsList().size();i++) {
				list.addToList(MethodClonesReader.getMcsList().get(i));
				
			}
			
		}
		/*
		list=MethodClonesReader.getMcsList();*/
		 
		/*	private int id;
			private int fccId;
			private ArrayList <Integer> structure;
			private int numberOfInstance;
			private double atc;
			private double apc;
			private FccCloneInstanceList cloneList;
			
		
		 
		 
		 for (int i = 0; i < ClonesReader.getFileCloneList().size(); i++)
		 {
			 
			 
			int fccId =ClonesReader.getFileCloneList().get(i).getClusterID();
			int instanceCount =ClonesReader.getFileCloneList().get(i).getNumberOfInstance();
			ArrayList<Integer> structure = ClonesReader.getFileCloneList().get(i).getvCloneClasses();
			double  atc = ClonesReader.getFileCloneList().get(i).getAverageTokenCount();
			double  ptc = ClonesReader.getFileCloneList().get(i).getAveragePercentageCount();
			
			
			ArrayList<Integer> integerList = ClonesReader.getMethodClones().get(i).getvCloneClasses();
			PrimaryMccObject temp = new PrimaryMccObject(i, mccid, integerList, inctancecount, 0, 0);
			mccList.addToList(temp);
			populate(mccid);
		 }
		 
		 
		 
		 
		 for (int i = 0; i < ClonesReader.getMethodClones().size(); i++)
		 {
					int mccid =ClonesReader.getMethodClones().get(i).getClusterID();
					int inctancecount =ClonesReader.getMethodClones().get(i).getMCCInstanceCount();
					ArrayList<Integer> integerList = ClonesReader.getMethodClones().get(i).getvCloneClasses();
					PrimaryMccObject temp = new PrimaryMccObject(i, mccid, integerList, inctancecount, 0, 0);
					mccList.addToList(temp);
					populate(mccid);
				    }
			//	}
			 
		 
		 
		 
		 
		
		 *  */
		 
	 }
	public boolean validForFraming()
	{
		boolean flag=false;
		IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
		for(int i=0; i< editorlist.length;i++) {
		if(editorlist[i].getTitle().equals("Method Clone Structure Editor")) {
			flag=true;
		break;
		}
		}
		return flag;
	}
	public static int deleteMCS(int mcsID) {
	   		
	   		try 
					{
						String SPC_PATH = mcsID + "_MCS_SPC.art";
					    
					    int frameid;
					    File f;
					    
					    Statement stmt = CloneRepository.getConnection().createStatement();
					    stmt.execute("use framerepository;");
					    ResultSet rs=stmt.executeQuery("select fid from mcsframes where mcsid = "+mcsID);
					    
					    if(rs.next()) 
					    {
					    	frameid=rs.getInt(1);
					    	stmt.execute("delete from mcsframes where mcsid = "+mcsID);
						    stmt.execute("delete from frames where fid = "+frameid);
						    
						    f=new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsSPC+SPC_PATH));
						    f.delete();
						    
						    for(int i=0; i<MethodClonesReader.getMcsList().size(); i++)
						    {
						    	if(MethodClonesReader.getMcsList().get(i).getMcsId() == mcsID)
						    	{
						    		for(int j=0; j<MethodClonesReader.getMcsList().get(i).getStructure().size(); j++)
						    		{
						    			MccView.deleteMCC(MethodClonesReader.getMcsList().get(i).getStructure().get(j));
						    		}
						    	}
						    }
						    return 1;
					    }
					    else
					    {
					    	return 2;
					    }
						
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
			return 0;
	   	}
	

}
