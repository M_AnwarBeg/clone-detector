package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class FccList {

	private ArrayList<PrimaryFccObject> list;

    public FccList()
    {
	list = new ArrayList<PrimaryFccObject>();

    }

    public FccList(ArrayList<PrimaryFccObject> list)
    {
	this.list = list;
    }

    public void addToList(PrimaryFccObject object)
    {
	list.add(object);
    }

    public ArrayList<PrimaryFccObject> getList()
    {
	return list;
    }
    public void clear() {
    	list.clear();
    }
}
