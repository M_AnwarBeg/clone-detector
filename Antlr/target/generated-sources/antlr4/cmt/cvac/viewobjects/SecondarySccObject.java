package cmt.cvac.viewobjects;

public class SecondarySccObject
{

    private int id;
    private int fId;
    private int startLine;
    private int endLine;
    private String fileName;
    private int startColumn;
    private int endColumn;
    

    public SecondarySccObject()
    {
	id = -1;
	fId = -1;
	startLine = -1;
	endLine = -1;
	fileName = null;
	startColumn = -1;
	endColumn = -1;

    }

    public SecondarySccObject(int id, int mId, String methodName, int startLine, int endLine, int startColumn,
	    int endColumn)
    {
	this.id = id;
	this.fId = mId;
	this.startLine = startLine;
	this.endLine = endLine;
	this.fileName = methodName;
	this.startColumn = startColumn;
	this.endColumn = endColumn;
    }

    public String getFileName()
    {
	return fileName;
    }

    public int getFId()
    {
	return fId;
    }

    public int getStartLine()
    {
	return startLine;
    }

    public int getEndLine()
    {
	return endLine;
    }

    public int getId()
    {
	return id;
    }

	public int getStartColumn() {
		return startColumn;
	}

	public void setStartColumn(int startColumn) {
		this.startColumn = startColumn;
	}

	public int getEndColumn() {
		return endColumn;
	}

	public void setEndColumn(int endColumn) {
		this.endColumn = endColumn;
	}
    
    

}
