package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class PrimaryFcsObjectCrossDirs {
	
	private int fcsId;
	private ArrayList <Integer> fccStructure;
	private ArrayList <Integer> dirStructure;
	private int numberOfInstance;
	private ArrayList<SecondaryFcsObjectCrossDirs> instanceList;
	
	public PrimaryFcsObjectCrossDirs()
	{
		fcsId=0;
		fccStructure=new ArrayList<>();
		dirStructure=new ArrayList<>();
		numberOfInstance=0;
		instanceList=new ArrayList();
	}
	public PrimaryFcsObjectCrossDirs(int fcsId,ArrayList<Integer> fccStructure, ArrayList<Integer> dirStructure,int numberOfInstance)
	{
		this.fcsId=fcsId;
		this.fccStructure = fccStructure;
		this.dirStructure = dirStructure;
		this.numberOfInstance=numberOfInstance;
	}
	public void setCloneList(ArrayList<SecondaryFcsObjectCrossDirs> cloneList)
	{
		this.instanceList=cloneList;
	}
	public int getFcsId()
	{
		return fcsId;
	}
	public ArrayList<Integer> getFccStructure()
	{
		return fccStructure;
	}
	public ArrayList<Integer> getDirStructure()
	{
		return dirStructure;
	}
	public int getNumberOfInstance()
	{
		return numberOfInstance;
	}
	public ArrayList<SecondaryFcsObjectCrossDirs> getCloneList()
	{
		return instanceList;
	}
	public void setFcsId(int id)
	{
		this.fcsId=id;
	}
	public void addToFccStructure(int s)
	{
		this.fccStructure.add(s);
	}
	public void addToDirStructure(int s)
	{
		this.dirStructure.add(s);
	}
	public void setNofInstances(int n)
	{
		this.numberOfInstance=n;
	}
	public void addToCloneList(SecondaryFcsObjectCrossDirs obj)
	{
		this.instanceList.add(obj);
	}

}
