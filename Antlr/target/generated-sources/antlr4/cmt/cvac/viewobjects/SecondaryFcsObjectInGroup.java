package cmt.cvac.viewobjects;

public class SecondaryFcsObjectInGroup {
	
    private int fId;
    private int gId;
	private int fccid;
    private String fileName;

    public SecondaryFcsObjectInGroup()
    {
	fccid = -1;
	fId = -1;
	gId = -1;
	fileName = null;
    }

    public SecondaryFcsObjectInGroup(int gid, int fccid, int fId, String fileName)
    {
	this.fccid = fccid;
	this.fId = fId;
	this.gId = gid;
	this.fileName = fileName;
    }

    public int getFccId()
    {
	return fccid;
    }

    public int getFId()
    {
	return fId;
    }

    public int getGId()
    {
	return gId;
    }
    
    public String getFileName()
    {
	return fileName;
    }

}
