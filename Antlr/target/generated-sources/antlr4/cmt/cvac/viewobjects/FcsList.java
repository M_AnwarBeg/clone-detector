package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class FcsList {
	private ArrayList<PrimaryFcsObjectCrossDirs> list;

    public FcsList()
    {
	list = new ArrayList<PrimaryFcsObjectCrossDirs>();

    }

    public FcsList(ArrayList<PrimaryFcsObjectCrossDirs> list)
    {
	this.list = list;
    }

    public void addToList(PrimaryFcsObjectCrossDirs object)
    {
	list.add(object);
    }

    public ArrayList<PrimaryFcsObjectCrossDirs> getList()
    {
	return list;
    }

}
