package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class FcsListInGroup {
	private ArrayList<PrimaryFcsObjectInGroup> list;

    public FcsListInGroup()
    {
	list = new ArrayList<PrimaryFcsObjectInGroup>();

    }

    public FcsListInGroup(ArrayList<PrimaryFcsObjectInGroup> list)
    {
	this.list = list;
    }

    public void addToList(PrimaryFcsObjectInGroup object)
    {
	list.add(object);
    }

    public ArrayList<PrimaryFcsObjectInGroup> getList()
    {
	return list;
    }

}
