package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class PrimaryFccObject {

	private int id;
	private int fccId;
	private ArrayList <Integer> structure;
	private int numberOfInstance;
	private double atc;
	private double apc;
	private FccCloneInstanceList cloneList;
	
	public PrimaryFccObject()
	{
		id=0;
		fccId=0;
		structure=new ArrayList<>();
		numberOfInstance=0;
		atc=0;
		apc=0;
		cloneList=new FccCloneInstanceList();
	}
	public PrimaryFccObject(int id,int fccId,ArrayList<Integer> structure,int numberOfInstance,
			double atc,double apc)
	{
		this.id=id;
		this.fccId=fccId;
		this.structure=structure;
		this.numberOfInstance=numberOfInstance;
		this.apc=apc;
		this.atc=atc;
	}
	public void setCloneList(FccCloneInstanceList cloneList)
	{
		this.cloneList=cloneList;
	}
	public int getId()
	{
		return id;
	}
	public int getFccId()
	{
		return fccId;
	}
	public ArrayList<Integer> getStructure()
	{
		return structure;
	}
	public int getNumberOfInstance()
	{
		return numberOfInstance;
	}
	public double getApc()
	{
		return apc;
	}
	public double getAtc()
	{
		return atc;
	}
	public FccCloneInstanceList getCloneList()
	{
		return cloneList;
	}
	
	public void setId(int id)
	{
		this.id=id;
	}
	public void setFccId(int id)
	{
		this.fccId=id;
	}
	public void addToStructure(int s)
	{
		this.structure.add(s);
	}
	public void setNofInstances(int n)
	{
		this.numberOfInstance=n;
	}
	public void setAtc(double atc)
	{
		this.atc=atc;
	}
	public void setApc(double apc)
	{
		this.apc=apc;
	}
	public void addToCloneList(SecondaryFccObject obj)
	{
		this.cloneList.addToList(obj);
	}
}
