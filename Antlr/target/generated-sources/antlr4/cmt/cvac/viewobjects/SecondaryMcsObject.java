package cmt.cvac.viewobjects;

public class SecondaryMcsObject {
	
	private int id;
    private int fId;
    private int gId;
    private int dId;
    private String fileName;

    public SecondaryMcsObject()
    {
	id = 0;
	fId = 0;
	dId = 0;
	gId = 0;
	fileName = null;
    }

    public SecondaryMcsObject( int Id, int fId, int dId, int gId, String fileName)
    {
	this.id = Id;
	this.fId = fId;
	this.dId = dId;
	this.gId = gId;
	this.fileName = fileName;
    }

    public int getId()
    {
	return id;
    }

    public int getFId()
    {
	return fId;
    }

    public int getDId()
    {
	return dId;
    }

    public int GId()
    {
	return gId;
    }

    public String getFileName()
    {
	return fileName;
    }

}
