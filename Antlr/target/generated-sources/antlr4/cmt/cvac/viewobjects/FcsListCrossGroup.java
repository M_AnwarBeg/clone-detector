package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class FcsListCrossGroup {
	private ArrayList<PrimaryFcsObjectCrossGroup> list;

    public FcsListCrossGroup()
    {
	list = new ArrayList<PrimaryFcsObjectCrossGroup>();

    }

    public FcsListCrossGroup(ArrayList<PrimaryFcsObjectCrossGroup> list)
    {
	this.list = list;
    }

    public void addToList(PrimaryFcsObjectCrossGroup object)
    {
	list.add(object);
    }

    public ArrayList<PrimaryFcsObjectCrossGroup> getList()
    {
	return list;
    }

}
