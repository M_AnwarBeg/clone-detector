package cmt.cvac.viewobjects;

public class SecondaryFcsObjectInDirs {
	
    private int fId;
    private int instanceId;
	private int fccid;
    private String fileName;

    public SecondaryFcsObjectInDirs()
    {
	fccid = 0;
	fId = 0;
	instanceId = 0;
	fileName = null;
    }

    public SecondaryFcsObjectInDirs(int fccid, int fId, int iId, String fileName)
    {
	this.fccid = fccid;
	this.fId = fId;
	this.instanceId = iId;
	this.fileName = fileName;
    }

    public int getFccId()
    {
	return fccid;
    }

    public int getFId()
    {
	return fId;
    }

    public int getInstanceId()
    {
	return instanceId;
    }

    public String getFileName()
    {
	return fileName;
    }

}
