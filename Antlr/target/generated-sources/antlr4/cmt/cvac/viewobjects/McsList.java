package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class McsList {
	 private ArrayList<PrimaryMcsObject> list;

	    public McsList()
	    {
		list = new ArrayList<PrimaryMcsObject>();

	    }

	    public void MccList(ArrayList<PrimaryMcsObject> list)
	    {
		this.list = list;
	    }

	    public void addToList(PrimaryMcsObject object)
	    {
		list.add(object);
	    }

	    public ArrayList<PrimaryMcsObject> getList()
	    {
		return list;
	    }

}
