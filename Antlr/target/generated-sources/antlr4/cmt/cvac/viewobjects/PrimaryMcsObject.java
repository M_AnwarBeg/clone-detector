package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class PrimaryMcsObject {
	
	private int id;
	private int mcsId;
	private ArrayList <Integer> structure;
	private int numberOfInstance;
	private ArrayList<SecondaryMcsObject> cloneList;
	
	public PrimaryMcsObject()
	{
		id =0;
		mcsId=0;
		structure=new ArrayList<>();
		numberOfInstance=0;
		cloneList=new ArrayList();
	}
	public PrimaryMcsObject(int i,int mcsId,ArrayList<Integer> structure,int numberOfInstance)
	{
		this.id =i;
		this.mcsId=mcsId;
		this.structure=structure;
		this.numberOfInstance=numberOfInstance;
		cloneList = new ArrayList<SecondaryMcsObject>();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setCloneList(ArrayList<SecondaryMcsObject> cloneList)
	{
		this.cloneList=cloneList;
	}
	public int getMcsId()
	{
		return mcsId;
	}
	public ArrayList<Integer> getStructure()
	{
		return structure;
	}
	public int getNumberOfInstance()
	{
		return numberOfInstance;
	}
	public ArrayList<SecondaryMcsObject> getCloneList()
	{
		return cloneList;
	}
	public void setMcsId(int id)
	{
		this.mcsId=id;
	}
	public void addToStructure(int s)
	{
		this.structure.add(s);
	}
	public void setNofInstances(int n)
	{
		this.numberOfInstance=n;
	}
	public void addToCloneList(SecondaryMcsObject obj)
	{
		this.cloneList.add(obj);
	}

}
