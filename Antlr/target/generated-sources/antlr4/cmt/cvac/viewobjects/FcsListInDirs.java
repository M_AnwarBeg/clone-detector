package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class FcsListInDirs {
	private ArrayList<PrimaryFcsObjectInDirs> list;

    public FcsListInDirs()
    {
	list = new ArrayList<PrimaryFcsObjectInDirs>();

    }

    public FcsListInDirs(ArrayList<PrimaryFcsObjectInDirs> list)
    {
	this.list = list;
    }

    public void addToList(PrimaryFcsObjectInDirs object)
    {
	list.add(object);
    }

    public ArrayList<PrimaryFcsObjectInDirs> getList()
    {
	return list;
    }

}
