package cmt.cvac.viewobjects;

public class SecondaryFcsObjectCrossDirs {
	
    private int fId;
    private int dId;
	private int fccid;
    private String fileName;

    public SecondaryFcsObjectCrossDirs()
    {
	fccid = 0;
	fId = 0;
	dId = 0;
	fileName = null;
    }

    public SecondaryFcsObjectCrossDirs(int fccid, int fId, int dId, String fileName)
    {
	this.fccid = fccid;
	this.fId = fId;
	this.dId = dId;
	this.fileName = fileName;
    }

    public int getFccId()
    {
	return fccid;
    }

    public int getFId()
    {
	return fId;
    }

    public int getDId()
    {
	return dId;
    }

    public String getFileName()
    {
	return fileName;
    }

}
