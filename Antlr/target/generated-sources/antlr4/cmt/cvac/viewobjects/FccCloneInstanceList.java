package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class FccCloneInstanceList {

	private ArrayList<SecondaryFccObject> list;

    public FccCloneInstanceList()
    {
	list = new ArrayList<SecondaryFccObject>();
    }

    public FccCloneInstanceList(ArrayList<SecondaryFccObject> list)
    {
	this.list = list;
    }

    public void addToList(SecondaryFccObject object)
    {
	list.add(object);
    }

    public ArrayList<SecondaryFccObject> getList()
    {
	return list;
    }
}
