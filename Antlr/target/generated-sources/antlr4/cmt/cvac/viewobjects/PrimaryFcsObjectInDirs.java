package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class PrimaryFcsObjectInDirs {
	
	private int fcsId;
	private ArrayList <Integer> fccStructure;
	private int dirId;
	private int numberOfInstance;
	private ArrayList<SecondaryFcsObjectInDirs> instanceList;
	
	public PrimaryFcsObjectInDirs()
	{
		fcsId=0;
		fccStructure=new ArrayList<>();
		dirId=0;
		numberOfInstance=0;
		instanceList=new ArrayList();
	}
	public PrimaryFcsObjectInDirs(int fcsId,ArrayList<Integer> fccStructure, int dir,int numberOfInstance)
	{
		this.fcsId=fcsId;
		this.fccStructure = fccStructure;
		dirId = dir;
		this.numberOfInstance=numberOfInstance;
	}
	public void setCloneList(ArrayList<SecondaryFcsObjectInDirs> cloneList)
	{
		this.instanceList=cloneList;
	}
	public int getFcsId()
	{
		return fcsId;
	}
	public ArrayList<Integer> getFccStructure()
	{
		return fccStructure;
	}
	public int getDirId()
	{
		return dirId;
	}
	public int getNumberOfInstance()
	{
		return numberOfInstance;
	}
	public ArrayList<SecondaryFcsObjectInDirs> getCloneList()
	{
		return instanceList;
	}
	public void setFcsId(int id)
	{
		this.fcsId=id;
	}
	public void addToFccStructure(int s)
	{
		this.fccStructure.add(s);
	}
	public void setDirId(int s)
	{
		this.dirId = s;
	}
	public void setNofInstances(int n)
	{
		this.numberOfInstance=n;
	}
	public void addToCloneList(SecondaryFcsObjectInDirs obj)
	{
		this.instanceList.add(obj);
	}

}
