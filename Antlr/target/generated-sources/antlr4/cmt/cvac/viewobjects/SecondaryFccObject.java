package cmt.cvac.viewobjects;

public class SecondaryFccObject {

	private int id;
    private int fId;
    private int gId;
    private int tc;
    private double pc;
    private int dId;
    private String fileName;

    public SecondaryFccObject()
    {
	id = 0;
	fId = 0;
	dId = 0;
	tc = 0;
	pc = 0;
	gId = 0;
	fileName = null;
    }

    public SecondaryFccObject(int id, int fId, int dId, int tc, double pc, int gId, String fileName)
    {
	this.id = id;
	this.fId = fId;
	this.dId = dId;
	this.tc = tc;
	this.pc = pc;
	this.gId = gId;
	this.fileName = fileName;
    }

    public int getId()
    {
	return id;
    }

    public int getFId()
    {
	return fId;
    }

    public int getDId()
    {
	return dId;
    }

    public int getTc()
    {
	return tc;
    }

    public double getPc()
    {
	return pc;
    }

    public int GId()
    {
	return gId;
    }

    public String getFileName()
    {
	return fileName;
    }
}
