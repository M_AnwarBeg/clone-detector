package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class PrimaryFcsObjectCrossGroup {
	
	private int fcsId;
	private ArrayList <Integer> fccStructure;
	private ArrayList <Integer> groupStructure;
	private int numberOfInstance;
	private ArrayList<SecondaryFcsObjectCrossGroup> instanceList;
	
	public PrimaryFcsObjectCrossGroup()
	{
		fcsId=0;
		fccStructure=new ArrayList<>();
		groupStructure=new ArrayList<>();
		numberOfInstance=0;
		instanceList=new ArrayList();
	}
	public PrimaryFcsObjectCrossGroup(int fcsId,ArrayList<Integer> fccStructure, ArrayList<Integer> groupStructure,int numberOfInstance)
	{
		this.fcsId=fcsId;
		this.fccStructure = fccStructure;
		this.groupStructure = groupStructure;
		this.numberOfInstance=numberOfInstance;
	}
	public void setCloneList(ArrayList<SecondaryFcsObjectCrossGroup> cloneList)
	{
		this.instanceList=cloneList;
	}
	public int getFcsId()
	{
		return fcsId;
	}
	public ArrayList<Integer> getFccStructure()
	{
		return fccStructure;
	}
	public ArrayList<Integer> getGroupStructure()
	{
		return groupStructure;
	}
	public int getNumberOfInstance()
	{
		return numberOfInstance;
	}
	public ArrayList<SecondaryFcsObjectCrossGroup> getCloneList()
	{
		return instanceList;
	}
	public void setFcsId(int id)
	{
		this.fcsId=id;
	}
	public void addToFccStructure(int s)
	{
		this.fccStructure.add(s);
	}
	public void addToGroupStructure(int s)
	{
		this.groupStructure.add(s);
	}
	public void setNofInstances(int n)
	{
		this.numberOfInstance=n;
	}
	public void addToCloneList(SecondaryFcsObjectCrossGroup obj)
	{
		this.instanceList.add(obj);
	}

}
