package cmt.cvac.viewobjects;

import java.util.ArrayList;

public class PrimaryFcsObjectInGroup {
	
	private int fcsId;
	private ArrayList <Integer> fccStructure;
	private ArrayList <Integer> groupStructure;
	private int numberOfInstance;
	private ArrayList<SecondaryFcsObjectInGroup> instanceList;
	
	public PrimaryFcsObjectInGroup()
	{
		fcsId=0;
		fccStructure=new ArrayList<>();
		groupStructure=new ArrayList<>();
		numberOfInstance=0;
		instanceList=new ArrayList();
	}
	public PrimaryFcsObjectInGroup(int fcsId,ArrayList<Integer> fccStructure, ArrayList<Integer> groupStructure,int numberOfInstance)
	{
		this.fcsId=fcsId;
		this.fccStructure = fccStructure;
		this.groupStructure = groupStructure;
		this.numberOfInstance=numberOfInstance;
	}
	public void setCloneList(ArrayList<SecondaryFcsObjectInGroup> cloneList)
	{
		this.instanceList=cloneList;
	}
	public int getFcsId()
	{
		return fcsId;
	}
	public ArrayList<Integer> getFccStructure()
	{
		return fccStructure;
	}
	public ArrayList<Integer> getGroupStructure()
	{
		return groupStructure;
	}
	public int getNumberOfInstance()
	{
		return numberOfInstance;
	}
	public ArrayList<SecondaryFcsObjectInGroup> getCloneList()
	{
		return instanceList;
	}
	public void setFcsId(int id)
	{
		this.fcsId=id;
	}
	public void addToFccStructure(int s)
	{
		this.fccStructure.add(s);
	}
	public void addToGroupStructure(int s)
	{
		this.groupStructure.add(s);
	}
	public void setNofInstances(int n)
	{
		this.numberOfInstance=n;
	}
	public void addToCloneList(SecondaryFcsObjectInGroup obj)
	{
		this.instanceList.add(obj);
	}

}
