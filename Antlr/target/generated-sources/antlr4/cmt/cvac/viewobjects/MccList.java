package cmt.cvac.viewobjects;

import java.util.ArrayList;


public class MccList {

    private ArrayList<PrimaryMccObject> list;

    public MccList()
    {
	list = new ArrayList<PrimaryMccObject>();

    }

    public MccList(ArrayList<PrimaryMccObject> list)
    {
	this.list = list;
    }

    public void addToList(PrimaryMccObject object)
    {
	list.add(object);
    }

    public ArrayList<PrimaryMccObject> getList()
    {
	return list;
    }



}
