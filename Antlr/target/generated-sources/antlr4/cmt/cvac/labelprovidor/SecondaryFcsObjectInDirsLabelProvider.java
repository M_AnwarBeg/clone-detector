package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectInDirs;


class SecondaryFcsObjectColumnInDirs
{
    public static final int fccid = 2;
    public static final int fId = 1;
    public static final int instanceId = 0;
    public static final int fileName = 3;
}
public class SecondaryFcsObjectInDirsLabelProvider implements ITableLabelProvider{

	public SecondaryFcsObjectInDirsLabelProvider()
    {

    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
    	SecondaryFcsObjectInDirs object = (SecondaryFcsObjectInDirs) element;
	String text = "";
	switch (columnIndex) {
	case SecondaryFcsObjectColumnInDirs.fccid:
	    text = String.valueOf(object.getFccId());
	    break;
	case SecondaryFcsObjectColumnInDirs.fId:
	    text = String.valueOf(object.getFId());
	    break;
	case SecondaryFcsObjectColumnInDirs.instanceId:
	    text = String.valueOf(object.getInstanceId());
	    break;
	case SecondaryFcsObjectColumnInDirs.fileName:
	    text = (object.getFileName());
	    break;
	}
	return text;
    }
}
