package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.PrimaryFccObject;
import cmt.cvac.viewobjects.PrimaryMcsObject;

class PrimaryMcsObjectColumn
{
	public static final int Id = 0;
    public static final int mcsId = 1;
    public static final int structure = 2;
    public static final int numberOfInstance = 3;

}
public class PrimaryMcsObjectLabelProvider implements ITableLabelProvider{

	public PrimaryMcsObjectLabelProvider()
    {
	super();
    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
	PrimaryMcsObject object = (PrimaryMcsObject) element;
	String text = "";
	switch (columnIndex) {
	case PrimaryMcsObjectColumn.Id:
		text = String.valueOf(object.getId());
		break;
	case PrimaryMcsObjectColumn.mcsId:
	    text = String.valueOf(object.getMcsId());
	    break;
	case PrimaryMcsObjectColumn.structure:
	    String result = "";
	    for (int i = 0; i < object.getStructure().size(); i++)
	    {
		result = result + String.valueOf(object.getStructure().get(i));
		if (i < object.getStructure().size() - 1)
		{
		    result = result + ",";
		}
	    }
	    text = result;
	    break;
	case PrimaryMcsObjectColumn.numberOfInstance:
	    text = String.valueOf(object.getNumberOfInstance());
	    break;
	}
	return text;
    }
}
