package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectInDirs;

class PrimaryFcsObjectColumnInDirs
{
    public static final int fcsId = 0;
    public static final int fccStructure = 1;
    public static final int dirId = 2;
    public static final int numberOfInstance = 3;

}
public class PrimaryFcsObjectInDirsLabelProvider implements ITableLabelProvider{

	public PrimaryFcsObjectInDirsLabelProvider()
    {
	super();
    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
    	PrimaryFcsObjectInDirs object = (PrimaryFcsObjectInDirs) element;
	String text = "";
	switch (columnIndex) {
	case PrimaryFcsObjectColumnInDirs.fcsId:
	    text = String.valueOf(object.getFcsId());
	    break;
	case PrimaryFcsObjectColumnInDirs.fccStructure:
	    String result = "";
	    for (int i = 0; i < object.getFccStructure().size(); i++)
	    {
			result = result + String.valueOf(object.getFccStructure().get(i));
			if (i < object.getFccStructure().size() - 1)
				{
				    result = result + ",";
				}
	    }
	    text = result;
	    break;
	case PrimaryFcsObjectColumnInDirs.dirId:
		text = String.valueOf(object.getDirId());
	    break;
	case PrimaryFcsObjectColumnInDirs.numberOfInstance:
	    text = String.valueOf(object.getNumberOfInstance());
	    break;
	}
	return text;
    }
}
