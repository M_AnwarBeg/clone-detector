package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.SecondaryFccObject;
import cmt.cvac.viewobjects.SecondaryMcsObject;

class SecondaryMcsObjectColumn
{
    public static final int id = 0;
    public static final int fId = 3;
    public static final int gId = 1;
    public static final int dId = 2;
    public static final int fileName = 4;
}
public class SecondaryMcsObjectLabelProvider implements ITableLabelProvider{

	public SecondaryMcsObjectLabelProvider()
    {

    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
	SecondaryMcsObject object = (SecondaryMcsObject) element;
	String text = "";
	switch (columnIndex) {
	case SecondaryMcsObjectColumn.id:
		text = String.valueOf(object.getId());
		break;
	case SecondaryMcsObjectColumn.fId:
	    text = String.valueOf(object.getFId());
	    break;
	case SecondaryMcsObjectColumn.gId:
	    text = String.valueOf(object.GId());
	    break;
	case SecondaryMcsObjectColumn.dId:
	    text = String.valueOf(object.getDId());
	    break;
	case SecondaryMcsObjectColumn.fileName:
	    text = (object.getFileName());
	    break;
	}
	return text;
    }
}
