package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;


class SecondaryFcsObjectColumn
{
    public static final int fccid = 2;
    public static final int fId = 1;
    public static final int dId = 0;
    public static final int fileName = 3;
}
public class SecondaryFcsObjectCrossDirsLabelProvider implements ITableLabelProvider{

	public SecondaryFcsObjectCrossDirsLabelProvider()
    {

    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
    	SecondaryFcsObjectCrossDirs object = (SecondaryFcsObjectCrossDirs) element;
	String text = "";
	switch (columnIndex) {
	case SecondaryFcsObjectColumn.fccid:
	    text = String.valueOf(object.getFccId());
	    break;
	case SecondaryFcsObjectColumn.fId:
	    text = String.valueOf(object.getFId());
	    break;
	case SecondaryFcsObjectColumn.dId:
	    text = String.valueOf(object.getDId());
	    break;
	case SecondaryFcsObjectColumn.fileName:
	    text = (object.getFileName());
	    break;
	}
	return text;
    }
}
