package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.SecondaryFccObject;
import cmt.cvac.viewobjects.SecondaryMccObject;

class FccCloneInstanceListColumn
{
    public static final int id = 0;
    public static final int gId = 1;
    public static final int dId = 2;
    public static final int fId = 3;
    public static final int tc = 4;
    public static final int pc = 5;
    public static final int fileName = 6;
}
public class FccCloneInstanceListLabelProvider implements ITableLabelProvider{

	public FccCloneInstanceListLabelProvider()
    {

    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
	SecondaryFccObject object = (SecondaryFccObject) element;
	String text = "";
	switch (columnIndex) {
	case FccCloneInstanceListColumn.id:
	    text = String.valueOf(object.getId());
	    break;
	case FccCloneInstanceListColumn.gId:
	    text = String.valueOf(object.getFId());
	    break;
	case FccCloneInstanceListColumn.dId:
	    text = String.valueOf(object.getDId());
	case FccCloneInstanceListColumn.fId:
	    text = String.valueOf(object.getFId());
	    break;
	case FccCloneInstanceListColumn.tc:
	    text = String.valueOf(object.getTc());
	    break;
	case FccCloneInstanceListColumn.pc:
	    text = Double.toString(object.getPc());
	    break;
	case FccCloneInstanceListColumn.fileName:
	    text = (object.getFileName());
	    break;
	}
	return text;
    }
}
