package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossGroup;
import cmt.cvac.viewobjects.PrimaryFcsObjectInGroup;

class PrimaryFcsObjectColumnInGroup
{
    public static final int fcsId = 0;
    public static final int fccStructure = 1;
    public static final int groupStructure = 2;
    public static final int numberOfInstance = 3;

}
public class PrimaryFcsObjectInGroupLabelProvider implements ITableLabelProvider{

	public PrimaryFcsObjectInGroupLabelProvider()
    {
	super();
    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
    	PrimaryFcsObjectInGroup object = (PrimaryFcsObjectInGroup) element;
	String text = "";
	switch (columnIndex) {
	case PrimaryFcsObjectColumnInGroup.fcsId:
	    text = String.valueOf(object.getFcsId());
	    break;
	case PrimaryFcsObjectColumnInGroup.fccStructure:
	    String result = "";
	    for (int i = 0; i < object.getFccStructure().size(); i++)
	    {
			result = result + String.valueOf(object.getFccStructure().get(i));
			if (i < object.getFccStructure().size() - 1)
				{
				    result = result + ",";
				}
	    }
	    text = result;
	    break;
	case PrimaryFcsObjectColumnInGroup.groupStructure:
	    String result2 = "";
	    for (int i = 0; i < object.getGroupStructure().size(); i++)
	    {
	    	result2 = result2 + String.valueOf(object.getGroupStructure().get(i));
			if (i < object.getGroupStructure().size() - 1)
				{
					result2 = result2 + ",";
				}
	    }
	    text = result2;
	    break;
	case PrimaryFcsObjectColumnInGroup.numberOfInstance:
	    text = String.valueOf(object.getNumberOfInstance());
	    break;
	}
	return text;
    }
}
