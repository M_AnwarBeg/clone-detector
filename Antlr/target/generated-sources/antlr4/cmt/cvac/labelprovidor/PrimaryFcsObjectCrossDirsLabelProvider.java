package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;

class PrimaryFcsObjectColumn
{
    public static final int fcsId = 0;
    public static final int fccStructure = 1;
    public static final int dirStructure = 2;
    public static final int numberOfInstance = 3;

}
public class PrimaryFcsObjectCrossDirsLabelProvider implements ITableLabelProvider{

	public PrimaryFcsObjectCrossDirsLabelProvider()
    {
	super();
    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
    	PrimaryFcsObjectCrossDirs object = (PrimaryFcsObjectCrossDirs) element;
	String text = "";
	switch (columnIndex) {
	case PrimaryFcsObjectColumn.fcsId:
	    text = String.valueOf(object.getFcsId());
	    break;
	case PrimaryFcsObjectColumn.fccStructure:
	    String result = "";
	    for (int i = 0; i < object.getFccStructure().size(); i++)
	    {
			result = result + String.valueOf(object.getFccStructure().get(i));
			if (i < object.getFccStructure().size() - 1)
				{
				    result = result + ",";
				}
	    }
	    text = result;
	    break;
	case PrimaryFcsObjectColumn.dirStructure:
	    String result2 = "";
	    for (int i = 0; i < object.getDirStructure().size(); i++)
	    {
	    	result2 = result2 + String.valueOf(object.getDirStructure().get(i));
			if (i < object.getDirStructure().size() - 1)
				{
					result2 = result2 + ",";
				}
	    }
	    text = result2;
	    break;
	case PrimaryFcsObjectColumn.numberOfInstance:
	    text = String.valueOf(object.getNumberOfInstance());
	    break;
	}
	return text;
    }
}
