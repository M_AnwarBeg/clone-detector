package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.PrimaryFccObject;
import cmt.cvac.viewobjects.PrimaryMccObject;

class FccListColumn
{
    public static final int id = 0;
    public static final int fccId = 1;
    public static final int structure = 2;
    public static final int numberOfInstance = 3;
    public static final int atc = 4;
    public static final int apc = 5;

}
public class FccListLabelProvider implements ITableLabelProvider{

	public FccListLabelProvider()
    {
	super();
    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
	PrimaryFccObject object = (PrimaryFccObject) element;
	String text = "";
	switch (columnIndex) {
	case FccListColumn.id:
	    text = String.valueOf(object.getId());
	    break;
	case FccListColumn.fccId:
	    text = String.valueOf(object.getFccId());
	    break;
	case FccListColumn.structure:
	    String result = "";
	    for (int i = 0; i < object.getStructure().size(); i++)
	    {
		result = result + String.valueOf(object.getStructure().get(i));
		if (i < object.getStructure().size() - 1)
		{
		    result = result + ",";
		}
	    }
	    text = result;
	    break;
	case FccListColumn.numberOfInstance:
	    text = String.valueOf(object.getNumberOfInstance());
	    break;
	case FccListColumn.atc:
	    text = String.valueOf(object.getAtc());
	    break;
	case FccListColumn.apc:
	    text = String.valueOf(object.getApc());
	}
	return text;
    }
}
