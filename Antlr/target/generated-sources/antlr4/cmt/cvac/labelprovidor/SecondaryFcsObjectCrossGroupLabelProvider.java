package cmt.cvac.labelprovidor;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossGroup;


class SecondaryFcsObjectColumnCrossGroup
{
    public static final int fccid = 2;
    public static final int fId = 1;
    public static final int gId = 0;
    public static final int fileName = 3;
}
public class SecondaryFcsObjectCrossGroupLabelProvider implements ITableLabelProvider{

	public SecondaryFcsObjectCrossGroupLabelProvider()
    {

    }

    @Override
    public void addListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public boolean isLabelProperty(Object element, String property)
    {
	// TODO Auto-generated method stub
	return false;
    }

    @Override
    public void removeListener(ILabelProviderListener listener)
    {
	// TODO Auto-generated method stub

    }

    @Override
    public Image getColumnImage(Object element, int columnIndex)
    {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public String getColumnText(Object element, int columnIndex)
    {
    	SecondaryFcsObjectCrossGroup object = (SecondaryFcsObjectCrossGroup) element;
	String text = "";
	switch (columnIndex) {
	case SecondaryFcsObjectColumnCrossGroup.fccid:
	    text = String.valueOf(object.getFccId());
	    break;
	case SecondaryFcsObjectColumnCrossGroup.fId:
	    text = String.valueOf(object.getFId());
	    break;
	case SecondaryFcsObjectColumnCrossGroup.gId:
	    text = String.valueOf(object.getGId());
	    break;
	case SecondaryFcsObjectColumnCrossGroup.fileName:
	    text = (object.getFileName());
	    break;
	}
	return text;
    }
}
