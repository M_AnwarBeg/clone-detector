package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import cmt.cddc.simpleclones.SimpleCloneInstance;
import eu.cqse.check.framework.scanner.IToken;

public class SimpleCloneCompareInput implements IEditorInput{
	
	public final String leftCloneData;
	public final String rightCloneData;
	
	public final int LstartL;
	public final int LendL;
	
	public final int RstartL;
	public final int RendL;
	
	public final int rightInstanceId;
	public final int leftInstanceId;
	
	List<IToken> leftCloneToken;
	List<IToken> rightCloneToken;
	
	String leftFileName;
	String rightFileName;
	
	public ArrayList<SimpleCloneInstance> instancesList;
	
	public SimpleCloneCompareInput(String leftCloneData,
			String rightCloneData , int LstartL, int LendL, int RstartL, int RendL,List<IToken> leftCloneToken, 
			List<IToken> rightCloneToken, String name01, String name02, ArrayList<SimpleCloneInstance>list, int curentLeft,int currentRight) {
		this.leftCloneData = leftCloneData;
		this.rightCloneData = rightCloneData;
		this.LstartL = LstartL;
		this.LendL = LendL;
		this.RstartL = RstartL;
		this.RendL = RendL;
		this.leftCloneToken = leftCloneToken;
		this.rightCloneToken = rightCloneToken;
		this.leftFileName = name01;
		this.rightFileName = name02;
		this.instancesList = list;
		this.rightInstanceId = currentRight;
		this.leftInstanceId = curentLeft;
	}

	@Override
	public <T> T getAdapter(Class<T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return null;
	}

}
