package cmt.cvac.clonecomparsion;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

public class CloneCompareIndicator extends Canvas implements PaintListener 
{
	
	/** The left text widget. */
	private StyledText leftText;
	
	/** The right text widget. */
	private StyledText rightText;
	
	/** Left clone data. */
	private final String leftCloneData;
	
	/** Right clone data. */
	private final String rightCloneData;
	
	private int LstartL;
	private int LendL;
	private int leftSize;
	private int rightSize;
	private int RstartL;
	private int RendL;
	private ArrayList<Integer> leftStartLine;
	private ArrayList<Integer> leftEndLine;
	private ArrayList<Integer> rightStartLine;
	private ArrayList<Integer> rightEndLine;
	
	/** Color used to draw the lines connecting the clones. */
	private static final Color FOREGROUND_COLOR = Display.getDefault()
		.getSystemColor(SWT.COLOR_BLACK);
	
	/** Constructor. */
	public CloneCompareIndicator(Composite parent, int style,
		String leftCloneData, String rightCloneData, int lsl, int lel, int rsl, int rel) {
	super(parent, style);
	this.LstartL=lsl;
	this.LendL=lel;
	
	this.RstartL=rsl;
	this.RendL=rel;
	this.leftSize = 1;
	this.rightSize = 1;
	
	this.leftCloneData = leftCloneData;
	this.rightCloneData = rightCloneData;
	addPaintListener(this);
	}
	public CloneCompareIndicator(Composite container, int Style, String leftCloneData, String rightCloneData) {
		super(container, Style);
		this.leftCloneData = leftCloneData;
		this.rightCloneData = rightCloneData;
		
	}
	
	public CloneCompareIndicator(Composite container, int Style, String leftCloneData, String rightCloneData,
			ArrayList<Integer>leftStart, ArrayList<Integer>leftEnd, ArrayList<Integer>rightStart, ArrayList<Integer>rightEnd) {
		// TODO Auto-generated constructor stub
		super(container, Style);
		this.leftCloneData = leftCloneData;
		this.rightCloneData = rightCloneData;
		this.LstartL = leftStart.get(0);
		this.LendL = leftEnd.get(0);
		this.RstartL = rightStart.get(0);
		this.RendL = rightEnd.get(0);
		this.leftStartLine = leftStart;
		this.leftEndLine = leftEnd;
		this.rightStartLine = rightStart;
		this.rightEndLine = rightEnd;
		this.leftSize = leftStart.size();
		this.rightSize = rightStart.size();
		addPaintListener(this);
	}

	/** Sets left StyledText widget. */
	public void setLeftText(StyledText leftText) {
	this.leftText = leftText;
	}
	
	/** Sets right StyledText widget. */
	public void setRightText(StyledText rightText) {
	this.rightText = rightText;
	}
	
	/** {@inheritDoc} */
	@Override
	public void paintControl(PaintEvent e) {
	Rectangle sashBounds = ((Control) e.widget).getBounds();
	((Control) e.widget).setBounds(sashBounds.x, leftText.getBounds().y, sashBounds.width, sashBounds.height);
	sashBounds = ((Control) e.widget).getBounds();
	int topSpace = leftText.getBounds().y - sashBounds.y + 1;
	
	GC gc = e.gc;
	gc.setInterpolation(SWT.HIGH);
	gc.setAntialias(SWT.ON);
	
	Color backgroundColor = Display.getCurrent().getSystemColor(
			SWT.COLOR_WIDGET_BACKGROUND);
	
	gc.setBackground(backgroundColor);
	gc.fillRectangle(0, 0, sashBounds.width, sashBounds.height);
	
	gc.setBackground(new Color(null, 228, 243, 250));
	gc.setForeground(FOREGROUND_COLOR);
	fillClonesPoly(gc, sashBounds, topSpace);
	
	gc.setBackground(backgroundColor);
	gc.fillRectangle(0, 0, sashBounds.width, topSpace);
	}
	
	/** Draws the polygon between the clones. */
	private void fillClonesPoly(GC gc, Rectangle sashBounds, int topSpace) {
	/*Range leftRange = determineCloneRange(leftCloneData, leftText, topSpace);
	Range rightRange = determineCloneRange(rightCloneData, rightText,0
			topSpace);*/
		
		if(leftSize == 1 && rightSize == 1) {
			int relCloneStartL = this.LstartL * leftText.getLineHeight() + 0 - leftText.getTopPixel();
			int relCloneEndL = relCloneStartL + this.LendL * leftText.getLineHeight();
			
			int relCloneStartR = this.RstartL * rightText.getLineHeight() + 0 - rightText.getTopPixel();
			int relCloneEndR = relCloneStartR + this.RendL * rightText.getLineHeight();
			
			int[] poly = { 0, (int) relCloneEndL, 0, (int) relCloneStartL, sashBounds.width, (int) relCloneStartR, 
					sashBounds.width, (int) relCloneEndR };
			gc.fillPolygon(poly);
			gc.drawLine(0, (int) relCloneEndL, sashBounds.width,
					(int) relCloneEndR);
			gc.drawLine(0, (int) relCloneStartL, sashBounds.width,
					(int) relCloneStartR);
			
		}
		else {
			
			for(int i=0; i<leftSize;i++) {
				int relCloneStartL = this.leftStartLine.get(i) * leftText.getLineHeight() + 0 - leftText.getTopPixel();
				int relCloneEndL = relCloneStartL + this.leftEndLine.get(i) * leftText.getLineHeight();
				
				int relCloneStartR = this.rightStartLine.get(i) * rightText.getLineHeight() + 0 - rightText.getTopPixel();
				int relCloneEndR = relCloneStartR + this.rightEndLine.get(i) * rightText.getLineHeight();
				
				int[] poly = { 0, (int) relCloneEndL, 0, (int) relCloneStartL, sashBounds.width, (int) relCloneStartR, 
						sashBounds.width, (int) relCloneEndR };
				//gc.fillPolygon(poly);
				gc.drawLine(0, (int) relCloneEndL, sashBounds.width,
						(int) relCloneEndR);
				gc.drawLine(0, (int) relCloneStartL, sashBounds.width,
						(int) relCloneStartR);
				
			}
		}
	
	
		
		/*int relCloneStart = this.startL
				* text.getLineHeight() + 0
				- text.getTopPixel();
		int relCloneEnd = relCloneStart + this.endL
				* text.getLineHeight();*/
		
		//Range range = CloneCompareIndicator.determineCloneRange(cloneData, text, 0);
		/*gc.drawLine(0, (int) relCloneEnd, text.getBounds().width,
				(int) relCloneEnd);
		gc.drawLine(0, (int) relCloneStart, text.getBounds().width,
				(int) relCloneStart);*/
		
	
	//drawConnectingLines(gc, sashBounds, leftRange, rightRange);
	
	}
	
	/** Draws the connecting lines between the clones. */
	/*private void drawConnectingLines(GC gc, Rectangle sashBounds,
		Range leftRange, Range rightRange) {
	gc.drawLine(0, (int) leftRange.getLower(), sashBounds.width,
			(int) rightRange.getLower());
	gc.drawLine(0, (int) leftRange.getUpper(), sashBounds.width,
			(int) rightRange.getUpper());
	}*/
	
	/** Calculates the range of the clone in pixel relative to the styled text. */
	/* package static Range determineCloneRange(CloneDataBase cloneData,
		StyledText cloneText, int topSpace) {
	int relCloneStart = cloneData.getStartLineInElement()
			* cloneText.getLineHeight() + topSpace
			- cloneText.getTopPixel();
	int relCloneEnd = relCloneStart + cloneData.getLengthInElement()
			* cloneText.getLineHeight();
	return new Range(relCloneStart, relCloneEnd);
	}*/
	
	}
