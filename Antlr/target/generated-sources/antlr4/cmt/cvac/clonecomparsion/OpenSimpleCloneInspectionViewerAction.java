package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ILenientScanner;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerFactory;
import eu.cqse.check.framework.scanner.ScannerUtils;

public class OpenSimpleCloneInspectionViewerAction extends Action{
	private final SimpleCloneInstance clone1;
	private final SimpleCloneInstance clone2;
	private ArrayList<SimpleCloneInstance> instanceList;
	private final int leftInstanceId;
	private final int rightInstanceID;
	private final Boolean flag;
	
	public OpenSimpleCloneInspectionViewerAction(SimpleCloneInstance clone1, SimpleCloneInstance clone2, ArrayList<SimpleCloneInstance>list, int leftCloneId, int rightCloneId) 
	{
		this.clone1=clone1;
		this.clone2=clone2;
		this.instanceList = list;
		this.leftInstanceId = leftCloneId;//clone1.getMethodCloneID();
		this.rightInstanceID = rightCloneId;//clone2.getMethodCloneID();
		this.flag = false;
		//this.leftInstanceId = 
	}
	
	@Override
	public void run() {
		try 
		{
			sFile File_clone1 = sFile_List.getFile(clone1.getFileId());//.getMethod().getFileID());
			sFile File_clone2 = sFile_List.getFile(clone2.getFileId());//getMethod().getFileID());
			String file01 = ProjectInfo.getFileName(File_clone1.getFileID());
			String file02 = ProjectInfo.getFileName(File_clone2.getFileID());
			ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
			
			ILenientScanner scanner02 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())), ProjectInfo.getInputFile(File_clone2.getFileID()));
			
			List<IToken> leftCloneToken = ScannerUtils.readTokens(scanner01);
			List<IToken> rightCloneToken = ScannerUtils.readTokens(scanner02);
			
			//PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().close();
			//PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().
			if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()!=null) {
				if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle().equals("Simple Clone Editor")) {
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor(), true);
				}
				else {
					IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
					for(int i=0; i< editorlist.length;i++) {
						if(editorlist[i].getTitle().equals("Simple Clone Editor")) {
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(editorlist[i], false);
							break;
						}
					}
				}
				
			}
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
					new SimpleCloneCompareInput(
							FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())),
							FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())),
							clone1.getStartingIndex()-1, 
							(clone1.getEndingIndex() - clone1.getStartingIndex())+1, 
							clone2.getStartingIndex()-1, 
							(clone2.getEndingIndex() - clone2.getStartingIndex())+1,
							leftCloneToken, rightCloneToken, file01, file02, instanceList, leftInstanceId, rightInstanceID),
							SimpleClonePairEditor.ID);
		
			
			
							/*clone1.getMethod().getStartToken()+1, clone1.getMethod().getEndToken(), 
							clone2.getMethod().getStartToken(), clone2.getMethod().getEndToken(),*/
			
			
			
		}
		catch (Exception e) 
		{
			// TODO: error handling
			e.printStackTrace();
		}
	}
		
	}
	


