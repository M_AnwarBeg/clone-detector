package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;



import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.Bullet;
import org.eclipse.swt.custom.ST;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.GlyphMetrics;
import org.eclipse.swt.widgets.Display;

import antlr.auto.gen.csharp.CSharpParser.Boolean_literalContext;
import cmt.cddc.langtokenizer.InitJava;
import cmt.cddc.simpleclones.Tokens;
import cmt.cddc.simpleclones.TokensList;
import cmt.cvac.views.ui.swt.SourceCodeStyle;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.IToken;
public class CloneStyleProvider {

	/**
	 * Color used to highlight differences in a clone (note that gaps/type-3
	 * have an additional highlighting).
	 */
	private static final Color CLONE_DIFF_COLOR = new Color(null, 255, 186, 123);

	/** Color used to highlight differences in a clone's gap. */
	private static final Color CLONE_DIFF_COLOR_GAP = new Color(null, 204, 160,
			120);
	public static ArrayList<java.awt.Color> cloneColors = new ArrayList<java.awt.Color>();

	/** Color used for non-clone areas. */
	/* package */static final Color NONE_CLONE_COLOR = Display.getDefault().getSystemColor(SWT.COLOR_WHITE);

	/**
	 * Color used to highlight the background of the clone (unless it is
	 * difference or gap).
	 */
	/* package */static final Color CLONE_COLOR = new Color(null, 228, 243, 250);

	/** Sets the given text and formats it. */
	public static void setStyledText(String cloneData, StyledText text, int sl, int el, Set<IToken> unique, List<IToken> cloneToken) {
		

	
		SourceCodeStyle style = SourceCodeStyle.get(ELanguage.JAVA);

		text.setText(cloneData);
		text.setFont(JFaceResources.getTextFont());

		int cloneStart = sl;//converter.getOffset(sl + 1);
		int cloneEnd = el;//converter.getOffset(el + 2) - 1;
		

		//Set<Integer> gapLines = determineGapLines(cloneData);
		List<StyleRange> ranges = new ArrayList<StyleRange>();
		int line =text.getLineCount();
		
	
		
		text.setLineBackground(cloneStart, cloneEnd, CLONE_COLOR);
		StyleRange r =null;
		for(IToken token : cloneToken) {
			
			
			ranges.add(determineStyleRange(token, cloneStart, cloneEnd, unique, style, text));
			
			
		}
		text.setStyleRanges(ranges.toArray(new StyleRange[ranges.size()]));
		
		
		//text.setLineBackground(cloneStart, cloneEnd, CLONE_COLOR);
		addLineNumers(text);
	}
	
	

	

	private static StyleRange determineStyleRange(IToken token, int cloneStart, int cloneEnd, Set<IToken> unique, SourceCodeStyle style, StyledText txt) {
		// TODO Auto-generated method stub
		Color background = null;
		boolean inClone = token.getLineNumber() >= cloneStart && token.getLineNumber() <= (cloneStart+cloneEnd);
		if(unique.contains(token) && inClone) {
			
			background = CLONE_DIFF_COLOR;
		}else {
			background = txt.getLineBackground(token.getLineNumber());
		}
		
		/*determineBackgroundColor(token, cloneStart,
				cloneEnd, unique);*/
		
		
		return style.getStyleRange(token.getType(), token.getOffset(), token
				.getText().length(), background);
	}
	
	private static StyleRange determineStyleRange(IToken token, Boolean ckeckFlag, Set<IToken> unique, SourceCodeStyle style, StyledText txt) {
		// TODO Auto-generated method stub
		Color background = null;
		boolean inClone = ckeckFlag;//token.getLineNumber() >= cloneStart && token.getLineNumber() <= (cloneStart+cloneEnd);
		/*if(unique.contains(token) && inClone) {
			
			//background = CLONE_DIFF_COLOR;
		}else {
			
		}*/
		
		/*determineBackgroundColor(token, cloneStart,
				cloneEnd, unique);*/
		
		background = txt.getLineBackground(token.getLineNumber());
		return style.getStyleRange(token.getType(), token.getOffset(), token
				.getText().length(), background);
	}
	private static StyleRange determineStyleRange(IToken token, Boolean ckeckFlag, SourceCodeStyle style, StyledText txt) {
		// TODO Auto-generated method stub
		Color background = null;
		boolean inClone = ckeckFlag;//token.getLineNumber() >= cloneStart && token.getLineNumber() <= (cloneStart+cloneEnd);
		/*if(unique.contains(token) && inClone) {
			
			//background = CLONE_DIFF_COLOR;
		}else {
			
		}*/
		
		/*determineBackgroundColor(token, cloneStart,
				cloneEnd, unique);*/
		
		background = txt.getLineBackground(token.getLineNumber());
		return style.getStyleRange(token.getType(), token.getOffset(), token
				.getText().length(), background);
	}



	/** Adds line numbers to the styled text, which are rendered using bullets. */
	private static void addLineNumers(StyledText text) {
		// Calculates text width according to the graphic context using the
		// maximum amount of digits of the highest line number as width and adds
		// two extra chars for margins.
		int lineNumberDigits = 1 + (int) Math.log10(text.getLineCount());
		GC gc = new GC(text.getDisplay());
		int bulletWidth = (gc.getFontMetrics().getAverageCharWidth())
				* (lineNumberDigits + 2);
		gc.dispose();

		StyleRange bulletStyle = new StyleRange();
		bulletStyle.metrics = new GlyphMetrics(0, 0, bulletWidth);
		bulletStyle.foreground = text.getDisplay().getSystemColor(
				SWT.COLOR_TITLE_INACTIVE_FOREGROUND);

		for (int i = 0; i < text.getLineCount(); i++) {
			Bullet bullet = new Bullet(ST.BULLET_TEXT, bulletStyle);
			bullet.text = Integer.toString(i + 1);
			text.setLineBullet(i, 1, bullet);
		}
	}

	/** Calculates the style range for a single token. */
	/*private static StyleRange determineStyleRange(IToken token,String lineText, int lineNumber, int cloneStart,
			int cloneEnd, Set<IToken> unique, Set<Integer> gapLines,
			SourceCodeStyle style, int lineCount) {
		Color background = determineBackgroundColor(token,lineNumber, cloneStart,
				cloneEnd, unique, gapLines, lineCount);
		return style.getStyleRange(token.getType(), token.getOffset(), token
				.getText().length(), background);
	}*/

	/** Returns a set of gap positions relative to the file (not the clone). */
	/*private static Set<Integer> determineGapLines(CloneDataBase cloneData) {
		Set<Integer> gapLines = new HashSet<Integer>();
		int startLine = cloneData.getStartLineInElement();
		for (int gap : cloneData.getGapPositions()) {
			gapLines.add(gap + startLine);
		}
		return gapLines;
	}*/

	/** Determines the background color to be used when rendering a token. */
	private static Color determineBackgroundColor(IToken token, int cloneStart,
			int cloneEnd, Set<IToken> unique) {
		int end = token.getOffset() + token.getText().length();
		boolean inClone = token.getLineNumber() >= cloneStart && token.getLineNumber() <= cloneEnd;
		boolean inGap = false;

		if (inClone) {
			if (unique.contains(token)) {
				/*if (inGap) {
					return CLONE_DIFF_COLOR_GAP;
				}*/
				return CLONE_DIFF_COLOR;
			} 
			/*else if (!inGap) {*/
				return CLONE_COLOR;
			}
		
		return NONE_CLONE_COLOR;
	}
	
	

	public static void setStyledText(String cloneData, StyledText text, ArrayList<Integer> sl,
			ArrayList<Integer> el,  List<IToken> fileToken, ArrayList<java.awt.Color>backgroundColor) {
		// TODO Auto-generated method stub
		cloneColors = backgroundColor;
		SourceCodeStyle style = SourceCodeStyle.get(ELanguage.JAVA);

		text.setText(cloneData);
		text.setFont(JFaceResources.getTextFont());

		

		//Set<Integer> gapLines = determineGapLines(cloneData);
		List<StyleRange> ranges = new ArrayList<StyleRange>();
		
		
		StyleRange r =null;
	
			int line =text.getLineCount();
			/*for(int i=0; i<sl.size();i++) {
				//Color background = new Color(null, null, cloneColors.get(i).getRGB());/*(null, null, cloneColors.get(i).getRGB());*/
				//for(int j=i+1; j<sl.size();j++) {
		/*
		 * int red = cloneColors.get(i).getRed(); int blue =
		 * cloneColors.get(i).getBlue(); int green = cloneColors.get(i).getGreen(); int
		 * start = sl.get(i); int end = el.get(i); for(int j = 0; j < end; j++) { Color
		 * clr = text.getLineBackground(start);
		 * 
		 * if(clr==null) {
		 * 
		 * text.setLineBackground(start, 1, new Color(null, red, green, blue)); } else {
		 * text.setLineBackground(start, 1, CLONE_COLOR); } start++;
		 * 
		 * }
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * }
		 */
				
			
			
			
			//text.setLineBackground(cloneStart, cloneEnd, CLONE_COLOR);
			for(IToken token : fileToken) {
				Boolean chk = checkInClone(token, sl,el);
				
				ranges.add(determineStyleRange(token, chk, style, text));
				
				
			
		}
		
		text.setStyleRanges(ranges.toArray(new StyleRange[ranges.size()]));
		
		
		//text.setLineBackground(cloneStart, cloneEnd, CLONE_COLOR);
		addLineNumers(text);

	}
	public static void setStyledText(String cloneData, StyledText text, ArrayList<Integer> sl,
			ArrayList<Integer> el, Set<IToken> unique,  List<IToken> fileToken, ArrayList<java.awt.Color>backgroundColor) {
		// TODO Auto-generated method stub
		cloneColors = backgroundColor;
		SourceCodeStyle style = SourceCodeStyle.get(ELanguage.JAVA);

		text.setText(cloneData);
		text.setFont(JFaceResources.getTextFont());

		

		//Set<Integer> gapLines = determineGapLines(cloneData);
		List<StyleRange> ranges = new ArrayList<StyleRange>();
		
		
		StyleRange r =null;
	
			int line =text.getLineCount();
			for(int i=0; i<sl.size();i++) {
				//Color background = new Color(null, null, cloneColors.get(i).getRGB());/*(null, null, cloneColors.get(i).getRGB());
				//for(int j=i+1; j<sl.size();j++) {
		
		  int red = cloneColors.get(i).getRed(); 
		  int blue = cloneColors.get(i).getBlue(); 
		  int green = cloneColors.get(i).getGreen(); 
		  int
		  start = sl.get(i); 
		  int end = el.get(i); 
		  for(int j = 0; j < end; j++) { 
			  Color clr = text.getLineBackground(start);
		  
		  if(clr==null) {
		  
		  text.setLineBackground(start, 1, new Color(null, red, green, blue)); } 
		  else {
		  
			  text.setLineBackground(start, 1, CLONE_COLOR); 
			  } 
		  start++;
		  
		  }
		  
		  
		  
		  
		  
		  
		  }
		 
				
			
			
			
			//text.setLineBackground(cloneStart, cloneEnd, CLONE_COLOR);
			for(IToken token : fileToken) {
				Boolean chk = checkInClone(token, sl,el);
				
				ranges.add(determineStyleRange(token, chk, unique, style, text));
				
				
			
		}
		
		text.setStyleRanges(ranges.toArray(new StyleRange[ranges.size()]));
		
		
		//text.setLineBackground(cloneStart, cloneEnd, CLONE_COLOR);
		addLineNumers(text);

	}
	
	public static Boolean checkInClone(IToken token, ArrayList<Integer>StartLine,ArrayList<Integer>EndLine) {
		Boolean check = false;
		for(int i=0;i<StartLine.size();i++){
			if(token.getLineNumber() >= StartLine.get(i) && token.getLineNumber() <= (StartLine.get(i)+EndLine.get(i))) {
				check = true;
			}
			
		}
		
		return check;
	}

	/** Sets the line background of the styled text. */
	/*private static void setLineBackground(StyledText text, CloneDataBaseString cloneData, Set<Integer> gapLines) {
		text.setLineBackground(0, cloneData.getStartLineInElement(), NONE_CLONE_COLOR);
		text.setLineBackground(cloneData.getStartLineInElement(), cloneData.getLastLineInElement() - cloneData.getStartLineInElement() + 1, CLONE_COLOR);
		text.setLineBackground(cloneData.getLastLineInElement() + 1,
				text.getLineCount() - cloneData.getLastLineInElement() - 1,	NONE_CLONE_COLOR);

		for (int gapPos : gapLines) {
			text.setLineBackground(gapPos, 1, NONE_CLONE_COLOR);
		}
	}*/
}
