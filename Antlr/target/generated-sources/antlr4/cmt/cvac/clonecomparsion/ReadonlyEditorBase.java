package cmt.cvac.clonecomparsion;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.part.EditorPart;

public abstract class ReadonlyEditorBase extends EditorPart {

	/** {@inheritDoc} */
	@Override
	public void doSave(IProgressMonitor monitor) {
		// does nothing
	}

	/** {@inheritDoc} */
	@Override
	public void doSaveAs() {
		// does nothing
	}

	/** {@inheritDoc} */
	@Override
	public boolean isDirty() {
		return false;
	}

	/** {@inheritDoc} */
	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}
}