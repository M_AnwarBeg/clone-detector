package cmt.cvac.clonecomparsion;

import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.ScrollBar;

public class ClonePairScrollModel {

	/**
	 * The number of lines that are shown  before the clone instances when
	 * initially scrolling to the clone.
	 */
	private static final int NUMBER_OF_CONTEXT_LINES_BEFORE = 2;

	/** The left text widget. */
	private final StyledText leftText;

	/** The right text widget. */
	private final StyledText rightText;

	/** Left clone data. */
	private final String leftCloneData;

	/** Right clone data. */
	private final String rightCloneData;
	/**Right Start Line*/
	private final int rsl;
	/**Right End Line*/
	private final int rel;
	/**Left Start Line*/
	private final int lsl;
	/**Left End Line*/
	private final int lel;

	/** Setup synchronized scrolling of the two given {@link StyledText}s. */
	public static void setupScrollModel(StyledText leftText,StyledText rightText, String leftCloneData,	String rightCloneData, int rightStartLine, int rightendLine, int leftStartLine, int leftEndLine) {
		ClonePairScrollModel model = new ClonePairScrollModel(leftText,
				rightText, leftCloneData, rightCloneData, rightStartLine, rightendLine, leftStartLine, leftEndLine);
		model.setupListeners();
		model.jumpToClone(leftStartLine, rightStartLine);
	}
	
	/** Scrolls both StyledTexts to the clone. */
	private void jumpToClone(int lsl, int rsl) {
		int leftTopLine = lsl
				- NUMBER_OF_CONTEXT_LINES_BEFORE;
		leftText.setTopPixel(leftTopLine * leftText.getLineHeight());
		int rightTopLine = rsl
				- NUMBER_OF_CONTEXT_LINES_BEFORE;
		rightText.setTopPixel(rightTopLine * rightText.getLineHeight());
	}

	/** Constructor. */
	private ClonePairScrollModel(StyledText leftText, StyledText rightText,
			String leftCloneData, String rightCloneData, int rightStartLine, int rightendLine, int leftStartLine, int leftEndLine) {
		this.leftText = leftText;
		this.rightText = rightText;
		this.leftCloneData = leftCloneData;
		this.rightCloneData = rightCloneData;
		this.rsl = rightStartLine;
		this.rel = rightendLine;
		this.lsl = leftStartLine;
		this.lel = leftEndLine;
	}

	/** Setup needed listeners. */
	private void setupListeners() {
		setupSynchronizedScrollAdapters(leftText, leftCloneData, rightText,
				rightCloneData,rsl,rel,lsl,lel);
		setupSynchronizedScrollAdapters(rightText, rightCloneData, leftText,
				leftCloneData,rsl,rel,lsl,lel);
	}

	/**
	 * Sets up the scroll adapters for horizontal and vertical synchronization
	 * between <code>sourceText</code> and <code>targetText</code>.
	 */
	private void setupSynchronizedScrollAdapters(StyledText sourceText,
			String sourceClone, StyledText targetText,
			String targetClone, int rightStartLine, int rightendLine, int leftStartLine, int leftEndLine) {
		sourceText.getVerticalBar().addSelectionListener(
				new SynchronizedVerticalScrollAdapter(sourceText, sourceClone,
						targetText, targetClone, rightStartLine, rightendLine,leftStartLine,leftEndLine));
		sourceText.getHorizontalBar().addSelectionListener(
				new SynchronizedHorizontalScrollAdapter(targetText));
	}
}

	class SynchronizedVerticalScrollAdapter extends
	SelectionAdapter {
	
		/** The StyledText that this adapter belongs to (the source of events). */
		private final StyledText sourceText;
		/** The StyledText that should be scrolled synchronous. */
		private final StyledText targetText;
		/** The clone data belonging to the source text. */
		private final String sourceClone;
		/** The clone data belonging to the target text. */
		private final String targetClone;
		private final int sourceStartLine;
		private final int sourceEndLine;
		private final int targetStartLine;
		private final int targetEndLine;
		
		/** Constructor. */
		public SynchronizedVerticalScrollAdapter(StyledText sourceText,
				String sourceClone, StyledText targetText,
				String targetClone, int rightStartLine, int rightendLine, int leftStartLine, int leftEndLine) {
		this.sourceText = sourceText;
		this.sourceClone = sourceClone;
		this.targetText = targetText;
		this.targetClone = targetClone;
		this.sourceStartLine = leftStartLine;
		this.sourceEndLine = leftEndLine;
		this.targetStartLine = rightStartLine;
		this.targetEndLine = rightendLine;
		
		}
		
		/**
		* {@inheritDoc}
		* <p>
		* Scrolls the target component so that both clone instances are moved
		* along each other.
		*/
		public void widgetSelected(SelectionEvent event) {
		float sourcePosition = sourceText.getTopPixel()
				+ sourceText.getSize().y / 2f;
		float sourceMiddleLine = sourcePosition
				/ sourceText.getLineHeight();
		
		float targetMiddleLine = determineTargetMiddleLine(sourceMiddleLine);
		float targetPosition = targetMiddleLine	* targetText.getLineHeight();
		
		targetText.setTopPixel((int) targetPosition
				- targetText.getSize().y / 2);
		}
		
		/**
		* Determines the line that should be in the vertical middle of the
		* target component if the given line is in the middle of the source
		* component.
		*/
		public float determineTargetMiddleLine(float sourceMiddleLine) {
		int sourceTop = sourceStartLine;
		int sourceBottom = sourceEndLine;
		int targetTop = targetStartLine;
		int targetBottom = targetEndLine;
		
		int sourceRange = sourceBottom - sourceTop;
		int targetRange = targetBottom - targetTop;
		
		return targetTop + (sourceMiddleLine - sourceTop) * targetRange
				/ sourceRange;
		}
	}


	class SynchronizedHorizontalScrollAdapter extends SelectionAdapter {
	
		/** The other StyledText widget. */
		private final StyledText targetText;
	
		/** The width of the text in the other StyledText in pixels. */
		private final int targetTextWidthPixels;
	
		/**
		 * Constructor.
		 * 
		 * @param target
		 *            The StyledText that should be scrolled in sync.
		 */
		public SynchronizedHorizontalScrollAdapter(StyledText target) {
			targetText = target;
			//targetTextWidthPixels = SWTUtils.getTextWidthInPixels(target);
			targetTextWidthPixels=1;
		}
	
		/** {@inheritDoc} */
		@Override
		public void widgetSelected(SelectionEvent e) {
			ScrollBar scrollBar = (ScrollBar) e.getSource();
			float relativePosition = (float) scrollBar.getSelection()
					/ (scrollBar.getMaximum() - scrollBar.getMinimum());
			targetText.setHorizontalPixel(Math.round(relativePosition
					* targetTextWidthPixels));
		}
	}
