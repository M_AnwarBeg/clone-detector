package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import eu.cqse.check.framework.scanner.IToken;

public class FileCloneCompareInput implements IEditorInput{
	
	HashMap<Integer, ArrayList<SimpleCloneInstance>> fileToSimpleCloneMapping;
	List<IToken> leftCloneToken;
	List<IToken> rightCloneToken;
	String file01; 
	String file02;
	String cloneData01;
	String cloneData02;
	ArrayList<SimpleClone> sccList;

	
	public FileCloneCompareInput(String codeSegment01, String codeSegment02, HashMap<Integer, ArrayList<SimpleCloneInstance>>maping, 
			List<IToken> leftCloneToken, List<IToken> rightCloneToken, String file01, String file02, ArrayList<SimpleClone> RequiresSCC) {
		this.file01 = file01;
		this.file02 = file02;
		this.fileToSimpleCloneMapping = maping;
		this.leftCloneToken = leftCloneToken;
		this.rightCloneToken = rightCloneToken;
		this.cloneData01 = codeSegment01;
		this.cloneData02 = codeSegment02;
		this.sccList = RequiresSCC;
	}
	@Override
	public <T> T getAdapter(Class<T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return null;
	}

}
