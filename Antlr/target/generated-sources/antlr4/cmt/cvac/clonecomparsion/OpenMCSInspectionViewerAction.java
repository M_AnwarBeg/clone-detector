package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ILenientScanner;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerFactory;
import eu.cqse.check.framework.scanner.ScannerUtils;

public class OpenMCSInspectionViewerAction extends Action{
	HashMap<Integer,ArrayList<MethodCloneInstance>> fileToMethodMapping;
	ArrayList<MethodClones> requiredMCCs;

	public OpenMCSInspectionViewerAction(HashMap<Integer,ArrayList<MethodCloneInstance>> fileToMethodMapping,
	ArrayList<MethodClones> requiredMCCs) {
		// TODO Auto-generated constructor stub
		this.fileToMethodMapping = fileToMethodMapping;
		this.requiredMCCs = requiredMCCs;
	}
	@Override
	public void run() 
	{
		
			try 
			{
				int a = (int) fileToMethodMapping.keySet().toArray()[0];
				sFile File_clone1 = sFile_List.getFile(a);
				int b = (int) fileToMethodMapping.keySet().toArray()[1];
				
				sFile File_clone2 = sFile_List.getFile(b);
				String file01 = ProjectInfo.getFileName(File_clone1.getFileID());
				String file02 = ProjectInfo.getFileName(File_clone2.getFileID());
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				ILenientScanner scanner02 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())), ProjectInfo.getInputFile(File_clone2.getFileID()));
				
				List<IToken> leftCloneToken = ScannerUtils.readTokens(scanner01);
				List<IToken> rightCloneToken = ScannerUtils.readTokens(scanner02);
				if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()!=null) {
					if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle().equals("Method Clone Structure Editor")) {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor(), true);
					}
					else {
						IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
						for(int i=0; i< editorlist.length;i++) {
							if(editorlist[i].getTitle().equals("Method Clone Structure Editor")) {
								PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(editorlist[i], false);
								break;
							}
						}
					}
				}
				
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
						new MCSCompareInput(
								FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())),
								FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())),
								fileToMethodMapping,
								/*clone1.getMethod().getStartToken()-1, 
								(clone1.getMethod().getEndToken() - clone1.getMethod().getStartToken())+1, 
								clone2.getMethod().getStartToken()-1, 
								(clone2.getMethod().getEndToken() - clone2.getMethod().getStartToken())+1,*/
								leftCloneToken, rightCloneToken, file01, file02),
								MCSPairEditor.ID);
			
				
				
								
				
				
				
			}
			catch (Exception e) 
			{
				// TODO: error handling
				e.printStackTrace();
			}
		}
	

}
