package cmt.cvac.clonecomparsion;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.conqat.lib.commons.algo.Diff;
import org.conqat.lib.commons.algo.Diff.Delta;
import org.conqat.lib.commons.collections.IdentityHashSet;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
//import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.dialogs.NewFolderDialog;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import cmt.cvac.viewobjects.SccList;
import cmt.cvac.views.ui.swt.FormLayoutUtils;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ILenientScanner;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerFactory;
import eu.cqse.check.framework.scanner.ScannerUtils;
import eu.cqse.check.framework.scanner.TokenEquator;

public class FileClonePairEditor extends ReadonlyEditorBase {
	
	public static final String ID = FileClonePairEditor.class.getName();
	private static String leftCloneData;
	private static String rightCloneData;
	private static Composite container;
	private static Composite composite01;
	private static Composite composite03;
	private static Composite composite02;
	public static Boolean leftRightFlag = false;
	
	/** Left text widget. */
	private static StyledText leftText;

	/** Right text widget. */
	private static StyledText rightText;
	private Button rightNext;
	private Button rightPrev;
	private Button leftNext;
	private Button leftPrev;
	private static Label rightFileNameLable;
	private static Label leftFileNameLable;
	static int currentLeftFileId;
    static int currentRightFileId;
    private Combo comboDropDown;
    
    public static ArrayList<Color> colorList;
    
    private static ArrayList<SimpleCloneInstance> instanceList;
    
    static CloneBarPainter leftBar;
    
    static CloneBarPainter rightBar;
    static CloneCompareIndicator compareIndicator;
	
	private static ArrayList<Integer> LstartL;
	private static ArrayList<Integer> LendL;
	
	private static ArrayList<Integer> RstartL;
	private static ArrayList<Integer> RendL;
	
	static List<IToken> leftFileToken;
	static List<IToken> rightFileToken;
	static ArrayList<List<IToken>> leftCloneToken;
	static ArrayList<List<IToken>> rightCloneToken;
	static org.eclipse.swt.graphics.Color clr = new org.eclipse.swt.graphics.Color(null, 228, 243, 250);
	
	String leftFileName;
	String rightFileName;
	private static Set<IToken> uniqueTokens;
	static HashMap<Integer,ArrayList<SimpleCloneInstance>> fileToSimpleCloneMapping;
	ArrayList<SimpleClone> RequiredScc;
	static ArrayList<SimpleCloneInstance>SCI01;
	static ArrayList<SimpleCloneInstance>SCI02;
	static ArrayList <Integer> FID = new ArrayList<Integer>();
	
	private void getNext(int currentFileId, boolean flag) {
		// TODO Auto-generated method stub
		if(flag) {
int index = FID.indexOf(currentLeftFileId);
			
			
			if((index-1)>=0 && FID.get((index-1)) != currentRightFileId) {
				currentLeftFileId = FID.get((index-1));
			}
			else {
				if((index-2)>=0 && FID.get((index-2)) != currentRightFileId) {
					currentLeftFileId = FID.get((index-2));
				}
				else if(currentRightFileId != FID.get((FID.size()-1))) {
					currentLeftFileId = FID.get((FID.size()-1));
				}
				else {
					currentLeftFileId = FID.get((FID.size()-2));
				}
			}
			try {
				leftText.dispose();
				rightText.dispose();
				//comboDropDown.dispose();
				
				/*
				 * comboDropDown =new Combo(composite03, SWT.BORDER | SWT.DROP_DOWN| SWT.FILL);
				 * comboDropDown.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true) );
				 * FormData comboData = new FormData(); comboData.top = new FormAttachment(0);
				 * comboData.left = new FormAttachment(0,5); comboData.right = new
				 * FormAttachment(100,-5); comboDropDown.setLayoutData(comboData);
				 * comboDropDown.add("Select Clone to View"); for(SimpleClone Scc: RequiredScc)
				 * { //String a = Scc.getSSCId().toString();
				 * comboDropDown.add(String.valueOf(Scc.getSSCId())); }
				 * comboDropDown.setVisibleItemCount(5);
				 */
				comboDropDown.select(0);
				
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				rightText.setText(rightCloneData);
				
				sFile File_clone1 = sFile_List.getFile(currentLeftFileId);
				leftFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				leftCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				leftText.setText(leftCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				leftFileToken = ScannerUtils.readTokens(scanner01);
				SCI01 = fileToSimpleCloneMapping.get(currentLeftFileId);
				LstartL = getStartLine(SCI01);
				LendL = getEndLine(SCI01);
				leftCloneToken = getCloneToken(leftFileToken, LstartL, LendL);
				if(LstartL.size()>RstartL.size()) {
				
					colorList = CloneColor.getColors(LstartL.size());
				}
				else {
					colorList = CloneColor.getColors(RstartL.size());
				}
				
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, leftFileToken, colorList);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, rightFileToken, colorList);
				
				composite01.redraw();
				composite02.redraw();
				composite03.redraw();
				
				composite01.layout(true, true);
				composite02.layout(true, true);
				composite03.layout(true, true);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			
		}
		else {
int index = FID.indexOf(currentRightFileId);
			
			
			if((index-1)>=0 && FID.get((index-1)) != currentLeftFileId) {
				currentRightFileId = FID.get((index-1));
			}
			else {
				if((index-2)>=0 && FID.get((index-2)) != currentLeftFileId) {
					currentRightFileId = FID.get((index-2));
				}
				else if(currentLeftFileId != FID.get((FID.size()-1))) {
					currentRightFileId = FID.get((FID.size()-1));
				}
				else {
					currentRightFileId = FID.get((FID.size()-2));
				}
			}
			try {
				leftText.dispose();
				rightText.dispose();
				//comboDropDown.dispose();
				
				/*
				 * comboDropDown =new Combo(composite03, SWT.BORDER | SWT.DROP_DOWN| SWT.FILL);
				 * comboDropDown.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true) );
				 * FormData comboData = new FormData(); comboData.top = new FormAttachment(0);
				 * comboData.left = new FormAttachment(0,5); comboData.right = new
				 * FormAttachment(100,-5); comboDropDown.setLayoutData(comboData);
				 * comboDropDown.add("Select Clone to View"); for(SimpleClone Scc: RequiredScc)
				 * { //String a = Scc.getSSCId().toString();
				 * comboDropDown.add(String.valueOf(Scc.getSSCId())); }
				 * comboDropDown.setVisibleItemCount(5);
				 */
				comboDropDown.select(0);
				
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				leftText.setText(leftCloneData);
				
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				
				sFile File_clone1 = sFile_List.getFile(currentRightFileId);
				rightFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				rightCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				rightText.setText(leftCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				rightFileToken = ScannerUtils.readTokens(scanner01);
				SCI02 = fileToSimpleCloneMapping.get(currentRightFileId);
				RstartL = getStartLine(SCI02);
				RendL = getEndLine(SCI02);
				rightCloneToken = getCloneToken(rightFileToken, RstartL, RendL);
				//uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				
				if(LstartL.size()>RstartL.size()) {
					
					colorList = CloneColor.getColors(LstartL.size());
				}
				else {
					colorList = CloneColor.getColors(RstartL.size());
				}
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, leftFileToken, colorList);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, rightFileToken, colorList);
				
				composite01.redraw();
				composite02.redraw();
				composite03.redraw();
				
				composite01.layout(true, true);
				composite02.layout(true, true);
				composite03.layout(true,true);
			}catch (Exception ex) {
				ex.printStackTrace();
			}
			
			
		}
		
	}
	protected void getPrev(int currentFileId, boolean flag) {
		// TODO Auto-generated method stub
		if(flag) {
			int index = FID.indexOf(currentLeftFileId);
			
			
			if((index+1)< FID.size() && FID.get((index+1))!= currentRightFileId) {
				currentLeftFileId = FID.get((index+1));
			}
			else {
				if((index+2)<FID.size() && FID.get((index+2))!= currentRightFileId) {
					currentLeftFileId = FID.get((index+2));
					
				}
				else if(currentRightFileId != FID.get(0)) {
					currentLeftFileId = FID.get(0);
					
				}
				else {
					currentLeftFileId = FID.get(1);
				}
			}
			
			try {
				leftText.dispose();
				rightText.dispose();
				//comboDropDown.dispose();
				
				/*comboDropDown =new Combo(composite03, SWT.BORDER | SWT.DROP_DOWN| SWT.FILL);
				
				 * comboDropDown.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true) );
				 * FormData comboData = new FormData(); comboData.top = new FormAttachment(0);
				 * comboData.left = new FormAttachment(0,5); comboData.right = new
				 * FormAttachment(100,-5); comboDropDown.setLayoutData(comboData);
				 * comboDropDown.add("Select Clone to View"); for(SimpleClone Scc: RequiredScc)
				 * { //String a = Scc.getSSCId().toString();
				 * comboDropDown.add(String.valueOf(Scc.getSSCId())); }
				 * comboDropDown.setVisibleItemCount(5);
				 */
				comboDropDown.select(0);
				
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				rightText.setText(rightCloneData);
				
				
				sFile File_clone1 = sFile_List.getFile(currentLeftFileId);
				leftFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				leftCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				leftText.setText(leftCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				leftFileToken = ScannerUtils.readTokens(scanner01);
				SCI01 = fileToSimpleCloneMapping.get(currentLeftFileId);
				LstartL = getStartLine(SCI01);
				LendL = getEndLine(SCI01);
				leftCloneToken = getCloneToken(leftFileToken, LstartL, LendL);
				//uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				if(LstartL.size()>RstartL.size()) {
					
					colorList = CloneColor.getColors(LstartL.size());
				}
				else {
					colorList = CloneColor.getColors(RstartL.size());
				}
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, leftFileToken, colorList);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, rightFileToken, colorList);
				
				
				composite01.redraw();
				composite02.redraw();
				composite03.redraw();
				
				composite01.layout(true, true);
				composite02.layout(true, true);
				composite03.layout(true, true);
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		else {
			int index = FID.indexOf(currentRightFileId);
			
			
			if((index+1)< FID.size() && FID.get((index+1))!= currentLeftFileId) {
				currentRightFileId = FID.get((index+1));
			}
			else {
				if((index+2)<FID.size() && FID.get((index+2))!= currentLeftFileId) {
					currentRightFileId = FID.get((index+2));
					
				}
				else if(currentLeftFileId != FID.get(0)) {
					currentRightFileId = FID.get(0);
					
				}
				else {
					currentRightFileId = FID.get(1);
				}
			}
			try {
				leftText.dispose();
				rightText.dispose();
				//comboDropDown.dispose();
				
				/*
				 * comboDropDown =new Combo(composite03, SWT.BORDER | SWT.DROP_DOWN| SWT.FILL);
				 * comboDropDown.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true) );
				 * FormData comboData = new FormData(); comboData.top = new FormAttachment(0);
				 * comboData.left = new FormAttachment(0,5); comboData.right = new
				 * FormAttachment(100,-5); comboDropDown.setLayoutData(comboData);
				 * comboDropDown.add("Select Clone to View"); for(SimpleClone Scc: RequiredScc)
				 * { //String a = Scc.getSSCId().toString();
				 * comboDropDown.add(String.valueOf(Scc.getSSCId())); }
				 * comboDropDown.setVisibleItemCount(5);
				 */
				comboDropDown.select(0);
				
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				leftText.setText(leftCloneData);
				
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				
				sFile File_clone1 = sFile_List.getFile(currentRightFileId);
				rightFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				rightCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				rightText.setText(leftCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				rightFileToken = ScannerUtils.readTokens(scanner01);
				SCI02 = fileToSimpleCloneMapping.get(currentRightFileId);
				RstartL = getStartLine(SCI02);
				RendL = getEndLine(SCI02);
				rightCloneToken = getCloneToken(rightFileToken, RstartL, RendL);
				//uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				if(LstartL.size()>RstartL.size()) {
					
					colorList = CloneColor.getColors(LstartL.size());
				}
				else {
					colorList = CloneColor.getColors(RstartL.size());
				}
				
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, leftFileToken, colorList);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, rightFileToken, colorList);
				
				composite01.redraw();
				composite02.redraw();
				composite03.redraw();
				
				composite01.layout(true, true);
				composite02.layout(true, true);
				composite03.layout(true, true);
				
			}catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		
		
	}
	
	public static ArrayList<Integer> getStartLine(ArrayList<SimpleCloneInstance>simpleCloneInstances){
		ArrayList<Integer> startIndex = new ArrayList<Integer>();
		for (SimpleCloneInstance scc : simpleCloneInstances) {
			startIndex.add((scc.getStartingIndex()-1));
			
		}
		
		return startIndex;
	}
	
	public static ArrayList<Integer> getEndLine(ArrayList<SimpleCloneInstance>simpleCloneInstances){
		ArrayList<Integer> endIndex = new ArrayList<Integer>();
		for (SimpleCloneInstance scc : simpleCloneInstances) {
			//scc.get
			endIndex.add((scc.getEndingIndex() - scc.getStartingIndex())+1);
			
		}
		return endIndex;
	}
	
	
	
	

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		// TODO Auto-generated method stub
		setInput(input);
		setSite(site);
		leftFileName = ((FileCloneCompareInput) input).file01;
		rightFileName = ((FileCloneCompareInput) input).file02;
		leftCloneData = ((FileCloneCompareInput) input).cloneData01;
		rightCloneData = ((FileCloneCompareInput) input).cloneData02;
		leftFileToken = ((FileCloneCompareInput) input).leftCloneToken;
		rightFileToken = ((FileCloneCompareInput) input).rightCloneToken;
		fileToSimpleCloneMapping = ((FileCloneCompareInput) input).fileToSimpleCloneMapping;
		int a = (int) fileToSimpleCloneMapping.keySet().toArray()[0];
		int b = (int) fileToSimpleCloneMapping.keySet().toArray()[1];
		SCI01 = fileToSimpleCloneMapping.get(a);
		SCI02 = fileToSimpleCloneMapping.get(b);
		LstartL = getStartLine(SCI01);
		LendL = getEndLine(SCI01);
		RstartL = getStartLine(SCI02);
		RendL = getEndLine(SCI02);
		FID.clear();
		for(int i =0; i < fileToSimpleCloneMapping.keySet().size(); i++) {
			int ab = (int) fileToSimpleCloneMapping.keySet().toArray()[i];
			FID.add(ab);
			
		}
		currentLeftFileId = FID.get(0);
		currentRightFileId = FID.get(1);
		
		leftCloneToken = getCloneToken(leftFileToken, LstartL, LendL);
		rightCloneToken = getCloneToken(rightFileToken, RstartL, RendL);
		//uniqueTokens = computeUniqueCloneTokens(leftCloneToken, rightCloneToken);
		RequiredScc = ((FileCloneCompareInput) input).sccList;
	}
	private static ArrayList<List<IToken>> getCloneToken(List<IToken>tokenList, ArrayList<Integer> start, ArrayList<Integer> end){
		List<IToken> cloneToken = new ArrayList<IToken>();
		ArrayList<List<IToken>> cloneTokenList = new ArrayList<List<IToken>>(); 
		for (int i=0; i<start.size();i++) {
			int startline = start.get(i)-1;
			int endline = end.get(i);
			for(IToken token: tokenList) {
				
				boolean inClone = token.getLineNumber() >= startline && token.getLineNumber() <= ((endline- startline)+1);
				if(inClone) {
					cloneToken.add(token);
				}
				
			}
			cloneTokenList.add(cloneToken);
			
		}
		
		return cloneTokenList;
	}
	
	/*
	 * private static Set<IToken> computeUniqueCloneTokens(ArrayList<List<IToken>>
	 * leftCloneTokens, ArrayList<List<IToken>> rightCloneTokens) { Set<IToken>
	 * unique = new IdentityHashSet<IToken>(); for(int i=0;
	 * i<leftCloneTokens.size(); i++) { Delta<IToken> delta =
	 * Diff.computeDelta(leftCloneTokens.get(i), rightCloneTokens.get(i),
	 * TokenEquator.INSTANCE);
	 * 
	 * for (int j = 0; j < delta.getSize(); ++j) { unique.add(delta.getT(j)); }
	 * 
	 * 
	 * }
	 * 
	 * return unique; }
	 */
	

	@Override
	public void createPartControl(Composite root) {
		// TODO Auto-generated method stub
		root.setLayout(new FillLayout());
		

		
		container = new Composite(root, SWT.NONE);
		container.setLayout(new GridLayout(2, true));
		
		
		  composite03 = new Composite(container, SWT.None); 
		  composite03.setLayout(new FormLayout() );
		  composite03.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
		
		comboDropDown =new Combo(composite03, SWT.BORDER | SWT.DROP_DOWN| SWT.FILL);
		comboDropDown.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true) ); 
		FormData comboData = new FormData();
		comboData.top = new FormAttachment(0);
		comboData.left = new FormAttachment(0,5);
		comboData.right = new FormAttachment(100,-5);
		comboDropDown.setLayoutData(comboData);
		comboDropDown.add("Select Clone to View");
		for(SimpleClone Scc: RequiredScc) {
			//String a = Scc.getSSCId().toString();
			comboDropDown.add(String.valueOf(Scc.getSSCId()));
		}
		comboDropDown.setVisibleItemCount(5);
		comboDropDown.select(0);
		
		composite01 = new Composite(container, SWT.BORDER);
		composite01.setLayout(new FormLayout());
		composite01.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		
		leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		leftFileNameLable = new Label(composite01, SWT.None);
		leftFileNameLable.setText(leftFileName);
		
		leftNext = new Button(composite01, SWT.ARROW|SWT.RIGHT);
		leftNext.setToolTipText("Next Instance");
		
		leftPrev = new Button(composite01, SWT.ARROW|SWT.LEFT);
		leftPrev.setToolTipText("Previous Instance");
		
		 FormData lablData = new FormData();
			lablData.top = new FormAttachment(0);
			lablData.left = new FormAttachment(0, 5);
			lablData.width = 250;
			leftFileNameLable.setLayoutData(lablData);
		
		
			FormData pervData = new FormData();
			pervData.right = new FormAttachment(leftNext,-5);
			pervData.top = new FormAttachment(0);
			leftPrev.setLayoutData(pervData);
		
			FormData nextData = new FormData();
			nextData.top = new FormAttachment(0);
			nextData.right = new FormAttachment(100, -5);
			leftNext.setLayoutData(nextData);
		
		leftText.setText(leftCloneData);
		
		FormData textData = new FormData();
		textData.top = new FormAttachment(leftFileNameLable,5);
		textData.left = new FormAttachment(0, 5);
		textData.right = new FormAttachment(100,-5);
		textData.bottom = new FormAttachment(100,-5);
		leftText.setLayoutData(textData);
	
		if(LstartL.size()>RstartL.size()) {
			
			colorList = CloneColor.getColors(LstartL.size());
		}
		else {
			colorList = CloneColor.getColors(RstartL.size());
		}
		CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, leftFileToken, colorList);
		
		//compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData);
		
		composite02 = new Composite(container, SWT.BORDER);
		composite02.setLayout(new FormLayout());
		composite02.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		/*
		 * FormData com2Data = new FormData(); com2Data.top = new
		 * FormAttachment(composite03); composite02.setLayoutData(com2Data);
		 */
		
		rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		rightFileNameLable = new Label(composite02, SWT.None);
		FormData rightLablData = new FormData();
		rightLablData.top = new FormAttachment(0);
		rightLablData.left = new FormAttachment(0, 5);
		rightLablData.width = 250;
		rightFileNameLable.setLayoutData(rightLablData);
		
		rightNext = new Button(composite02, SWT.ARROW|SWT.RIGHT);
		rightPrev = new Button(composite02, SWT.ARROW|SWT.LEFT);
		
		FormData rightPervData = new FormData();
		rightPervData.right = new FormAttachment(rightNext,-5);
		rightPervData.top = new FormAttachment(0);
		rightPrev.setLayoutData(rightPervData);
		
		
		FormData rightNextData = new FormData();
		rightNextData.top = new FormAttachment(0);
		//nextData.left = new FormAttachment(btnPrevious, 5);
		rightNextData.right = new FormAttachment(100, -5);
		rightNext.setLayoutData(rightNextData);
		
		rightFileNameLable.setText(rightFileName);
		
		rightNext.setToolTipText("Next Instance");
		//GridDataFactory.fillDefaults().align(10, 0).grab(false, false).applyTo(fileName);
		rightPrev.setToolTipText("Previous Instance");
		
		/*for(int i =0;i<RstartL.size();i++) {
			
			rightBar = new CloneBarPainter(rightCloneData, rightText , RstartL.get(i) , RendL.get(i));
			rightText.addPaintListener(rightBar);
		}*/
		
		rightText.setText(rightCloneData);
		FormData rightTextData = new FormData();
		rightTextData.top = new FormAttachment(rightFileNameLable,5);
		rightTextData.left = new FormAttachment(0, 5);
		rightTextData.right = new FormAttachment(100,-5);
		rightTextData.bottom = new FormAttachment(100,-5);
		rightText.setLayoutData(rightTextData);
		
		
		CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL,RendL, rightFileToken,colorList);
		/*ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData,
				  rightCloneData, RstartL.get(0), RendL.get(0), LstartL.get(0), LendL.get(0));
		
		 * FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(),
		 * compareIndicator, rightText.getParent(),5);
		 * compareIndicator.setLeftText(leftText);
		 * compareIndicator.setRightText(rightText);
		 * 
		 * 
		 * setupRedrawListeners(compareIndicator);
		 */		
		leftNext.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//compareIndicator.dispose();
				getNext(currentLeftFileId, true);
				//compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData);
				//FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),5);
				
				
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			
		});
		leftPrev.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//compareIndicator.dispose();
				getPrev(currentLeftFileId, true);
				//compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData);
				//FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),5);
				//compareIndicator.setLeftText(leftText);
				//compareIndicator.setRightText(rightText);
				//ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL.get(0),
				//		RendL.get(0), LstartL.get(0), LendL.get(0));
				//setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		rightNext.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//compareIndicator.dispose();
				getNext(currentRightFileId, false);
				//compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData);
				//FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),5);
				//compareIndicator.setLeftText(leftText);
				//compareIndicator.setRightText(rightText);
				//ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL.get(0),
				//		RendL.get(0), LstartL.get(0), LendL.get(0));
				//setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		rightPrev.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//compareIndicator.dispose();
				getPrev(currentRightFileId, false);
				//compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData);
				//FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),5);
				
				/*
				 * ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData,
				 * rightCloneData, RstartL.get(0), RendL.get(0), LstartL.get(0), LendL.get(0));
				 *///setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		comboDropDown.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
				
				
			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				int index = comboDropDown.getSelectionIndex();
				String Item = comboDropDown.getItem(index);
				
				try {
					leftText.setLineBackground(0,leftText.getLineCount(),Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
					rightText.setLineBackground(0,rightText.getLineCount(),Display.getDefault().getSystemColor(SWT.COLOR_WHITE));
					
					for(SimpleClone scc : RequiredScc){
						
						if(String.valueOf(scc.getSSCId()).equals(Item)) {
							ArrayList<SimpleCloneInstance> instances= scc.getInstances();
							for(SimpleCloneInstance scci : instances) {
								int id = scci.getFileId();
								if(id==currentLeftFileId) {
									int start = scci.getStartingIndex()-1;
									int end = (scci.getEndingIndex()-start)+1;
									leftText.setLineBackground(start, end, clr);;
								}
								else if(id==currentRightFileId){
									int start = scci.getStartingIndex()-1;
									int end = (scci.getEndingIndex()-start)+1;
									rightText.setLineBackground(start, end, clr);
									
									
								}
							}
							
							
							
						}
						
					}
					
				}
				catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
				
			}
			
		});
		
		
		
		
		
		
	}
	

	private static void setupRedrawListeners(final Control compareIndicator) {
		leftText.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				compareIndicator.redraw();
			}
		});
		
		rightText.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				compareIndicator.redraw();
			}
		});

		leftText.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(final ControlEvent e) {
				compareIndicator.redraw();
			}
		});

		rightText.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				compareIndicator.redraw();
			}
		});
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}

}
