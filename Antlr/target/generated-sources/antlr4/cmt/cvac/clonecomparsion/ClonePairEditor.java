package cmt.cvac.clonecomparsion;

import org.eclipse.swt.widgets.Button;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
/*Classes to check*/
import cmt.cvac.clonecomparsion.CloneCompareIndicator;
import cmt.cvac.views.ui.swt.FormLayoutUtils;
import cmt.cvac.views.ui.swt.SWTUtils;

import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ILenientScanner;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerFactory;
import eu.cqse.check.framework.scanner.ScannerUtils;
import eu.cqse.check.framework.scanner.TokenEquator;

import org.conqat.lib.commons.algo.Diff;
import org.conqat.lib.commons.algo.Diff.Delta;
import org.conqat.lib.commons.collections.IdentityHashSet;
/*END*/
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.NewFolderDialog;

public class ClonePairEditor extends ReadonlyEditorBase{

	/** The ID of this editor. */
	public static final String ID = ClonePairEditor.class.getName();

	/** The Clone for the left side */
	private static String leftCloneData;

	/** The Clone for the right side */
	private static String rightCloneData;

	/** Container composite that holds the StyledTexts etc. */
	private Composite container;
	private static Composite composite01;
	private static Composite composite02;
	public static Boolean leftRightFlag = false;

	/** Left text widget. */
	private static StyledText leftText;

	/** Right text widget. */
	private static StyledText rightText;
	private Button rightNext;
	private Button rightPrev;
	private Button leftNext;
	private Button leftPrev;
	private static Label rightFileNameLable;
	private static Label leftFileNameLable;
	static int currentLeftInstanceId;
    static int currentRightInstanceId;
    private static ArrayList<MethodCloneInstance> instanceList;
    
    static CloneBarPainter leftBar;
    
    static CloneBarPainter rightBar;
	
	
	static CloneCompareIndicator compareIndicator;
	
	private static int LstartL;
	private static int LendL;
	
	private static int RstartL;
	private static int RendL;
	
	
	static List<IToken> leftFileToken;
	static List<IToken> rightFileToken;
	static List<IToken> leftCloneToken;
	static List<IToken> rightCloneToken;
	String leftFileName;
	String rightFileName;
	private static Set<IToken> uniqueTokens;
	
	@Override
	public void init(IEditorSite site, IEditorInput input) {
			setInput(input);
			setSite(site);

			leftCloneData = ((CloneCompareInput) input).leftCloneData;
			rightCloneData = ((CloneCompareInput) input).rightCloneData;
			
			LstartL = ((CloneCompareInput) input).LstartL;
			LendL = ((CloneCompareInput) input).LendL;
			RstartL = ((CloneCompareInput) input).RstartL;
			RendL = ((CloneCompareInput) input).RendL;
			leftFileToken = ((CloneCompareInput) input).leftCloneToken;
			rightFileToken = ((CloneCompareInput) input).rightCloneToken;
			leftCloneToken = getCloneToken(leftFileToken, LstartL, LendL);
			rightCloneToken = getCloneToken(rightFileToken, RstartL, RendL);
			uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
			leftFileName = ((CloneCompareInput) input).leftFileName;
			rightFileName = ((CloneCompareInput) input).rightFileName;
			currentLeftInstanceId = ((CloneCompareInput) input).leftInstanceId;
			currentRightInstanceId = ((CloneCompareInput) input).rightInstanceId;
			instanceList = ((CloneCompareInput) input).instancesList;
			
		}
	private static List<IToken> getCloneToken(List<IToken>tokenList, int startLine, int endline){
		List<IToken> cloneToken = new ArrayList<IToken>();
		for(IToken token: tokenList) {
			
			boolean inClone = token.getLineNumber() >= startLine && token.getLineNumber() <= ((startLine + endline)-1);
			if(inClone) {
				cloneToken.add(token);
			}
		}
		return cloneToken;
	}
	
	private static Set<IToken> computeUniqueCloneTokens(List<IToken> leftCloneTokens,
			List<IToken> rightCloneTokens) {
		Delta<IToken> delta = Diff.computeDelta(leftCloneTokens,
				rightCloneTokens, TokenEquator.INSTANCE);
		Set<IToken> unique = new IdentityHashSet<IToken>();
		for (int i = 0; i < delta.getSize(); ++i) {
			unique.add(delta.getT(i));
		}
		return unique;
	}
	public static void getNext(int id, Boolean flag) {
		if(flag) {
			if((id+1) != currentRightInstanceId&&(id+1)>instanceList.size()) {
				id++;
				currentLeftInstanceId = id;
				
			}
			else {
				
				if((id+2)<instanceList.size() && (id+2) != currentRightInstanceId) {
					id= id+2;
					currentLeftInstanceId = id;
				}
				else if(currentRightInstanceId != 0){
					id = 0;
					currentLeftInstanceId = id;
				}
				else {
					id = 1;
					currentLeftInstanceId = id;
				}
			}
			try {
				leftText.dispose();
				rightText.dispose();
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				rightText.setText(rightCloneData);
				rightBar = new CloneBarPainter(rightCloneData, rightText, RstartL, RendL);
				rightText.addPaintListener(rightBar);
				
				
				sFile File_clone1 = sFile_List.getFile(instanceList.get(currentLeftInstanceId).getMethod().getFileID());
				leftFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				leftCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				leftText.setText(leftCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				leftFileToken = ScannerUtils.readTokens(scanner01);
				LstartL = instanceList.get(currentLeftInstanceId).getMethod().getStartToken()-1;
				LendL = (instanceList.get(currentLeftInstanceId).getMethod().getEndToken() - instanceList.get(currentLeftInstanceId).getMethod().getStartToken())+1;
				leftCloneToken = getCloneToken(leftFileToken, LstartL, LendL);
				uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, uniqueTokens, leftFileToken);
				CloneStyleProvider.setStyledText(rightCloneData,rightText,RstartL, RendL,uniqueTokens,rightFileToken);
				leftBar = new CloneBarPainter(leftCloneData, leftText , LstartL , LendL);
				leftText.addPaintListener(leftBar);
				
				composite01.redraw();
				composite02.redraw();
				
				composite01.layout(true, true);
				composite02.layout(true, true);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			
		}
		else {
			if((id+1) != currentLeftInstanceId && (id+1)>instanceList.size()) {
				id++;
				currentRightInstanceId = id;
				
			}
			else {
				
				if((id+2)<instanceList.size() && (id+2) != currentLeftInstanceId) {
					id= id+2;
					currentRightInstanceId = id;
				}
				else if(currentLeftInstanceId != 0){
					id = 0;
					currentRightInstanceId = id;
				}
				else {
					id = 1;
					currentRightInstanceId = id;
				}
			}
			try {
				rightText.dispose();
				leftText.dispose();
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				
				sFile File_clone1 = sFile_List.getFile(instanceList.get(currentRightInstanceId).getMethod().getFileID());
				rightFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				rightCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				rightText.setText(rightCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				rightFileToken = ScannerUtils.readTokens(scanner01);
				RstartL = instanceList.get(currentRightInstanceId).getMethod().getStartToken()-1;
				RendL = (instanceList.get(currentRightInstanceId).getMethod().getEndToken() - instanceList.get(currentRightInstanceId).getMethod().getStartToken())+1;
				rightCloneToken = getCloneToken(rightFileToken, RstartL, RendL);
				uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, uniqueTokens, rightFileToken);
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, uniqueTokens, leftFileToken);
				rightBar = new CloneBarPainter(rightCloneData, rightText , RstartL , RendL);
				leftBar = new CloneBarPainter(leftCloneData, leftText, LstartL, LendL);
				rightText.addPaintListener(rightBar);
				leftText.addPaintListener(leftBar);
				
				composite02.redraw();
				composite01.redraw();
				
				composite02.layout(true, true);
				composite01.layout(true, true);
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	public static void getPrevious(int id, Boolean flag) {
		if(flag) {
			if((id-1) != currentRightInstanceId && (id-1)>0 ) {
				
				 id--;
					currentLeftInstanceId = id;
				
				
			}
			else {
				if((id-2)>=0 && (id-2) != currentRightInstanceId) {
					id= id-2;
					currentLeftInstanceId = id;
				}
				else if(currentRightInstanceId != (instanceList.size()-1)){
					id = instanceList.size()-1;
					currentLeftInstanceId = id;
				}
				else {
					id = instanceList.size()-2;
					currentLeftInstanceId = id;
				}
			}
			try {
				leftText.dispose();
				rightText.dispose();
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				
				sFile File_clone1 = sFile_List.getFile(instanceList.get(currentLeftInstanceId).getMethod().getFileID());
				leftFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				leftCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				leftText.setText(leftCloneData);
				
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				leftFileToken = ScannerUtils.readTokens(scanner01);
				LstartL = instanceList.get(currentLeftInstanceId).getMethod().getStartToken()-1;
				LendL = (instanceList.get(currentLeftInstanceId).getMethod().getEndToken() - instanceList.get(currentLeftInstanceId).getMethod().getStartToken())+1;
				leftCloneToken = getCloneToken(leftFileToken, LstartL, LendL);
				uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, uniqueTokens, leftFileToken);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, uniqueTokens, rightFileToken);
				leftBar = new CloneBarPainter(leftCloneData, leftText , LstartL , LendL);
				rightBar = new CloneBarPainter(rightCloneData, rightText, RstartL, RendL);
				leftText.addPaintListener(leftBar);
				rightText.addPaintListener(rightBar);
				
				composite01.redraw();
				composite02.redraw();
				
				composite01.layout(true, true);
				composite02.layout(true, true);
				
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
		else {
			if((id-1) != currentLeftInstanceId && (id-1)>0) {
				id--;
				currentRightInstanceId = id;
				
			}
			else {
				if((id-1) != currentLeftInstanceId && (id-2)>0) {
					id= id-2;
					currentRightInstanceId = id;
				}
				else if(currentLeftInstanceId != instanceList.size()-1){
					id = instanceList.size()-1;
					currentRightInstanceId =id;
				}
				else {
					id = instanceList.size()-2;
					currentRightInstanceId =id;
				}
			}
			try {
				rightText.dispose();
				leftText.dispose();
				leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData textData = new FormData();
				textData.top = new FormAttachment(leftFileNameLable,5);
				textData.left = new FormAttachment(0, 5);
				textData.right = new FormAttachment(100,-5);
				textData.bottom = new FormAttachment(100,-5);
				leftText.setLayoutData(textData);
				
				rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
				FormData rightTextData = new FormData();
				rightTextData.top = new FormAttachment(rightFileNameLable,5);
				rightTextData.left = new FormAttachment(0, 5);
				rightTextData.right = new FormAttachment(100,-5);
				rightTextData.bottom = new FormAttachment(100,-5);
				rightText.setLayoutData(rightTextData);
				
				sFile File_clone1 = sFile_List.getFile(instanceList.get(currentRightInstanceId).getMethod().getFileID());
				rightFileNameLable.setText(ProjectInfo.getFileName(File_clone1.getFileID()));
				rightCloneData = FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID()));
				rightText.setText(rightCloneData);
				ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
				
				rightFileToken = ScannerUtils.readTokens(scanner01);
				RstartL = instanceList.get(currentRightInstanceId).getMethod().getStartToken()-1;
				RendL = (instanceList.get(currentRightInstanceId).getMethod().getEndToken() - instanceList.get(currentRightInstanceId).getMethod().getStartToken())+1;
				rightCloneToken = getCloneToken(rightFileToken, RstartL, RendL);
				uniqueTokens = computeUniqueCloneTokens(leftCloneToken,	rightCloneToken);
				CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL, RendL, uniqueTokens, rightFileToken);
				CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, uniqueTokens, leftFileToken);
				rightBar = new CloneBarPainter(rightCloneData, rightText , RstartL , RendL);
				leftBar = new CloneBarPainter(leftCloneData, leftText, LstartL, LendL);
				rightText.addPaintListener(rightBar);
				leftText.addPaintListener(leftBar);
				composite02.redraw();
				composite01.redraw();
				
				
				composite02.layout(true, true);
				composite02.redraw();
				
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	
	
	@Override
	public void createPartControl(Composite root) {
		
		root.setLayout(new FillLayout());
		
		
		
		container = new Composite(root, SWT.NONE);
		container.setLayout(new FormLayout());
		
		composite01 = new Composite(container, SWT.NONE);
		composite01.setLayout(new FormLayout());
		//composite01.d

		
		leftText = new StyledText(composite01, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		//createCloneText(container, leftCloneData , LstartL, LendL, uniqueTokens, leftCloneToken, leftFileName, true, false);
		leftFileNameLable = new Label(composite01, SWT.None);
		leftFileNameLable.setText(leftFileName);
		
		leftNext = new Button(composite01, SWT.ARROW|SWT.RIGHT);
		leftNext.setToolTipText("Next Instance");
		
		leftPrev = new Button(composite01, SWT.ARROW|SWT.LEFT);
		leftPrev.setToolTipText("Previous Instance");
		
		FormData lablData = new FormData();
		lablData.top = new FormAttachment(0);
		lablData.left = new FormAttachment(0, 5);
		lablData.width = 250;
		leftFileNameLable.setLayoutData(lablData);
		
		
		FormData pervData = new FormData();
		pervData.right = new FormAttachment(leftNext,-5);
		pervData.top = new FormAttachment(0);
		leftPrev.setLayoutData(pervData);
		
		
		FormData nextData = new FormData();
		nextData.top = new FormAttachment(0);
		nextData.right = new FormAttachment(100, -5);
		leftNext.setLayoutData(nextData);
		leftBar = new CloneBarPainter(leftCloneData, leftText , LstartL , LendL);
		leftText.addPaintListener(leftBar);
		leftText.setText(leftCloneData);
		
		FormData textData = new FormData();
		textData.top = new FormAttachment(leftFileNameLable,5);
		textData.left = new FormAttachment(0, 5);
		textData.right = new FormAttachment(100,-5);
		textData.bottom = new FormAttachment(100,-5);
		leftText.setLayoutData(textData);
	
		//createLabel(composite, FileName);
		CloneStyleProvider.setStyledText(leftCloneData, leftText, LstartL, LendL, uniqueTokens, leftFileToken);
		//GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).applyTo(leftText);
		
		

		
		compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData, 
				LstartL, LendL, RstartL, RendL);
		composite02 = new Composite(container, SWT.NONE);
		composite02.setLayout(new FormLayout());
		
		rightText = new StyledText(composite02, SWT.READ_ONLY | SWT.MULTI | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		//createCloneText(container, rightCloneData, RstartL, RendL, uniqueTokens, rightCloneToken, rightFileName, false, true); 
		rightFileNameLable = new Label(composite02, SWT.None);
		FormData rightLablData = new FormData();
		rightLablData.top = new FormAttachment(0);
		rightLablData.left = new FormAttachment(0, 5);
		rightLablData.width = 250;
		rightFileNameLable.setLayoutData(rightLablData);
		
		rightNext = new Button(composite02, SWT.ARROW|SWT.RIGHT);
		rightPrev = new Button(composite02, SWT.ARROW|SWT.LEFT);
		
		FormData rightPervData = new FormData();
		rightPervData.right = new FormAttachment(rightNext,-5);
		rightPervData.top = new FormAttachment(0);
		rightPrev.setLayoutData(rightPervData);
		
		
		FormData rightNextData = new FormData();
		rightNextData.top = new FormAttachment(0);
		//nextData.left = new FormAttachment(btnPrevious, 5);
		rightNextData.right = new FormAttachment(100, -5);
		rightNext.setLayoutData(rightNextData);
		
		rightFileNameLable.setText(rightFileName);
		
		rightNext.setToolTipText("Next Instance");
		//GridDataFactory.fillDefaults().align(10, 0).grab(false, false).applyTo(fileName);
		rightPrev.setToolTipText("Previous Instance");
		
		
		rightBar = new CloneBarPainter(rightCloneData, rightText , RstartL , RendL);
		rightText.addPaintListener(rightBar);
		rightText.setText(rightCloneData);
		FormData rightTextData = new FormData();
		rightTextData.top = new FormAttachment(rightFileNameLable,5);
		rightTextData.left = new FormAttachment(0, 5);
		rightTextData.right = new FormAttachment(100,-5);
		rightTextData.bottom = new FormAttachment(100,-5);
		rightText.setLayoutData(rightTextData);
	
		CloneStyleProvider.setStyledText(rightCloneData, rightText, RstartL,RendL, uniqueTokens, rightFileToken);

		FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),20);
		compareIndicator.setLeftText(leftText);
		compareIndicator.setRightText(rightText);
		
		ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL, RendL, LstartL, LendL);

		setupRedrawListeners(compareIndicator);
		leftNext.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				compareIndicator.dispose();
				getNext(currentLeftInstanceId, true);
				compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData, 
						LstartL, LendL, RstartL, RendL);
				FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),20);
				compareIndicator.setLeftText(leftText);
				compareIndicator.setRightText(rightText);
				
				ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL, RendL, LstartL, LendL);

				setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
				
				
				
				
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		leftPrev.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//leftBar.removeListner();
				compareIndicator.dispose();
				getPrevious(currentLeftInstanceId, true);
				compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData, 
						LstartL, LendL, RstartL, RendL);
				FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),20);
				compareIndicator.setLeftText(leftText);
				compareIndicator.setRightText(rightText);
				
				ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL, RendL, LstartL, LendL);

				setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		rightNext.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				//rightBar.removeListner();
				compareIndicator.dispose();
				getNext(currentRightInstanceId, false);
				compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData, 
						LstartL, LendL, RstartL, RendL);
				FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),20);
				compareIndicator.setLeftText(leftText);
				compareIndicator.setRightText(rightText);
				
				ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL, RendL, LstartL, LendL);

				setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		rightPrev.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				
				// TODO Auto-generated method stub
				//rightBar.removeListner();
				compareIndicator.dispose();
				getPrevious(currentRightInstanceId, false);
				compareIndicator = new CloneCompareIndicator(container, SWT.NONE, leftCloneData, rightCloneData, 
						LstartL, LendL, RstartL, RendL);
				FormLayoutUtils.layoutHorizontallyWithSeparatorControl(leftText.getParent(), compareIndicator, rightText.getParent(),20);
				compareIndicator.setLeftText(leftText);
				compareIndicator.setRightText(rightText);
				
				ClonePairScrollModel.setupScrollModel(leftText, rightText,leftCloneData, rightCloneData, RstartL, RendL, LstartL, LendL);

				setupRedrawListeners(compareIndicator);
				container.redraw();
				container.layout(true,true);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
	}
	
	private static void setupRedrawListeners(final Control compareIndicator) {
		leftText.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				compareIndicator.redraw();
			}
		});

		rightText.getVerticalBar().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				compareIndicator.redraw();
			}
		});

		leftText.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(final ControlEvent e) {
				compareIndicator.redraw();
			}
		});

		rightText.addControlListener(new ControlAdapter() {
			@Override
			public void controlResized(ControlEvent e) {
				compareIndicator.redraw();
			}
		});
	}


	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		container.setFocus();
	}

}
