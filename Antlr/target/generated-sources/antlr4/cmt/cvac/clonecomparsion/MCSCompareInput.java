package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import cmt.cddc.methodclones.MethodCloneInstance;
import eu.cqse.check.framework.scanner.IToken;

public class MCSCompareInput implements IEditorInput {
	HashMap<Integer, ArrayList<MethodCloneInstance>> fileToMethodMapping;
	List<IToken> leftCloneToken;
	List<IToken> rightCloneToken;
	String file01; 
	String file02;
	String cloneData01;
	String cloneData02;
	

	public MCSCompareInput(String codeSegment, String codeSegment2,
			HashMap<Integer, ArrayList<MethodCloneInstance>> fileToMethodMapping, List<IToken> leftCloneToken,
			List<IToken> rightCloneToken, String file01, String file02) {
		this.file01 = file01;
		this.file02 = file02;
		this.leftCloneToken = leftCloneToken;
		this.rightCloneToken = rightCloneToken;
		this.fileToMethodMapping = fileToMethodMapping;
		this.cloneData01 = codeSegment;
		this.cloneData02 = codeSegment2;
		
		// TODO Auto-generated constructor stub
	}

	@Override
	public <T> T getAdapter(Class<T> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		// TODO Auto-generated method stub
		return null;
	}

}
