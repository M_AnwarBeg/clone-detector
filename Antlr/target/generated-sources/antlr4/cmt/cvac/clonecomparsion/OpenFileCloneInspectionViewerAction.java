package cmt.cvac.clonecomparsion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ILenientScanner;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerFactory;
import eu.cqse.check.framework.scanner.ScannerUtils;


public class OpenFileCloneInspectionViewerAction extends Action{
	
	HashMap<Integer,ArrayList<SimpleCloneInstance>> fileToSimpleCloneMapping;
	ArrayList<SimpleClone> requiredSCCs;
	
	public OpenFileCloneInspectionViewerAction(HashMap<Integer,ArrayList<SimpleCloneInstance>> maping, ArrayList<SimpleClone> scc) {
		this.fileToSimpleCloneMapping = maping;
		this.requiredSCCs = scc;
	}
	
	
	@Override
	public void run() 
	{
		try {
			int a = (int) fileToSimpleCloneMapping.keySet().toArray()[0];
			int b = (int) fileToSimpleCloneMapping.keySet().toArray()[1];
			
			sFile File_clone1 = sFile_List.getFile(a);
			sFile File_clone2 = sFile_List.getFile(b);
			
			String file01 = ProjectInfo.getFileName(File_clone1.getFileID());
			String file02 = ProjectInfo.getFileName(File_clone2.getFileID());
			
			ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), 
					ProjectInfo.getInputFile(File_clone1.getFileID()));
					
			ILenientScanner scanner02 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())), 
					ProjectInfo.getInputFile(File_clone2.getFileID()));
			
			List<IToken> leftCloneToken = ScannerUtils.readTokens(scanner01);
			List<IToken> rightCloneToken = ScannerUtils.readTokens(scanner02);
			if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()!=null) {
				if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle().equals("File Clone Editor")) {
					//cmt.cvac.clonecomparsion.FileClonePairEditor@211b2a2c
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor(), false);
				}
				else {
					IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
					for(int i=0; i< editorlist.length;i++) {
						if(editorlist[i].getTitle().equals("File Clone Editor")) {
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(editorlist[i], false);
							break;
						}
					}
				}
				
				//PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().close
			}
			
			
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(new FileCloneCompareInput(
					FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), 
					FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())), fileToSimpleCloneMapping, leftCloneToken, rightCloneToken, file01, file02, requiredSCCs), 
					FileClonePairEditor.ID);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
