package cmt.cvac.clonecomparsion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import eu.cqse.check.framework.scanner.ELanguage;
import eu.cqse.check.framework.scanner.ILenientScanner;
import eu.cqse.check.framework.scanner.IToken;
import eu.cqse.check.framework.scanner.ScannerFactory;
import eu.cqse.check.framework.scanner.ScannerUtils;

public class OpenCloneInspectionViewerAction extends Action{
	
	private final MethodCloneInstance clone1;
	private final MethodCloneInstance clone2;
	private ArrayList<MethodCloneInstance> instanceList;
	private final int leftInstanceId;
	private final int rightInstanceID;
	private final Boolean flag;
	
	public OpenCloneInspectionViewerAction(MethodCloneInstance clone1, MethodCloneInstance clone2, ArrayList<MethodCloneInstance>list, int leftCloneId, int rightCloneId) 
	{
		this.clone1=clone1;
		this.clone2=clone2;
		this.instanceList = list;
		this.leftInstanceId = leftCloneId;//clone1.getMethodCloneID();
		this.rightInstanceID = rightCloneId;//clone2.getMethodCloneID();
		this.flag = false;
		//this.leftInstanceId = 
	}
	

		
		@Override
		public void run() 
		{
			
				try 
				{
					sFile File_clone1 = sFile_List.getFile(clone1.getMethod().getFileID());
					sFile File_clone2 = sFile_List.getFile(clone2.getMethod().getFileID());
					String file01 = ProjectInfo.getFileName(File_clone1.getFileID());
					String file02 = ProjectInfo.getFileName(File_clone2.getFileID());
					ILenientScanner scanner01 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())), ProjectInfo.getInputFile(File_clone1.getFileID()));
					
					ILenientScanner scanner02 = ScannerFactory.newLenientScanner(ELanguage.JAVA, FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())), ProjectInfo.getInputFile(File_clone2.getFileID()));
					
					List<IToken> leftCloneToken = ScannerUtils.readTokens(scanner01);
					List<IToken> rightCloneToken = ScannerUtils.readTokens(scanner02);
					
					if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()!=null) {
						if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle().equals("Method Clone Editor")) {
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor(), true);
						}
						else {
							IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
							for(int i=0; i< editorlist.length;i++) {
								if(editorlist[i].getTitle().equals("Method Clone Editor")) {
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(editorlist[i], false);
									break;
								}
							}
						}
					}
					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
							new CloneCompareInput(
									FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone1.getFileID())),
									FileClonesReader.getCodeSegment(ProjectInfo.getInputFile(File_clone2.getFileID())),
									clone1.getMethod().getStartToken()-1, 
									(clone1.getMethod().getEndToken() - clone1.getMethod().getStartToken())+1, 
									clone2.getMethod().getStartToken()-1, 
									(clone2.getMethod().getEndToken() - clone2.getMethod().getStartToken())+1,
									leftCloneToken, rightCloneToken, file01, file02, instanceList, leftInstanceId, rightInstanceID),
									ClonePairEditor.ID);
				
					
					
									/*clone1.getMethod().getStartToken()+1, clone1.getMethod().getEndToken(), 
									clone2.getMethod().getStartToken(), clone2.getMethod().getEndToken(),*/
					
					
					
				}
				catch (Exception e) 
				{
					// TODO: error handling
					e.printStackTrace();
				}
			}
				
		}

