package cmt.cvac.clonecomparsion;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.widgets.Display;

public class CloneBarPainter implements PaintListener {

	/** The clone data. */
	private final String cloneData;

	/** The text to be decorated. */
	private final StyledText text;
	
	private final int startL;
	private final int endL;
	
	GC gc;

	/** Constructor. */
	public CloneBarPainter(String cloneData, StyledText text, int startLine, int endLine) {
		this.cloneData = cloneData;
		this.text = text;
		this.startL = startLine;
		this.endL = endLine;
	}

	/** Draw lines above and below the clone area. */
	@Override
	public void paintControl(PaintEvent e) {
		this.gc = e.gc;
		gc.setForeground(Display.getDefault().getSystemColor(SWT.COLOR_BLACK));

		
		
		int relCloneStart = this.startL
				* text.getLineHeight() + 0
				- text.getTopPixel();
		int relCloneEnd = relCloneStart + this.endL
				* text.getLineHeight();
		
		//Range range = CloneCompareIndicator.determineCloneRange(cloneData, text, 0);
		
		
		gc.drawLine(0, (int) relCloneEnd, text.getBounds().width,
				(int) relCloneEnd);
		gc.drawLine(0, (int) relCloneStart, text.getBounds().width,
				(int) relCloneStart);
	}
	
}
