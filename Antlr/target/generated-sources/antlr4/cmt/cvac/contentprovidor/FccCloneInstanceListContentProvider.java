package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import cmt.cvac.viewobjects.SecondaryFccObject;

public class FccCloneInstanceListContentProvider implements IStructuredContentProvider {

private ArrayList<SecondaryFccObject> list;
	
	public FccCloneInstanceListContentProvider()
	{
		list=new ArrayList<SecondaryFccObject> ();
	}
	public FccCloneInstanceListContentProvider(ArrayList<SecondaryFccObject> list)
	{
		this.list=list;
	}
	
	public void setList(ArrayList<SecondaryFccObject> list)
	{
		this.list=list;
	}
	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) 
	{
		// TODO Auto-generated method stub
		return list.toArray();
	}
}
