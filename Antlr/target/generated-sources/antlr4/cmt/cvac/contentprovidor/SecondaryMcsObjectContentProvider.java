package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import cmt.cvac.viewobjects.SecondaryFccObject;
import cmt.cvac.viewobjects.SecondaryMcsObject;

public class SecondaryMcsObjectContentProvider implements IStructuredContentProvider {

private ArrayList<SecondaryMcsObject> list;
	
	public SecondaryMcsObjectContentProvider()
	{
		list=new ArrayList<SecondaryMcsObject> ();
	}
	public SecondaryMcsObjectContentProvider(ArrayList<SecondaryMcsObject> list)
	{
		this.list=list;
	}
	
	public void setList(ArrayList<SecondaryMcsObject> list)
	{
		this.list=list;
	}
	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) 
	{
		// TODO Auto-generated method stub
		return list.toArray();
	}
}
