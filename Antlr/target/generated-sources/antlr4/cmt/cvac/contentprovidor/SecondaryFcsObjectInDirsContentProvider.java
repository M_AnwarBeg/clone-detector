package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectInDirs;

public class SecondaryFcsObjectInDirsContentProvider implements IStructuredContentProvider {

private ArrayList<SecondaryFcsObjectInDirs> list;
	
	public SecondaryFcsObjectInDirsContentProvider()
	{
		list=new ArrayList<SecondaryFcsObjectInDirs> ();
	}
	public SecondaryFcsObjectInDirsContentProvider(ArrayList<SecondaryFcsObjectInDirs> list)
	{
		this.list=list;
	}
	
	public void setList(ArrayList<SecondaryFcsObjectInDirs> list)
	{
		this.list=list;
	}
	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) 
	{
		// TODO Auto-generated method stub
		return list.toArray();
	}
}
