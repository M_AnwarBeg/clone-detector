package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossGroup;
import cmt.cvac.viewobjects.PrimaryFcsObjectInGroup;

public class PrimaryFcsObjectInGroupContentProvider implements IStructuredContentProvider {

	private ArrayList<PrimaryFcsObjectInGroup> list;

    public PrimaryFcsObjectInGroupContentProvider()
    {
	list = new ArrayList<PrimaryFcsObjectInGroup>();
    }

    public PrimaryFcsObjectInGroupContentProvider(ArrayList<PrimaryFcsObjectInGroup> list)
    {
	this.list = list;
    }

    public void setList(ArrayList<PrimaryFcsObjectInGroup> list)
    {
	this.list = list;
    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {

    }

    @Override
    public Object[] getElements(Object inputElement)
    {
	if (list != null)
	{
	    return list.toArray();
	} else
	{
	    return new Object[] {};
	}
    }
}
