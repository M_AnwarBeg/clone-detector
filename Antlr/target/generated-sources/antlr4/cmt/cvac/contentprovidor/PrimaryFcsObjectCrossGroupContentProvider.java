package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossGroup;

public class PrimaryFcsObjectCrossGroupContentProvider implements IStructuredContentProvider {

	private ArrayList<PrimaryFcsObjectCrossGroup> list;

    public PrimaryFcsObjectCrossGroupContentProvider()
    {
	list = new ArrayList<PrimaryFcsObjectCrossGroup>();
    }

    public PrimaryFcsObjectCrossGroupContentProvider(ArrayList<PrimaryFcsObjectCrossGroup> list)
    {
	this.list = list;
    }

    public void setList(ArrayList<PrimaryFcsObjectCrossGroup> list)
    {
	this.list = list;
    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {

    }

    @Override
    public Object[] getElements(Object inputElement)
    {
	if (list != null)
	{
	    return list.toArray();
	} else
	{
	    return new Object[] {};
	}
    }
}
