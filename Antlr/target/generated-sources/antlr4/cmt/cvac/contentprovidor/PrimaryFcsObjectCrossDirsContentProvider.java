package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;

public class PrimaryFcsObjectCrossDirsContentProvider implements IStructuredContentProvider {

	private ArrayList<PrimaryFcsObjectCrossDirs> list;

    public PrimaryFcsObjectCrossDirsContentProvider()
    {
	list = new ArrayList<PrimaryFcsObjectCrossDirs>();
    }

    public PrimaryFcsObjectCrossDirsContentProvider(ArrayList<PrimaryFcsObjectCrossDirs> list)
    {
	this.list = list;
    }

    public void setList(ArrayList<PrimaryFcsObjectCrossDirs> list)
    {
	this.list = list;
    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {

    }

    @Override
    public Object[] getElements(Object inputElement)
    {
	if (list != null)
	{
	    return list.toArray();
	} else
	{
	    return new Object[] {};
	}
    }
}
