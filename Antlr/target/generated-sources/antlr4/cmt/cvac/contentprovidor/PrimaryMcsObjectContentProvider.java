package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import cmt.cvac.viewobjects.PrimaryFccObject;
import cmt.cvac.viewobjects.PrimaryMcsObject;

public class PrimaryMcsObjectContentProvider implements IStructuredContentProvider {

	private ArrayList<PrimaryMcsObject> list;

    public PrimaryMcsObjectContentProvider()
    {
	list = new ArrayList<PrimaryMcsObject>();
    }

    public PrimaryMcsObjectContentProvider(ArrayList<PrimaryMcsObject> list)
    {
	this.list = list;
    }

    public void setList(ArrayList<PrimaryMcsObject> list)
    {
	this.list = list;
    }

    @Override
    public void dispose()
    {
	// TODO Auto-generated method stub

    }

    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {

    }

    @Override
    public Object[] getElements(Object inputElement)
    {
	if (list != null)
	{
	    return list.toArray();
	} else
	{
	    return new Object[] {};
	}
    }
}
