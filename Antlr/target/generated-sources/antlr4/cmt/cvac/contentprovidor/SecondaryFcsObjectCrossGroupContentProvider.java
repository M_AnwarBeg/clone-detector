package cmt.cvac.contentprovidor;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossGroup;

public class SecondaryFcsObjectCrossGroupContentProvider implements IStructuredContentProvider {

private ArrayList<SecondaryFcsObjectCrossGroup> list;
	
	public SecondaryFcsObjectCrossGroupContentProvider()
	{
		list=new ArrayList<SecondaryFcsObjectCrossGroup> ();
	}
	public SecondaryFcsObjectCrossGroupContentProvider(ArrayList<SecondaryFcsObjectCrossGroup> list)
	{
		this.list=list;
	}
	
	public void setList(ArrayList<SecondaryFcsObjectCrossGroup> list)
	{
		this.list=list;
	}
	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) 
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] getElements(Object inputElement) 
	{
		// TODO Auto-generated method stub
		return list.toArray();
	}
}
