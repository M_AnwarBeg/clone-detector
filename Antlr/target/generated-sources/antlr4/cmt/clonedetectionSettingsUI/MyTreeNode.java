package cmt.clonedetectionSettingsUI;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.TreeItem;

public class MyTreeNode<T>{ 
    private T data = null;
    private List<MyTreeNode> children = new ArrayList<>();
    private MyTreeNode parent = null;
    private boolean selected = false;
    private TreeItem correspondingTreeItem;
    private String path ;
    
   
    
    public MyTreeNode(T data) {
        this.data = data;
    }

    public void addChild(MyTreeNode child) {
        child.setParent(this);
        this.children.add(child);
    }

    public MyTreeNode<T> addChild(T data) {
        MyTreeNode<T> newChild = new MyTreeNode<>(data);
        this.addChild(newChild);
        return newChild;
    }

    public void addChildren(List<MyTreeNode> children) {
        for(MyTreeNode t : children) {
            t.setParent(this);
        }
        this.children.addAll(children);
    }

    public void setCorrespondingTreeItem(TreeItem correspondingTreeItem) {
        this.correspondingTreeItem = correspondingTreeItem;
    }
    
    
    public TreeItem getCorrespondingTreeItem() {
        return this.correspondingTreeItem;
    }
    
    
    public List<MyTreeNode> getChildren() {
        return children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(MyTreeNode parent) {
        this.parent = parent;
    }

    public MyTreeNode getParent() {
        return parent;
    }
    
    
    
    public void setSelected(boolean selected) { 
    	this.selected = selected;
    }
    
    public boolean getSelected() {
    	return selected;
    }
    
    
    public String getPath() {
    	return this.path;
    }
    
    public void setPath(String path) {
    	this.path = path;
    }
}