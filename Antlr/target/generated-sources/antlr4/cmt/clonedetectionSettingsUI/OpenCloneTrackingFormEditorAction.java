package cmt.clonedetectionSettingsUI;

import org.eclipse.jface.action.IAction;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class OpenCloneTrackingFormEditorAction  extends OpenFormEditorAction {
	@Override
	public void run(IAction arg0) {
		try 
		{
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
				new CloneDetectionEditorInput("Clone Tracking "), "cmt.clonedetectionSettingsUI.CloneTrackingFormEditor");
		}
		catch (PartInitException e) {
			System.out.println(e);
		}
	}
}