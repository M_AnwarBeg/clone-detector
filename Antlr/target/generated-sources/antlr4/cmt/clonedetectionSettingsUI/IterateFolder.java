package cmt.clonedetectionSettingsUI;
import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class IterateFolder {

	

private static Display display ;
private static Tree tree  ;
private static  Shell shell;

	public static void main(String[] args) {
	
		tree = new Tree(shell, SWT.OPEN | SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	
		MyTreeNode<String> root = new MyTreeNode<String>("Root");
	    setSelectionList(new File("D:\\TreeStructureTest\\"),root);
	    traverse(root);
	
	}	 
	
	
	static void traverse(MyTreeNode<String> obj) {
	    if (obj != null) {    	
	    	for(MyTreeNode<String> child: obj.getChildren()) {
	         //   System.out.println(obj.getData());
	            traverse(child);
	
	    	}
	    }
	    return;
	}

	public static MyTreeNode<String> setSelectionList(File dir, MyTreeNode<String> parent)
	{
		String javaExtension = "java";		
		File[] files = dir.listFiles();
		boolean isLeaf = false;
		
		MyTreeNode<String> level = new MyTreeNode<String>(dir.getName());
		for (int i = 0; i < files.length; i++) {
			File file = files[i];
			String filename = file.getName();
		  //  level = 

//			System.out.println(filename);
			if (file.isFile()) {
				String ext = getExtension(filename);
				isLeaf = true;
				if (ext.equals(javaExtension)) {
					level.addChild(file.getName());
				}
			} 
			
			else {
				isLeaf = false;
				setSelectionList(file,level);
			}
			
		}
		if (!level.getChildren().isEmpty()) {
			if(parent == null ) {
				level.setData(dir.getAbsolutePath()+ "\\.");
				return level;
			}
			else
			parent.addChild(level);			
		}
		return null;
	}

public static MyTreeNode<String> reCreateCodeBase(File dir, MyTreeNode<String> parent,Map<String,Integer> cloneRunfiles)
{
	String javaExtension = "java";		
	File[] files = dir.listFiles();
	boolean isLeaf = false;
	
	MyTreeNode<String> level = new MyTreeNode<String>(dir.getName());
	for (int i = 0; i < files.length; i++) {
		File file = files[i];
		String filename = file.getName();
	  //  level = 

		//System.out.println(filename);
		if (file.isFile()) {
			String ext = getExtension(filename);
			isLeaf = true;
			if (ext.equals(javaExtension)) {
				MyTreeNode<String> child = level.addChild(file.getName());
				String childAbsolutePath = dir.getAbsolutePath() + "\\" + filename;
				if(cloneRunfiles.containsKey(childAbsolutePath))
					child.setSelected(true);
			}
		} 
		
		else {
			isLeaf = false;
			reCreateCodeBase(file,level,cloneRunfiles);
		}
		
	}
	if (!level.getChildren().isEmpty()) {
		if(parent == null ) {
			level.setData(dir.getAbsolutePath()+ "\\.");
		
			return level;
			
		}
		else
		parent.addChild(level);			
	}
	return null;
}






//static boolean reCreateCodeBase(MyTreeNode<String> root,HashSet<String> cloneRunfiles) {
//	String javaExtension = "java";
//	boolean checkEntireFolder = false;
//	for (MyTreeNode<String> child : root.getChildren()) {
//		if (getExtension(child.getData()).equals(javaExtension)) {
//				String filePath = constructPath(child);
//				if(cloneRunfiles.contains(filePath)) {
//					child.setSelected(true);
//				}
//				else
//					checkEntireFolder = false;
//				
//		}
//		else { 
//			boolean check = reCreateCodeBase(child,cloneRunfiles); 
//			if(check)
//				child.setSelected(true);
//		}
//				
//		}
//		
//	return checkEntireFolder;
//	
//}



static String constructPath(MyTreeNode<String> item)
{	
	if(item == null)
		return new String();

	String itemPath = new String();
	if(item.getData().contains("\\")) {
		itemPath = item.getData().substring(0, item.getData().length()-2);
	}
	else 
		itemPath =  "\\" + item.getData();
		String subTreePath = constructPath(item.getParent()) ;
		return subTreePath + itemPath;
}

public static String getExtension(String filename)
{
String ext;
int dotPlace = filename.lastIndexOf('.');
ext = filename.substring(dotPlace + 1);
return ext;
}


	
}
