package cmt.clonedetectionSettingsUI;

import org.eclipse.jface.action.IAction;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class OpenCloneDetectionFormEditorAction  extends OpenFormEditorAction {
	@Override
	public void run(IAction arg0) {
		try 
		{
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
				new CloneDetectionEditorInput("Clone Detection Settings"), "cmt.clonedetectionSettingsUI.CloneDetectionFormPage");
		}
		catch (PartInitException e) {
			System.out.println(e);
		}
	}
}