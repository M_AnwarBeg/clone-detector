package cmt.clonedetectionSettingsUI;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.ISharedImages; 
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.HyperlinkSettings;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.IMessage;
import org.eclipse.ui.forms.IMessageManager;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import cmt.cddc.clonedetectioninitializer.CloneDetector;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.common.Directories;
import cmt.common.NotificationPopUpUI;
import cmt.common.ProjectImages;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;

import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;

public class CloneDetectionFormPage extends FormPage {

	// private List selectedTokenListItem;
	Hashtable<Integer, String> tokenIds = new Hashtable<Integer, String>();
	HashSet<String> st = new HashSet<String>();
	HashSet<String> selectedTokenToEquate = new HashSet<String>();
	// static ArrayList<String> selectedTokens = new ArrayList<String>();
	// ArrayList<String> ruleContent = new ArrayList<String>();
	Hashtable<Integer, String> selectedTokens = new Hashtable<Integer, String>();

	ScrolledForm form;
	private Text textCloneRunName;
	private Text textDescription;
	private Text textSimpleCloneThreshold;
	private Text textMethodCloneThreshold;
	private Text textFileCloneThreshold;
	private Text textFileCloneTokenCoverage;
	private Text textMethodCloneTokenCoverage;
	private Text txtGroupName;
	private Text txtGroupDirectory;

	private Table tblGroupNames;
	private Table tableSuppresstokens;

	private Table tblEquateToken;
	private Table tblCreateRules;
	private Table tblRules;
	private Label lblCloneRunInfoError;
	private Hashtable<Integer, String> token_details = new Hashtable<Integer, String>();
	private ArrayList<Integer> SuppressedTokenID = new ArrayList<Integer>();
	HashSet<String> selectedSupressedTokens = new HashSet<String>();
	private ArrayList<ArrayList<Integer>> equateTokens = new ArrayList<ArrayList<Integer>>();

	TreeViewer tree1;
	TableItem item, tokenItem;
	private Tree tree;

	private HashMap<String, MyTreeNode<String>> mapGroupFileTree = new HashMap<String, MyTreeNode<String>>();

	private HashMap<String, TableItem> mapTokenIdToEquateTokentbl = new HashMap<String, TableItem>();
	private HashMap<String, Button> mapTokenIdSupressTokentbl = new HashMap<String, Button>();

	Hashtable<Integer, String> mapTokenIdToName = new Hashtable<Integer, String>();

	HashSet<String> setGroupNames = new HashSet<String>();

	static ArrayList<ArrayList<String>> rules = new ArrayList<ArrayList<String>>();
	static ArrayList<ArrayList<Integer>> rulesId = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<String>> equateTokenRules = new ArrayList<ArrayList<String>>();

	int groupId = 0;
	static Display display = Display.getCurrent();

	// Color listBackground = display.getSystemColor(SWT.COLOR_LIST_BACKGROUND);
	private static Color gray = display.getSystemColor(SWT.COLOR_GRAY);
	private static Color white = display.getSystemColor(SWT.COLOR_WHITE);

	ArrayList<String> SkipTokens = new ArrayList<String>();
	ArrayList<Button> btn = new ArrayList<>();

	 Button btnAddRule;
	 Button btnAddToken;
	 Button btnEditRule;
	 Button btnDeleteRule;
	 Button btnRemoveToken;
	Boolean NewRuleFlag = false;

	 int indexEquateRules = 0;

	/**
	 * Create the form page.
	 * 
	 * @param id
	 * @param title
	 */
	/*
	 * public test(String id, String title) { super(id, title); }
	 */

	public CloneDetectionFormPage(FormEditor editor) {
		super(editor, "second", "Clone Detection"); //$NON-NLS-1$ //$NON-NLS-2$
		SkipToken();
		readLanguageToken();
	}

	/**
	 * Create the form page.
	 * 
	 * @param editor
	 * @param id
	 * @param title
	 * @wbp.parser.constructor
	 * @wbp.eval.method.parameter id "Some id"
	 * @wbp.eval.method.parameter title "Some title"
	 */

	public CloneDetectionFormPage(FormEditor editor, String id, String title) {
		super(editor, id, title);
	}

	/**
	 * Create contents of the form.
	 * 
	 * @param managedForm
	 */
	@Override
	protected void createFormContent(IManagedForm managedForm) {
		form = managedForm.getForm();
		FormToolkit toolkit = managedForm.getToolkit();
		toolkit.getHyperlinkGroup().setHyperlinkUnderlineMode(HyperlinkSettings.UNDERLINE_HOVER);
		form.setText("Clone Detection Setting");
		toolkit.decorateFormHeading(form.getForm());
		form.getForm().addMessageHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				String title = e.getLabel();
				// String details = title;
				Object href = e.getHref();
				if (href instanceof IMessage[]) {
					// details =
					// managedForm.getMessageManager().createSummary((IMessage[])href);
				}
				// int type = form.getForm().getMessageType();
				/*
				 * switch (type) { case IMessageProvider.NONE: case
				 * IMessageProvider.INFORMATION: MessageDialog.openInformation(form.getShell(),
				 * title, details); break; case IMessageProvider.WARNING:
				 * MessageDialog.openWarning(form.getShell(), title, details); break; case
				 * IMessageProvider.ERROR: MessageDialog.openError(form.getShell(), title,
				 * details); break; }
				 */
				Point hl = ((Control) e.widget).toDisplay(0, 0);
				hl.x += 10;
				hl.y += 10;
				Shell shell = new Shell(form.getShell(), SWT.ON_TOP | SWT.TOOL);
				shell.setImage(getImage(form.getMessageType()));
				shell.setText(title);
				shell.setLayout(new FillLayout());
				// ScrolledFormText stext = new ScrolledFormText(shell, false);
				// stext.setBackground(toolkit.getColors().getBackground());
				FormText text = toolkit.createFormText(shell, true);
				configureFormText(form.getForm(), text);
				// stext.setFormText(text);
				if (href instanceof IMessage[])
					text.setText(createFormTextContent((IMessage[]) href), true, false);
				shell.setLocation(hl);
				shell.pack();
				shell.open();
			}

		});

		final IMessageManager mmng = managedForm.getMessageManager();

		/*
		 * TableWrapLayout layout = new TableWrapLayout();
		 * form.getBody().setLayout(layout); Section section =
		 * toolkit.createSection(form.getBody(), Section.TITLE_BAR);
		 * section.setText("Local field messages"); Composite sbody =
		 * toolkit.createComposite(section); section.setClient(sbody); GridLayout
		 * glayout = new GridLayout(); glayout.horizontalSpacing = 10;
		 * glayout.numColumns = 2; sbody.setLayout(glayout);
		 * toolkit.paintBordersFor(sbody);
		 * 
		 * 
		 */

		Action myAction = new Action("My Action") {
			public void run() {

				if (verifyAllFields(mmng)) {
					performCloneDetection();

				}

			}
		};
		ImageDescriptor desc = ImageDescriptor.createFromFile(null,
				Directories.getAbsolutePath(ProjectImages.CLONE_DETECTION));

		myAction.setToolTipText("Start Clone Detection");
		myAction.setImageDescriptor(desc);

		// myAction.setEnabled(false);

		form.getToolBarManager().add(myAction);
		form.getToolBarManager().add(new Separator());
		form.getToolBarManager().update(true);

		form.getToolBarManager().update(true);
		// form.setBackgroundImage(FormArticlePlugin.getDefault().getImage(
		// FormArticlePlugin.IMG_FORM_BG));
		form.getForm().addMessageHyperlinkListener(new HyperlinkAdapter() {

		});

		managedForm.getForm().getBody().setLayout(new GridLayout(2, true));

		// Section Clone Run Information
		Section section = createSection(toolkit, form, "Clone Run Information", 2);
		section.setDescription("Provide Clone Run Information");
		Composite sectionClient = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout(2, false));

		toolkit.createLabel(sectionClient, "Name"); //$NON-NLS-1$

		textCloneRunName = toolkit.createText(sectionClient, "");
		textCloneRunName.addVerifyListener(new VerifyListener() {
			public void verifyText(VerifyEvent event) {

				// Assume we don't allow it
				event.doit = false;

				// Get the character typed
				char myChar = event.character;

				// Allow 0-9
				if (Character.isDigit(myChar))
					event.doit = true;

				// Allow backspace
				if (myChar == '\b')
					event.doit = true;

				if (Character.isAlphabetic(myChar)) {
					event.doit = true;
				}

			}
		});

		textCloneRunName.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				String s = textCloneRunName.getText();
				if (s.length() <= 0) {
					mmng.addMessage("textLength", "Field can not be empty", null, IMessageProvider.ERROR,
							textCloneRunName);
				} else {
					mmng.removeMessage("textLength", textCloneRunName);
				}
			}
		});

		GridDataFactory.defaultsFor(textCloneRunName).hint(150, SWT.DEFAULT).indent(5, 0).applyTo(textCloneRunName);
		section.setClient(sectionClient);
		toolkit.createLabel(sectionClient, "Description");

		textDescription = toolkit.createText(sectionClient, "");

		GridDataFactory.defaultsFor(textDescription).hint(150, SWT.DEFAULT | SWT.WRAP | SWT.MULTI).indent(5, 0)
				.applyTo(textDescription);

		section.setClient(sectionClient);

		// SECTION SURPRESSED TOKENS

		section = createSection(toolkit, form, "Settings for Minimum Tokens", 1);
		section.setDescription("Set the minimum tokens of different types of clone classes;");
		sectionClient = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout(1, false));

		Group simpleCloneSettingGroup = new Group(sectionClient, SWT.NONE);
		simpleCloneSettingGroup.setText("Simple Clone");
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		simpleCloneSettingGroup.setLayout(layout);
		toolkit.createLabel(simpleCloneSettingGroup, "Minimum Similarity"); //$NON-NLS-1$
		textSimpleCloneThreshold = toolkit.createText(simpleCloneSettingGroup, "15");
		GridDataFactory.defaultsFor(textSimpleCloneThreshold).hint(20, SWT.DEFAULT).indent(5, 0)
				.applyTo(textSimpleCloneThreshold);
		managedForm.getToolkit().adapt(simpleCloneSettingGroup);
		managedForm.getToolkit().paintBordersFor(simpleCloneSettingGroup);
		textSimpleCloneThreshold.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				event.doit = false;
				char myChar = event.character;
				// Allow 0-9
				if (Character.isDigit(myChar))
					event.doit = true;
				// Allow backspace
				if (myChar == '\b')
					event.doit = true;
			}
		});

		textSimpleCloneThreshold.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textSimpleCloneThreshold, mmng);
			}
		});

		section.setClient(sectionClient);

		sectionClient.setLayout(new GridLayout(1, false));

		Group methodCloneSettingGroup = new Group(sectionClient, SWT.NONE);
		methodCloneSettingGroup.setText("Method Clone");
		layout = new GridLayout();
		layout.numColumns = 4;
		methodCloneSettingGroup.setLayout(layout);
		toolkit.createLabel(methodCloneSettingGroup, "Minimum Similarity");
		textMethodCloneThreshold = toolkit.createText(methodCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textMethodCloneThreshold).hint(20, SWT.DEFAULT).indent(5, 0)
				.applyTo(textMethodCloneThreshold);

		textMethodCloneThreshold.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				event.doit = false;
				char myChar = event.character;
				// Allow 0-9
				if (Character.isDigit(myChar))
					event.doit = true;
				// Allow backspace
				if (myChar == '\b')
					event.doit = true;
			}
		});

		textMethodCloneThreshold.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textMethodCloneThreshold, mmng);
			}
		});

		toolkit.createLabel(methodCloneSettingGroup, "Minimum Token Coverage");
		textMethodCloneTokenCoverage = toolkit.createText(methodCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textMethodCloneTokenCoverage).hint(20, SWT.DEFAULT).indent(5, 0)
				.applyTo(textMethodCloneTokenCoverage);

		textMethodCloneTokenCoverage.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				event.doit = false;
				char myChar = event.character;
				// Allow 0-9
				if (Character.isDigit(myChar))
					event.doit = true;
				// Allow backspace
				if (myChar == '\b')
					event.doit = true;
			}
		});

		textMethodCloneTokenCoverage.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textMethodCloneTokenCoverage, mmng);
			}
		});

		managedForm.getToolkit().adapt(methodCloneSettingGroup);
		managedForm.getToolkit().paintBordersFor(methodCloneSettingGroup);
		section.setClient(sectionClient);

		sectionClient.setLayout(new GridLayout(1, false));
		Group fileCloneSettingGroup = new Group(sectionClient, SWT.NONE);
		fileCloneSettingGroup.setText("File Clone");
		layout = new GridLayout();
		layout.numColumns = 4;
		fileCloneSettingGroup.setLayout(layout);
		toolkit.createLabel(fileCloneSettingGroup, "Minimum Similarity"); //$NON-NLS-1$
		textFileCloneThreshold = toolkit.createText(fileCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textFileCloneThreshold).hint(20, SWT.DEFAULT).indent(5, 0)
				.applyTo(textFileCloneThreshold);

		textFileCloneThreshold.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				event.doit = false;
				char myChar = event.character;
				// Allow 0-9
				if (Character.isDigit(myChar))
					event.doit = true;
				// Allow backspace
				if (myChar == '\b')
					event.doit = true;
			}
		});

		textFileCloneThreshold.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textFileCloneThreshold, mmng);
			}
		});

		toolkit.createLabel(fileCloneSettingGroup, "Minimum Token Coverage"); //$NON-NLS-1$
		textFileCloneTokenCoverage = toolkit.createText(fileCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textFileCloneTokenCoverage).hint(20, SWT.DEFAULT).indent(5, 0)
				.applyTo(textFileCloneTokenCoverage);

		textFileCloneTokenCoverage.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
				event.doit = false;
				char myChar = event.character;
				// Allow 0-9
				if (Character.isDigit(myChar))
					event.doit = true;
				// Allow backspace
				if (myChar == '\b')
					event.doit = true;
			}
		});

		textFileCloneTokenCoverage.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textFileCloneTokenCoverage, mmng);
			}
		});

		managedForm.getToolkit().adapt(fileCloneSettingGroup);
		managedForm.getToolkit().paintBordersFor(fileCloneSettingGroup);
		section.setClient(sectionClient);

		// SECTION CLONE RUN FILES

		section = createSection(toolkit, form, "Clone Detection Files", 4);
		section.setDescription("Select Group-Wise Code Base for Clone Detection.");

		sectionClient = toolkit.createComposite(section);

		Group groupInfo = new Group(sectionClient, SWT.NONE);
		groupInfo.setText("Group Information");
		layout = new GridLayout();
		// sectionClient.makeColumnsEqualWidth = false;
		layout.numColumns = 3;
		groupInfo.setLayout(layout);
		toolkit.createLabel(groupInfo, "Name"); //$NON-NLS-1$
		txtGroupName = toolkit.createText(groupInfo, "Default");

		txtGroupName.addVerifyListener(new VerifyListener() {

			@Override
			public void verifyText(VerifyEvent event) {
//				 event.doit = false;
//
//			        // Get the character typed
//			        char myChar = event.character;
//			        String text = ((Text) event.widget).getText();
//
//			        // Allow '-' if first character
//			        if (myChar == '-' && text.length() == 0)
//			          event.doit = true;
//
//			        // Allow 0-9
//			        if (Character.isDigit(myChar))
//			          event.doit = true;
//
//			        // Allow backspace
//			        if (myChar == '\b')
//			          event.doit = true;
			}

		});

		txtGroupName.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				String s = txtGroupName.getText();
				if (s.length() <= 0) {
					mmng.addMessage("textLength", "Provide a group name", null, IMessageProvider.ERROR, txtGroupName);

				} else {
					mmng.removeMessage("textLength", txtGroupName);
				}
			}
		});

		GridDataFactory.defaultsFor(txtGroupName).hint(150, SWT.DEFAULT).indent(5, 0).applyTo(txtGroupName);

		toolkit.createLabel(groupInfo, "");

		toolkit.createLabel(groupInfo, "Root Folder");
		txtGroupDirectory = toolkit.createText(groupInfo, "");
		GridDataFactory.defaultsFor(txtGroupDirectory).hint(150, SWT.DEFAULT).indent(5, 0).applyTo(txtGroupDirectory);
		txtGroupDirectory.setEditable(false);

		txtGroupDirectory.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				String s = txtGroupDirectory.getText();
				if (s.length() <= 0) {
					mmng.addMessage("textLength", "Select root directory of group", null, IMessageProvider.ERROR,
							txtGroupDirectory);

				} else {
					mmng.removeMessage("textLength", txtGroupDirectory);
				}
			}
		});

		Button btnBrowseDir = new Button(groupInfo, SWT.PUSH);
		btnBrowseDir.setText("Browse...");
		GridData gridData = new GridData(GridData.END, GridData.CENTER, true, false);
		gridData.horizontalIndent = 4;

		btnBrowseDir.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// System.out.print("Hello World");
				// TODO Auto-generated method stub
				Shell shell = new Shell();
				DirectoryDialog directoryDialog = new DirectoryDialog(shell);
				directoryDialog.setMessage("Please select root directory for group...");
				directoryDialog.setFilterPath("");
				try {
					String selection = directoryDialog.open();
					if (selection != null)
						txtGroupDirectory.setText(selection);
					System.out.println(selection);
					// setSelectionList(dir, lang);
				} catch (Exception exp) {
				}

			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		toolkit.createLabel(groupInfo, "");

		Button btnAddGroup = new Button(groupInfo, SWT.PUSH);
		btnAddGroup.setText("Add ");
		gridData = new GridData(GridData.END, GridData.CENTER, true, false);
		gridData.horizontalIndent = 5;

		btnAddGroup.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {

				boolean verifyGroupName = true;
				boolean verifyRootPath = true;
				if (txtGroupName.getText().isEmpty() && txtGroupDirectory.getText().isEmpty()) {
					mmng.addMessage("textLength", "Create a group", null, IMessageProvider.ERROR, txtGroupName);
					mmng.addMessage("textLength", "Create a group", null, IMessageProvider.ERROR, txtGroupDirectory);
					verifyGroupName = false;
					verifyRootPath = false;

				}

				if (txtGroupName.getText().isEmpty()) {
					mmng.addMessage("textLength", "Provide a group name", null, IMessageProvider.ERROR, txtGroupName);
					verifyGroupName = false;

				}

				if (txtGroupDirectory.getText().isEmpty()) {
					mmng.addMessage("textLength", "Select root directory of group", null, IMessageProvider.ERROR,
							txtGroupDirectory);
					verifyRootPath = false;

				}

				if (txtGroupName.getText().isEmpty()) {
					mmng.addMessage("textLength", "Provide a group name", null, IMessageProvider.ERROR, txtGroupName);
					verifyGroupName = false;

				}

				if (!txtGroupName.getText().isEmpty()) {

					if (setGroupNames.contains(txtGroupName.getText())) {
						mmng.addMessage("textLength", "Group name exists", null, IMessageProvider.ERROR, txtGroupName);
						verifyGroupName = false;
					}

				}

				if (!txtGroupDirectory.getText().isEmpty()) {
					String overlappingPathName = checkOverlapingPaths(txtGroupDirectory.getText());
					if (overlappingPathName != null) {
						mmng.addMessage("textLength", "Directory Path overlaps with group " + overlappingPathName, null,
								IMessageProvider.ERROR, txtGroupDirectory);
						verifyRootPath = false;

					}
				}

				if (verifyGroupName) {
					mmng.removeMessage("textLength", txtGroupName);
				}

				if (verifyRootPath) {
					mmng.removeMessage("textLength", txtGroupDirectory);
				}

				if (verifyGroupName && verifyRootPath) {

					TableItem item = new TableItem(tblGroupNames, SWT.NONE);
					item.setText(txtGroupName.getText());
					MyTreeNode<String> root = IterateFolder.setSelectionList(new File(txtGroupDirectory.getText()),
							null);
					mapGroupFileTree.put(txtGroupName.getText(), root);
					setGroupNames.add(txtGroupName.getText());
					txtGroupDirectory.setText("");
					txtGroupName.setText("");
					mmng.removeMessage("textLength", txtGroupName);
					mmng.removeMessage("textLength", txtGroupDirectory);

					// mapGroupsIdToName.put(groupId, txtGroupName.getText());
					// groupId++;

				}

			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		managedForm.getToolkit().adapt(groupInfo);
		managedForm.getToolkit().paintBordersFor(groupInfo);
		section.setClient(sectionClient);

		toolkit.createLabel(sectionClient, "");
		toolkit.createLabel(sectionClient, "");

		// Composite sectionGroupList = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout(4, false));

		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		sectionClient.setLayout(gridLayout);

		toolkit.createLabel(sectionClient, "Groups");
		toolkit.createLabel(sectionClient, "");
		toolkit.createLabel(sectionClient, "Java Files");

		tblGroupNames = new Table(sectionClient, SWT.BORDER | SWT.V_SCROLL);
		tblGroupNames.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event arg0) {
				displayTree();

			}
		});

		gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
		gridData.heightHint = 300;
		gridData.widthHint = 10;
		tblGroupNames.setLayoutData(gridData);

		Button btnDeleteGroup = new Button(sectionClient, SWT.PUSH);
		btnDeleteGroup.setText("Delete");
		gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false);
		gridData.horizontalIndent = 4;
		btnDeleteGroup.setLayoutData(gridData);
		btnDeleteGroup.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// writeCloneRunFiles();

				IStatus status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);

				managedForm.getForm().getMessageManager().addMessage("empty_message_key", "", null,
						IMessageProvider.NONE);

				TableItem[] selectedToken = tblGroupNames.getSelection();
				String groupName = selectedToken[0].getText();
				tblGroupNames.remove(tblGroupNames.getSelectionIndex());
				mapGroupFileTree.remove(groupName);
				tree.removeAll();
				tree.clearAll(true);
			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		tree = new Tree(sectionClient, SWT.OPEN | SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
		gridData.heightHint = 300;
		gridData.widthHint = 400;
		// listHeight = fileList.getItemHeight() * 12;
		// trim = fileList.computeTrim(0, 0, 0, listHeight);
		// gridData.heightHint = trim.height;
		tree.setLayoutData(gridData);
		tree.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				if (event.detail == SWT.CHECK) {
					TreeItem item = (TreeItem) event.item;
					boolean checked = item.getChecked();
					TreeViewWithFolderStructure.checkItems(item, checked);
				}

			}
		});

		toolkit.createLabel(sectionClient, "");

		/*
		 * Button importList = new Button(sectionClient, SWT.PUSH);
		 * importList.setText("Import List"); gridData = new
		 * GridData(GridData.BEGINNING, GridData.CENTER, true, false);
		 * gridData.horizontalIndent = 4; importList.setLayoutData(gridData);
		 * importList.addSelectionListener(new SelectionAdapter() {
		 * 
		 * });
		 */

		/*
		 * toolkit.createLabel(sectionClient, ""); toolkit.createLabel(sectionClient,
		 * ""); importList = new Button(sectionClient, SWT.PUSH);
		 * importList.setText("Export List"); gridData = new
		 * GridData(GridData.BEGINNING, GridData.CENTER, true, false);
		 * gridData.horizontalIndent = 4; importList.setLayoutData(gridData);
		 * importList.addSelectionListener(new SelectionAdapter() {
		 * 
		 * });
		 * 
		 */

		// SECTION SURPRESSED TOKENS
		toolkit.createLabel(sectionClient, ""); //$NON-NLS-1$
		section.setClient(sectionClient);
		section = createSection(toolkit, form, "Supress Token", 4);
		section.setDescription("Select the suppressed tokens from the Java token list below.");
		sectionClient = toolkit.createComposite(section);
		gridLayout = new GridLayout();
		gridLayout.numColumns = 1;
		sectionClient.setLayout(gridLayout);

		tableSuppresstokens = toolkit.createTable(sectionClient, SWT.BORDER | SWT.MULTI);

		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 300;
		gd.widthHint = 400;
		tableSuppresstokens.setLayoutData(gd);
		// new Table(container, SWT.BORDER | SWT.MULTI);
		for (int i = 0; i < 4; i++) {
			TableColumn column = new TableColumn(tableSuppresstokens, SWT.NONE);
			column.setWidth(100);
		}

		fillSupressTokensTable();

		// SECTION EQUATE TOKENS
		section.setClient(sectionClient);
		section = createSection(toolkit, form, "Equate Token", 4);
		sectionClient = toolkit.createComposite(section);
		section.setDescription(
				"Select multiple tokens from the token list below to make them equivalent for Clone Detection (Example: \"token1 = token2, token3 = token4 = token5\").");

		gridLayout = new GridLayout();
		gridLayout.numColumns = 6;
		sectionClient.setLayout(gridLayout);

		toolkit.createLabel(sectionClient, "Tokens");
		toolkit.createLabel(sectionClient, "");
		toolkit.createLabel(sectionClient, "Create Rule");
		toolkit.createLabel(sectionClient, "");
		toolkit.createLabel(sectionClient, "Rules List");
		toolkit.createLabel(sectionClient, "");

		tblEquateToken = new Table(sectionClient, SWT.BORDER | SWT.MULTI | SWT.SINGLE);
		// tblEquateTokenList = new Table(sectionClient, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_BOTH);
		// gd.horizontalSpan = 25;
		// gd.verticalSpan = 50;

		gd.heightHint = 300;
		gd.widthHint = 125;

		tblEquateToken.setLayoutData(gd);
		// TableColumn column = new TableColumn(tokenTable, SWT.NONE);

		fillequateTokensTable();

		btnAddToken = new Button(sectionClient, SWT.PUSH);
		btnAddToken.setText(">>");
		gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, false, false);
		gridData.horizontalIndent = 4;
		int size = btn.size();
		for (int i = 0; i < size; i++) {
			Button b = btn.get(i);
			b.addSelectionListener(new SelectionAdapter() {

				@Override
				public void widgetSelected(SelectionEvent e) {
					// TODO Auto-generated method stub
					if (b.getSelection()) {
						b.setSelection(true);
						// System.out.println(b.getText());
						selectedSupressedTokens.add(b.getText());
						mapTokenIdToEquateTokentbl.get(b.getText()).setBackground(gray);
						// TableItem item =
						// tableSuppresstokens.getItem(tableSuppresstokens.getSelectionIndex());
						for (Entry<Integer, String> m : mapTokenIdToName.entrySet()) {
							if (m.getValue().equals(b.getText())) {
								SuppressedTokenID.add(m.getKey());
							}
						}

					} else {
						b.setSelection(false);
						selectedSupressedTokens.remove(b.getText());
						mapTokenIdToEquateTokentbl.get(b.getText()).setBackground(white);
						for (Entry<Integer, String> m : mapTokenIdToName.entrySet()) {
							if (m.getValue().equals(b.getText())) {
								SuppressedTokenID.remove(m.getKey());
							}
						}
					}
					// System.out.println(selectedSupressedTokens.toString());

				}
			});
		}

		btnAddToken.setLayoutData(gridData);

		tblCreateRules = new Table(sectionClient, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_BOTH);
		/*
		 * gd.horizontalSpan = 25; gd.verticalSpan = 50;
		 */

		gd.heightHint = 300;
		gd.widthHint = 125;
		tblCreateRules.setLayoutData(gd);
		// tblCreateRules.add("Add");

		// toolkit.createLabel(sectionClient, "1");
		// toolkit.createLabel(sectionClient, "2");
		// toolkit.createLabel(sectionClient, "3");
		// toolkit.createLabel(sectionClient, "4");

		btnAddRule = new Button(sectionClient, SWT.PUSH);
		btnAddRule.setText("Add Rule");
		gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false);
		gridData.horizontalIndent = 4;
		btnAddRule.setLayoutData(gridData);

		gd = new GridData(GridData.FILL_BOTH);

		tblRules = new Table(sectionClient, SWT.BORDER | SWT.SINGLE);
		gd = new GridData(GridData.FILL_BOTH);
		/*
		 * gd.horizontalSpan = 80; gd.verticalSpan = 50;
		 */
		gd.heightHint = 300;
		gd.widthHint = 250;

		tblRules.setLayoutData(gd);

		btnEditRule = new Button(sectionClient, SWT.PUSH);
		btnEditRule.setText("Edit Rule");
		gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false);
		gridData.horizontalIndent = 4;
		btnEditRule.setLayoutData(gridData);

		toolkit.createLabel(sectionClient, "");

		btnRemoveToken = new Button(sectionClient, SWT.PUSH);
		btnRemoveToken.setText("<<");
		gridData = new GridData(GridData.FILL_BOTH, GridData.FILL_BOTH, true, true);
		gridData.horizontalIndent = 4;
		gridData.verticalIndent = -300;

		btnRemoveToken.setLayoutData(gridData);

		toolkit.createLabel(sectionClient, "");
		toolkit.createLabel(sectionClient, "");
		// Button btnDeleteRule1 = new Button(sectionClient, SWT.PUSH);
		/// btnDeleteRule1.setText("Hello Rule");
		// GridData gridData1 = new GridData(GridData.FILL_BOTH, GridData.FILL_BOTH,
		// true, true);

		// toolkit.createLabel(sectionClient, "3");
		btnDeleteRule = new Button(sectionClient, SWT.PUSH);
		btnDeleteRule.setText("Delete Rule");
		gridData = new GridData(GridData.CENTER, GridData.CENTER, true, true);
		// gridData1.horizontalIndent = -30;

		// gridData1.horizontalIndent = 100;
		// gridData1.verticalIndent = -10;

		// gridData.heightHint =100 ;

		// Button btnDeleteRule = new Button(sectionClient, SWT.PUSH);
		// btnDeleteRule.setText("Delete Rule");
		// gridData = new GridData(GridData.END, GridData.END, true, true);
		// gridData.horizontalIndent = 4;
		// gridData.verticalIndent = -50;

		// gridData.heightHint =50 ;

		btnDeleteRule.setLayoutData(gridData);

		btnRemoveToken.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				String token = tblCreateRules.getItem(tblCreateRules.getSelectionIndex()).getText();
				int index = 0;

				if (selectedTokens.contains(token)) {
					for (Entry<Integer, String> m : selectedTokens.entrySet()) {
						if (m.getValue().equals(token)) {
							index = m.getKey();
							break;
						}
					}

				}
				tblEquateToken.getItem(index).setBackground(white);
				mapTokenIdSupressTokentbl.get(token).setEnabled(true);

				tblCreateRules.remove(tblCreateRules.getSelectionIndex());
				if (tblCreateRules.getItemCount() == 0) {
					// selectedTokenListItem.add("Add New Token.");
					btnAddRule.setEnabled(false);
					btnEditRule.setEnabled(false);
					btnDeleteRule.setEnabled(false);
					btnAddToken.setEnabled(false);
					btnRemoveToken.setEnabled(false);
				} else {
					btnAddRule.setEnabled(true);
					btnAddToken.setEnabled(true);
					btnRemoveToken.setEnabled(true);
				}

				/*
				 * TableItem[] selectedToken = tblCreateRules.getSelection();
				 * 
				 * for(TableItem tableItemtoken : selectedToken){
				 * 
				 * tblCreateRules.remove(tblCreateRules.getSelectionIndex());
				 * mapTokenIdSupressTokentbl.get(tableItemtoken.getText()).setEnabled(true);
				 * 
				 * 
				 * }
				 * 
				 * 
				 * 
				 * 
				 * // TableItem[] selectedToken = tblCreateRules.getSelection();
				 * 
				 * // for(TableItem tableItemtoken : selectedToken){
				 * 
				 * // mapTokenIdSupressTokentbl.get(tableItemtoken.getText()).setEnabled(false);
				 * 
				 * // tblCreateRules.remove(tblCreateRules.getSelectionIndex());
				 * 
				 * // tokenItem = new TableItem(tblCreateRules, SWT.NONE); //
				 * tokenItem.setText(tableItemtoken.getText());
				 * 
				 * // }
				 * 
				 * // tblCreateRules.remove(tblCreateRules.getSelectionIndex());
				 * if(selectedTokenListItem.getItemCount() == 0) {
				 * selectedTokenListItem.add("Add New Token."); addNewRule.setEnabled(false);
				 * editRule.setEnabled(false); DeleteRule.setEnabled(false);
				 * addTokenButton.setEnabled(false); removeToken.setEnabled(false); } else {
				 * addNewRule.setEnabled(true); addTokenButton.setEnabled(true);
				 * removeToken.setEnabled(true); } }
				 * 
				 * 
				 * for(TableItem tableItemtoken : selectedToken){
				 * 
				 * 
				 * tokenItem = new TableItem(tblCreateRules, SWT.NONE);
				 * tokenItem.setText(tableItemtoken.getText());
				 * 
				 * }
				 */

			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		tblRules.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				btnAddRule.setEnabled(false);
				btnDeleteRule.setEnabled(true);
				btnEditRule.setEnabled(true);

			}
		});
		btnEditRule.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				int index = tblRules.getSelectionIndex();
				// ruleContent.clear();
				ArrayList<String> ruleContent = rules.get(index);
				int size = ruleContent.size();
				tblCreateRules.removeAll();
				for (int i = 0; i < size; i++) {
					TableItem item = new TableItem(tblCreateRules, SWT.NONE);
					item.setText(ruleContent.get(i));
				}
				rulesId.remove(index);
				rules.remove(index);
				tblRules.remove(index);
				btnEditRule.setEnabled(false);
				btnDeleteRule.setEnabled(false);
				btnAddRule.setEnabled(true);

				TableItem[] st = tblEquateToken.getSelection();
				int count = tblCreateRules.getItemCount();
				String token = "";
				/*
				 * for (int i = 0; i < count; i++) { String line =
				 * tblCreateRules.getItem(i).getText(); if (line.equals(st[0].getText())) { int
				 * a = line.indexOf(st[0].getText()); if (a == 0 && (line.length() !=
				 * st[0].getText().length())) { token = line.replace(st[0] + "=", "");
				 * tblCreateRules.remove(i); selectedTokenToEquate.remove(st[0]);
				 * tblCreateRules.add(token, i); // rules.remove(st[0]); ArrayList<String>
				 * ruleToken = rules.get(i); for (int j = 0; j < ruleToken.size(); j++) { if
				 * (ruleToken.equals(st[0])) { ruleToken.remove(st[0]); rules.add(i, ruleToken);
				 * }
				 * 
				 * }
				 * 
				 * } else if (line.length() == st[0].getText().length()) { token =
				 * line.replace(st[0].getText(), ""); if (token == null) {
				 * selectedTokenListItem.remove(i); selectedTokenToEquate.remove(st[0]);
				 * selectedTokenListItem.add("New Rule Will be Added Here", i); //
				 * rules.remove(st[0]); ArrayList<String> ruleToken = rules.get(i); for (int j =
				 * 0; j < ruleToken.size(); j++) { if (ruleToken.equals(st[0])) {
				 * ruleToken.remove(st[0]); rules.add(i, ruleToken); }
				 * 
				 * } } else {
				 * 
				 * }
				 * 
				 * 
				 * ArrayList<String> ruleToken = rules.get(i); for(int j =0; j<ruleToken.size();
				 * j++) { if(ruleToken.equals(st[0])) { ruleToken.remove(st[0]); rules.add(i,
				 * ruleToken); }
				 * 
				 * }
				 * 
				 * } else { token = line.replace("=" + st[0], "");
				 * selectedTokenListItem.remove(i); selectedTokenToEquate.remove(st[0]);
				 * selectedTokenListItem.add(token, i); // rules.remove(st[0]);
				 * ArrayList<String> ruleToken = rules.get(i); for (int j = 0; j <
				 * ruleToken.size(); j++) { if (ruleToken.equals(st[0])) {
				 * ruleToken.remove(st[0]); rules.add(i, ruleToken); }
				 * 
				 * } }
				 * 
				 * }
				 * 
				 * }
				 */
			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		btnDeleteRule.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {

				int index = tblRules.getSelectionIndex();
				int index01 = 0;

				ArrayList<String> ruleContent = rules.get(index);
				int size = ruleContent.size();
				tblCreateRules.removeAll();
				for (int i = 0; i < size; i++) {
					try {
						for (Entry<Integer, String> m : selectedTokens.entrySet()) {
							if (m.getValue().equals(ruleContent.get(i))) {
								index01 = m.getKey();
								tblEquateToken.getItem(index01).setBackground(white);
								selectedTokens.remove(m.getKey(), m.getValue());
								break;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					/*
					 * for (int j = 0; j < selectedTokens.size(); j++) { if
					 * (.equals(selectedTokens.get(j))) { TableItem item =
					 * tblEquateToken.getItem(j); item.setForeground(white);
					 * 
					 * break;
					 * 
					 * } }
					 */

				}
				tblRules.remove(tblRules.getSelectionIndex());
				rulesId.remove(index);
				rules.remove(index);
				// tblRules.remove(index);
				if (tblRules.getItemCount() == 0) {
					btnAddRule.setEnabled(false);
					btnEditRule.setEnabled(false);
					btnDeleteRule.setEnabled(false);
					btnAddToken.setEnabled(false);
					btnRemoveToken.setEnabled(false);
				}
				// tblCreateRules.add("Add New Token.");

				/*
				 * int index = tblRules.getSelectionIndex(); ArrayList<String> ruleContent =
				 * rules.get(index); int size = ruleContent.size(); //
				 * selectedTokenListItem.removeAll();
				 * 
				 * for(int i=0;i<size;i++) { for(int j =0; j<selectedTokens.size();j++) {
				 * if(ruleContent.get(i).equals(selectedTokens.get(j))) { TableItem item =
				 * tokenTable.getItem(j); item.setForeground(white); break; } } }
				 * 
				 * rules.remove(index); tblRules.remove(index);
				 * 
				 * 
				 * rulesTable.remove(index); if(rulesTable.getItemCount()==0) {
				 * addNewRule.setEnabled(false); editRule.setEnabled(false);
				 * DeleteRule.setEnabled(false); addTokenButton.setEnabled(false);
				 * removeToken.setEnabled(false); } selectedTokenListItem.add("Add New Token.");
				 * 
				 */
			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		tblEquateToken.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (tblEquateToken.getSelectionCount() > 0) {
					TableItem[] selectedToken = tblEquateToken.getSelection();
					TableItem item = selectedToken[0];
					org.eclipse.swt.graphics.Color color = item.getBackground();
					try {
						if (color.equals(gray)) {
							btnAddToken.setEnabled(false);

						} else {
							btnAddToken.setEnabled(true);
						}

					} catch (Exception ex) {
						ex.printStackTrace();
					}

					// editRule.setEnabled(true);
					// editRule.setEnabled(true);

				}

				else {
					btnAddToken.setEnabled(false);
					// editRule.setEnabled(false);
					// editRule.setEnabled(false);
				}
			}

		});
		btnAddToken.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				TableItem[] selectedToken = tblEquateToken.getSelection();
				// int id = tokenList.getSelectionIndex();
				int index = tblEquateToken.getSelectionIndex();
				// tblEquateToken.getSelectionIndex().setForeground(gray);
				// tblEquateToken.getSelection
				TableItem seletectedTblItem = selectedToken[0];
				seletectedTblItem.setBackground(gray);
				TableItem item;
				selectedTokens.put(index, seletectedTblItem.getText());
				ArrayList<String> rule = new ArrayList<String>();

				mapTokenIdSupressTokentbl.get(seletectedTblItem.getText()).setEnabled(false);

				/*
				 * if (tblCreateRules.getItem(0).equals("Add New Token.")) {
				 * selectedTokenListItem.remove(0);
				 * 
				 * }
				 */

				if (tblCreateRules.getItemCount() == 0) {

					rule.add(selectedToken[0].getText());
					item = new TableItem(tblCreateRules, SWT.NONE);
					item.setText(selectedToken[0].getText());

					// tblCreateRules.add(/* selectedToken[0].getText() */item);
					selectedTokenToEquate.add(selectedToken[0].getText());
					btnAddRule.setEnabled(true);

				} else {
					rule.add(selectedToken[0].getText());
					item = new TableItem(tblCreateRules, SWT.NONE);
					item.setText(selectedToken[0].getText());
					// selectedTokenListItem.add(selectedToken[0].getText());
					selectedTokenToEquate.add(selectedToken[0].getText());

				}
				// btnAddToken.setEnabled(false);
				btnAddRule.setEnabled(true);
				btnRemoveToken.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		btnAddRule.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {

				int count = tblCreateRules.getItemCount();
				TableItem[] content = null;
				if (count > 0) {
					content = tblCreateRules.getItems();
				}
				item = new TableItem(tblRules, SWT.None);
				String rule = "";
				ArrayList<String> currentRule = new ArrayList<String>();
				ArrayList<Integer> ruleID = new ArrayList<Integer>();
				ArrayList<String> ruleData = new ArrayList<String>();
				// ArrayList<Integer> ruleIds = new ArrayList<Integer>();
				for (int i = 0; i < count; i++) {
					if (i == 0) {
						rule += content[i].getText();
					} else {
						rule += " = " + content[i].getText();
					}
					ruleData.add(content[i].getText());
					currentRule.add(content[i].getText());
				}

				// equateTokenRules

				int size = rules.size();
				if (tblRules.getItemCount() == 1) {
					if (rules.size() != 0) {
						rules.clear();
						rules.add(0, ruleData);
					} else {
						rules.add(0, ruleData);
					}

				} else {
					rules.add(size, ruleData);
				}

				
				  ArrayList<Integer> ruleTokenIDS = new ArrayList<Integer>();
				  
				  for (int i =0;i<ruleData.size();i++) { 
					  mapTokenIdToName.size();
					  for(Entry<Integer, String> m:mapTokenIdToName.entrySet()) { 
						  if(m.getValue().equals(ruleData.get(i))) {
							  ruleTokenIDS.add(m.getKey()); 
							  break; 
							  } 
						  } 
					  }
				 
				size = rulesId.size();
				rulesId.add(size, ruleTokenIDS);

				item.setText(rule);
				tblCreateRules.removeAll();
				// tblCreateRules.add("Add New Token.");
				btnAddRule.setEnabled(false);
				btnAddToken.setEnabled(false);

			}

			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		tblCreateRules.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				if (tblCreateRules.getItemCount() > 0) {
					btnRemoveToken.setEnabled(true);
				}

			}

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}

		});

		toolkit.createLabel(sectionClient, "");
		section.setClient(sectionClient);
	}

	private Section createSection(FormToolkit toolkit, ScrolledForm form, String title, int horizontalSpa1n) {

		Section section = toolkit.createSection(form.getBody(), Section.TWISTIE | Section.DESCRIPTION);

		section.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 2));
		section.setActiveToggleColor(toolkit.getHyperlinkGroup().getActiveForeground());
		section.setToggleColor(toolkit.getColors().getColor(IFormColors.SEPARATOR));
		toolkit.createCompositeSeparator(section);
		FormText rtext = toolkit.createFormText(section, false);
		section.setClient(rtext);
		// loadFormText(rtext, toolkit);
		section.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				form.reflow(false);
			}
		});
		section.setExpanded(true);
		section.setText(title);
		return section;
	}

	private static void testGridLayout(final ScrolledForm form, final FormToolkit toolkit) {
		final ExpandableComposite exp = toolkit.createExpandableComposite(form.getBody(), ExpandableComposite.TREE_NODE
		// ExpandableComposite.NONE
		);

		Composite composite = toolkit.createComposite(exp);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 20;
		gridLayout.marginWidth = 20;
		composite.setLayout(gridLayout);

	}

	static public Section createSection(FormToolkit toolkit, Composite parent, String title, int hSpan, int vSpan) {

		Section section = toolkit.createSection(parent, Section.SHORT_TITLE_BAR);
		GridData osectionGridData = new GridData(SWT.FILL, SWT.BEGINNING, false, false);
		osectionGridData.horizontalSpan = hSpan;
		osectionGridData.verticalSpan = vSpan;
		osectionGridData.horizontalIndent = 5;
		section.setLayoutData(osectionGridData);
		section.setText(title);

		GridLayout osectionGL = new GridLayout(1, true);
		osectionGL.marginHeight = 0;
		osectionGL.marginWidth = 0;
		section.setLayout(osectionGL);

		return section;
	}

	private void getSelectedFiles() {
		MyTreeNode<String> date;

	}

	private void displayTree() {
		tree.removeAll();
		// TreeViewWithFolderStructure.mapping.clear();
		TableItem[] selection = tblGroupNames.getSelection();
		String groupName = selection[0].getText();
		MyTreeNode<String> root = mapGroupFileTree.get(groupName);
		TreeItem parent = new TreeItem(tree, SWT.NONE | SWT.CHECK);
		parent.setImage(new Image(form.getDisplay(), Directories.getAbsolutePath(ProjectImages.FOLDER)));
		parent.setText(root.getData());
		TreeViewWithFolderStructure.mapping.clear();
		TreeViewWithFolderStructure.traverse(parent, root, form.getDisplay());
		TreeViewWithFolderStructure.expandAllTree(parent);

	}

	private void fillSupressTokensTable() {

		for (int i = 0; i < (mapTokenIdToName.size() / 4) + (mapTokenIdToName.size() % 4); i++) {
			new TableItem(tableSuppresstokens, SWT.NONE);
		}

		int col = 0, row = 0;
		Button button;
		TableItem[] items = tableSuppresstokens.getItems();

		for (Entry<Integer, String> m : mapTokenIdToName.entrySet()) {
			TableEditor editor = new TableEditor(tableSuppresstokens);
			button = new Button(tableSuppresstokens, SWT.CHECK);// container
			button.setText(m.getValue());
			button.setData(m.getKey());
			editor.grabHorizontal = true;
			editor.setEditor(button, items[row], col);
			btn.add(button);
			editor.grabHorizontal = true;
			editor.setEditor(button, items[row], col);
			mapTokenIdSupressTokentbl.put(m.getValue(), button);

			if (col == 0 || col == 1 || col == 2) {
				col++;
			}

			else if (col == 3) {
				col = 0;
				row++;
			}
		}

	}

	private void readLanguageToken() {
		try {
			String filePath = Directories.getAbsolutePath(Directories.TOKEN_JAVA);
			File file = new File(filePath);
			FileInputStream filein = new FileInputStream(file);
			BufferedReader stdin = new BufferedReader(new InputStreamReader(filein));
			String line = "";

			while ((line = stdin.readLine()) != null) {
				int last = line.lastIndexOf("=");
				int endIndex = last;
				int beginIndex = last + 1;
				int length = line.length();
				String token = line.substring(0, endIndex);
				String tokenID = line.substring(beginIndex, length);
				String IDS = "";
				if (mapTokenIdToName.containsKey(Integer.parseInt(tokenID))) {
					if (token.contains("'")) {
						if (token.lastIndexOf("'") == (token.length() - 1)) {
							IDS = token.substring(1, (token.length() - 1));
							if (!SkipTokens.contains(IDS)) {
								mapTokenIdToName.put(Integer.parseInt(tokenID), token);
							} else {
								mapTokenIdToName.remove(Integer.parseInt(tokenID));
							}

						}

					}
				} else {
					if (!SkipTokens.contains(token)) {
						mapTokenIdToName.put(Integer.parseInt(tokenID), token);
					}
				}
			}
			stdin.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private void writeCloneRunFiles() {
		try {
			// itemPath = itemPath = item.getText().substring(0, item.getText().length()-2);
			String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES);
			// System.out.println("Here: "+pathStr);
			File file = new File(pathStr);
			file.createNewFile();
			FileOutputStream fileout = new FileOutputStream(file);
			PrintWriter stdout = new PrintWriter(fileout);

			for (Map.Entry<String, MyTreeNode<String>> listEntry : mapGroupFileTree.entrySet()) {

				TreeViewWithFolderStructure.writeCheckedFiles(listEntry.getValue(), stdout);
				stdout.println(";");

			}
			stdout.flush();
			stdout.close();
			fileout.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private void writeCloneRunFilesGroups() {
		try {

			// itemPath = itemPath = item.getText().substring(0, item.getText().length()-2);
			String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES_GROUP);
			// System.out.println("Here: "+pathStr);
			File file = new File(pathStr);
			file.createNewFile();
			FileOutputStream fileout = new FileOutputStream(file);
			PrintWriter stdout = new PrintWriter(fileout);

			for (Map.Entry<String, MyTreeNode<String>> listEntry : mapGroupFileTree.entrySet()) {
				stdout.println(listEntry.getKey() + "~"
						+ listEntry.getValue().getData().substring(0, listEntry.getValue().getData().length() - 2));
			}
			stdout.flush();
			stdout.close();
			fileout.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private void SkipToken() {
		String[] token = { "Identifier", "(", ")", "throws", ";", "{", "}", "class", "interface", "=", "]" };
		for (int i = 0; i < token.length; i++) {
			SkipTokens.add(token[i]);
		}
	}

	private void fillequateTokensTable() {
		for (Entry<Integer, String> tokenItemEntry : mapTokenIdToName.entrySet()) {
			tokenItem = new TableItem(tblEquateToken, SWT.NONE);
			tokenItem.setText(tokenItemEntry.getValue());
			mapTokenIdToEquateTokentbl.put(tokenItemEntry.getValue(), tokenItem);
		}
	}

	public static int getMessageType(IStatus status) {
		switch (status.getSeverity()) {
		case IStatus.ERROR:
			return IMessageProvider.ERROR;
		case IStatus.WARNING:
			return IMessageProvider.WARNING;
		case IStatus.INFO:
			return IMessageProvider.INFORMATION;
		}
		return IMessageProvider.NONE;
	}

	private boolean verifyAllFields(IMessageManager mmng) {

		boolean validateInput = true;

		if (textCloneRunName.getText().length() <= 0) {
			mmng.addMessage("textLength", "Field can not be empty", null, IMessageProvider.ERROR, textCloneRunName);
			validateInput =  false;
		}
		
		if(!(verifyDigitFields(textSimpleCloneThreshold, mmng) &&
		   verifyDigitFields(textMethodCloneThreshold, mmng) && 
		verifyDigitFields(textFileCloneThreshold, mmng) &&
		verifyDigitFields(textFileCloneTokenCoverage, mmng) &&
		verifyDigitFields(textMethodCloneTokenCoverage, mmng) )) {
			validateInput = false;
		}
		
		return validateInput;

	}

	private void configureFormText(final Form form, FormText text) {
		text.addHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				String is = (String) e.getHref();
				try {
					int index = Integer.parseInt(is);
					IMessage[] messages = form.getChildrenMessages();
					IMessage message = messages[index];
					Control c = message.getControl();
					((FormText) e.widget).getShell().dispose();
					if (c != null)
						c.setFocus();
				} catch (NumberFormatException ex) {
				}
			}
		});
		text.setImage("error", getImage(IMessageProvider.ERROR));
		text.setImage("warning", getImage(IMessageProvider.WARNING));
		text.setImage("info", getImage(IMessageProvider.INFORMATION));
	}

	private Image getImage(int type) {
		switch (type) {
		case IMessageProvider.ERROR:
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_ERROR_TSK);
		case IMessageProvider.WARNING:
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_WARN_TSK);
		case IMessageProvider.INFORMATION:
			return PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_OBJS_INFO_TSK);
		}
		return null;
	}

	private String createFormTextContent(IMessage[] messages) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		pw.println("<form>");
		for (int i = 0; i < messages.length; i++) {
			IMessage message = messages[i];
			pw.print("<li vspace=\"false\" style=\"image\" indent=\"16\" value=\"");
			switch (message.getMessageType()) {
			case IMessageProvider.ERROR:
				pw.print("error");
				break;
			case IMessageProvider.WARNING:
				pw.print("warning");
				break;
			case IMessageProvider.INFORMATION:
				pw.print("info");
				break;
			}
			pw.print("\"> <a href=\"");
			pw.print(i + "");
			pw.print("\">");
			if (message.getPrefix() != null)
				pw.print(message.getPrefix());
			pw.print(message.getMessage());
			pw.println("</a>");
		}
		pw.println("</form>");
		pw.flush();
		return sw.toString();
	}

	private boolean verifyDigitFields(Text field, IMessageManager mmng) {
		String s = field.getText();
		if (s.length() <= 0) {
			mmng.addMessage("textLength", "Enter a positive Integer greater than 0", null, IMessageProvider.ERROR,
					field);
			return false;
		}
		if (Integer.parseInt(s) == 0) {
			mmng.addMessage("textLength", "Enter a positive Integer greater than 0", null, IMessageProvider.ERROR,
					field);
			return false;
		} else {
			mmng.removeMessage("textLength", field);
			return true;
		}
	}

	private String checkOverlapingPaths(String newDirectory) {

		String[] newFolderStructure = newDirectory.split("\\\\");
		boolean overlap = true;
		for (Entry<String, MyTreeNode<String>> entry : mapGroupFileTree.entrySet()) {
			MyTreeNode<String> value = entry.getValue();
			String existingRootPath = value.getData().substring(0, value.getData().length() - 2);
			String[] exitingFolderStructure = existingRootPath.split("\\\\");
			String[] longerPath;
			String[] shorterPath;
			if (newFolderStructure.length >= exitingFolderStructure.length) {
				longerPath = newFolderStructure;
				shorterPath = exitingFolderStructure;
			} else {
				longerPath = exitingFolderStructure;
				shorterPath = newFolderStructure;
			}

			for (int i = 0; i < shorterPath.length; i++) {

				if (!longerPath[i].equals(shorterPath[i])) {
					overlap = false;
					break;

				}

			}
			if (overlap)
				return entry.getKey();

		}
		return null;
	}

	private void performCloneDetection() {

		writeCloneRunFiles();
		writeCloneRunFilesGroups();

		SysConfig.setEquateTokens(rulesId);

		for (int i = 0; i < SuppressedTokenID.size(); i++) {
			SysConfig.setSuppressTokenAt(SuppressedTokenID.get(i), true);
		}

		SysConfig.setProjectName(textCloneRunName.getText());
		SysConfig.setProjectDiscrp(textDescription.getText());

		SysConfig.setThreshold(Integer.parseInt(textSimpleCloneThreshold.getText()));

		SysConfig.setMinMClusT(Integer.parseInt(textMethodCloneThreshold.getText()));
		SysConfig.setMinMClusP(Integer.parseInt(textMethodCloneTokenCoverage.getText()));

		SysConfig.setMinFClusP(Integer.parseInt(textFileCloneThreshold.getText()));
		SysConfig.setMinFClusT(Integer.parseInt(textFileCloneTokenCoverage.getText()));

		SysConfig.setSystemTym();
		Job job = new Job("Clone Detection In Progress...") {

			@Override
			protected IStatus run(IProgressMonitor arg0) {

				initiateCloneDetection();
				syncWithUi();
				return Status.OK_STATUS;

			}
		};
 
		job.schedule();
		if (PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor() != null) {
			if (PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle()
					.equals("File Clone Editor")) {
				// cmt.cvac.clonecomparsion.FileClonePairEditor@211b2a2c
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor(), false);
			} else {
				IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
						.getEditors();
				for (int i = 0; i < editorlist.length; i++) {
					if (editorlist[i].getTitle().equals("Clone Detection")) {
						PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(editorlist[i],
								false);
						break;
					}
				}
			}

			// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().close
		}

//			page.close();

	}

	private void initiateCloneDetection() {
		CloneTrackingFormPage.isIncremental = false;
		CloneDetector.detectClones();
	}

	private void syncWithUi() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {

				/*
				 * MessageDialog.openInformation(PlatformUI.getWorkbench().
				 * getActiveWorkbenchWindow().getShell(), "Status",
				 * "Clone Detection has finished.");
				 */
				NotificationPopUpUI popup = new NotificationPopUpUI(
						/* NotificationQuartzJobHelper.getInstance().getCurrentDisp() */PlatformUI.getWorkbench()
								.getDisplay());
				popup.setMessage("Clone Detection Completed.");
				popup.open();
			}
		});

	}

}
