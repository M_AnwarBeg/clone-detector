package cmt.clonedetectionSettingsUI;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import cmt.common.Directories;
import cmt.common.ProjectImages;

public class TreeViewWithFolderStructure {

private static Map<String, TreeItem> nodes = new HashMap<>();
private static Map<TreeItem, List<String>> children = new HashMap<>();

private static Display display ;
private static Tree tree  ;
private static  Shell shell;
private static TreeItem treeItem; 
private static ArrayList<String> filesModified = new ArrayList<String>();


private static String javaExtension = "java"; 

//static HashMap<MyTreeNode<String>,TreeItem> mapping = new HashMap<MyTreeNode<String>,TreeItem>();
public static HashMap<TreeItem,MyTreeNode<String>> mapping = new HashMap<TreeItem,MyTreeNode<String>>();


	/*static void traverse(TreeItem treeRoot , MyTreeNode<String> folderRoot){
		String javaExtension = "java";
		for (MyTreeNode<String> child : folderRoot.getChildren()) {
			if (getExtension(child.getData()).equals(javaExtension)) {
				TreeItem leaf = new TreeItem(treeRoot, SWT.NONE | SWT.CHECK);
				leaf.setText(child.getData());
				leaf.setImage(new Image(display, "d:\\jcu_obj.png"));
			}
			else {
				   TreeItem childTreeItem = new TreeItem(treeRoot, SWT.NONE | SWT.CHECK );
				   childTreeItem.setText(child.getData());
				   childTreeItem.setImage(new Image(display, "d:\\folder.png"));
				   childTreeItem.setExpanded(true);
				   traverse(childTreeItem,child);
				   
				   
				   
				}
			}

		}
      */  
	static void traverse(TreeItem treeRoot , MyTreeNode<String> folderRoot, Display display){
		String javaExtension = "java";
		 
		if(folderRoot.getData().contains("\\")) {
			mapping.put(treeRoot,folderRoot);
		}
		
		for (MyTreeNode<String> child : folderRoot.getChildren()) {
			if (getExtension(child.getData()).equals(javaExtension)) {
				TreeItem leaf = new TreeItem(treeRoot, SWT.NONE | SWT.CHECK);
				leaf.setText(child.getData());
				child.setCorrespondingTreeItem(leaf);
				leaf.setImage(new Image(display, Directories.getAbsolutePath(ProjectImages.JAVA_ECLIPSE)));
				leaf.setChecked(child.getSelected());
				mapping.put(leaf,child);

				
			}
			else {
				   TreeItem childTreeItem = new TreeItem(treeRoot, SWT.NONE | SWT.CHECK );
				   childTreeItem.setText(child.getData());
				   childTreeItem.setImage(new Image(display,  Directories.getAbsolutePath(ProjectImages.FOLDER)));
				   childTreeItem.setExpanded(true);
					mapping.put(childTreeItem,child);
					traverse(childTreeItem,child,display); 
				}
			}

		}
	
	
	
	static void addItemToArrayList(ArrayList<String> list) {
		
		
		
		
	}
	
	
	static void checkItems(TreeItem item, boolean checked) {
	    item.setGrayed(false);
	    item.setChecked(checked);
	    MyTreeNode<String>  treeNode =  mapping.get(item); 
	    treeNode.setSelected(checked);
	    if(checked) {
	    	String filePath = constructPath(item);
	    	treeNode.setPath(filePath);
	    }
	    TreeItem[] items = item.getItems();
	    for (int i = 0; i < items.length; i++) {
	        checkItems(items[i], checked);
	    }
	}
	
	
	
	static void checkItems(TreeItem item, boolean checked, HashSet<String> fileschanged) {
	    item.setGrayed(false);
	    item.setChecked(checked);
	    MyTreeNode<String>  treeNode =  mapping.get(item); 
	    treeNode.setSelected(checked);
	    String filePath = constructPath(item);
	    if(checked) {
	    	treeNode.setPath(filePath);
	    }
	    
	    if(getExtension(filePath).equals(javaExtension)) {
	    	fileschanged.add(filePath);
	    }
	    
	    
	    TreeItem[] items = item.getItems();
	    for (int i = 0; i < items.length; i++) {
	        checkItems(items[i], checked);
	    }
	}
	
	
	
	
	

	/*static void writeCheckedFiles(MyTreeNode<String> root,PrintWriter stdout,boolean firstLine) { 
		String javaExtension = "java"; 
		for (MyTreeNode<String> child : root.getChildren()) { 
			if (getExtension(child.getData()).equals(javaExtension)) { 
				if(child.getSelected()) {
					if(firstLine) {
						stdout.print(constructPath(child)); 
						firstLine = false;

					}
					else {
						stdout.println();
						stdout.print(constructPath(child)); 
					}
				} 
					 
			} 
			else {  
				writeCheckedFiles(child,stdout,firstLine);  
				} 
			} 
	} */
	
	static void writeCheckedFiles(MyTreeNode<String> root,PrintWriter stdout) { 
		String javaExtension = "java"; 
		for (MyTreeNode<String> child : root.getChildren()) { 
			if (getExtension(child.getData()).equals(javaExtension)) { 
				if(child.getSelected()) { 
					stdout.println(constructPath(child)); 
				} 
					 
			} 
			else {  
				writeCheckedFiles(child,stdout);  
				} 
			} 
	} 
	
	
	
//	
//	static boolean reCreateCodeBase(MyTreeNode<String> root,HashSet<String> cloneRunfiles) {
//		String javaExtension = "java";
//		boolean checkEntireFolder = false;
//		for (MyTreeNode<String> child : root.getChildren()) {
//			if (getExtension(child.getData()).equals(javaExtension)) {
//					String filePath = constructPath(child);
//					if(cloneRunfiles.contains(filePath)) {
//						child.setSelected(true);
//					}
//					else
//						checkEntireFolder = false;
//					
//			}
//			else { 
//				boolean check = reCreateCodeBase(child,cloneRunfiles); 
//				if(check)
//					child.setSelected(true);
//			}
//					
//			}
//			
//		return checkEntireFolder;
//		
//	}
	
	
	
//	
//	static void checkItems(TreeItem item, boolean checked, ArrayList<String> filesModified) {
//	    item.setGrayed(false);
//	    item.setChecked(checked);
//	    TreeItem[] items = item.getItems();
//	    	String filePath = constructPath(item);
//	    	filesModified.add(filePath);
//	    for (int i = 0; i < items.length; i++) {
//	        checkItems(items[i], checked);
//	    }
//	}
	
	
	static String constructPath(TreeItem item)
	{	
		if(item == null)
			return new String();

		String itemPath = new String();
		if(item.getText().contains("\\")) {
			itemPath = item.getText().substring(0, item.getText().length()-2);
		}
		else 
			itemPath =  "\\" + item.getText();
			String subTreePath = constructPath(item.getParentItem()) ;
			return subTreePath + itemPath;
	}
	
	
	static String constructPath(MyTreeNode<String> item)
	{	
		if(item == null)
			return new String();

		String itemPath = new String();
		if(item.getData().contains("\\")) {
			itemPath = item.getData().substring(0, item.getData().length()-2);
		}
		else 
			itemPath =  "\\" + item.getData();
			String subTreePath = constructPath(item.getParent()) ;
			return subTreePath + itemPath;
	}
	
	
	
	
	
	
	static void expandAllTree(TreeItem item) {
		item.setExpanded(true);
 
	    TreeItem[] items = item.getItems();
	    for (int i = 0; i < items.length; i++) {
	    	expandAllTree(items[i]);
	    }
		
	}
	
	
	/* public void SkipToken() {
	    	String[] token = {"Identifier", "(", ")", "throws", ";", "{", "}", "class", "interface", "=", "]"};
	    	for(int i=0; i<token.length; i++) {
	    		SetSkipToken(token[i]);
	    		}
	    	}
*/
	public static String getExtension(String filename)
	{
		
		String ext;
		int dotPlace = filename.lastIndexOf('.');
		if(dotPlace==-1) {
			return "";
		}
		ext = filename.substring(dotPlace + 1);
		return ext;
	}
}