package cmt.clonedetectionSettingsUI;


import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.forms.HyperlinkSettings;
import org.eclipse.ui.forms.IFormColors;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.IMessage;
import org.eclipse.ui.forms.IMessageManager;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.events.ExpansionAdapter;
import org.eclipse.ui.forms.events.ExpansionEvent;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;

import cmt.cddc.clonedetectioninitializer.CloneDetector;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonerunmanager.CloneRunInfoReader;
import cmt.cddc.clonerunmanager.CloneRunInfoWriter;
import cmt.cddc.structures.sGroup;
import cmt.common.Directories;
import cmt.common.NotificationPopUpUI;
import cmt.common.ProjectImages;

import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.TreeViewer;

import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;


public class CloneTrackingFormPage extends FormPage {
	
	ScrolledForm form;
	// Form Widges
	private Text textCloneRunName;
	private Text textDescription;
	private Text textSimpleCloneThreshold;
	private Text textMethodCloneThreshold;
	private Text textFileCloneThreshold ;
	private Text textFileCloneTokenCoverage;
	private Text textMethodCloneTokenCoverage;
	private Text txtGroupName;
	private Text txtGroupDirectory;
	
	
	Button btnDeleteGroup; 
	
	
	private Table tblGroupNames; 
    private Table tableSuppresstokens;


    private Table tblEquateToken;
    private Table tblCreateRules;
	private Table tblRules;
	
	
    private Label lblCloneRunInfoError;
    
    
    private int activeGroupID = 0;
    private String activeGroupName = "";
  //  private HashSet<String> checkedFiles = new HashSet<String>();
  //  private HashSet<String> uncheckedFiles = new HashSet<String>();
    
    private Map<String,Integer> checkedFiles = new HashMap<String,Integer>();
    private Map<String,Integer> uncheckedFiles = new HashMap<String,Integer>();
    private Map<Integer, String> mapGroupIdToName = new HashMap<Integer,String>();
    
	
    TreeMap<Integer, sGroup> groups ; 
		
	private  ArrayList<Integer> SuppressedTokenID = new ArrayList<Integer>();
	private  ArrayList<ArrayList<Integer>> equateTokens = new ArrayList<ArrayList<Integer>>();
	private  int languageChoice;
	private  int minFClust;
	private  int minFClusp;
	private  int minMClust;
	private  int minMClusp;
	private  int threshold;
	private  String cloneRunName;
	private  String cloneRunDiscrp;
	private  String groupName;
	TreeViewer tree1;
	TableItem item, tokenItem;
	private Tree tree; 
	
	public static boolean isIncremental = false;

	private Map<String,  MyTreeNode<String>>  mapGroupFileTree = new TreeMap<String,  MyTreeNode<String>>();
	private HashMap<String, TableItem>  mapTokenIdToEquateTokentbl = new HashMap<String, TableItem>();
	//private HashSet<String>  allCloneRunFiles = new HashSet<String>();
	Hashtable<Integer,String> mapTokenIdToName= new Hashtable<Integer,String>();
	HashSet<String> setGroupNames = new HashSet<String>();
	private Map<String,Integer>  allCloneRunFiles = new HashMap<String,Integer>();
	Hashtable <Integer,ArrayList<String>> updatedGroupingInfo = new Hashtable<Integer, ArrayList<String>>();

	
	
	private int currentGroupId = -1;
    
    static ArrayList<ArrayList<String>> rules = new ArrayList<ArrayList<String>>();
	static ArrayList<ArrayList<Integer>> rulesId = new ArrayList<ArrayList<Integer>>();
	static ArrayList<ArrayList<String>> equateTokenRules = new ArrayList<ArrayList<String>>();
	
	   
	   
	static HashSet <String> newlyAddedFiles = new HashSet<String>();
	static ArrayList<Integer> deletedFileID = new ArrayList<Integer>();
	static ArrayList<String> deletedFiles = new ArrayList<String>();
	
	
	
	   int groupId = 0;
	
	
    private static org.eclipse.swt.graphics.Color gray;
	private static org.eclipse.swt.graphics.Color white;
    
    
    
    static ArrayList<String> SkipTokens = new ArrayList<String>();
    static ArrayList<Button> btn = new ArrayList<>();

        
    Button btnAddRule ;
    Button btnAddToken; 
     
    
    
    static int indexEquateRules = 0;
	/**
	 * Create the form page.
	 * 
	 * @param id
	 * @param title
	 */
	/*
	 * public test(String id, String title) { super(id, title); }
	 */

	public CloneTrackingFormPage(FormEditor editor) {
		super(editor, "first", "Clone Tracking"); 
	
	}

	/**
	 * Create the form page.
	 * 
	 * @param editor
	 * @param id
	 * @param title
	 * @wbp.parser.constructor
	 * @wbp.eval.method.parameter id "Some id"
	 * @wbp.eval.method.parameter title "Some title"
	 */
	public CloneTrackingFormPage(FormEditor editor, String id, String title) {
		super(editor, id, title);
	}

	/**
	 * Create contents of the form.
	 * 
	 * @param managedForm
	 */
	@Override
	protected void createFormContent(IManagedForm managedForm) {
		form = managedForm.getForm();
		FormToolkit toolkit = managedForm.getToolkit();
		toolkit.getHyperlinkGroup().setHyperlinkUnderlineMode(HyperlinkSettings.UNDERLINE_HOVER);
		form.setText("Clone Tracking Files");
		toolkit.decorateFormHeading(form.getForm());
		form.getForm().addMessageHyperlinkListener(new HyperlinkAdapter() {
			public void linkActivated(HyperlinkEvent e) {
				String title = e.getLabel();
				// String details = title;
				Object href = e.getHref();
				if (href instanceof IMessage[]) {
					// details =
					// managedForm.getMessageManager().createSummary((IMessage[])href);
				}
		
				Point hl = ((Control) e.widget).toDisplay(0, 0);
				hl.x += 10;
				hl.y += 10;
				Shell shell = new Shell(form.getShell(), SWT.ON_TOP | SWT.TOOL);
				shell.setImage(getImage(form.getMessageType()));
				shell.setText(title);
				shell.setLayout(new FillLayout());
				// ScrolledFormText stext = new ScrolledFormText(shell, false);
				// stext.setBackground(toolkit.getColors().getBackground());
				FormText text = toolkit.createFormText(shell, true);
				configureFormText(form.getForm(), text);
				// stext.setFormText(text);
				if (href instanceof IMessage[])
					text.setText(createFormTextContent((IMessage[]) href),
							true, false);
				shell.setLocation(hl);			
				shell.pack();
				shell.open();
			}
			
			
			
		});
		
		
		
		final IMessageManager mmng = managedForm.getMessageManager();
		Action myAction = new Action("My Action") {
		    public void run() {  
		    	saveCodeBaseChanges();
		    	trackClonesInUpdatedFiles();
		 
		    }
		};
		ImageDescriptor desc = ImageDescriptor.createFromFile(null, Directories.getAbsolutePath(ProjectImages.UPDATE_CLONES));

		myAction.setToolTipText("Track Clones");
		myAction.setImageDescriptor(desc);		

	//	myAction.setEnabled(false);

		
		
		form.getToolBarManager().add(myAction);
		form.getToolBarManager().add(new Separator());
		form.getToolBarManager().update(true);
		
		
		form.getToolBarManager().update(true);
		//form.setBackgroundImage(FormArticlePlugin.getDefault().getImage(
		//		FormArticlePlugin.IMG_FORM_BG));
		form.getForm().addMessageHyperlinkListener(new HyperlinkAdapter() {
			
		});
		
		managedForm.getForm().getBody().setLayout(new GridLayout(2, true));

		
		Section section = createSection(toolkit, form, "Clone Detection Files", 4);
		section.setDescription("Select Group-Wise Code Base for Clone Detection.");
		Composite sectionClient = toolkit.createComposite(section);
		section.setClient(sectionClient);
		section.setExpanded(true);

	
 
	//	Composite sectionGroupList = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout(3, false));

		
		GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		sectionClient.setLayout(gridLayout);
	    
		toolkit.createLabel(sectionClient, "Groups");		 
		toolkit.createLabel(sectionClient, "");		
		toolkit.createLabel(sectionClient, "Java Files");		 
		
		
		
	    tblGroupNames =  new Table(sectionClient, SWT.BORDER | SWT.V_SCROLL);
	    tblGroupNames.addListener(SWT.Selection, new Listener(){
	    		public void handleEvent(Event arg0) {
	    			displayTree();		
	    		}	
	    	});
		
		
	
		GridData gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
		gridData.heightHint=300;
		gridData.widthHint = 100;
		tblGroupNames.setLayoutData(gridData);
		
		btnDeleteGroup = new Button(sectionClient, SWT.PUSH);
		btnDeleteGroup.setText("Delete");
		btnDeleteGroup.setEnabled(false);
		gridData = new GridData(GridData.BEGINNING, GridData.BEGINNING, true, false);
		gridData.horizontalIndent = 4;
		btnDeleteGroup.setLayoutData(gridData);
		btnDeleteGroup.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(org.eclipse.swt.events.SelectionEvent arg0) {
			
			
				Shell s = new Shell();
				MessageBox messageBox = new MessageBox(s, SWT.ICON_QUESTION
			            | SWT.YES | SWT.NO);
				if(groups.size()==1)
			        messageBox.setMessage("Do you really want to delete Group? This will delete entire clone run");
				else
					messageBox.setMessage("Do you really want to delete Group?");
			        messageBox.setText("Group Deletion");
			        int response = messageBox.open();
			        
			        if (response == SWT.YES) {
			        	if(groups.size()==1) {
			        		// delete the data base and everything. Close the form. 
			        	}
			        	else {
			        	TableItem[] selection = tblGroupNames.getSelection();
			        	
						String groupName = selection[0].getText();  
						int deletedGroupId = -1 ; 
						for (Map.Entry<Integer, sGroup> entry : groups.entrySet()) {
						sGroup group = entry.getValue();
							if(group.getGroupName().equals(groupName)) {
								deletedGroupId = group.getGroupID();
								HashSet<String> groupFiles = group.getGroupFiles();
								for(String file: groupFiles) {
									Integer fileId = allCloneRunFiles.get(file);
									deletedFileID.add(fileId);
									deletedFiles.add(file);	
							}
						}
						}
						tblGroupNames.remove(tblGroupNames.getSelectionIndex());	
						CloneRunInfoWriter.deleteGroup(deletedGroupId);				
						groups.remove(deletedGroupId);
			        	}
			        }
			        
			        if (response == SWT.NO) {
			        	s.dispose();
			        }
		}
			
			@Override
			public void widgetDefaultSelected(org.eclipse.swt.events.SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	
		tree = new Tree(sectionClient, SWT.OPEN | SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		gridData = new GridData(GridData.FILL, GridData.FILL, true, true);
		gridData.heightHint=300;
		gridData.widthHint = 400;
		 //listHeight = fileList.getItemHeight() * 12;
		// trim = fileList.computeTrim(0, 0, 0, listHeight);
		//gridData.heightHint = trim.height;
		tree.setLayoutData(gridData);
		tree.addListener(SWT.Selection, new  Listener() {
		
		@Override
		public void handleEvent(Event event) {
			 if (event.detail == SWT.CHECK) {
			        TreeItem item = (TreeItem) event.item;
			        boolean checked = item.getChecked();
			        HashSet<String> filesChecked = new HashSet<String>();
			        TreeViewWithFolderStructure.checkItems(item, checked,filesChecked);
			        for(String file : filesChecked) {
			        	if(checked) {
			        		uncheckedFiles.remove(file);
			        		checkedFiles.put(file,currentGroupId);
			        	}	
			        	else {
			        		checkedFiles.remove(file);
			        		uncheckedFiles.put(file,currentGroupId);
			        	}
			    }
			
			 }
		}
	}); 
	
		setUpGroups();


		// Section Clone Run Information
		section = createSection(toolkit, form, "Clone Run Parameters", 2);
		section.setDescription("Clone Run Parameters");
		sectionClient = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout(2, false));


		
		toolkit.createLabel(sectionClient, "Name"); //$NON-NLS-1$
		toolkit.createLabel(sectionClient, "CloneRun45"); //$NON-NLS-1$

		toolkit.createLabel(sectionClient, "Description"); //$NON-NLS-1$
		toolkit.createLabel(sectionClient, "CloneRun45"); //$NON-NLS-1$

	
		Group simpleCloneSettingGroup = new Group(sectionClient, SWT.NONE);
		simpleCloneSettingGroup.setText("Simple Clone");
		GridLayout layout = new GridLayout();
		layout.numColumns = 2;
		simpleCloneSettingGroup.setLayout(layout);
		toolkit.createLabel(simpleCloneSettingGroup, "Minimum Similarity"); //$NON-NLS-1$
		textSimpleCloneThreshold = toolkit.createText(simpleCloneSettingGroup, "15");
		GridDataFactory.defaultsFor(textSimpleCloneThreshold).hint(20, SWT.DEFAULT).indent(5, 0).applyTo(textSimpleCloneThreshold);
		managedForm.getToolkit().adapt(simpleCloneSettingGroup);
		managedForm.getToolkit().paintBordersFor(simpleCloneSettingGroup);
		textSimpleCloneThreshold.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent event) {
		        event.doit = false;
		        char myChar = event.character;
		        // Allow 0-9
		        if (Character.isDigit(myChar))
		          event.doit = true;
		        // Allow backspace
		        if (myChar == '\b')
		          event.doit = true;
			}
		});
		
		textSimpleCloneThreshold.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textSimpleCloneThreshold, mmng) ;
			}
		});

		section.setClient(sectionClient);
		section.setExpanded(true);
 
		
		

		sectionClient.setLayout(new GridLayout(1, false));
		
		Group methodCloneSettingGroup = new Group(sectionClient, SWT.NONE);
		methodCloneSettingGroup.setText("Method Clone");
		layout = new GridLayout();
		layout.numColumns = 4;
		methodCloneSettingGroup.setLayout(layout);
		toolkit.createLabel(methodCloneSettingGroup, "Minimum Similarity"); 
		textMethodCloneThreshold = toolkit.createText(methodCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textMethodCloneThreshold).hint(20, SWT.DEFAULT).indent(5, 0).applyTo(textMethodCloneThreshold);

		textMethodCloneThreshold.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent event) {
		        event.doit = false;
		        char myChar = event.character;
		        // Allow 0-9
		        if (Character.isDigit(myChar))
		          event.doit = true;
		        // Allow backspace
		        if (myChar == '\b')
		          event.doit = true;
			}
		});
	
		textMethodCloneThreshold.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textMethodCloneThreshold, mmng) ;
			}
		});
		
		
		toolkit.createLabel(methodCloneSettingGroup, "Minimum Token Coverage");
		textMethodCloneTokenCoverage = toolkit.createText(methodCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textMethodCloneTokenCoverage).hint(20, SWT.DEFAULT).indent(5, 0).applyTo(textMethodCloneTokenCoverage);

		
		textMethodCloneTokenCoverage.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent event) {
		        event.doit = false;
		        char myChar = event.character;
		        // Allow 0-9
		        if (Character.isDigit(myChar))
		          event.doit = true;
		        // Allow backspace
		        if (myChar == '\b')
		          event.doit = true;
			}
		});
		
		
		
		textMethodCloneTokenCoverage.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textMethodCloneTokenCoverage, mmng) ;
			}
		});
		
		managedForm.getToolkit().adapt(methodCloneSettingGroup);
		managedForm.getToolkit().paintBordersFor(methodCloneSettingGroup);
		section.setClient(sectionClient);

		sectionClient.setLayout(new GridLayout(1, false));
		Group fileCloneSettingGroup = new Group(sectionClient, SWT.NONE);
		fileCloneSettingGroup.setText("File Clone");
		layout = new GridLayout();
		layout.numColumns = 4;
		fileCloneSettingGroup.setLayout(layout);
		toolkit.createLabel(fileCloneSettingGroup, "Minimum Similarity"); //$NON-NLS-1$
		textFileCloneThreshold = toolkit.createText(fileCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textFileCloneThreshold).hint(20, SWT.DEFAULT).indent(5, 0).applyTo(textFileCloneThreshold);

		textFileCloneThreshold.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent event) {
		        event.doit = false;
		        char myChar = event.character;
		        // Allow 0-9
		        if (Character.isDigit(myChar))
		          event.doit = true;
		        // Allow backspace
		        if (myChar == '\b')
		          event.doit = true;
			}
		});
		
		textFileCloneThreshold.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textFileCloneThreshold, mmng) ;
			}
		});
		
		
		
		
		toolkit.createLabel(fileCloneSettingGroup, "Minimum Token Coverage"); //$NON-NLS-1$
		textFileCloneTokenCoverage = toolkit.createText(fileCloneSettingGroup, "20");
		GridDataFactory.defaultsFor(textFileCloneTokenCoverage).hint(20, SWT.DEFAULT).indent(5, 0).applyTo(textFileCloneTokenCoverage);

		
		textFileCloneTokenCoverage.addVerifyListener(new VerifyListener() {
			
			@Override
			public void verifyText(VerifyEvent event) {
		        event.doit = false;
		        char myChar = event.character;
		        // Allow 0-9
		        if (Character.isDigit(myChar))
		          event.doit = true;
		        // Allow backspace 
		        if (myChar == '\b')
		          event.doit = true;
			}
		});
		
		textFileCloneTokenCoverage.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				// TODO Auto-generated method stub
				verifyDigitFields(textFileCloneTokenCoverage, mmng) ;
			}
		});
		
		
		
		
		managedForm.getToolkit().adapt(fileCloneSettingGroup);
		managedForm.getToolkit().paintBordersFor(fileCloneSettingGroup);
		section.setVisible(false);
		section.setClient(sectionClient);
			
	
		
	}

	private Section createSection(FormToolkit toolkit, ScrolledForm form, String title, int horizontalSpa1n) {

		Section section = toolkit.createSection(form.getBody(), Section.TWISTIE | Section.DESCRIPTION);

		section.setLayoutData(new GridData(SWT.LEFT, SWT.CENTER, true, false, 2, 2));
		section.setActiveToggleColor(toolkit.getHyperlinkGroup().getActiveForeground());
		section.setToggleColor(toolkit.getColors().getColor(IFormColors.SEPARATOR));
		section.redraw(); 
	//	section.siz
		//section.
		toolkit.createCompositeSeparator(section);
		FormText rtext = toolkit.createFormText(section, false);
		section.setClient(rtext);
		// loadFormText(rtext, toolkit);
		section.addExpansionListener(new ExpansionAdapter() {
			public void expansionStateChanged(ExpansionEvent e) {
				form.reflow(false);
			}
		});
		section.setText(title);
		return section;
	}

	private static void testGridLayout(final ScrolledForm form, final FormToolkit toolkit) {
		final ExpandableComposite exp = toolkit.createExpandableComposite(form.getBody(), ExpandableComposite.TREE_NODE
		// ExpandableComposite.NONE
		);

		Composite composite = toolkit.createComposite(exp);
		GridLayout gridLayout = new GridLayout();
		gridLayout.marginHeight = 20;
		gridLayout.marginWidth = 20;
		composite.setLayout(gridLayout);

	}
	
	static public Section createSection(FormToolkit toolkit, Composite parent, String title, int hSpan, int vSpan) {

		Section section = toolkit.createSection(parent, Section.SHORT_TITLE_BAR);
		GridData osectionGridData = new GridData(SWT.FILL, SWT.BEGINNING, false, false);
		osectionGridData.horizontalSpan = hSpan;
		osectionGridData.verticalSpan = vSpan;
		osectionGridData.horizontalIndent = 5;
		section.setLayoutData(osectionGridData);
		section.setText(title);

		GridLayout osectionGL = new GridLayout(1, true);
		osectionGL.marginHeight = 0;
		osectionGL.marginWidth = 0;
		section.setLayout(osectionGL);

		return section;
	}
	
	
	
	private void getInput() {
		
		cloneRunName = textCloneRunName.getText();
		cloneRunDiscrp = textDescription.getText();
		threshold = Integer.parseInt(textSimpleCloneThreshold.getText());
		minMClust = Integer.parseInt(textMethodCloneThreshold.getText());
		minMClusp = Integer.parseInt(textMethodCloneTokenCoverage.getText());
		minFClust = Integer.parseInt(textFileCloneTokenCoverage.getText());
		minFClusp = Integer.parseInt(textFileCloneTokenCoverage.getText());
		
	/*
	 * 
	private Hashtable<Integer, String> token_details = new Hashtable<Integer, String>();
	private  ArrayList<Integer> SuppressedTokenID = new ArrayList<Integer>();
	private  ArrayList<ArrayList<Integer>> equateTokens;
	private  int languageChoice;	
	*/		
	}
	
	
	private void getSelectedFiles() {
	MyTreeNode<String> date ;	
		
		
	}
	
	
	private void displayTree() {
		tree.removeAll();   
		
	//	TreeViewWithFolderStructure.mapping.clear();
		TableItem[] selection = tblGroupNames.getSelection();
	    String groupName = selection[0].getText(); 
	    currentGroupId = getCurrentGroupId(groupName);
		btnDeleteGroup.setEnabled(true);

	    MyTreeNode<String> root =   mapGroupFileTree.get(groupName);	    
	    TreeItem parent = new TreeItem(tree, SWT.NONE | SWT.CHECK);
	    parent.setImage(new Image(form.getDisplay(), Directories.getAbsolutePath(ProjectImages.FOLDER)));
		parent.setText(root.getData());
		TreeViewWithFolderStructure.mapping.clear();
		TreeViewWithFolderStructure.traverse(parent,root,form.getDisplay());
		TreeViewWithFolderStructure.expandAllTree(parent);  
	}
	
	
	
	

	
	/*private void writeCloneRunFiles() {
			try
			{
				
				// itemPath = itemPath = item.getText().substring(0, item.getText().length()-2);
			    String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES);
			  //  System.out.println("Here: "+pathStr);
			    File file = new File(pathStr);
			    file.createNewFile();
			    FileOutputStream fileout = new FileOutputStream(file);
			    PrintWriter stdout = new PrintWriter(fileout);
	
			    for(Map.Entry<String,  MyTreeNode<String>>  listEntry : mapGroupFileTree.entrySet()){
			    	TreeViewWithFolderStructure.writeCheckedFiles(listEntry.getValue(),stdout);
			    	stdout.println(";");
		        } 
			    stdout.flush();
			    stdout.close();
			    fileout.close();
			}
			catch (Exception e)
				{
				    System.err.println(e.getMessage());
				    e.printStackTrace();
				}
		}
	
	private void writeCloneRunFilesGroups() {
		try
		{
			
			
			// itemPath = itemPath = item.getText().substring(0, item.getText().length()-2);
		    String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES_GROUP);
		  //  System.out.println("Here: "+pathStr);
		    File file = new File(pathStr);
		    file.createNewFile();
		    FileOutputStream fileout = new FileOutputStream(file);
		    PrintWriter stdout = new PrintWriter(fileout);

		    for(Map.Entry<String,  MyTreeNode<String>>  listEntry : mapGroupFileTree.entrySet()){
		    	stdout.println(listEntry.getKey() + "~" + listEntry.getValue().getData().substring(0, listEntry.getValue().getData().length()-2));
	        } 
		    stdout.flush();
		    stdout.close();
		    fileout.close();
		}
		catch (Exception e)
			{
			    System.err.println(e.getMessage());
			    e.printStackTrace();
			}
	}*/
	
	
	  
	  public static int getMessageType(IStatus status) {
			switch (status.getSeverity()) {
			case IStatus.ERROR:
				return IMessageProvider.ERROR;
			case IStatus.WARNING:
				return IMessageProvider.WARNING;
			case IStatus.INFO:
				return IMessageProvider.INFORMATION;
			}
			return IMessageProvider.NONE;
	  }
	 
	  private boolean verifyAllFields(IMessageManager mmng) {
		  
		  boolean validateInput = true;
		  
			if ( textCloneRunName.getText().length() <= 0) {
				mmng.addMessage("textLength",
						"Field can not be empty", null,
						IMessageProvider.ERROR, textCloneRunName);
		  	}
			
			
			return validateInput;
			
	  }
	  
	  private void configureFormText(final Form form, FormText text) {
			text.addHyperlinkListener(new HyperlinkAdapter() {
				public void linkActivated(HyperlinkEvent e) {
					String is = (String)e.getHref();
					try {
						int index = Integer.parseInt(is);
						IMessage [] messages = form.getChildrenMessages();
						IMessage message =messages[index];
						Control c = message.getControl();
						((FormText)e.widget).getShell().dispose();
						if (c!=null)
							c.setFocus();
					}
					catch (NumberFormatException ex) {
					}
				}
			});
			text.setImage("error", getImage(IMessageProvider.ERROR));
			text.setImage("warning", getImage(IMessageProvider.WARNING));
			text.setImage("info", getImage(IMessageProvider.INFORMATION));
		}
	  
	  private Image getImage(int type) {
			switch (type) {
			case IMessageProvider.ERROR:
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_ERROR_TSK);
			case IMessageProvider.WARNING:
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_WARN_TSK);
			case IMessageProvider.INFORMATION:
				return PlatformUI.getWorkbench().getSharedImages().getImage(
						ISharedImages.IMG_OBJS_INFO_TSK);
			}
			return null;
		}
	  
	 private String createFormTextContent(IMessage[] messages) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw); 
			pw.println("<form>");
			for (int i = 0; i < messages.length; i++) {
				IMessage message = messages[i];
				pw
						.print("<li vspace=\"false\" style=\"image\" indent=\"16\" value=\"");
				switch (message.getMessageType()) {
				case IMessageProvider.ERROR:
					pw.print("error");
					break;
				case IMessageProvider.WARNING:
					pw.print("warning");
					break;
				case IMessageProvider.INFORMATION:
					pw.print("info");
					break;
				}
				pw.print("\"> <a href=\"");
				pw.print(i+"");
				pw.print("\">");
				if (message.getPrefix() != null)
					pw.print(message.getPrefix());
				pw.print(message.getMessage());
				pw.println("</a>");
			}
			pw.println("</form>");
			pw.flush();
			return sw.toString();
		}
	  
	  
	 private void verifyDigitFields(Text field, IMessageManager mmng) {
			String s = field.getText();
			if (s.length() <= 0) {
				mmng.addMessage("textLength",
						"Enter a positive Integer greater than 0", null,
						IMessageProvider.ERROR, field);				
			}
			if(Integer.parseInt(s)==0) {
				mmng.addMessage("textLength",
						"Enter a positive Integer greater than 0", null,
						IMessageProvider.ERROR, field);
			}
			else {
				mmng.removeMessage("textLength", field);
			}
	 }
	 
	
	  private int getCurrentGroupId(String groupName) {
		  
		  
		  //    TreeMap<Integer, sGroup> groups ; 

		  for (Map.Entry<Integer, sGroup> entry : groups.entrySet()) {
			     Integer id = entry.getKey();
			     sGroup group = entry.getValue();
			     if(groupName.equals(group.getGroupName())){
			    		 return id;
			     }
		  }
		  return -1;
	  }
	  
	  
	  private void setUpGroups() {
		  groups = (TreeMap<Integer, sGroup>) CloneRunInfoReader.getGroups();
		  for (Map.Entry<Integer, sGroup> entry : groups.entrySet()) {
			    sGroup group = entry.getValue();
			    TableItem item = new TableItem(tblGroupNames, SWT.NONE);
				 MyTreeNode<String> root = IterateFolder.reCreateCodeBase(new File(group.getRootPath()),null,group.getGroupFileIds()); 
				 allCloneRunFiles.putAll(group.getGroupFileIds());
				 item.setText(group.getGroupName());
				 mapGroupFileTree.put(group.getGroupName(), root);
		} 
	  } 
	 
	  private void saveCodeBaseChanges() { 
		  clearStructures();
		  for (Map.Entry<String, Integer> entry : uncheckedFiles.entrySet()) {			  
		  String fileName = entry.getKey();  		  
		  if(allCloneRunFiles.containsKey(fileName)) { 
				  Integer fileId = allCloneRunFiles.get(fileName);
				  deletedFileID.add(fileId);
				  deletedFiles.add(fileName); }
		  }
		  for (Map.Entry<String, Integer> entry : checkedFiles.entrySet()) {			  
				String fileName = entry.getKey();  			  
				  if(!allCloneRunFiles.containsKey(fileName)) {
					   Integer groupId = entry.getValue();
					   newlyAddedFiles.add(fileName);
					   if(!updatedGroupingInfo.containsKey(groupId)) {
						  ArrayList<String> groupFiles = new ArrayList<String>();
						  groupFiles.add(fileName);
						  updatedGroupingInfo.put(groupId, groupFiles);
					  }
					  else {
						  updatedGroupingInfo.get(groupId).add(fileName);
					  }
			 }
		}
	  }
	  
	  
	  private void clearStructures() {
		  deletedFileID.clear();
		  deletedFiles.clear();
		  newlyAddedFiles.clear();
		  
		  
	  }
	  
	  
	  private void update() {
			if(!deletedFileID.isEmpty()) {
				CloneRunInfoWriter.deleteFileData(deletedFileID);
			}
			if(!updatedGroupingInfo.isEmpty()) {
				CloneRunInfoWriter.updateFileData(updatedGroupingInfo);
				CloneRunInfoWriter.updateTime();
			}
		}
	  
	  
	  
	  private void  trackClonesInUpdatedFiles() {
			update();
			 ArrayList<ArrayList<String>> cloneRunFiles =  CloneRunInfoReader.getCloneRunNames();
			 Vector<String[]> fileGroups = new Vector<String[]>();

			 for(ArrayList<String> groupFilesList : cloneRunFiles) {
			 	 String[] groupFilesArray = new String[groupFilesList.size()];
				 groupFilesArray = groupFilesList.toArray(groupFilesArray);
				 fileGroups.add(groupFilesArray);
			 }
			 CloneRunInfoWriter.writeCloneRunInputFiles(fileGroups);
			 isIncremental = true;
    		 CloneDetector.detectClones();
    		  
    		  
    		  if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor()!=null) {
  				if(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor().getTitle().equals("File Clone Editor")) {
  					//cmt.cvac.clonecomparsion.FileClonePairEditor@211b2a2c
  					PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor(), false);
  				}
  				else {
  					IEditorPart[] editorlist = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditors();
  					for(int i=0; i< editorlist.length;i++) {
  						if(editorlist[i].getTitle().equals("Clone Tracking")) { 
  							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().closeEditor(editorlist[i], false);
  							break;
  						}
  					}
  				}
  				
  				//PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().close
  			}
    		  
    		  
    		  
    		  
    		  
    		  
    		  
    		  NotificationPopUpUI popup = new  NotificationPopUpUI(/*NotificationQuartzJobHelper.getInstance().getCurrentDisp()*/PlatformUI.getWorkbench().getDisplay());
			  popup.setMessage("Clone Tracking Completed");
			  popup.open();
			
		/*	 Job job = new Job("Clone Tracking In Progress...") {

					@Override
					protected IStatus run(IProgressMonitor arg0) {

						initiateCloneTracking();
						syncWithUi();
						return Status.OK_STATUS;

					}
				};
				job.schedule();	 */
			 
  }
	 
	  private void initiateCloneTracking() {
		    isIncremental = true;
			CloneDetector.detectClones();
			System.out.println("Testing");
			
			
		}

		private void syncWithUi() {
			Display.getDefault().asyncExec(new Runnable() {
				public void run() {

					/*MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Status",
							"Clone Detection has finished.");*/
					NotificationPopUpUI popup = new  NotificationPopUpUI(/*NotificationQuartzJobHelper.getInstance().getCurrentDisp()*/PlatformUI.getWorkbench().getDisplay());
				   popup.setMessage("Clone Tracking Completed");
					popup.open();
				}
			});

		}

		
		public static ArrayList<String> getDeletedFileNames() {
			return deletedFiles;
		}
		
		public static HashSet<String> getNewlyAddedFiles(){
			return newlyAddedFiles;
		}
		
		public static int[] getDeletedFileID() {
			return deletedFileID.stream().mapToInt(i -> i).toArray();
		}
}
