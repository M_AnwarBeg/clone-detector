package cmt.clonedetectionSettingsUI;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.ui.PartInitException;

import org.eclipse.ui.forms.editor.FormEditor;

public class CloneDetectionFormEditor extends FormEditor {

	@Override
	protected void addPages() {
		
		try {
		//	addPage(new CloneTrackingFormPage(this));
			addPage(new CloneDetectionFormPage(this));
			
			}
			catch (PartInitException e) {
	}
		
		
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doSave(IProgressMonitor arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doSaveAs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isSaveAsAllowed() {
		// TODO Auto-generated method stub
		return false;
	}

}
