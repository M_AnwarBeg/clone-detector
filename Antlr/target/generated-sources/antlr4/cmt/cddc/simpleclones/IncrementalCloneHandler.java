package cmt.cddc.simpleclones;

import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonerunmanager.CloneRunConfiguration;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.cddc.structures.sMethod;
import cmt.clonedetectionSettingsUI.CloneTrackingFormPage;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.sMarkedMethod;
import cmt.cddc.structures.MCStruct;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;
import cmt.ctc.incrementalclones.TrackInfo;
import cmt.ctc.incrementalclones.Tracks;
import cmt.ctc.incrementalclones.TrackInfos;
public class IncrementalCloneHandler 
{
	static TrackInfos oldTracks = new TrackInfos();
	
	public static void compareCloneFiles(TrackInfos trackinfos, List<DBObject> rs) 
	{		
		boolean check = true;
		ArrayList<TrackInfo> newlyAddedTrackinfo = new ArrayList<TrackInfo>();
		if(rs.size() == 1)
		{
			trackinfos.setAllTracksMatchFlag(false);
			return;
		}			
		for(int y=0;y<trackinfos.getTrackInfosSize();y++)
		{	
			for(int z=0;z<trackinfos.getTrackAt(y).getTrackInstancesSize();z++)
			{
				for(int x=0;x<rs.size();x++)
				{
					DBObject DataObj = rs.get(x);
					if(DataObj.get("filename").equals(trackinfos.getTrackAt(y).getTrackInfoAt(z).filename) && 
							((Integer)(DataObj.get("sequenceindex")))==(trackinfos.getTrackAt(y).getTrackInfoAt(z).seqindex+1))
					{
						trackinfos.getTrackAt(y).getTrackInfoAt(z).match = true;
					}
				}
			}
		}
		
		int trueCount = 0 , newlyAddedTracksCount = 0;		
		for(int i=0;i<trackinfos.getTrackInfosSize();i++)
		{
			boolean marked = false;
			trueCount = 0;
			ArrayList<Integer> fileIds = new ArrayList<Integer>();
			for(int j=0;j<trackinfos.getTrackAt(i).getTrackInstancesSize();j++)
			{
				if(!trackinfos.getTrackAt(i).getTrackInfoAt(j).match)
				{
					trackinfos.getTrackAt(i).setMatchFlag(false);
					marked = true;
					//break;
				}
				else
				{
					if(!marked)
						trackinfos.getTrackAt(i).setMatchFlag(true);
					fileIds.add(trackinfos.getTrackAt(i).getTrackInfoAt(j).file_id);
					trueCount++;
				}
			}
			//To handle smaller clone extraction...
			if(!trackinfos.getTrackAt(i).isMatchFlag() && trueCount > 1 && !fileIds.containsAll(getFilesList(rs)))
			{
				ArrayList<TrackInfo> extractedTrackinfo = new ArrayList<TrackInfo>();
				for(int j=0;j<trackinfos.getTrackAt(i).getTrackInstancesSize();j++)
				{
					if(trackinfos.getTrackAt(i).getTrackInfoAt(j).match)
					{
						extractedTrackinfo.add(trackinfos.getTrackAt(i).getTrackInfoAt(j));
					}
				}
				trackinfos.addTrack(new Tracks(extractedTrackinfo));
				trackinfos.getTrackAt(trackinfos.getTrackInfosSize()-1).setMatchFlag(true);
				newlyAddedTracksCount++;
			}
		}
		
		if(trackinfos.getTrackInfosSize() > 0 && rs.size() < trackinfos.getTrackAt(trackinfos.getTrackInfosSize()-(newlyAddedTracksCount+1)).getTrackInfoSize())
        {
			for(int p = 0 ; p < trackinfos.getTrackInfosSize() - newlyAddedTracksCount ; p++)
			{
				if(trackinfos.getTrackAt(p).getTrackInstancesSize() == rs.size() && trackinfos.getTrackAt(p).isMatchFlag())
				{
					check = false;
					break;
				}
			}		
        }		
		
		if(check && trackinfos.getTrackInfosSize() > 0 && rs.size() < trackinfos.getTrackAt(trackinfos.getTrackInfosSize()-(newlyAddedTracksCount+1)).getTrackInfoSize())
		{			
			for(int i = 0 ; i < trackinfos.getTrackInfosSize() - newlyAddedTracksCount ; i++)
			{
				if(trackinfos.getTrackAt(i).containsAllFiles(getFilesList(rs)))
				{
					for(int j = 0 ; j < trackinfos.getTrackAt(i).getTrackInfoSize() ; j++)
					{		
						boolean match = false;
						for(int x=0 ; x < rs.size() ; x++)
						{
							DBObject DataObj = rs.get(x);
							
						    if(DataObj.get("filename").equals(trackinfos.getTrackAt(i).getTrackInfoAt(j).filename) && 
									((Integer)(DataObj.get("sequenceindex")))==(trackinfos.getTrackAt(i).getTrackInfoAt(j).seqindex+1))
						    {								  
							      match = true;
								  break;
						    }
					   }
					   if(match)
					   {
						   newlyAddedTrackinfo.add(trackinfos.getTrackAt(i).getTrackInfoAt(j));
					   }
				   }
				   if(newlyAddedTrackinfo.size() > 0)
				   {
					   trackinfos.addTrack(new Tracks(newlyAddedTrackinfo));
					   trackinfos.getTrackAt(trackinfos.getTrackInfosSize()-1).setMatchFlag(true);
					   break;
				   }
			   }											
		   }
		   
		   	
		}
		/*if(oldTracks != null && oldTracks.getTrackInfosSize() > 0 && rs.size() < oldTracks.getTrackAt(oldTracks.getTrackInfosSize()-1).getTrackInfoSize())
		{
			for(int i = 0; i < oldTracks.getTrackInfosSize() ; i++)
			{
				if(!oldTracks.getTrackAt(i).containsAllFiles(getFilesList(rs)))
					continue;
				boolean check = true;
				newlyAddedTrackinfo.clear();
				for(int j=0; j < oldTracks.getTrackAt(i).getTrackInfoSize() ; j++)
				{
					boolean match = false;	
					for(int x=0 ; x < rs.size() ; x++)
					{
						DBObject DataObj = rs.get(x);
						if(DataObj.get("filename").equals(oldTracks.getTrackAt(i).getTrackInfoAt(j).filename) && 
								((Integer)(DataObj.get("sequenceindex")))==(oldTracks.getTrackAt(i).getTrackInfoAt(j).seqindex+1))
						{
							match = true;
							newlyAddedTrackinfo.add(oldTracks.getTrackAt(i).getTrackInfoAt(j));
						}
					}
					if(!match)
					{
						check = false;
						break;
					}
				}
				if(check)
				{
					 trackinfos.addTrack(new Tracks(newlyAddedTrackinfo));
					 trackinfos.getTrackAt(trackinfos.getTrackInfosSize()-1).setMatchFlag(true);
					 break;
				}
			}
		}
		maintainOldTracks(rs);*/
		
	}
	public static ArrayList<Integer> MethodsFromFiles(ArrayList<Integer> fileIDs)
	{
		ArrayList<Integer> methodIDs = new ArrayList<>();
		try
		{
			MongoClient mongo = new MongoClient("localhost:27017");
			DB db = mongo.getDB("admin");
			DBCollection collection = db.getCollection("MethodInfo");
			for(Integer fileID :fileIDs)
			{
				BasicDBObject whereQuery = new BasicDBObject();  
				whereQuery.put("FileId", fileID);
				DBCursor cursor2 = collection.find(whereQuery);
				while (cursor2.hasNext()) 
				{
					DBObject obj2 = cursor2.next();
					methodIDs.add(Integer.parseInt(obj2.get("MethodId").toString()));
				}
			}

		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		return methodIDs;
	}
	private static ArrayList<Integer> getFilesList(List<DBObject> rs)
	{
		ArrayList<Integer> filesList = new ArrayList<Integer>();
		
		for(int i = 0 ; i < rs.size() ; i++)
		{
			DBObject DataObj = rs.get(i);
			int file_id = (int) DataObj.get("fileid");
			filesList.add(file_id);
		}
		return filesList;		
	}
	
	public static void maintainOldTracks(List<DBObject> rs)
	{
		if(rs.size() == 1)
			oldTracks.getTrackInfos().clear();
		else if(oldTracks.getTrackInfosSize() < 1 || oldTracks.getTrackAt(oldTracks.getTrackInfosSize() - 1).getTrackInfoSize() != rs.size())
			updateTrackInfo(oldTracks, rs);
	}
	
	public static void cloneDetected(ArrayList<SimpleClone> clone, ArrayList<TrackInfo> trackinfo, int class_id) 
    {
		int clone_id=1;
		for(int n=0; n<trackinfo.size(); n++) 
		{
			 clone.get(class_id).addSCCInstance(new SimpleCloneInstance(clone_id, trackinfo.get(n).filename, trackinfo.get(n).startsequenceindex , trackinfo.get(n).startindex, trackinfo.get(n).stopsequenceindex , trackinfo.get(n).stopindex,
					trackinfo.get(n).startline, trackinfo.get(n).endline,trackinfo.get(n).filename, 1,trackinfo.get(n).method_id , trackinfo.get(n).file_id));
			 //clone.add(new CloneInfo(class_id,clone_id,trackinfo.get(n).filename, trackinfo.get(n).startline,trackinfo.get(n).endline,trackinfo.get(n).startindex,trackinfo.get(n).stopindex,temptokens));
			 clone_id++;
		}
		//commented  addition of clone itself
		//clone.get(class_id).addInstance(new SimpleCloneInstance(clone_id, file, file_token,file_token, hashseq.get(k).token_stop, hashseq.get(k).token_stop,
		//		file_line, hashseq.get(k).start_line,file, 1));
		

		//clone.add(new CloneInfo(class_id,clone_id,file, file_line,hashseq.get(k).start_line,file_token,hashseq.get(k).token_stop,tokentrack));
		//return clone_id;
	}
    
    public static void updateTrackInfo(TrackInfos trackinfos, List<DBObject> rs) {    	
		ArrayList<TrackInfo> trackinfo = new ArrayList<TrackInfo>();
    	for(int n=0; n<rs.size(); n++) 
		{
			 DBObject obj=rs.get(n);
			 String file1=(String) obj.get( "filename" );
			 int startseqid=(int) obj.get( "sequenceindex" );
			 int stopseqid=startseqid+SysConfig.getThreshold() - 1;//(int) obj.get( "sequenceindex" );
			 //String hash=(String) obj.get( "sequencehash" );
			 int start_line=(int) obj.get( "startline" );
			 int stop_line=(int) obj.get( "stopline" );
			 int t_start=(int) obj.get( "startindex" );
			 int t_stop=(int) obj.get( "stopindex" );
			 int method_id=(int) obj.get( "methodid" );
			 int file_id = (int) obj.get("fileid");
			 trackinfo.add(new TrackInfo(file1,startseqid,stopseqid/*startseqid+32*/,start_line,stop_line,t_start,t_stop,method_id,startseqid,file_id));
		}
    	Tracks obj = new Tracks(trackinfo);
    	trackinfos.addTrack(obj);
	}
    
    public static void saveMethodList(ArrayList<sMethod> methods) throws UnknownHostException 
    {
    	if(CloneTrackingFormPage.isIncremental)
    	{
	    	MongoClient mongo = new MongoClient("localhost:27017");
	        DB db = mongo.getDB("admin");
	        DBCollection collection = db.getCollection("MethodInfo");
	    	if(methods!=null)
	        {
	        	for(sMethod m:methods)
	        	{  
					 	BasicDBObject document = new BasicDBObject();
					 	document.put("MethodName", m.getMethodName());
					 	document.put("MethodSignature", m.getMethodSig());
				        document.put("MethodId", m.getMethodId());
				        document.put("FileId", m.getFileID());
				        document.put("FilePath", m.getFilePath());
				        document.put("StartToken", m.getStartToken());
				        document.put("EndToken", m.getEndToken());
				        document.put("TokenCount", m.getEndToken() - m.getStartToken() + 1);/*m.getTokenCount());*/
				        collection.insert(document);
				       // InsertInDB(collection,document);
				} 
	        }
		//return 0;
    	}
    }
    
    public static void saveSimpleClones(ArrayList<SimpleClone> clone) throws UnknownHostException {
        if(clone!=null)
        {	
        	MongoClient mongo = new MongoClient("localhost:27017");
            DB db = mongo.getDB("admin");
            DBCollection col = db.getCollection("SimpleClones");
            //DBCollection col = data_base.getCollection("ByFiles");
			DBCursor dbcursor=col.find().sort( new BasicDBObject( "SCCId" , -1 ) ).limit(1);
			int SSCId=0;
 		      //  long end = System.currentTimeMillis();
				
				List<DBObject> res = new ArrayList<DBObject> (); 
				while (dbcursor.hasNext()) 
				{
					res.add(dbcursor.next());
				}
				for(int n=0; n<res.size(); n++) 
  				{
    	        	DBObject obj=res.get(n);
    	        	SSCId=(int) obj.get( "SSCId" );
  				}
				SSCId=SSCId+1;
     		for(SimpleClone c:clone){  
				 BasicDBObject document = new BasicDBObject();
				 //InsertThread T=new InsertThread(collection,s);
				// T.start();
				 	document.put("SSCId", SSCId);
			        document.put("RNR", c.getRNR());
			        //document.put("sequenceindex", c.get);
			        document.put("Instances", c.getInstancesSize());
			        col.insert(document);
			        DBCollection coll = db.getCollection("SimpleClonesInstances");
			        saveSimpleClonesInstance(c.getInstances(),SSCId,coll);
			        SSCId++;
			       // InsertInDB(collection,document);
			} 

        }
    	
       // return -1;// not there is list
    }
    
    public static int saveSimpleClonesInstance(ArrayList<SimpleCloneInstance> Instances,int SSCId,DBCollection collection) throws UnknownHostException {
        if(Instances!=null)
        {
        	for(SimpleCloneInstance c:Instances){  
				 BasicDBObject document = new BasicDBObject();
				 	document.put("SSCId", SSCId);
				 	document.put("InstanceId", c.getSCCID());
			        document.put("filename", c.getFileName());
			        document.put("methodId", c.getMethodId());
			        document.put("startCol", c.getStartCol());
			        document.put("endCol", c.getEndCol());
			        document.put("startLine", c.getStartLine());
			        document.put("endLine", c.getEndLine());
			        collection.insert(document);
			       // InsertInDB(collection,document);
			} 
        }
		return 0;
    }
    
    public static ArrayList<SimpleClone> pruneDuplicateClones(ArrayList<SimpleClone> clones) {		
		 int i=0;

		 List<SimpleClone> toRemove = new ArrayList<SimpleClone>();
		 for (SimpleClone c : clones) {
			 int j=0;
			 for (SimpleClone c1 : clones) {
				 if(j>i)
				 {
				    if (compareInstances(c.getInstances(), c1.getInstances())) {
				        toRemove.add(c1);
				        j--;
				    }
				 }
				 j++;
				}
			 i++;
			}
		 	clones.removeAll(toRemove);
		 
		 Iterator<SimpleClone> iter = clones.iterator();
		 Iterator<SimpleClone> iter1 = clones.iterator();
		 
		 while (iter.hasNext()) {
			 SimpleClone c = iter.next();
			 int j=0;
			 while (iter1.hasNext()) {
				 SimpleClone c1 = iter1.next();
				 //SimpleClone clone2 = clones.get(i);
				 if(j>i)
				 {
					 if(compareInstances(c.getInstances(), c1.getInstances()))
					 {
						 iter1.remove();
						 //c1.removeInstance(j);
						 //c.removeInstance(j);
						 //c1.removeInstance(j);
						 j--;
					 }
				 }
				 j++;
				 }
			 i++;
			 
		 }
		 return clones;
	 }
    
    public static boolean compareInstances(ArrayList<SimpleCloneInstance> instances_c, ArrayList<SimpleCloneInstance> instances_c1) {
		
		if(instances_c.size() == instances_c1.size())
		{
			boolean[] match_all = new boolean[instances_c.size()];
			
			for(int i=0; i<instances_c.size();i++)
			{
				if(instances_c.get(i).getFileName().equals(instances_c1.get(i).getFileName())&& instances_c.get(i).getStartLine()==instances_c1.get(i).getStartLine() && instances_c.get(i).getEndLine()==instances_c1.get(i).getEndLine() 
						&& instances_c.get(i).getMethodId()==instances_c1.get(i).getMethodId())
				{
					match_all[i]=true;
				}
			}
			boolean match=false;
			for(boolean m : match_all) 
			{
				if(!m) 
				{
					match= false;
					break;
				}
			else 
				{
					match= true;
				}
			}
		return match;
		}
		else
			return false;
	}

    @SuppressWarnings("unchecked")
    public static ArrayList<SimpleClone> filterIncrementalClones(ArrayList<SimpleClone> incrementalClones ,ArrayList<SimpleClone> clonesFromDB ,ArrayList<Integer> updatedOrNewlyAddedFileIds)
    {   
    	ArrayList<Integer> clonesToBeRemoved = new ArrayList<Integer>();
    	//File addition and removal scenario to be handled...
    	int maxSCCID = SimpleClonesReader.getMaxSCCId() + 1;    	
    	
    	ArrayList<SimpleClone> finalClones = new ArrayList<SimpleClone>();
    	for(int i = 0; i < incrementalClones.size() ; i++)
    	{
    		boolean caseCheck = false;
    		//Case 1: New clone detected comprising of only updated files or a single unmodified file...
    		//Case 2 : FILE ADDITION (New clones detected after files addition comprising of instances only of newly added file) 
    		//Becoz we have removed such clones from db at the start.. so we'll directly add them to our final clones without the need of comparison.
    		if(incrementalClones.get(i).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size() < 2)
    		{
    			finalClones.add(incrementalClones.get(i));
    			finalClones.get(finalClones.size() - 1).setSSCId(maxSCCID++);
    			continue;
    		}
    		ArrayList<Integer> incrementalClonesUnmodifedFiles = incrementalClones.get(i).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds);
    		
    		if(clonesFromDB != null && clonesFromDB.size() > 0)
    		{
				ArrayList<SimpleClone> clonesFromDB_copy = (ArrayList<SimpleClone>) clonesFromDB.clone();
				SimpleClone incrementalClone_copy = new SimpleClone(); 
				incrementalClone_copy.setInstances((ArrayList<SimpleCloneInstance>)incrementalClones.get(i).getInstances().clone());			
				
	    		for(int j = 0; j < clonesFromDB_copy.size() ; j++)
	    		{
	    			ArrayList<Integer> clonesFromDBUnmodifiedFiles = clonesFromDB_copy.get(j).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds);
	    			
	    			if(incrementalClonesUnmodifedFiles.size() == clonesFromDBUnmodifiedFiles.size()   //If unmodified files in both the clones are same, then we match their details i-e starting and ending index/column...
	    					&& incrementalClonesUnmodifedFiles.containsAll(clonesFromDBUnmodifiedFiles) 
	    					&& clonesFromDBUnmodifiedFiles.containsAll(incrementalClonesUnmodifedFiles))
	    			{
	    				boolean match = true;
	    				for(int k = 0 ; k < incrementalClonesUnmodifedFiles.size(); k++)
	    				{
	    					int sc1_index = incrementalClone_copy.getInstanceIndexOfFile(incrementalClonesUnmodifedFiles.get(k));
	    					int sc2_index = clonesFromDB_copy.get(j).getInstanceIndexOfFile(incrementalClonesUnmodifedFiles.get(k));
	    					
	    					SimpleCloneInstance sc1 = incrementalClone_copy.getInstanceAt(sc1_index);    					
	    					SimpleCloneInstance sc2 = clonesFromDB_copy.get(j).getInstanceAt(sc2_index);
	    					
	    					if(!SimpleCloneInstance.areSCCInstancesEqual(sc1, sc2)) //if all the details of unmodified files are matched then it would the same clone... 
	    					{
	    						match = false;
	    						break;
	    					}
	    					incrementalClone_copy.removeInstance(sc1_index);
	    					clonesFromDB_copy.get(j).removeInstance(sc2_index);
	    				}
	    				if(match)
	    				{
	    					// Case 2: No such change in modified files that affect clone details OR
							// New instance added 
							// Some instance removed
							    								
							finalClones.add(incrementalClones.get(i));
							finalClones.get(finalClones.size() -1).setSSCId(clonesFromDB.get(j).getSSCId());
							clonesToBeRemoved.add(clonesFromDB.get(j).getSSCId()); //this clone must be removed from DB as finalClones will be stored in DB...
							clonesFromDB.remove(j);
							j--;
							caseCheck = true;
							break;
	    				}
	    			}
	    			else
	    				continue;
	    		}
    		}
    		if(!caseCheck)
    		{
	    		//Case 3: New clone detected comprising of both modified and unmodified files...
    			//CASE 3A: Case, where an instance is added to an existing clone of only unmodified files...
    			
    			//CASE : FILE ADDITION (New clones detected after files addition)  

    			ArrayList<SimpleClone> sccFromDB = SimpleClonesReader.getClonesByUnModifiedFiles(incrementalClonesUnmodifedFiles); 
				ArrayList<SimpleClone> filteredClones = filterUnnecessaryClones(sccFromDB , incrementalClonesUnmodifedFiles);

				SimpleClone incrementalClone = new SimpleClone(); 
				incrementalClone.setInstances((ArrayList<SimpleCloneInstance>)incrementalClones.get(i).getInstances().clone());
				int matchedCloneId = -1;
				for(int f = 0 ; f < filteredClones.size() ; f++)
				{
					boolean match = true;
					if(incrementalClonesUnmodifedFiles.size() == filteredClones.get(f).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size())
					{						
						for(int g = 0 ; g < incrementalClonesUnmodifedFiles.size() ; g++)
						{
							int sc1index = incrementalClone.getInstanceIndexOfFile(incrementalClonesUnmodifedFiles.get(g));
							int sc2index = filteredClones.get(f).getInstanceIndexOfFile(incrementalClonesUnmodifedFiles.get(g));
							
							if(sc1index == -1 || sc2index == -1)
							{
								match = false;
								break;
							}
							
							SimpleCloneInstance sc1 = incrementalClone.getInstanceAt(sc1index);
							SimpleCloneInstance sc2 = filteredClones.get(f).getInstanceAt(sc2index);
	    					
	    					if(!SimpleCloneInstance.areSCCInstancesEqual(sc1, sc2)) //if all the details of unmodified files are matched then it would the same clone...    
	    					{
	    						match = false;
	    						break;
	    					}
	    					incrementalClone.removeInstance(sc1index);
	    					filteredClones.get(f).removeInstance(sc2index);	    
	    					matchedCloneId = filteredClones.get(f).getSSCId();
						}
					}
					if(match)
					{
						finalClones.add(incrementalClones.get(i));
						if(matchedCloneId != -1)
							finalClones.get(finalClones.size() - 1).setSSCId(matchedCloneId);
						//removing original from db...
						clonesToBeRemoved.add(filteredClones.get(f).getSSCId());	
						
						break;
					}
					else
					{
						filteredClones.remove(f);
						f--;
					}
				}	
				
				if(filteredClones.size() == 0) //new clone detected comprising of both modified and unmodified files
				{
					finalClones.add(incrementalClones.get(i));
					finalClones.get(finalClones.size()-1).setSSCId(maxSCCID++);
				}
    		}
    	}    	
    	//Case: Clone removal in case if the new clone detected through incremental comprises of large boundary than the 
    	//existing clone in DB with same no of instances of unmodified files..
    	if(clonesFromDB != null && clonesFromDB.size() > 0)
    	{    		
    		for(int x = 0 ; x < clonesFromDB.size() ; x++)
    		{
    			ArrayList<SimpleClone> clonesFromDB_copy = (ArrayList<SimpleClone>) clonesFromDB.clone();
    			for(int y = 0; y < incrementalClones.size() ; y++)
    			{
    				ArrayList<Integer> unModFiles = clonesFromDB.get(x).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds);
    				Collections.sort(unModFiles);
    				if(unModFiles.size() == incrementalClones.get(y).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size())
    				{
    					SimpleClone incrementalClone_copy = new SimpleClone(); 
    					incrementalClone_copy.setInstances((ArrayList<SimpleCloneInstance>)incrementalClones.get(y).getInstances().clone());    					
    					boolean match = true;					
    					for(int g = 0 ; g < unModFiles.size() ; g++)
    					{
    						int sc1index = incrementalClone_copy.getInstanceIndexOfFile(unModFiles.get(g));
    						int sc2index = clonesFromDB_copy.get(x).getInstanceIndexOfFile(unModFiles.get(g));
    						
    						if(sc1index == -1 || sc2index == -1)
    						{
    							match = false;
    							break;
    						}
							SimpleCloneInstance sc1 = incrementalClone_copy.getInstanceAt(sc1index);
	    					SimpleCloneInstance sc2 = clonesFromDB_copy.get(x).getInstanceAt(sc2index);
	    					
	    					if(!SimpleCloneInstance.SCCInstanceContain(sc1, sc2))
	    					{
	    						match = false;
	    						break;
	    					}
	    					incrementalClone_copy.removeInstance(sc1index);
	    					clonesFromDB_copy.get(x).removeInstance(sc2index);
    					}
    					if(match)
    					{
    						clonesToBeRemoved.add(clonesFromDB.get(x).getSSCId()); //this clone must be removed from DB as a new clone covering larger boundary than this clone is already detected...
							clonesFromDB.remove(x);
							x--;
							break;
    					}    					
    				}    							
    			}
    		}
    	}
    	//Case : Instance Removal : (If an instance of modified file is not the part of a clone anymore)
    	//In such case we'll remove all such instances of modified files...
    	if(clonesFromDB != null && clonesFromDB.size() > 0)
    	{
	    	for(int x = 0 ; x < clonesFromDB.size() ; x++)
	    	{
	    		clonesToBeRemoved.add(clonesFromDB.get(x).getSSCId());
	    		
	    		for(int y = 0 ; y < clonesFromDB.get(x).getInstancesSize() ; y ++)
	    		{
	    			if(updatedOrNewlyAddedFileIds.contains(clonesFromDB.get(x).getInstanceAt(y).getFileId()))
	    			{
	    				clonesFromDB.get(x).removeInstance(y);
	    			}
	    		}
	    		finalClones.add(clonesFromDB.get(x));
	    	}
    	}
    	if(clonesToBeRemoved != null && clonesToBeRemoved.size() > 0)
    		SimpleCloneWriterDB.deleteSCCInstance(clonesToBeRemoved , false);
    	return finalClones;
    }
    
    @SuppressWarnings("unchecked")
	private static ArrayList<SimpleClone> filterUnnecessaryClones(ArrayList<SimpleClone> clones , ArrayList<Integer> unModFiles)
    {
    	ArrayList<Integer> files ;
    	for(int i = 0 ; i < clones.size() ; i++)
    	{
    		files = (ArrayList<Integer>) unModFiles.clone();
    		if(unModFiles.size() == clones.get(i).getInstancesSize())
    		{
	    		for(int j = 0; j < clones.get(i).getInstancesSize() ; j++)
	    		{
	    			if(files.contains(clones.get(i).getInstanceAt(j).getFileId()))
	    			{	    				
	    				files.remove(Integer.valueOf(clones.get(i).getInstanceAt(j).getFileId()));
	    			}
	    			else
	    			{
	    				clones.remove(i);
	    				i--;
	    				break;
	    			}
	    		}
    		}
    		else
    		{
    			clones.remove(i);
    			i--;    			
    		}
    	}
    	return clones;
    }

    public static void reorderSimpleClonesInstances(ArrayList<SimpleClone> clones , ArrayList<Integer> updatedOrNewlyAddedFileIds)
    { 
    	//set updated file instance as the first instance of each clone... 
    	for(int i = 0 ; i < clones.size() ; i++)
    	{
    		for(int j = 0 ; j < clones.get(i).getInstancesSize() ; j++)
    		{
    			if(updatedOrNewlyAddedFileIds.contains(clones.get(i).getInstanceAt(j).getFileId()))
    			{
    				if(j != 0)
    				{    					
    					SimpleCloneInstance inst = clones.get(i).getInstanceAt(0);
    					SimpleCloneInstance modifiedInst = clones.get(i).getInstanceAt(j);
    					clones.get(i).getInstances().set(0, modifiedInst);
    					clones.get(i).getInstances().set(j, inst);    					
    				}
    				break;
    			}
    		}
    	}
    }
    
    public static ArrayList<SimpleClone> extractClonesConsistingOfUpdatedOrNewlyAddedFilesOnly(ArrayList<SimpleClone> clones , ArrayList<Integer> updatedOrNewlyAddedFileIds)
    {
    	//extract the clones which consist of only the updated files...
    	ArrayList<SimpleClone> extractedClones = new ArrayList<SimpleClone>();
    	if(updatedOrNewlyAddedFileIds != null && updatedOrNewlyAddedFileIds.size() > 0)
    	{
	    	for(int i = 0 ; i < clones.size() ; i++)
	    	{
	    		if(clones.get(i).getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size() == 0)
	    		{
	    			extractedClones.add(clones.get(i));
	    			clones.remove(i);
	    			i--;
	    		}
	    	}
	    	/*for(int i = 0 ; i < clones.size() ; i++)
	    	{    		
	    		ArrayList<SimpleCloneInstance> instances = new ArrayList<SimpleCloneInstance>();
	    		for(int j = 0 ; j < clones.get(i).getInstancesSize() ; j++)
	    		{
	    			if(updatedFileIds.contains(clones.get(i).getInstanceAt(j).getFileId()))
	    			{
	    				instances.add(clones.get(i).getInstanceAt(j));
	    			}
	    		}
	            extractedClones.get(i).setInstances(instances);
	    	}*/
    	}	
    	return extractedClones;
    }
    public static Connection openConnection(String dbName)
	{
		Connection conn  = null;
		try
		{
			String url = "jdbc:mysql://localhost:3306/"+dbName;
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, "root", "root");
		} 
		catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException  e)
		{
			System.err.println("Cannot connect to database server" + e.getMessage());
			System.err.println("Error Code" + ((SQLException) e).getErrorCode() );

			//e.printStackTrace();
		}
		return conn;
	}

	public static  void closeConnection(Connection  conn)
	{
		if (conn != null)
		{
			try
			{
				conn.close(); 
				conn = null; 
			} 
			catch (Exception e)
			{ 
				/* ignore close errors */
			}
		}
	}
    public static ArrayList<Integer> WriteIncrementalMCCStructure(ArrayList<MethodClones> methodClusters,
			ArrayList<Integer> updatedOrNewlyAddedFileIds, Integer finalClones,
			MCStruct methodToCloneStrucuture) {
    	ArrayList<Integer> retVal = new ArrayList<>();
		
		Connection conn  = openConnection(CloneRepository.getDBName());
		for(Integer fileID : updatedOrNewlyAddedFileIds)
		{
			java.sql.Statement stmt;
			try {
				stmt = conn.createStatement();

				ResultSet result = stmt.executeQuery("SELECT mcc_id FROM mcc_instance where fid = "+fileID);

				ArrayList<Integer> mccIds = new ArrayList<>();
				while (result.next()) {
					if(!mccIds.contains(result.getInt(1)))
						mccIds.add(result.getInt(1));
				}
				stmt.executeUpdate("delete FROM mcc_instance where fid = "+fileID);
				stmt.executeUpdate("delete from mcc_file where fid="+fileID);
				for(Integer mcc : mccIds)
				{
					result = stmt.executeQuery("Select * from mcc_file where mcc_id = "+ mcc);
					int res = 0;
					while(result.next())
					{
						res++;
					}
					if(res <=1)
					{
						stmt.executeUpdate("delete from mcc_file where mcc_id = " +mcc);
					}
					stmt.executeUpdate("update mcc set members =members -1, atc = atc/2, apc = apc/2 where mcc_id = "+ mcc);
					stmt.executeUpdate("DELETE FROM mcc WHERE mcc_id = "+mcc+" AND members <= 2");
					result = stmt.executeQuery("select * from mcc_instance where mcc_id="+mcc);
					int instancecount= 0;
					while(result.next())
					{
						instancecount++;
					}
					if(instancecount<=1)
					{
						stmt.executeUpdate("delete from mcc_instance where mcc_id="+mcc);
						stmt.executeUpdate("delete from mcc_scc where mcc_id="+mcc);

					}
				}

			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(finalClones <= 0 || methodClusters.size() < 1)
		{
			return retVal;
		}
		//mcc_scc -> write mccid - > sccids
		//mcc_instance ->mcc_instance_id ,MID,mcc_id,tc,pc,fid,did,gid,synced   
		//remove the mcc data from db with fileID matching updatedornewlyaddedfileids
		int max_Mcc_id = 0;
		for(MethodClones mc : methodClusters)
		{
			ArrayList<Integer> sccInMC = mc.getvCloneClasses();
			if(methodToCloneStrucuture.IsInstanceAdditionCase(sccInMC))
			{
				Integer mcID = methodToCloneStrucuture.GetMCIDforSCC(sccInMC);
				ArrayList<Integer> existingMethodIDs = methodToCloneStrucuture.GetClusterMethodIDs(mcID);
				ArrayList<Integer> newMethods = mc.getvMethods();
				newMethods.removeAll(existingMethodIDs);
				java.sql.Statement stmt;
				try {
					stmt = conn.createStatement();
					int maxID= 0;
					ResultSet result = stmt.executeQuery("SELECT Max(mcc_instance_id) FROM mcc_instance where mcc_id = "+mcID);
					while(result.next())
					{
						maxID = result.getInt(1)+1;
					}
					int i = 0;
					ArrayList<Integer> fileIDsForMCC_ID = new ArrayList<>();
					for(Integer mccInstance : newMethods)
					{
						//insert into mcc_instance_id at htis point for newMethods..
						String INSERT_MCC_INSTANCE = "INSERT INTO mcc_instance(mcc_instance_id, mcc_id, mid, tc, pc, fid, did, gid) values ";
						INSERT_MCC_INSTANCE += "( \"" + maxID + "\" , \"" + mcID + "\", \"" + mccInstance + "\", \"" + sMethod_List.getMethodWithId(mccInstance).getTokenCount() + "\", \""
								+ mc.getvMethodCoverageAt(i) + "\", \"" + sMethod_List.getMethodWithId(mccInstance).getFileID() + "\", \"" + ProjectInfo.getDidFromFid(sMethod_List.getMethodWithId(mccInstance).getFileID()) + "\", \"" + ProjectInfo.getGidFromFid(sMethod_List.getMethodWithId(mccInstance).getFileID()) + "\"  )";


						stmt.executeUpdate(INSERT_MCC_INSTANCE);

						stmt.executeUpdate("UPDATE mcc SET atc = ((atc/members) *(members +1)) , apc = (apc/members)*(members +1), members =members +1 WHERE mcc_id  ="+ mcID);

						if(!fileIDsForMCC_ID.contains(sMethod_List.getMethodWithId(mccInstance).getFileID()))
							fileIDsForMCC_ID.add(sMethod_List.getMethodWithId(mccInstance).getFileID());
						maxID++;
						i++;
						if(!retVal.contains(mcID))
						{
							retVal.add(mcID);
						}
					}
					for(Integer f_id : fileIDsForMCC_ID)
					{
						String insert_mcc_file  = "Insert into mcc_file (mcc_id, fid) values";
						insert_mcc_file += "( \"" + mcID + "\" , \"" +f_id+ "\"  )";
						stmt.executeUpdate(insert_mcc_file);						
					}

				}
				catch(Exception ex)
				{
					ex.printStackTrace();
				}



			}
			else	
			{
				//cluster addition case//
				java.sql.Statement stmt = null;


				ResultSet result;
				try {
					stmt = conn.createStatement();
					result = stmt.executeQuery("SELECT Max(mcc_id) FROM mcc_instance");

					while(result.next())
					{
						max_Mcc_id = result.getInt(1) + 1;
					}
					ArrayList<sMarkedMethod> methodsInMCC = mc.getvMarkedMethods();
					int id = 0;
					ArrayList<Integer> fileIDsForMCC_ID = new ArrayList<>();
					Integer totalTokenCount = 0;
					float totalPTokenCov = 0;
					for(sMarkedMethod methodINMC : methodsInMCC)
					{
						//insert into mcc_instance_id at this point for newMethods..
						String INSERT_MCC_INSTANCE = "INSERT INTO mcc_instance(mcc_instance_id, mcc_id, mid, tc, pc, fid, did, gid) values ";
						INSERT_MCC_INSTANCE += "( \"" + id + "\" , \"" + max_Mcc_id + "\", \"" + methodINMC.getMethodID() + "\", \"" + sMethod_List.getMethodWithId(methodINMC.getMethodID()).getTokenCount() + "\", \""
								+ mc.getvMethodCoverageAt(id) + "\", \"" + sMethod_List.getMethodWithId(methodINMC.getMethodID()).getFileID() + "\", \"" + ProjectInfo.getDidFromFid(sMethod_List.getMethodWithId(methodINMC.getMethodID()).getFileID()) + "\", \"" + ProjectInfo.getGidFromFid(sMethod_List.getMethodWithId(methodINMC.getMethodID()).getFileID()) + "\"  )";
						if(!fileIDsForMCC_ID.contains(sMethod_List.getMethodWithId(methodINMC.getMethodID()).getFileID()))
							fileIDsForMCC_ID.add(sMethod_List.getMethodWithId(methodINMC.getMethodID()).getFileID());
						stmt.executeUpdate(INSERT_MCC_INSTANCE);
						totalTokenCount += sMethod_List.getMethodWithId(methodINMC.getMethodID()).getTokenCount();

						totalPTokenCov += mc.getvMethodCoverageAt(id);
						id++;
						if(!retVal.contains(max_Mcc_id))
						{
							retVal.add(max_Mcc_id);
						}
					}
					for(Integer f_id : fileIDsForMCC_ID)
					{
						String insert_mcc_file  = "Insert into mcc_file (mcc_id, fid) values";
						insert_mcc_file += "( \"" + max_Mcc_id + "\" , \"" +f_id+ "\"  )";
						stmt.executeUpdate(insert_mcc_file);						
					}
					for(Integer sccId : mc.getvCloneClasses())
					{
						String mcc_sccInsertQuery = "INSERT INTO mcc_scc(mcc_id, scc_id) values " + "(\"" + max_Mcc_id + "\" , \"" +sccId+ "\"  )";
						stmt.executeUpdate(mcc_sccInsertQuery);

					}
					String INSERT_MCC = "INSERT INTO mcc(mcc_id, atc, apc, members) values ";
					INSERT_MCC += "( \"" + max_Mcc_id + "\" , \"" +totalTokenCount/id+ "\", \"" + totalPTokenCov/id + "\", \"" + id + "\" )";
					stmt.executeUpdate(INSERT_MCC);						
					max_Mcc_id++;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		closeConnection(conn);
		return retVal;
	}
    public static ArrayList<Integer> WriteIncrementalFCCStructure(ArrayList<FileCluster> fileClusters,ArrayList<Integer> updatedOrNewlyAddedFileIds) {
    	ArrayList<Integer> retVal = new ArrayList<Integer>();
    	java.sql.Statement stmt;
    	try {
    		Connection conn  = openConnection(CloneRepository.getDBName());
    		stmt = conn.createStatement();
    		String query1 = "DELETE FROM fcc_instance WHERE";
    		String query2 = "SELECT MAX(fcc_instance_id) FROM fcc_instance WHERE fcc_id=";
    		String query3 = "SELECT MAX(fcc_id) FROM fcc_instance";
    		String query4 = "SELECT fcc_id FROM fcc_instance where";
    		ResultSet result;

    		for(Integer fileID : updatedOrNewlyAddedFileIds)
    		{
    			String queryData = "SELECT fcc_id FROM fcc_instance WHERE fid="+ fileID;
    			result = stmt.executeQuery(queryData);
    			ArrayList<Integer> fcc_ids = new ArrayList<>();

    			while(result.next())
    			{
    				if(!fcc_ids.contains(result.getInt(1)))
    					fcc_ids.add(result.getInt(1));
    			}

    			for(Integer fcc_id : fcc_ids)
    			{
    				String qu1 = "SELECT COUNT(fcc_id) FROM  fcc_instance WHERE fid= "+ fileID + " and fcc_id="+ fcc_id;
    				ResultSet result2 = stmt.executeQuery(qu1);
    				if(result2.next())
    					qu1 = "update fcc set atc = atc * members / members - "+ result2.getInt(1)+", apc = apc * members /members - "+result2.getInt(1)+", members = members - " + result2.getInt(1)+ " where fcc_id =  "+ fcc_id;
    				stmt.executeUpdate(qu1);
    			}
    		}

    		for(Integer fileID : updatedOrNewlyAddedFileIds)
    		{
    			query1 += " fid = "+ fileID + "|";
    			query4 += " fid = "+ fileID + "|";
    		}
    		query1 = query1.substring(0, query1.length() - 1);
    		query4 = query4.substring(0, query4.length() - 1);



    		ArrayList<Integer> fcc_id_against_updNwFiles = new ArrayList<>();
    		result = stmt.executeQuery(query4);

    		while(result.next())
    		{
    			fcc_id_against_updNwFiles.add(result.getInt(1));
    		}


    		stmt.executeUpdate(query1);
    		for(Integer fcc_id : fcc_id_against_updNwFiles)
    		{
    			String query5 = "select count(fcc_instance_id) from fcc_instance where fcc_id = " + fcc_id;
    			result = stmt.executeQuery(query5);
    			if(result.next() && result.getInt(1) == 1)
    			{
    				query2 += fcc_id;
    				stmt.executeUpdate("delete from fcc_instance where fcc_id = "+ fcc_id);
    				stmt.executeUpdate("delete from fcc_scc where fcc_id = "+ fcc_id);
    				stmt.executeUpdate("delete from fcc where fcc_id = "+ fcc_id);
    				stmt.executeUpdate("delete from fcc_dir where fcc_id = "+ fcc_id);
    				stmt.executeUpdate("delete from fcc_group where fcc_id = "+ fcc_id);
    			}
    		}

    		HashMap<Integer,ArrayList<Integer>> fcc_id_to_scc_id = new HashMap<>();
    		result = stmt.executeQuery("select fcc_id,scc_id from fcc_scc");
    		if(result.getFetchSize() > 0)
    		{
    			while(result.next())
    			{
    				fcc_id_to_scc_id.putIfAbsent(result.getInt(1),new ArrayList<Integer>());
    				fcc_id_to_scc_id.get(result.getInt(1)).add(result.getInt(2));
    			}
    		}
    		// TODO handle fcc_cluster addition scenario || fcc_isntance addition scenario.
    		for(int i = 0 ; i < fileClusters.size(); i++)
    		{
    			FileCluster fc  = fileClusters.get(i);

    			boolean instanceAddition = false;
    			for(Integer fcc_id : fcc_id_to_scc_id.keySet())
    			{
    				if(fcc_id_to_scc_id.get(fcc_id).size() == fc.getvCloneClasses().size() && fcc_id_to_scc_id.get(fcc_id).containsAll(fc.getvCloneClasses()))
    				{
    					//case of instance addition//
    					//which instance to add ??//
    					result = stmt.executeQuery("select fid from fcc_instance where fcc_id ="+fcc_id);
    					ArrayList<Integer> fidsAlreadyInDB = new ArrayList<>();
    					if(result.getFetchSize() > 0)
    					{
    						while(result.next())
    						{
    							fidsAlreadyInDB.add(result.getInt(1));
    						}
    					}
    					query2+= ""+fcc_id;
    					result = stmt.executeQuery(query2);
    					int maxFcc_instance_id = 0;
    					if(result.next())
    					{
    						maxFcc_instance_id = result.getInt(1);
    					}
    					//fc.getvFiles().removeAll(fidsAlreadyInDB);
    					int instanceAdded = 0;
    					float _coverage = 0;
    					float _tokencount = 0;
    					String INSERT_FCC_INSTANCE = "INSERT INTO fcc_instance(fcc_instance_id, fcc_id, fid, tc, pc, did, gid) values ";
    					String INSERT_FCC_DIR = "INSERT INTO fcc_dir(fcc_id, did) values ";
    					String INSERT_FCC_GROUP = "INSERT INTO fcc_group(fcc_id, gid) values ";
    					for(int gi = 0 ; gi < fc.getvFileSize() ;gi++)
    					{
    						if(!fidsAlreadyInDB.contains(fc.getVFileAt(gi)))
    						{
    							int tokenCount = fc.getvFileTokenAt(gi);
    							float coverage = fc.getvFileCoverageAt(gi);
    							int file = fc.getVFileAt(gi);
    							//insert this record into db//
    							INSERT_FCC_INSTANCE += "( \"" + maxFcc_instance_id++ + "\" ,\"" + fcc_id + "\" , \"" + file + "\" , \"" + tokenCount
    									+ "\" , \"" + coverage + "\", \"" + ProjectInfo.getDidFromFid(file) + "\" , \"" + ProjectInfo.getGidFromFid(file) + "\"  ),";
    							//update members in fcc,atc,apc
    							instanceAdded++;
    							_coverage+= coverage;
    							_tokencount+= tokenCount;
    							INSERT_FCC_DIR += "( \"" + fcc_id + "\" , \"" + ProjectInfo.getDidFromFid(file) + "\"  ),";
    							INSERT_FCC_GROUP += "( \"" + fcc_id + "\" , \"" + ProjectInfo.getGidFromFid(file) + "\"  ),";
    							if(!retVal.contains(fcc_id))
    							{
    								retVal.add(fcc_id);
    							}
    						}
    					}
    					if(instanceAdded > 0)
    					{
    						String INSERT_FCC = "Update fcc set apc = apc * members +"+_coverage+ " / members + "+instanceAdded+", atc = atc * members + "+_tokencount +"/ members +" + instanceAdded +" , members = members+" + instanceAdded + " where fcc_id = "+ fcc_id;
    						stmt.executeUpdate(INSERT_FCC);
    						INSERT_FCC_INSTANCE = INSERT_FCC_INSTANCE.substring(0, INSERT_FCC_INSTANCE.length() - 1);
    						INSERT_FCC_DIR = INSERT_FCC_DIR.substring(0, INSERT_FCC_DIR.length() - 1);
    						INSERT_FCC_GROUP = INSERT_FCC_GROUP.substring(0, INSERT_FCC_GROUP.length() - 1);
    						stmt.executeUpdate(INSERT_FCC_INSTANCE);
    						stmt.executeUpdate(INSERT_FCC_DIR);
    						stmt.executeUpdate(INSERT_FCC_GROUP);
    					}
    				}
    				else
    				{
    					instanceAddition = false;
    				}
    			}
    			if(instanceAddition == false)
    			{
    				//add new cluster here
    				result = stmt.executeQuery(query3);
    				int max = 0;
    				if(result.getFetchSize() > 0)
    				{
    					max = result.getInt(1);
    				}
    				int fcc_id = max +1;
    				String INSERT_FCC_INSTANCE = "INSERT INTO fcc_instance(fcc_instance_id, fcc_id, fid, tc, pc, did, gid) values ";
    				String INSERT_FCC_DIR = "INSERT INTO fcc_dir(fcc_id, did) values ";
    				String INSERT_FCC_GROUP = "INSERT INTO fcc_group(fcc_id, gid) values ";
    				String INSERT_FCC_SCC = "INSERT INTO fcc_scc(scc_id,fcc_id) values";
    				String INSERT_FCC = "INSERT INTO fcc(fcc_id,atc,apc,members) values";
    				int j = 0;
    				float avgPCount = 0;
    				int avgTokenCount = 0;
    				for(j = 0 ; j < fc.getvFileSize() ; j++)
    				{
    					int tokenCount = fc.getvFileTokenAt(j);
    					float coverage = fc.getvFileCoverageAt(j);
    					int file = fc.getVFileAt(j);
    					//insert this record into db against fcc_id = max +1, j would be fcc_instance_id//
    					INSERT_FCC_INSTANCE += "( \"" + j + "\" ,\"" + fcc_id + "\" , \"" + file + "\" , \"" + tokenCount
    							+ "\" , \"" + coverage + "\", \"" + ProjectInfo.getDidFromFid(file) + "\" , \"" + ProjectInfo.getGidFromFid(file) + "\"  ),";
    					INSERT_FCC_DIR += "( \"" + fcc_id + "\" , \"" + ProjectInfo.getDidFromFid(file) + "\"  ),";
    					INSERT_FCC_GROUP += "( \"" + fcc_id + "\" , \"" + ProjectInfo.getGidFromFid(file) + "\"  ),";
    					avgPCount += coverage;
    					avgTokenCount += tokenCount;
    					if(!retVal.contains(fcc_id))
    					{
    						retVal.add(fcc_id);
    					}

    				}
    				avgPCount  = avgPCount/ fc.getvFileSize();
    				avgTokenCount = avgTokenCount / fc.getvFileSize();

    				for(int x = 0 ; x < fc.getvCloneClassSize() ; x++)
    				{

    					INSERT_FCC_SCC += "( \"" + fc.getvCloneClassAt(x) + "\" , \"" + fcc_id + "\"  ),";
    				}
    				INSERT_FCC += "( \"" + fcc_id + "\" , \"" + fc.getAverageTokenCount() + "\", \"" + avgPCount + "\", \"" + avgTokenCount + "\"  ),";
    				INSERT_FCC_INSTANCE = INSERT_FCC_INSTANCE.substring(0, INSERT_FCC_INSTANCE.length() - 1);
    				INSERT_FCC_DIR = INSERT_FCC_DIR.substring(0, INSERT_FCC_DIR.length() - 1);
    				INSERT_FCC_GROUP = INSERT_FCC_GROUP.substring(0, INSERT_FCC_GROUP.length() - 1);
    				INSERT_FCC_SCC = INSERT_FCC_SCC.substring(0, INSERT_FCC_SCC.length() - 1);
    				INSERT_FCC = INSERT_FCC.substring(0, INSERT_FCC.length() - 1);
    				stmt.executeUpdate(INSERT_FCC_INSTANCE);
    				stmt.executeUpdate(INSERT_FCC_DIR);
    				stmt.executeUpdate(INSERT_FCC_GROUP);
    				stmt.executeUpdate(INSERT_FCC_SCC);
    				stmt.executeUpdate(INSERT_FCC);




    			}
    		}

    	}
    	catch(Exception ex)
    	{
    		ex.printStackTrace();
    	}
    	return retVal;
    }
public static void WriteUpdatedMethodstoDB(ArrayList<Integer> updatedOrNewlyAddedFileIds,
		ArrayList<Integer> updatedOrNewlyAddedFileMethods) {
	java.sql.Statement stmt1;
	java.sql.Statement stmt2;
	try {
		Connection conn  = openConnection(CloneRepository.getDBName());
		stmt1 = conn.createStatement();
		stmt2 = conn.createStatement();
		
		for(int fid : updatedOrNewlyAddedFileIds)
		{
			String query1= "select mid from method_file where fid = "+ fid;
			ResultSet res= stmt1.executeQuery(query1);
			while(res.next())
			{
				String query2 = "delete from method where mid = " + res.getInt(1);
				stmt2.executeUpdate(query2);
			}
		}
		if(updatedOrNewlyAddedFileMethods.size() == 0)
		{
			return;
		}
		String INSERT_METHOD = "INSERT INTO method(mid, mname, tokens, startline, endline) values ";
		String INSERT_METHOD_FILE = "INSERT INTO method_file(mid, fid, startline, endline) values ";
		for(int mid : updatedOrNewlyAddedFileMethods)
		{
			sMethod method = sMethod_List.getMethodWithId(mid);
			INSERT_METHOD += "( \"" + mid + "\" , \"" + method.getMethodName() +"("+method.getMethodSig()+")" + "\", \"" + method.getTokenCount() + "\", \"" + method.getStartToken() + "\", \""
					+ method.getEndToken() + "\"  ),";
			INSERT_METHOD_FILE += "( \"" + mid + "\" , \"" + method.getFileID() + "\", \"" + method.getStartToken() + "\", \"" + method.getEndToken() + "\"  ),";
		}
		INSERT_METHOD = INSERT_METHOD.substring(0, INSERT_METHOD.length() - 1);
		INSERT_METHOD_FILE = INSERT_METHOD_FILE.substring(0, INSERT_METHOD_FILE.length() - 1);
		stmt2.executeUpdate(INSERT_METHOD);
		stmt2.executeUpdate(INSERT_METHOD_FILE);
		conn.close();
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
}


}