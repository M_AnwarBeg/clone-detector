package cmt.cddc.simpleclones;
import java.util.ArrayList;

public class TokensList 
{
	private ArrayList<Tokens> tokens = new ArrayList<Tokens>();

	public ArrayList<Tokens> getTokens() {
		return tokens;
	}

	public void setTokens(ArrayList<Tokens> tokens) {
		this.tokens = tokens;
	}
	
	public Tokens getTokenAt(int index)
	{
         return tokens.get(index);
	}
	
	public void setTokenAt(int index ,Tokens t)
	{
	    tokens.set(index, t);
	}
			
	public void addTokenAt(int index,Tokens t)
	{
		tokens.add(index, t);
	}
	
	public void addToken(Tokens token)
	{
		tokens.add(token);
	}
	
	public void removeToken(int index)
	{
		tokens.remove(index);
	}
	
	public int getTokensListSize()
	{
		return tokens.size();
	}
	
	public int[] getAllTokenTypes()
	{
		int [] tokenTypes = new int[this.tokens.size()];
		for(int i=0 ;i< this.getTokensListSize() ; i++)
		{
			tokenTypes[i]=this.tokens.get(i).getType();
		}
		return tokenTypes;
	}
	public void clearTokensList()
	{
		tokens.clear();
	}
}
