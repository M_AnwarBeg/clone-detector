package cmt.cddc.simpleclones;
import java.util.ArrayList;


public class SimpleClone 
{
	private int SSCId;
	private ArrayList<SimpleCloneInstance> instances;
	private float RNR;
	private int instanceCount;
	private int benefit;
	
	public int getBenefit() {
		return benefit;
	}

	public void setBenefit(int benefit) {
		this.benefit = benefit;
	}


	private int tokenCount ;
	private ArrayList<Integer> tokensList;  //created for incremental clone detection...

	public SimpleClone()
	{
		this.instances = new ArrayList<SimpleCloneInstance>();
	}
	
	public SimpleClone(ArrayList<SimpleCloneInstance> instances)
	{
		this.instances = instances;
	}
	
	public SimpleClone(int cid,ArrayList<Integer> tokenList)
	{
		this.SSCId = cid;
		this.instances = new ArrayList<SimpleCloneInstance>();
		setTokensList(tokenList);
	}
	
	public float getRNR() {
		return RNR;
	}

	public void setRNR(float rNR) {
		RNR = rNR;
	}

	public int getSSCId() {
		return SSCId;
	}

	public void setSSCId(int sSCId) {
		SSCId = sSCId;
	}
	
	public ArrayList<SimpleCloneInstance> getInstances() {
		return instances;
	}

	public SimpleCloneInstance getInstanceAt(int index) {
		return instances.get(index);
	}
	
	public int getInstancesSize()
	{
		return instances.size();
	}
	
	public void removeInstance(int index)
	{
		instances.remove(index);
	}
	
	public void setInstances(ArrayList<SimpleCloneInstance> instances) {
		this.instances = instances;
	}
	
	public void setTokenCount(int tokenCount)
    {
	    this.tokenCount = tokenCount;
    }
	
	public void setSCCInstanceCount(int instanceCount)
	{
		this.instanceCount = instanceCount;
	} 

	
	 public int getTokenCount()
	 {
		return tokenCount;
	    }
	
	 public int getSCCInstanceCount()
	 {
		return instances.size(); //instanceCount;
	 }
	public ArrayList<SimpleCloneInstance> getSCCInstances()
	{
		return instances;
	}
	 
	 
	public void addSCCInstance(SimpleCloneInstance instances)
	{
		this.instances.add(instances);
	}

	public ArrayList<Integer> getTokensList() {
		return tokensList;
	}

	public void setTokensList(ArrayList<Integer> tokensList) {
		this.tokensList = tokensList;
	}  

	
	public ArrayList<Integer> getUnmodifiedFileIds(ArrayList<Integer> modifiedFiles)
	{
		ArrayList<Integer> fileIds = new ArrayList<Integer>();
		for(int i = 0 ; i< instances.size() ; i++)
		{
			if(!modifiedFiles.contains(instances.get(i).getFileId()))
				fileIds.add(instances.get(i).getFileId());
		}
		return fileIds;
	}
	
	public int getInstanceIndexOfFile(int fileId)
	{
		int selectedCloneIndex = -1;
		for(int i = 0 ; i < instances.size() ; i++)
		{
			if(instances.get(i).getFileId() == fileId)
			{
				if(selectedCloneIndex != -1)
				{
					if(instances.get(selectedCloneIndex).getStartLine() > instances.get(i).getStartLine())					
						selectedCloneIndex = i;					
				}
				else
					selectedCloneIndex = i;
			}
		}
		return selectedCloneIndex;
	}
}
