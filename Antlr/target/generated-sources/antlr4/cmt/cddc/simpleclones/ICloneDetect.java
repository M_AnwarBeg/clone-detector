package cmt.cddc.simpleclones;

import java.util.ArrayList;

import cmt.ctc.mongodb.AllFilesContent;

public interface ICloneDetect
{
	TokensList tokenizeInput(AllFilesContent inputFiles);
	ArrayList<SimpleClone> detectSimpleClone(TokensList tokensList);
}
