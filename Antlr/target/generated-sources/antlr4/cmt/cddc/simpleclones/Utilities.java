package cmt.cddc.simpleclones;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import cmt.cddc.clonedetectioninitializer.CloneDetector;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.psy.SuffixArrayCreation;
import cmt.common.Directories;

public final class Utilities 
{
	private Utilities()
	{
		
	}
	
	public static int[] createSuffixArray(int[] tokens)
    {
        SuffixArrayCreation suffix = new SuffixArrayCreation();
        int[] suffixArray = suffix.createSuffixArray(tokens);
        for(int i = 0 ; i < suffixArray.length ; i++)
        {
            System.out.println(suffixArray[i]);
        }
        return suffixArray;
    } 

	public static TokensList markRepetitions(TokensList tokensList)
    {
    	if(tokensList.getTokens() != null && tokensList.getTokensListSize() > 0)
    	{
	    	ArrayList<Integer> vPrev = new ArrayList<Integer>();
	    	ArrayList<Integer> vCurr = new ArrayList<Integer>();
	    	
	    	int curLine = tokensList.getTokenAt(0).getTokenLine();
	    	
	    	 //tokens at the start and end of the two lines
	    	 int startToken1 = 0;
	    	 int endToken1 = 0;
	    	 int startToken2 = 0;
	    	 int endToken2 =  0;
	    	//read the first line
	    	 while(tokensList.getTokenAt(endToken1).getTokenLine() == curLine && endToken1 < tokensList.getTokensListSize())
	    	 {
	    		 vPrev.add(tokensList.getTokenAt(endToken1).getType());    	
	    		 endToken1++;
	    	 }
	    	 startToken2 = endToken2 = endToken1--;
	    	 for (int i = endToken2; i < tokensList.getTokensListSize() - 2; i++) 
	    	 {
	    		 curLine = tokensList.getTokenAt(i).getTokenLine();
	    		//read the next line
	    		 while (tokensList.getTokenAt(i).getTokenLine() == curLine && i < tokensList.getTokensListSize()-2) //2) 
	    		 {
	    	            vCurr.add(tokensList.getTokenAt(i).getType());
	    	            i++;
	    	     }
	    		 endToken2 = i - 1;
	    		//if the two lines are equal
	    	    //see of the line size is less than threshold
	    	    //otherwise this repetitions is still a clone
	 	        if (vCurr.size() < SysConfig.getThreshold() && vCurr.equals(vPrev)) 
	 	            for (int k = startToken1; k <= endToken2; k++) 
	 	            	tokensList.getTokenAt(k).setTokenRepetition(true);	 	
	 	        
	 	        startToken1 = startToken2;
		        endToken1 = endToken2;
		        startToken2 = endToken1 + 1;
		        i--; //because of i++ in for loop
		        vPrev.clear();    
		        vPrev = new ArrayList<>(vCurr);
		        vCurr.clear();   
	    		 
	    	 }
    	}
    	//markRepititions2(tokensList);
    	return tokensList;
    }
	
	@SuppressWarnings("unused")
	private static TokensList markRepititions2(TokensList tokensList)
    {
    	ArrayList<Integer> vPrev = new ArrayList<Integer>();
    	ArrayList<Integer> vCurr = new ArrayList<Integer>();
    	
    	//int EOL = language->Instance()->getEOLToken();

        int startToken1 = 0;
        int endToken1 = 0;
        int startToken2 = 0;
        int endToken2 = 0;
        
        while (/*tokens.get(endToken1).getType() != EOL && */ endToken1 <= tokensList.getTokensListSize()) 
        {            
            vPrev.add(tokensList.getTokenAt(endToken1).getType());
            endToken1++;
        }
        startToken2 = endToken2 = endToken1 + 1;
        
        for (int i = endToken2; i < tokensList.getTokensListSize() - 2; i++) 
        {
        	while (/*vTokens.get(i) != EOL &&*/ i < tokensList.getTokensListSize() - 2) {
                vCurr.add(tokensList.getTokenAt(i).getType());                
                i++;
            }
            endToken2 = i;
            
            if (vCurr.size() < SysConfig.getThreshold() && vCurr == vPrev) 
                for (int k = startToken1; k <= endToken2; k++) 
                	tokensList.getTokenAt(k).setTokenRepetition(true);	 

            startToken1 = startToken2;
            endToken1 = endToken2;
            startToken2 = endToken1 + 1;
            vPrev.clear();
            vPrev = vCurr;
            vCurr.clear();
        }
        return tokensList;
    }
    
    public static TokensList markMultiLineRepititions(TokensList tokensList , int[] suffixArray)
    {
    	int diff1, diff2;
    	
    	 for (int i = 0; i < suffixArray.length - 2; i++) 
    	 {
    	        diff1 =  java.lang.Math.abs(suffixArray[i] - suffixArray[i + 1]);
    	        diff2 =  java.lang.Math.abs(suffixArray[i + 1] - suffixArray[i + 2]);

    	        if (diff1 < SysConfig.getThreshold() && diff1 == diff2) 
    	        {
    	            int j = i + 2;
    	            if(j != suffixArray.length - 1)
    	            {
	    	            while ( java.lang.Math.abs(suffixArray[j] - suffixArray[j + 1]) == diff1) 
	    	            {
	    	                j++;
	    	            }
    	            }
    	            for (int k = i; k <= j; k++) 
    	            {
    	                int index = suffixArray[k];
    	                tokensList.getTokenAt(index).setTokenRepetition(true);
    	            }
    	            i = j;
    	        }
    	    }
    	 return tokensList;
    }
    
    public static ArrayList<SimpleClone> checkIfDetectedCloneIsARepetition(ArrayList<SimpleClone> simpleClones,TokensList tokensList , boolean isIncremental , ArrayList<Integer> updatedOrNewlyAddedFileIds)
    {
    	int nRepetitions = 0;
    	int sccId = 0;
    	int noOfModifiedOrNewlyAddedFiles = 0 ,orgDiff = 0;
    	if(simpleClones != null && simpleClones.size() > 0)
    	{
	    	for(int index = 0 ; index < simpleClones.size() ; index++)
	      	{	    		
	    		SimpleClone clone = simpleClones.get(index);
	    		if(isIncremental)
	    		{
	    			noOfModifiedOrNewlyAddedFiles = clone.getInstancesSize() - clone.getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size();
	    		    orgDiff = clone.getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size() - noOfModifiedOrNewlyAddedFiles;
	    		}
	    		for(int instanceId = 0; instanceId < clone.getInstancesSize() ; instanceId++)
	    		{
	    			SimpleCloneInstance instance = clone.getInstanceAt(instanceId);
	    			if(isIncremental && !updatedOrNewlyAddedFileIds.contains(instance.getFileId()))
	    				continue;
	    			
	    			nRepetitions = 0;
	    			int start = instance.getStartingIndex();
	    			int end = instance.getEndingIndex();
	    			int length = end - start +1;
	    			
	    			for(int i=start; i<=end;i++)
	    			{
	    				if(tokensList.getTokenAt(i).getTokenRepetition() == true)
	    				{
	    					nRepetitions++;
	    				}
	    			}
	    			float RNR =(float) (1.0 - (float)nRepetitions / (float)length);	    			
	    			if(RNR <= SysConfig.getRNRThreshold()) //0.5)  //ignore this clone as repetition exceeds 50% of the detected clone...
	    			{
	    				clone.removeInstance(instanceId);
	    				instanceId--;
	    			}
	    			clone.setRNR(RNR);
	    		}
	    		if((!isIncremental && clone.getInstancesSize() <= 1) || (isIncremental && clone.getInstancesSize() - clone.getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size() <=1 && noOfModifiedOrNewlyAddedFiles != 1 && clone.getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size() != 0 && orgDiff != 1))
	    		{
	    			if(isIncremental && noOfModifiedOrNewlyAddedFiles== 1 && clone.getInstancesSize() - clone.getUnmodifiedFileIds(updatedOrNewlyAddedFileIds).size() != 0)
	    			{
	    				//do nothing...
	    			}
	    			else
	    			{
	    				simpleClones.remove(index);
	    				index--;
	    			}
	    		}
	    		else 
	    		{
	    			if(!isIncremental)
	    			{
	    				simpleClones.get(index).setSSCId(sccId);
	    				sccId++;
	    			}
	    		}
	        }
    	}
    	return simpleClones;
    }
    
    public static ArrayList<ArrayList<Integer>> initializeArrayList(ArrayList<ArrayList<Integer>> arr , int size)
    {
    	arr = new ArrayList<ArrayList<Integer>>();
    	for(int i=0;i< size ; i ++)
    	{
    		arr.add(new ArrayList<Integer>());
    	}
    	return arr;
    }
    
    public static boolean checkIfAnyFileIsUpdatedAfterExecDate(Date execDate) throws FileNotFoundException
    {
    	File f = new File(Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES));
    	if(f.isFile() && f.getAbsoluteFile() != null)
	    {
    		Scanner scanner = new Scanner(f);
			scanner.useDelimiter(";");  //Group of files separated by semi-colon(;) in InputFiles.txt...
	
			while(scanner.hasNext()) 
			{
				String input = scanner.next();
			    String [] files = input.split("\n");
			    for(int i=0 ; i < files.length ; i ++)
			    {
			    	String file = files[i];
			    	if(file.endsWith("\r"))		    		
			    		file = files[i].substring(0,files[i].length()-1);
			    	
			    	if(file != null && file != "" && file.length() > 1)
			    	{
			    		File fil = new File(file);				    	
				    	java.util.Date timestamp = new java.util.Date(fil.lastModified());
				    	if(execDate.before(timestamp))
				    		return false;
			    	}
			    }
			}
	    }
    	return true;
    }
}
