package cmt.cddc.simpleclones;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import org.antlr.v4.parse.ANTLRParser.exceptionGroup_return;

import antlr.auto.gen.java.JavaLexer;
import cmt.cddc.structures.MCStruct;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;
import cmt.common.Directories;

public final class SimpleCloneWriter 
{
	   public static void writeSimpleCloneFiles(ArrayList<SimpleClone> simpleClones,TokensList token_list , boolean isIncremental)
	   {
			createClones_File(simpleClones, token_list , isIncremental);
			if(!isIncremental)
				createClonesByFile_File(simpleClones , token_list);
			
			if(!isIncremental)
				createClonesByMethods_File(simpleClones);
			else
				createClonesByMethods_FileIncremental(simpleClones);
	        
	        createClonesRNR_File(simpleClones);	        
	   }
       
	   private static void createClones_File(ArrayList<SimpleClone> simpleClones,TokensList token_list, boolean isIncremental)
       {
    	   PrintWriter writer;
		   try 
		   {
			   writer = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_OUTPUT), "UTF-8");
			   
			   for(SimpleClone clone : simpleClones)
			   {			
				   if(clone == null || clone.getInstancesSize() == 0)
					   continue;
				   
					writer.print(clone.getSSCId() + ";" + clone.getInstanceAt(0).getInstanceLength() + ";" + clone.getInstancesSize() + ";");
					writer.printf("%.2f", clone.getRNR());
					writer.println();				  
				   
				   for(SimpleCloneInstance instance : clone.getInstances())
				   {					   
					   if(isIncremental)						   
						   writer.println(instance.getFileId() + ":" + instance.getStartLine() + "." + instance.getStartCol() + "-" + instance.getEndLine() + "." + instance.getEndCol());
					   else
					   {
						   Tokens startingToken = token_list.getTokenAt(instance.getStartingIndex());
						   Tokens endingToken = token_list.getTokenAt(instance.getEndingIndex());
						   
						   writer.println(startingToken.getTokenFile() + ":" + startingToken.getTokenLine() + "." + startingToken.getTokenColumn() + "-" + endingToken.getTokenLine() + "." + endingToken.getTokenColumn());
					   }
				   }
				   writer.println();
			   }
	    	   writer.close();
		   } 
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }    	  
       }
	   public static void CreateIncrementalMethodFiles(MCStruct methodStruct, ArrayList<Integer> newlyOrUpdatedFileMethods)
		{
		   if(newlyOrUpdatedFileMethods.size() == 0)
		   {
			   return;
		   }
			try {
				ArrayList<Integer> methodForFiles = new ArrayList<>();
				methodForFiles.addAll(newlyOrUpdatedFileMethods);
				for(Integer methodID : newlyOrUpdatedFileMethods)
				{
					if(methodStruct.ContainsMethod(methodID))
					{
						for(Integer sccID: methodStruct.GetSCCsforMethod(methodID))
						{

							for(Integer _methodID: methodStruct.GetMethodsBySCCID(sccID))
							{
								if(!methodForFiles.contains(_methodID))
								{
									methodForFiles.add(_methodID);
								}
							}
						}
					}
				}
				Collections.sort(methodForFiles);
				CreateMethod_FilesIncremental(methodStruct,methodForFiles);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	   private static void CreateMethod_FilesIncremental(MCStruct methodStruct, ArrayList<Integer> methodIDs)
		{
			PrintWriter writer_normalfile;
			try 
			{
				writer_normalfile = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_METHOD_NORMAL) , "UTF-8");
				for(int i = 0 ; i <= methodIDs.get(methodIDs.size()-1) ; i++)
				{
					if(!methodIDs.contains(i))
					{
						writer_normalfile.println();
					}
					else
					{
						for(Integer sccID : methodStruct.GetSCCsforMethod(i))
						{
							writer_normalfile.print(sccID*100 + " ");
						}
						writer_normalfile.println();
					}
				}
				writer_normalfile.close();
			}
			catch(Exception ex){
				System.out.println(ex.getMessage());
				ex.printStackTrace();
			}
		}
       private static void createClonesByFile_File(ArrayList<SimpleClone> simpleClones,TokensList token_list)
       {
    	   ArrayList<SimpleClone> simpleClones_temp = new ArrayList<SimpleClone>(simpleClones.size());
    	   SimpleCloneWriter.arrayList_deepCopy(simpleClones, simpleClones_temp);
    	   PrintWriter writer_file;
    	   PrintWriter writer_normalfile;
		   try 
		   {
			   writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_BY_FILE), "UTF-8");
			   writer_normalfile = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_BY_FILE_NORMAL) , "UTF-8");
			   
			   for(int fileId=0 ; fileId < sFile_List.getFilesListSize() ; fileId++)
			   {
				   for(int cloneId = 0; cloneId < simpleClones_temp.size() ; cloneId++)
				   {
					   SimpleClone clone = simpleClones_temp.get(cloneId);
					   int increment = 0;
					   
					   for(int instanceId=0; instanceId < clone.getInstancesSize() ; instanceId++)
					   {
						   SimpleCloneInstance instance = clone.getInstanceAt(instanceId);
						   int fid = -1;
						   if(instance.getStartingIndex() < token_list.getTokensListSize() && instance.getEndingIndex() < token_list.getTokensListSize())
						   {
							   fid = token_list.getTokenAt(instance.getStartingIndex()).getTokenFile();
						   }
						   else
						   {
							   fid = instance.getFileId();
						   }
						   if(/*token_list.getTokenAt(instance.getStartingIndex()).getTokenFile()*/ fid == fileId /*&& token_list.getTokenAt(instance.getEndingIndex()).getTokenFile() == fileId*/)
						   {
							   writer_file.print(clone.getSSCId() + ",");
							   int transformed_sccid = clone.getSSCId()*100 + increment++;
							   writer_normalfile.print(transformed_sccid + " ");
							   
							   clone.removeInstance(instanceId);
							   instanceId--;
						   }
					   }
					   if(clone.getInstancesSize() == 0)
					   {
						   simpleClones_temp.remove(cloneId);
						   cloneId--;
					   }
				   }
				   writer_file.println();
				   writer_normalfile.println();
			   }
			   writer_file.close();
			   writer_normalfile.close();
		   } 
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }      
       }

	   private static void createClonesByMethods_File(ArrayList<SimpleClone> simpleClones)
       {
    	   ArrayList<SimpleClone> simpleClones_temp = new ArrayList<SimpleClone>(simpleClones.size());
    	   SimpleCloneWriter.arrayList_deepCopy(simpleClones, simpleClones_temp);
    	   PrintWriter writer_method;
    	   PrintWriter writer_normalmethod;
    	   try 
		   {
    		   writer_method = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_METHOD), "UTF-8");
    		   writer_normalmethod = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_METHOD_NORMAL), "UTF-8");
    		   
    		   for(int methodId=0 ; methodId < sMethod_List.getMethodListSize() ; methodId++)
			   {
    			   sMethod method = sMethod_List.getsMethodAt(methodId);
				   for(int cloneId = 0; cloneId < simpleClones_temp.size() ; cloneId++)
				   {
					   SimpleClone clone = simpleClones_temp.get(cloneId);
					   int increment = 0;
					   
					   for(int instanceId=0; instanceId < clone.getInstancesSize() ; instanceId++)
					   {
						   SimpleCloneInstance instance = clone.getInstanceAt(instanceId);
						   
						   if(instance.getStartingIndex() >= method.getStartToken() && instance.getEndingIndex() <= method.getEndToken())
						   {
							   writer_method.print(clone.getSSCId() + ",");
							   int transformed_sccid = clone.getSSCId()*100 + increment++;
							   writer_normalmethod.print(transformed_sccid + " ");
							   
							   clone.removeInstance(instanceId);
							   instanceId--;
						   }
					   }
					   if(clone.getInstancesSize() == 0)
					   {
						   simpleClones_temp.remove(cloneId);
						   cloneId--;
					   }
				   }
				   writer_method.println();
				   writer_normalmethod.println();
			   }
    		   writer_method.close();
			   writer_normalmethod.close();
		   }
    	   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }  
    	   
       }

	   @SuppressWarnings("resource")
	private static void createClonesByMethods_FileIncremental(ArrayList<SimpleClone> simpleClones)
       {
		   HashMap<Integer,ArrayList<Integer>> methodToSCCMapping  = new HashMap<>();
		   for(SimpleClone sc : simpleClones)
		   {
			   for(SimpleCloneInstance sci : sc.getInstances())
			   {
				   methodToSCCMapping.putIfAbsent(sci.getMethodId(),new ArrayList<>());
				   methodToSCCMapping.get(sci.getMethodId()).add(sc.getSSCId());
			   }
		   }
		   int maxID = 0;
		   for(Integer key : methodToSCCMapping.keySet())
		   {
			   if(key >= maxID)
			   {
				   maxID = key;
			   }
		   }
		   try
		   {
			   int increment = 0 ;
			   PrintWriter writer_method;
	    	   PrintWriter writer_normalmethod;
	    	   writer_method = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_METHOD), "UTF-8");
    		   writer_normalmethod = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_METHOD_NORMAL), "UTF-8");

			   for(int i = 0 ; i <= maxID; i++)
			   {
				   if(methodToSCCMapping.containsKey(i))
				   {
					   Collections.sort(methodToSCCMapping.get(i));
					   int prevValue = 0;
					   for(Integer value : methodToSCCMapping.get(i))
					   {
						   writer_method.print(value + ",");
						   writer_normalmethod.print(value * 100 + increment++ +" ");
						   if(value != prevValue)
						   {
							   prevValue = value;
							   increment = 0;
						   }
							   
					   }

					   writer_method.println();
					   writer_normalmethod.println();
				   }
				   else
				   {
					   writer_method.println();
					   writer_normalmethod.println();
				   }
			   }
			   writer_method.close();
			   writer_normalmethod.close();
		   }
		   catch(Exception ex)
		   {
			   ex.printStackTrace();
		   }
		   
//		   ArrayList<SimpleClone> simpleClones_temp = new ArrayList<SimpleClone>(simpleClones.size());
//		   SimpleCloneWriter.arrayList_deepCopy(simpleClones, simpleClones_temp);
//		   PrintWriter writer_method;
//		   PrintWriter writer_normalmethod;
//		   int line_no = 1;
//		   try 
//		   {
//
//			   for(int methodId=0 ; methodId < sMethod_List.getMethodListSize() ; methodId++)
//			   {
//				   sMethod method = sMethod_List.getsMethodAt(methodId);
//				   for(int cloneId = 0; cloneId < simpleClones_temp.size() ; cloneId++)
//				   {
//					   SimpleClone clone = simpleClones_temp.get(cloneId);
//					   int increment = 0;
//
//					   for(int instanceId=0; instanceId < clone.getInstancesSize() ; instanceId++)
//					   {
//						   SimpleCloneInstance instance = clone.getInstanceAt(instanceId);
//
//						   if(instance.getStartingIndex() >= method.getStartToken() && instance.getEndingIndex() <= method.getEndToken())
//						   {
//							   while(line_no != method.getMethodId())
//							   {
//								   writer_method.println();
//								   writer_normalmethod.println();
//
//								   line_no++;
//							   }
//							   writer_method.print(clone.getSSCId() + ",");
//							   int transformed_sccid = clone.getSSCId()*100 + increment++;
//							   writer_normalmethod.print(transformed_sccid + " ");
//
//							   clone.removeInstance(instanceId);
//							   instanceId--;
//						   }
//					   }
//					   if(clone.getInstancesSize() == 0)
//					   {
//						   simpleClones_temp.remove(cloneId);
//						   cloneId--;
//					   }
//				   }
//				   writer_method.println();
//				   writer_normalmethod.println();
//			   }
//			   writer_method.close();
//			   writer_normalmethod.close();
//		   }
//		   catch (FileNotFoundException e) 
//		   {
//			   // TODO Auto-generated catch block
//			   e.printStackTrace();
//		   } 
//		   catch (UnsupportedEncodingException e) 
//		   {
//			   // TODO Auto-generated catch block
//			   e.printStackTrace();
//		   }  
    	   
       }
	   
       private static void createClonesRNR_File(ArrayList<SimpleClone> simpleClones)
       {
    	   PrintWriter writer;
		   try 
		   {
			   writer = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_RNR), "UTF-8");
			   
			   for(SimpleClone clone : simpleClones)					   				   				   
				   writer.println(clone.getRNR());
			   
	    	   writer.close();
		   } 
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }    	  
       }              
       
       private static void arrayList_deepCopy(ArrayList<SimpleClone> source , ArrayList<SimpleClone> dest)
       {
    	   for(SimpleClone clone : source)
    	   {   
    		   SimpleClone sc = new SimpleClone();
    		   ArrayList<SimpleCloneInstance> instances = new ArrayList<SimpleCloneInstance>();
    		   for(SimpleCloneInstance instance : clone.getInstances())
    		   {
    			   instances.add(instance);
    		   }   
    		   sc.setSSCId(clone.getSSCId());
    		   sc.setInstances(instances);    		   
    		   dest.add(sc);
    	   }
       }
       
       public static void writeInfileStructures()
       {
	       	PrintWriter writer;
	   		try 
	   		{
   			 writer = new PrintWriter(Directories.getAbsolutePath(Directories.IN_FILE_STRUCTURE), "UTF-8");
	   			 for (int f = 0; f < sFile_List.getFilesListSize(); f++) 
	   			 {
	   				 ArrayList<ArrayList<Integer>> repeats = new ArrayList<ArrayList<Integer>>();
	   				 repeats.add(new ArrayList<Integer>());
	   				 int repeatCount = 1;
	   				 for(int c = 0 ; c < sFile_List.getsFileAt(f).getCloneClassesSize() - 1; c++)
	   				 {
	   					 if(sFile_List.getsFileAt(f).getCloneClassAt(c)/100 == sFile_List.getsFileAt(f).getCloneClassAt(c+1)/100)
	   					 {
	   						 repeatCount++;
	   						 if(repeats.size() <= repeatCount)
	   							 repeats.add(new ArrayList<Integer>());	 
	   						 
	   						 repeats.get(repeatCount-1).add(sFile_List.getsFileAt(f).getCloneClassAt(c)/100);
	   					 }
	   					 else
	   						 repeatCount = 1;
	   				 }
	   				 for(int i = 0 ; i< repeats.size() ; i++)
	   				 {
	   					 if(repeats.get(i).size() <=0)
	   						 continue;
	   				     writer.print("(" + (i+1)+ ")");
	   					 
	   					 for(int j=0; j< repeats.get(i).size() ; j++)
	   						writer.print(repeats.get(i).get(j) + ",");							 
	   				 }
	   				 writer.println();
	   			 }			 
	   			 writer.close(); 
	   		}
	   		catch (FileNotFoundException e) 
	   	    {
	   		   // TODO Auto-generated catch block
	   	 	   e.printStackTrace();
	   	    } 
	   	    catch (UnsupportedEncodingException e) 
	   	    {
	   		    // TODO Auto-generated catch block
	   		    e.printStackTrace();
	   	    }   			
       }
       public static void writeInMethodStructures()
       {
	       	PrintWriter writer;
	   		try 
	   		{
	   		     writer = new PrintWriter(Directories.getAbsolutePath(Directories.IN_METHOD_STRUCTURE), "UTF-8");
		   		 for (int m = 0; m < sMethod_List.getMethodListSize() ; m++) 
	   			 {
		   			 ArrayList<ArrayList<Integer>> repeats = new ArrayList<ArrayList<Integer>>();
	   				 repeats.add(new ArrayList<Integer>());
	   				 int repeatCount = 1;
	   				 for(int c = 0 ; c < sMethod_List.getsMethodAt(m).getCloneClassSize() - 1; c++)
	   				 {
	   					 if(sMethod_List.getsMethodAt(m).getCloneClassAt(c)/100 == sMethod_List.getsMethodAt(m).getCloneClassAt(c+1)/100)
	   					 {
	   						 repeatCount++;
	   						 if(repeats.size() <= repeatCount)
	   							 repeats.add(new ArrayList<Integer>());	 
	   						 
	   						 repeats.get(repeatCount-1).add(sMethod_List.getsMethodAt(m).getCloneClassAt(c)/100);
	   					 }
	   					 else
	   						 repeatCount = 1;
	   				 }
	   				 for(int i = 0 ; i< repeats.size() ; i++)
	   				 {
	   					 if(repeats.get(i).size() <=0)
	   						 continue;
	   				     writer.print("(" + (i+1)+ ")");
	   					 
	   					 for(int j=0; j< repeats.get(i).size() ; j++)
	   						writer.print(repeats.get(i).get(j) + ",");							 
	   				 }
	   				 writer.println();
	   			 }
	   		     writer.close(); 
	   		}
	   		catch (FileNotFoundException e) 
	   	    {
	   		   // TODO Auto-generated catch block
	   	 	   e.printStackTrace();
	   	    } 
	   	    catch (UnsupportedEncodingException e) 
	   	    {
	   		    // TODO Auto-generated catch block
	   		    e.printStackTrace();
	   	    }   		
	   		
       }
       
       /*public static void writeInfileStructures()
       {
       	PrintWriter writer;
   		try 
   		{
   			 writer = new PrintWriter("output\\InfileStructures.txt", "UTF-8");
   			 for (int f = 0; f < sFile_List.getFilesListSize(); f++) {
   			        //fout<<vFiles[f].fileName<<":"
   			        //step 1: find the MAX_REPEAT
   			        int MAX_REPEAT = 0;
   			        int temp = 1;
   			        if (sFile_List.getsFileAt(f).getCloneClassesSize() == 0) 
   			        {
   			        	writer.println();
   			            continue;
   			        }
   			        for (int i = 0; i < sFile_List.getsFileAt(f).getCloneClassesSize() - 1; i++) 
   			        {
   			            if (sFile_List.getsFileAt(f).getCloneClassesSize() > i)//required???
   			            {
   			                if (sFile_List.getsFileAt(f).getCloneClassAt(i)/100 == sFile_List.getsFileAt(f).getCloneClassAt(i+1)/100) 
   			                {
   			                    temp++;
   			                }
   			                else 
   			                {
   			                    if (temp > MAX_REPEAT) 
   			                    {
   			                        MAX_REPEAT = temp;
   			                    }
   			                    temp = 1;
   			                }
   			            }
   			            if (temp > MAX_REPEAT) 
   			            {
   			                MAX_REPEAT = temp;
   			            }
   			        }
   			        //step 2: find 
   			        if (MAX_REPEAT > 1) 
   			        {
   			        	ArrayList<ArrayList<Integer>> vInFileStructures = null;
   			        	vInFileStructures = Utilities.initializeArrayList(vInFileStructures,MAX_REPEAT);
   			            for (int i = 1; i < MAX_REPEAT; i++) 
   			            {
   			                //dont print yet
   			                for (int j = 0; j < sFile_List.getsFileAt(f).getCloneClassesSize(); j++) 
   			                {
   			                    if (sFile_List.getsFileAt(f).getCloneClassesSize() > j + i) 
   			                    {
   			                        if (sFile_List.getsFileAt(f).getCloneClassAt(j)/100==sFile_List.getsFileAt(f).getCloneClassAt(j+i)/100) 
   			                        {
   			                            //dont print yet...store first	
   			                            vInFileStructures.get(i).add(sFile_List.getsFileAt(f).getCloneClassAt(j)/100);			                                    
   			                            j = j + i;
   			                        }
   			                    }
   			                }
   			            }
   			          //k should not exceed the limit...   
   			            for (int k = 0; k < vInFileStructures.size() - 1; k++) {
   			                if (vInFileStructures.get(k) == vInFileStructures.get(k + 1)) 
   			                {
   			                    continue;
   			                }
   			                else 
   			                {
   			                    if (vInFileStructures.get(k).size()>0) 
   			                    {
   			                    	writer.print("(" + (k + 1) + ")");			                        
   			                        for (int m = 0; m < vInFileStructures.get(k).size(); m++) 
   			                        {
   			                        	writer.print(vInFileStructures.get(k).get(m) + ",");
   			                        }
   			                    }
   			                }
   			            }
   			            //print the final one now
   			            int n = vInFileStructures.size() - 1;
   			            if (vInFileStructures.get(n).size() > 0) {
   			            	writer.print("(" + (n + 1) + ")");
   			                for (int p = 0; p < vInFileStructures.get(n).size(); p++) 
   			                {
   			                	writer.print(vInFileStructures.get(n).get(p) + ",");			                    
   			                }
   			            }
   			        }
   			        writer.println();
   			    }
   			 writer.close();  
   		}		 
   	    catch (FileNotFoundException e) 
   	    {
   		   // TODO Auto-generated catch block
   	 	   e.printStackTrace();
   	    } 
   	    catch (UnsupportedEncodingException e) 
   	    {
   		    // TODO Auto-generated catch block
   		    e.printStackTrace();
   	    }   
       }*/
       
       public static void CreateClonesByFileStructureInc(HashMap<Integer, ArrayList<Integer>> result) {
   		// TODO Auto-generated method stub
   		PrintWriter writer_file;
   		PrintWriter writer_normalfile;
   		try 
   		{
   			writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_BY_FILE), "UTF-8");
   			writer_normalfile = new PrintWriter(Directories.getAbsolutePath(Directories.CLONES_BY_FILE_NORMAL) , "UTF-8");

   			int prev = -1;
   			int max = Collections.max(result.keySet());
   			for(int i = 0 ; i <= max ; i++)
   			{
   				if(result.containsKey(i))
   				{
   					int ix = 0;
   					Collections.sort(result.get(i));
   					for(Integer a : result.get(i))
   					{
   						writer_file.print(a +",");
   						if(prev == a)
   							ix++;
   						else
   							ix = 0;
   						writer_normalfile.print(a*100 + ix + " ");
   						prev = a;
   						
   					}
   					writer_file.println();
   					writer_normalfile.println();
   					prev = -1;
   				}
   				else
   				{
   					writer_file.println();
   					writer_normalfile.println();
   				}
   			}
   			writer_file.close();
   			writer_normalfile.close();
   		}
   		catch(Exception ex)
   		{
   			ex.printStackTrace();
   		}
   	}
}
