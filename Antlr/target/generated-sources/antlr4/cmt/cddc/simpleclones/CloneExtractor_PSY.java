package cmt.cddc.simpleclones;
import java.util.ArrayList;

import cmt.cddc.computeallruns.AllRuns;
import cmt.cddc.computeallruns.ComputeAllRuns;
import cmt.cddc.psy.PSY;
import cmt.cddc.psy.PSY_clones;
import cmt.cddc.structures.sFile_List;

public class CloneExtractor_PSY implements ICloneExtract
{  
	private ArrayList<SimpleClone> extractClones(ArrayList<PSY_clones> clonesMarking, int[] suffixArray, int[] lcp, TokensList token_list)
      {
		boolean check = true;
    	  ArrayList<SimpleClone> simpleClones = new ArrayList<SimpleClone>();
    	  int cloneId = 0;
    	  for(PSY_clones clone : clonesMarking)
      	  {		

    		  if(check)
    		  {
	    		  try
	    		  {
	    			  AllRuns ar = new AllRuns();   
	    			  float RNR=(float) 0.0;  // to be calculated ....
		    		  if(new ComputeAllRuns().isRepeatRepetitious(clone, RNR , suffixArray , token_list))
		    		  	  continue;
	    		  }
	    		  catch(UnsatisfiedLinkError e)
	    		  {
	    			  check = false;	    			  
	    		  }
    		  }
    		  SimpleClone c = new SimpleClone();
    		  c.setSSCId(cloneId++);
    		  ArrayList<SimpleCloneInstance> instances = new ArrayList<SimpleCloneInstance>();
    		  for(int i = clone.left_boundry ; i <= clone.right_boundry ; i++)
    		  {
    			  SimpleCloneInstance singleInstance= new SimpleCloneInstance();
    			  
    		 	  int startingIndex = suffixArray[i];
    			  int endingIndex = startingIndex + clone.length -1;
    			 
    			  singleInstance.setStartingIndex(startingIndex);
    			  singleInstance.setEndingIndex(endingIndex);
    			  singleInstance.setFileId(token_list.getTokenAt(startingIndex).getTokenFile());
    			  singleInstance.setMethodId(token_list.getTokenAt(startingIndex).getTokenMethod());
    			  
    			  instances.add(singleInstance);
    		  }
    		  c.setInstances(instances);
    		  simpleClones.add(c);
    	  }    	  
    	 
    	  return simpleClones;
      }

	public ArrayList<SimpleClone> extractSimpleClones(ArrayList<PSY_clones> clonesMarking, int[] suffixArray, TokensList token_list)
	{
		int cloneId = 0;
		ArrayList<SimpleClone> simpleClones = new ArrayList<SimpleClone>();
		for(PSY_clones clone : clonesMarking)
    	{	
			SimpleClone c = new SimpleClone();
  		  	c.setSSCId(cloneId++);
  		  	ArrayList<SimpleCloneInstance> instances = new ArrayList<SimpleCloneInstance>();
  		  	for(int i = clone.left_boundry ; i <= clone.right_boundry ; i++)
  		  	{
  		  		SimpleCloneInstance singleInstance= new SimpleCloneInstance();
  			  
  		  		int startingIndex = suffixArray[i];
  		  		int endingIndex = startingIndex + clone.length -1;
  			 
  		  		singleInstance.setStartingIndex(startingIndex);
  		  		singleInstance.setEndingIndex(endingIndex);
  		  		singleInstance.setFileId(token_list.getTokenAt(startingIndex).getTokenFile());
  		  		singleInstance.setMethodId(token_list.getTokenAt(startingIndex).getTokenMethod());
  			  
  		  		instances.add(singleInstance);
  		  	}
  		  	c.setInstances(instances);
  		  	simpleClones.add(c);			
    	}
		return simpleClones;
	}
	
	public static void checkForRuns(ArrayList<SimpleClone> clones , TokensList token_list)
	{
		for(int i = 0 ; i < clones.size() ; i++)
		{
			 try
   		     {
	    		  if(new ComputeAllRuns().isRepeatRepetitious(clones.get(i), token_list))
	    		  {
	    			  clones.remove(i);
	    			  i--;
	    		  }
   		     }
   		  	 catch(UnsatisfiedLinkError e)
   		     {
   		  		 return; 			  
   		     }
		}
	}
	    private int[] bwt(int[] tokens,int[] suffixArr)
	    {
	    	int[] bwt = new int[suffixArr.length + 1];
	    	for(int i = 0 ; i < suffixArr.length ; i++)
	    	{
	    		if(tokens[suffixArr[i]] != -1 && suffixArr[i] != 0)
	    			bwt[i] = tokens[suffixArr[i] - 1];
	    		else
	    			bwt[i] = -1;
	    	}
	    	bwt[suffixArr.length] = -1;
			return bwt;
	    }
	    
	    
	    /**
	     * @param tokens
	     * @param suffixArr
	     * @return LCP using 
	     * kasai algorithm
	     */
	    
	    private int[] kasai(int[] tokens,int[] suffixArr)
	    {	    
	    	int N = suffixArr.length;
	    	int[] lcp = new int[N + 1];
	    	int [] inv = new int[N];
	    	
	    	for (int i = 0; i < N; i++) 
	    	   inv[suffixArr[i]] = i;
	    	for (int i = 0, len = 0; i < N; i++) 
	    	{
	    	    if (inv[i] > 0) 
	    	    {
	    	    	int k = suffixArr[inv[i]-1];
	    	    	
	    	        while( (i + len < N) && (k + len < N) && tokens[i+len] == tokens[k+len] ) 
	    	          len++;
	    	        
	    	        lcp[inv[i]] = len;
	    	        if (len > 0)      
	    	           len--;
	    	    }
	    	 }
	    	 lcp[0] = -1;
		     lcp[N] = -1;
	    	 return lcp;
	    	
		    /*int n = suffixArr.length; 	 
		    int[] lcp = new int[n + 1];
		 
		    
		     * An auxiliary array to store inverse of suffix
	         * elements.  For example if suffixArr[0] is 5, 
	         * theinvSuff[5] would store 0.  This is used to 
	         * get next suffix string from suffix array.
	         
		    int[] invSuff = new int[n];
		 
		    // Fill values in invSuff[]
		    for (int i=0; i < n; i++)
		        invSuff[suffixArr[i]] = i;
		 
		    // Initialize length of previous LCP
		    int k = 0;
		 
		    // Process all suffixes one by one starting from
		    // first suffix in txt[]
		    for (int i = 1 ; i < n ; i++)
		    {
		         
		         * If the current suffix is at n-1, then we don’t
		         * have next substring to consider. So lcp is not
		         * defined for this substring, we put -1. 
		         
		        if (invSuff[i] == n-1)
		        {
		            k = 0;
		            continue;
		        }
		 
		         
		         * j contains index of the next substring to
		         * be considered  to compare with the present
		         * substring, i.e., next string in suffix array 
		         
		        int j = suffixArr[invSuff[i] + 1];
		 
		         
		         * Directly start matching from k'th index 
		         * as at-least k-1 characters will match
		         * char[] chars=txt.toCharArray();
		         
		        while (i + k < n && j + k < n && tokens[i + k] == tokens[j + k])
		            k++;
		 
		        lcp[invSuff[i]+1] = k; // lcp for the present suffix.
		 
		        // Deleting the starting character from the string.
		        if (k>0)
		            k--;
		    }
		    
		    
		     * Last and first index of LCP would always
		     * have value -1 because for first index 
		     * there won't be any previous element so 
		     * there won't be any repeat & last index
		     * indicates the empty string.
		     
		    lcp[0] = -1;
		    lcp[n] = -1;
		    return lcp;*/
	    }	    	     	    	    
	    
	    /**
	     * It will first create the suffix array using DC3 
	     * algorithm then from this suffix array LCP is created
	     * further we create BWT and by using BWT & LCP clones 
	     * are detected using PSY algorithm in linear time.
	     */  
	   @Override
	   public ArrayList<SimpleClone> getSimpleClones(TokensList token_list) {
		// TODO Auto-generated method stub
		    int [] tokens = token_list.getAllTokenTypes();
	       
	        int[] tokens_$ = new int[tokens.length+1];
	        System.arraycopy(tokens, 0, tokens_$, 0, tokens.length);
	        tokens_$[tokens_$.length-1] = -1;
	        int[] suffixArray = Utilities.createSuffixArray(tokens);
	        
	        token_list = Utilities.markRepetitions(token_list);
	        //token_list = Utilities.markMultiLineRepititions(token_list,suffixArray);
	        
	        int[] lcp = kasai(tokens_$, suffixArray);
	        int[] bwt = bwt(tokens_$, suffixArray);
	        PSY psy = new PSY();
	        //code added for decoding of tokens
	        
	        ArrayList<PSY_clones> clones=psy.PSY_algo(suffixArray, bwt, lcp);
	       //// return Utilities.checkIfDetectedCloneIsARepetition(this.extractClones(clones ,suffixArray , lcp, token_list),token_list);
	        ArrayList<SimpleClone> simpleClones = this.extractSimpleClones(clones, suffixArray, token_list);
	        checkForRuns(simpleClones , token_list);
	        return Utilities.checkIfDetectedCloneIsARepetition(simpleClones , token_list  ,false , null);
	   }	   	   
}
