package cmt.cddc.simpleclones;

import java.util.ArrayList;

import cmt.cddc.antlrparser.AntlrParser;
import cmt.cddc.clonedetectioninitializer.BasicOutputWriter;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod_List;
import cmt.ctc.mongodb.AllFilesContent;

public class CloneDetector_PSY implements ICloneDetect
{
	@Override
	public TokensList tokenizeInput(AllFilesContent inputFiles)
    {
		AntlrParser antlrparser=new AntlrParser();              
        TokensList token_list = antlrparser.InitializeParser(inputFiles); 
        //updateTokenListWithMethodIds(token_list);
		return token_list;    	
    }
	
    @Override
    public ArrayList<SimpleClone> detectSimpleClone(TokensList tokens_list)
    { 
        ICloneExtract c = new CloneExtractor_PSY();
        ArrayList<SimpleClone> extractedClones = c.getSimpleClones(tokens_list);        
        return extractedClones;
    }
    
    public void updateTokenListWithMethodIds(TokensList token_list)
    {
    	for(int methodId = 0 ; methodId < sMethod_List.getMethodListSize() ; methodId++)
    	{
    		int startMethod = sMethod_List.getsMethodAt(methodId).getStartToken();
    		int endMethod = sMethod_List.getsMethodAt(methodId).getEndToken();
    		
    		for(int i = startMethod ; i <= endMethod ; i++)
    		{
    			token_list.getTokenAt(i).setTokenMethod(methodId);
    		}
    	}
    }
    //lists which clone classes are represented in each file
    public void updateFilesWithSimpleClones(ArrayList<SimpleClone> simpleClones)
    {
    	for (int i = 0; i < simpleClones.size(); i++) {
            for (int j = 0; j < simpleClones.get(i).getInstancesSize(); j++) {
                int normalCC = i * 100;
                int fileID = simpleClones.get(i).getInstanceAt(j).getFileId();
                int numberCC = sFile_List.getFileWithId(fileID).getCloneClassesSize();
                int lastCC = 0;
                if (numberCC > 0) {
                    lastCC = sFile_List.getFileWithId(fileID).getCloneClassAt(numberCC - 1);
                    if (lastCC / 100 == i) {
                        normalCC = lastCC + 1;
                    }
                }
                sFile_List.getFileWithId(fileID).addCloneClass(normalCC);
            }
        }
    }
    //lists which clone classes are represented in each method
    public void updateMethodsWithSimpleClones(ArrayList<SimpleClone> simpleClones)
    {
    	for (int i = 0; i < simpleClones.size(); i++) {
            for (int j = 0; j < simpleClones.get(i).getInstancesSize(); j++) {
                int normalCC = i * 100;
                int methodID = simpleClones.get(i).getInstanceAt(j).getMethodId();
                if (methodID >= 0) {
                    int numberCC = sMethod_List.getMethodWithId(methodID).getCloneClassSize();
                    int lastCC = 0;
                    if (numberCC > 0) {
                        lastCC = sMethod_List.getMethodWithId(methodID).getCloneClassAt(numberCC - 1);
                        if (lastCC / 100 == i) {
                            normalCC = lastCC + 1;
                        }
                    }
                    sMethod_List.getMethodWithId(methodID).addCloneClass(normalCC);
                }
            }
        }
    }
}
