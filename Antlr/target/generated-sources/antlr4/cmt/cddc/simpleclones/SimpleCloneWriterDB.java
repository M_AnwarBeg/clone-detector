package cmt.cddc.simpleclones;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import cmt.common.Directories;
import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonerunmanager.ProjectInfo;

public class SimpleCloneWriterDB {
	
		private static String INSERT_SCC_METHOD;
	 	private static String INSERT_SCC;
	    private static String INSERT_SCC_INSTANCE;
	    private static String INSERT_SCC_FILE;
	    
	    private static String INSERT_SCS_CROSSFILE;
	    private static String INSERT_SCSCROSSFILE_SCC;
	    private static String INSERT_SCSCROSSFILE_FILE;

	    private static String INSERT_SCS_INFILE;
	    private static String INSERT_SCSINFILE_SCC;
	    private static String INSERT_SCSINFILE_FILE;
	    private static String INSERT_SCSINFILE_FRAGMENTS;

	    private static String INSERT_SCS_CROSSMETHOD;
	    private static String INSERT_SCSCROSSMETHOD_SCC;
	    private static String INSERT_SCSCROSSMETHOD_METHOD;
	    public static final int FILE_TYPE = 21;
	    public static final int METHOD_TYPE = 21;
	    
	public static void InitQuery()
	{
		INSERT_SCC_METHOD = "INSERT INTO scc_method(scc_id, mid) values ";
		INSERT_SCSCROSSFILE_SCC = "INSERT INTO scscrossfile_scc(scc_id, scs_crossfile_id) values ";
		INSERT_SCSINFILE_SCC = "INSERT INTO scsinfile_scc(scc_id, scs_infile_id) values ";
		INSERT_SCS_CROSSFILE = "INSERT INTO scs_crossfile(scs_crossfile_id, atc, apc, members) values ";
		INSERT_SCS_CROSSMETHOD = "INSERT INTO scs_crossmethod(scs_crossmethod_id, atc, apc, members) values ";
		INSERT_SCS_INFILE = "INSERT INTO scs_infile(scs_infile_id, members) values ";
		INSERT_SCSCROSSFILE_FILE = "INSERT INTO scscrossfile_file(scs_crossfile_id, fid, tc, pc, did, gid) values ";
		INSERT_SCSCROSSMETHOD_METHOD = "INSERT INTO scscrossmethod_method(scs_crossmethod_id, mid, tc, pc, fid, did, gid) values ";
		INSERT_SCSCROSSMETHOD_SCC = "INSERT INTO scscrossmethod_scc(scs_crossmethod_id, scc_id) values ";
		INSERT_SCSINFILE_FILE = "INSERT INTO scsinfile_file(scs_infile_id, fid, did, gid) values ";
		INSERT_SCSINFILE_FRAGMENTS = "INSERT INTO scsinfile_fragments(scs_infile_id, fid, scc_id, scsinfile_instance_id, scc_instance_id) values ";

		INSERT_SCC_FILE = "INSERT INTO scc_file(scc_id, fid) values ";
		INSERT_SCC_INSTANCE = "INSERT INTO scc_instance(scc_instance_id, scc_id, fid, startline, startcol, endline, endcol, mid, did, gid) values ";
		INSERT_SCC = "INSERT INTO scc(scc_id, length, members, benefit, rnr) values ";
	}
	public static void executeTransaction(String sql)
    {
		try
			{
			    if (sql.indexOf("\"") != -1)
			    {
				sql = sql.substring(0, sql.length() - 1);
			    }
			    sql = sql + ";";
			    Statement st = CloneRepository.getConnection().createStatement();
			    st.execute("use "+ CloneRepository.getDBName()+";");
			    st.execute(sql);
			    st.close();
			} 
		catch (Exception e)
			{
			    System.err.println(e.getMessage());
			    e.printStackTrace();
			}
    }
	
	public static void insertSCC_Instance(int scc_instance_id, int scc_id, int fid, int startline, int startcol,
		    int endline, int endcol, int mid)
	    {
		INSERT_SCC_INSTANCE += "( \"" + scc_instance_id + "\" , \"" + scc_id + "\", \"" + fid + "\", \"" + startline
			+ "\", \"" + startcol + "\" , \"" + endline + "\", \"" + endcol + "\", \"" + mid + "\", \""
			+ ProjectInfo.getDidFromFid(fid) + "\", \"" + ProjectInfo.getGidFromFid(fid) + "\"  ),";
	    }
	
	public static void insertSCC_File(int scc_id, int fid)
    {
		INSERT_SCC_FILE += "( \"" + scc_id + "\" , \"" + fid + "\"  ),";
    }
	
	public static void insertSCC(int scc_id, int length, int members, int benefit, String rnr)
    {
		INSERT_SCC += "( \"" + scc_id + "\" , \"" + length + "\", \"" + members + "\", \"" + benefit + "\", \"" + rnr +"\" ),";
    }
	
	public static void insertSCSCrossFile_SCC(int scc_id, int scs_crossfile_id)
    {
	INSERT_SCSCROSSFILE_SCC += "( \"" + scc_id + "\" , \"" + scs_crossfile_id + "\"  ),";
    }
	
	public static void insertSCSCrossFile_File(int scs_crossfile_id, int fid, double tc, double pc)
    {
	INSERT_SCSCROSSFILE_FILE += "( \"" + scs_crossfile_id + "\" , \"" + fid + "\" , \"" + tc + "\" , \"" + pc
		+ "\", \"" + ProjectInfo.getDidFromFid(fid) + "\" , \"" + ProjectInfo.getGidFromFid(fid) + "\"  ),";
    }
	
	public static void insertSCS_CrossFile(int scs_crossfile_id, double atc, double apc, int members)
    {
	INSERT_SCS_CROSSFILE += "( \"" + scs_crossfile_id + "\" , \"" + atc + "\", \"" + apc + "\", \"" + members
		+ "\"  ),";
    }
	
	public static void insertSCSInFile_SCC(int scc_id, int scs_infile_id)
    {
	INSERT_SCSINFILE_SCC += "( \"" + scc_id + "\" , \"" + scs_infile_id + "\"  ),";
    }
	
	public static void insertSCSInFile_Fragments(int scs_infile_id, int fid, int scc_id, int scsinfile_instance_id,
		    int scc_instance_id)
	    {
		INSERT_SCSINFILE_FRAGMENTS += "( \"" + scs_infile_id + "\" , \"" + fid + "\" , \"" + scc_id + "\" , \""
			+ scsinfile_instance_id + "\" , \"" + scc_instance_id + "\"  ),";
	    }

    public static void insertSCS_InFile(int scs_infile, int members)
    {
	INSERT_SCS_INFILE += "( \"" + scs_infile + "\" , \"" + members + "\"  ),";
    }

    public static void insertSCSInFile_File(int scs_infile_id, int fid)
    {
	INSERT_SCSINFILE_FILE += "( \"" + scs_infile_id + "\" , \"" + fid + "\", \"" + ProjectInfo.getDidFromFid(fid) + "\" , \""
		+ ProjectInfo.getGidFromFid(fid) + "\"  ),";
    }

    public static void insertSCSCrossMethod_SCC(int scs_crossmethod_id, int scc_id)
    {
	INSERT_SCSCROSSMETHOD_SCC += "( \"" + scs_crossmethod_id + "\" , \"" + scc_id + "\"  ),";
    }
    
    public static void insertSCSCrossMethod_Method(int scs_crossmethod_id, int mid, double tc, double pc)
    {
	int fid = ProjectInfo.getFidFromMid(mid);
	INSERT_SCSCROSSMETHOD_METHOD += "( \"" + scs_crossmethod_id + "\" , \"" + mid + "\" , \"" + tc + "\" , \"" + pc
		+ "\", \"" + fid + "\", \"" + ProjectInfo.getDidFromFid(fid) + "\" , \"" + ProjectInfo.getGidFromFid(fid) + "\"  ),";
    }
    
    public static void insertSCS_CrossMethod(int scs_crossmethod_id, double atc, double apc, int members)
    {
	INSERT_SCS_CROSSMETHOD += "( \"" + scs_crossmethod_id + "\" , \"" + atc + "\", \"" + apc + "\", \"" + members
		+ "\"  ),";
    }
    
    public static void insertSCC_Method(int scc_id, int mid)
    {
	INSERT_SCC_METHOD += "( \"" + scc_id + "\" , \"" + mid + "\"  ),";
    }
    
    
	public static void populateDatabase(int dlm , boolean isIncremental)
	{
		try
		{
			StringTokenizer st, st1, st2;
		    String line, id, l, m, rnr, fid, sl, sc, el, ec, filePath = null;
		    String mid, mName;
		    int num = 0, mId = -1;
		    
		    
			filePath = Directories.getAbsolutePath(Directories.CLONES_OUTPUT);
		    File file = new File(filePath);
		    FileInputStream filein = new FileInputStream(file);
		    BufferedReader stdin = new BufferedReader(new InputStreamReader(filein));
		    int scc_instance_id, sccId;
		    while ((line = stdin.readLine()) != null)
		    {
				if (!line.equalsIgnoreCase(""))
				{
				    st = new StringTokenizer(line, ";");
				    id = st.nextToken();
				    l = st.nextToken();
				    m = st.nextToken();
				    rnr=st.nextToken();
				    num = new Integer(m).intValue();
				    scc_instance_id = 0;
				    sccId = Integer.parseInt(id);
				    for (int i = 0; i < num; i++)
				    {
					line = stdin.readLine();
					st = new StringTokenizer(line, ":");
					fid = st.nextToken();
					st1 = new StringTokenizer(st.nextToken(), "-");
					st2 = new StringTokenizer(st1.nextToken(), ".");
					sl = st2.nextToken();
					sc = st2.nextToken();
					st2 = new StringTokenizer(st1.nextToken(), ".");
					el = st2.nextToken();
					ec = st2.nextToken();
					
					if (dlm == 1)
					{
					    mId = ProjectInfo.getMethodId(Integer.parseInt(fid), Integer.parseInt(sl), Integer.parseInt(el));
					} else
					{
					    mId = -1;
					}
					insertSCC_Instance(scc_instance_id, Integer.parseInt(id), Integer.parseInt(fid),
						Integer.parseInt(sl), Integer.parseInt(sc), Integer.parseInt(el), Integer.parseInt(ec),
						mId);
					scc_instance_id++;
					insertSCC_File(sccId, Integer.parseInt(fid));
				    }
				    insertSCC(sccId, Integer.parseInt(l), Integer.parseInt(m),
					    Integer.parseInt(l) * (Integer.parseInt(m) - 1),rnr);
				}
		    }
	
		    if (!INSERT_SCC.equalsIgnoreCase("INSERT INTO scc(scc_id, length, members, benefit, rnr) values "))
		    {
		    	executeTransaction(INSERT_SCC);
		    }
		    if (!INSERT_SCC_INSTANCE.equalsIgnoreCase(
			    "INSERT INTO scc_instance(scc_instance_id, scc_id, fid, startline, startcol, endline, endcol, mid, did, gid) values "))
		    {
		    	executeTransaction(INSERT_SCC_INSTANCE);
		    }
		    if (!INSERT_SCC_FILE.equalsIgnoreCase("INSERT INTO scc_file(scc_id, fid) values "))
		    {
		    	executeTransaction(INSERT_SCC_FILE);
		    }
		    if(!isIncremental)
		    {
			    filePath = Directories.getAbsolutePath(Directories.CLONES_CLUSTER);
			    File file2 = new File(filePath);
			    FileInputStream filein2 = new FileInputStream(file2);
			    BufferedReader stdin2 = new BufferedReader(new InputStreamReader(filein2));
			    String temp;
			    while ((line = stdin2.readLine()) != null)
			    {
					if (!line.equalsIgnoreCase(""))
					{
					    st = new StringTokenizer(line, ";");
					    String cid = st.nextToken();
					    int scs_crossfile_id = Integer.parseInt(cid);
					    String support = st.nextToken();
					    String sccs = stdin2.readLine();
					    st2 = new StringTokenizer(sccs, ",");
					    while (st2.hasMoreTokens())
					    {
						temp = st2.nextToken();
						insertSCSCrossFile_SCC(Integer.parseInt(temp), scs_crossfile_id);
					    }
		
					    int sup = (new Integer(support)).intValue();
					    double atc = 0;
					    double apc = 0;
					    for (int i = 0; i < sup; i++)
					    {
						line = stdin2.readLine();
						st1 = new StringTokenizer(line, ";,");
						fid = st1.nextToken();
						String tk = st1.nextToken();
						String coverage = st1.nextToken();
						atc += Double.parseDouble(tk);
						apc += Double.parseDouble(coverage);
						insertSCSCrossFile_File(scs_crossfile_id, Integer.parseInt(fid), Double.parseDouble(tk),
							Double.parseDouble(coverage));
					    }
					    atc = atc / sup;
					    apc = apc / sup;
					    insertSCS_CrossFile(scs_crossfile_id, atc, apc, sup);
					}
			    }
	
			    if (!INSERT_SCSCROSSFILE_SCC
				    .equalsIgnoreCase("INSERT INTO scscrossfile_scc(scc_id, scs_crossfile_id) values "))
			    {
			    	executeTransaction(INSERT_SCSCROSSFILE_SCC);
			    }
			    if (!INSERT_SCSCROSSFILE_FILE
				    .equalsIgnoreCase("INSERT INTO scscrossfile_file(scs_crossfile_id, fid, tc, pc, did, gid) values "))
			    {
			    	executeTransaction(INSERT_SCSCROSSFILE_FILE);
			    }
			    if (!INSERT_SCS_CROSSFILE
				    .equalsIgnoreCase("INSERT INTO scs_crossfile(scs_crossfile_id, atc, apc, members) values "))
			    {
			    	executeTransaction(INSERT_SCS_CROSSFILE);
			    }
			    
			    filePath = Directories.getAbsolutePath(Directories.IN_FILE_STRUCTURE);
			    File file5 = new File(filePath);
			    FileInputStream filein5 = new FileInputStream(file5);
			    BufferedReader stdin5 = new BufferedReader(new InputStreamReader(filein5));
			    int count = 0;
			    int count0 = 0;
			    int count1 = 0;
			    int fId = -1;
			    Vector<String> fragments;
			    int fragmentSize;
			    int cluster_size;
			    Vector<String> sccs = new Vector<>();
			    int size = ProjectInfo.getFileSize();
			    for (int i = 0; i < size; i++)
			    {
				line = stdin5.readLine();
				if (line!=null && !line.equalsIgnoreCase(""))
				{
				    st = new StringTokenizer(line, "()");
				    int loop = st.countTokens() / 2;
				    for (int j = 0; j < loop; j++)
				    {
					int inst = (new Integer(st.nextToken())).intValue();
					String pat = st.nextToken();
					StringTokenizer stPat = new StringTokenizer(pat, ",");
					String pre = "";
					cluster_size = 0;
					sccs = new Vector<>();
					while (stPat.hasMoreTokens())
					{
					    temp = stPat.nextToken();
					    if (!pre.equalsIgnoreCase(temp))
					    {
						sccs.add(temp);
						cluster_size++;
					    }
					    insertSCSInFile_SCC(Integer.parseInt(temp), count);
					    pre = temp;
					}
	
					fragments = null;
					fId = count0;
					for (int k = 0; k < cluster_size; k++)
					{
					    sccId = Integer.parseInt(sccs.get(k));
					    fragments = SimpleCloneWriterDB.getSCS_Fragments(fId, sccId, FILE_TYPE);
					    if (fragments != null)
					    {
						fragmentSize = fragments.size();
						count1 = 0;
						for (int n = 0; n < inst; n++)
						{
						    for (int q = 0; q < fragmentSize / inst; q++)
						    {
							insertSCSInFile_Fragments(count, fId, sccId, n,
								Integer.parseInt(fragments.get(count1)));
							count1++;
						    }
						}
					    }
					}
					insertSCS_InFile(count, inst);
					insertSCSInFile_File(count, count0);
					count++;
				    }
				}
				count0++;
			    }
	
			    if (!INSERT_SCSINFILE_SCC.equalsIgnoreCase("INSERT INTO scsinfile_scc(scc_id, scs_infile_id) values "))
			    {
			    	executeTransaction(INSERT_SCSINFILE_SCC);
			    }
			    if (!INSERT_SCS_INFILE.equalsIgnoreCase("INSERT INTO scs_infile(scs_infile_id, members) values "))
			    {
			    	executeTransaction(INSERT_SCS_INFILE);
			    }
			    if (!INSERT_SCSINFILE_FILE
				    .equalsIgnoreCase("INSERT INTO scsinfile_file(scs_infile_id, fid, did, gid) values "))
			    {
			    	executeTransaction(INSERT_SCSINFILE_FILE);
			    }
			    if (!INSERT_SCSINFILE_FRAGMENTS.equalsIgnoreCase(
				    "INSERT INTO scsinfile_fragments(scs_infile_id, fid, scc_id, scsinfile_instance_id, scc_instance_id) values "))
			    {
			    	executeTransaction(INSERT_SCSINFILE_FRAGMENTS);
			    }
			    
			    filePath = Directories.getAbsolutePath(Directories.METHOD_CLUSTER);
				File file8 = new File(filePath);
				FileInputStream filein8 = new FileInputStream(file8);
				BufferedReader stdin8 = new BufferedReader(new InputStreamReader(filein8));
				double atc = 0;
				double apc = 0;
				String temp2 = null;
				while ((line = stdin8.readLine()) != null)
				{
				    if (!line.equalsIgnoreCase(""))
				    {
					st = new StringTokenizer(line, ";");
					String mCid = st.nextToken().trim();
					int scs_crossmethod_id = Integer.parseInt(mCid);
					String mSupport = st.nextToken().trim();
					temp = stdin8.readLine();
					sccs = new Vector<>();
					st2 = new StringTokenizer(temp, ",");
					while (st2.hasMoreTokens())
					{
					    temp2 = st2.nextToken();
					    insertSCSCrossMethod_SCC(scs_crossmethod_id, Integer.parseInt(temp2));
					}
	
					int mSup = (new Integer(mSupport)).intValue();
					atc = 0;
					apc = 0;
					for (int i = 0; i < mSup; i++)
					{
					    line = stdin8.readLine();
					    st1 = new StringTokenizer(line, ";,");
					    mid = st1.nextToken().trim();
					    String mTk = st1.nextToken().trim();
					    String mCoverage = st1.nextToken().trim();
					    atc += Double.parseDouble(mTk);
					    apc += Double.parseDouble(mCoverage);
					    mName = ProjectInfo.getMethodName(Integer.parseInt(mid));
					    insertSCSCrossMethod_Method(scs_crossmethod_id, Integer.parseInt(mid),
						    Double.parseDouble(mTk), Double.parseDouble(mCoverage));
					}
					atc = atc / mSup;
					apc = apc / mSup;
					insertSCS_CrossMethod(scs_crossmethod_id, atc, apc, mSup);
				    }
				}
	
				if (!INSERT_SCSCROSSMETHOD_METHOD.equalsIgnoreCase(
					"INSERT INTO scscrossmethod_method(scs_crossmethod_id, mid, tc, pc, fid, did, gid) values "))
				{
				    executeTransaction(INSERT_SCSCROSSMETHOD_METHOD);
				}
				if (!INSERT_SCSCROSSMETHOD_SCC
					.equalsIgnoreCase("INSERT INTO scscrossmethod_scc(scs_crossmethod_id, scc_id) values "))
				{
				    executeTransaction(INSERT_SCSCROSSMETHOD_SCC);
				}
				if (!INSERT_SCS_CROSSMETHOD
					.equalsIgnoreCase("INSERT INTO scs_crossmethod(scs_crossmethod_id, atc, apc, members) values "))
				{
				    executeTransaction(INSERT_SCS_CROSSMETHOD);
				}
				filein2.close();
			    filein5.close();
				filein8.close();
		    }
				filePath = Directories.getAbsolutePath(Directories.CLONES_METHOD);
				File file7 = new File(filePath);
				FileInputStream filein7 = new FileInputStream(file7);
				BufferedReader stdin7 = new BufferedReader(new InputStreamReader(filein7));
				int methodPosn = 0;
				while ((line = stdin7.readLine()) != null)
				{
				    if (!line.equalsIgnoreCase(""))
				    {
					st = new StringTokenizer(line, ",");
					while (st.hasMoreTokens())
					{
					    String s1 = st.nextToken();
					    if (!s1.equalsIgnoreCase(""))
					    {
						insertSCC_Method(Integer.parseInt(s1), methodPosn);
					    }
					}
				    }
				    methodPosn++;
				}
	
				if (!INSERT_SCC_METHOD.equalsIgnoreCase("INSERT INTO scc_method(scc_id, mid) values "))
				{
				    executeTransaction(INSERT_SCC_METHOD);
				}
			    
				filein7.close();				
			    
		    
		    filein.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	public static Vector<String> getSCS_Fragments(int cid, int scc_id, int type)
	{
	Vector<String> list = null;
	String frag = null;
	
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = null;
	    if (type == FILE_TYPE)
	    {
		results = s.executeQuery("select scc_instance_id " + "from scc_instance " + "where fid = " + cid
			+ " and scc_id = " + scc_id + ";");
	    } else if (type == METHOD_TYPE)
	    {
		results = s.executeQuery("select scc_instance_id " + "from scc_instance " + "where mid = " + cid
			+ " and scc_id = " + scc_id + ";");
	    }
	    list = new Vector<>();
	    while (results.next())
	    {
		frag = results.getString("scc_instance_id");
		list.add(frag);
	    }
	
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	    System.err.println(e.getMessage());
	}
	
	return list;
	}
	
	public static  ArrayList<SimpleClone> removeClones(ArrayList<Integer> updatedORDeletedFiles , boolean deletedFilesCase)
	{
		ArrayList<SimpleClone> sccFromDB = null;
		if(updatedORDeletedFiles != null && updatedORDeletedFiles.size() > 0)
		{
			sccFromDB = SimpleClonesReader.getClonesByFile(updatedORDeletedFiles);		
			ArrayList<Integer> clonesToBeDeleted = new ArrayList<Integer>();
			for(int i = 0; i < sccFromDB.size() ; i++) //Removing all the clones that comprises only of updated files or a single unmodified file or deleted files
	    	{										   //becoz all such clones will be detected through incremental approach...
	    		int unModifiedFilesCount = 0;
	    		for(int j = 0; j < sccFromDB.get(i).getInstancesSize() ; j++)
	    		{
	    			if(!updatedORDeletedFiles.contains(sccFromDB.get(i).getInstanceAt(j).getFileId()))    			
	    				unModifiedFilesCount++;    	
	    			else if(deletedFilesCase)
	    			{
	    				sccFromDB.get(i).removeInstance(j);
	    				j--;
	    				sccFromDB.get(i).setSCCInstanceCount(sccFromDB.get(i).getSCCInstanceCount() - 1);
	    			}
	    		}   
	    		if(unModifiedFilesCount == 0 || unModifiedFilesCount == 1)
	    		{
	    			clonesToBeDeleted.add(sccFromDB.get(i).getSSCId());
	    			sccFromDB.remove(i);
	    			i--;
	    		}
	    	}
			if(clonesToBeDeleted != null && clonesToBeDeleted.size() > 0)
				deleteSCCInstance(clonesToBeDeleted , false);
			
			if(deletedFilesCase)
			{
				deleteSCCInstance(updatedORDeletedFiles , true);
				
			}
		}
		return sccFromDB;
	}
	
	public static ArrayList<SimpleClone> removeDuplicateClones(ArrayList<Integer> updatedOrNewlyAddedFileIds, ArrayList<SimpleClone> incrementalClones)
	{
		ArrayList<SimpleClone> sccFromDB = null;
		sccFromDB = removeClones(updatedOrNewlyAddedFileIds , false);

		ArrayList<SimpleClone> finalClones = IncrementalCloneHandler.filterIncrementalClones(incrementalClones , sccFromDB , updatedOrNewlyAddedFileIds);		
		return finalClones;
	}
	
	public static void deleteSCCInstance(ArrayList<Integer> ids , boolean deletedFileCase)
	{
		try
		{
			Connection dbConn = CloneRepository.getConnection();
		    Statement s = dbConn.createStatement();
		    s.execute("use "+ CloneRepository.getDBName()+";");
		    
		    Set<Integer> sccIds_distinct = new HashSet<Integer>(ids);
		    ids.clear();
		    ids.addAll(sccIds_distinct);
		    
		    if(!deletedFileCase)
			{
				String sccIds = "(";
				for(int i = 0; i < ids.size() ; i++)
				{
					if(i != ids.size() - 1)
						sccIds = sccIds + ids.get(i) + ",";
					else
						sccIds = sccIds + ids.get(i) + ")";
				}
						
				String query1 = "DELETE FROM scc_file WHERE scc_id IN "+ sccIds;
				int rs1 = s.executeUpdate(query1);
			
				String query2 = "DELETE FROM scc_instance WHERE scc_id IN "+ sccIds;
				int rs2 = s.executeUpdate(query2);
				
				String query3 = "DELETE FROM scc_method WHERE scc_id IN "+ sccIds;
				int rs3 = s.executeUpdate(query3);
				
				String query = "DELETE FROM scc WHERE scc_id IN "+ sccIds;
				int rs = s.executeUpdate(query);
			}
			else
			{
				String fileIds = "(";
				for(int i = 0; i < ids.size() ; i++)
				{
					if(i != ids.size() - 1)
						fileIds = fileIds + ids.get(i) + ",";
					else
						fileIds = fileIds + ids.get(i) + ")";
				}
				
				String query1 = "DELETE FROM scc_file WHERE fid IN "+ fileIds;
				int rs1 = s.executeUpdate(query1);
			
				String query2 = "DELETE FROM scc_instance WHERE fid IN "+ fileIds;
				int rs2 = s.executeUpdate(query2);
				
				String query3 = "DELETE FROM scc_method WHERE mid IN "+ fileIds;
				int rs3 = s.executeUpdate(query3);	
				
				String query4 = "DELETE method.* FROM method INNER JOIN method_file ON method.mid = method_file.mid WHERE method_file.fid IN " + fileIds;
				int rs4 = s.executeUpdate(query4);
				
				String query5 = "DELETE FROM method_file WHERE fid IN "+ fileIds;
				int rs5 = s.executeUpdate(query5);					
			}
			
		}
		catch (Exception e)
		{
		    e.printStackTrace();
		    System.err.println(e.getMessage());
		}
	}
}
