package cmt.cddc.simpleclones;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.common.Directories;

public class SimpleClonesReader {

	 static ArrayList<SimpleClone> sccList;
	 static ArrayList<SimpleClone> filteredSccList;

	public SimpleClonesReader(){
		sccList = new ArrayList<SimpleClone>();
		filteredSccList  = new ArrayList<SimpleClone>();
		 
	 }
	
	public static ArrayList<SimpleClone> getFilteredSimpleClone(){
		return filteredSccList;
	}
	public static ArrayList<Integer> GetSCCAgainstMCC(int mccID)
	{
		ArrayList<Integer> retVal = new ArrayList<>();
		String query = "SELECT scc_id FROM mcc_scc WHERE mcc_id = " + mccID;
		try 
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next())
			{
				retVal.add(rs.getInt(1));
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return retVal;
	}
	public static int getMaxSCCId()
	{	
		int maxSCCId = 0;
		
		try 
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			String query = "SELECT scc_id FROM scc";
			ResultSet rs = stmt.executeQuery(query);
			//if(rs.getFetchSize()==0)
			//	return 0;
			
			while(rs.next())
			{
				if(maxSCCId < Integer.parseInt(rs.getString(1)))
					maxSCCId = Integer.parseInt(rs.getString(1));
			}
		}
		catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maxSCCId;
		/*int maxSCCId = -1;
		try 
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			String query = "SELECT MAX(scc_id) FROM scc";
			ResultSet rs = stmt.executeQuery(query);
	
			if(rs.first()) 
			{
			    // there's stuff to do				
				while(rs.next()) 
				{
					maxSCCId = (Integer.parseInt(rs.getString(1)));
				}
				
			} 								 
		}
		catch (Exception e)
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		return maxSCCId;*/
	}
	public static HashMap<Integer,ArrayList<Integer>> ReadIncrementalMCCDetectionData()
	{
		HashMap<Integer,ArrayList<Integer>> method_to_scc = new HashMap<>();
		try
		{
			String query2 = "SELECT mid,scc_id  FROM scc_method";
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			ResultSet rs = stmt.executeQuery(query2);
			while(rs.next()) {
				method_to_scc.putIfAbsent(rs.getInt(1), new ArrayList<Integer>());
				method_to_scc.get(rs.getInt(1)).add(rs.getInt(2));
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return method_to_scc;
	}
	public static HashMap<Integer,ArrayList<Integer>> ReadIncrementalMCCClusterData()
	{
		HashMap<Integer,ArrayList<Integer>> mcc_to_mid = new HashMap<>();
		try
		{
			String query1 = "select mcc_id, mid from mcc_instance";
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			ResultSet rs = stmt.executeQuery(query1);
			while(rs.next()) {
				mcc_to_mid.putIfAbsent(rs.getInt(1), new ArrayList<Integer>());
				mcc_to_mid.get(rs.getInt(1)).add(rs.getInt(2));
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return mcc_to_mid;
	}
	public static ArrayList<SimpleClone> getClonesByFile(ArrayList<Integer> updatedFileIds)
	{
		ArrayList<SimpleClone> scc= new ArrayList<SimpleClone>();
		try 
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			String query = "SELECT DISTINCT scc.scc_id , scc.length , scc.members , scc.benefit ,RNR FROM scc INNER JOIN scc_file ON scc.scc_id = scc_file.scc_id WHERE scc_file.fid IN (";
			for(int i = 0; i< updatedFileIds.size() ; i++)
			{
				if(i != updatedFileIds.size()-1)
				   query = query + updatedFileIds.get(i) + ",";
			    else
				   query = query + updatedFileIds.get(i) + ")";
			}
			ResultSet rs = stmt.executeQuery(query);
 
			while(rs.next()) {
				 SimpleClone s = new SimpleClone();
				 s.setSSCId(Integer.parseInt(rs.getString(1)));
				 s.setTokenCount(Integer.parseInt(rs.getString(2)));
				 s.setSCCInstanceCount(Integer.parseInt(rs.getString(3)));

				 Statement stmt2 = CloneRepository.getConnection().createStatement();
				 ResultSet rs2 = stmt2.executeQuery("select * from scc_instance where scc_id=" + s.getSSCId());
				 while (rs2.next())
				 {
					SimpleCloneInstance sccInstance = new SimpleCloneInstance();
				    sccInstance.setSCCID(s.getSSCId());
				    sccInstance.setFileId(Integer.parseInt(rs2.getString(3)));
				    sccInstance.setStartLine(Integer.parseInt(rs2.getString(4)));
				    sccInstance.setStartCol(Integer.parseInt(rs2.getString(5)));
				    sccInstance.setEndLine(Integer.parseInt(rs2.getString(6)));
				    sccInstance.setEndCol(Integer.parseInt(rs2.getString(7)));				    				   
				    s.addSCCInstance(sccInstance);	  
				 }
				 scc.add(s);
			}
		}
		catch (Exception e)
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		return scc;
	}
	
	public static ArrayList<SimpleClone> getClonesByUnModifiedFiles(ArrayList<Integer> unModFiles)
	{
		ArrayList<SimpleClone> scc= new ArrayList<SimpleClone>();
		try 
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			String query = "SELECT scc_file.scc_id , scc.length , scc.members FROM scc_file JOIN scc ON scc.scc_id = scc_file.scc_id WHERE fid IN (";
			for(int i =0 ; i < unModFiles.size() ; i++)
			{
				if(i != unModFiles.size() - 1)
					query = query + unModFiles.get(i) + "," ;
				else
					query = query + unModFiles.get(i) + ")";
			}
			query = query + " GROUP BY scc_id HAVING count(*) = " + unModFiles.size() + ";";
			
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				SimpleClone s = new SimpleClone();
				 s.setSSCId(Integer.parseInt(rs.getString(1)));
				 s.setTokenCount(Integer.parseInt(rs.getString(2)));
				 s.setSCCInstanceCount(Integer.parseInt(rs.getString(3)));

				 Statement stmt2 = CloneRepository.getConnection().createStatement();
				 ResultSet rs2 = stmt2.executeQuery("select * from scc_instance where scc_id=" + s.getSSCId());
				 while (rs2.next())
				 {
					SimpleCloneInstance sccInstance = new SimpleCloneInstance();
				    sccInstance.setSCCID(s.getSSCId());
				    sccInstance.setFileId(Integer.parseInt(rs2.getString(3)));
				    sccInstance.setStartLine(Integer.parseInt(rs2.getString(4)));
				    sccInstance.setStartCol(Integer.parseInt(rs2.getString(5)));
				    sccInstance.setEndLine(Integer.parseInt(rs2.getString(6)));
				    sccInstance.setEndCol(Integer.parseInt(rs2.getString(7)));				    				   
				    s.addSCCInstance(sccInstance);	  
				 }
				 scc.add(s);
			}				
		}
		catch (Exception e)
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		return scc;
	}
	
	public static void filterClones(String location, String locationID, String maxBen, String minBen, String minLength, String maxLength, String minMember, String maxMember) {
		SimpleClone scc = null;
		 SimpleCloneInstance sccInstance = null;
		 boolean filterFlag = false;
		try {
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			String query = "";
			if(!location.isEmpty()) {
				if(location.equals("File")) {
					 query = "SELECT *, scc_file.fid"+
							" FROM scc "
							+ "INNER JOIN scc_file ON scc.scc_id = scc_file.scc_id where ";
					 if(!locationID.isEmpty()) {
						 String IDS[]= {};
						 if(locationID.contains(",")) {
							  IDS = locationID.split(",");
						 }
						 if(IDS.length>0) {
							 for(int i =0;i<IDS.length;i++) {
								 if(filterFlag) {
									 query += "or scc_file.fid = "+IDS[i]+ " ";
								 }
								 else {
									 query += "scc_file.fid = "+IDS[i]+ " ";
									 filterFlag = true;
								 }
								 
							 }
						 }
						 else {
							 if(filterFlag) {
								 query += "or scc_file.fid = "+locationID+ " ";
							 }
							 else {
								 query += "scc_file.fid = "+locationID+ " ";
								 filterFlag = true;
							 }
						 }
						 
							
							
						}
					 
					 
					
					
				}
				else if(location.equals("Method")) {
					query = "SELECT *, scc_method.mid"
							+ "INNER JOIN scc_method ON scc.scc_id = scc_method.scc_id "+
							" FROM scc " + 
							" where ";//
					if(!locationID.isEmpty()) 
					{
						String IDS[]= {};
						if(locationID.contains(",")) {
							IDS = locationID.split(",");
						}
						if(IDS.length>0) {
							for(int i =0;i<IDS.length;i++) {
								if(filterFlag) {
									query += "or scc_method.mid = "+IDS[i]+ " ";
								}
								else {
									query += "scc_method.mid = "+IDS[i]+ " ";
									filterFlag = true;
								}
							 
							}
						}
						else {
							if(filterFlag) {
								query += "or scc_method.mid = "+locationID+ " ";
							}
							else {
								query += "scc_method.mid = "+locationID+ " ";
								filterFlag = true;
							}
						}
					}
					
				}
				
			 }
			else {
				query = "SELECT scc.*"+
						" FROM scc where ";
				
				
			}
			
			if(!maxBen.isEmpty()) {
				if(filterFlag) {
					query += "and scc.benefit <= "+maxBen+" ";
				 }
				 else {
					 query += "scc.benefit <= "+maxBen+" ";
					 filterFlag = true;
				 }
				
				
			}
			if(!minBen.isEmpty()) {
				if(filterFlag) {
					query += "and scc.benefit >= "+minBen+" ";
				 }
				 else {
					 query += "scc.benefit >= "+minBen+" ";
					 filterFlag = true;
				 }
			 
			}
			if(!maxLength.isEmpty()) {
				if(filterFlag) {
					query += "and scc.length <= "+maxLength+" ";
				 }
				 else {
					 query += "scc.length <= "+maxLength+" ";
					 filterFlag = true;
				 }
				
			}
			if(!minLength.isEmpty()) {
				if(filterFlag) {
					query += "and scc.length >= "+minLength+" ";
				 }
				 else {
					 query += "scc.length >= "+minLength+" ";
					 filterFlag = true;
				 }
				
			}
			if(!minMember.isEmpty()) {
				if(filterFlag) {
					query += "and scc.members >= "+minMember+" ";
				 }
				 else {
					 query += "scc.members >= "+minMember+" ";
					 filterFlag = true;
				 }
				
			}
			if(!maxMember.isEmpty()) {
				if(filterFlag) {
					query += "and scc.members <= "+maxMember+" ";
				 }
				 else {
					 query += "scc.members <= "+maxMember+" ";
					 filterFlag = true;
					 
				 }
				
			}
			
			query += " group by scc.scc_id;"; 
			
			
			ResultSet rs = stmt.executeQuery(query);
			
			filteredSccList.clear();
			while(rs.next()) {
				 scc = new SimpleClone();
				 scc.setSSCId(Integer.parseInt(rs.getString(1)));
				 scc.setTokenCount(Integer.parseInt(rs.getString(2)));
				 scc.setSCCInstanceCount(Integer.parseInt(rs.getString(3)));
				 scc.setBenefit(Integer.parseInt(rs.getString(4)));
			

				 Statement stmt2 = CloneRepository.getConnection().createStatement();
				 ResultSet rs2 = stmt2.executeQuery("select * from scc_instance where scc_id=" + scc.getSSCId());
				 while (rs2.next())
				 {
				    sccInstance = new SimpleCloneInstance();
				    sccInstance.setSCCID(scc.getSSCId());
				    sccInstance.setFileId(Integer.parseInt(rs2.getString(3)));
				    sccInstance.setStartingIndex(Integer.parseInt(rs2.getString(4)));
				    sccInstance.setStartCol(Integer.parseInt(rs2.getString(5)));
				    sccInstance.setEndingIndex(Integer.parseInt(rs2.getString(6)));
				    sccInstance.setEndCol(Integer.parseInt(rs2.getString(7)));

				    	sccInstance
				    	.setCodeSegment(getCodeSegment(getFilePath(sccInstance.getFileId()), sccInstance.getStartingIndex(),
				    sccInstance.getStartCol(), sccInstance.getEndingIndex(), sccInstance.getEndCol(), sccInstance));
				    sccInstance.setDirId(getDirID(sccInstance.getFileId()));
				    sccInstance.setFilePath(getFilePath(sccInstance.getFileId()));  
				    // Writer.writer(tempclone);
				    scc.addSCCInstance(sccInstance);
	  
				 }
				 
				 filteredSccList.add(scc);
				
			}
			
			
				
		}
		catch (Exception e)
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
	}
	 
	
	 public  void CloneParser_loadMethodList()
	 {		 
		// ArrayList<SimpleClone> simpleClones = null;
		 SimpleClone scc = null;
		 SimpleCloneInstance sccInstance = null;
		 try
		 {
			 Statement stmt = CloneRepository.getConnection().createStatement();
			 stmt.execute("use "+CloneRepository.getDBName());
			 ResultSet rs = stmt.executeQuery("select scc_id,length,members,benefit from scc");

			 while (rs.next())
			 {
				 scc = new SimpleClone();
				 scc.setSSCId(Integer.parseInt(rs.getString(1)));
				 scc.setTokenCount(Integer.parseInt(rs.getString(2)));
				 scc.setSCCInstanceCount(Integer.parseInt(rs.getString(3)));
				 scc.setBenefit(Integer.parseInt(rs.getString(4)));

				 Statement stmt2 = CloneRepository.getConnection().createStatement();
				 ResultSet rs2 = stmt2.executeQuery("select * from scc_instance where scc_id=" + scc.getSSCId());
				 while (rs2.next())
				 {
				    sccInstance = new SimpleCloneInstance();
				    sccInstance.setSCCID(scc.getSSCId());
				    sccInstance.setFileId(Integer.parseInt(rs2.getString(3)));
				    sccInstance.setStartingIndex(Integer.parseInt(rs2.getString(4)));
				    sccInstance.setStartCol(Integer.parseInt(rs2.getString(5)));
				    sccInstance.setEndingIndex(Integer.parseInt(rs2.getString(6)));
				    sccInstance.setEndCol(Integer.parseInt(rs2.getString(7)));

				    	sccInstance
				    	.setCodeSegment(getCodeSegment(getFilePath(sccInstance.getFileId()), sccInstance.getStartingIndex(),
				    sccInstance.getStartCol(), sccInstance.getEndingIndex(), sccInstance.getEndCol(), sccInstance));			
				    	sccInstance.setCodeSegmentToTokenze(
				    	getCodeSegmentToTokenize(getFilePath(sccInstance.getFileId()), sccInstance.getStartingIndex(),  sccInstance.getEndingIndex()));
				    sccInstance.setDirId(getDirID(sccInstance.getFileId()));
				    sccInstance.setFilePath(getFilePath(sccInstance.getFileId()));  
				    scc.addSCCInstance(sccInstance);
	  
				 }
				 
				 sccList.add(scc);
				
	//addSCC(scc);
	
    }

} catch (Exception e)
{
    // TODO Auto-generated catch block
    e.printStackTrace();
}
	//	 return sccList;
}

public static String getCodeSegment(String path, int start_line, int start_col, int end_line, int end_col,
	    SimpleCloneInstance inst) throws IOException
    {
	// if(inst.getSCCID()==8)
	// {
	// int x=0;
	// }
	File check = new File(path);
	FileReader frdr = null;
	BufferedReader buff = null;
	String data = "";
	if (check.exists())
	{
	    frdr = new FileReader(path);
	    buff = new BufferedReader(frdr);

	} else if (!check.exists())
	{
	    String path2 = Directories.CHECKFILE;
	    path2 =  Directories.getAbsolutePath(path2);	    
	  	frdr = new FileReader(path2);
	    buff = new BufferedReader(frdr);
	    System.out.println("File not found: " + path2);
	}
	int line_number = 0;
	try
	{
	    String line = null;
	    while ((line = buff.readLine()) != null)
	    {
		line_number++;
		line = line.replace("\t", "        "); // to handle the error occur due to space and \t
		if (start_line == end_line)
		{
		    if (line_number == start_line)
		    {
			if (end_col <= line.length() && start_col <= line.length())
			{
				
			    line = line.substring(start_col - 1, end_col);
			}
			// commented to make highlight code work in SCCview (Saud)
			//data += (line + "\n");
			data += line;
			frdr.close();
			buff.close();
			return data;
		    }
		} else if (line_number >= start_line && line_number <= end_line)
		{
		    if (line_number == start_line)
		    {
				if ((start_col - 1 <= line.length()) && start_col > 0)
					{
					    // line=line.replace("\t"," "); // to handle the error occur due to space and \t
					    line = line.substring(start_col - 1);
					}
				data += (line + "\n");
		    } 
		    else if (line_number == end_line)
		    {
				if (inst != null)
				{
				    if (end_col <= line.length())
					    {
							String temp = line.substring(0, end_col);
							line = temp;
					    }
				} 
				else
				{
				    if (end_col < 2)
				    {
					line = "";
				    } else
				    {
					line = line.substring(0, end_col);
				    }
				}
				data += line;
		    }
		    else
		    {
		    	data += (line + "\n");
		    }
		}
	    }
	} catch (Exception e)
	{
	    e.printStackTrace();
	} finally
	{
	    frdr.close();
	    buff.close();

	}
	return data;
    }



public static String getCodeSegmentToTokenize(String path, int start_line, int end_line) throws IOException
{	
	 	File check = new File(path);
		FileReader frdr = null;
		BufferedReader buff = null;
		String data = "";		
		if (check.exists())
		{
		    frdr = new FileReader(path);
		    buff = new BufferedReader(frdr);

		}		
		int line_number = 0;
		try
		{
		    String line = null;
		    while ((line = buff.readLine()) != null)
		    {
			line_number++;			
			if(line_number > end_line)
				break;
			
			if (line_number >= start_line && line_number <= end_line)
			{
			    data += (line + "\n");
			}

		}
		    
		} catch (Exception e)
		{
		    e.printStackTrace();
		} finally
		{
		    frdr.close();
		    buff.close();

		}
		return data;	 
}



public static int getDirID(int file_number)
{

try
{
    Statement stmt = CloneRepository.getConnection().createStatement();
    stmt.execute("use "+ CloneRepository.getDBName()+";");

    ResultSet rs = stmt.executeQuery("select did from file_directory where fid=" + file_number);
    rs.next();
    return rs.getInt(1);
} catch (SQLException e1)
{
    // TODO Auto-generated catch block
    e1.printStackTrace();
}

return 0;
}


 public static BufferedReader getBufferReader(String filename)
{

FileReader frdr = null;
BufferedReader reader = null;

try
{
    String path = filename;
    path = Directories.getAbsolutePath(path);
    frdr = new FileReader(path);
    reader = new BufferedReader(frdr);

} catch (FileNotFoundException e)
{
    System.out.println("file not found");
}

return reader;
}


public static String getFilePath(int fid) throws IOException
{

/*BufferedReader buffer = null;
buffer = getBufferReader(Directories.INPUTFILE);
try
{
    int index = 0;
    String line = null;
    while ((line = buffer.readLine()) != null)
    {
	if (index == fid)
	{
	    return line;
	}
	index++;
    }
} catch (Exception e)
{
    System.out.println("Error finding file Name");
}
try
{
    buffer.close();
} catch (IOException e)
{
}
return "N/A";*/
	return ProjectInfo.getInputFile(fid);
}


private static String getCodeSegment(String path, int start_line, int end_line) throws Exception
{
// TODO Auto-generated method stub
FileReader frdr = null;
BufferedReader reader = null;
String data = "";
frdr = new FileReader(path);
reader = new BufferedReader(frdr);

String line;
int linenumber = 0;
while ((line = reader.readLine()) != null)
{
    if (linenumber >= start_line - 1 && linenumber <= end_line)
    {
	data += line + "\n";
    }
    linenumber++;
}
reader.close();
return data;
}



// ---------------------------Combined Tokens------------------------------------------------------

public void CombinedTokensParser_loadMethodList()
{
	try
	{
		Statement stmt = CloneRepository.getConnection().createStatement();
	    stmt.execute("use "+ CloneRepository.getDBName()+";");
		ResultSet rs=stmt.executeQuery("select * from tokens");
		
		while(rs.next())
		{
			if(rs.getInt(1)==9 || rs.getInt(1)==99999)
			{
				int x=0;
			}
			{
				
				SimpleCloneInstance instance = getSCCInstance(rs.getInt(2),
						rs.getInt(3),
						rs.getInt(4),null);
				while(instance!=null)
				{
						if (rs.getString(9).equals("STARTMETHOD")
								|| rs.getString(9).equals("ENDMETHOD")
								|| rs.getString(9).equals("ENDFILE")) 
						{
							instance=null;
							continue;
						}
						//Convert ART keywords into ordinary characters by appending \\
						if(rs.getString(9).equals("?")){ 
							instance.addToken("\\?");
							instance=getSCCInstance(rs.getInt(2),rs.getInt(3),rs.getInt(4),instance);
							continue;
						}
						//Convert ART keywords into ordinary characters by appending \\
						if(rs.getString(9).equals("#")){
							instance.addToken("\\#");
							instance=getSCCInstance(rs.getInt(3),rs.getInt(3),rs.getInt(3),instance);
							continue;
						}
						instance.addToken(rs.getString(9));
						instance=getSCCInstance(rs.getInt(2),rs.getInt(3),rs.getInt(4),instance);
//						System.out.println("hahaha3");
				}
			}
		}
		
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}
}


public static ArrayList<SimpleClone> getClones()
{
	return sccList;
}

private SimpleCloneInstance getSCCInstance(int filenumber, int linenumber, int colnumber, SimpleCloneInstance prevInstance)
	    throws IOException
    {
	
	int index1 = 0;
	if (prevInstance != null)
	{
	    for (int i = 0; i <  getClones().size(); i++)
	    {
		for (int j = 0; j < getClones().get(i).getSCCInstances().size(); j++)
		{
		    if (getClones().get(i).getSCCInstances().get(j) == prevInstance)
		    {
			index1 = i + 1;
			if (j + 1 == getClones().get(i).getSCCInstances().size())
			{
			    index1 = i + 1;
			}
			break;
		    }
		}
	    }
	}
	for (int i = index1; i < getClones().size(); i++)
	{
	    for (int j = 0; j < getClones().get(i).getSCCInstances().size(); j++)
	    {
		if (getClones().get(i).getSCCInstances().get(j).getFileId() == filenumber
			&& getClones().get(i).getSCCInstances().get(j).getStartingIndex() <= linenumber
			&& getClones().get(i).getSCCInstances().get(j).getEndingIndex() >= linenumber)
		{
		    if (getClones().get(i).getSCCInstances().get(j).getStartingIndex() == linenumber)
		    {
			if (getClones().get(i).getSCCInstances().get(j).getStartCol() <= colnumber)
			{
			    return getClones().get(i).getSCCInstances().get(j);
			} else
			{
			}
		    } else if (getClones().get(i).getSCCInstances().get(j).getEndingIndex() == linenumber)
		    {
			if (getClones().get(i).getSCCInstances().get(j).getEndCol() >= colnumber)
			{
			    return getClones().get(i).getSCCInstances().get(j);
			} else
			{
			}
		    } else
		    {
			return getClones().get(i).getSCCInstances().get(j);
		    }
		}
	    }
	}
	return null;
    }

public static HashMap<Integer,ArrayList<Integer>> ReadSccAgainstFiles(ArrayList<SimpleClone> finalClones ,ArrayList<Integer> updatedOrNewlyAddedFiles)
{
	HashMap<Integer,ArrayList<Integer>> retVal = new HashMap<>();
	for(Integer a : updatedOrNewlyAddedFiles)
	{
		retVal.putIfAbsent(a, new ArrayList<Integer>());
	}
	ArrayList<Integer> newSimpleClonesDetected = new ArrayList<>();
	for(SimpleClone sc : finalClones)
	{
		newSimpleClonesDetected.add(sc.getSSCId());
		for(SimpleCloneInstance sci : sc.getInstances())
		{
			if(retVal.containsKey(sci.getFileId()))
			{
				retVal.get(sci.getFileId()).add(sc.getSSCId());
			}
			else
			{
				ArrayList<Integer> newList = new ArrayList<>();
				newList.add(sc.getSSCId());
				retVal.put(sci.getFileId(), newList);
			}
		}
	}
	
	if(newSimpleClonesDetected.size() > 0)
	{
		try 
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			String query = "SELECT DISTINCT fid FROM scc_instance WHERE scc_id = " +newSimpleClonesDetected.get(0) ;
			for(int i = 1 ; i <  newSimpleClonesDetected.size() ; i++)
			{
				query+= " OR scc_id = " + newSimpleClonesDetected.get(i);
			}
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				retVal.putIfAbsent(rs.getInt(1), new ArrayList<Integer>());
			}
			for(Integer key : retVal.keySet())
			{
				String query2 = "Select scc_id from scc_instance where fid = " + key;
				rs = stmt.executeQuery(query2);
				while(rs.next()) {
					retVal.get(key).add(rs.getInt(1));
				}
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	return retVal;
}
public static HashMap<Integer, Integer> ReadCoverageForUnModifiedFiles(ArrayList<Integer> updatedOrNewlyAddedFileIds) 
{
	HashMap<Integer, Integer> result = new HashMap<>();
	try
	{
		Statement stmt = CloneRepository.getConnection().createStatement();
		stmt.execute("use "+ CloneRepository.getDBName()+";");

		ResultSet rs = stmt.executeQuery("select fid,length from file_directory");
		while(rs.next())
		{
			if(!updatedOrNewlyAddedFileIds.contains(rs.getInt(1)))
			{
				result.put(rs.getInt(1), rs.getInt(2));
			}
		}
	}
	catch (SQLException e1)
	{
	    e1.printStackTrace();
	}
	return result;
}

public static void LoadFilelist() {
	// TODO Auto-generated method stub
	ProjectInfo.loadInputFiles();
}

}