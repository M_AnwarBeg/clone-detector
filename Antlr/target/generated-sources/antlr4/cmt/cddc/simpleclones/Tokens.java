package cmt.cddc.simpleclones;
public class Tokens
{
    private String text;
    private int type;
    private int tokenLine;
    private Boolean tokenRepetitionInd;
    private int tokenColumn;
    private int tokenFile;
    private int tokenClass;
    private int tokenMethod;
    private int tokenStart = -1;
    private int tokenStop = -1;
    
    public Tokens()
    {
    	tokenRepetitionInd = false;
    	text = null;
    	type=-1;
    	tokenLine=-1;
    	tokenColumn = -1;
    	tokenFile=-1;
    	tokenClass=-1;
    	tokenMethod=-1;
    }
    public String getText()
    {
        return text;
    }
    public void setText(String text)
    {
        this.text = text;
    }
    public int getType()
    {
        return type;
    }
    public void setType(int i)
    {
        this.type = i;
    }
    
    public void setTokenLine(int line)
    {
    	this.tokenLine = line;
    }
    
    public int getTokenLine()
    {
    	return this.tokenLine;
    }
    
    public void setTokenRepetition(Boolean tokenRepetition)
    {
    	this.tokenRepetitionInd = tokenRepetition;
    }
    
    public Boolean getTokenRepetition()
    {
    	return this.tokenRepetitionInd;
    }
    
    public void setTokenColumn(int col)
    {
    	this.tokenColumn = col;
    }
    
    public int getTokenColumn()
    {
    	return this.tokenColumn;
    }
    
    public void setTokenFile(int file)
    {
    	this.tokenFile = file;
    }
    
    public int getTokenFile()
    {
    	return this.tokenFile;
    }
    
    public void setTokenClass(int tokenCls)
    {
    	this.tokenClass = tokenCls;
    }
    
    public int getTokenClass()
    {
    	return this.tokenClass;
    }
	public int getTokenMethod() {
		return tokenMethod;
	}
	public void setTokenMethod(int tokenMethod) {
		this.tokenMethod = tokenMethod;
	}
	public int getTokenStart() {
		return tokenStart;
	}
	public void setTokenStart(int tokenStart) {
		this.tokenStart = tokenStart;
	}
	public int getTokenStop() {
		return tokenStop;
	}
	public void setTokenStop(int tokenStop) {
		this.tokenStop = tokenStop;
	}	
}
