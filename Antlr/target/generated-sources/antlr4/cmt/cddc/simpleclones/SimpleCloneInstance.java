package cmt.cddc.simpleclones;

import java.io.IOException;
import java.util.ArrayList;

public class SimpleCloneInstance extends CloneInstance implements Comparable<SimpleCloneInstance> {
	private int startingIndex;
	private int endingIndex;
	private int fileId;
	private int methodId;

	private int SSCId;
	private int startCol;
	private int endCol;
	private int dirId;
	private String filePath;
	private String codeSegment;
	private String codeSegmentToTokenze;
	
	
	private ArrayList<String> tokens;
	
	//------------Added for incremental clone detection.-----

	private ArrayList<Integer> tokensStartLine;
	private ArrayList<Integer> tokensStartColumn;
	private int startLine;
	private int endLine;
	private String fileName;
	private int seqIndex;
	//-------------------------------------------------------

	public SimpleCloneInstance() {
		this.startingIndex = -1;
		this.endingIndex = -1;
		this.methodId = -1;
		
		
		
		SSCId = 0;
		tokens = new ArrayList<>();
		startCol = 0;
		endCol = 0;
		dirId = 0;
		
		
	}

	public SimpleCloneInstance(int SSCId, String filename, int startingIndex, int startCol, int endingIndex, int endCol,
			int startline, int endline,String filePath, int dirId,int methodId , int fid) {
		this.SSCId = SSCId;
		this.fileName = filename;
		this.startingIndex = startingIndex;
		this.startCol = startCol;
		this.endingIndex = endingIndex;
		this.endCol = endCol;
		this.filePath = filePath;
		this.dirId = dirId;
		this.startLine = startline;
		this.endLine = endline;
		this.methodId=methodId;
		this.fileId = fid;
		this.seqIndex=seqIndex;
		tokens = new ArrayList<>();
	}
	
	public SimpleCloneInstance(int SSCId, int fileId, int startingIndex, int startCol, int endingIndex, int endCol,
			String filePath, int dirId) {
		this.SSCId = SSCId;
		this.fileId = fileId;
		this.startingIndex = startingIndex;
		this.startCol = startCol;
		this.endingIndex = endingIndex;
		this.endCol = endCol;
		this.filePath = filePath;
		this.dirId = dirId;
		tokens = new ArrayList<>();
	}
	public int getSeqIndex() {
		return seqIndex;
	}

	public void setSeqIndex(int seqIndex) {
		this.seqIndex = seqIndex;
	}
	
	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public void setSCCID(int SSCId) {
		this.SSCId = SSCId;
	}

	public void setStartCol(int startCol) {
		this.startCol = startCol;
	}

	public void setEndCol(int endCol) {
		this.endCol = endCol;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setCodeSegment(String codeSegment) {
		this.codeSegment = codeSegment;
	}

	public void setDirId(int dirId) {
		this.dirId = dirId;
	}

	public void setTokens(ArrayList<String> tokenList) {
		this.tokens = tokenList;
	}
	
	public void setTokensLine(ArrayList<Integer> tokenList) {
		this.tokensStartLine = tokenList;
	}
	
	public void setTokensColumn(ArrayList<Integer> tokenList) {
		this.tokensStartColumn = tokenList;
	}

	public int getSCCID() {
		return SSCId;
	}

	public int getStartCol() {
		return startCol;
	}

	public int getEndCol() {
		return endCol;
	}

	public int getDirId() {
		return dirId;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getCodeSegment() {
		return codeSegment;
	}

	public ArrayList<String> getTokens() {
		return tokens;
	}
	
	public ArrayList<Integer> getTokensLine() {
		return tokensStartLine;
	}
	
	public ArrayList<Integer> getTokensColumn() {
		return tokensStartColumn;
	}

	public void addToken(String token) {
		this.tokens.add(token);
	}

	public SimpleCloneInstance(int startingIndex, int endingIndex) {
		this.startingIndex = startingIndex;
		this.endingIndex = endingIndex;
	}

	public int getStartingIndex() {
		return startingIndex;
	}

	public void setStartingIndex(int startingIndex) {
		this.startingIndex = startingIndex;
	}

	public int getEndingIndex() {
		return endingIndex;
	}

	public void setEndingIndex(int endingIndex) {
		this.endingIndex = endingIndex;
	}
	 public int getInstanceLength()
	 {
		 int length = this.getEndingIndex()-this.getStartingIndex()+1;
		 return length;
	 }

	public int getMethodId() {
		return methodId;
	}

	public void setMethodId(int methodId) {
		this.methodId = methodId;
	}

	@Override
	public int compareTo(SimpleCloneInstance sccInstance) {
		int startLine = sccInstance.getStartingIndex();
		return this.startingIndex - startLine;
	}

	public void updateCode(String path) throws IOException {
		// _codeSegment = ClonesParser.getCodeSegment(path, _startLine, _startCol,
		// _endLine, _endCol, null);
	}

	public boolean checkForGapWith(SimpleCloneInstance nextInstance) {

		if (getEndingIndex() < nextInstance.getStartingIndex()) {
			return true;
		} else if (getEndingIndex() == nextInstance.getStartingIndex()) {
			if (getEndCol() <= nextInstance.getStartCol()) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	public void compareSCCInstances(int[][] matrix, SimpleCloneInstance nextInstance, int index1, int index2) {
		if (index1 == index2) {
			matrix[index1][index2] = 0;
			return;
		} else if (checkForGapWith(nextInstance)) {
			matrix[index1][index2] = 1;
			matrix[index2][index1] = 1;
			return;
		} else {
			matrix[index1][index2] = 0;
			matrix[index2][index1] = 0;
			return;
		}
	}	
	
	public String getCodeSegmentToTokenze() {
		return codeSegmentToTokenze;
	}

	public void setCodeSegmentToTokenze(String codeSegmentToTokenze) {
		this.codeSegmentToTokenze = codeSegmentToTokenze;
	}

	public static boolean areSCCInstancesEqual(SimpleCloneInstance sc1 , SimpleCloneInstance sc2)
	{
		if(sc1.getStartLine() == sc2.getStartLine() && sc1.getEndLine() == sc2.getEndLine() && 
				sc1.getStartCol() == sc2.getStartCol() && sc1.getEndCol() == sc2.getEndCol())
			return true;
		else
			return false;
	}
	
	public static boolean SCCInstanceContain(SimpleCloneInstance sc1 , SimpleCloneInstance sc2)
	{
		if(sc1.getStartLine() < sc2.getStartLine() && sc1.getEndLine() > sc2.getEndLine())
		{
			return true;
		}
		else if(sc1.getStartLine() <= sc2.getStartLine() && sc1.getEndLine() > sc2.getEndLine())
		{
			if(sc1.getStartCol() <= sc2.getStartCol())
				return true;
		}
		else if(sc1.getStartLine() < sc2.getStartLine() && sc1.getEndLine() >= sc2.getEndLine())
		{
			if(sc1.getEndCol() >= sc2.getEndCol())
				return true;
		}
		else if(sc1.getStartLine() == sc2.getStartLine() && sc1.getEndLine() == sc2.getEndLine())
		{
			if(sc1.getStartCol() <= sc2.getStartCol() && sc1.getEndCol() >= sc2.getEndCol())
				return true;
		}
		return false;
	}

}
