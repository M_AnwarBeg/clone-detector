package cmt.cddc.simpleclones;
import java.util.ArrayList;

public interface ICloneExtract 
{	
	public ArrayList<SimpleClone> getSimpleClones(TokensList token_list);
}
