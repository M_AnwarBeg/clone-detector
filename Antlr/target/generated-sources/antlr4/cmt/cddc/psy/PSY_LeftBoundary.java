package cmt.cddc.psy;

/**
 * 
 * @author maroof
 * 
 * It is being used in PSY Algorithm(Clone Detection)
 *
 */
public class PSY_LeftBoundary{
    
    PSY_LeftBoundary(int a ,int b , int c)
    {
        lcp = a;
        lb = b;
        bwt1 = c;
    }
    public int lcp;
    public int lb;
    public int bwt1;
}