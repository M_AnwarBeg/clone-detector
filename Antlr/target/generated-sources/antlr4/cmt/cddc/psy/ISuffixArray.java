package cmt.cddc.psy;

public interface ISuffixArray
{
    public int[] createSuffixArray(int[] str);
}
