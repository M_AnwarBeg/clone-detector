package cmt.cddc.psy;
import java.util.ArrayList;
import java.util.Stack;

import cmt.cddc.clonedetectioninitializer.SysConfig;

public class PSY {

    private int LEletter(int l1, int l2)
    {
        if(l1 == -1 || l1 != l2)
            return -1;
        return l1;
    }
    
    /**
     * This algorithm detects the clones using LCP - Longest 
     * Common Prefix, BWT - Burrows Wheel Transform & Suffix 
     * Array 
     * */
    public ArrayList<PSY_clones> PSY_algo(int[] SA,int[] BWT,int[] LCP)
    {
        int lcp = LCP[0]; 
        int lb = 0; 
        int bwt1 = BWT[0];
        Stack<PSY_LeftBoundary> lbStack = new Stack<PSY_LeftBoundary>();
        PSY_LeftBoundary obj = new PSY_LeftBoundary(lcp, lb, bwt1);
        lbStack.push(obj);
        ArrayList<PSY_clones> clones = new ArrayList<PSY_clones>();
        for (int  i = 0 ; i < SA.length ; i++)
        {
            lb = i;
            lcp = LCP[i+1];
            int bwt2 = BWT[i+1];
            int bwt = LEletter(bwt1,bwt2);
            bwt1 = bwt2;
            while(lbStack.peek().lcp > lcp)
            {
            	PSY_LeftBoundary obj1 = lbStack.pop();
                if(obj1.bwt1 == -1 && obj1.lcp >= SysConfig.getThreshold())
                {
                	if(!clones.isEmpty()&&clones.get(clones.size()-1).right_boundry==obj1.lb)
                	{
                		clones.add(new PSY_clones(obj1.lcp,clones.get(clones.size()-1).left_boundry,i));
                	}
                	else
                		clones.add(new PSY_clones(obj1.lcp,obj1.lb,i));
                
                }
                lb = obj1.lb; //i; 
                lbStack.peek().bwt1 = LEletter(obj1.bwt1 ,  lbStack.peek().bwt1);
                bwt = LEletter(obj1.bwt1, bwt);
              }
                if(lbStack.peek().lcp == lcp)
                    lbStack.peek().bwt1 = LEletter(lbStack.peek().bwt1, bwt);
                else
                    lbStack.push(new PSY_LeftBoundary(lcp, lb, bwt));
        }
		return clones;
        
    }
}
