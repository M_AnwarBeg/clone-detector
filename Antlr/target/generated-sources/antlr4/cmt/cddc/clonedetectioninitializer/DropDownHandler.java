package cmt.cddc.clonedetectioninitializer;

import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.ui.packageview.PackageFragmentRootContainer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.Workbench;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cvac.views.SccView;

public class DropDownHandler extends AbstractHandler 
{
	  private static final String PARM_MSG = "z.ex.dropdown.msg";
	  private static IWorkbenchWindow window;
	  private CloneDetectionSettingsWizard cdsWizard;

	  public static IProject getCurrentProject()
	  {
		ISelectionService selectionService = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService();
		ISelection selection = selectionService.getSelection();
		IProject project = null;
		if (selection instanceof IStructuredSelection)
		{
		    Object element = ((IStructuredSelection) selection).getFirstElement();
	
		    if (element instanceof IResource)
		    {
			project = ((IResource) element).getProject();
		    } else if (element instanceof PackageFragmentRootContainer)
		    {
			IJavaProject jProject = ((PackageFragmentRootContainer) element).getJavaProject();
			project = jProject.getProject();
		    } else if (element instanceof IJavaElement)
		    {
			IJavaProject jProject = ((IJavaElement) element).getJavaProject();
			project = jProject.getProject();
		    }
		}
		return project;
	  }
	  public Object execute(ExecutionEvent event) 
	  {
		  	String msg = event.getParameter(PARM_MSG);
		  	if(msg!=null)
		  	{
			    
			    window=PlatformUI.getWorkbench().getActiveWorkbenchWindow();
			    
			    if(msg.equals("Complete System Clone Detection"))
			    {
				    	cdsWizard = new CloneDetectionSettingsWizard();
					    ISelection selection = window.getSelectionService().getSelection();
					    IStructuredSelection selectionToPass = StructuredSelection.EMPTY;
					    if (selection instanceof IStructuredSelection)
						    {
							selectionToPass = (IStructuredSelection) selection;
						    }
					    cdsWizard.init(window.getWorkbench(), selectionToPass);
					    Shell shell = window.getWorkbench().getActiveWorkbenchWindow().getShell();
					    WizardDialog dialog = new WizardDialog(shell, cdsWizard);
					    try
						    {
								dialog.create();
								int res = dialog.open();
								//notifyResult(res == Window.OK);
						    } 
					    catch (Exception e)
						    {
								e.printStackTrace();
								System.err.println(e.getMessage());
						    }
			    }
			    else if(msg.equals("Visualize Detected Clones"))
			    {
			    		ClonesReader reader = new ClonesReader();
			    		reader.readClones();
			    		IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					   try
						{
						   	CloneDetectionSettingsWizard.closeOpenedViews(workbenchPage);
						    workbenchPage.showView("mccView");
						    workbenchPage.showView("sccView");
						    workbenchPage.showView("fccView");
						    workbenchPage.showView("mcsView");
						    workbenchPage.showView("fcsView");

						}
					   catch (Exception e)
						{
						    e.printStackTrace();
						}
			    }
		  	}
		    return event;
	  }
}