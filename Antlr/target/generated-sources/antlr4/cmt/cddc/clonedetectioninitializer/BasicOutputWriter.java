package cmt.cddc.clonedetectioninitializer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import antlr.auto.gen.java.JavaLexer;
import cmt.cddc.simpleclones.Tokens;
import cmt.cddc.simpleclones.TokensList;
import cmt.cddc.simpleclones.Utilities;
import cmt.cddc.structures.sDir_List;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;
import cmt.common.Directories;

public final class BasicOutputWriter 
{
    public static void writeBasicOutputFiles(TokensList tokens_list)
    {
    	createCombinedTokens_File(tokens_list);
    	createMethodsInfo_File(tokens_list);
    	createFilesInfo_File(tokens_list);
    	writeDirsInfo();
    	createRepetitions_File(tokens_list);
    }
	
    public static void createCombinedTokens_File(TokensList tokens_list)
    {
 	   PrintWriter writer;
		   try 
		   {
			   int counter = 0;			   		  
			   writer = new PrintWriter(Directories.getAbsolutePath(Directories.COMBINEDTOKENSFILE), "UTF-8");			  
			   writer.println((String.format("%8s %8s %8s %8s %8s %8s %8s %8s %-8s","No.","File","Line","Col.","Cls.","Mthd.","Rep.","Tok.","      Text")));
			   for(Tokens token : tokens_list.getTokens())
			   {
				   if(token.getType() == JavaLexer.COMMENT || token.getType() == JavaLexer.LINE_COMMENT)
					   continue;
				   
				   int repCheck = 0;
				   if(token.getTokenRepetition())
					   repCheck = 1;
				   writer.println(String.format("%8s %8s %8s %8s %8s %8s %8s %8s %8s", counter++,token.getTokenFile(), token.getTokenLine(),token.getTokenColumn(),token.getTokenClass(),token.getTokenMethod(),repCheck,token.getType(),"      " +token.getText()));				  
			   }
	    	   writer.close();
		   } 
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }    	 
    }
	
	//write methods info: method id, file id, method name, starting line no , ending line no, total no of tokens
	private static void createMethodsInfo_File(TokensList tokens_list)
	{
		   PrintWriter writer;
		   try 
		   {
			   writer = new PrintWriter(Directories.getAbsolutePath(Directories.METHODSINFOFILE), "UTF-8");	
			   for(int m = 0 ; m < sMethod_List.getMethodListSize() ; m++)
			   {
				   sMethod method = sMethod_List.getsMethodAt(m);
				   writer.println(m + ";" + method.getFileID() + ";"+method.getMethodName()+"(" +method.getMethodSig()+ ")" + ";" + tokens_list.getTokenAt(method.getStartToken()).getTokenLine() + ";" + tokens_list.getTokenAt(method.getEndToken()).getTokenLine() +";" + (method.getEndToken() - method.getStartToken() + 1));   
			   }
			   writer.close();
		   } 
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }    
	}
	
	//write files info: file id, dir id, total tokens
	private static void createFilesInfo_File(TokensList tokens_list)
	{
		   PrintWriter writer;
		   try 
		   {
			   writer = new PrintWriter(Directories.getAbsolutePath(Directories.FILESINFOFILE), "UTF-8");	
			   for(int f = 0 ; f < sFile_List.getFilesListSize() ; f++)
			   {
				   sFile file = sFile_List.getsFileAt(f);
				   writer.println(f + "," + file.getDirID() + "," + (file.getFileEndToken() - file.getFileStartToken() +1) + "," + tokens_list.getTokenAt(file.getFileEndToken()-2).getTokenLine());  //-2 to get the end line number of file, as the last two tokens are ENDFILE and ENDCLASS markers...
			   }
			   writer.close();
		   } 
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }    
	}
	
	//write directory names to a file for reference

	private static void writeDirsInfo() 
	{
		PrintWriter writer;
		   try 
		   {	
			   writer = new PrintWriter(Directories.getAbsolutePath(Directories.DIRSINFOFILE), "UTF-8");	
	           for (int i = 0; i < sDir_List.getDirListSize(); i++) {
	        	   writer.println(i + "," + sDir_List.getDirAt(i).getStrPath());	                
	           }
	           writer.close();
		   }
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   }  
	}
	
    private static void createRepetitions_File(TokensList tokens_list)
	{
		PrintWriter writer;
		//boolean check = false;
		try 
		{
			 writer = new PrintWriter(Directories.getAbsolutePath(Directories.REPETITIONS), "UTF-8");	
			 for (int i = 0; i < tokens_list.getTokensListSize() - 1; i++) 
			 {
			        //start of repetition: 0 -> 1
				    if(!tokens_list.getTokenAt(i).getTokenRepetition() && tokens_list.getTokenAt(i+1).getTokenRepetition())
			        {
			            int t = tokens_list.getTokenAt(i+1).getType();
			            int fileNumber = 0;
			            while (i + 1 <= sFile_List.getFilesListSize() && i + 1 > sFile_List.getsFileAt(fileNumber).getFileEndToken()) 
			            {
			                fileNumber++;
			            }
			            //if(!check)
			           // {
			            	writer.print(sFile_List.getsFileAt(fileNumber).getFullPath() + ":" + tokens_list.getTokenAt(i+1).getTokenLine());
			            	//check = true;
			            //}
			        }
			            //end of repetition: 1 -> 0
			        else if (tokens_list.getTokenAt(i).getTokenRepetition() && !tokens_list.getTokenAt(i+1).getTokenRepetition()) 
			        {
			            //hamid : fixing repetition breaking at endmethod/startmethod            
			            int t = tokens_list.getTokenAt(i+1).getType();
			           
			            writer.println(" - " + tokens_list.getTokenAt(i).getTokenLine());
			           // check = false;
			        }
			  }
			 writer.close();  
		}		 
	    catch (FileNotFoundException e) 
	    {
		   // TODO Auto-generated catch block
	 	   e.printStackTrace();
	    } 
	    catch (UnsupportedEncodingException e) 
	    {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
	    }   
	}
}
