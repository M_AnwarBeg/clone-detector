package cmt.cddc.clonedetectioninitializer;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.ui.packageview.PackageFragmentRootContainer;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.internal.Workbench;

public class CloneDetectionSettingsWizardDelegate extends Action implements org.eclipse.ui.IWorkbenchWindowPulldownDelegate{
	
	 private static IWorkbenchWindow window;
	 private CloneDetectionSettingsWizard cdsWizard;
	    
	public CloneDetectionSettingsWizardDelegate() 
	{
		super();
	}
	
    public static IProject getCurrentProject()
    {
	ISelectionService selectionService = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService();
	ISelection selection = selectionService.getSelection();
	IProject project = null;
	if (selection instanceof IStructuredSelection)
	{
	    Object element = ((IStructuredSelection) selection).getFirstElement();

	    if (element instanceof IResource)
	    {
		project = ((IResource) element).getProject();
	    } else if (element instanceof PackageFragmentRootContainer)
	    {
		IJavaProject jProject = ((PackageFragmentRootContainer) element).getJavaProject();
		project = jProject.getProject();
	    } else if (element instanceof IJavaElement)
	    {
		IJavaProject jProject = ((IJavaElement) element).getJavaProject();
		project = jProject.getProject();
	    }
	}
	return project;
    }

	public void run(IAction action) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		if (getCurrentProject() == null)
    	{ 
    	    MessageBox box = new MessageBox(window.getShell(), SWT.ICON_INFORMATION);
    	    box.setMessage("Please Select a Project!!!");
    	    box.open();
    	} 
    	else 
    	{
	    	cdsWizard = new CloneDetectionSettingsWizard();
		    ISelection selection = window.getSelectionService().getSelection();
		    IStructuredSelection selectionToPass = StructuredSelection.EMPTY;
		    if (selection instanceof IStructuredSelection)
			    {
				selectionToPass = (IStructuredSelection) selection;
			    }
		    cdsWizard.init(window.getWorkbench(), selectionToPass);
		    Shell shell = window.getWorkbench().getActiveWorkbenchWindow().getShell();
		    WizardDialog dialog = new WizardDialog(shell, cdsWizard);
		    try
			    {
					dialog.create();
					int res = dialog.open();
					//notifyResult(res == Window.OK);
			    } 
		    catch (Exception e)
			    {
					e.printStackTrace();
					System.err.println(e.getMessage());
			    }
	    }
		
	}
	@Override
	public Menu getMenu(Control control) 
	{
		Menu menu = new Menu(control);
		
		MenuItem completeCloneDetection =  new MenuItem(menu,1);
		completeCloneDetection.setText("Complete System Clone Detection");
		
		MenuItem incrementalCloneDetection =  new MenuItem(menu,1);
		incrementalCloneDetection.setText("Incremental Clone Detection");
		
		MenuItem framedCloneBrowsing =  new MenuItem(menu,1);
		framedCloneBrowsing.setText("Framed Clones Browsing");
		return menu;
	}

	public void selectionChanged(IAction arg0, ISelection arg1)
	{
		
	}
	public void init(IWorkbenchWindow w) 
	{
		this.window=w;
	}


	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
