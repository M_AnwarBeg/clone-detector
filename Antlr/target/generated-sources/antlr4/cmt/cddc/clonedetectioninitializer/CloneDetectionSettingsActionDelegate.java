package cmt.cddc.clonedetectioninitializer;

import javax.swing.JOptionPane;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.ui.packageview.PackageFragmentRootContainer;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.internal.Workbench;

import cmt.cfc.incremental.IncrementalObserver;

public class CloneDetectionSettingsActionDelegate extends Action implements IWorkbenchWindowActionDelegate
{

    private IWorkbenchWindow window;
    private CloneDetectionSettingsWizard cdsWizard;

    public CloneDetectionSettingsActionDelegate()
    {
    	super();
    }

    @Override
    public void dispose()
    {

    }

    @Override
    public void init(IWorkbenchWindow window)
    {
    	this.window = window;
    }

    @Override
    public void run(IAction action)
    {
    	//IncrementalObserver.getUpdatedClones();  //(Un-Comment this to run front-back propagation)
    	
		    cdsWizard = new CloneDetectionSettingsWizard();
		    ISelection selection = window.getSelectionService().getSelection();
		    IStructuredSelection selectionToPass = StructuredSelection.EMPTY;
		    if (selection instanceof IStructuredSelection)
			    {
					selectionToPass = (IStructuredSelection) selection;
			    }
		    cdsWizard.init(window.getWorkbench(), selectionToPass);
		    Shell shell = window.getWorkbench().getActiveWorkbenchWindow().getShell();
		    WizardDialog dialog = new WizardDialog(shell, cdsWizard);
		    try
			    {
					dialog.create();
					int res = dialog.open();
					//notifyResult(res == Window.OK);
			    } 
		    catch (Exception e)
			    {
				e.printStackTrace();
				System.err.println(e.getMessage());
			    }
    }

    @Override
    public void selectionChanged(IAction action, ISelection selection)
    {

    }
    
    public static IProject getCurrentProject()
    {
	ISelectionService selectionService = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService();
	ISelection selection = selectionService.getSelection();
	IProject project = null;
	if (selection instanceof IStructuredSelection)
	{
	    Object element = ((IStructuredSelection) selection).getFirstElement();

	    if (element instanceof IResource)
	    {
		project = ((IResource) element).getProject();
	    } else if (element instanceof PackageFragmentRootContainer)
	    {
		IJavaProject jProject = ((PackageFragmentRootContainer) element).getJavaProject();
		project = jProject.getProject();
	    } else if (element instanceof IJavaElement)
	    {
		IJavaProject jProject = ((IJavaElement) element).getJavaProject();
		project = jProject.getProject();
	    }
	}
	return project;
    }

}
