package cmt.cddc.clonedetectioninitializer;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.INewWizard;

public class CloneDetectionSettingsWizardPage4 extends WizardPage {

	public CloneDetectionSettingsWizardPage4(String pageName, String title, ImageDescriptor titleImage) {
		// TODO Auto-generated constructor stub
		super(pageName, title, titleImage);
	}

	public CloneDetectionSettingsWizardPage4(CloneDetectionSettingsWizard cloneDetectionSettingsWizard) {
		// TODO Auto-generated constructor stub
		super("Equal Tokens Settings");
		 setTitle("Settings for Equal Tokens");
			setDescription("Select multiple tokens from the token list below to make "
				+ "them equivalent for Clone Detection (Example: \"3=18, 7=9=12\").");
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		
	}

}
