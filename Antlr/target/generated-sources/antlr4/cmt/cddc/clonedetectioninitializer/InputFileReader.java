package cmt.cddc.clonedetectioninitializer;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.structures.sDir;
import cmt.cddc.structures.sDir_List;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sGroup;
import cmt.cddc.structures.sGroup_List;
import cmt.clonedetectionSettingsUI.CloneTrackingFormPage;
import cmt.common.Directories;
import cmt.ctc.mongodb.AllFilesContent;

public final class InputFileReader {

	@SuppressWarnings({ "resource", "finally" })
	public static AllFilesContent readFiles() throws FileNotFoundException, URISyntaxException
	{
		//ArrayList<String> filesContent = new ArrayList<String>();
		ArrayList<String> updatedfilesContent = new ArrayList<String> ();
		ArrayList<String> newfilesContent = new ArrayList<String> ();
    	ArrayList<String> filesContent = new ArrayList<String> ();
    	ArrayList<String> updatedfiles = new ArrayList<String> ();
    	ArrayList<String> oldfiles = new ArrayList<String> ();
    	ArrayList<String> newfiles = new ArrayList<String> ();
    	ArrayList<String> removedfiles = new ArrayList<String>();
    	AllFilesContent allfilescontent=new AllFilesContent();
    	java.util.Date lastrun=checklastrun(); 
    	
    	try 
    	{
    		MongoClient mongo = new MongoClient("localhost:27017");
            DB db = mongo.getDB("admin");
            DBCollection collection = db.getCollection("FileInfo");
            if(CloneTrackingFormPage.isIncremental)
            {
            	populateFileList(collection);
            }
            	
            
            int fileId = sFile_List.getMaxFileId() + 1;
            File f = new File(Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES));
            int fileIdForDirnGroup = 0;	
		    if(f.isFile() && f.getAbsoluteFile() != null)
		    {					
				Scanner scanner = new Scanner(f);
				scanner.useDelimiter(";");  //Group of files separated by semi-colon(;) in InputFiles.txt...
		
				while(scanner.hasNext()) 
				{
					boolean newGroupInd = true;
				    String input = scanner.next();
				    //String [] parts = input.split(":" , 2);
				    //String groupName = parts[0].replaceAll("(\\r|\\n)", "");
				    //String [] files = parts[1].split("\n");
				    String [] files = input.split("\n");
				    for(int i=0 ; i < files.length ; i ++)
				    {				   
				    	String file = files[i];
				    	if(file.endsWith("\r"))		    		
				    		file = files[i].substring(0,files[i].length()-1);
				    	
				    	if(file != null && file != "" && file.length() > 1)
				    	{
				    		File fil = new File(file);				    	
 				    		java.util.Date timestamp = new java.util.Date(fil.lastModified());				    	
					    									
							//int index = file.lastIndexOf("\\");
							//String fileName = file.substring(index + 1);
							//String fullPath = file.substring(0, index);							
							
					    	//sFile sf = new sFile(fileName.toString());
					    	//sf.setFullPath(file);
					    	//sFile_List.addsFile(sf);
				    		boolean newFileCheck = false;
				    		if(checkIfNewlyAddedFile(fil))
				    			newFileCheck = true;
				    		
				    		if(lastrun.equals(new java.util.Date(Long.MIN_VALUE)) || newFileCheck)
							{
				    			String content = null;
				    			try
					    		{
				    				content = new Scanner(new File(file)).useDelimiter("\\Z").next();
					    		}
				    			catch(FileNotFoundException e)
					    		{
					    			System.out.println(file+ " NOT FOUND...");
					    		}
								if(content != null)
						        {	
																					
									Path p = Paths.get(file);
									sFile file_temp = new sFile(p.getFileName().toString(),fileId);			
									file_temp.setFullPath(file);
									file_temp.setUpdated(false);
									
									if(newFileCheck)
									{
										newfilesContent.add(content);
										newfiles.add(file_temp.getFileName()) ;
										file_temp.setNewlyAddedFile(true);
										allfilescontent.setrunflag(false);
									}
									else
									{
									    filesContent.add(content);
										oldfiles.add(file_temp.getFileName());
										allfilescontent.setrunflag(true);
									}
								
								    sFile_List.addsFile(file_temp);				
						    		BasicDBObject document = new BasicDBObject();
						    		
						    		document.put("filename", p.getFileName().toString());
						    		document.put("fullpath", file);
						    		document.put("fileId", fileId);
						    		
						    		if(CloneTrackingFormPage.isIncremental) //LoadList.framingEnabledFlag)
				             		{
						    			InsertInDB(collection,document);
				             		}
						    		fileIdForDirnGroup = fileId;
						    		fileId++;
						        }
								
								
							}	
				    		else if(lastrun.before(timestamp))
							{
				    			String content = null;
				    			try
					    		{
				    				content = new Scanner(new File(file)).useDelimiter("\\Z").next();
					    		}
				    			catch(FileNotFoundException e)
					    		{
					    			System.out.println(file+ " NOT FOUND...");
					    		}
								if(content != null)
						        {							
									allfilescontent.setrunflag(false);
									updatedfilesContent.add(content);				
									Path p = Paths.get(file);
	
									List<DBObject> rs = new ArrayList<DBObject> (); 
	
									DBObject document1 = new BasicDBObject("filename", p.getFileName().toString());  
									DBObject document2 = new BasicDBObject("fullpath", file);       
	
									BasicDBList or = new BasicDBList();
									or.add(document1);
									or.add(document2);
									DBObject query = new BasicDBObject("$and", or);
	
									DBCursor cursor=collection.find(query);//user is the collection
	
									while(cursor.hasNext()){
										rs.add(cursor.next());
									}
									int fid=0;
									for(int n=0; n<rs.size(); n++) 
									{
										DBObject obj=rs.get(0);
										fid=(int) obj.get( "fileId" );
									}
									
									sFile_List.getFile(fid).setNewlyAddedFile(false);
									sFile_List.getFile(fid).setUpdated(true);
									
									//sFile file_temp = new sFile(p.getFileName().toString(),fid);			
									//file_temp.setFullPath(file);
									//file_temp.setUpdated(true);
									//file_temp.setNewlyAddedFile(false);
									
									updatedfiles.add(sFile_List.getFile(fid).getFileName());
																		
									//sFile_List.addsFile(file_temp);		
									fileIdForDirnGroup = fid;
						        }
								
							}				    		
				    		else
							{								
								allfilescontent.setrunflag(false);		
								Path p = Paths.get(file);

								List<DBObject> rs = new ArrayList<DBObject> (); 

								DBObject document1 = new BasicDBObject("filename", p.getFileName().toString());  
								DBObject document2 = new BasicDBObject("fullpath", file);       

								BasicDBList or = new BasicDBList();
								or.add(document1);
								or.add(document2);
								DBObject query = new BasicDBObject("$and", or);

								DBCursor cursor=collection.find(query);//user is the collection

								while(cursor.hasNext())
								{
									rs.add(cursor.next());
								}
								int fid=0;
								for(int n=0; n<rs.size(); n++) 
								{
									 DBObject obj=rs.get(0);
									fid=(int) obj.get( "fileId" );
								}

								sFile_List.getFile(fid).setUpdated(false);
								sFile_List.getFile(fid).setNewlyAddedFile(false);
								
								//sFile file_temp = new sFile(p.getFileName().toString(),fid);			
								//file_temp.setFullPath(file);
								oldfiles.add(sFile_List.getFile(fid).getFileName()) ;
								//file_temp.setUpdated(false);
								//file_temp.setNewlyAddedFile(false);
								//sFile_List.addsFile(file_temp);
								fileIdForDirnGroup = fid;
							}
				    		
				    		try {
				    		initializeDirs(fileIdForDirnGroup, file);			    	
					    	initializeGroups(fileIdForDirnGroup , newGroupInd);// , groupName);
					    	newGroupInd = false;					    	
				    		}
				    		catch(Exception exp) {
				    			exp.printStackTrace();
				    		}
				    	}
				    }		 				
				}
				removedfiles = getRemovedFiles();	
				createOutputDirectory();  //For output files to be stored
				scanner.close();
			}
    	}
    	finally
    	{    		
    		allfilescontent.setOld(filesContent);
    		allfilescontent.setUpdated(updatedfilesContent);
    		allfilescontent.setOldfiles(oldfiles);
    		allfilescontent.setUpdatedfiles(updatedfiles);
    		allfilescontent.setNewfiles(newfiles);
    		allfilescontent.setNewcontent(newfilesContent);
    		allfilescontent.setRemovedfiles(removedfiles);
    		return allfilescontent;
    	}
    }
	
	public static void populateGroupInfoTable()
	{
		int groupID = 0;
        File f = new File(Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES_GROUP));
	    if(f.isFile() && f.getAbsoluteFile() != null)
	    {
	    	try
	    	{
				Scanner scanner = new Scanner(f);
				scanner.useDelimiter("\n");
				
				while(scanner.hasNext())
				{
					String input = scanner.next();
				    String [] files = input.split("~");
				    String groupName = files[0];
				    String groupPath = files[1].replace("\\", "/");
				    Statement st = CloneRepository.getConnection().createStatement();
				    st.execute("use "+CloneRepository.getDBName()+";");
				    st.execute("insert into groupinfo (gid, gname, gpath) values ("+groupID+", \""+groupName+"\",\""+groupPath+"\");");
				    groupID++;
				}
	    	}
	    	catch(Exception e)
	    	{
	    		e.printStackTrace();
	    	}
	    }
	
	}
	
	private static void populateFileList(DBCollection collection)
	{
		if(collection != null)
        {
			ArrayList<String> removedFiles = CloneTrackingFormPage.getDeletedFileNames();
	        DBCursor cursor = collection.find();
            while(cursor.hasNext()) 
            {
                DBObject obj = cursor.next();
                sFile file = new sFile((String) obj.get("filename") , (int) obj.get("fileId"));
                boolean check = true;
                for(int i = 0 ; i < removedFiles.size() ; i++)                
                	if(((String)obj.get("fullpath")).equals(removedFiles.get(i)))
	                	check = false;                
                
                if(check)
                {
	                file.setFullPath((String)obj.get("fullpath"));
	                sFile_List.addsFile(file);
                }
            }
        }
	} 
	
	private static ArrayList<String> getRemovedFiles()
	{
		ArrayList<String> removedFiles = CloneTrackingFormPage.getDeletedFileNames(); 
		
		/*for(int i = 0 ;  i < removedFiles.size() ; i++)
		{
			for(int j = 0 ; j < sFile_List.getFilesListSize() ; j++)
			{
				if(sFile_List.getsFileAt(j).getFullPath().equals(removedFiles.get(i)))
				{
					sFile_List.removeFileById(sFile_List.getsFileAt(j).getFileID());
					j--;
				}
			}
		}*/
	/*	new ArrayList<String>();
		Hashtable<Integer, String> fileList	=	CloneRunUpdateTemp.getFileToIdMapping();
	//	Hashtable<Integer, String> fileList  = CloneRunInfoReader.getFileListID();
		ArrayList<Integer> removedFilesId = CloneRunUpdateTemp.getDeletedFileID();
		for(Integer fileId  : removedFilesId)
		{
			removedFiles.add(fileList.get(fileId)); 
		}*/
		return removedFiles;
	}
	
	private static boolean checkIfNewlyAddedFile(File file)
	{		
		String path = file.getPath();
	//	path = path.replaceAll("\\", "/");
		//HashSet<String> fileList  = CloneRunUpdateTemp.getNewlyAddedFiles();
		HashSet<String> fileList  = CloneTrackingFormPage.getNewlyAddedFiles();
		boolean newFile = false;
		System.out.println("Find File "+ file.getAbsolutePath());
		Iterator fileListItr = fileList.iterator();
		while(fileListItr.hasNext()) {
			String fileName = (String) fileListItr.next();
		//	path=path.replaceAll("\\\\", "/");
			System.out.println(fileName);
			if(fileName.trim().equals(path.trim())) {
				return true;
			}
	    }	
		return newFile;
	}
	
    private static void InsertInDB(DBCollection collection,BasicDBObject document)
    {
    	collection.insert(document);
  	}
    	  
	private static java.util.Date checklastrun() 
	{
		try
		{ 
			
				MongoClient mongo = new MongoClient("localhost:27017");
	            DB db = mongo.getDB("admin");
	            BasicDBObject whereQuery = new BasicDBObject();
	            whereQuery.put("ProjectName", "Test");
	            List<DBObject> rs = new ArrayList<DBObject> (); 
	            DBCursor cursor = (DBCursor) db.getCollection("ExecutionTime").find(whereQuery).iterator();
	            if(cursor.count() != 0 && CloneTrackingFormPage.isIncremental)
	            {
	                while (cursor.hasNext()) 
	                {
	                	rs.add(cursor.next());
	                }
	                 DBObject DataObj = rs.get(0);
	                
	                 java.util.Date date=(java.util.Date) new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse((String) DataObj.get("LastRun"));  
	                 return date;
	      
	                 //DateFormat df2 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	                 //return (Date)df2.parse((String) DataObj.get("LastRun"));
	             }
	             else
	             {	 
	            	if(CloneTrackingFormPage.isIncremental)
	     			{
		                DBCollection collection = db.getCollection("ExecutionTime");
		                BasicDBObject document = new BasicDBObject();
		                document.put("ProjectName", "Test");
		                document.put("Status", "active");
		                document.put("LastRun", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));
		                collection.insert(document);
	     			}
		            return new java.util.Date(Long.MIN_VALUE);
	     			
	             }			
		}
        catch(MongoException e)
		{ 
        	System.out.println(e);
        }
		catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;				
	}
	
	private static void initializeDirs(int fileId , String fullPath)
	{
		int index = fullPath.lastIndexOf("\\");
		String dirPath = fullPath.substring(0, index);	
		String dirName = dirPath.substring(dirPath.lastIndexOf("\\")+1,dirPath.length());
		
		boolean bDirExist = false;
	    for (int i = 0; i < sDir_List.getDirListSize(); i++) {
	        if (dirPath.equals(sDir_List.getDirAt(i).getStrPath())) {
	            bDirExist = true;
	            sDir_List.getDirAt(i).addContainedFile(fileId);
	            //update the files with the dir no.	            
	            sFile_List.getFile(fileId).setDirID(i);
	            break;
	        }
	    }
	    if (!bDirExist) {
	        sDir aDir = new sDir();
	        aDir.setStrPath(dirPath);
	        aDir.setDirName(dirName);
	        sDir_List.addDir(aDir);
	        sDir_List.getDirAt(sDir_List.getDirListSize()-1).addContainedFile(fileId);	        
	        //update the files with the dir no.
	        sFile_List.getFile(fileId).setDirID(sDir_List.getDirListSize() -1);
	    }
	}
	
	private static void initializeGroups(int fileId , boolean newGroupInd)/* , String groupName)*/
	{
	    //create a new group structure
	    if (newGroupInd) {
	        sGroup aGroup = new sGroup();
	        //aGroup.setGroupName(groupName);
	        if(sGroup_List.getGroupListSize() <= 0)
	        	aGroup.setGroupID(0);
	        else
	        	aGroup.setGroupID(sGroup_List.getGroupListSize());
	        sGroup_List.addsGroup(aGroup);
	    }
	    sGroup_List.getGroupAt(sGroup_List.getGroupListSize()- 1).addContainedFiles(fileId);
	    //update the files with the group no.
	    sFile_List.getFile(fileId).setGroupID(sGroup_List.getGroupListSize() - 1);
	}
	private static void createOutputDirectory()
	{
		new File(".//output").mkdirs();
	}
}
