package cmt.cddc.clonedetectioninitializer;

import java.util.ArrayList;
import java.util.Scanner;

import com.mongodb.MongoClient;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.fileclones.FccWriter;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.fileclones.StructuralClones_FileClusters;
import cmt.cddc.methodclones.MccWriter;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.StructuralClones_MethodClusters;
import cmt.cddc.simpleclones.CloneDetector_PSY;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneWriter;
import cmt.cddc.simpleclones.TokensList;
import cmt.cddc.structures.sClass_List;
import cmt.cddc.structures.sDir_List;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sGroup_List;
import cmt.cddc.structures.sMethod_List;
import cmt.clonedetectionSettingsUI.CloneTrackingFormPage;
import cmt.ctc.mongodb.AllFilesContent;

import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
 
public class CloneDetector
{    
    public static void main(String[] args) throws Exception
    {

    }

	public static void detectClones() 
	{
		   AllFilesContent inputFiles = null; 
		   
	       try 
	       {
	    	    clearStructures();
	    		inputFiles = InputFileReader.readFiles();    		  
	       }       
	       catch(FileNotFoundException exception )  
	       {
	    	   System.out.println(exception.getMessage());
	    	   System.out.println(exception.getStackTrace());    
	       }       
	       catch(URISyntaxException exception ) 
	       {
	    	   System.out.println(exception.getMessage());
		       System.out.println(exception.getStackTrace());
	       }
	       if(inputFiles != null)
	       {	    	   
	    	   //-------------------------------------------- SIMPLE CLONES -----------------------------------------------//
	    	   CloneDetector_PSY cloneDetect = new CloneDetector_PSY();     
	    	   
	    	   TokensList tokens_list = cloneDetect.tokenizeInput(inputFiles);    	   
	    	   if(inputFiles.getrunflag() && tokens_list != null && tokens_list.getTokensListSize() > 0 && !CloneTrackingFormPage.isIncremental)
	    	   {
	    		   cloneDetect.updateTokenListWithMethodIds(tokens_list);
		    	   ArrayList<SimpleClone> simpleClones = cloneDetect.detectSimpleClone(tokens_list); 
		    	   
		    	   SimpleCloneWriter.writeSimpleCloneFiles(simpleClones, tokens_list, false);   
		    	   
		    	   cloneDetect.updateFilesWithSimpleClones(simpleClones);
		    	   cloneDetect.updateMethodsWithSimpleClones(simpleClones);
		    	   
		    	   BasicOutputWriter.writeBasicOutputFiles(tokens_list);
		    	   SimpleCloneWriter.writeInfileStructures();
		    	   SimpleCloneWriter.writeInMethodStructures();
		    	   
		    	   //----------------------------------------------------------------------------------------------------------//
		    	   
		    	   
		    	   //---------------------------------- FILE CLUSTERS / FCC ---------------------------------------------------//
		    	   ArrayList<FileCluster> fileClusters = StructuralClones_FileClusters.detectFileClusters(simpleClones);
				   FccWriter.writeFileClusters(fileClusters , false);	
				   
				   StructuralClones_FileClusters.pruneFileClusters(fileClusters);
				   FccWriter.writeFileClusters(fileClusters , true);  //writing pruned file clusters...
				   
				   StructuralClones_FileClusters.updateDirsWithFileClones(fileClusters); 
				   FccWriter.writeFileClonesByDirs();
				   
				   FccWriter.writeInDirsFileCloneStructures();//writeInDirsFileCloneStructures();
				   
				   StructuralClones_FileClusters.findCrossDirsFileCloneStructures();
				   FccWriter.writeCrossDirsFileCloneStructures(fileClusters);
				   
				   StructuralClones_FileClusters.updateGroupsWithFileClones(fileClusters);
				   FccWriter.writeFileClonesByGroups();
				   
				   FccWriter.writeInGroupsFileCloneStructures();//writeInGroupsFileCloneStructures();
				   
				   StructuralClones_FileClusters.findCrossGroupsFileCloneStructures();
				   FccWriter.writeCrossGroupsFileCloneStructures(fileClusters);
				   //----------------------------------------------------------------------------------------------------------//
				   
				   
				   //-------------------------------------- METHOD CLUSTERS / MCC ---------------------------------------------//
				   ArrayList<MethodClones> methodClusters = StructuralClones_MethodClusters.detectMethodClusters(simpleClones);
				   MccWriter.writeMethodClusters(methodClusters , false);
				   
				   StructuralClones_MethodClusters.pruneMethodClusters(methodClusters, false);
				   MccWriter.writeMethodClusters(methodClusters , true);  //writing pruned method clusters...
				   
				   StructuralClones_MethodClusters.updateFilesWithMethodClones(methodClusters);
				   MccWriter.writeMethodClonesByFiles();
				   
				   StructuralClones_MethodClusters.findCrossFileCloneMethodStructures();
				   MccWriter.writeCrossFileCloneMethodStructures(methodClusters);
				   
				   
				   //----------------------------------------------------------------------------------------------------------//
		       
				   
				   //----------------------------------------- CROSS + IN STRUCTURES-------------------------------------------//
				   
				   MccWriter.writeMethodStructures(simpleClones);
				   FccWriter.writeFileStructures(simpleClones);
				   FccWriter.writeDirStructures(fileClusters);			   
				   FccWriter.writeGroupStructures(fileClusters);
				   
				   //----------------------------------------------------------------------------------------------------------//
				   
				   //---------------------------------------------------DATABASE ---------------------------------------------//
				   CloneRepository.openConnection();
				   CloneRepository.createSchema();
			/*	   CloneRepository.createFrameRepo();
            */	   CloneRepository.setDBName();
				   CloneRepository.getNameofDB();
				   CloneRepository.initQuery();
				   CloneRepository.clearDatabase();
				   CloneRepository.populateDatabase(0);
               }
			   
	       }
	       else
	    	   System.out.println("FILE NOT FOUND...");
	}
    private static void clearStructures() 
    {
	    sDir_List.clearDirList();
		sFile_List.clearFilesList();
		sMethod_List.clearMethodsList();
		(new TokensList()).clearTokensList();
		sClass_List.clearClassList();
		sGroup_List.clearGroupList();
	}

	@SuppressWarnings("unused")
	private static void initLangAndFileName() throws FileNotFoundException
    {
        Scanner sc = new Scanner(System.in);
        System.out.println("Select the type of language in which your code is written: ");
        System.out.println("1. PHP ");
        System.out.println("2. C# ");
        System.out.println("3. C");
        System.out.println("4. JAVA");
        System.out.println("5. PYTHON");
        System.out.println("6. RUBY");
        SysConfig.setLangChoice(sc.nextInt());
        while(SysConfig.getLangChoice() > 6 && SysConfig.getLangChoice() < 1)
        {
            System.out.println("Input Correct Number");
            SysConfig.setLangChoice(sc.nextInt());
        }
        
        System.out.println("Set the threshold limit for clone detection: ");
        SysConfig.setThreshold(sc.nextInt());
        
        System.out.println("Set the Group check: ");
        SysConfig.setGroupCheck(sc.nextInt());
        
        System.out.println("Set the minimum coverage limit of file cluster : ");
        SysConfig.setMinFClusP(sc.nextInt());
        
        System.out.println("Set the minimum tokens limit of file cluster : ");
        SysConfig.setMinFClusT(sc.nextInt());        
        
        System.out.println("Set the minimum coverage limit of method cluster : ");
        SysConfig.setMinMClusP(sc.nextInt());
        
        System.out.println("Set the minimum tokens limit of method cluster : ");
        SysConfig.setMinMClusT(sc.nextInt());
        
        getSuppressedTokens(sc);
        
        getTokensToEquate(sc);
               
        sc.close();
    }
    
    private static void getTokensToEquate(Scanner sc)
    {
        System.out.println("Enter the tokens you want to equate seperated by space: ");
        String readString = null;
        readString = sc.nextLine();
        
        SysConfig.setEquateTokens(new ArrayList<ArrayList<Integer>>());
        while(true)
        {
	        if(!(readString.isEmpty()))
	        {
	        	ArrayList<Integer> temp = new ArrayList<Integer>();
		        String[] stringArray = readString.split("\\s+");           
		        for (int i = 0; i < stringArray.length; i++) {
		        	temp.add(Integer.parseInt(stringArray[i]));
		        }  
		        SysConfig.addEquateToken(temp);		        
	        
		        System.out.println("Do you want to enter another of tokens to equate(press Y for Yes N for No)");
	            String choice = sc.next();
	            if(choice.equals("Y") || choice.equals("y"))
	            {
	            	System.out.println("Enter the tokens you want to suppress seperated by space: ");
	            	readString = sc.nextLine();       
	            	if(sc.hasNextLine())
	            	{
	            		readString = sc.nextLine(); 
	            	}
	            }
	            else
	            	break;
	        }
	        else
	        	break;
        }
    }
    private static void getSuppressedTokens(Scanner sc)
    {
        System.out.println("Enter the tokens you want to suppress seperated by space(press enter to exit): ");
        
        String readString = null;
        readString = sc.nextLine();
        if (sc.hasNextLine())
            readString = sc.nextLine();
        else
            readString = null;

        if(!(readString.isEmpty()))
        {
	        String[] stringArray = readString.split("\\s+");           
	        for (int i = 0; i < stringArray.length; i++) {
	        	SysConfig.setSuppressTokenAt(Integer.parseInt(stringArray[i]), true);
	        }    
        }
    }
}