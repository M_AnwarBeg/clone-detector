package cmt.cddc.clonedetectioninitializer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;


import cmt.common.NotificationPopUpUI;


public class CloneDetectionSettingsWizard extends Wizard implements INewWizard {
	
	private Process miner;
	private boolean isDelim = false;
	private static int stc = 30;
	private static int groupIndex = 0;
	ArrayList<Integer> clique;
	private Hashtable<Integer, String> token_details = new Hashtable<Integer, String>();
	private static ArrayList<Integer> SuppressedTokenID = new ArrayList<Integer>();
	private static ArrayList<ArrayList<Integer>> equateTokens;
	private static int languageChoice;
	private static int minFClust;
	private static int minFClusp;
	private static int minMClust;
	private static int minMClusp;
	private static int threshold;
	private static String projectName;
	private static String projectDiscrp;

	public static void setProjectDicrp(String Discrp) {
		projectDiscrp = Discrp;
	}

	public static String getProjectDicrp() {
		return projectDiscrp;
	}

	public static void setProjectName(String name) {
		projectName = name;
	}

	public static String getProjectName() {
		return projectName;
	}

	public static int getThreshold() {
		return threshold;
	}

	public static void setThreshold(int aa) {
		threshold = aa;
	}

	public static int getMinFClust() {
		return minFClust;
	}

	public static void setMinFClust(int a) {
		minFClust = a;
	}

	public static int getMinFClusp() {
		return minFClusp;
	}

	public static void setMinFClusp(int a) {
		minFClusp = a;
	}

	public static int getMinMClust() {
		return minMClust;
	}

	public static void setMinMClust(int a) {
		minMClust = a;
	}

	public static int getMinMClusp() {
		return minMClusp;
	}

	public static void setMinMClusp(int a) {
		minMClusp = a;
	}

	public static ArrayList<ArrayList<Integer>> getEquateTokens() {
		return equateTokens;
	}

	public static void setEquateTokens(ArrayList<ArrayList<Integer>> equateTokensother) {
		equateTokens = equateTokensother;
	}

	public void setSuppressedTokenID(int[] a) {
		for (int i = 0; i < a.length; i++) {
			SuppressedTokenID.add(a[i]);
		}

	}

	public int[] getSuppressedTokenID() {
		int[] a = new int[SuppressedTokenID.size()];
		for (int i = 0; i < SuppressedTokenID.size(); i++) {
			a[i] = SuppressedTokenID.get(i);
		}

		return a;
	}

	public Hashtable<Integer, String> getToken_details() {
		return token_details;
	}

	public void setToken_details(Hashtable<Integer, String> token_details) {
		this.token_details = token_details;
	}

	// MCC mcc;
	CloneDetectionSettingsWizardCloneSettingInput page1;
	CloneDetectionSettingsWizardSetThreshold page1A;
	CloneDetectionSettingsWizardSetTokensToSkip page21, page22, page24, page25, page26, page27;
	CloneDetectionSettingsWizardCreateRules page31, page32, page34, page35, page36, page37;
	CloneDetectionSettingsWizardPage4 page4;
	CloneDetectionSettingsWizardPage5 page5;

	public CloneDetectionSettingsWizard() {
		super();

	}

	public boolean get_isDelim() {
		return isDelim;
	}

	public int get_stc() {
		return stc;
	}

	public int get_groupIndex() {
		return groupIndex;
	}

	public void setDelim(boolean isDelim) {
		this.isDelim = isDelim;
	}

	public void setStc(int stc) {
		this.stc = stc;
	}

	public void setLangIndex(int langIndex) {
		languageChoice = langIndex;
	}

	public void setGroupIndex(int Index) {
		groupIndex = Index;
	}

	public void setFileList(Vector<String[]> fileList) {
	}

	public void setSavePage1(boolean savePage1) {
	}

	public void setSavePage1A(boolean savePage1) {
	}

	public void setSavePage2(boolean savePage2) {
	}

	@Override
	public void addPages() {
		page1 = new CloneDetectionSettingsWizardCloneSettingInput(this);
		page1A = new CloneDetectionSettingsWizardSetThreshold(0, this);
		page21 = new CloneDetectionSettingsWizardSetTokensToSkip(0, this);
		page22 = new CloneDetectionSettingsWizardSetTokensToSkip(1, this);
		page24 = new CloneDetectionSettingsWizardSetTokensToSkip(3, this);
		page25 = new CloneDetectionSettingsWizardSetTokensToSkip(4, this);
		page26 = new CloneDetectionSettingsWizardSetTokensToSkip(5, this);
		page27 = new CloneDetectionSettingsWizardSetTokensToSkip(6, this);

		page31 = new CloneDetectionSettingsWizardCreateRules(0, this);
		page32 = new CloneDetectionSettingsWizardCreateRules(1, this);
		page34 = new CloneDetectionSettingsWizardCreateRules(3, this);
		page35 = new CloneDetectionSettingsWizardCreateRules(4, this);
		page36 = new CloneDetectionSettingsWizardCreateRules(5, this);
		page37 = new CloneDetectionSettingsWizardCreateRules(6, this);

		page4 = new CloneDetectionSettingsWizardPage4(this);
		page5 = new CloneDetectionSettingsWizardPage5(this);

		addPage(page1);
		addPage(page1A);
		addPage(page21);
		addPage(page22);
		addPage(page24);
		addPage(page25);
		addPage(page26);
		addPage(page27);

		addPage(page31);
		addPage(page32);
		addPage(page34);
		addPage(page35);
		addPage(page36);
		addPage(page37);

		// addPage(page4);
		// addPage(page5);
	}

	/*
	 * public void refresh_page4() { page4 = new
	 * CloneDetectionSettingsWizardPage4(this); }
	 */
	public CloneDetectionSettingsWizardSetThreshold getPage1A() {
		return page1A;
	}

	public CloneDetectionSettingsWizardSetTokensToSkip getPage21() {
		return page21;
	}

	public CloneDetectionSettingsWizardSetTokensToSkip getPage22() {
		return page22;
	}

	public CloneDetectionSettingsWizardSetTokensToSkip getPage24() {
		return page24;
	}

	public CloneDetectionSettingsWizardSetTokensToSkip getPage25() {
		return page25;
	}

	public CloneDetectionSettingsWizardSetTokensToSkip getPage26() {
		return page26;
	}

	public CloneDetectionSettingsWizardSetTokensToSkip getPage27() {
		return page27;
	}

	public CloneDetectionSettingsWizardCreateRules getPage31() {
		return page31;
	}

	public CloneDetectionSettingsWizardCreateRules getPage32() {
		return page32;
	}

	public CloneDetectionSettingsWizardCreateRules getPage34() {
		return page34;
	}

	public CloneDetectionSettingsWizardCreateRules getPage35() {
		return page35;
	}

	public CloneDetectionSettingsWizardCreateRules getPage36() {
		return page36;
	}

	public CloneDetectionSettingsWizardCreateRules getPage37() {
		return page37;
	}

	public CloneDetectionSettingsWizardPage4 getPage4() {
		return page4;
	}

	public CloneDetectionSettingsWizardPage5 getPage5() {
		return page5;
	}

	@Override
	public boolean canFinish() {
		if (getContainer().getCurrentPage() == page31 || getContainer().getCurrentPage() == page32
				|| getContainer().getCurrentPage() == page34 || getContainer().getCurrentPage() == page35
				|| getContainer().getCurrentPage() == page36 || getContainer().getCurrentPage() == page37)
			/* if(getContainer().getCurrentPage()== page1) */
			return true;
		else
			return false;
		/*
		 * if (getContainer().getCurrentPage() == page1) { return true; } else { return
		 * false; }
		 */
	}

	public static void closeOpenedViews(IWorkbenchPage workbenchPage) {
		IViewPart mccView = workbenchPage.findView("mccView");
		IViewPart sccView = workbenchPage.findView("sccView");
		IViewPart fccView = workbenchPage.findView("fccView");
		IViewPart mcsView = workbenchPage.findView("mcsView");
		IViewPart fcsView = workbenchPage.findView("fcsView");
		if (mccView != null) {
			workbenchPage.hideView(mccView);
		}
		if (sccView != null) {
			workbenchPage.hideView(sccView);
		}
		if (fccView != null) {
			workbenchPage.hideView(fccView);
		}
		if (mcsView != null) {
			workbenchPage.hideView(mcsView);
		}
		if (fcsView != null) {
			workbenchPage.hideView(fcsView);
		}
	}

	@Override
	public boolean performFinish() {
		CloneDetectionSettingsWizardCreateRules.saveDataToWizard();
		SysConfig.setEquateTokens(equateTokens);

		for (int i = 0; i < SuppressedTokenID.size(); i++) {
			SysConfig.setSuppressTokenAt(SuppressedTokenID.get(i), true);
		}
		SysConfig.setGroupCheck(groupIndex);
		SysConfig.setLangChoice(languageChoice);
		SysConfig.setMinFClusP(getMinFClusp());
		SysConfig.setMinFClusT(getMinFClust());
		SysConfig.setMinMClusP(getMinMClusp());
		SysConfig.setMinMClusT(getMinMClust());
		// SysConfig.setThreshold(getThreshold());
		SysConfig.setProjectName(projectName);
		SysConfig.setProjectDiscrp(projectDiscrp);
		SysConfig.setThreshold(stc);
		SysConfig.setSystemTym();
		Job job = new Job("Clone Detection In Progress...") {

			@Override
			protected IStatus run(IProgressMonitor arg0) {

				initiateCloneDetection();
				syncWithUi();
				return Status.OK_STATUS;

			}
		};

		job.schedule();

		return true;
	}

	private void initiateCloneDetection() {
	//	CloneTrackingFormPage.isIncremental = false;
		CloneDetector.detectClones();
	} 

	private void syncWithUi() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {

				/*MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Status",
						"Clone Detection has finished.");*/
				NotificationPopUpUI popup = new  NotificationPopUpUI(/*NotificationQuartzJobHelper.getInstance().getCurrentDisp()*/PlatformUI.getWorkbench().getDisplay());
			    popup.open();
			}
		});

	}

	private void markAllMethodClones() {
		// for (int j = 0; j < SampleHandler.f.mccList.size(); j++)
		// {
		// markMethods(j);
		// }
	}

	// return true;

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		System.out.println("Preparing wizard..");
		this.setWindowTitle("Clone Detection Settings");
		this.setNeedsProgressMonitor(true);
		this.setHelpAvailable(false);
		this.setForcePreviousAndNextButtons(true);
	}

	@Override
	public void createPageControls(Composite pageContainer) {
	}

	@Override
	public boolean performCancel() {
		if (miner != null) {
			miner.destroy();
		}

		return super.performCancel();
	}

	public static boolean deleteFolder(File folder) {
		if (folder.isDirectory()) {
			String[] files = folder.list();
			for (int i = 0; i < files.length; i++) {
				boolean success = deleteFolder(new File(folder, files[i]));
				if (!success) {
					return false;
				}
			}
		}
		return folder.delete();

	}

	private void markMethods(int index) {
		/*
		 * MCC mcc = SampleHandler.f.mccList.get(index); for (MCCInstance instance :
		 * mcc.getMCCInstances()) { Method currentMethod = instance.getMethod(); File
		 * fileToOpen = new File(currentMethod.getFilePath()); String text =
		 * getFileContent(fileToOpen); IFile ifile = getIFile(fileToOpen); try { IMarker
		 * marker = ifile.createMarker("org.eclipse.core.resources.methodclonesmarker");
		 * int startChar = text.indexOf(currentMethod.getCodeSegment()); int offset =
		 * currentMethod.getCodeSegment().length();
		 * marker.setAttribute(IMarker.CHAR_START, startChar+2);
		 * marker.setAttribute(IMarker.CHAR_END, startChar+2 + offset);
		 * marker.setAttribute(IMarker.DONE, true); } catch (Exception e) {
		 * e.printStackTrace(); } }
		 */
	}

	private IFile getIFile(File fileToOpen) {
		IPath location = Path.fromOSString(fileToOpen.getAbsolutePath());
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		return workspace.getRoot().getFileForLocation(location);
	}

	private String getFileContent(File fileToOpen) {
		String content = "";
		try {
			FileInputStream fis = new FileInputStream(fileToOpen);
			byte[] data = new byte[(int) fileToOpen.length()];
			fis.read(data);
			fis.close();
			content = new String(data, "UTF-8");
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		return content;
	}

	public void setSavePage3(boolean b) {
		// TODO Auto-generated method stub

	}

}
