package cmt.cddc.clonedetectioninitializer;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map.Entry;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import cmt.common.Directories;


public class CloneDetectionSettingsWizardCreateRules extends WizardPage implements Listener{
	
	Display display;// = new Display();
	private static org.eclipse.swt.graphics.Color gray;
	private static org.eclipse.swt.graphics.Color white;
	
	
	private Button addTokenButton;
	private Button editRule;
	private Button addNewRule;
	private Button DeleteRule;
	private Button removeToken;
	TableItem item, tokenItem;
	
	
	
	
	//private List tokenList;
	private List selectedTokenListItem;
	
	private String lang;
	private String langTitle;
	
	private Table rulesTable, tokenTable;
	
	static Hashtable<Integer,String> tokenIds = new Hashtable<Integer,String>();
	HashSet<String> st = new HashSet<String>();
	HashSet<String> selectedTokenToEquate = new HashSet<String>();
	static ArrayList<ArrayList<String>> rules = new ArrayList<ArrayList<String>>();
	static ArrayList<ArrayList<Integer>> rulesId = new ArrayList<ArrayList<Integer>>();
	static ArrayList<String> selectedTokens = new ArrayList<String>();
	static ArrayList<String> SkipTokens = new ArrayList<String>();
	private void SetSkipToken(String token) {
		SkipTokens.add(token);
	}
	public void SkipToken() {
    	String[] token = {"Identifier", "(", ")", "throws", ";", "{", "}", "class", "interface", "=", "]"};
    	for(int i=0; i<token.length; i++) {
    		SetSkipToken(token[i]);
    		}
    	}
	
	
	
	Boolean NewRuleFlag = false;
	
	//private final Initializer init = CloneManagementPlugin.getInitializer();
	private static CloneDetectionSettingsWizard wizard;
	private CloneDetectionSettingsWizardPage4 page4;
	private ArrayList<Integer> surpressedToken ; 

	
	 public CloneDetectionSettingsWizardCreateRules(int langIndex, CloneDetectionSettingsWizard wizard)
	    {
		 
		
		 super("Equal Tokens Settings");
		 setTitle("Settings for Equal Tokens");
			setDescription("Select multiple tokens from the token list below to make "
				+ "them equivalent for Clone Detection (Example: \"token1 = token2, token3 = token4 = token5\").");
			if (langIndex == 0)
			{
			    lang = Directories.TOKEN_JAVA;
			    langTitle = "Java ";
			} else if (langIndex == 1)
			{
			    lang = Directories.TOKEN_CPP;
			    langTitle = "C++ ";
			} else if (langIndex == 3)
			{
			    lang = Directories.TOKEN_CSHARP;
			    langTitle = "CSHARP ";
			} else if (langIndex == 4)
			{
			    lang = Directories.TOKEN_RUBBY;
			    langTitle = "RUBY ";
			} else if (langIndex == 5)
			{
			    lang = Directories.TOKEN_PHP;
			    langTitle = "PHP ";
			} else if (langIndex == 6)
			{
			    lang = Directories.TOKEN_TXT;
			    langTitle = "TXT ";
			} else
			{
			    lang = null;
			    langTitle = "";
			}
		 
		 
		 
	    }
	 public CloneDetectionSettingsWizardCreateRules(String pageName, String title, ImageDescriptor titleImage)
	    {
		super(pageName, title, titleImage);
	    }

	 
	 
	 
	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		Composite container = new Composite(parent, SWT.NULL);
		display = container.getDisplay();
		gray = display.getSystemColor(SWT.COLOR_GRAY);
		white = display.getSystemColor(SWT.COLOR_BLACK);
		
		 
		SkipToken();
		addTokenButton = new Button(container, SWT.None);
		addTokenButton.setText(">>>");
		addTokenButton.setBounds(140, 113, 44, 25);
		addTokenButton.setEnabled(false);
		
		removeToken = new Button(container, SWT.None);
		removeToken.setText("<<<");
		removeToken.setBounds(140, 144, 44, 25);
		removeToken.setEnabled(false);
		
		addNewRule = new Button(container, SWT.None);
		addNewRule.setText("Add Rule");
		addNewRule.setBounds(214, 295, 75, 25);
		addNewRule.setEnabled(false);
		
		DeleteRule = new Button(container, SWT.None);
		DeleteRule.setText("Delete Rule");
		DeleteRule.setBounds(444, 295, 75, 25);
		DeleteRule.setEnabled(false);
		
		editRule = new Button(container, SWT.None);
		editRule.setText("Edit Rule");
		editRule.setBounds(363, 295, 75, 25);
		editRule.setEnabled(false);
		
		//tokenList = new List(container, SWT.BORDER | SWT.V_SCROLL | SWT.SINGLE);
		tokenTable = new Table(container, SWT.BORDER | SWT.V_SCROLL | SWT.SINGLE);
		//tokenList
		tokenTable.setBounds(0, 34, 135, 255);
		
		selectedTokenListItem = new List(container, SWT.BORDER | SWT.V_SCROLL | SWT.SINGLE);
		selectedTokenListItem.setBounds(195, 34, 118, 255);
		selectedTokenListItem.add("Add New Token.");
		
		Label lblTokensList = new Label(container, SWT.NONE);
		lblTokensList.setBounds(10, 10, 94, 18);
		lblTokensList.setText("Tokens List");
		
		Label lblSelectedTokens = new Label(container, SWT.NONE);
		lblSelectedTokens.setBounds(195, 10, 94, 18);
		lblSelectedTokens.setText("Selected Tokens");
		
		Label lblRulesList = new Label(container, SWT.NONE);
		lblRulesList.setBounds(380, 10, 184, 18);
		lblRulesList.setText("Rules List");
		
		rulesTable = new Table(container, SWT.BORDER | SWT.FULL_SELECTION | SWT.SINGLE);
		rulesTable.setBounds(324, 34, 236, 255);
		rulesTable.setHeaderVisible(false);
		
		rulesTable.setLinesVisible(true);

		
		
		selectedTokenListItem.addSelectionListener(new SelectionAdapter() {
			@Override
		    public void widgetSelected(SelectionEvent e) {
				if(selectedTokenListItem.getSelectionCount() > 0 ) {
					if(selectedTokenListItem.getSelectionCount() == 1) {
						addNewRule.setEnabled(false);
						DeleteRule.setEnabled(true);
						editRule.setEnabled(true);
						removeToken.setEnabled(true);
					}
					
					
				}
				
				else {
					addNewRule.setEnabled(false);
					DeleteRule.setEnabled(false);
					editRule.setEnabled(false);
				}
			}
		    
			
			
		});
		
		tokenTable.addSelectionListener(new SelectionAdapter() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {if (tokenTable.getSelectionCount() > 0)
			{
		    	TableItem[] selectedToken = tokenTable.getSelection();
		    	org.eclipse.swt.graphics.Color color = selectedToken[0].getForeground();
		    	if(color.equals(gray)) {
		    		addTokenButton.setEnabled(false);
		    		
		    	}
		    	else {
		    		addTokenButton.setEnabled(true);
		    	}
		    	
		    	
		    		
				    //editRule.setEnabled(true);
				    //editRule.setEnabled(true);
		    		
		    	}
		    	
			    
			else
			{
			    addTokenButton.setEnabled(false);
			    //editRule.setEnabled(false);
			    //editRule.setEnabled(false);
			}
		    }
		});
		
		HashSet<String> surpressedtokenIDList = getSurpressedTokens();
		ArrayList<Integer> STID = new ArrayList<Integer>();
		if(surpressedtokenIDList.size()!=0) {
			String IDS = surpressedtokenIDList.toString();
			IDS = IDS.substring(1, (IDS.length()-1));
			String [] ids = IDS.split(", ");
			
			for(int i =0; i<ids.length;i++) {
				int id = Integer.parseInt(ids[i]);
				STID.add(id); 
			}
		}
		
		
		try
		{
		    String filePath = Directories.getAbsolutePath(lang);
		    File file = new File(filePath);
		    FileInputStream filein = new FileInputStream(file);
		    BufferedReader stdin = new BufferedReader(new InputStreamReader(filein));
		    String line = "";
		   
		   
		 
		   while ((line = stdin.readLine()) != null)
		    {
		    	int last = line.lastIndexOf("=");
		    	
		    	int endIndex = last;
		    	int beginIndex = last +1;
		    	int length = line.length();
		    	String token = line.substring(0, endIndex);
		    	String tokenID = line.substring(beginIndex, length);
		    	int a= Integer.parseInt(tokenID);
		    	String IDS = "";
		    	if(STID.size()!=0) {
		    		for(int i =0; i<STID.size();i++) {
			    		if(!STID.contains(a)) {
			    			if(tokenIds.containsKey(Integer.parseInt(tokenID))) {
					    		if(token.contains("'")) {
						    		if(token.lastIndexOf("'")==(token.length()-1)) {
						    			IDS = token.substring(1, (token.length()-1));
						    			if(!SkipTokens.contains(IDS)) {
						    				tokenIds.put(Integer.parseInt(tokenID), token);
						    			}
						    			else {
						    				tokenIds.remove(Integer.parseInt(tokenID));
						    			}
						    			
						    			
						    			
						    		}
						    		
						    	}
					    		
					    	}
					    	else {
					    		if(!SkipTokens.contains(token)) {
				    				tokenIds.put(Integer.parseInt(tokenID), token);
				    			}
					    	}
			    		}
			    	}
		    	}
		    	else {
		    		
		    			if(tokenIds.containsKey(Integer.parseInt(tokenID))) {
				    		if(token.contains("'")) {
					    		if(token.lastIndexOf("'")==(token.length()-1)) {
					    			IDS = token.substring(1, (token.length()-1));
					    			if(!SkipTokens.contains(IDS)) {
					    				tokenIds.put(Integer.parseInt(tokenID), token);
					    			}
					    			else {
					    				tokenIds.remove(Integer.parseInt(tokenID));
					    			}
					    			
					    		}
					    		
					    	}
				    		
				    	}
				    	else {
				    		if(!SkipTokens.contains(token)) {
		    				tokenIds.put(Integer.parseInt(tokenID), token);
		    			}
				    	}
		    		
		    	}
		    	
		    		
		    	}
		   for(Entry<Integer, String> m:tokenIds.entrySet()) {
				// String a = tokenIds.get(i);
				 
					 selectedTokens.add(m.getValue());//(m.getValue());
					 
					 
				 }
		 
	
		   
		   
		    
		   stdin.close();
		} catch (Exception e)
		{
		    System.err.println(e.getMessage());
		    e.printStackTrace();
		}
		for(Entry<Integer, String> m:tokenIds.entrySet()) {
			tokenItem = new TableItem(tokenTable, SWT.NONE);
			tokenItem.setText(m.getValue());
			   
			   
		   }
		
	   
		
		addTokenButton.addSelectionListener(new SelectionAdapter() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
		    	
		    	TableItem[] selectedToken = tokenTable.getSelection();
		    	//int id = tokenList.getSelectionIndex(); 
		    	int index = tokenTable.getSelectionIndex();
		    	
		    	selectedToken[0].setForeground(gray);
		    	
		    	ArrayList<String> rule = new ArrayList<String>();
		    	
		    	if(selectedTokenListItem.getItem(0).equals("Add New Token.")) {
		    		selectedTokenListItem.remove(0);
		    		
		    	}
		    	
		    		if(selectedTokenListItem.getItemCount() == 0 ) {
		    			

		    			
		    			rule.add(selectedToken[0].getText());
			    		selectedTokenListItem.add(selectedToken[0].getText());
			    		selectedTokenToEquate.add(selectedToken[0].getText());
			    		addNewRule.setEnabled(true);
			    		
			    	}
		    		else {
		    			rule.add(selectedToken[0].getText());
		    			selectedTokenListItem.add(selectedToken[0].getText());
			    		selectedTokenToEquate.add(selectedToken[0].getText());
		    			
		    			
		    			
		    		}
		    		addTokenButton.setEnabled(false);
		
		    		
		    	
		    	
		    	
		    	
		    }
		});
		
		addNewRule.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int count = selectedTokenListItem.getItemCount();
				String[] content = {""}; 
				if(count > 0) {
					content = selectedTokenListItem.getItems();
					
					
				}
				item = new TableItem(rulesTable, SWT.None);
				String rule = ""; 
				ArrayList<String> ruleData = new ArrayList<String>();
				for(int i =0;i<count;i++) {
					
					if(i == 0) {
						rule += content[i];
						ruleData.add(content[i]);
					}
					else {
						rule += " = "+content[i];
						ruleData.add(content[i]);
					}
					
					
						
					
				}
				
				int size = rules.size();
				 if(rulesTable.getItemCount() == 1 ) {
					 if(rules.size()!=0) {
						 rules.clear();
						 rules.add(0, ruleData);
					 }
					 else {
						 rules.add(0, ruleData);
					 }
					 
				 }
				 else {
					 rules.add(size, ruleData);
				 }
				
				
				ArrayList<Integer> ruleTokenIDS = new ArrayList<Integer>();
				for (int i =0;i<ruleData.size();i++) {
					for(Entry<Integer, String> m:tokenIds.entrySet()) {
			    		if(m.getValue().equals(ruleData.get(i))) {
			    			ruleTokenIDS.add(m.getKey());
			    			break;
			    			
			    		}
				}
				}
				size = rulesId.size();
				rulesId.add(size, ruleTokenIDS);
				
				item.setText(rule);
				selectedTokenListItem.removeAll();
				selectedTokenListItem.add("Add New Token.");
				addNewRule.setEnabled(false);
				
				
			}
			
		});
		rulesTable.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				addNewRule.setEnabled(false);
				DeleteRule.setEnabled(true);
				editRule.setEnabled(true);
				
				
				
				
				
			}
		});

			
			
		

		
		
		editRule.addSelectionListener(new SelectionAdapter() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
		    	int index = rulesTable.getSelectionIndex();
		    	ArrayList<String> ruleContent = rules.get(index);
		    	int size = ruleContent.size();
		    	selectedTokenListItem.removeAll();
		    	for(int i=0;i<size;i++) {
		    		selectedTokenListItem.add(ruleContent.get(i));
		    	}
		    	rules.remove(index);
		    	rulesTable.remove(index);
		    	editRule.setEnabled(false);
		    	DeleteRule.setEnabled(false);
		    	addNewRule.setEnabled(true);
		    	
		    	TableItem[] st = tokenTable.getSelection();
		    	int count = selectedTokenListItem.getItemCount();
		    	String token="";
		    	for (int i = 0;i<count; i++) {
		    		String line = selectedTokenListItem.getItem(i);
		    		if(line.equals(st[0].getText())) {
		    			int a = line.indexOf(st[0].getText());
		    			if(a==0&&(line.length()!= st[0].getText().length())) {
		    				token = line.replace(st[0]+"=", "");
				    		selectedTokenListItem.remove(i);
				    		selectedTokenToEquate.remove(st[0]);
				    		selectedTokenListItem.add(token, i);
				    		//rules.remove(st[0]);
				    		ArrayList<String> ruleToken = rules.get(i);
							for(int j =0; j<ruleToken.size(); j++) {
								if(ruleToken.equals(st[0])) {
									ruleToken.remove(st[0]);
									rules.add(i, ruleToken);
								}
								
							}
		    				
		    			}
		    			else if(line.length()== st[0].getText().length()) {
		    				token = line.replace(st[0].getText(), "");
		    				if(token == null) {
		    					selectedTokenListItem.remove(i);
					    		selectedTokenToEquate.remove(st[0]);
					    		selectedTokenListItem.add("New Rule Will be Added Here", i);
					    		//rules.remove(st[0]);
					    		ArrayList<String> ruleToken = rules.get(i);
								for(int j =0; j<ruleToken.size(); j++) {
									if(ruleToken.equals(st[0])) {
										ruleToken.remove(st[0]);
										rules.add(i, ruleToken);
									}
									
								}
		    				}
		    				else {
		    					
		    					
		    				}
				    		
				    		
				    		/*ArrayList<String> ruleToken = rules.get(i);
							for(int j =0; j<ruleToken.size(); j++) {
								if(ruleToken.equals(st[0])) {
									ruleToken.remove(st[0]);
									rules.add(i, ruleToken);
								}
								
							}*/
		    			}
		    			else {
		    				token = line.replace("="+st[0], "");
				    		selectedTokenListItem.remove(i);
				    		selectedTokenToEquate.remove(st[0]);
				    		selectedTokenListItem.add(token, i);
				    		//rules.remove(st[0]);
				    		ArrayList<String> ruleToken = rules.get(i);
							for(int j =0; j<ruleToken.size(); j++) {
								if(ruleToken.equals(st[0])) {
									ruleToken.remove(st[0]);
									rules.add(i, ruleToken);
								}
								
							}
		    			}
			    		
			    		
			    	}
		    		
			    	
		    		
		    	}
		    	
		    	
		    	
		    	
		    	
		    }
		});
		
		DeleteRule.addSelectionListener(new SelectionAdapter() {
			@Override
		    public void widgetSelected(SelectionEvent e)
		    {
				int index = rulesTable.getSelectionIndex();
				
		    	ArrayList<String> ruleContent = rules.get(index);
		    	int size = ruleContent.size();
		    	selectedTokenListItem.removeAll();
		    	for(int i=0;i<size;i++) {
		    		for(int j =0; j<selectedTokens.size();j++) {
		    			if(ruleContent.get(i).equals(selectedTokens.get(j))) {
		    				TableItem item = tokenTable.getItem(j);
		    				item.setForeground(white);
		    				
		    				break;
		    				
		    			}
		    		}
		    		
		    	}
		    	rules.remove(index);
		    	rulesTable.remove(index);
		    	if(rulesTable.getItemCount()==0) {
		    		addNewRule.setEnabled(false);
					 editRule.setEnabled(false);
					 DeleteRule.setEnabled(false);
					 addTokenButton.setEnabled(false);
					 removeToken.setEnabled(false);
		    	}
		    	selectedTokenListItem.add("Add New Token.");
				
					
				
		    }
			
		});
		removeToken.addSelectionListener(new SelectionAdapter() {
			
			 public void widgetSelected(SelectionEvent e)
			    {
				 String token = selectedTokenListItem.getItem(selectedTokenListItem.getSelectionIndex());
				 if(selectedTokens.contains(token)){
					 for(int i =0; i<=selectedTokens.size();i++) {
						// String a = tokenIds.get(i);
						 if(token.equals(selectedTokens.get(i))) {
							 TableItem item = tokenTable.getItem(i);
							 item.setForeground(white);
							 break;
						 }
						 
					 }
					 
				 }
				
					 
				 
				 
				 selectedTokenListItem.remove(selectedTokenListItem.getSelectionIndex());
				 if(selectedTokenListItem.getItemCount() == 0) {
					 selectedTokenListItem.add("Add New Token.");
					 addNewRule.setEnabled(false);
					 editRule.setEnabled(false);
					 DeleteRule.setEnabled(false);
					 addTokenButton.setEnabled(false);
					 removeToken.setEnabled(false);
				 }
				 else {
					 addNewRule.setEnabled(true);
					 addTokenButton.setEnabled(true);
					 removeToken.setEnabled(true);
				 }
			    }
			
		});
		
		
		setControl(container);
		
	}
		public IWizardPage getNextPage() { 
  	  saveDataToWizard();
  	  setPageComplete(false);
  	  page4 = null; 
  	  System.out.println("About to run clone miner!!");
  	  //init.ExCloneMiner(getContainer(), wizard.get_stc(), wizard.get_isDelim(), wizard.get_groupIndex());
  	  System.out.println("Clone miner finished running!"); 
  	  return page4; 
  	  }
	@Override
    public boolean canFlipToNextPage()
    {
    	//return true;
	return false;
    }
	public static void saveDataToWizard()
    {
	try
	{

	   /* String equateTokenFilePath = Directories.getFilePath(Directories.CLONES_INPUT_EQUALTOKEN);

	    File file = new File(equateTokenFilePath);
	    file.createNewFile();
	    FileOutputStream fileout = new FileOutputStream(file);
	    PrintWriter stdout = new PrintWriter(fileout);
	    if (tokenSelected != null && tokenSelected.getText() != null)
	    {
		stdout.print(tokenSelected.getText());
	    }
	    stdout.flush();
	    fileout.close();
	    stdout.close();*/
	    int size = rules.size();
	    ArrayList<ArrayList<Integer>> ruleID = new ArrayList<ArrayList<Integer>>();
	    for(int i =0; i<size; i++) {
		    ArrayList <Integer> tokenId= new ArrayList<Integer>();
	    	String rule = rules.get(i).toString();
	    	rule = rule.substring(1, rule.length()-1);
	    	String [] tokens = rule.split(", ");
	    	for(int j =0; j<tokens.length; j++) {
	    		
	    		for(Entry<Integer, String> m:tokenIds.entrySet()) {
		    		if(m.getValue().equals(tokens[j])) {
		    			tokenId.add(m.getKey());
		    			break;
		    		}
		    	
		    	}
	    		
	    	}
	    	ruleID.add(tokenId);
	    	
	    	}
	    CloneDetectionSettingsWizard.setEquateTokens(ruleID);
	    //ruleID.clear();
	    rules.clear();
	    
	    
	    
	    }
	
	catch (Exception ex)
	{
	    System.err.println(ex.getMessage());
	    ex.printStackTrace();
	}

	// change
	/*
	 * System.out.println("About to run clone miner!!");
	 * init.ExCloneMiner(getContainer(), wizard.get_stc(), wizard.get_isDelim(),
	 * wizard.get_groupIndex());
	 * System.out.println("Clone miner finished running!"); IWorkbenchPage
	 * workbenchPage =
	 * PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(); try {
	 * closeOpenedViews(workbenchPage); workbenchPage.showView("sccView");
	 * workbenchPage.showView("mccView"); } catch (PartInitException e) {
	 * e.printStackTrace(); }
	 */
	//
    }
	private HashSet<String>  getSurpressedTokens() {
		
		HashSet<String> surpressedTokenIds = new HashSet<String>();
		
			try {
			String supressedTokenInputFile = Directories.getAbsolutePath(Directories.CLONES_INPUT_SUPPRESSED);
			 

		    File file = new File(supressedTokenInputFile);
		    FileInputStream filein = new FileInputStream(file);
		    BufferedReader stdin = new BufferedReader(new InputStreamReader(filein));
		    String line = stdin.readLine();
		    if(line!=null) {
		    	int a = line.length();
			    String[] surpressedTokensInput = line.split(",");
			    
			    for(int i = 0 ; i < surpressedTokensInput.length ; i++) {
			    	
			    	surpressedTokenIds.add(surpressedTokensInput[i]);
			    }
		    }
		    
		    
		   
		    
				
			
			}
			catch(Exception exp) {
				System.out.println(exp);
				
			}
			return surpressedTokenIds;
			
			
		}
	@Override
	public void handleEvent(Event e) {
		
		
	}
	
	
	

}
