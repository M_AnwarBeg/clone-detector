package cmt.cddc.clonedetectioninitializer;

import java.awt.Color;
//import org.eclipse.swt.graphics.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.ui.packageview.PackageFragmentRootContainer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.internal.Workbench;
import org.eclipse.wb.swt.SWTResourceManager;

import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizard;
import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizardSetTokensToSkip;
import cmt.common.Directories;

public class CloneDetectionSettingsWizardCloneSettingInput extends WizardPage implements Listener {
	protected static final Shell Shell = null;
	private CloneDetectionSettingsWizard wizard;
    private CloneDetectionSettingsWizardSetThreshold page1A;
    private Combo languageChoice;
    private List selectionList;
    HashSet <String> list;
    private IStatus selectionListStatus;
    private IStatus nameStatus;
    
    private Button removeButton;
    private Button addDirButton;
    private Button addFileButton;
    private Button saveList;
    private Button loadList;
    private Button preGroupButton;
    private Button nextGroupButton;
    private Button mixedMode;
    private Button accrossGroup;
    
    
    private Text projectName;
    private Text projectDiscrp;
    private int langIndex = 0;
    private int groupIndex = 0;
    private int currentGroup = 0;
    private Vector<String[]> fileGroups;
    private ArrayList <String> filesList ; 
    //private final Initializer init = CloneManagementPlugin.getInitializer();
    private String[] javaLang = { "java" };

	public CloneDetectionSettingsWizardCloneSettingInput(CloneDetectionSettingsWizard wizard) {
		// TODO Auto-generated constructor stub
		super("Add Files");
		
		setTitle("Settings for Input Files");
		setDescription("Add Files or Projects For Clone Identification\n"
			+ "(applicable to Java Language only);\nAdd input source files of " + "your selected language.");
		this.wizard = wizard;
		nameStatus = new Status(IStatus.ERROR, "error_name", 0, "Enter correct name for project.", null);
		//selectionListStatus = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
		
		filesList = new ArrayList<String>();
		fileGroups = new Vector<String[]>();
		list = new HashSet<String>();
	}
	public Boolean chkFile(String a) {
		try {
			
		}
		catch(Exception ex) {
			System.out.println(ex);
		}
		return true;
	}

	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		Composite container = new Composite(parent, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		
		
		//container.setLayout(new FormLayout());
		Label lblClo = new Label(container, SWT.NONE);
		lblClo.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		lblClo.setForeground(SWTResourceManager.getColor(0, 0, 0));
		//lblClo.setForeground(SWTResourceManager.getCol);
		lblClo.setBounds(10, 11, 157, 17);
		lblClo.setText("Clone Detection Run Name");
		
		Label lblNewLabel0 = new Label(container, SWT.NONE);
		lblNewLabel0.setFont(SWTResourceManager.getFont("Segoe UI", 7, SWT.NORMAL));
		lblNewLabel0.setBounds(170, 13, 14, 15);
		lblNewLabel0.setForeground(SWTResourceManager.getColor(255, 0, 0));
		lblNewLabel0.setText("*");
		
		Label lblCloneDetectionDiscription = new Label(container, SWT.NONE);
		lblCloneDetectionDiscription.setText("Clone Detection Discription");
		lblCloneDetectionDiscription.setForeground(SWTResourceManager.getColor(0, 0, 0));
		lblCloneDetectionDiscription.setFont(SWTResourceManager.getFont("Segoe UI", 10, SWT.NORMAL));
		lblCloneDetectionDiscription.setBounds(10, 38, 157, 17);
		
		projectName = new Text(container, SWT.BORDER);
		projectName.setBounds(186, 10, 354, 21);
		projectName.addListener(SWT.Modify, this);
		
		projectDiscrp = new Text(container, SWT.BORDER |SWT.WRAP);
		projectDiscrp.setBounds(186, 37, 354, 63);
		projectDiscrp.setTextLimit(150);
		
		addDirButton = new Button(container, SWT.NONE);
		addDirButton.setBounds(447, 161, 93, 25);
		addDirButton.setText("Add Dir");
		addDirButton.addListener(SWT.Selection, this);
		
		removeButton = new Button(container, SWT.NONE);
		removeButton.setBounds(447, 223, 93, 25);
		removeButton.setText("Remove File");
		removeButton.addListener(SWT.Selection, this);
		
		saveList = new Button(container, SWT.NONE);
		saveList.setBounds(447, 254, 93, 25);
		saveList.setText("Export List");
		saveList.addListener(SWT.Selection,this);
		
		addFileButton = new Button(container, SWT.NONE);
		addFileButton.setBounds(447, 192, 93, 25);
		addFileButton.setText("Add File");
		addFileButton.addListener(SWT.Selection, this);
		
		loadList = new Button(container, SWT.NONE);
		loadList.setBounds(447, 285, 93, 25);
		loadList.setText("Import List");
		loadList.addListener(SWT.Selection, this);
		
		
		
		preGroupButton = new Button(container, SWT.NONE);
		preGroupButton.setBounds(239, 347, 98, 25);
		preGroupButton.setText("Previous Group");
		preGroupButton.addListener(SWT.Selection, this);
		
		nextGroupButton = new Button(container, SWT.NONE);
		nextGroupButton.setBounds(343, 347, 98, 25);
		nextGroupButton.setText("Next Group");
		nextGroupButton.addListener(SWT.Selection, this);
		
		Label lblLnguageChoice = new Label(container, SWT.NONE);
		lblLnguageChoice.setBounds(10, 114, 157, 15);
		lblLnguageChoice.setText("Language Choice");
		
		
		selectionList = new List(container, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL |SWT.MULTI);
		selectionList.setBounds(10, 161, 431, 180);
		selectionList.addListener(SWT.Selection, this);
		
		
		languageChoice = new Combo(container, SWT.NONE);
		languageChoice.setItems(new String[] {"Java", "C++", "VB.NET", "C#", "Ruby", "PHP", "Plain Text"});
		languageChoice.setBounds(186, 106, 354, 23);
		languageChoice.select(0);
		languageChoice.addListener(SWT.Selection, this);
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setBounds(10, 138, 157, 15);
		lblNewLabel.setText("Grouping Choice");
		
		Group group1 = new Group(container, SWT.NONE);
		new Button(group1, SWT.RADIO).setText("Mixed Mode");
		new Button(group1, SWT.RADIO).setText("Accross Group");
		mixedMode = new Button(container, SWT.RADIO);
		mixedMode.setBounds(186, 137, 90, 16);
		mixedMode.setText("Mixed Mode");
		mixedMode.addListener(SWT.Selection, this);
		
		accrossGroup = new Button(container, SWT.RADIO);
		accrossGroup.setBounds(283, 137, 114, 16);
		accrossGroup.setText("Accross Group");
		accrossGroup.addListener(SWT.Selection, this);
		
		setControl(container);
	}
	public static boolean testRegularExpression(String string) {
		//String string = "theinput";
	    String regularExpression = "^[0-9a-zA-Z$]+$";
	    Pattern pattern = Pattern.compile(regularExpression);
	    Matcher matcher = pattern.matcher(string);
	   if(matcher.matches()) {
		   System.out.println("Valide Run NAME");
		   return true;
	   }
	   else {
		   System.out.println("Invalid Run NAME");
		   return false;
	   }
	}
	

	@Override
	public void handleEvent(Event e) {
		// TODO Auto-generated method stub
		Status status = new Status(IStatus.OK, "not_used", 0, "", null);

		
		if (e.widget == selectionList)
		{
		    if (selectionList.getSelectionCount() > 0)
		    {
			removeButton.setEnabled(true);
			nextGroupButton.setEnabled(true);
		    } else
		    {
			removeButton.setEnabled(false);
			nextGroupButton.setEnabled(false);
			status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
			setPageComplete(false);
		    }

		   
		}
		else if(e.widget == projectDiscrp) {
			if( projectName.getText()=="") {
				status = new Status(IStatus.ERROR, "not_used", 0, "PLEASE ENTER THE NAME FOR THE PROJECT", null);
				setPageComplete(false);
				selectionListStatus = status;
				//nameStatus = status;
				applyErrorMessage();
			}
		}
		
		else if(e.widget == projectName) {
			String name = projectName.getText();
			if(name=="") {
				status = new Status(IStatus.ERROR, "not_used", 0, "PLEASE ENTER THE NAME FOR THE PROJECT", null);
				setPageComplete(false);
			}
			else {
				status = new Status(IStatus.OK, "not_used", 0, "", null);
			}
			boolean test =  testRegularExpression(name);
			if(test) {
				status = new Status(IStatus.OK, "not_used", 0, "", null);
				
			}
			else {
				status = new Status(IStatus.ERROR, "not_used", 0, "PLEASE ENTER THE VALID NAME FOR THE PROJECT", null);
				setPageComplete(false);
			}
			selectionListStatus = status;
			//nameStatus = status;
			applyErrorMessage();
			
		}
		else if (e.widget == nextGroupButton)
		{
		    if (selectionList.getItemCount() > 0)
		    {
			String[] fileList = selectionList.getItems();
			if (currentGroup < fileGroups.size())
			{
			    fileGroups.set(currentGroup, fileList);
			} else
			{
			    fileGroups.add(currentGroup, fileList);
			}
			preGroupButton.setEnabled(true);
			currentGroup++;
			if (currentGroup < fileGroups.size())
			{
			    String[] nextFileList = fileGroups.get(currentGroup);
			    selectionList.setItems(nextFileList);
			    for(int i=0;i<nextFileList.length;i++) {
			    	list.add(nextFileList[i]);
			    }
			} else
			{
			    selectionList.removeAll();
			    list.clear();
			}

			if (selectionList.getItemCount() > 0)
			{
			    nextGroupButton.setEnabled(true);
			} else
			{
			    nextGroupButton.setEnabled(false);
			}
		    }
		    else {
		    	status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
		    }
		}
		else if (e.widget == preGroupButton)
		{
		    if (selectionList.getItemCount() > 0)
		    {
			String[] fileList = selectionList.getItems();
			if (currentGroup < fileGroups.size())
			{
			    fileGroups.set(currentGroup, fileList);
			} else
			{
			    fileGroups.add(currentGroup, fileList);
			}
		    }
		    else {
		    	status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
		    }
		    nextGroupButton.setEnabled(true);
		    currentGroup--;
		    String[] preFileList = fileGroups.get(currentGroup);
		    selectionList.setItems(preFileList);
		    for(int i=0;i<preFileList.length;i++) {
		    	list.add(preFileList[i]);
		    }

		    if (currentGroup > 0)
		    {
			preGroupButton.setEnabled(true);
		    } else
		    {
			preGroupButton.setEnabled(false);
		    }
		}
		else if (e.widget == languageChoice)
		{
		    if (languageChoice.getSelectionIndex() != langIndex)
		    {
			selectionList.removeAll();
			list.clear();
		    }
		    langIndex = languageChoice.getSelectionIndex();

		    /*
		     * if(languageChoice.getSelectionIndex() != 0){
		     * methodAnalysisButton.setEnabled(false); } else{
		     */
		    //methodAnalysisButton.setEnabled(true);

		    // }
		} 
		
		else if (e.widget == removeButton)
		{
		    if (selectionList.getSelectionCount() > 0)
		    	
		    {
		    	/*if(selectionList.getSelectionIndices().length==fileGroups.get(currentGroup).length) {
		    		fileGroups.remove(currentGroup);
			    	selectionList.remove(selectionList.getSelectionIndices());
		    	}
		    	else*/ {
		    		
		    		int[]  a = selectionList.getSelectionIndices();
		    		for(int i = 0;i<a.length;i++) {
		    			String b = selectionList.getItem(a[i]);
		    			list.remove(b);
		    			filesList.remove(selectionList.getItem(a[i]));
		    		}
		    		
		    		selectionList.remove(selectionList.getSelectionIndices());
		    		fileGroups.remove(currentGroup);
		    		String [] items = selectionList.getItems();
		    		fileGroups.add(currentGroup, items);
		    	}
		    	
		    }
		    else {
		    	status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
		    }
		    if(selectionList.getItemCount()==0 && fileGroups.size()>0) {
		    	String[] a = fileGroups.get(currentGroup);
		    	for(int i =0;i<a.length;i++) {
		    		 
		    		selectionList.add(a[i]);
		    	}
		    	
		    }

		    if (selectionList.getItemCount() > 0)
		    {
		    	//String a 
			nextGroupButton.setEnabled(true);
		    } else
		    {
			nextGroupButton.setEnabled(false);
		    }
		} 
		else if (e.widget == addFileButton)
		{
		    FileDialog fileDialog = new FileDialog(getShell(), SWT.MULTI);
		    fileDialog.setText("Open");
		    fileDialog.setFilterPath("");
		    String[] javaLang = { "*.java" };
		    String[] cppLang = { "*.c", "*.cpp", "*.cxx", "*.h", "*.hh", "*.hpp", "*.hxx" };
		    String[] vbnetLang = { "*.vb" };
		    String[] csharpLang = { "*.cs" };
		    String[] rubbyLang = { "*.rb" };
		    String[] phpLang = { "*.php" };
		    String[] plainTextLang = { "*.txt" };
		    String[] lang = { "empty" };

		    switch (languageChoice.getSelectionIndex()) {
		    case 0:
			lang = javaLang;
			break;
		    case 1:
			lang = cppLang;
			break;
		    case 2:
			lang = vbnetLang;
			break;
		    case 3:
			lang = csharpLang;
			break;
		    case 4:
			lang = rubbyLang;
			break;
		    case 5:
			lang = phpLang;
			break;
		    case 6:
			lang = plainTextLang;
			break;
		    }

		    fileDialog.setFilterExtensions(lang);
		    String a = fileDialog.open();
		    if(!list.contains(a)) {
		    	list.add(a);
		    	selectionList.add(a);
		    }
		    
		    
		    
		    if (selectionList.getItemCount() > 0)
		    {
			nextGroupButton.setEnabled(true);
		    } else
		    {
			nextGroupButton.setEnabled(false);
		    }
		} 
		else if (e.widget == addDirButton)
		{
		    DirectoryDialog directoryDialog = new DirectoryDialog(getShell());
		    String[] javaLang = { "java" };
		    String[] cppLang = { "c", "cpp", "cxx", "h", "hh", "hpp", "hxx" };
		    String[] pythonLang = { "py","ipy","ipyw","pyd","pyc" };
		    String[] csharpLang = { "cs" };
		    String[] rubbyLang = { "rb" };
		    String[] phpLang = { "php" };
		    String[] plainTextLang = { "ipy", "py" };
		    String[] lang = { "empty" };
		    switch (languageChoice.getSelectionIndex()) {
		    case 0:
			lang = javaLang;
			break;
		    case 1:
			lang = cppLang;
			break;
		    case 2:
			lang = csharpLang;
			break;
		    case 3:
			lang = csharpLang;
			break;
		    case 4:
			lang = rubbyLang;
			break;
		    case 5:
			lang = phpLang;
			break;
		    case 6:
			lang = pythonLang;
			break;
		    }
		    directoryDialog.setMessage("Please select a directory and click OK");
		    directoryDialog.setFilterPath("");
		    try
		    {
		    	String temp = directoryDialog.open();
				File dir = new File(temp);
				setSelectionList(dir, lang);
		    } catch (Exception exp)
		    {
		    }
		    if (selectionList.getItemCount() > 0)
		    {
			nextGroupButton.setEnabled(true);
		    } else
		    {
			nextGroupButton.setEnabled(false);
		    }
		}
		else if(e.widget == loadList) {
			String[] filter = {"*.txt"};
			ArrayList<String> fileContent = new ArrayList<String>() ;
			FileDialog fileDialog = new FileDialog(getShell(), SWT.OPEN);
			fileDialog.setFilterExtensions(filter);
		    fileDialog.setText("Open");
		    fileDialog.setFilterPath("");
		    fileDialog.open();
		    String name = fileDialog.getFileName();
		    if(name.toLowerCase().endsWith(".txt")) {
		    	String path = fileDialog.getFilterPath()+"\\"+name;
		    	File file = new File(path);
		    	/*1*/
		    	if(file.isFile()) {
		    		try {
		    			FileReader reader = new FileReader(file);
			    		BufferedReader br = new BufferedReader(reader);
			    		
			    		int j = 0;
			    		fileGroups.removeAllElements();
			    		String a= "";
			    		currentGroup =0; 
			    		while ((a=br.readLine()) != null) {
			    			 //= br.readLine();
			    			System.out.println(a);
			    			if(a.contains(";")) {
			    				//System.out.println(a);
			    				a=a.substring(0, a.length()-1);
			    				File ChkFile = new File(a);
			    				if(ChkFile.isFile()){
			    					fileContent.add(a);
				    				String[] arr = new String[fileContent.size()];
				    				for(int i =0 ; i<fileContent.size();i++) {
				    					 arr[i] = fileContent.get(i);
				    					 selectionList.add(fileContent.get(i));
				    				}
				    				
					    			fileGroups.add(j, arr);
					    			
					    			j++;
					    			fileContent.clear();
			    					
			    				}
			    				
			    				
			    			}
			    			else {
			    				fileContent.add(a);
			    				
				    			
			    			}	
			    		}
			    		if(j==0) {
			    			String[] arr = new String[fileContent.size()];
			    			for(int i =0 ; i<fileContent.size();i++) {
	    						selectionList.add(fileContent.get(i));
	    						arr[i] = fileContent.get(i);
	    				}
	    					
	    				fileGroups.add(j, arr);
	    				
	    				
		    			
		    			j++;
		    			fileContent.clear();
			    			
			    		}
			    		else {
			    			String[] arr = new String[fileContent.size()];
			    			for(int i =0 ; i<fileContent.size();i++) {
	    						//selectionList.add(fileContent.get(i));
	    						arr[i] = fileContent.get(i);
	    				}
	    					
	    				fileGroups.add(j, arr);
	    				
	    				
		    			
		    			j++;
		    			fileContent.clear();
			    		}
			    			
			    		
		    		}catch(Exception ex) {
		    			System.out.println(ex);
		    			
		    		}
		    		
		    		
		    	}
		    }
	 		
		}
		else if(e.widget == mixedMode) {
			if(mixedMode.getSelection()) {
				groupIndex =0;
			}
			
		           
		    	
			//}
			//System.out.pr
		}
		else if(e.widget == accrossGroup ) {
			if(accrossGroup.getSelection()) {
				groupIndex = 1;
 			}/*
			System.out.print(mixedMode.getText());
            System.out.println(" selected = " + mixedMode.getSelection());
			*/
		}
		else if(e.widget == saveList) {
			
			String[] filter = {"*.txt"};
			

			  
			FileDialog fileDialog = new FileDialog(getShell(), SWT.SAVE);
			
			//groupIndex = groupChoice.getSelectionIndex();
			groupIndex = 0;
			if (groupIndex == 1)
			{
			    groupIndex++;
			}
			if (selectionList.getItemCount() > 0)
			{
			    String[] fileList = selectionList.getItems();
			    if (currentGroup < fileGroups.size())
			    {
				fileGroups.set(currentGroup, fileList);
			    } else
			    {
				fileGroups.add(currentGroup, fileList);
			    }
			}
			else {
				status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
			}
			fileDialog.setFilterExtensions(filter);
		    fileDialog.setText("Save");
		    fileDialog.setFilterPath("");
		    fileDialog.open();
		    
 		    String name = fileDialog.getFileName();
 		    String path = fileDialog.getFilterPath()+"\\"+name;
		    File file = new File(path);
 		    if(!file.exists()) {
 		    	try {
 		    		file.createNewFile();
 		    		FileOutputStream fileout = new FileOutputStream(file);
 		    		PrintWriter stdout = new PrintWriter(fileout);

 		   	    for (int i = 0; i < fileGroups.size(); i++)
 		   	    {
 		   		for (int j = 0; j < fileGroups.get(i).length; j++)
 		   		{
 		   		    if (j == fileGroups.get(i).length - 1 && i != fileGroups.size() - 1)
 		   		    {
 		   			stdout.println(fileGroups.get(i)[j] + ";");
 		   		    } else
 		   		    {
 		   			stdout.println(fileGroups.get(i)[j]);
 		   		    }
 		   		}
 		   	    }
 		   	    stdout.flush();
 		   	    stdout.close();
 		   	    fileout.close();
 		    		
 		    	}catch(Exception ex) {
 		    		
 		    	}
 		    	
 		    }
		    
			}
		else
		{
		}
		
		/*if (selectionList.getItemCount() <= 0 && projectName.getText().equals("")) {
			status = new Status(IStatus.ERROR, "not_used", 0, "Project name Cannot be Empty and Input File List Cannot Be Empty", null);
		}
		else {
			if (selectionList.getItemCount() <= 0){
				
				status = new Status(IStatus.ERROR, "not_used", 0, "Input File List Cannot Be Empty", null);
			
			
		    
		    
		}
		if(projectName.getText().equals("")) {
				status = new Status(IStatus.ERROR, "not_used", 0, "Project name Cannot be Empty", null);
			
			
		}
		//else 
			if(!testRegularExpression(projectName.getText())){
			status = new Status(IStatus.ERROR, "not_used", 0, "Please Enter a Valid Name", null);
			setPageComplete(false);
		}
		}*/
		
		
		selectionListStatus = status;
		//nameStatus = status;
		applyErrorMessage();
		wizard.setSavePage1(false);
		getWizard().getContainer().updateButtons();
	}
	public void setSelectionList(File dir, String[] lang)
    {
	File[] files = dir.listFiles();
	for (int i = 0; i < files.length; i++)
	{
	    File file = files[i];
	    String filename = file.getName();
	    if (file.isFile())
	    {
			String ext = getExtension(filename);
			boolean ok = false;
			for (int j = 0; j < lang.length; j++)
			{
			    if (ext.equals(lang[j]))
			    {
				ok = true;
				break;
			    }
			}
			if (ok)
			{
				String a = file.getAbsolutePath();
				 if(!filesList.contains(a)) {
					 list.add(a);
					 filesList.add(a);
					 selectionList.add(a);
				 }
			    /*selectionList.add(a);*/
			}
	    }
	    else
	    {
	    	setSelectionList(file, lang);
	    }
	}
    }
    /**
     * helper function for getting the file extension part to decide file type
     */
    public String getExtension(String filename)
    {
	String ext;
	int dotPlace = filename.lastIndexOf('.');
	ext = filename.substring(dotPlace + 1);
	return ext;
    }
    public void applyErrorMessage()
    {
	boolean methodStatusOK = true;
	//
	/*if (methodAnalysisButton.getSelection() && (!mtcStatus.isOK() || !mpcStatus.isOK()))
	{
	    methodStatusOK = false;
	}*/

	if (/*sccStatus.isOK() && ftcStatus.isOK() && fpcStatus.isOK() && methodStatusOK &&*/ selectionListStatus.isOK())
	{
	    setErrorMessage(null);
	} else
	{
	    String msg = "";
	    
	    if(!selectionListStatus.isOK())
	    { 
	    	msg += selectionListStatus.getMessage() +"\n"; 
	    }
	    
	  
	    setErrorMessage(msg);
	}
    }
    @Override
    public boolean isPageComplete()
    {
	if (!selectionListStatus.isOK() )
	{
	    return false;
	}
	else if(projectDiscrp.getText().equals(null)) {
		return false;
		
	}
	else if(!nameStatus.isOK() || projectName == null) {
		return false;
	}

	else {
		return true;
		
	}


    }
    @Override
    public IWizardPage getNextPage()
    {
	saveDataToWizard();
	page1A = wizard.getPage1A();
	
		

	return page1A;
    }
    @Override
    public boolean canFlipToNextPage()
    {
    	if(selectionList.getItemCount()<=0 /*&& */) {
			//setPageComplete(false);
    		return false;
    		
			
		}
    	else if(!testRegularExpression(projectName.getText())) {
    		return false;
		}
    	
    		 
    	else {
    	
			//setPageComplete(true);
			return true;
		}

	 //return isPageComplete();
	//return true;
    }
    public void saveDataToWizard()
    {
	
	
	 langIndex = languageChoice.getSelectionIndex();
	//langIndex = 0;// javaLang
	wizard.setLangIndex(langIndex);
	String a = projectName.getText();
	
	if(a==null) {
		setPageComplete(false);
	}else {
		wizard.setProjectName(a);
	}
	a= projectDiscrp.getText();
	if(a.equals(null)) {
		setPageComplete(false);
	}else {
		wizard.setProjectDicrp(a);
	}
	/*groupIndex = groupChoice.getSelectionIndex();
	//groupIndex = 0;
	if (groupIndex == 1)
	{
	    groupIndex++;
	}
	wizard.setGroupIndex(groupIndex);*/
	

	if (selectionList.getItemCount() > 0)
	{
	    String[] fileList = selectionList.getItems();
	    if (currentGroup < fileGroups.size())
	    {
		fileGroups.set(currentGroup, fileList);
	    } else
	    {
		fileGroups.add(currentGroup, fileList);
	    }
	}

	wizard.setFileList(fileGroups);

	try
	{
	    String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES);
	    System.out.println("Here: "+pathStr);
	    File file = new File(pathStr);
	    file.createNewFile();
	    FileOutputStream fileout = new FileOutputStream(file);
	    PrintWriter stdout = new PrintWriter(fileout);

	    for (int i = 0; i < fileGroups.size(); i++)
	    {
		for (int j = 0; j < fileGroups.get(i).length; j++)
		{
		    if (j == fileGroups.get(i).length - 1 && i != fileGroups.size() - 1)
		    {
			stdout.println(fileGroups.get(i)[j] + ";");
		    } else
		    {
			stdout.println(fileGroups.get(i)[j]);
		    }
		}
	    }
	    stdout.flush();
	    stdout.close();
	    fileout.close();

	   /* pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_CLUSTER_PTRS);
	    file = new File(pathStr);
	    file.createNewFile();
	    fileout = new FileOutputStream(file);
	    stdout = new PrintWriter(fileout);
	    stdout.print(fpcInt + "," + ftcInt + "," + mpcInt + "," + mtcInt);
	    stdout.flush();
	    stdout.close();
	    fileout.close();*/
	} catch (Exception e)
	{
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	}

	// init.setMinerSettings(groupIndex,fileGroups.size(), delim, stcInt, fpcInt,
	// ftcInt, mpcInt, mtcInt);
	//init.setMinerSettings(0, fileGroups.size(), delim, stcInt, fpcInt, ftcInt, mpcInt, mtcInt);
	wizard.setSavePage1(true);
    }
    public static IProject getCurrentProject()
    {
	ISelectionService selectionService = Workbench.getInstance().getActiveWorkbenchWindow().getSelectionService();
	ISelection selection = selectionService.getSelection();
	IProject project = null;
	if (selection instanceof IStructuredSelection)
	{
	    Object element = ((IStructuredSelection) selection).getFirstElement();

	    if (element instanceof IResource)
	    {
		project = ((IResource) element).getProject();
	    } 
	    else if (element instanceof PackageFragmentRootContainer)
	    {
		IJavaProject jProject = ((PackageFragmentRootContainer) element).getJavaProject();
		project = jProject.getProject();
	    } 
	    else if (element instanceof IJavaElement)
	    {
		IJavaProject jProject = ((IJavaElement) element).getJavaProject();
		project = jProject.getProject();
	    }
	}
	return project;
    }

}
