package cmt.cddc.clonedetectioninitializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Vector;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import cmt.common.Directories;

public class CloneDetectionSettingsWizardSetThreshold extends WizardPage implements Listener {
	
	
	private CloneDetectionSettingsWizard wizard;
    private CloneDetectionSettingsWizardSetTokensToSkip page2;
    private Text fpc;
    private IStatus fpcStatus;
    private Text ftc;
    private IStatus ftcStatus;
    private Text mpc;
    private IStatus mpcStatus;
    private Text mtc;
    private IStatus mtcStatus;
    private Text scc;
    private IStatus sccStatus;
    private Button methodAnalysisButton;
    private int languageChoice = 0;
    

	public CloneDetectionSettingsWizardSetThreshold(int langChoice, CloneDetectionSettingsWizard wizard) {
		// TODO Auto-generated constructor stub
		
		super("Minimum Token and Input Files Settings");
		setTitle("Settings for Minimum Tokens and Input Files");
		setDescription("Set the minimum tokens of different types of clone classes;\n"
			+ "Set whether method boundaries should be detected during clone detection "
			+ "(applicable to Java Language only);\nAdd input source files of " + "your selected language.");
		this.wizard = wizard;
		languageChoice = langChoice;
		
		sccStatus = new Status(IStatus.OK, "not_used", 0, "", null);
		ftcStatus = new Status(IStatus.OK, "not_used", 0, "", null);
		fpcStatus = new Status(IStatus.OK, "not_used", 0, "", null);
		mtcStatus = new Status(IStatus.OK, "not_used", 0, "", null);
		mpcStatus = new Status(IStatus.OK, "not_used", 0, "", null);
		
	}
	
	public void createControl(Composite parent) {
		//parent.setEnabled(false);
		Composite container = new Composite(parent, SWT.NONE);

		setControl(container);
		
		Group grpSimpleClones = new Group(container, SWT.SHADOW_NONE);
		grpSimpleClones.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpSimpleClones.setText("Simple Clones");
		grpSimpleClones.setBounds(103, 10, 276, 68);
		
		Label lblMinimumSimilarity = new Label(grpSimpleClones, SWT.NONE);
		lblMinimumSimilarity.setBounds(10, 35, 157, 15);
		lblMinimumSimilarity.setText("Minimum Similarity");
		
		scc = new Text(grpSimpleClones, SWT.BORDER);
		scc.setBounds(190, 32, 50, 21);
		scc.setText("30");
		scc.addListener(SWT.Verify, this);
		
		Group grpMethodClones = new Group(container, SWT.NONE);
		grpMethodClones.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpMethodClones.setText("Method Clones");
		grpMethodClones.setBounds(103, 101, 276, 76);
		
		Label lblMinimumSimilarities = new Label(grpMethodClones, SWT.NONE);
		lblMinimumSimilarities.setBounds(10, 22, 157, 15);
		lblMinimumSimilarities.setText("Minimum Similarities");
		
		mtc = new Text(grpMethodClones, SWT.BORDER);
		mtc.setBounds(191, 16, 48, 21);
		mtc.setText("50");
		mtc.addListener(SWT.Verify, this);
		mtc.setEnabled(true);
		
		mpc = new Text(grpMethodClones, SWT.BORDER);
		mpc.setBounds(191, 43, 48, 21);
		mpc.addListener(SWT.Verify, this);
		mpc.setText("50");
		mpc.setEnabled(true);
		
		Label lblMinimumTokenCoverage_1 = new Label(grpMethodClones, SWT.NONE);
		lblMinimumTokenCoverage_1.setBounds(10, 49, 157, 15);
		lblMinimumTokenCoverage_1.setText("Minimum Token Coverage");
		
		/*methodAnalysisButton = new Button(grpMethodClones, SWT.CHECK);
		methodAnalysisButton.setBounds(10, 65, 157, 16);
		methodAnalysisButton.addListener(SWT.Selection, this);
		methodAnalysisButton.setText("Method Analysis");*/
		
		Group grpFileClones = new Group(container, SWT.NONE);
		grpFileClones.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpFileClones.setText("File Clones");
		grpFileClones.setBounds(103, 213, 276, 82);
		
		Label lblMiniumSimilarities = new Label(grpFileClones, SWT.NONE);
		lblMiniumSimilarities.setBounds(10, 24, 157, 15);
		lblMiniumSimilarities.setText("Minimum Similarities");
		
		Label lblMinimumTokenCoverage_2 = new Label(grpFileClones, SWT.NONE);
		lblMinimumTokenCoverage_2.setBounds(10, 51, 157, 15);
		lblMinimumTokenCoverage_2.setText("Minimum Token Coverage");
		
		fpc = new Text(grpFileClones, SWT.BORDER);
		fpc.setBounds(193, 51, 48, 21);
		fpc.addListener(SWT.Verify, this);
		fpc.setText("50");
		
		
		ftc = new Text(grpFileClones, SWT.BORDER);
		ftc.setBounds(193, 24, 48, 21);
		ftc.setText("50");
		ftc.addListener(SWT.Verify, this);
	}
	@Override
	public void handleEvent(Event e) {
		Status status = new Status(IStatus.OK, "not_used", 0, "", null);
		if (e.widget == scc)
		{
			String string = e.text;
	        char[] chars = new char[string.length()];
	        string.getChars(0, chars.length, chars, 0);
	        for (int i = 0; i < chars.length; i++) {
	          if (!('0' <= chars[i] && chars[i] <= '9')|| chars[i] ==',') {
	            e.doit = false;
	            return;
	          }
	        }
		    try
		    {
			if (!scc.getText().equalsIgnoreCase(""))
			{
			    int sccInt = Integer.parseInt(scc.getText().trim());
			    if (sccInt <= 0)
			    {
				status = new Status(IStatus.ERROR, "not_used", 0,
					"Minimum Token Count Value Cannot Be Smaller Than or Smaller Than or Equal To 0", null);
			    }
			} else
			{
			    status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Count Value Cannot Be Empty",
				    null);
			}
		    } catch (Exception exception)
		    {
			status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Count Value Must Be A Integer Value",
				null);
		    }

		    sccStatus = status;
		}
		else if (e.widget == ftc)
		{
			String string = e.text;
	        char[] chars = new char[string.length()];
	        string.getChars(0, chars.length, chars, 0);
	        for (int i = 0; i < chars.length; i++) {
	          if (!('0' <= chars[i] && chars[i] <= '9')|| chars[i] ==',') {
	            e.doit = false;
	            return;
	          }
	        }
		    try
		    {
			if (!ftc.getText().equalsIgnoreCase(""))
			{
			    int ftcInt = Integer.parseInt(ftc.getText().trim());
			    if (ftcInt <= 0)
			    {
				status = new Status(IStatus.ERROR, "not_used", 0,
					"Minimum Token Count Value Cannot Be Smaller Than or Smaller Than or Equal To 0", null);
			    }
			} else
			{
			    status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Count Value Cannot Be Empty",
				    null);
			}
		    } catch (Exception exception)
		    {
			status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Count Value Must Be An Integer Value",
				null);
		    }

		    ftcStatus = status;
		}
		else if (e.widget == fpc)
		{
			String string = e.text;
	        char[] chars = new char[string.length()];
	        string.getChars(0, chars.length, chars, 0);
	        for (int i = 0; i < chars.length; i++) {
	          if (!('0' <= chars[i] && chars[i] <= '9')|| chars[i] ==',') {
	            e.doit = false;
	            return;
	          }
	        }
		    try
		    {
			if (!fpc.getText().equalsIgnoreCase(""))
			{
			    int fpcInt = Integer.parseInt(fpc.getText().trim());
			    if (fpcInt > 100 || fpcInt <= 0)
			    {
				status = new Status(IStatus.ERROR, "not_used", 0,
					"Minimum Token Percentage Value Cannot Be Larger Than 100 or Smaller Than or Equal To 0",
					null);
			    }
			} else
			{
			    status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Percentage Value Cannot Be Empty",
				    null);
			}
		    } catch (Exception exception)
		    {
			status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Percentage Must Be An Integer Value",
				null);
		    }

		    fpcStatus = status;
		}
		else if (e.widget == mtc)
		{
			String string = e.text;
	        char[] chars = new char[string.length()];
	        string.getChars(0, chars.length, chars, 0);
	        for (int i = 0; i < chars.length; i++) {
	          if (!('0' <= chars[i] && chars[i] <= '9')|| chars[i] ==',') {
	            e.doit = false;
	            return;
	          }
	        }
		    try
		    {
			boolean isMethodAnalysis = methodAnalysisButton.getSelection();
			if (isMethodAnalysis)
			{
			    if (!mtc.getText().equalsIgnoreCase(""))
			    {
				int mtcInt = Integer.parseInt(mtc.getText().trim());
				if (mtcInt <= 0)
				{
				    status = new Status(IStatus.ERROR, "not_used", 0,
					    "Minimum Token Count Value Cannot Be Smaller Than or Equal To 0", null);
				}
			    } else
			    {
				status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Count Value Cannot Be Empty",
					null);
			    }
			}
		    } catch (Exception exception)
		    {
			status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Count Value Must Be An Integer Value",
				null);
		    }

		    mtcStatus = status;
		}
		else if (e.widget == mpc)
		{
			String string = e.text;
	        char[] chars = new char[string.length()];
	        string.getChars(0, chars.length, chars, 0);
	        for (int i = 0; i < chars.length; i++) {
	          if (!('0' <= chars[i] && chars[i] <= '9')|| chars[i] ==',') {
	            e.doit = false;
	            return;
	          }
	        }
		    try
		    {
			/*boolean isMethodAnalysis = methodAnalysisButton.getSelection();
			if (isMethodAnalysis)
			{*/
			    if (!mpc.getText().equalsIgnoreCase(""))
			    {
				int mpcInt = Integer.parseInt(mpc.getText().trim());
				if (mpcInt > 100 || mpcInt <= 0)
				{
				    status = new Status(IStatus.ERROR, "not_used", 0,
					    "Minimum Token Percentage Value Cannot Be Larger Than 100 or Smaller Than or Equal To 0",
					    null);
				}
			    } else
			    {
				status = new Status(IStatus.ERROR, "not_used", 0,
					"Minimum Token Percentage Value Cannot Be Empty", null);
			    }
			/*}*/
		    } catch (Exception exception)
		    {
			status = new Status(IStatus.ERROR, "not_used", 0, "Minimum Token Percentage Must Be An Integer Value",
				null);
		    }

		    mpcStatus = status;
		}
		/*else if (e.widget == methodAnalysisButton)
		{
		    if (methodAnalysisButton.getSelection())
		    {
		    	
			mtc.setEnabled(true);
			mpc.setEnabled(true);
		    } else
		    {
			mtc.setEnabled(false);
			mpc.setEnabled(false);
		
		    }
		    
		}*/
	}
	
	@Override
    public boolean isPageComplete()
    {
	if (getErrorMessage() != null)
	{
	    return false;
	}

	boolean methodStatusOK = true;
	if (methodAnalysisButton.getSelection() && (!mtcStatus.isOK() || !mpcStatus.isOK()))
	{//
	    methodStatusOK = false;
	}

	// if(sccStatus.isOK() && ftcStatus.isOK() && fpcStatus.isOK()
	// && methodStatusOK && selectionListStatus.isOK()){
	if (sccStatus.isOK() && ftcStatus.isOK() && fpcStatus.isOK() && methodStatusOK)
	{
	    return true;
	}

	return false;

    }
	
	public void applyErrorMessage()
    {
	boolean methodStatusOK = true;
	//
	if (methodAnalysisButton.getSelection() && (!mtcStatus.isOK() || !mpcStatus.isOK()))
	{
	    methodStatusOK = false;
	}

	if (sccStatus.isOK() && ftcStatus.isOK() && fpcStatus.isOK() && methodStatusOK)
	{
	    setErrorMessage(null);
	} else
	{
	    String msg = "";
	    /*
	     * if(!selectionListStatus.isOK()){ msg += selectionListStatus.getMessage() +
	     * "\n"; }
	     */
	    if (!sccStatus.isOK())
	    {
		msg += sccStatus.getMessage() + "\n";
	    }
	    if (!ftcStatus.isOK())
	    {
		msg += ftcStatus.getMessage() + "\n";
	    }
	    if (!fpcStatus.isOK())
	    {
		msg += fpcStatus.getMessage() + "\n";
	    }
	    if (methodAnalysisButton.getSelection() && !mtcStatus.isOK())
	    {
		msg += mtcStatus.getMessage() + "\n";
	    }
	    if (methodAnalysisButton.getSelection() && !mpcStatus.isOK())
	    {//
		msg += mpcStatus.getMessage() + "\n";
	    }
	    setErrorMessage(msg);
	}
    }
	 @Override
	    public IWizardPage getNextPage()
	    {
		saveDataToWizard();
		page2 = wizard.getPage21();
		
			if (languageChoice == 0) {
				page2 = wizard.getPage21();
			} else if (languageChoice == 1) {
				page2 = wizard.getPage22();
			}else if (languageChoice == 2) {
				page2 = wizard.getPage27();
			} else if (languageChoice == 3) {
				page2 = wizard.getPage24();
			} else if (languageChoice == 4) {
				page2 = wizard.getPage25();
			} else if (languageChoice == 5) {
				page2 = wizard.getPage26();
			} else {
				page2 = null;
			}
		 

		return page2;
	    }
	 
	 @Override
	    public boolean canFlipToNextPage()
	    {

		// return isPageComplete();
		return true;
	    }
	 
	 public void saveDataToWizard() {
		 int stcInt = Integer.parseInt(scc.getText().trim());
		 wizard.setStc(stcInt);
		 int ftcInt = Integer.parseInt(ftc.getText().trim());
			int fpcInt = Integer.parseInt(fpc.getText().trim());
			wizard.setMinFClusp(fpcInt);
			wizard.setMinFClust(ftcInt);
			boolean delim = true;
			wizard.setDelim(delim);
			int mtcInt = 0;
			int mpcInt = 0;
			/*if (methodAnalysisButton.getSelection())
			{*/
				
			    mtcInt = Integer.parseInt(mtc.getText().trim());
			    mpcInt = Integer.parseInt(mpc.getText().trim());
			    wizard.setMinMClusp(mpcInt);
			    wizard.setMinMClust(mtcInt);
			    
			/*}*/
			try {
				String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_CLUSTER_PTRS);
			    File file = new File(pathStr);
			    file.createNewFile();
			    FileOutputStream fileout = new FileOutputStream(file);
			    PrintWriter stdout = new PrintWriter(fileout);
			    stdout.print(fpcInt + "," + ftcInt + "," + mpcInt + "," + mtcInt);
			    stdout.flush();
			    stdout.close();
			    fileout.close();
			} catch (Exception e)
			{
			    System.err.println(e.getMessage());
			    e.printStackTrace();
			
			}
			wizard.setSavePage1A(true);
		 
	 }
	
	
	

}
