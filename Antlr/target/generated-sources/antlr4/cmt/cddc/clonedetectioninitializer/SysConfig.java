package cmt.cddc.clonedetectioninitializer;

import java.util.ArrayList;

import antlr.auto.gen.java.JavaLexer;

public final class SysConfig 
{
	    private static int langChoice;
	    private static boolean[] supressTokens = new boolean[JavaLexer.ruleNames.length+1];
	    private static ArrayList<ArrayList<Integer>> equateTokens;
	    private static int threshold;
	    private static int groupCheck;
	    private static float RNRThreshold = (float) 0.5;  //hard coded for the time being until it's added in the UI..    
		//global variables for clustering
	    private static int minFClusP = 0;
	    private static int minFClusT = 0;
	    private static int minMClusP = 0;
	    private static int minMClusT = 0;
	    private static String projectName;
	    private static String projectDiscrp;
	    private static long tym;
	    private static Boolean enableFraming = false;
	    
	    public static Boolean getEnableFraming() {
			return enableFraming;
		}
		public static void setEnableFraming(Boolean enableFraming) {
			SysConfig.enableFraming = enableFraming;
		}
		public static float getRNRThreshold() {
			return RNRThreshold;
		}
		public static void setRNRThreshold(float rNRThreshold) {
			RNRThreshold = rNRThreshold;
		}
	    public static void setProjectDiscrp(String a) {
	    	projectDiscrp = "";
	    	projectDiscrp = a; 
	    }
	    public static String getProjectDiscrp() {
	    	return projectDiscrp; 
	    }
	    public static void setProjectName(String name) {
	    	projectName = "";
	    	projectName = name;
	    }
	    public static String getProjectName() {
	    	return projectName;
	    }
	    
	    public static int getLangChoice() {
	    	langChoice= 0;
			return langChoice;
		}
		public static void setLangChoice(int langChoice) {
			SysConfig.langChoice = langChoice;
		}
		public static boolean[] getSupressTokens() {
			
			return supressTokens;
		}
		public static void setSupressTokens(boolean[] supressTokens) {
			
			SysConfig.supressTokens = supressTokens;
		}
		public static ArrayList<ArrayList<Integer>> getEquateTokens() {
			return equateTokens;
		}
		public static void setEquateTokens(ArrayList<ArrayList<Integer>> equateTokens) {
			SysConfig.equateTokens = new ArrayList<>();
			SysConfig.equateTokens = equateTokens;
		}
		public static int getThreshold() {
			return threshold;
		}
		public static void setThreshold(int threshold) {
			SysConfig.threshold = threshold;
		}
		public static int getGroupCheck() {
			return groupCheck;
		}
		public static void setGroupCheck(int groupCheck) {
			SysConfig.groupCheck = groupCheck;
		}
		public static void addEquateToken(ArrayList<Integer> equateToken)
		{
			equateTokens.add(equateToken);
		}
		public static void setSuppressTokenAt(int index , boolean supressToken)
		{
			supressTokens[index] = supressToken;
		}
		public static int getMinFClusP() {
			return minFClusP;
		}
		public static void setMinFClusP(int minFClusP) {
			SysConfig.minFClusP = minFClusP;
		}
		public static int getMinFClusT() {
			return minFClusT;
		}
		public static void setMinFClusT(int minFClusT) {
			SysConfig.minFClusT = minFClusT;
		}
		public static int getMinMClusP() {
			return minMClusP;
		}
		public static void setMinMClusP(int minMClusP) {
			SysConfig.minMClusP = minMClusP;
		}
		public static int getMinMClusT() {
			return minMClusT;
		}
		public static void setMinMClusT(int minMClusT) {
			SysConfig.minMClusT = minMClusT;
		}
		public static void setSystemTym() {
			tym = System.currentTimeMillis();
			
		}
		public static String getSystemTime() {
			String a = String.valueOf(tym);
			return a;
		}
		public static void clearAll() {
			equateTokens.clear();
			
			for(int i=0;i<supressTokens.length;i++) {
				supressTokens[i]= false;
				
			}
		}
}
