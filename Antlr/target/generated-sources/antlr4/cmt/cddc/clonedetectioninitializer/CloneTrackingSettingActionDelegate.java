package cmt.cddc.clonedetectioninitializer;

import java.util.ArrayList;
import java.util.Vector;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonerunmanager.CloneRunConfiguration;
import cmt.cddc.clonerunmanager.CloneRunInfoReader;
import cmt.cddc.clonerunmanager.CloneRunInfoWriter;
//import cmt.cddc.clonerunmanager.CloneRunUpdateTemp;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.clonedetectionSettingsUI.CloneDetectionEditorInput;
import cmt.ctc.mongodb.AllFilesContent;

public class CloneTrackingSettingActionDelegate  extends Action implements IWorkbenchWindowActionDelegate{
	
   // cloneRunConfiguration;

	public static void triggerIncrementalCloneDetection() {
		  
		
		ArrayList<CloneRunConfiguration>cloneRunConfiguration = CloneRunInfoReader.getCloneRuns();
		boolean cloneRunSet = false;
		 for(CloneRunConfiguration config : cloneRunConfiguration) {
		      if(config.getEnableFraming()) {
		      LoadList loadList = new LoadList();
		      loadList.setActiveCloneRun(config); 
		      cloneRunSet = true;
		      break;
		      }
		 }
		 			
		 Shell shell = new Shell();
		 if(cloneRunSet) {			
			 try 
				{
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().openEditor(
						new CloneDetectionEditorInput("Clone Tracking "), "cmt.clonedetectionSettingsUI.CloneTrackingFormEditor");
				}
				catch (PartInitException e) {
					System.out.println(e);
				}
		 
		 
		 }
		 
		 if(!cloneRunSet) {
			  shell = new Shell( SWT.CLOSE | SWT.TITLE);
		   	  MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING); 
			  messageBox.setText("Warning");
		      messageBox.setMessage("Please enable Clone Tracking and Framing from Clone Run Setting UI.");
		      messageBox.open();
		
		 }
	}

	

	@Override
	public void run(IAction arg0) {	
		triggerIncrementalCloneDetection();
		
	}



	@Override
	public void selectionChanged(IAction arg0, ISelection arg1) {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void init(IWorkbenchWindow arg0) {
		// TODO Auto-generated method stub
		
	}
	

}
