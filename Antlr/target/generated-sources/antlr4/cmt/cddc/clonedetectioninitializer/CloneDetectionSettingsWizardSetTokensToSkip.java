package cmt.cddc.clonedetectioninitializer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import cmt.common.Directories;


public class CloneDetectionSettingsWizardSetTokensToSkip extends WizardPage{
	
    
    Button button;
    static ArrayList<Button> btn = new ArrayList<>();
    private Table tokenList;
    private int langIndex = 0;
    private String lang;
    private String langTitle;
    private CloneDetectionSettingsWizard wizard;
    private CloneDetectionSettingsWizardCreateRules page3;
    HashSet <String> selected = new HashSet<String>();
    Hashtable<Integer,String> tokenData= new Hashtable<Integer,String>();
    static ArrayList<String> SkipTokens = new ArrayList<String>();
    private void SetSkipToken(String token) {
    	SkipTokens.add(token);
    }
    
    //tokensNotToShow.
    
   
    int tokenCount = 0;
    Boolean haveValue = true;
    public void SkipToken() {
    	String[] token = {"Identifier", "(", ")", "throws", ";", "{", "}", "class", "interface", "=", "]"};
    	for(int i=0; i<token.length; i++) {
    		SetSkipToken(token[i]);
    		}
    	}
   
    
    //private final Initializer init = CloneManagementPlugin.getInitializer();
    
	public CloneDetectionSettingsWizardSetTokensToSkip(int langIndex, CloneDetectionSettingsWizard wizard)
    {
		super("Minimum Token and Input Files Settings");
		//wizard.getShell().setSize(575, 550);
		setTitle("Settings for Suppressed Tokens");
		
		this.langIndex = langIndex;
		if (langIndex == 0)
		{
		   lang = Directories.TOKEN_JAVA;
		    langTitle = "Java ";
		} else if (langIndex == 1)
		{
		    lang = Directories.TOKEN_CPP;
		    langTitle = "C++ ";
		} else if (langIndex == 3)
		{
		    lang = Directories.TOKEN_CSHARP;
		    langTitle = "CSHARP ";
		} else if (langIndex == 4)
		{
		    lang = Directories.TOKEN_RUBBY;
		    langTitle = "RUBY ";
		} else if (langIndex == 5)
		{
		    lang = Directories.TOKEN_PHP;
		    langTitle = "PHP ";
		} else if (langIndex == 6)
		{
		    lang = Directories.TOKEN_TXT;
		    langTitle = "TXT ";
		} else
		{
		    lang = null;
		    langTitle = "";
		}
		setDescription("Select the suppressed tokens from the "+langTitle+" token list below.");
		 
		this.wizard = wizard;
    }
	public CloneDetectionSettingsWizardSetTokensToSkip(String pageName, String title, ImageDescriptor titleImage)
    {
	super(pageName, title, titleImage);
	 
    }
	
	

	@Override
	public void createControl(Composite parent) {
		
		Composite container = new Composite(parent, SWT.NONE);//V_SCROLL | SWT.BORDER | SWT.RESIZE);
		
		SkipToken();
		
		tokenList = new Table(container, SWT.BORDER | SWT.MULTI);
		tokenList.setBounds(0, 21, 574, 312);
		
		
		int width = (574/4)-5;
	    for (int i = 0; i < 4; i++) {
	      TableColumn column = new TableColumn(tokenList, SWT.NONE);
	       
	      column.setWidth(width);
	    }
		
		
		try
		{
			
		    String filePath = Directories.getAbsolutePath(lang);
		    File file = new File(filePath);
		    FileInputStream filein = new FileInputStream(file);
		    BufferedReader stdin = new BufferedReader(new InputStreamReader(filein));
		    String line = "";

		    while ((line = stdin.readLine()) != null)
		    {
		    	int last = line.lastIndexOf("=");
		    	
		    	int endIndex = last;
		    	int beginIndex = last +1;
		    	int length = line.length();
		    	String token = line.substring(0, endIndex);
		    	String tokenID = line.substring(beginIndex, length);
		    	String IDS = "";
		    	
		    		if(tokenData.containsKey(Integer.parseInt(tokenID))) {
			    		if(token.contains("'")) {
				    		if(token.lastIndexOf("'")==(token.length()-1)) {
				    			IDS = token.substring(1, (token.length()-1));
				    			if(!SkipTokens.contains(IDS)) {
				    				tokenData.put(Integer.parseInt(tokenID), token);
				    			}
				    			else {
				    				tokenData.remove(Integer.parseInt(tokenID));
				    			}
				    			
				    			
				    		}
				    		
				    	}
			    		
			    	}
			    	else {
			    		if(!SkipTokens.contains(token)) {
		    				tokenData.put(Integer.parseInt(tokenID), token);
		    			}
			    	}
		    	}
		    wizard.setToken_details(tokenData);
		    
		    
		    
		    tokenCount = tokenData.size();
		    
		    System.out.println(tokenCount);
		    if(!btn.isEmpty()) {
		    	btn.clear();
		    }
		    for (int i =0;i<(tokenData.size()/4)+(tokenData.size()%4) ;i++) {
		    	new TableItem(tokenList, SWT.NONE);
		    }
		    TableItem[] items = tokenList.getItems();
		    int j=0, setSize=0, flag =0;
		    
		    for(Entry<Integer, String> m:tokenData.entrySet()) {
		    	if(setSize!= tokenData.size()) {
		    		if(j==0) {
			    		String a = m.getValue();
				    	TableEditor editor = new TableEditor(tokenList);
				    	button = new Button(tokenList, SWT.CHECK);//container
				    	
					    button.setText(m.getValue());
					    button.setData(m.getKey());
					    
					    btn.add(button);
					    editor.grabHorizontal = true;
					      editor.setEditor(button, items[flag], j);
					     
					      j++;
			    	}
			    	else if(j==1) {
			    		String a = m.getValue();
				    	TableEditor editor = new TableEditor(tokenList);
				    	button = new Button(tokenList, SWT.CHECK);//container
				    	
				    	
					    button.setText(m.getValue());
					    button.setData(m.getKey());
					    
					    btn.add(button);
					    editor.grabHorizontal = true;
					      editor.setEditor(button, items[flag], j);
					     
					      j++;
			    	}
			    	else if(j==2) {
			    		String a = m.getValue();
				    	TableEditor editor = new TableEditor(tokenList);
				    	button = new Button(tokenList, SWT.CHECK);//container
				    	System.out.println(m.getKey());
				    	
				    	System.out.println(m.getValue());
				    	
					    button.setText(m.getValue());
					    button.setData(m.getKey());
					    
					    btn.add(button);
					    editor.grabHorizontal = true;
					      editor.setEditor(button, items[flag], j);
					     
					      j++;
			    	}
			    	else if (j==3) {
			    		String a = m.getValue();
				    	TableEditor editor = new TableEditor(tokenList);
				    	button = new Button(tokenList, SWT.CHECK);//container
				    	System.out.println(m.getKey());
				    	
				    	System.out.println(m.getValue());
				    	
					    button.setText(m.getValue());
					    button.setData(m.getKey());
					    
					    btn.add(button);
					    editor.grabHorizontal = true;
					      editor.setEditor(button, items[flag], j);
					     
					      j=0;flag++;
			    	}
			    	setSize++;
		    	}
		    	
			    
		    }
		    

		    int size = btn.size();
		    for(int i =0; i<size; i++) {
		    	Button b = btn.get(i);
		    	b.addSelectionListener(new SelectionAdapter() {
		    		
		    		@Override
		    		public void widgetSelected(SelectionEvent e) {
		    			// TODO Auto-generated method stub
		    			if(b.getSelection()){
		    				b.setSelection(true);
			    			System.out.println(b.getText());
			    			selected.add(b.getText());
		    			}
		    			else {
		    				b.setSelection(false);
		    				selected.remove(b.getText());
		    			}
		    			System.out.println(selected.toString());
		    			
		    		}
				});
		    }
 
		    
		   
	
		    stdin.close();
		} catch (Exception e)
		{
		    System.err.println(e.getMessage());
		    e.printStackTrace();
		}
		
		setControl(container);
	}
	@Override
    public IWizardPage getNextPage()
    {
	saveDataToWizard();
	if (langIndex == 0)
	{
	    page3 = wizard.getPage31();
	} else if (langIndex == 1)
	{
	    page3 = wizard.getPage32();
	} else if (langIndex == 3)
	{
	    page3 = wizard.getPage34();
	} else if (langIndex == 4)
	{
	    page3 = wizard.getPage35();
	} else if (langIndex == 5)
	{
	    page3 = wizard.getPage36();
	} else if (langIndex == 6)
	{
	    page3 = wizard.getPage37();
	} else
	{
	    page3 = null;
	}

	return page3;
    }
	@Override
    public boolean canFlipToNextPage()
    {
	return isPageComplete();
    }
	
	
	
	public void saveDataToWizard()
    {
		
	try
	{
	    String surpressedTokenFilePath = Directories.getAbsolutePath(Directories.CLONES_INPUT_SUPPRESSED);
	    File file = new File(surpressedTokenFilePath);
	    file.createNewFile();
	    FileOutputStream fileout = new FileOutputStream(file);
	    PrintWriter stdout = new PrintWriter(fileout);
	    if(selected.size()!=0) {
	    	String b = selected.toString();
	    	int a = b.length();
	    	b = b.substring(1, --a);
	    	String[] c = b.split(", ");
	    	a= c.length;
	    	b="";
	    	ArrayList<Integer> tokenID= new ArrayList<Integer>();
	    	
	    	for(int i=0;i<c.length;i++) {
	    		for(Entry<Integer, String> m:tokenData.entrySet()) {
		    		if(m.getValue().equals(c[i])) {
		    			b = b+m.getKey()+",";
		    			tokenID.add(m.getKey()) ;
		    			break;
		    			
		    		}
		    		
		    		
		    	}
	    	}
	    	int []aa = new int[tokenID.size()];
	    	for(int i=0; i<tokenID.size();i++) {
	    		aa[i] = tokenID.get(i);
	    	}
	    	System.out.println(b);
	    	wizard.setSuppressedTokenID(aa);
	    	selected.clear();
	    	stdout.println(b);
	    	
	    	
	    	
	    }
	    int []aa = new int[0];
	    wizard.setSuppressedTokenID(aa);
	    stdout.flush();
	    stdout.close();
	    fileout.close();
	} catch (Exception ex)
	{
	    System.err.println(ex.getMessage());
	    ex.printStackTrace();
	}
	//tokenSelected.setEditable(false);
	wizard.setSavePage2(true);
    }

}
