package cmt.cddc.clonedetectioninitializer;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.internal.ui.packageview.PackageFragmentRootContainer;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IPerspectiveDescriptor;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.Workbench;



import org.eclipse.ui.WorkbenchException;
import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.CloneRunInfoReader;
import cmt.cddc.clonerunmanager.CloneRunInfoWriter;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.cvac.views.FccView;
import cmt.cvac.views.FcsViewCrossDirs;
import cmt.cvac.views.MccView;
import cmt.cvac.views.McsView;
import cmt.cvac.views.SccView;

public class VisualizationSettingsActionDelegate extends Action implements IWorkbenchWindowActionDelegate
{

    private IWorkbenchWindow window;
    private CloneDetectionSettingsWizard cdsWizard;
    public static ClonesReader reader =  null;
    public VisualizationSettingsActionDelegate()
    {
    	super();
    }

    @Override
    public void dispose()
    {

    }

    @Override
    public void init(IWorkbenchWindow window)
    {
    	this.window = window;
    }
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage,  titleBar, JOptionPane.WARNING_MESSAGE/*INFORMATION_MESSAGE*/);
    }
    private void syncWithUi() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				visualizeData();

				/*MessageDialog.openInformation(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Status",
						"Clone Reading Has Finished.");*/
				
			}
		});

	}
   
  //  public void confirmationChangePerspective1() {
   	public static void infoBox(){  
   		
   		Shell shell = new Shell( SWT.CLOSE | SWT.TITLE);
   		MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING| SWT.YES | SWT.NO); 
   		
   		messageBox.setText("question");
   		messageBox.setMessage("Previously Saved Framing Data will be Deleted.\nDo You want to continue?");
   		int buttonID = messageBox.open();
   		switch(buttonID) {
       case SWT.YES:
       	{
     	       	  
       	}
       	case SWT.NO:
         // exits here ...
       		break;
       case SWT.CANCEL:
         // does nothing ...       
    	   }
    }
    
    public void confirmationChangePerspective() {
    	  JFrame f = new JFrame();
    	  int response  =  JOptionPane.showConfirmDialog(f,"Clones are configured to be viewed in Clones Perspective.\n" +
    		         "Do you want to open this perspective now \n", 
    		        "Confirm Perspective Switch",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
    	    if (response == JOptionPane.NO_OPTION) {
    	    
    	    } else if (response == JOptionPane.YES_OPTION) {
    	    	openPerspective("CMTPerspective");
    	    	displayClonesinTabularView();
    	    	
    	    } else if (response == JOptionPane.CLOSED_OPTION) {
    	      
    	    }
    }
    
    private void openPerspective(String perspectiveID) {
    	IWorkbench workbenchWindow = PlatformUI.getWorkbench();
    	window = workbenchWindow.getActiveWorkbenchWindow();    	
    	try{ 
    		PlatformUI.getWorkbench().showPerspective( perspectiveID, window); 
    	} 
    	catch (WorkbenchException e) { 
    	System.out.println("ERROR! Unable to open Perspective"); 
    	MessageDialog.openError(window.getShell(), 
    	"Error Opening Perspective", 
    	"Could not open Perspective with ID: " + perspectiveID ); 
    	}
    } 
    
    public void displayClonesinTabularView() {
    	//	IWorkbench workbench = PlatformUI.getWorkbench();
    	 try {
         //    workbench.showPerspective(perspectiveID, workbench.getActiveWorkbenchWindow());
         	 IWorkbenchPage workbenchPage = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
         	  
         		   	CloneDetectionSettingsWizard.closeOpenedViews(workbenchPage);
         		   	MccView.filterFlag = false;
         		    SccView.filterFlag  = false;
         		    FccView.filterFlag = false;
         		    McsView.filterFlag = false;
         		    McsView.filtered = false;
         		    
         		    workbenchPage.showView("mccView");
         		    workbenchPage.showView("fccView");
         		    workbenchPage.showView("mcsView");
         		    workbenchPage.showView("fcsView");
         		    
         		    workbenchPage.showView("fcsViewInDirs");
         		    workbenchPage.showView("fcsViewCrossGroup");
         		    workbenchPage.showView("fcsViewInGroup");
         		   workbenchPage.showView("sccView");
         		    CloneRepository.anotateFrames();
         	}
         	   catch (Exception e)
         		{
         		    e.printStackTrace();
         		}                	
    }
    
    
    
    
    
    
   public void visualizeData() {
    	
    	IWorkbench workbenchWindow = PlatformUI.getWorkbench();
    	IWorkbenchWindow win = workbenchWindow.getActiveWorkbenchWindow();
    	IWorkbenchPage page = win.getActivePage();
    	IPerspectiveDescriptor perspective = page.getPerspective();
    	String label = perspective.getLabel();
    	if(!label.equals("Clone Management")) {
    		confirmationChangePerspective();    			
    	}   
    	else
	    	displayClonesinTabularView();    				
    }

    @Override
    public void run(IAction action)
    {
    	
    	if(CloneRepository.getDBName().equals("")) {
    		String msg = "Please Select a Project to Visualize.";
    		infoBox(msg, "Message");
    		
    	}
    	else 
    	{
    			//(Saud) Now in every case , visualization action will refresh the data
    			//if(reader == null || (reader != null && !CloneRepository.getDBName().contains(LoadList.projectName) && !CloneRepository.getDBName().contains(LoadList.creationTime))) {
    				reader = new ClonesReader();
    				Job job = new Job("Reading Clones") {

						@Override
						protected IStatus run(IProgressMonitor arg0) {
							// TODO Auto-generated method stub
							reader.readClones();
							syncWithUi();
							return Status.OK_STATUS;
						}
    					
    				};
    				job.schedule();
            		
    			/*}
    			else {
    				visualizeData();
    			}*/
    		}
    	}
    	
    

 

	@Override
    public void selectionChanged(IAction action, ISelection selection)
    {

    }
}

