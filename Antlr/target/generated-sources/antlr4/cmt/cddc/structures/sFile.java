package cmt.cddc.structures;

import java.util.ArrayList;
import java.util.HashMap;

public class sFile {
	
    private String fileName;
    private String fullPath;
    private int startToken;
    private int endToken;
    private int dirID;
    private int groupID;
    private int fileID;
    private int endLine;
    private ArrayList<Integer> cloneClasses= new ArrayList<Integer>();
    private ArrayList<Integer> vMethods= new ArrayList<Integer>();
    private ArrayList<Integer> vCloneMethodClasses= new ArrayList<Integer>();
    private Boolean updated;    
    private Boolean newlyAddedFile;

	public sFile(String fN) 
    {
    	fileName=fN;
    }
    
    public sFile(String fN , int fileId) 
    {
    	fileName=fN;
    	fileID = fileId;
    }
    
    public Boolean isNewlyAddedFile() {
		return newlyAddedFile;
	}

	public void setNewlyAddedFile(Boolean newlyAddedFile) {
		this.newlyAddedFile = newlyAddedFile;
	}
	
    public Boolean getUpdated() {
		return updated;
	}

	public void setUpdated(Boolean updated) {
		this.updated = updated;
	}
	
    public int getFileID() {
		return fileID;
	}

	public void setFileID(int fileID) {
		this.fileID = fileID;
	}
	
	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int EndLine) {
		this.endLine = EndLine;
	}
    
    public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFullPath() {
		return fullPath;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	public void setStartToken(int startToken) {
		this.startToken = startToken;
	}
	public void setEndToken(int endToken) {
		this.endToken = endToken;
	}
	public int getDirID() {
		return dirID;
	}
	public void setDirID(int dirID) {
		this.dirID = dirID;
	}
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public ArrayList<Integer> getCloneClasses() {
		return cloneClasses;
	}
	public void setCloneClasses(ArrayList<Integer> cloneClasses) {
		this.cloneClasses = cloneClasses;
	}
	public int getCloneClassesSize()
	{
		return this.cloneClasses.size();
	}
	public int getCloneClassAt(int index) {
		return cloneClasses.get(index);
	}
	public void addCloneClass(int cc)
	{
		this.cloneClasses.add(cc);
	}
	public ArrayList<Integer> getvMethods() {
		return vMethods;
	}
	public void setvMethods(ArrayList<Integer> vMethods) {
		this.vMethods = vMethods;
	}
	public ArrayList<Integer> getvCloneMethodClasses() {
		return vCloneMethodClasses;
	}
	public void setvCloneMethodClasses(ArrayList<Integer> vCloneMethodClasses) {
		this.vCloneMethodClasses = vCloneMethodClasses;
	}
	public int getvCloneMethodClassAt(int index)
	{
		return this.vCloneMethodClasses.get(index);
	}
	public void addvCloneMethodClass(int mcc)
	{
		this.vCloneMethodClasses.add(mcc);
	}
	public int getvCloneMethodClassSize()
	{
		return this.vCloneMethodClasses.size();
	}
    public int getFileStartToken()
    {
    	return this.startToken;
    }
    public int getFileEndToken()
    {
    	return this.endToken;
    }
    public void addvMethod(int methodId)
    {
    	this.vMethods.add(methodId);
    }
};
