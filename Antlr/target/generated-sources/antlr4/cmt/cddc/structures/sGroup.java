package cmt.cddc.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
 
public class sGroup {
	private int groupID;
	private String groupName;    
	private int nFiles;
    private ArrayList<Integer> vContainedFiles = new ArrayList<Integer>();
    private ArrayList<Integer> vFileCloneClasses = new ArrayList<Integer>();
   // private ArrayList<String> groupFiles = new ArrayList<String>();
    private HashSet<String> groupFiles = new HashSet<String>();
    private Map<String,Integer> groupFileIds = new HashMap<String,Integer>();
    
    private String rootDirectory = new String();
    
	public int getGroupID() {
		return groupID;
	}
	public void setGroupID(int groupID) {
		this.groupID = groupID;
	}
	public int getnFiles() {
		return nFiles;
	}
	public void setnFiles(int nFiles) {
		this.nFiles = nFiles;
	}
	public ArrayList<Integer> getvContainedFiles() {
		return vContainedFiles;
	}
	public void setvContainedFiles(ArrayList<Integer> vContainedFiles) {
		this.vContainedFiles = vContainedFiles;
	}
	public ArrayList<Integer> getvFileCloneClasses() {
		return vFileCloneClasses;
	}
	public int getvFileCloneClassSize() {
		return this.vFileCloneClasses.size();
	}
	public int getvFileCloneClassAt(int index)
	{
		return this.vFileCloneClasses.get(index);
	}
	public void setvFileCloneClasses(ArrayList<Integer> vFileCloneClasses) {
		this.vFileCloneClasses = vFileCloneClasses;
	}
	public void addContainedFiles(int fileId)
	{
		this.vContainedFiles.add(fileId);
	}
	public void addFileCloneClasses(int cloneId)
	{
		this.vFileCloneClasses.add(cloneId);
	}
	public String getGroupName() 
	{
		return groupName;
	}
	public void setGroupName(String groupName) 
	{
		this.groupName = groupName;
	}
	public HashSet<String> getGroupFiles() {
		return groupFiles;
	}
	public void setGroupFiles(HashSet<String> groupFiles) {
		this.groupFiles = groupFiles;
	}
	public String getRootPath() {
		return rootDirectory;
	}
	public void setRootPath(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}
	public Map<String,Integer> getGroupFileIds() {
		return groupFileIds;
	}
	public void setGroupFileIds(Map<String,Integer> groupFileIds) {
		this.groupFileIds = groupFileIds;
	}
	
}
