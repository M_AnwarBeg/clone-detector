package cmt.cddc.structures;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.swt.internal.C;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodCluster;
import cmt.cddc.methodclones.sMarkedMethod;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleClonesReader;


public class MCStruct {
	private class MCCCluster
	{
		ArrayList<Integer> MethodIDs;
		ArrayList<Integer> SCIDs;
	}

	private HashMap<Integer,MCCCluster> MethodCloneClusters;
	private HashMap<Integer, ArrayList<Integer>> MethodToCloneStructure;
	private HashMap<Integer, ArrayList<Integer>> CloneToMethodStructure;
	/*
	 * This structure will be filled in mongo db to for incremental clone detection
	 */
	public MCStruct()
	{
		MethodToCloneStructure = new HashMap<Integer, ArrayList<Integer>>();
		CloneToMethodStructure = new HashMap<Integer, ArrayList<Integer>>();
		MethodCloneClusters = new HashMap<Integer,MCCCluster>();
	}

	public void AddMethodandClone(int methodID, int SCID)
	{
		if(MethodToCloneStructure.containsKey(methodID))
		{
			if(MethodToCloneStructure.get(methodID).contains(SCID) == false )
			{
				MethodToCloneStructure.get(methodID).add(SCID);
			}
		}
		else
		{
			ArrayList<Integer> list = new ArrayList<Integer>();
			list.add(SCID);
			MethodToCloneStructure.put(methodID, list);
		}

		if(CloneToMethodStructure.containsKey(SCID))
		{
			if(CloneToMethodStructure.get(SCID).contains(methodID)== false)
			{
				CloneToMethodStructure.get(SCID).add(methodID);
			}
		}
		else
		{
			ArrayList<Integer> list = new ArrayList<Integer>();
			list.add(methodID);
			CloneToMethodStructure.put(SCID, list);
		}

	}
	public ArrayList<Integer> GetSCCsforMethod(int methodID)
	{
		if(MethodToCloneStructure.containsKey(methodID))
			return MethodToCloneStructure.get(methodID);
		
		return new ArrayList<Integer>();
	}
	public ArrayList<Integer> GetMethodsBySCCID(int SccID)
	{
		return CloneToMethodStructure.get(SccID);
	}
	private void AddSCCListAgainstMethod(int methodID, ArrayList<Integer> sccID)
	{
		for(int scc : sccID)
		{
			AddMethodandClone(methodID, scc/100);
		}
	}
	public void FillStructure(ArrayList<sMethod> methods, boolean writetoDB) 
	{

		for(sMethod mc : methods)
		{
			ArrayList<Integer> cloneIDs = mc.getCloneClasses();
			AddSCCListAgainstMethod(mc.getMethodId(),cloneIDs);
		}
		if(writetoDB)
		{
			WriteToDB();
		}
	}
	public void FillStructure(ArrayList<sMethod> methods) 
	{

		for(sMethod mc : methods)
		{
			ArrayList<Integer> cloneIDs = mc.getCloneClasses();
			for(int scc : cloneIDs)
			{
				AddMethodandClone(mc.getMethodId(), scc);
			}
		}
	}

	private void WriteToDB() 
	{
		try
		{
			@SuppressWarnings("deprecation")
			Mongo mongo = new Mongo("localhost", 27017);

			DB database = mongo.getDB("admin");
			if(database!=null)
			{
				DBCollection collection = database.getCollection("MethodToSccIndexing");
				for(Integer key : MethodToCloneStructure.keySet())
				{
					BasicDBObject document = new BasicDBObject();
					document.put("MethodID", key);
					document.put("SCCIDs", ToSimpleString(MethodToCloneStructure.get(key)));
					collection.insert(document);
				}

				DBCollection collection2 = database.getCollection("SccToMethodIndexing");
				for(Integer key : CloneToMethodStructure.keySet())
				{
					BasicDBObject document = new BasicDBObject();
					document.put("SCCID", key);
					document.put("MethodIDs", ToSimpleString2(CloneToMethodStructure.get(key)));
					collection2.insert(document);	
				}

			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	private String ToSimpleString(ArrayList<Integer> arrayList) {
		if(arrayList.size() <= 0)
			return "";
		String value = arrayList.get(0).toString();
		for(int i = 1 ;  i < arrayList.size() ; i++)
		{
			value = value +"," + arrayList.get(i);

		}
		return value;
	}
	private String ToSimpleString2(ArrayList<Integer> arrayList) {
		if(arrayList.size() <= 0)
			return "";
		String value = arrayList.get(0).toString();
		for(int i = 1 ;  i < arrayList.size() ; i++)
		{
			value = value +"," + arrayList.get(i).toString();

		}
		return value;
	}
	public void updateMethodsWithSimpleClones(ArrayList<SimpleClone> simpleClones)
	{
		try {
			for(SimpleClone sc : simpleClones ) 
			{
				for(SimpleCloneInstance sci : sc.getInstances())
				{
					if(sci.getMethodId() >= 0 && sMethod_List.MethodExist(sci.getMethodId()))
					{
						if(sMethod_List.getMethodWithId(sci.getMethodId()).getCloneClasses().contains(sc.getSSCId()) == false)
							sMethod_List.getMethodWithId(sci.getMethodId()).getCloneClasses().add(sc.getSSCId());  
					}
					else if(sci.getMethodId() >= 0)
					{
						sMethod method = new sMethod();
						method.setMethodId(sci.getMethodId());
						method.getCloneClasses().add(sc.getSSCId());
						sMethod_List.addsMethod(method);
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void FillFromDB() {


		try
		{
			@SuppressWarnings("deprecation")
			Mongo mongo = new Mongo("localhost", 27017);

			DB database = mongo.getDB("admin");
			if(database!=null)
			{
				sMethod_List.LoadFilelist();
				DBCollection collectiona = database.getCollection("MethodInfo");
				DBCursor cursor1 = collectiona.find();
				while (cursor1.hasNext()) {
						DBObject obj2 = cursor1.next();
						int methodID = Integer.parseInt(obj2.get("MethodId").toString());
						if(!sMethod_List.MethodExist(methodID))
							sMethod_List.addsMethod(new sMethod(obj2.get("MethodName").toString(),obj2.get("MethodSignature").toString(),methodID,Integer.parseInt(obj2.get("FileId").toString()),Integer.parseInt(obj2.get("StartToken").toString()),Integer.parseInt(obj2.get("EndToken").toString()),Integer.parseInt(obj2.get("TokenCount").toString())));   
				}
					
//				DBCollection collection = database.getCollection("MethodToSccIndexing");
//				DBCursor cursor = collection.find();
//				while (cursor.hasNext()) {
//					DBObject obj = cursor.next();
//					int MethodID = Integer.parseInt(obj.get("MethodID").toString());
//					String SCCids = obj.get("SCCIDs").toString();
//					String[] tokens = SCCids.split(",");
//					for(String token: tokens)
//					{
//						AddMethodandClone(MethodID, Integer.parseInt(token));
//					}
//				}
//				DBCollection collection2 = database.getCollection("MCClusterIndexing");
//				DBCursor cursor2 = collection2.find();
//				while (cursor2.hasNext()) {
//					DBObject obj = cursor2.next();
//					int clusterID = Integer.parseInt(obj.get("ClusterID").toString());
//					String SCCids = obj.get("SCCIDs").toString();
//					String methodIds = obj.get("MethodIDs").toString();
//					String[] tokens = SCCids.split(",");
//					ArrayList<Integer> sccIDs = new ArrayList<>();
//					String[] Methodtokens = methodIds.split(",");
//					ArrayList<Integer> MethodIDs = new ArrayList<>();
//					for(String token: tokens)
//					{
//						sccIDs.add(Integer.parseInt(token));
//					}
//					for(String method : Methodtokens)
//					{
//						MethodIDs.add(Integer.parseInt(method));
//					}
//					MCCCluster MCcluster = new MCCCluster();
//					MCcluster.MethodIDs = MethodIDs;
//					MCcluster.SCIDs = sccIDs;
//					MethodCloneClusters.put(clusterID, MCcluster);
//				}
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}

	public boolean ContainsMethod(Integer methodID) {
		// TODO Auto-generated method stub
		if(MethodToCloneStructure.containsKey(methodID))
			return true;
		return false;
	}

	@SuppressWarnings("deprecation")
	public void MethodCloneClusterStructure(ArrayList<MethodClones> methodClusters, boolean AddtoDB) {
		// TODO Auto-generated method stub
		for(MethodClones mc : methodClusters)
		{
			MCCCluster MCcluster = new MCCCluster();
			MCcluster.MethodIDs = new ArrayList<>();
			MCcluster.SCIDs = new ArrayList<>();
			for(Integer a: mc.getvCloneClasses())
			{
				MCcluster.SCIDs.add(a/100);	
			}
			for(sMarkedMethod method : mc.getvMarkedMethods())
			{
				MCcluster.MethodIDs.add(method.getMethodID());
			}
			MethodCloneClusters.put(mc.getClusterID(), MCcluster);
		}
		if(AddtoDB)
		{
			Mongo mongo;
			try {
				mongo = new Mongo("localhost", 27017);
			DB database = mongo.getDB("admin");
			if(database!=null)
			{
				DBCollection collection = database.getCollection("MCClusterIndexing");
				for(Integer key : MethodCloneClusters.keySet())
				{
					BasicDBObject document = new BasicDBObject();
					document.put("ClusterID", key);
					document.put("MethodIDs", ToSimpleString(MethodCloneClusters.get(key).MethodIDs));
					document.put("SCCIDs", ToSimpleString(MethodCloneClusters.get(key).SCIDs));
					collection.insert(document);
				}
			}
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public ArrayList<Integer> getMethodsBySCCIDs(ArrayList<Integer> vCloneClasses) {
		//all those methods which contains all of vcloneclasses//
		ArrayList<Integer> methodIDs = new ArrayList<>();
		for(Integer sccID : vCloneClasses)
		{
			if(CloneToMethodStructure.containsKey(sccID))
			{
				for(Integer methodID : CloneToMethodStructure.get(sccID))
				{
					if(MethodToCloneStructure.get(methodID).containsAll(vCloneClasses))
					{
						if(!methodIDs.contains(methodID) && methodID >= 0)
						{
							methodIDs.add(methodID);
						}
					}
				}
			}
		}
		return methodIDs;
	}

	public boolean IsInstanceAdditionCase(ArrayList<Integer> sccInMC) {
		for(Integer key : MethodCloneClusters.keySet())
		{
			if(MethodCloneClusters.get(key).SCIDs.containsAll(sccInMC) && MethodCloneClusters.get(key).SCIDs.size() == sccInMC.size())
			{
				return true;
			}
		}
		return false;
	}
	public Integer GetMCIDforSimpleCloneClasses(ArrayList<Integer> sccInMC) {
		for(Integer key : MethodCloneClusters.keySet())
		{
			if(MethodCloneClusters.get(key).SCIDs.containsAll(sccInMC) && MethodCloneClusters.get(key).SCIDs.size() == sccInMC.size())
			{
				return key;
			}
		}
		return -1;
	}
	public Integer GetMCIDforSCC(ArrayList<Integer> sccInMC) {
		return GetMCIDforSimpleCloneClasses(sccInMC);
	}

	public ArrayList<Integer> GetClusterMethodIDs(Integer mcID) {
		return MethodCloneClusters.get(mcID).MethodIDs;
	}

	public void RemoveMCClonesofMethods(ArrayList<Integer> updatedOrNewlyAddedFileMethods) {
		try {
			for(Integer mId : updatedOrNewlyAddedFileMethods)
			{
				for(Integer mcId : MethodCloneClusters.keySet())
				{
					if(MethodCloneClusters.get(mcId).MethodIDs.contains(mId))
					{
						MethodCloneClusters.get(mcId).MethodIDs.remove(mId);
						if(MethodCloneClusters.get(mcId).MethodIDs.size() <= 1)
						{
							MethodCloneClusters.remove(mcId);
						}
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void FillStructureIncremental(HashMap<Integer, ArrayList<Integer>> mid_to_scc,HashMap<Integer, ArrayList<Integer>> mcc_to_mid, ArrayList<SimpleClone> finalClones) 
	{
		try
		{
			FillFromDB();
			for(Integer mid : mid_to_scc.keySet())
			{
				//methodid
				for(Integer scc_id : mid_to_scc.get(mid))
					AddMethodandClone(mid,scc_id);
			}
			
			//create cluster data from mcc_to_mid
			
			
			for(Integer mcc_id : mcc_to_mid.keySet())
			{
				MCCCluster cluster =new MCCCluster();
				cluster.MethodIDs = mcc_to_mid.get(mcc_id);
				ArrayList<Integer> sccIds = SimpleClonesReader.GetSCCAgainstMCC(mcc_id);
//				for(Integer mid : mcc_to_mid.get(mcc_id))
//				{
//					if(MethodToCloneStructure.containsKey(mid))
//					{
//						for(int i : MethodToCloneStructure.get(mid))
//							if(!sccIds.contains(i))
//								sccIds.add(i);
//					}
//				}
				cluster.SCIDs = sccIds;
				MethodCloneClusters.put(mcc_id,cluster);
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
