package cmt.cddc.structures;

import java.util.ArrayList;

public final class sDir_List {
	private static ArrayList<sDir> dirList = new ArrayList<sDir>();

	public static ArrayList<sDir> getDirList() {
		return dirList;
	}

	public static void setDirList(ArrayList<sDir> dirList) {
		sDir_List.dirList = dirList;
	}
	
	public static sDir getDirAt(int index)
	{
		return dirList.get(index);
	}
	
	public static void addDir(sDir dir)
	{
		dirList.add(dir);
	}
	
	public static int getDirListSize()
	{
		return dirList.size();
	}
	public static void clearDirList()
	{
		dirList.clear();
	}
}
