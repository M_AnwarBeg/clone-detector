package cmt.cddc.structures;

import java.io.IOException;
import java.util.ArrayList;

import cmt.cddc.simpleclones.SimpleClonesReader;

public class sMethod {
    private String methodName;
    private String methodSig;   
	//string fileName;
    private int fileID;
    private int startToken;
    private int endToken;
    private ArrayList<Integer> cloneClasses = new ArrayList<Integer>(); 
    
    
	private int methodId;
	private int tokenCount;
	private String codeSegment;
	private String filePath;
    private boolean mark;
	
	public sMethod()
	{
	
	}
   
    
	public sMethod(String methodName,String methodSig, int methodId, int fileId, int startToken, int endToken, int tokenCount) throws IOException
	{
		this.methodName =methodName;
		this.methodSig = methodSig;
		this.methodId = methodId;
		this.fileID = fileId;
		this.startToken = startToken;
		this.endToken = endToken;
		this.tokenCount = tokenCount;
		this.cloneClasses = new ArrayList<>();
		codeSegment = "";
		filePath=SimpleClonesReader.getFilePath(fileId);
	}
    
	public sMethod(String methodName,int methodId, int fileId, int startToken, int endToken, int tokenCount) throws IOException
	{
		this.methodName =methodName;
		this.methodId = methodId;
		this.fileID = fileId;
		this.startToken = startToken;
		this.endToken = endToken;
		this.tokenCount = tokenCount;
		this.cloneClasses = new ArrayList<>();
		codeSegment = "";
		filePath=SimpleClonesReader.getFilePath(fileId);
		mark = false;
	}
	
	public void addMethodParameter(String param)
	{
		methodSig = methodSig + param;
	}
	public String getMethodSig() 
	{
		return methodSig;
	}

	public void setMethodSig(String methodParm) 
	{
		this.methodSig = methodParm;
	}
	
	public int getMethodId() {
		return methodId;
	}
	public void setMethodId(int methodId) {
		this.methodId = methodId;
	}
    
    
	public int getTokenCount() {
		return tokenCount;
	}
	public void setTokenCount(int tokenCount) {
		this.tokenCount = tokenCount;
	}
	
	
    
    public ArrayList<Integer> getCloneClasses() {
		return cloneClasses;
	}
    public int getCloneClassAt(int index)
    {
    	return this.cloneClasses.get(index);
    }
    public void addCloneClass(int cc)
    {
    	this.cloneClasses.add(cc);
    }
    public int getCloneClassSize()
    {
    	return this.cloneClasses.size();
    }
	public void setCloneClasses(ArrayList<Integer> cloneClasses) {
		this.cloneClasses = cloneClasses;
	}
	public String getMethodName() {
		return methodName;
	}
	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public int getFileID() {
		return fileID;
	}
	public void setFileID(int fileID) {
		this.fileID = fileID;
	}
	public int getStartToken() {
		return startToken;
	}
	public void setStartToken(int startToken) {
		this.startToken = startToken;
	}
	public int getEndToken() {
		return endToken;
	}
	public void setEndToken(int endToken) {
		this.endToken = endToken;
	}
	
	public void setSCCList(ArrayList<Integer> cloneClasses) {
		this.cloneClasses = cloneClasses;
	}
	
	  public String getCodeSegment() {
			return codeSegment;
	 }
		
	  public void setCodeSegment(String codeSegment) {
			this.codeSegment = codeSegment;
	}
	    
	  
	  public void displaySCCList() {
		/*	 TODO Auto-generated method stub
			for(int i=0;i<this._sccList.size();i++)
			{
				System.out.print(this._sccList.get(i) + " ");
			}
			System.out.println();
		*/}
	  
	  public String getFilePath() {
			return filePath;
		}
		public void setFilePath(String fileName) {
			this.filePath = fileName;
		}
		
		public void setMark(boolean mark) {
			this.mark = mark;
		}
		
		public boolean getMark() {
			return this.mark;
		}
		
		
	  
	
};