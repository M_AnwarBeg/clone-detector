package cmt.cddc.structures;

import java.util.ArrayList;

public final class sFile_List {
	static private ArrayList<sFile> files_list = new ArrayList<sFile>();

	public static ArrayList<sFile> getFilesList() {
		return files_list;
	}

	public static void setFilesList(ArrayList<sFile> vFile) {
		sFile_List.files_list = vFile;
	}

	public static int getFilesListSize()
	{
		return files_list.size();
	}
	public static sFile getsFileAt(int index)
	{
		return files_list.get(index);
	}
	
	public static sFile getFileWithId(int id)
	{
		for(int i = 0; i < getFilesList().size() ; i++)
		{
			if(getsFileAt(i).getFileID() == id)
				return getsFileAt(i);
		}
		return null;
	}
	
	public static void addsFile(sFile file)
	{
		files_list.add(file);
	}
	
	public static void clearFilesList()
	{
		files_list.clear();
	}
	
	public static sFile getFile(int fileId) {
		for(sFile file : files_list) {
			if(file.getFileID() == fileId)
				return file;
		}
		return null;
	}
	public static int getMaxFileId()
	{
		int maxFileId = -1;
		for(int i = 0 ; i < sFile_List.getFilesListSize(); i++)
		{
			if(maxFileId < sFile_List.getsFileAt(i).getFileID())
				maxFileId = sFile_List.getsFileAt(i).getFileID();
		}
		return maxFileId;
	}
	public static int getFileIdAgainstFileName(String filename)
	{
		sFile fileobject = null;
    	for (sFile object: sFile_List.getFilesList()) 
    	{
    		 if(object.getFileName() == filename)
    		 {
    		    fileobject=object;
    		    break;
    		  }
    	}
    	return fileobject.getFileID();    	
	}
	public static void removeFileById(int fileId)
	{
		for(int i = 0 ; i < sFile_List.getFilesListSize() ; i++)
		{
			if(sFile_List.getsFileAt(i).getFileID() == fileId)
			{
				sFile_List.getFilesList().remove(i);
				i--;
			}
		}
	}
}
