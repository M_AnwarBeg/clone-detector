package cmt.cddc.structures;

import java.util.ArrayList;

public class sDir {
	private String strPath;
	private String dirName;
    private int nFiles;
    private ArrayList<Integer> containedFiles = new ArrayList<Integer>();
    private ArrayList<Integer> fileCloneClasses = new ArrayList<Integer>();
    
	public String getStrPath() {
		return strPath;
	}
	public void setStrPath(String strPath) {
		this.strPath = strPath;
	}
	public int getnFiles() {
		return nFiles;
	}
	public void setnFiles(int nFiles) {
		this.nFiles = nFiles;
	}
	public ArrayList<Integer> getContainedFiles() {
		return containedFiles;
	}
	public void setContainedFiles(ArrayList<Integer> containedFiles) {
		this.containedFiles = containedFiles;
	}
	public ArrayList<Integer> getFileCloneClasses() {
		return fileCloneClasses;
	}
	public int getFileCloneClassAt(int index)
	{
		return this.fileCloneClasses.get(index);
	}
	public int getFileCloneClassSize()
	{
		return fileCloneClasses.size();
	}
	public void setFileCloneClasses(ArrayList<Integer> fileCloneClasses) {
		this.fileCloneClasses = fileCloneClasses;
	}  
	public void addContainedFile(int fileId)
	{
		this.containedFiles.add(fileId);
	}
	public void addFileCloneClass(int cloneId)
	{
		this.fileCloneClasses.add(cloneId);
	}
	public String getDirName() {
		return dirName;
	}
	public void setDirName(String dirName) {
		this.dirName = dirName;
	}
}
