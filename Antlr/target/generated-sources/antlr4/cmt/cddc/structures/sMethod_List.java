package cmt.cddc.structures;

import java.util.ArrayList;

import cmt.cddc.simpleclones.SimpleClonesReader;

public final class sMethod_List {
	static private ArrayList<sMethod> methods_list = new ArrayList<sMethod>();

	public static ArrayList<sMethod> getMethodsList() {
		return methods_list;
	}

	public static void setMethodList(ArrayList<sMethod> vMethod) {
		sMethod_List.methods_list = vMethod;
	}
	
	public static int getMethodListSize()
	{
		return methods_list.size();
	}
	
	public static void addsMethod(sMethod method)
	{
		methods_list.add(method);
	}
	
	public static sMethod getsMethodAt(int index)
	{
		return methods_list.get(index);
	}
	public static void clearMethodsList()
	{
		methods_list.clear();
	}
	
	public static sMethod getMethodWithId(int id)
	{
		for(int i = 0; i < getMethodListSize() ; i++)
		{
			if(getsMethodAt(i).getMethodId() == id)
				return getsMethodAt(i);
		}
		return null;
	}
	public static boolean MethodExist(int methodID)
	{
		for(int i = 0 ; i <methods_list.size(); i++)
		{
			if(methods_list.get(i).getMethodId() == methodID)
			{
				return true;
			}
		}
		return false;
	}
	public static void LoadFilelist() {
		// TODO Auto-generated method stub
		SimpleClonesReader.LoadFilelist();
	}
}
