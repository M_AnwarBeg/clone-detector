package cmt.cddc.structures;

import java.util.ArrayList;

public class sClass {
	    private String className;
	    private String fileName;
	    private int startToken;
	    private int endToken;
	    private int nCurly;
	    private ArrayList<Integer> cloneClasses= new ArrayList<Integer>();
	    
		public String getClassName() {
			return className;
		}
		public void setClassName(String className) {
			this.className = className;
		}
		public String getFileName() {
			return fileName;
		}
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}
		public int getStartToken() {
			return startToken;
		}
		public void setStartToken(int startToken) {
			this.startToken = startToken;
		}
		public int getEndToken() {
			return endToken;
		}
		public void setEndToken(int endToken) {
			this.endToken = endToken;
		}
		public int getnCurly() {
			return nCurly;
		}
		public void setnCurly(int nCurly) {
			this.nCurly = nCurly;
		}
		public ArrayList<Integer> getCloneClasses() {
			return cloneClasses;
		}
		public void setCloneClasses(ArrayList<Integer> cloneClasses) {
			this.cloneClasses = cloneClasses;
		}
}
