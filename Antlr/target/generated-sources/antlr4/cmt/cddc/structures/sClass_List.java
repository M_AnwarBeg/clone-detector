package cmt.cddc.structures;

import java.util.ArrayList;

public final class sClass_List {
	static private ArrayList<sClass> classes_list = new ArrayList<sClass>();

	public static ArrayList<sClass> getClassesList() {
		return classes_list;
	}

	public static void setClassesList(ArrayList<sClass> vClass) {
		sClass_List.classes_list = vClass;
	} 
	
	public static int getClassListSize()
	{
		return classes_list.size();
	}
	
	public static void addsClass(sClass sclass)
	{
		classes_list.add(sclass);
	}
	
	public static sClass getsClassAt(int index)
	{
		return classes_list.get(index);
	}
	
	public static void clearClassList()
	{
		classes_list.clear();
	}
}
