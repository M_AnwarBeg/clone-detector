package cmt.cddc.structures;

import java.util.ArrayList;

public class sGroup_List {
	private static ArrayList<sGroup> groupList = new ArrayList<sGroup>();

	public static ArrayList<sGroup> getGroupList() {
		return groupList;
	}

	public static void setGroupList(ArrayList<sGroup> groupList) {
		sGroup_List.groupList = groupList;
	}
	
	public static int getGroupListSize()
	{
		return groupList.size();
	}
	
	public static void addsGroup(sGroup group) 
	{
		groupList.add(group);
	}
	
	public static sGroup getGroupAt(int index)
	{
		return groupList.get(index);
	}
	
	public static void clearGroupList()
	{
		groupList.clear();
	}
}
