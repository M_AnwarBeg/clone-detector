package cmt.cddc.fileclones;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.common.CommonUtility;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.Utilities;
import cmt.cddc.structures.sDir_List;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sGroup_List;
import cmt.common.Directories;

public final class FccWriter 
{
	public static void writeFileClusters(ArrayList<FileCluster> fileClusters , boolean prunedClusters)
	{
		if(prunedClusters)
			writeFileClusters(Directories.getAbsolutePath(Directories.FILE_CLUSTER_XX) , true , fileClusters);
		else			
			writeFileClusters(Directories.getAbsolutePath(Directories.FILE_CLUSTER), false , fileClusters);		
	}
	
	private static void writeFileClusters(String path , boolean bSignificantOnly , ArrayList<FileCluster> fileClusters)
	{		
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter(path, "UTF-8");
			if(fileClusters != null && fileClusters.size() > 0)
			{
				for(FileCluster cluster : fileClusters)
				{
					if(bSignificantOnly && !cluster.isbSignificant())
						continue;
					
					writer.println(cluster.getClusterID() + ";" + cluster.getvFileSize());
					for(int cc: cluster.getvCloneClasses())
						writer.print(cc/100 + ",");
					
					writer.println();
					for(int i=0 ; i< cluster.getvFileSize() ; i++)
					{
						writer.print(cluster.getVFileAt(i) + ";" + cluster.getvFileTokenAt(i) + ",");
						writer.printf("%.2f", cluster.getvFileCoverageAt(i));
						writer.println();
					}
					writer.println();
				}
			}
			writer.close();
		} 
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 	    	
	}
	
	public static void writeFileClonesByDirs()
	{
		PrintWriter writer_file;
	  	PrintWriter writer_normalfile;
	  	try 
		{
	  		 writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_DIR), "UTF-8");
			 writer_normalfile = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_DIR_NORMAL) , "UTF-8");
			 for (int i = 0; i < sDir_List.getDirListSize(); i++) 
			 {
				 for (int j = 0; j < sDir_List.getDirAt(i).getFileCloneClassSize(); j++) 
				 {
					 writer_file.print(sDir_List.getDirAt(i).getFileCloneClassAt(j)/100 + ",");
					 writer_normalfile.print(sDir_List.getDirAt(i).getFileCloneClassAt(j) + " ");
				 }
				 writer_file.println();
				 writer_normalfile.println();
			 }
			 writer_file.close();
			 writer_normalfile.close();
		}
	  	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	         e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }

	//write cross-dir file clone structures
	//two versions of output
	public static void writeCrossDirsFileCloneStructures(ArrayList<FileCluster> fileClusters)
	{
		PrintWriter writer_file;
		try 
		{
			writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_DIR_EX), "UTF-8");						
			ArrayList<String> fcsAcrossDirs = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_DIR));
			//for every structure
			
			for (int i = 0, validFCS = 0; i < fcsAcrossDirs.size(); i++) 
		    {
		    	    String [] line_parts = fcsAcrossDirs.get(i).split("#");
				    if(line_parts.length < 1)
					   continue;
				
					String [] temp = line_parts[0].split(" ");
					if(temp.length <= 0)
						continue;
					
					ArrayList<String> fcc_temp = new ArrayList<String>();
					Collections.addAll(fcc_temp, temp);
					
					String [] support_temp = line_parts[1].split(" ");
					int support = Integer.parseInt(support_temp[1]);
					
					if(support <= 1)
					   continue;
					
					//sort the list because the result is not ordered
					ArrayList<Integer> vFileCloneClasses = CommonUtility.convertStrArrListToInt(fcc_temp , false);
					Collections.sort(vFileCloneClasses);
					
			        //take the first file clone class
			        int FCC = vFileCloneClasses.get(0) / 100;
			        
			        //find all dirs and groups that have this file clone class
			        //use list for ease of deletion in middle
			        List<Integer> iTempDirs = new ArrayList<Integer>();
			        List<Integer> iTempGroups = new ArrayList<Integer>();
			        
			        for(FileCluster cluster : fileClusters)
			        {
			        	if (cluster.getClusterID() == FCC) 
			        	{
			        		 for (int j = 0; j < cluster.getvFileSize(); j++) 
			        		 {
			                     int fileID = cluster.getVFileAt(j);
			                     int dirID = sFile_List.getsFileAt(fileID).getDirID();
			                     int groupID = sFile_List.getsFileAt(fileID).getGroupID();
			                     iTempDirs.add(dirID);
			                     iTempGroups.add(groupID);
			                 }
			                 break;
			        	}
			        }
			        
			        //remove adjacent same dir IDs& group IDs
				    Set<Integer> hs_dir = new HashSet<Integer>(iTempDirs);
				    Set<Integer> hs_group = new HashSet<Integer>(iTempGroups);
				    iTempDirs.clear();
				    iTempGroups.clear();
				    iTempDirs.addAll(hs_dir);	
				    iTempGroups.addAll(hs_group);
				    
				    //sort the dirs & groups
				    Collections.sort(iTempDirs);
				    Collections.sort(iTempGroups);
				    
				    //for each dir in temporary list
				    for (int id = 0 ; id < iTempDirs.size(); id++) 
			        {
				    	int dirId = iTempDirs.get(id);
				    	
				    	//if the dir does not have all members of this structure		                
			        	if(!(sDir_List.getDirAt(dirId).getFileCloneClasses().containsAll(vFileCloneClasses)))
			            {
			        		iTempDirs.remove(id);  //remove this dir from temp list
			        		id--;
			        	}
			        }	
				    //for each group in temporary list
				    for (int id = 0 ; id < iTempGroups.size(); id++) 
			        {
				    	int groupId = iTempGroups.get(id);
				    	
				    	//if the group does not have all members of this structure		                
			        	if(!(sGroup_List.getGroupAt(groupId).getvFileCloneClasses().containsAll(vFileCloneClasses)))
			            {
			        		iTempGroups.remove(id);  //remove this group from temp list
			        		id--;
			        	}
			        }
				  //group check here
			      if (SysConfig.getGroupCheck() == 2) 
			      {
			          //we have to make sure that each file clone structure
			          //is present across groups
			          //for this, just check that lTempGroups has atleast 2 elements
			          if (iTempGroups.size() < 2)
			               continue;
			      }
			      
			      //now we can safely print this structure
			      writer_file.print(validFCS++ + " ");
			      writer_file.println(support);
			      
			      //print the full structure
			      for (int j = 0; j < vFileCloneClasses.size(); j++) 
			      {
			    	  writer_file.print(vFileCloneClasses.get(j)/100 + ",");
			      }
			      writer_file.println();	
			      
			      for (int id = 0 ; id < iTempDirs.size(); id++) 
			      {
	                     int dirID = iTempDirs.get(id);
	                     writer_file.println();
	                     writer_file.println(dirID);
	                     
	                     //for each file clone class in this structure
	                     for (int n = 0; n < vFileCloneClasses.size(); n++) 
	                     {
	                    	 if(vFileCloneClasses.size() > 1)
	                    	 {
		                         //if it is the same file clone class...skip
		                         //if (n > 0 && vFileCloneClasses.get(n) / 100 == vFileCloneClasses.get(n - 1) / 100) {
		                         //    continue;
		                         //}
	                    	 }
	                         int FCCId = vFileCloneClasses.get(n) / 100;
	                         
	                         //find the clone file IDs
	                         String towrite = "";
	                         for(FileCluster cluster : fileClusters)
	                         {
	                        	 if (cluster.getClusterID() == FCCId) 
	                        	 {
	                                 for (int j = 0; j < cluster.getvFileSize(); j++) 
	                                 {
	                                     int fileID = cluster.getVFileAt(j);
	                                     if (sFile_List.getsFileAt(fileID).getDirID() == dirID) 
	                                     {
	                                    	 writer_file.print(fileID + ",");
	                                     }	                                                                            
	                                 }
	                             }
	                         }
	                         writer_file.println();
	                     }
			      }
			      writer_file.println("***********************************************");
		    }
			writer_file.close();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    public static void writeFileClonesByGroups()
    {
    	PrintWriter writer_file;
	  	PrintWriter writer_normalfile;
	  	try 
		{
	  		writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_GROUP), "UTF-8");
			writer_normalfile = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_GROUP_NORMAL), "UTF-8");
			 
			 for (int i = 0; i < sGroup_List.getGroupListSize(); i++) 
			 {
			        for (int j = 0; j < sGroup_List.getGroupAt(i).getvFileCloneClassSize(); j++) 
			        {
			        	writer_file.print(sGroup_List.getGroupAt(i).getvFileCloneClassAt(j)/100 + ",");
			        	writer_normalfile.print(sGroup_List.getGroupAt(i).getvFileCloneClassAt(j) + " ");			            
			        }
			        writer_file.println();
					writer_normalfile.println();
			 }
	  		writer_file.close();
			writer_normalfile.close();
		}
	  	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	         e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  		
    }

    //write cross group clone file structures
    //two versions of output
    public static void writeCrossGroupsFileCloneStructures(ArrayList<FileCluster> fileClusters)
    {
    	PrintWriter writer_file;
		try 
		{
			writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_GROUP_EX), "UTF-8");						
			ArrayList<String> fcsAcrossGroups = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_GROUP));
			//for every structure
			for (int i = 0; i < fcsAcrossGroups.size(); i++) 
			{				
				String [] line_parts = fcsAcrossGroups.get(i).split("#");
			    if(line_parts.length < 1)
				   continue;
			
				String [] temp = line_parts[0].split(" ");
				if(temp.length <= 0)
					continue;
				
				ArrayList<String> fcc_temp = new ArrayList<String>();
				Collections.addAll(fcc_temp, temp);
				
				String [] support_temp = line_parts[1].split(" ");
				int support = Integer.parseInt(support_temp[1]);
				
				if(support <= 1)
				   continue;
				
				writer_file.println(i + " " + support); 
				
				//sort the list because the result is not ordered
				ArrayList<Integer> vFileCloneClasses = CommonUtility.convertStrArrListToInt(fcc_temp , false);
				Collections.sort(vFileCloneClasses);
				
				//take the first clone file class
		        int CFC = vFileCloneClasses.get(0) / 100;
		        
		        //print the full structure
		        for (int j = 0; j < vFileCloneClasses.size(); j++) {
		        	writer_file.print(vFileCloneClasses.get(j) / 100 + ",");
		        }
		        writer_file.println();
		        
		        //find all groups that have this clone file class
		        //use list for ease of deletion in middle
		        List<Integer> iTempGroups = new ArrayList<Integer>();
		        
		        for (FileCluster cluster : fileClusters)
		        {
		            if (cluster.getClusterID() == CFC) 
		            {
		                for (int j = 0; j < cluster.getvFileSize(); j++) 
		                {
		                    int fileID = cluster.getVFileAt(j); 
		                    int groupID = sFile_List.getsFileAt(fileID).getGroupID(); 
		                    iTempGroups.add(groupID);		                    
		                }
		                break;
		            }
		        }
		        //remove adjacent same group IDs
			    Set<Integer> hs_group = new HashSet<Integer>(iTempGroups);
			    iTempGroups.clear();	
			    iTempGroups.addAll(hs_group);
			    
			    //sort the dirs & groups
			    Collections.sort(iTempGroups);
			    
			  //for each group in temporary list
			    for(int id = 0 ; id < iTempGroups.size() ; id++)
			    {
			    	int groupID = iTempGroups.get(id);
			    	//if the group does not have all members of this structure
			    	if(!(sGroup_List.getGroupAt(groupID).getvFileCloneClasses().containsAll(vFileCloneClasses)))
			    	{
			    		iTempGroups.remove(id);  //remove this group from temp list
			    		id--;
			    	}
			    }
			    
			    for(int id = 0 ; id < iTempGroups.size() ; id++)
			    {
			    	int groupID = iTempGroups.get(id);
			    	writer_file.println();
			    	writer_file.println(groupID);
			    	
			    	//for each clone file class in this structure
		            for (int n = 0; n < vFileCloneClasses.size(); n++) 
		            {
		            	if(vFileCloneClasses.size() > 1)
                   	    {
			                //if it is the same file clone class...skip
			                if (n > 0 && vFileCloneClasses.get(n) / 100 == vFileCloneClasses.get(n - 1) / 100) 
			                    continue;
                   	    }  
		                int CFCId = vFileCloneClasses.get(n) / 100;
		                //find the clone file IDs
		                for (FileCluster cluster : fileClusters) 
		                {
		                    if (cluster.getClusterID() == CFCId) 
		                    {
		                        for (int j = 0; j < cluster.getvFileSize(); j++) 
		                        {
		                            int fileID = cluster.getVFileAt(j);
		                            if (sFile_List.getsFileAt(fileID).getGroupID() == groupID) 
		                            {
		                            	writer_file.print(fileID + ",");		                               
		                            }
		                        }
		                    }
		                }
		                writer_file.println();
		            }
			    }
			    writer_file.println("***********************************************");
			}
			writer_file.close();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    //write in-dir clone-file structures
    /*public static void writeInDirsFileCloneStructures()  
    {
    	PrintWriter writer_file;
    	try 
		{
	  		writer_file = new PrintWriter("output\\InDirsCloneFileStructures.txt", "UTF-8");
	  		for (int f = 0; f < sDir_List.getDirListSize(); f++) 
	  		{
	  	        //step 1: find the MAX_REPEAT
	  	        int MAX_REPEAT = 0;
	  	        int temp = 1;
	  	        if (sDir_List.getDirAt(f).getFileCloneClassSize() == 0)   //FileCloneStructures = File Clusters
	  	        {
	  	        	writer_file.println();
	  	            continue;
	  	        }
	  	        for (int i = 0; i < sDir_List.getDirAt(f).getFileCloneClassSize() - 1; i++) 
	  	        {
	              if (sDir_List.getDirAt(f).getFileCloneClassSize() > i)//required???
	              {
	                  if (sDir_List.getDirAt(f).getFileCloneClassAt(i) / 100 == sDir_List.getDirAt(f).getFileCloneClassAt(i+1)/100)
	                  {
	                      temp++;
	                  }
	                  else 
	                  {
	                      if (temp > MAX_REPEAT) 
	                          MAX_REPEAT = temp;
	                      
	                      temp = 1;
	                  }
	              }
	              if (temp > MAX_REPEAT)
	                  MAX_REPEAT = temp;	              
	           }
	  	       //step 2: find 
	  	       if (MAX_REPEAT > 1) 
	  	       {
	  	            ArrayList<ArrayList<Integer>> vInDirsCloneFileStructures = null;
	  	            vInDirsCloneFileStructures = Utilities.initializeArrayList(vInDirsCloneFileStructures,MAX_REPEAT);
	  	            for (int i = 1; i < MAX_REPEAT; i++) 
	  	            {
	  	                //dont print yet
	  	                for (int j = 0; j < sDir_List.getDirAt(f).getFileCloneClassSize(); j++) 
	  	                {
	  	                    if (sDir_List.getDirAt(f).getFileCloneClassSize() > j + i) 
	  	                    {
	  	                        if (sDir_List.getDirAt(f).getFileCloneClassAt(j)/100 == sDir_List.getDirAt(f).getFileCloneClassAt(j+i)/100) 
	  	                        {
	  	                            //dont print yet...store first
	  	                            vInDirsCloneFileStructures.get(i).add(sDir_List.getDirAt(f).getFileCloneClassAt(j)/ 100);
	  	                            j = j + i;
	  	                        }
	  	                    }
	  	                }
	  	            }
	  	            //k should not exceed the limit...   
	  	            for (int k = 0; k < vInDirsCloneFileStructures.size() - 1; k++) 
	  	            {
	  	                if (vInDirsCloneFileStructures.get(k) == vInDirsCloneFileStructures.get(k+1)) 	  	                
	  	                    continue;	  	                
	  	                else 
	  	                {
	  	                    if (vInDirsCloneFileStructures.get(k).size() > 0) 
	  	                    {
	  	                    	writer_file.print("(" + (k+1)+ ")");
	  	                        for (int m = 0; m < vInDirsCloneFileStructures.get(k).size(); m++) 
	  	                        {
	  	                        	writer_file.print(vInDirsCloneFileStructures.get(k).get(m) + ",");
	  	                        }
	  	                    }
	  	                }
	  	            }
	  	            //print the final one now
	  	            int n = vInDirsCloneFileStructures.size() - 1;
	  	            if (vInDirsCloneFileStructures.get(n).size() > 0) 
	  	            {
	  	            	writer_file.print("(" + (n+1) + ")");
	  	                for (int p = 0; p < vInDirsCloneFileStructures.get(n).size(); p++)
	  	                {
	  	                	writer_file.print(vInDirsCloneFileStructures.get(n).get(p) + ",");
	  	                }
	  	            }
	  	        }
	  	        writer_file.println();
	  	    }
	  		writer_file.close();
		}
	  	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	         e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }*/

    public static void writeInDirsFileCloneStructures()  
    {
    	PrintWriter writer_file;
    	try 
		{
	  		writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.IN_DIR_STRUCTURE), "UTF-8");
	  		for (int d = 0; d < sDir_List.getDirListSize(); d++) 
	  		{
	  			ArrayList<ArrayList<Integer>> repeats = new ArrayList<ArrayList<Integer>>();
				repeats.add(new ArrayList<Integer>());
				int repeatCount = 1;
				for(int c = 0 ; c < sDir_List.getDirAt(d).getFileCloneClassSize() - 1; c++)
				{
					 if(sDir_List.getDirAt(d).getFileCloneClassAt(c) / 100 == sDir_List.getDirAt(d).getFileCloneClassAt(c+1)/100)
					 {
						 repeatCount++;
						 if(repeats.size() <= repeatCount)
							 repeats.add(new ArrayList<Integer>());	 
						 
						 repeats.get(repeatCount-1).add(sDir_List.getDirAt(d).getFileCloneClassAt(c)/100);
					 }
					 else
						 repeatCount = 1;
				 }
				for(int i = 0 ; i< repeats.size() ; i++)
				{
					 if(repeats.get(i).size() <=0)
						 continue;
					 writer_file.print("(" + (i+1)+ ")");
					 
					 for(int j=0; j< repeats.get(i).size() ; j++)
					 {
						 writer_file.print(repeats.get(i).get(j) + ",");
					 }
				 }	 				
				writer_file.println();
	  		}
	  		writer_file.close();
		}
	  	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	         e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }
    
    //write in-group clone-file structures
    /*public static void writeInGroupsFileCloneStructures()
    {
    	PrintWriter writer_file;
    	try 
		{
	  		 writer_file = new PrintWriter("output\\InGroupCloneFileStructures.txt", "UTF-8");
	  		 for (int f = 0; f < sGroup_List.getGroupListSize(); f++) 
	  		 {
	  	        //step 1: find the MAX_REPEAT
	  	        int MAX_REPEAT = 0;
	  	        int temp = 1;
	  	        if (sGroup_List.getGroupAt(f).getvFileCloneClassSize() == 0) 
	  	        {
	  	        	writer_file.println();
	  	            continue;
	  	        }
	  	        for (int i = 0; i < sGroup_List.getGroupAt(f).getvFileCloneClassSize() - 1; i++) 
	  	        {
	              if (sGroup_List.getGroupAt(f).getvFileCloneClassSize() > i)//required???
	              {
	                  if (sGroup_List.getGroupAt(f).getvFileCloneClassAt(i) / 100 == sGroup_List.getGroupAt(f).getvFileCloneClassAt(i+1) / 100) 
	                  {
	                      temp++;
	                  }
	                  else 
	                  {
	                      if (temp > MAX_REPEAT) 
	                      {
	                          MAX_REPEAT = temp;
	                      }
	                      temp = 1;
	                  }
	              }
	              if (temp > MAX_REPEAT) 
	              {
	                  MAX_REPEAT = temp;
	              }
	          }
	  	      //step 2: find 
	  	      if (MAX_REPEAT > 1) 
	  	      {
	  	    	ArrayList<ArrayList<Integer>> vInGroupsCloneFileStructures = null;	
	  	    	vInGroupsCloneFileStructures = Utilities.initializeArrayList(vInGroupsCloneFileStructures, MAX_REPEAT);
	  	            for (int i = 1; i < MAX_REPEAT; i++) 
	  	            {
	  	                //dont print yet
	  	                for (int j = 0; j < sGroup_List.getGroupAt(f).getvFileCloneClassSize(); j++) 
	  	                {
	  	                    if (sGroup_List.getGroupAt(f).getvFileCloneClassSize() > j + i) 
	  	                    {
	  	                        if (sGroup_List.getGroupAt(f).getvFileCloneClassAt(j)/100 == sGroup_List.getGroupAt(f).getvFileCloneClassAt(j+i)/100) 
	  	                        {
	  	                            //dont print yet...store first
	  	                        	vInGroupsCloneFileStructures.get(i).add(sGroup_List.getGroupAt(f).getvFileCloneClassAt(j)/ 100);	  	                            
	  	                            j = j + i;
	  	                        }
	  	                    }
	  	                }
	  	            }
	  	            //k should not exceed the limit...   
	  	            for (int k = 0; k < vInGroupsCloneFileStructures.size() - 1; k++) 
	  	            {
	  	                if (vInGroupsCloneFileStructures.get(k) == vInGroupsCloneFileStructures.get(k + 1)) 	  	                
	  	                    continue;	  	                
	  	                else 
	  	                {
	  	                    if (vInGroupsCloneFileStructures.get(k).size() > 0) 
	  	                    {
	  	                    	writer_file.print("(" + (k + 1) + ")");
	  	                        for (int m = 0; m < vInGroupsCloneFileStructures.get(k).size(); m++) 
	  	                        {	  	                        	
	  	                        	writer_file.print(vInGroupsCloneFileStructures.get(k).get(m) + ",");	  	                            
	  	                        }
	  	                    }
	  	                }
	  	            }
	  	            //print the final one now
	  	            int n = vInGroupsCloneFileStructures.size() - 1;
	  	            if (vInGroupsCloneFileStructures.get(n).size() > 0) 
	  	            {
	  	            	writer_file.print("(" + (n+1) + ")");
	  	                for (int p = 0; p < vInGroupsCloneFileStructures.get(n).size(); p++) 
	  	                {
	  	                	writer_file.print(vInGroupsCloneFileStructures.get(n).get(p) + ",");
	  	                }
	  	            }
	  	     }
	  	     writer_file.println();
	       }
 		   writer_file.close();
		}
	 	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	        e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }*/
    
    public static void writeInGroupsFileCloneStructures()
    {
    	PrintWriter writer_file;
    	try 
		{
	  		 writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.IN_GROUP_STRUCTURE), "UTF-8");
	  		 for (int g = 0; g < sGroup_List.getGroupListSize(); g++) 
	  		 {
	  			ArrayList<ArrayList<Integer>> repeats = new ArrayList<ArrayList<Integer>>();
				repeats.add(new ArrayList<Integer>());
				int repeatCount = 1;
				for(int c = 0 ; c < sGroup_List.getGroupAt(g).getvFileCloneClassSize() - 1; c++)
				{
					 if (sGroup_List.getGroupAt(g).getvFileCloneClassAt(c) / 100 == sGroup_List.getGroupAt(g).getvFileCloneClassAt(c+1) / 100) 
					 {
						 repeatCount++;
						 if(repeats.size() <= repeatCount)
							 repeats.add(new ArrayList<Integer>());	 
						 
						 repeats.get(repeatCount-1).add(sGroup_List.getGroupAt(g).getvFileCloneClassAt(c)/100);
					 }
					 else
						 repeatCount = 1;
				 }
				for(int i = 0 ; i< repeats.size() ; i++)
				{
					 if(repeats.get(i).size() <=0)
						 continue;
					 writer_file.print("(" + (i+1)+ ")");
					 
					 for(int j=0; j< repeats.get(i).size() ; j++)
					 {
						 writer_file.print(repeats.get(i).get(j) + ",");
					 }
				 }	 				
				writer_file.println();
	  		 }
	  		 
		   writer_file.close();
		}
	 	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	        e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
     }

    public static void writeFileStructures(ArrayList<SimpleClone> simpleClones)
    {
    	PrintWriter writer_file;
    	try 
		{
	    		int structureCount=0;
		  		writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.FILE_STRUCTURES), "UTF-8");
		  		ArrayList<ArrayList<Integer>> crossPatterns = new ArrayList<ArrayList<Integer>>();
		  		ArrayList<String> crossFileStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_FILE_CLONE_STRUCTURE));
		  		ArrayList<String> clonesByFile = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CLONES_BY_FILE_NORMAL));
		  		ArrayList<String> inFileStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_STRUCTURES));
				if(crossFileStructures != null && crossFileStructures.size() > 0)
				{
					//for every structure
					for(int i = 0 ; i < crossFileStructures.size() ; i++)
					{
				        String [] line_parts = crossFileStructures.get(i).split("#");
						if(line_parts.length <= 1)
							continue;
	
						String [] support_temp = line_parts[1].split(" ");
						int support = Integer.parseInt(support_temp[1]);  //the support is the count of occurrences
						
						if(support <= 1)
							continue;
						
						String [] temp = line_parts[0].split(" ");
						if(temp.length <= 0)
							continue;	
						
						ArrayList<String> crossFilePattern_temp = new ArrayList<String>();
						Collections.addAll(crossFilePattern_temp, temp);	
						ArrayList<Integer> crossFilePattern = CommonUtility.convertStrArrListToInt(crossFilePattern_temp , true);
						Collections.sort(crossFilePattern);
						crossPatterns.add(crossFilePattern);
						ArrayList<Integer> filesContainingCrossFilePattern = new ArrayList<Integer>();
						
						for(int j =0 ; j < clonesByFile.size() ; j++)
						{
							String [] clonesList = clonesByFile.get(j).split(" ");
							ArrayList<String> clonesList_temp = new ArrayList<String>();
							Collections.addAll(clonesList_temp, clonesList);	
							ArrayList<Integer> clonesByFileList = CommonUtility.convertStrArrListToInt(clonesList_temp , true);
							boolean isFirstMatch = true;
							
							while(true)
							{		
								boolean patternMatched = true;
								for(int z=0 ; z < crossFilePattern.size() ; z++)
								{					
									int element = crossFilePattern.get(z);
									if(Collections.frequency(crossFilePattern, element) > Collections.frequency(clonesByFileList , element))
									{
										patternMatched = false;	
										break;
									}
								}
								
								if(patternMatched)
								{
									if(isFirstMatch)
										isFirstMatch=false;
									else
										support++;
									
									filesContainingCrossFilePattern.add(j);
									for(int k =0 ; k < crossFilePattern.size() ; k++)
										clonesByFileList.remove(crossFilePattern.get(k));
								}
								else
									break;
							}
						}
						writer_file.println(structureCount + " " + support);
					    
						fileStructWriter_HelperMethod(writer_file , crossFilePattern , filesContainingCrossFilePattern,simpleClones);
						writer_file.println();
						structureCount++;
					}										
			    }
			    for(int x=0 ; x < inFileStructures.size() ; x++)
				{
					String [] infileStruc = inFileStructures.get(x).split("\\(");
					for(int y=0;y < infileStruc.length ; y++)
					{
						boolean repeatingPattern = false;
						if(infileStruc[y].isEmpty() == false && infileStruc[y] != "" && infileStruc[y] != null)
						{
							String [] p = infileStruc[y].split("\\)");
							int totalOccurenceCount = Integer.parseInt(p[0]);
							String [] temp = p[1].split(",");
							ArrayList<String> inFilePattern_temp = new ArrayList<String>();
							Collections.addAll(inFilePattern_temp, temp);	
							ArrayList<Integer> inFilePattern = CommonUtility.convertStrArrListToInt(inFilePattern_temp , false);
							
							for(int c = 0 ;c < crossPatterns.size() ; c++)
							{								
								if(crossPatterns.get(c).equals(inFilePattern))
								{
									repeatingPattern= true;
									break;
								}
							}
							if(!repeatingPattern)
							{
								ArrayList<Integer> fileContainingInFilePattern = new ArrayList<Integer>();
								for(int c=0 ; c < totalOccurenceCount ; c++)									
									fileContainingInFilePattern.add(x);
								writer_file.println(structureCount + " " + totalOccurenceCount);							
								fileStructWriter_HelperMethod(writer_file ,inFilePattern, fileContainingInFilePattern,simpleClones);
								writer_file.println();
								structureCount++;
							}
						}
					}				
				}			
	  		    writer_file.close();		
		}
	 	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	        e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }
    
	private static void fileStructWriter_HelperMethod(PrintWriter writer_file , ArrayList<Integer> pattern , ArrayList<Integer> filesContainingPattern,ArrayList<SimpleClone> simpleClones)
	{
		for(int z = 0 ; z < pattern.size() ; z++)
			writer_file.print(pattern.get(z) + ",");
		writer_file.println();
		
		Collections.sort(filesContainingPattern);
		
		int repeatingFileCount = 1;
		for(int f = 0 ; f< filesContainingPattern.size() ; f++)
		{
			int fileID = filesContainingPattern.get(f);
			
			if(f != filesContainingPattern.size() - 1  && fileID == filesContainingPattern.get(f+1))
			{
				repeatingFileCount++;				
				continue;
			}
			else if(f == filesContainingPattern.size() -1 || fileID != filesContainingPattern.get(f+1))
			{				
					writer_file.print(fileID + ":");		
			}
			
			for(int id=0 ; id< pattern.size() ; id++)
			{
				writer_file.print(" [");
				int cloneId=pattern.get(id);
				
				SimpleClone clone = simpleClones.get(cloneId);
				for(int instanceID = 0 ;  instanceID < clone.getInstancesSize() ; instanceID++)
				{
					SimpleCloneInstance instance = clone.getInstanceAt(instanceID);
					
					if(instance.getFileId() == fileID /*&& similarClonecount != 0*/)
						writer_file.print(cloneId + "." + instanceID + ",");
				}
				writer_file.print("] ,");				
			}
			int totalNoOfTokensCovered = StructuralClones_FileClusters.findFileCoverage(pattern, simpleClones, fileID , false);						 
	        int iFileTokenCount = sFile_List.getsFileAt(fileID).getFileEndToken() - sFile_List.getsFileAt(fileID).getFileStartToken();
	        float totalCovPercentage = ((float) totalNoOfTokensCovered / (float) iFileTokenCount)* 100;
	        writer_file.print("("+ totalNoOfTokensCovered + "|" );
	        writer_file.printf("%.2f", totalCovPercentage); 
	        writer_file.print("%" +")" + "(" + repeatingFileCount + ")");
	        writer_file.println();
	        repeatingFileCount = 1;
	   }
	}

    public static void writeDirStructures(ArrayList<FileCluster> fileClusters)
    {
    	PrintWriter writer_file;
    	try
		{
	    		int structureCount=0;
		  		writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.DIR_STRUCTURES), "UTF-8");
		  		ArrayList<ArrayList<Integer>> crossPatterns = new ArrayList<ArrayList<Integer>>();
		  		ArrayList<String> crossDirFileStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_DIR));
		  		ArrayList<String> fileClonesByDir = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_DIR_NORMAL));
		  		ArrayList<String> inDirFileStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.IN_DIR_STRUCTURE));
		  	    
		  		if(crossDirFileStructures != null && crossDirFileStructures.size() > 0)
				{
					//for every structure
					for(int i = 0 ; i < crossDirFileStructures.size() ; i++)
					{
				        String [] line_parts = crossDirFileStructures.get(i).split("#");
						if(line_parts.length <= 1)
							continue;
	
						String [] support_temp = line_parts[1].split(" ");
						int support = Integer.parseInt(support_temp[1]);  //the support is the count of occurrences
						
						if(support <= 1)
							continue;
						
						String [] temp = line_parts[0].split(" ");
						if(temp.length <= 0)
							continue;	
						
						ArrayList<String> crossDirFilePattern_temp = new ArrayList<String>();
						Collections.addAll(crossDirFilePattern_temp, temp);	
						ArrayList<Integer> crossDirFilePattern = CommonUtility.convertStrArrListToInt(crossDirFilePattern_temp , true);
						Collections.sort(crossDirFilePattern);
						crossPatterns.add(crossDirFilePattern);
						ArrayList<Integer> dirsContainingCrossDirFilePattern = new ArrayList<Integer>();
						
						for(int j =0 ; j < fileClonesByDir.size() ; j++)
						{
							String [] clonesList = fileClonesByDir.get(j).split(" ");
							if(clonesList != null && clonesList.length > 0)
							{
								ArrayList<String> clonesList_temp = new ArrayList<String>();
								Collections.addAll(clonesList_temp, clonesList);	
								ArrayList<Integer> fileClonesByDirList = CommonUtility.convertStrArrListToInt(clonesList_temp , true);
								boolean isFirstMatch = true;
								
								while(true)
								{	
									boolean patternMatched = true;
									for(int z=0 ; z < crossDirFilePattern.size() ; z++)
									{					
										int element = crossDirFilePattern.get(z);
										if(Collections.frequency(crossDirFilePattern, element) > Collections.frequency(fileClonesByDirList , element))
										{
											patternMatched = false;	
											break;
										}
									}
									
									if(patternMatched)
									{
										if(isFirstMatch)
											isFirstMatch=false;										
										else
											support++;
										
										dirsContainingCrossDirFilePattern.add(j);
										
										for(int k =0 ; k < crossDirFilePattern.size() ; k++)
											fileClonesByDirList.remove(crossDirFilePattern.get(k));
									}
									else
										break;
								}
							}
						}
					
						writer_file.println(structureCount + " " + support);
					    
						dirStrucWriter_HelperMethod(writer_file , crossDirFilePattern , dirsContainingCrossDirFilePattern,fileClusters);
						writer_file.println();
						structureCount++;
					}
				}
		  		for(int x=0 ; x < inDirFileStructures.size() ; x++)
				{
					String [] inDirfileStruc = inDirFileStructures.get(x).split("\\(");
					for(int y=0;y < inDirfileStruc.length ; y++)
					{
						boolean repeatingPattern = false;
						if(inDirfileStruc[y].isEmpty() == false && inDirfileStruc[y] != "" && inDirfileStruc[y] != null)
						{
							String [] p = inDirfileStruc[y].split("\\)");
							int totalOccurenceCount = Integer.parseInt(p[0]);
							String [] temp = p[1].split(",");
							ArrayList<String> inDirFilePattern_temp = new ArrayList<String>();
							Collections.addAll(inDirFilePattern_temp, temp);	
							ArrayList<Integer> inDirFilePattern = CommonUtility.convertStrArrListToInt(inDirFilePattern_temp , false);
							Collections.sort(inDirFilePattern);
							
							for(int c = 0 ;c < crossPatterns.size() ; c++)
							{								
								if(crossPatterns.get(c).equals(inDirFilePattern))
								{
									repeatingPattern= true;
									break;
								}
							}
							if(!repeatingPattern)
							{
								ArrayList<Integer> dirContainingInDirFilePattern = new ArrayList<Integer>();
								for(int c=0 ; c < totalOccurenceCount ; c++)									
									dirContainingInDirFilePattern.add(x);
								
								writer_file.println(structureCount + " " + totalOccurenceCount);							
								dirStrucWriter_HelperMethod(writer_file ,inDirFilePattern, dirContainingInDirFilePattern, fileClusters);
								writer_file.println();
								structureCount++;
							}
						}
					}				
				}				  		
		  		writer_file.close();		
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
		    e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }
    
    private static void dirStrucWriter_HelperMethod(PrintWriter writer_file , ArrayList<Integer> fileClonePatterns , ArrayList<Integer> dirsContainingFileClonePatterns,ArrayList<FileCluster> fileClusters)    //FileCluster -> File Clones
    {
    	for(int z = 0 ; z < fileClonePatterns.size() ; z++)
			writer_file.print(fileClonePatterns.get(z) + ",");
		writer_file.println();
		
		Collections.sort(dirsContainingFileClonePatterns);

		int repeatingDirCount = 1;
		for(int d=0; d < dirsContainingFileClonePatterns.size() ; d++)
		{
			int dirId = dirsContainingFileClonePatterns.get(d);
			
			if(d != dirsContainingFileClonePatterns.size() - 1  && dirId == dirsContainingFileClonePatterns.get(d+1))
			{
				repeatingDirCount++;				
				continue;
			}
			else if(d == dirsContainingFileClonePatterns.size() -1 || dirId != dirsContainingFileClonePatterns.get(d+1))
			{				
					writer_file.print(dirId + ":");		
			}			
			ArrayList<Integer> totalFilesOfDir = null;
			ArrayList<Integer> filesOfDirThatArePartOfCluster = null;
			int noOfFilesToBeChosen = 0;
			for(int fileClusterId : fileClonePatterns)
			{					
				FileCluster fileCluster = getFileClusterAt(fileClusterId,fileClusters);
				if(fileCluster != null)
				{			
					ArrayList<Integer> filesContainedInCluster = fileCluster.getvFiles();
					totalFilesOfDir = sDir_List.getDirAt(dirId).getContainedFiles();
					filesOfDirThatArePartOfCluster = new ArrayList<Integer>();
					
					for(int a=0;a < totalFilesOfDir.size() ; a++)
						if(filesContainedInCluster.contains(totalFilesOfDir.get(a)))
							filesOfDirThatArePartOfCluster.add(totalFilesOfDir.get(a));	
					
					writer_file.print(" [");
					for(int f = 0 ; f < filesOfDirThatArePartOfCluster.size() ; f++)
					{
					    if(f != filesOfDirThatArePartOfCluster.size()-1)
					    	writer_file.print(filesOfDirThatArePartOfCluster.get(f) + ",");
					    else
					    	writer_file.print(filesOfDirThatArePartOfCluster.get(f));
					}
					writer_file.print("] , ");	
					noOfFilesToBeChosen++;
				}
			}
			if(filesOfDirThatArePartOfCluster != null && filesOfDirThatArePartOfCluster.size() > 0 && totalFilesOfDir !=null && totalFilesOfDir.size() >0)
			{
				float totalCovPercentage = ((float) noOfFilesToBeChosen / (float) totalFilesOfDir.size())* 100;
				writer_file.print("(" );
				writer_file.printf("%.2f", totalCovPercentage); 
				writer_file.println( ")" + "(" +repeatingDirCount+ ")");
				repeatingDirCount = 1;
			}
		}
		writer_file.println();
    }
    
    private static FileCluster getFileClusterAt(int clusterId,ArrayList<FileCluster> fileClusters)
	{
		for(FileCluster cluster : fileClusters)
		{
			if(cluster.getClusterID() == clusterId)
				return cluster;
		}
		return null;
	}

    public static void writeGroupStructures(ArrayList<FileCluster> fileClusters)
    {
    	PrintWriter writer_file;
    	try
		{
	    		int structureCount=0;
		  		writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.GROUP_STRUCTURES), "UTF-8");
		  		ArrayList<ArrayList<Integer>> crossPatterns = new ArrayList<ArrayList<Integer>>();
		  		ArrayList<String> crossGroupFileStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_GROUP));
		  		ArrayList<String> fileClonesByGroup = CommonUtility.readFile(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_GROUP_NORMAL));
		  		ArrayList<String> inGroupFileStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.IN_GROUP_STRUCTURE));
		  		
		  		if(crossGroupFileStructures != null && crossGroupFileStructures.size() > 0)
				{
					//for every structure
					for(int i = 0 ; i < crossGroupFileStructures.size() ; i++)
					{
				        String [] line_parts = crossGroupFileStructures.get(i).split("#");
						if(line_parts.length <= 1)
							continue;
	
						String [] support_temp = line_parts[1].split(" ");
						int support = Integer.parseInt(support_temp[1]);  //the support is the count of occurrences
						
						if(support <= 1)
							continue;
						
						String [] temp = line_parts[0].split(" ");
						if(temp.length <= 0)
							continue;	
						
						ArrayList<String> crossGroupFilePattern_temp = new ArrayList<String>();
						Collections.addAll(crossGroupFilePattern_temp, temp);	
						ArrayList<Integer> crossGroupFilePattern = CommonUtility.convertStrArrListToInt(crossGroupFilePattern_temp , true);
						Collections.sort(crossGroupFilePattern);
						crossPatterns.add(crossGroupFilePattern);
						ArrayList<Integer> groupsContainingCrossGroupFilePattern = new ArrayList<Integer>();
		  		
						for(int j =0 ; j < fileClonesByGroup.size() ; j++)
						{
							String [] clonesList = fileClonesByGroup.get(j).split(" ");
							if(clonesList != null && clonesList.length > 0)
							{
								ArrayList<String> clonesList_temp = new ArrayList<String>();
								Collections.addAll(clonesList_temp, clonesList);	
								ArrayList<Integer> fileClonesByGroupList = CommonUtility.convertStrArrListToInt(clonesList_temp , true);
								boolean isFirstMatch = true;
								
								while(true)
								{	
									boolean patternMatched = true;
									for(int z=0 ; z < crossGroupFilePattern.size() ; z++)
									{					
										int element = crossGroupFilePattern.get(z);
										if(Collections.frequency(crossGroupFilePattern, element) > Collections.frequency(fileClonesByGroupList , element))
										{
											patternMatched = false;	
											break;
										}
									}
									
									if(patternMatched)
									{
										if(isFirstMatch)
											isFirstMatch=false;										
										else
											support++;
										
										groupsContainingCrossGroupFilePattern.add(j);
										
										for(int k =0 ; k < crossGroupFilePattern.size() ; k++)
											fileClonesByGroupList.remove(crossGroupFilePattern.get(k));
									}
									else
										break;
								}
							}
						}
					
						writer_file.println(structureCount + " " + support);
					    
						groupStrucWriter_HelperMethod(writer_file , crossGroupFilePattern , groupsContainingCrossGroupFilePattern,fileClusters);
						writer_file.println();
						structureCount++;
					}
				}
		  		for(int x=0 ; x < inGroupFileStructures.size() ; x++)
				{
					String [] inGroupfileStruc = inGroupFileStructures.get(x).split("\\(");
					for(int y=0;y < inGroupfileStruc.length ; y++)
					{
						boolean repeatingPattern = false;
						if(inGroupfileStruc[y].isEmpty() == false && inGroupfileStruc[y] != "" && inGroupfileStruc[y] != null)
						{
							String [] p = inGroupfileStruc[y].split("\\)");
							int totalOccurenceCount = Integer.parseInt(p[0]);
							String [] temp = p[1].split(",");
							ArrayList<String> inGroupFilePattern_temp = new ArrayList<String>();
							Collections.addAll(inGroupFilePattern_temp, temp);	
							ArrayList<Integer> inGroupFilePattern = CommonUtility.convertStrArrListToInt(inGroupFilePattern_temp , false);
							Collections.sort(inGroupFilePattern);
							
							for(int c = 0 ;c < crossPatterns.size() ; c++)
							{								
								if(crossPatterns.get(c).equals(inGroupFilePattern))
								{
									repeatingPattern= true;
									break;
								}
							}
							if(!repeatingPattern)
							{
								ArrayList<Integer> dirContainingInDirFilePattern = new ArrayList<Integer>();
								for(int c=0 ; c < totalOccurenceCount ; c++)									
									dirContainingInDirFilePattern.add(x);
								
								writer_file.println(structureCount + " " + totalOccurenceCount);							
								groupStrucWriter_HelperMethod(writer_file ,inGroupFilePattern, dirContainingInDirFilePattern, fileClusters);
								writer_file.println();
								structureCount++;
							}
						}
					}				
				}		  		
		  		writer_file.close();		
		}
		catch (FileNotFoundException e) 
		{
			// TODO Auto-generated catch block
		    e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }
    
    private static void groupStrucWriter_HelperMethod(PrintWriter writer_file , ArrayList<Integer> fileClonePatterns , ArrayList<Integer> groupsContainingFileClonePatterns,ArrayList<FileCluster> fileClusters)    //FileCluster -> File Clones
    {
    	for(int z = 0 ; z < fileClonePatterns.size() ; z++)
			writer_file.print(fileClonePatterns.get(z) + ",");
		writer_file.println();
		
		Collections.sort(groupsContainingFileClonePatterns);

		int repeatingGroupCount = 1;
		for(int g=0; g < groupsContainingFileClonePatterns.size() ; g++)
		{
			int groupId = groupsContainingFileClonePatterns.get(g);
			
			if(g != groupsContainingFileClonePatterns.size() - 1  && groupId == groupsContainingFileClonePatterns.get(g+1))
			{
				repeatingGroupCount++;				
				continue;
			}
			else if(g == groupsContainingFileClonePatterns.size() -1 || groupId != groupsContainingFileClonePatterns.get(g+1))
			{				
					writer_file.print(groupId + ":");		
			}			
			ArrayList<Integer> totalFilesOfGroup = null;
			ArrayList<Integer> filesOfGroupThatArePartOfCluster = null;
			int noOfFilesToBeChosen = 0;
			for(int fileClusterId : fileClonePatterns)
			{					
				FileCluster fileCluster = getFileClusterAt(fileClusterId,fileClusters);
				if(fileCluster != null)
				{			
					ArrayList<Integer> filesContainedInCluster = fileCluster.getvFiles();
					totalFilesOfGroup = sGroup_List.getGroupAt(groupId).getvContainedFiles();
					filesOfGroupThatArePartOfCluster = new ArrayList<Integer>();
					
					for(int a=0;a < totalFilesOfGroup.size() ; a++)
						if(filesContainedInCluster.contains(totalFilesOfGroup.get(a)))
							filesOfGroupThatArePartOfCluster.add(totalFilesOfGroup.get(a));	
					
					writer_file.print(" [");
					for(int f = 0 ; f < filesOfGroupThatArePartOfCluster.size() ; f++)
					{
					    if(f != filesOfGroupThatArePartOfCluster.size()-1)
					    	writer_file.print(filesOfGroupThatArePartOfCluster.get(f) + ",");
					    else
					    	writer_file.print(filesOfGroupThatArePartOfCluster.get(f));
					}
					writer_file.print("] , ");	
					noOfFilesToBeChosen++;
				}
			}
			if(filesOfGroupThatArePartOfCluster != null && filesOfGroupThatArePartOfCluster.size() > 0 && totalFilesOfGroup !=null && totalFilesOfGroup.size() >0)
			{
				float totalCovPercentage = ((float) noOfFilesToBeChosen / (float) totalFilesOfGroup.size())* 100;
				writer_file.print("(" );
				writer_file.printf("%.2f", totalCovPercentage); 
				writer_file.println( ")" + "(" +repeatingGroupCount+ ")");
				repeatingGroupCount = 1;
			}
		}
		writer_file.println();
    }
}