package cmt.cddc.fileclones;

import java.util.List;
import java.util.Set;

import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.common.CommonUtility;
import cmt.cddc.fcim.MainTestFPClose_saveToFile;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleCloneWriter;
import cmt.cddc.structures.sDir_List;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sGroup_List;
import cmt.common.Directories;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;

public final class StructuralClones_FileClusters 
{
	public static ArrayList<FileCluster> detectFileClusters(ArrayList<SimpleClone> simpleClones)
	{	
		ArrayList<FileCluster> fileClusters = new ArrayList<FileCluster>();	
		try
		{
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.CLONES_BY_FILE_NORMAL),Directories.getAbsolutePath(Directories.CROSS_FILE_CLONE_STRUCTURE), 0);
			ArrayList<String> scsAcrossFiles = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_FILE_CLONE_STRUCTURE));
			if(scsAcrossFiles != null && scsAcrossFiles.size() > 0)
			{				
				//for every structure
				for(int i = 0 ; i < scsAcrossFiles.size() ; i++)
				{
					//create a cluster
			        FileCluster pFCluster = new FileCluster();
			        
			        String [] line_parts = scsAcrossFiles.get(i).split("#");
					if(line_parts.length <= 1)
						continue;

					String [] support_temp = line_parts[1].split(" ");
					int support = Integer.parseInt(support_temp[1]);  //the support is the count of occurrences
					
					if(support <= 1)
						continue;
					
					String [] temp = line_parts[0].split(" ");
					if(temp.length <= 0)
						continue;	
					
					ArrayList<String> clone_ids_spmf = new ArrayList<String>();
					Collections.addAll(clone_ids_spmf, temp);	
					ArrayList<Integer> vCloneClasses = CommonUtility.convertStrArrListToInt(clone_ids_spmf , false);
					//sort the vector because the result is not ordered
					Collections.sort(vCloneClasses);
					pFCluster.setvCloneClasses(vCloneClasses);	
					
					List<Integer> lTempFiles = CommonUtility.getFilesContainingStructure(simpleClones, vCloneClasses);
				    //group check here
				    if(!satisfyGroupCheck(lTempFiles))
					    continue; 
			       
				    //now temp list has only those files that have this complete structure
			        //put these file IDs in the cluster
			        pFCluster.setvFiles((ArrayList<Integer>)lTempFiles);
			        //lTempFiles is not used anymore
			        //lTempFiles.clear();
				    
			        for (int it = 0; it < pFCluster.getvFileSize(); it++) 
			        {
			            int fileID = pFCluster.getVFileAt(it);
			            //find percentage coverage of this file with this structure
			            //concern : send structure...with repetitions..not unique list
			            int iCover = findFileCoverage(pFCluster.getvCloneClasses(), simpleClones,fileID,true);			           
			            pFCluster.addvFileToken(iCover);

			            int iFileTokenCount = sFile_List.getsFileAt(fileID).getFileEndToken() - sFile_List.getsFileAt(fileID).getFileStartToken();
			            float iCoverP = ((float) iCover / (float) iFileTokenCount)* 100;
			            pFCluster.addvFileCoverage(iCoverP);
			        }
			        pFCluster.setClusterID(fileClusters.size());
			        //insert the new cluster into the cluster vector NOW
			        fileClusters.add(pFCluster);			       
				}
			}
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return fileClusters;		
	}
	
	public static int findFileCoverage(ArrayList<Integer> vCloneClasses , ArrayList<SimpleClone> simpleClones, int fileId , boolean isTransformedIds)
	{
		int iCoverage = 0;
		if(simpleClones != null && simpleClones.size() > 0 && vCloneClasses != null && vCloneClasses.size() > 0)
		{
			int iFileStartToken = sFile_List.getsFileAt(fileId).getFileStartToken();
			int iFileTokenCount = sFile_List.getsFileAt(fileId).getFileEndToken() - iFileStartToken; 
			boolean[] vBFile = new boolean[iFileTokenCount];
			int i = 0;
			
			while(i < vCloneClasses.size())
			{
				int cc = 0 , divisor = 0;
				if(isTransformedIds)
				{
					cc = vCloneClasses.get(i) / 100;
					divisor = 100;
				}
				else
				{
					cc = vCloneClasses.get(i);
					divisor = 1;
				}
				int count = 0; //count the same members
				while(i < vCloneClasses.size() && vCloneClasses.get(i) / divisor == cc)
				{
					count++;
					i++;
				}
				if(cc < simpleClones.size())
				{				
					for(int j = 0 ; j < simpleClones.get(cc).getInstancesSize() ; j++)
					{
						if(simpleClones.get(cc).getInstanceAt(j).getFileId() == fileId && count > 0)
						{
							count--;
							int startToken = simpleClones.get(cc).getInstanceAt(j).getStartingIndex();
							int endToken = simpleClones.get(cc).getInstanceAt(j).getEndingIndex();
							
							for(int k=startToken ; k <= endToken ; k++)
							{
								if(!vBFile[k-iFileStartToken])
								{
									iCoverage++;
									vBFile[k-iFileStartToken] = true;
								}
							}
						}
					}
			    }
			}
		}
		return iCoverage;
	}
	private static boolean satisfyGroupCheck(List<Integer> lTempFiles)
	{
		if(lTempFiles == null || lTempFiles.size() <= 0)
			return false;
		
		if(SysConfig.getGroupCheck() == 2)
		{		
				boolean bSatisfyGroupCheck = false;
		        int fileID = lTempFiles.get(0);
		        int firstFileGroup = sFile_List.getsFileAt(fileID).getGroupID();
		            for (int id = 0 ; id < lTempFiles.size() ; id++) {
		                fileID = lTempFiles.get(id);
		                int curFileGroup = sFile_List.getsFileAt(fileID).getGroupID();
		                if (firstFileGroup != curFileGroup) {
		                    bSatisfyGroupCheck = true;
		                }
		            }
		        return bSatisfyGroupCheck;	           
		}
		else
			return true;
	}
	
	public static void pruneFileClusters(ArrayList<FileCluster> fileClusters)
	{
		FC_FirstPruning(fileClusters);
		sort(fileClusters);
		FC_SecondPruning(fileClusters);
		FC_ThirdPruning(fileClusters);
	}
	
	private static void sort(ArrayList<FileCluster> fileClusters)
	{
		Comparator<FileCluster> customComp = new FileCluster();
		fileClusters.sort(customComp);
	}
	
	//prune all clusters with coverage less than threshold
	private static void FC_FirstPruning(ArrayList<FileCluster> fileClusters)
	{
		for(int i = 0 ; i < fileClusters.size() ; i++)
		{
			FileCluster cluster= fileClusters.get(i);
			for(int j = 0 ; j < cluster.getvFileCoverageSize() ; j++)
			{
				if (cluster.getvFileCoverageAt(j) > SysConfig.getMinFClusP() && cluster.getvFileTokenAt(j) > SysConfig.getMinFClusT()) {

					cluster.setbSignificant(true);
	                break;
	            }
			}
			if (!cluster.isbSignificant()) 
			{
				fileClusters.remove(i);	            	            
	            i--;	            
	        }
		}
	}
	
	//prune all clusters whose files are all present in another unique cluster
	private static void FC_SecondPruning(ArrayList<FileCluster> fileClusters)
	{
		for(int i = 0; i < fileClusters.size() ; i++)
		{
			FileCluster cluster1 = fileClusters.get(i);
			for(int j = i+1 ; j < fileClusters.size() ; j++)
			{
				FileCluster cluster2 = fileClusters.get(j);
				if(cluster2.getvFiles().containsAll(cluster1.getvFiles()))
				{
					cluster1.setbSignificant(false);
					break;
				}
			}
			if (!cluster1.isbSignificant()) 
			{
				fileClusters.remove(i);
	            i--;
	        }
		}
	}
	
	//prune clusters whose files are present in other different clusters
	private static void FC_ThirdPruning(ArrayList<FileCluster> fileClusters)
	{
		 //for each cluster
		for(int i = 0; i < fileClusters.size() ; i++)
		{
			FileCluster cluster1 = fileClusters.get(i);
			//for each file in this cluster
			for(int j = 0 ; j < cluster1.getvFileSize() ; j++)
			{
				int fileID = cluster1.getVFileAt(j);
				
				//look for this file in all other clusters
				for(int k = i+1 ; k < fileClusters.size() ; k++)
				{
					FileCluster cluster2 = fileClusters.get(k);
					
					//if the file is found in 2nd cluster...break   
					if(cluster2.getvFiles().contains(fileID))
					{
						cluster1.setbSignificant(false);
						break;
					}
				}
				//if file not found in any cluster...this cluster is significant
				//if(cluster1.isbSignificant())
				//	break;
			}			
			if(!cluster1.isbSignificant())
			{
				fileClusters.remove(i);
	            i--;
			}
		}
	}
	
	//lists which file clone classes are represented in each directory
	public static void updateDirsWithFileClones(ArrayList<FileCluster> fileClusters) 
	{
		 //for each file cluster
	    for (FileCluster cluster : fileClusters) 
	    {	       
	        //for each file in this cluster
	    	for (int j = 0; j < cluster.getvFileSize(); j++) 
	    	{
	            int normalFC = cluster.getClusterID()*100;
	            int fileID = cluster.getVFileAt(j); 
	            
	            //update the corresponding dir
	            int dirID = sFile_List.getsFileAt(fileID).getDirID();
	            int numberFC = sDir_List.getDirAt(dirID).getFileCloneClassSize();
	            int lastFC = 0;
	            if (numberFC > 0) 
	            {
	                lastFC = sDir_List.getDirAt(dirID).getFileCloneClassAt(numberFC - 1);
	                if (lastFC / 100 == cluster.getClusterID()) 
	                {
	                    normalFC = lastFC + 1;
	                }
	            }
	            sDir_List.getDirAt(dirID).addFileCloneClass(normalFC);
	    	}
	    }
	}

	public static void updateGroupsWithFileClones(ArrayList<FileCluster> fileClusters)
	{
		//for each file cluster
		for (FileCluster cluster : fileClusters) 
	    {	       
			//for each file in this cluster
	        for (int j = 0; j < cluster.getvFileSize(); j++) 
	        {
	            int normalFC = (cluster.getClusterID())*100;
	            int fileID = cluster.getVFileAt(j);
	            //update the corresponding dir
	            int groupID = sFile_List.getsFileAt(fileID).getGroupID();
	            int numberFC = sGroup_List.getGroupAt(groupID).getvFileCloneClassSize();
	            int lastFC = 0;
	            if (numberFC > 0) 
	            {
	                lastFC = sGroup_List.getGroupAt(groupID).getvFileCloneClassAt(numberFC - 1);
	                if (lastFC / 100 == cluster.getClusterID()) {
	                    normalFC = lastFC + 1;
	                }
	            }
	            sGroup_List.getGroupAt(groupID).addFileCloneClasses(normalFC);
	        }
	    }
	}
	
	public static void findCrossDirsFileCloneStructures()
	{
		try 
		{
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_DIR_NORMAL),Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_DIR), 0.1);
		
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void findCrossGroupsFileCloneStructures()
	{
		try 
		{
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.FILE_CLONES_BY_GROUP_NORMAL),Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_GROUP) , 0.1);
		
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
   
	public static ArrayList<FileCluster> detectFileClustersInc(HashMap<Integer,ArrayList<Integer>> result,ArrayList<SimpleClone> finalClones,HashMap<Integer,Integer> unModifiedFileCoverage)
	{	
		ArrayList<FileCluster> fileClusters = new ArrayList<FileCluster>();	
		try
		{
			SimpleCloneWriter.CreateClonesByFileStructureInc(result);
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.CLONES_BY_FILE_NORMAL),Directories.getAbsolutePath(Directories.CROSS_FILE_CLONE_STRUCTURE), 0.1);
			ArrayList<String> scsAcrossFiles = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_FILE_CLONE_STRUCTURE));
			if(scsAcrossFiles != null && scsAcrossFiles.size() > 0)
			{				
				//for every structure
				for(int ie = 0 ; ie < scsAcrossFiles.size() ; ie++)
				{
					//create a cluster
			        FileCluster pFCluster = new FileCluster();
			        
			        String [] line_parts = scsAcrossFiles.get(ie).split("#");
					if(line_parts.length <= 1)
						continue;

					String [] support_temp = line_parts[1].split(" ");
					int support = Integer.parseInt(support_temp[1]);  //the support is the count of occurrences
					
					if(support <= 1)
						continue;
					
					String [] temp = line_parts[0].split(" ");
					if(temp.length <= 0)
						continue;	
					
					ArrayList<String> clone_ids_spmf = new ArrayList<String>();
					Collections.addAll(clone_ids_spmf, temp);	
					ArrayList<Integer> vCloneClasses = CommonUtility.convertStrArrListToInt(clone_ids_spmf , true);
					//sort the vector because the result is not ordered
					Collections.sort(vCloneClasses);
					pFCluster.setvCloneClasses(vCloneClasses);	
					
					ArrayList<Integer> lTempFiles = new ArrayList<Integer>();
				    //group check here
					lTempFiles.addAll(result.keySet());
					for(Integer fid : result.keySet())
					{
						for(Integer vcloneID : vCloneClasses)
						{
							if(!result.get(fid).contains(vcloneID))
							{
								lTempFiles.remove(fid);
								break;
							}
						}
					}
					
				    if(!satisfyGroupCheck(lTempFiles))
					    continue; 
			       
				    //now temp list has only those files that have this complete structure
			        //put these file IDs in the cluster
			        pFCluster.setvFiles((ArrayList<Integer>)lTempFiles);
			        //lTempFiles is not used anymore
			        //lTempFiles.clear();
				    
			        for (int it = 0; it < pFCluster.getvFileSize(); it++) 
			        {
			        	int fileID = pFCluster.getVFileAt(it);
			        	//find percentage coverage of this file with this structure
			        	//concern : send structure...with repetitions..not unique list
			        	
			        	//----// = findFileCoverage(pFCluster.getvCloneClasses(), simpleClones,fileID,true);			           
			        	//vCloneClasses get coverage of those//
			        	int iCover = 0;
			        	ArrayList<SimpleCloneInstance> sciInFile = new ArrayList<>();
			        	for(SimpleClone sc : finalClones)
			        	{
			        		if(vCloneClasses.contains(sc.getSSCId()))
			        		{
			        			for(SimpleCloneInstance sci : sc.getInstances())
			        			{
			        				if(sci.getFileId() == fileID)
			        					sciInFile.add(sci);
			        			}
			        		}
			        	}
			        	if(sciInFile.size() > 1)
						{
							Integer lastIndex = 0;
							Integer smallestIndex = 0;
							SimpleCloneInstance smallest=sciInFile.get(0);
							for(int j = 1; j < sciInFile.size() ; j++)
							{
								if(smallest.getStartingIndex() < sciInFile.get(j).getStartingIndex())
								{
									smallest = sciInFile.get(j);
								}
							}
							iCover = smallest.getEndingIndex() - smallest.getStartingIndex();
							lastIndex = smallest.getEndingIndex();
							smallestIndex = smallest.getStartingIndex();
							sciInFile.remove(smallest);
							
							for(int i  = 0 ; i <sciInFile.size() ; i++)
							{
								if(sciInFile.get(i).getStartingIndex() > lastIndex)
								{
									iCover += sciInFile.get(i).getEndingIndex() -  sciInFile.get(i).getStartingIndex();
									lastIndex = sciInFile.get(i).getEndingIndex();
									smallestIndex = sciInFile.get(i).getStartingIndex();
								}
								else if(sciInFile.get(i).getStartingIndex() >= smallestIndex && sciInFile.get(i).getEndingIndex() <= lastIndex)
								{
									//ignore complete overlaping case
								}
								else if(sciInFile.get(i).getStartingIndex() >= smallestIndex && sciInFile.get(i).getEndingIndex() > lastIndex)
								{
									iCover += sciInFile.get(i).getEndingIndex() - lastIndex;
									lastIndex = sciInFile.get(i).getEndingIndex();
								}
							}
							
							
						}
						else if (sciInFile.size() == 1)
						{
							iCover = sciInFile.get(0).getEndingIndex() - sciInFile.get(0).getStartingIndex();
						}
			        	pFCluster.addvFileToken(iCover);
			        	int iFileTokenCount = 0;
			        	if(!unModifiedFileCoverage.containsKey(fileID))
			        		iFileTokenCount = sFile_List.getsFileAt(fileID).getFileEndToken() - sFile_List.getsFileAt(fileID).getFileStartToken();
			        	else
			        		iFileTokenCount = unModifiedFileCoverage.get(fileID);
			        	float iCoverP = ((float) iCover / (float) iFileTokenCount)* 100;
			        	pFCluster.addvFileCoverage(iCoverP);
			        }
			        pFCluster.setClusterID(fileClusters.size());
			        //insert the new cluster into the cluster vector NOW
			        fileClusters.add(pFCluster);			       
				}
			}
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return fileClusters;		
	}
	
	
}
