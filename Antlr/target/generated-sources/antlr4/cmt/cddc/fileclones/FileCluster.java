package cmt.cddc.fileclones;

import java.util.ArrayList;
import java.util.Comparator;

import cmt.cddc.simpleclones.SimpleCloneInstance;


public class FileCluster implements Comparator<FileCluster>
{
	private int clusterID;
    private ArrayList<Integer> vCloneClasses;
    private ArrayList<Integer> vFiles;
    private ArrayList<Integer> vFilesToken;
	private ArrayList<Float> vFilesCoverage;
    private boolean bSignificant;
    private boolean framed;
    
	private ArrayList<FileCloneInstance> fccInstance;
	double averageTokenCount;
	double averagePercentageCount;
	int numberOfInstance;
    
    public FileCluster()
    {
    	this.clusterID = -1;
    	this.vCloneClasses = new ArrayList<Integer>();
    	this.vFiles = new ArrayList<Integer>();
    	this.vFilesToken = new ArrayList<Integer>();
    	this.vFilesCoverage = new ArrayList<Float>();
    	this.bSignificant = false;
    	this.averagePercentageCount =0;
    	this.averageTokenCount = 0;
    	this.numberOfInstance = 0;
    	this.fccInstance = new ArrayList<FileCloneInstance>(); 
    }
    public int getClusterID() 
    {
		return clusterID;
	}
	public void setClusterID(int clusterID) 
	{
		this.clusterID = clusterID;
	}
	public ArrayList<Integer> getvCloneClasses() 
	{
		return vCloneClasses;
	}
	public void setvCloneClasses(ArrayList<Integer> vCloneClasses) 
	{
		this.vCloneClasses = vCloneClasses;
	}
	
	
	public void addTovCloneClasses(int cloneClass ) {
		this.vCloneClasses.add(cloneClass);
	}
	public ArrayList<Integer> getvFiles() 
	{
		return vFiles;
	}
	public void setvFiles(ArrayList<Integer> vFiles) 
	{
		this.vFiles = vFiles;
	}
	public ArrayList<Integer> getvFilesToken() 
	{
		return vFilesToken;
	}
	public void setvFilesToken(ArrayList<Integer> vFilesToken) 
	{
		this.vFilesToken = vFilesToken;
	}
	public ArrayList<Float> getvFilesCoverage() 
	{
		return vFilesCoverage;
	}
	public void setvFilesCoverage(ArrayList<Float> vFilesCoverage) 
	{
		this.vFilesCoverage = vFilesCoverage;
	}
	public boolean isbSignificant() 
	{
		return bSignificant;
	}
	public void setbSignificant(boolean bSignificant) 
	{
		this.bSignificant = bSignificant;
	}
	public void addvFile(int fileId)
	{
		if(this.vFiles == null)
			this.vFiles = new ArrayList<Integer>();
		this.vFiles.add(fileId);
	}
	public int getVFileAt(int index)
	{
		return this.vFiles.get(index);
	}
	public int getvFileSize()
	{
		return this.vFiles.size();
	}
	public int getvCloneClassSize()
	{
		return this.vCloneClasses.size();				
	}
	public int getvCloneClassAt(int index)
	{
		return this.vCloneClasses.get(index);
	}
	public void addvFileToken(int tokenCount)
	{
		this.vFilesToken.add(tokenCount);
	}
	public void addvFileCoverage(float fileCoverage)
	{
		this.vFilesCoverage.add(fileCoverage);
	}
	public int getvFileTokenAt(int index)
	{
		return this.vFilesToken.get(index);
	}
	public Float getvFileCoverageAt(int index)
	{
		return this.vFilesCoverage.get(index);
	}
	public int getvFileCoverageSize()
	{
		return this.vFilesCoverage.size();
	}
	public float maxCoverage()
	{
		float maxCov = 0;
		for(float cov : vFilesCoverage)
		{
			if(cov > maxCov)
				maxCov = cov;		
		}
		return maxCov;
	}
	
	public ArrayList<FileCloneInstance> getFCCInstance() {
		return fccInstance;
	}
	
	public void addFileCloneInstance(FileCloneInstance instance) {
		this.fccInstance.add(instance);
	}
	 
	public double getAveragePercentageCount() {
		return averagePercentageCount; 
	}
	
	public void setAveragePercentageCount(double averagePercentageCount) {
		this.averagePercentageCount = averagePercentageCount;
		
	}
	
	
	public double getAverageTokenCount() {
		return averageTokenCount; 
	}
	
	public void setAverageTokenCount(double averageTokenCount) {
		this.averageTokenCount = averageTokenCount;
		
	}
	
	public void setNumberOfInstance(int numberOfInstance) {
		this.numberOfInstance = numberOfInstance;		
	}
	
	public int getNumberOfInstance() {
		return this.numberOfInstance;		
	}
	
	public boolean isFramed() {
		return framed;
	}

	public void setFramed(boolean framed) {
		this.framed = framed;
	}		
	@Override
	public int compare(FileCluster o1, FileCluster o2) {
		int retVal = 0;
        if (o1.getvFileSize() < o2.getvFileSize()) {
            retVal = 1;
        }
        else if (o1.getvFileSize() == o2.getvFileSize()) 
        {
        	if(o1.maxCoverage() < o2.maxCoverage())
        		retVal = 1;            
        }
        return retVal;
	}
}
