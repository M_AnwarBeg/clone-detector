package cmt.cddc.fileclones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ProjectInfo;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import cmt.cvac.viewobjects.FccList;
import cmt.cvac.viewobjects.PrimaryFccObject;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectCrossGroup;
import cmt.cvac.viewobjects.PrimaryFcsObjectInDirs;
import cmt.cvac.viewobjects.PrimaryFcsObjectInGroup;
import cmt.cvac.viewobjects.PrimaryMcsObject;
import cmt.cvac.viewobjects.SecondaryFccObject;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectCrossGroup;
import cmt.cvac.viewobjects.SecondaryFcsObjectInDirs;
import cmt.cvac.viewobjects.SecondaryFcsObjectInGroup;
import cmt.cvac.viewobjects.SecondaryMcsObject;

public class FileClonesReader {

	private FccList fccList;
	private static FccList filteredFccList;
	private ArrayList<FileCluster> fileClones;
	private static ArrayList<FileCluster> filteredFileClones;
	private static sFile_List fileList;
	private static sFile_List filteredFileList;
	private static ArrayList<PrimaryFcsObjectCrossDirs> fcsListCrossDirs;
	private static ArrayList<PrimaryFcsObjectInDirs> fcsListInDirs;
	private static ArrayList<PrimaryFcsObjectCrossGroup> fcsListCrossGroup;
	private static ArrayList<PrimaryFcsObjectInGroup> fcsListInGroup;
	
	public FileClonesReader()
	{
		fileClones = new ArrayList<FileCluster>();
		filteredFileClones = new ArrayList<FileCluster>();
		fccList = new FccList();
		filteredFccList = new FccList();
		fileList = new sFile_List();
		filteredFileList = new sFile_List();
		fcsListCrossDirs=new ArrayList<PrimaryFcsObjectCrossDirs>();
		fcsListInDirs = new ArrayList<PrimaryFcsObjectInDirs>();
		fcsListCrossGroup = new ArrayList<PrimaryFcsObjectCrossGroup>();
		fcsListInGroup = new ArrayList<PrimaryFcsObjectInGroup>();
	}
	
	public static ArrayList<PrimaryFcsObjectCrossDirs> getFcsList()
    {
    	return fcsListCrossDirs;
    }
	public static ArrayList<PrimaryFcsObjectInDirs> getFcsListInDirs()
    {
    	return fcsListInDirs;
    }
	public static ArrayList<PrimaryFcsObjectCrossGroup> getFcsListCrossGroup()
    {
    	return fcsListCrossGroup;
    }
	public static ArrayList<PrimaryFcsObjectInGroup> getFcsListInGroup()
    {
    	return fcsListInGroup;
    }
	public void loadFilteredFcs() {
		
	}
	
	public void loadFcsInGroup()
    {
    	try
    	{
	    	Statement stmt1 = CloneRepository.getConnection().createStatement();
		    stmt1.execute("use "+ CloneRepository.getDBName()+";");
	
		    ResultSet rs = stmt1.executeQuery("select * from fcs_ingroup;");
		    
		    while(rs.next())
		    {
		    	PrimaryFcsObjectInGroup object=new PrimaryFcsObjectInGroup();
		    	object.setFcsId(rs.getInt(1));
		    	object.setNofInstances(rs.getInt(2));
		    	
		    	
		    	// poipulating fcc structure
		    	Statement stmt11 = CloneRepository.getConnection().createStatement();
			    stmt11.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_1 = stmt11.executeQuery("select fcc_id from fcsingroup_fcc where fcs_ingroup_id = " + object.getFcsId());
		    	
		    	while(rs_1.next())
		    	{
		    		object.addToFccStructure(rs_1.getInt(1));
		    	}
		    	
		    	Statement stmt12 = CloneRepository.getConnection().createStatement();
			    stmt12.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_2 = stmt12.executeQuery("select gid from fcsingroup_group where fcs_ingroup_id = " + object.getFcsId());
		    	
		    	while(rs_2.next())
		    	{
		    		object.addToGroupStructure(rs_2.getInt(1));
		    	}
		    	
		    	// **********************
		    	
			    Statement stmt31 = CloneRepository.getConnection().createStatement();
			    stmt31.execute("use "+ CloneRepository.getDBName()+";");
		
			    ResultSet rs3 = stmt31.executeQuery("select * from fcsingroup_files where fcs_ingroup_id="+object.getFcsId());
			    
			    while(rs3.next())
			    {
			    	Statement stmt41 = CloneRepository.getConnection().createStatement();
				    stmt41.execute("use "+ CloneRepository.getDBName()+";");
			
				    ResultSet rs4 = stmt41.executeQuery("select fname from file where fid="+rs3.getInt(4));
				    rs4.next();

			    	object.addToCloneList(new SecondaryFcsObjectInGroup(rs3.getInt(2), rs3.getInt(3),rs3.getInt(4), rs4.getString(1)));
			    }
			    fcsListInGroup.add(object);
		    }
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
	
	public void loadFcsCrossGroup()
    {
    	try
    	{
	    	Statement stmt1 = CloneRepository.getConnection().createStatement();
		    stmt1.execute("use "+ CloneRepository.getDBName()+";");
	
		    ResultSet rs = stmt1.executeQuery("select * from fcs_crossgroup;");
		    
		    while(rs.next())
		    {
		    	PrimaryFcsObjectCrossGroup object=new PrimaryFcsObjectCrossGroup();
		    	object.setFcsId(rs.getInt(1));
		    	object.setNofInstances(rs.getInt(2));
		    	
		    	
		    	// poipulating fcc structure
		    	Statement stmt11 = CloneRepository.getConnection().createStatement();
			    stmt11.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_1 = stmt11.executeQuery("select fcc_id from fcscrossgroup_fcc where fcs_crossgroup_id = " + object.getFcsId());
		    	
		    	while(rs_1.next())
		    	{
		    		object.addToFccStructure(rs_1.getInt(1));
		    	}
		    	
		    	Statement stmt12 = CloneRepository.getConnection().createStatement();
			    stmt12.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_2 = stmt12.executeQuery("select gid from fcscrossgroup_group where fcs_crossgroup_id = " + object.getFcsId());
		    	
		    	while(rs_2.next())
		    	{
		    		object.addToGroupStructure(rs_2.getInt(1));
		    	}
		    	
		    	// **********************
		    	
			    Statement stmt31 = CloneRepository.getConnection().createStatement();
			    stmt31.execute("use "+ CloneRepository.getDBName()+";");
		
			    ResultSet rs3 = stmt31.executeQuery("select * from fcscrossgroup_files where fcs_crossgroup_id="+object.getFcsId());
			    
			    while(rs3.next())
			    {
			    	Statement stmt41 = CloneRepository.getConnection().createStatement();
				    stmt41.execute("use "+ CloneRepository.getDBName()+";");
			
				    ResultSet rs4 = stmt41.executeQuery("select fname from file where fid="+rs3.getInt(4));
				    rs4.next();

			    	object.addToCloneList(new SecondaryFcsObjectCrossGroup(rs3.getInt(2), rs3.getInt(3),rs3.getInt(4), rs4.getString(1)));
			    }
			    fcsListCrossGroup.add(object);
		    }
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
	
	public void loadFcsInDirs()
    {
    	try
    	{
	    	Statement stmt1 = CloneRepository.getConnection().createStatement();
		    stmt1.execute("use "+ CloneRepository.getDBName()+";");
	
		    ResultSet rs = stmt1.executeQuery("select * from fcs_indir;");
		    
		    while(rs.next())
		    {
		    	PrimaryFcsObjectInDirs object=new PrimaryFcsObjectInDirs();
		    	object.setFcsId(rs.getInt(1));
		    	object.setNofInstances(rs.getInt(2));
		    	
		    	
		    	// poipulating fcc structure
		    	Statement stmt11 = CloneRepository.getConnection().createStatement();
			    stmt11.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_1 = stmt11.executeQuery("select fcc_id from fcsindir_fcc where fcs_indir_id = " + object.getFcsId());
		    	
		    	while(rs_1.next())
		    	{
		    		object.addToFccStructure(rs_1.getInt(1));
		    	}
		    	
		    	Statement stmt12 = CloneRepository.getConnection().createStatement();
			    stmt12.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_2 = stmt12.executeQuery("select did from fcsindir_dir where fcs_indir_id = " + object.getFcsId());
		    	
		    	rs_2.next();
		    	object.setDirId(rs_2.getInt(1));
		    	
		    	// **********************
		    	
			    Statement stmt31 = CloneRepository.getConnection().createStatement();
			    stmt31.execute("use "+ CloneRepository.getDBName()+";");
		
			    ResultSet rs3 = stmt31.executeQuery("select * from fcsindir_files where fcs_indir_id="+object.getFcsId());
			    
			    while(rs3.next())
			    {
			    	Statement stmt41 = CloneRepository.getConnection().createStatement();
				    stmt41.execute("use "+ CloneRepository.getDBName()+";");
			
				    ResultSet rs4 = stmt41.executeQuery("select fname from file where fid="+rs3.getInt(5));
				    rs4.next();

			    	object.addToCloneList(new SecondaryFcsObjectInDirs(rs3.getInt(3),rs3.getInt(5),rs3.getInt(4), rs4.getString(1)));
			    }
			    fcsListInDirs.add(object);
		    }
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
	
	public void loadFcs()
    {
    	try
    	{
	    	Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+ CloneRepository.getDBName()+";");
	
		    ResultSet rs1 = stmt.executeQuery("select * from fcs_crossdir;");
		    
		    while(rs1.next())
		    {
		    	PrimaryFcsObjectCrossDirs object=new PrimaryFcsObjectCrossDirs();
		    	object.setFcsId(rs1.getInt(1));
		    	object.setNofInstances(rs1.getInt(2));
		    	
		    	
		    	// poipulating fcc structure
		    	Statement stmt1 = CloneRepository.getConnection().createStatement();
			    stmt1.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_11 = stmt1.executeQuery("select fcc_id from fcscrossdir_fcc where fcs_crossdir_id = " + object.getFcsId());
		    	
		    	while(rs_11.next())
		    	{
		    		object.addToFccStructure(rs_11.getInt(1));
		    	}
			     
		    	// populating directory structure
		    	
		    	Statement stmt2 = CloneRepository.getConnection().createStatement();
			    stmt2.execute("use "+ CloneRepository.getDBName()+";");
		    	ResultSet rs_21 = stmt2.executeQuery("select did from fcscrossdir_dir where fcs_crossdir_id = " + object.getFcsId());
		    	
		    	while(rs_21.next())
		    	{
		    		object.addToDirStructure(rs_21.getInt(1));
		    	}
		    	
		    	// **********************
		    	
			    Statement stmt3 = CloneRepository.getConnection().createStatement();
			    stmt3.execute("use "+ CloneRepository.getDBName()+";");
		
			    ResultSet rs31 = stmt3.executeQuery("select * from fcscrossdir_files where fcs_crossdir_id="+object.getFcsId());
			    
			    while(rs31.next())
			    {
			    	Statement stmt4 = CloneRepository.getConnection().createStatement();
				    stmt4.execute("use "+ CloneRepository.getDBName()+";");
			
				    ResultSet rs41 = stmt4.executeQuery("select fname from file where fid="+rs31.getInt(4));
				    rs41.next();

			    	object.addToCloneList(new SecondaryFcsObjectCrossDirs(rs31.getInt(3),rs31.getInt(4),rs31.getInt(1), rs41.getString(1)));
			    }
			    fcsListCrossDirs.add(object);
		    }
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
	
	public static void filterFileClones(String minAPC, String maxAPC, String minATC, String maxATC, String locationIDs, String location, String minMembers, String maxMembers) {
		PrimaryFccObject obj;
		boolean filterFlag = false;
		String IDS[]= {};
		int id=0;
		String query = "";
		String query2 = "";
		 
		try {
			
			if(!location.isEmpty()) {
				if(location.equals("Directory")) {
					query = "select fcc.*, fcc_dir.did from fcc inner join fcc_dir on fcc.fcc_id = fcc_dir.fcc_id where ";
					if(locationIDs.contains(",")) {
						  IDS = locationIDs.split(",");
					 }
					 if(IDS.length>0) {
						 for(int i =0;i<IDS.length;i++) {
							 if(filterFlag) {
								 query += "or fcc_dir.did = "+IDS[i]+ " ";
							 }
							 else {
								 query += "fcc_dir.did = "+IDS[i]+ " ";
								 filterFlag = true;
							 }
							 
						 }
					 }
					 else {
						 if(filterFlag) {
							 query += "or fcc_dir.did = "+locationIDs+ " ";
						 }
						 else {
							 query += "fcc_dir.did = "+locationIDs+ " ";
							 filterFlag = true;
						 }
					 }
					
				}
				else if(location.equals("Group")) {
					query = "select fcc.*, fcc_group.gid from fcc inner join fcc_group on fcc.fcc_id = fcc_group.fcc_id where ";
					if(!locationIDs.isEmpty()) {
						//String IDS[]= {};
						 if(locationIDs.contains(",")) {
							  IDS = locationIDs.split(",");
						 }
						 if(IDS.length>0) {
							 for(int i =0;i<IDS.length;i++) {
								 if(filterFlag) {
									 query += "or fcc_group.gid = "+IDS[i]+ " ";
								 }
								 else {
									 query += "fcc_group.gid = "+IDS[i]+ " ";
									 filterFlag = true;
								 }
								 
							 }
						 }
						 else {
							 if(filterFlag) {
								 query += "or fcc_group.gid = "+locationIDs+ " ";
							 }
							 else {
								 query += "fcc_group.gid = "+locationIDs+ " ";
								 filterFlag = true;
							 }
						 }

					
				}
					
				}
			}
			else {
				query = "select fcc.* from fcc ";
			}
			
			
				
			
			
			if(!minAPC.isEmpty()) {
				if(filterFlag) {
					query += "and fcc.apc >= "+minAPC+" ";
					
				}
				else {
					query +="where fcc.apc >= "+minAPC+" ";
				}
				 
			}
			if(!maxAPC.isEmpty()) {
				if(filterFlag) {
					query += "and fcc.apc<= "+maxAPC+" ";
				}else {
					query += "where fcc.apc <= "+maxAPC+" ";
					filterFlag = true;
				}
			}
			if(!minATC.isEmpty()) {
				if(filterFlag) {
					query += "and fcc.atc >= "+minATC+" ";
					
				}
				else {
					query +="where fcc.atc >= "+minATC+" ";
					filterFlag = true;
				}
				 
			}
			if(!maxATC.isEmpty()) {
				if(filterFlag) {
					query += "and fcc.atc<= "+maxATC+" ";
				}else {
					query += "where fcc.atc <= "+maxATC+" ";
					filterFlag = true;
				}
			}
			if(!minMembers.isEmpty()) {
				if(filterFlag) {
					query += "and fcc.members >= "+minMembers+" ";
					
				}
				else {
					query +="where fcc.members >= "+minMembers+" ";
					filterFlag = true;
				}
				 
			}
			if(!maxMembers.isEmpty()) {
				if(filterFlag) {
					query += "and fcc.members<= "+maxMembers+" ";
				}else {
					query += "where fcc.members <= "+maxMembers+" ";
					filterFlag = true;
				}
			}
			query += "group by fcc.fcc_id;";
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			Statement stmt2 = CloneRepository.getConnection().createStatement();
			stmt2.execute("use "+CloneRepository.getDBName());
			Statement stmt3 = CloneRepository.getConnection().createStatement();
			stmt3.execute("use "+CloneRepository.getDBName());
			ResultSet rs = stmt.executeQuery(query);
			filteredFccList.clear();
			while(rs.next()) {
				obj = new PrimaryFccObject();
				obj.setId(id);
		    	id++;
				obj.setFccId(rs.getInt(1));
				obj.setAtc(rs.getDouble("atc"));
				obj.setApc(rs.getDouble("apc"));
				obj.setNofInstances(rs.getInt(4));
				
				ResultSet rs2 = stmt2.executeQuery("select scc_id from fcc_scc where fcc_id="+obj.getFccId()+";");
		    	
		    	while(rs2.next())
		    	{
		    		obj.addToStructure(rs2.getInt(1));
		    	}
		    	query2 = "select a.*,b.fname from fcc_instance a, file b where a.fcc_id="+obj.getFccId()+" and a.fid=b.fid ";
		    	
		    	
		    	query2+= ";";
		    	
		    	ResultSet rs3 = stmt3.executeQuery(query2);
		    	int fid=0;
		    	while(rs3.next())
		    	{
		    		obj.addToCloneList(new SecondaryFccObject(rs3.getInt(1),rs3.getInt(3),rs3.getInt(6),rs3.getInt(4),rs3.getDouble(5),rs3.getInt(7),rs3.getString(8)));
		    	}
		    	filteredFccList.addToList(obj);
		    	
		    	rs2.close();
		    	rs3.close();
				
			}
			
			rs.close();
			
			
			
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}
		///////////////////////////////////////////////////////////////////////
		filterFlag = false;
		FileCluster fileClone = null;
		FileCloneInstance fccIntance = null;
		if(!location.isEmpty()) {
			if(location.equals("Directory")) {
				query = "select *, fcc_dir.did from fcc, fcc_dir where ";
				if(!locationIDs.isEmpty()) {
					 
					 if(locationIDs.contains(",")) {
						  IDS = locationIDs.split(",");
					 }
					 if(IDS.length>0) {
						 for(int i =0;i<IDS.length;i++) {
							 if(filterFlag) {
								 query += "or fcc_dir.did = "+IDS[i]+ " ";
							 }
							 else {
								 query += "fcc_dir.did = "+IDS[i]+ " ";
								 filterFlag = true;
							 }
							 
						 }
					 }
					 else {
						 if(filterFlag) {
							 query += "or fcc_dir.did = "+locationIDs+ " ";
						 }
						 else {
							 query += "fcc_dir.did = "+locationIDs+ " ";
							 filterFlag = true;
						 }
					 }

				
			}
			}
			else if(location.equals("Group")) {
				query = "select *, fcc_group.gid from fcc, fcc_group where ";
				if(!locationIDs.isEmpty()) {
					//String IDS[]= {};
					 if(locationIDs.contains(",")) {
						  IDS = locationIDs.split(",");
					 }
					 if(IDS.length>0) {
						 for(int i =0;i<IDS.length;i++) {
							 if(filterFlag) {
								 query += "or fcc_group.gid = "+IDS[i]+ " ";
							 }
							 else {
								 query += "fcc_group.gid = "+IDS[i]+ " ";
								 filterFlag = true;
							 }
							 
						 }
					 }
					 else {
						 if(filterFlag) {
							 query += "or fcc_group.gid = "+locationIDs+ " ";
						 }
						 else {
							 query += "fcc_group.gid = "+locationIDs+ " ";
							 filterFlag = true;
						 }
					 }

				
			}
				
			}
			
		}
		else {
			query = "select fcc.* from fcc where ";
		}
		if(!minAPC.isEmpty()) {
			if(filterFlag) {
				query += "and fcc.apc >= "+minAPC+" ";
				
			}
			else {
				query +="fcc.apc >= "+minAPC+" ";
			}
			 
		}
		if(!maxAPC.isEmpty()) {
			if(filterFlag) {
				query += "and fcc.apc<= "+maxAPC+" ";
			}else {
				query += "fcc.apc <= "+maxAPC+" ";
				filterFlag = true;
			}
		}
		if(!minATC.isEmpty()) {
			if(filterFlag) {
				query += "and fcc.atc >= "+minATC+" ";
				
			}
			else {
				query +="fcc.atc >= "+minATC+" ";
			}
			 
		}
		if(!maxATC.isEmpty()) {
			if(filterFlag) {
				query += "and fcc.atc<= "+maxATC+" ";
			}else {
				query += "fcc.atc <= "+maxATC+" ";
				filterFlag = true;
			}
		}
		if(!minMembers.isEmpty()) {
			if(filterFlag) {
				query += "and fcc.members >= "+minMembers+" ";
				
			}
			else {
				query +="fcc.members >= "+minMembers+" ";
			}
			 
		}
		if(!maxMembers.isEmpty()) {
			if(filterFlag) {
				query += "and fcc.members<= "+maxMembers+" ";
			}else {
				query += "fcc.members <= "+maxMembers+" ";
				filterFlag = true;
			}
		}
		query += "group by fcc.fcc_id;";
		try {
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use "+CloneRepository.getDBName());
			Statement stmt2 = CloneRepository.getConnection().createStatement();
			stmt2.execute("use "+CloneRepository.getDBName());
			Statement stmt3 = CloneRepository.getConnection().createStatement();
			stmt3.execute("use "+CloneRepository.getDBName());
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()) {
				//fccIntance =new FileCloneInstance();
		    	fileClone = new FileCluster();
		    	fileClone.setClusterID(rs.getInt(1));
		    	fileClone.setAverageTokenCount(rs.getDouble("atc"));
		    	fileClone.setAveragePercentageCount(rs.getDouble("apc"));
		    	fileClone.setNumberOfInstance(rs.getInt(4));
		    		
		    	ResultSet resultSetFCC_SCC = stmt2.executeQuery("select scc_id from fcc_scc where fcc_id="+fileClone.getClusterID()+";");
		    	
		    	while(resultSetFCC_SCC.next())
		    	{
		    		
		    		fileClone.addTovCloneClasses(resultSetFCC_SCC.getInt(1));
		    	}
		    	
		    	ResultSet rs3 = stmt3.executeQuery("select a.*,b.fname from fcc_instance a, file b where a.fcc_id="+fileClone.getClusterID()+" and a.fid=b.fid;");
		    	while(rs3.next())
		    	{
		    		fccIntance = new FileCloneInstance();
		    		fccIntance.setFileId(rs3.getInt(3));
		    		fccIntance.setDirectoryId(rs3.getInt(6));
		    		fccIntance.setTokenCount(rs3.getInt(4));
		    		fccIntance.setPercentageCount(rs3.getDouble(5));
		    		fccIntance.setGroupId(rs3.getInt(7));
		    		fccIntance.setFileName(rs3.getString(8));
		    		sFile file = fileList.getFile(rs3.getInt(3));
		    		fccIntance.setFile(file);
		    		fileClone.addFileCloneInstance(fccIntance);
		    	}
		    			    
		    	filteredFileClones.add(fileClone);
		    	resultSetFCC_SCC.close();
		    	rs3.close();
		    }
			
		
		}
		
		catch(Exception e) {
			e.printStackTrace();
		}
		
		
		
		
		
	}
	
	public void fileClustersXXParser_loadMethodList()
	{
		PrimaryFccObject obj;
		int id=0;
		try
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+ CloneRepository.getDBName()+";");
		    
		    Statement stmt2 = CloneRepository.getConnection().createStatement();
		    stmt2.execute("use "+ CloneRepository.getDBName()+";");
	
		    Statement stmt3 = CloneRepository.getConnection().createStatement();
		    stmt3.execute("use "+ CloneRepository.getDBName()+";");
		    
		    ResultSet rs = stmt.executeQuery("select * from fcc;");
		    while (rs.next())
		    {
		    	obj=new PrimaryFccObject();
		    	
		    	obj.setId(id);
		    	id++;
		    	obj.setFccId(rs.getInt(1));
		    	obj.setAtc(rs.getDouble("atc"));
		    	obj.setApc(rs.getDouble("apc"));
		    	obj.setNofInstances(rs.getInt(4));
		    	
		    	ResultSet rs2 = stmt2.executeQuery("select scc_id from fcc_scc where fcc_id="+obj.getFccId()+";");
		    	
		    	while(rs2.next())
		    	{
		    		obj.addToStructure(rs2.getInt(1));
		    	}
		    	
		    	ResultSet rs3 = stmt3.executeQuery("select a.*,b.fname from fcc_instance a, file b where a.fcc_id="+obj.getFccId()+" and a.fid=b.fid;");
		    	int fid=0;
		    	while(rs3.next())
		    	{
		    		obj.addToCloneList(new SecondaryFccObject(rs3.getInt(1),rs3.getInt(3),rs3.getInt(6),rs3.getInt(4),rs3.getDouble(5),rs3.getInt(7),rs3.getString(8)));
		    	}
		    	fccList.addToList(obj);
		    	
		    	rs2.close();
		    	rs3.close();
		    }
		    rs.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void loadFiles() {
		try {
		
			Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+ CloneRepository.getDBName()+";");

		    
		    ResultSet resultSetFiles = stmt.executeQuery("select * from file;");
		    sFile file;
		    while (resultSetFiles.next())
		    {
		    	
		    	file = new sFile(null);//new sFile(resultSetFiles.getInt(1));
		    	file.setFileID(resultSetFiles.getInt(1));
		    	file.setFileName(resultSetFiles.getString(2));
		    	file.setGroupID(resultSetFiles.getInt(3));
			    fileList.addsFile(file);
	    
		    }
		}	
		catch(Exception e) {
			
		}
		}
	
	
	public sFile_List getFileList() {
		return this.fileList;
	}
	
	public sFile_List getFilteredFileList() {
		return this.filteredFileList;
	}
	
	public void loadFileClones()
	{		
		FileCluster fileClone = null;
		FileCloneInstance fccIntance = null;
		try
		{
			Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+ CloneRepository.getDBName()+";");

		    
		    
		    Statement stmt2 = CloneRepository.getConnection().createStatement();
		    stmt2.execute("use "+ CloneRepository.getDBName()+";");

		    Statement stmt3 = CloneRepository.getConnection().createStatement();
		    stmt3.execute("use "+ CloneRepository.getDBName()+";");
		    
		    ResultSet resultSetFCC = stmt.executeQuery("select * from fcc;");
		    while (resultSetFCC.next())
		    {
		    	//fccIntance =new FileCloneInstance();
		    	fileClone = new FileCluster();
		    	fileClone.setClusterID(resultSetFCC.getInt(1));
		    	fileClone.setAverageTokenCount(resultSetFCC.getDouble("atc"));
		    	fileClone.setAveragePercentageCount(resultSetFCC.getDouble("apc"));
		    	fileClone.setNumberOfInstance(resultSetFCC.getInt(4));
		    		
		    	ResultSet resultSetFCC_SCC = stmt2.executeQuery("select scc_id from fcc_scc where fcc_id="+fileClone.getClusterID()+";");
		    	
		    	while(resultSetFCC_SCC.next())
		    	{
		    		
		    		fileClone.addTovCloneClasses(resultSetFCC_SCC.getInt(1));
		    	}
		    	
		    	ResultSet rs3 = stmt3.executeQuery("select a.*,b.fname from fcc_instance a, file b where a.fcc_id="+fileClone.getClusterID()+" and a.fid=b.fid;");
		    	while(rs3.next())
		    	{
		    		fccIntance = new FileCloneInstance();
		    		fccIntance.setFileId(rs3.getInt(3));
		    		fccIntance.setDirectoryId(rs3.getInt(6));
		    		fccIntance.setTokenCount(rs3.getInt(4));
		    		fccIntance.setPercentageCount(rs3.getDouble(5));
		    		fccIntance.setGroupId(rs3.getInt(7));
		    		fccIntance.setFileName(rs3.getString(8));
		    		sFile file = fileList.getFile(rs3.getInt(3));
		    		fccIntance.setFile(file);
		    		fileClone.addFileCloneInstance(fccIntance);
		    	}
		    			    
		    	fileClones.add(fileClone);
		    	resultSetFCC_SCC.close();
		    	rs3.close();
		    }
		    resultSetFCC.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public  ArrayList<FileCluster> getFileClones(){
	    	return fileClones;
	}
	public ArrayList<FileCluster> getFilteredFileClones(){
		return filteredFileClones;
	}
	
	public FccList getList()
	{
		return this.fccList;
	}
	
	public FccList getFilteredList() {
		return this.filteredFccList;
	}
	
	
	public static String getCodeSegment(String path) throws IOException
        {
		String data = "";
		File check = new File(path);
		FileReader frdr = null;
		BufferedReader buff = null;
		String line="";
		
		try {
			if(check.exists())
			{
				frdr = new FileReader(path);
	    	    buff = new BufferedReader(frdr);
	    	    while ((line = buff.readLine()) != null)
	        	    {
	    	    		data+=line+"\n";
	        	    }
			}
		}
		catch(Exception e)
		{
			
		}
    	// if(inst.getSCCID()==8)
    	// {
    	// int x=0;
    	// }
    	
//    	File check = new File(path);
//    	FileReader frdr = null;
//    	BufferedReader buff = null;
//    	String data = "";
//
//    	try {
//    	if (check.exists())
//    	{
//    	    frdr = new FileReader(path);
//    	    buff = new BufferedReader(frdr);
//
//    	} else if (!check.exists())
//    	{
//    	    String path2 = Directories.CHECKFILE;
//    	    path2 = Directories.getAbsolutePath(path2);
//    	    frdr = new FileReader(path2);
//    	    buff = new BufferedReader(frdr);
//    	    System.out.println("File not found: " + path2);
//    	}
//    	int line_number = 0;
//    	//try
//    	//{
//    	    String line = null;
//    	    while ((line = buff.readLine()) != null)
//    	    {
//    		line_number++;
//    		line = line.replace("\t", "        "); // to handle the error occur due to space and \t
//    		if (start_line == end_line)
//    		{
//    		    if (line_number == start_line)
//    		    {
//    			if (end_col <= line.length() && start_col <= line.length())
//    			{
//    			    line = line.substring(start_col - 1, end_col);
//    			}
//    			data += (line + "\n");
//    			frdr.close();
//    			buff.close();
//    			return data;
//    		    }
//    		} else if (line_number >= start_line && line_number <= end_line)
//    		{
//    		    if (line_number == start_line)
//    		    {
//    			if (start_col - 1 <= line.length())
//    			{
//    			    // line=line.replace("\t"," "); // to handle the error occur due to space and \t
//    			    line = line.substring(start_col - 1);
//    			}
//
//    		    } else if (line_number == end_line)
//    		    {
//    			if (inst != null)
//    			{
//    			    if (end_col <= line.length())
//    			    {
//    				String temp = line.substring(0, end_col);
//    				line = temp;
//    			    }
//    			} else
//    			{
//    			    if (end_col < 2)
//    			    {
//    				line = "";
//    			    } else
//    			    {
//    			    	System.out.println("LINE IS "+ line);
//    				//line = line.substring(0, end_col); //Commented by Maham Line is null.
//    			    }
//    			}
//    		    }
//    		    data += (line + "\n");
//    		}
//    	    }
//    	} catch (Exception e)
//    	{
//    	    e.printStackTrace();
//    	} finally
//    	{
//    	    frdr.close();
//    	    buff.close();
//
//    	}
    	return data;
        }

	public static void filterFCS(String minMembers, String maxMembers, String location, String locationIDS, String SccIDS) {
		// TODO Auto-generated method stub
		
	}
	
	
}
