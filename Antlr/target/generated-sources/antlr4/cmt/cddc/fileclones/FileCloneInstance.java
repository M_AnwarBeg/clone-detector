package cmt.cddc.fileclones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cddc.simpleclones.CloneInstance;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sFile;
import cmt.cfc.fccframe.FCCAnayser;
import cmt.cfc.fccframe.FCCGraphSolver;
import cmt.cfc.mccframe.GraphSolver;

public class FileCloneInstance extends CloneInstance {
	int fileCloneInstanceId;
	int fileCloneClassId;
	int fileId;
	int tokenCount;
	double percentageCount;
	int directoryId;
	int groupId;
	String fileName;
	double averageTokenCount;
	double averagePercentageCount;
	public Set<Integer> maxclique;
	private ArrayList<String> analysisDetails;
	private sFile file;
	ArrayList<SimpleCloneInstance> SCCs_Contained;
	private Collection<Set<Integer>> sortedCliques;
	private Collection<Set<Integer>> allCliques;
	private ArrayList<SimpleCloneInstance> newSccs_Contained;
	private int[][] Adjeceny_Matrix_For_Overlaps;

	public FileCloneInstance() {
		Adjeceny_Matrix_For_Overlaps = null;
		fileCloneInstanceId = -1;
		fileCloneClassId = -1;
		fileId = -1;
		tokenCount = 0;
		percentageCount = 0;
		directoryId = -1;
		groupId = -1;
		fileName = "";
		averageTokenCount = 0;
		averagePercentageCount = 0;
		SCCs_Contained = new ArrayList<SimpleCloneInstance>();
		newSccs_Contained = new ArrayList<SimpleCloneInstance>();
		sortedCliques = new ArrayList<>();
		allCliques = new ArrayList<>();
		analysisDetails = new ArrayList<String>();
	}

	public Collection<Set<Integer>> getSortedCliques() {
		return sortedCliques;
	}

	public sFile getFile() {
		return this.file;
	}

	public void setFile(sFile file) {
		this.file = file;
	}

	public void setSortedCliques(Collection<Set<Integer>> sortedCliques) {
		this.sortedCliques = sortedCliques;
	}

	public Collection<Set<Integer>> getAllCliques() {
		return allCliques;
	}

	public void setAllCliques(Collection<Set<Integer>> allCliques) {
		this.allCliques = allCliques;
	}

	public ArrayList<SimpleCloneInstance> getNewSccs_Contained() {
		return newSccs_Contained;
	}

	public void setNewSccs_Contained(ArrayList<SimpleCloneInstance> newSccs_Contained) {
		this.newSccs_Contained = newSccs_Contained;
	}

	public int[][] getAdjeceny_Matrix_For_Overlaps() {
		return Adjeceny_Matrix_For_Overlaps;
	}

	public void setAdjeceny_Matrix_For_Overlaps(int[][] adjeceny_Matrix_For_Overlaps) {
		Adjeceny_Matrix_For_Overlaps = adjeceny_Matrix_For_Overlaps;
	}

	public int getFileCloneInstanceId() {
		return fileCloneInstanceId;
	}

	public void setFileCloneInstanceId(int fileCloneInstanceId) {
		this.fileCloneInstanceId = fileCloneInstanceId;
	}

	public int getFileCloneClassId() {
		return fileCloneClassId;
	}

	public void setFileCloneClassId(int fileCloneClassId) {
		this.fileCloneClassId = fileCloneClassId;
	}

	public int getFileId() {
		return fileId;
	}

	public void setFileId(int fileId) {
		this.fileId = fileId;
	}

	public int getTokenCount() {
		return tokenCount;
	}

	public void setTokenCount(int tokenCount) {
		this.tokenCount = tokenCount;
	}

	public double getPercentageCount() {
		return percentageCount;
	}

	public void setPercentageCount(double percentageCount) {
		this.percentageCount = percentageCount;
	}

	public int getDirectoryId() {
		return directoryId;
	}

	public void setDirectoryId(int directoryId) {
		this.directoryId = directoryId;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public ArrayList<SimpleCloneInstance> getSimpleCloneInstance() {
		return SCCs_Contained;
	}

	public void setSimpleCloneInstance(ArrayList<SimpleCloneInstance> simpleCloneInstance) {
		this.SCCs_Contained = simpleCloneInstance;
	}

	public void addSimpleCloneInstance(SimpleCloneInstance sccInstanceCustom) {
		this.SCCs_Contained.add(sccInstanceCustom);
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileName() {
		return fileName;

	}

	public double getAveragePercentageCount() {
		return averagePercentageCount;
	}

	public void setAveragePercentageCount(double averagePercentageCount) {
		this.averagePercentageCount = averagePercentageCount;

	}

	public double getAverageTokenCount() {
		return averageTokenCount;
	}

	public void setAverageTokenCount(double averageTokenCount) {
		this.averageTokenCount = averageTokenCount;

	}

	public ArrayList<SimpleCloneInstance> getSCCs() {
		return SCCs_Contained;
	}

	public ArrayList<String> getAnalysisDetails() {
		return analysisDetails;
	}
	public void setAnalysisDetails(ArrayList<String> analysis)
    {
	analysisDetails = analysis;
    }
	public void getIndexFile(String path) {

		this.file.setStartToken(0);
	}
	
	public void setFileBoundry(String path) {
		try {
			this.file.setStartToken(1);
			File file = new File(path);
		    FileReader frdr = new FileReader(path);
		    BufferedReader buff =new BufferedReader(frdr);
		    String data = "";
		    int lineNumber = 0;
		    String line = null;
		    while ((line = buff.readLine()) != null)
		    {  char ch;
				  for (int i = 0; i < line.length(); i++) {
				      ch = new Character(line.charAt(i));
				      if (Character.isLetter(ch)) {
				    	  this.file.setStartToken(i);
				    	  break;
				        }
				    }
				  lineNumber ++;
			 }
		    this.file.setEndToken(lineNumber);
		    }
		
		
		catch(IOException exp) {
			System.out.println(exp.getMessage());
			
		}
		catch(Exception exp) {
			exp.getMessage();	
			}
	}
	

	public void analyzFileCloneInstance(FileCluster parentClone) throws IOException {
		String path = ProjectInfo.getFilePath(this.fileId);
		GraphSolver.SolveGraph(this);
		getAnalysisDetails().clear();
		setFileBoundry(path);
		for (SimpleCloneInstance k : getNewSccs_Contained()) {
			getAnalysisDetails().add(" ");
			getAnalysisDetails().add(Integer.toString(k.getSCCID()) + "," + k.getCodeSegment());
		}
		getAnalysisDetails().add(" ");
		getIndexFile(path);
		// String fileCode =
		try {
			if (getFile().getFileStartToken() == getNewSccs_Contained().get(0).getStartingIndex()) {
				String completelinecode = MethodClonesReader.getCodeSegment(path, getFile().getFileStartToken(), 1,
						getFile().getFileStartToken() + 1, 1, null);

				String stringtobeadded = completelinecode.substring(0, getNewSccs_Contained().get(0).getStartCol() - 1);
				getAnalysisDetails().set(0, stringtobeadded);
			} else {
				getAnalysisDetails().set(0,
						MethodClonesReader.getCodeSegment(path, getFile().getFileStartToken(), 1,
								getNewSccs_Contained().get(0).getStartingIndex(),
								getNewSccs_Contained().get(0).getStartCol() - 1, null));
			}
			// need to analyze this
			getAnalysisDetails().set(getAnalysisDetails().size() - 1,
					MethodClonesReader.getCodeSegment(path,
							FCCAnayser.getSCCInstanceWithLastLine(this).getEndingIndex(),
							FCCAnayser.getSCCInstanceWithLastLine(this).getEndCol() + 1,
							getFile().getFileEndToken() + 1, 1, null));
			int currIndex = 2;
			for (int i = 2; i < getAnalysisDetails().size() - 2; i = i + 2) {
				SimpleCloneInstance PrevInstance = FCCAnayser.getInstance(getAnalysisDetails().get(currIndex - 1),
						this);
				SimpleCloneInstance NextInstance = FCCAnayser.getInstance(getAnalysisDetails().get(currIndex + 1),
						this);
				boolean check = FCCAnayser.checkInBetweenSCCInstances(this, currIndex, PrevInstance, NextInstance,
						path);
				if (check == true) {
					currIndex = currIndex + 2;
				}
				if (currIndex > getAnalysisDetails().size() - 3) {
					break;
				}
			}
		} catch (Exception e) // replace IOException
		{
			e.printStackTrace();
		}
		
		System.out.println("FCC Analyzed");
		
	}

}
