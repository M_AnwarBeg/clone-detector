package cmt.cddc.fileclones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.Vector;

import cmt.common.Directories;
import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonerunmanager.ProjectInfo;

public class FileCloneWriterDB {
	private static String INSERT_FCC;
    private static String INSERT_FCC_INSTANCE;
    private static String INSERT_FCC_SCC;

    private static String INSERT_FCC_DIR;

    private static String INSERT_FCS_CROSSDIR;
    private static String INSERT_FCSCROSSDIR_FCC;
    private static String INSERT_FCSCROSSDIR_DIR;
    private static String INSERT_FCSCROSSDIR_FILES;

    private static String INSERT_FCS_INDIR;
    private static String INSERT_FCSINDIR_DIR;
    private static String INSERT_FCSINDIR_FCC;
    private static String INSERT_FCSINDIR_FILES;

    private static String INSERT_FCC_GROUP;

    private static String INSERT_FCS_CROSSGROUP;
    private static String INSERT_FCSCROSSGROUP_FCC;
    private static String INSERT_FCSCROSSGROUP_GROUP;
    private static String INSERT_FCSCROSSGROUP_FILES;

    private static String INSERT_FCS_INGROUP;
    private static String INSERT_FCSINGROUP_GROUP;
    private static String INSERT_FCSINGROUP_FCC;
    private static String INSERT_FCSINGROUP_FILES;
    private static String INSERT_FILESTRUCTURES;
    private static String INSERT_FILESTRUCTURES_FILES;
    private static String INSERT_FILESTRUCTURES_SCC;
    
    public static void InitQuery()
    {
    	INSERT_FCC_DIR = "INSERT INTO fcc_dir(fcc_id, did) values ";
    	INSERT_FCC = "INSERT INTO fcc(fcc_id, atc, apc, members) values ";
    	INSERT_FCC_INSTANCE = "INSERT INTO fcc_instance(fcc_instance_id, fcc_id, fid, tc, pc, did, gid) values ";
    	INSERT_FCS_CROSSDIR = "INSERT INTO fcs_crossdir(fcs_crossdir_id, members) values ";
    	INSERT_FCS_INDIR = "INSERT INTO fcs_indir(fcs_indir_id, members) values ";
    	INSERT_FCSCROSSDIR_DIR = "INSERT INTO fcscrossdir_dir(fcs_crossdir_id, did) values ";
    	INSERT_FCSCROSSDIR_FILES = "INSERT INTO fcscrossdir_files(fcs_crossdir_id, did, fcc_id, fid) values ";
    	INSERT_FCSCROSSDIR_FCC = "INSERT INTO fcscrossdir_fcc(fcs_crossdir_id, fcc_id ) values ";
    	INSERT_FCSINDIR_DIR = "INSERT INTO fcsindir_dir(fcs_indir_id, did) values ";
    	INSERT_FCSINDIR_FCC = "INSERT INTO fcsindir_fcc(fcs_indir_id, fcc_id) values ";
    	INSERT_FCC_SCC = "INSERT INTO fcc_scc(scc_id, fcc_id) values ";
    	INSERT_FCSINDIR_FILES = "INSERT INTO fcsindir_files(fcs_indir_id, did, fcc_id, fcsindir_instance_id, fid) values ";
    	INSERT_FCC_GROUP = "INSERT INTO fcc_group(fcc_id, gid) values ";
    	INSERT_FCS_CROSSGROUP = "INSERT INTO fcs_crossgroup(fcs_crossgroup_id, members) values ";
    	INSERT_FCS_INGROUP = "INSERT INTO fcs_ingroup(fcs_ingroup_id, members) values ";
    	INSERT_FCSCROSSGROUP_GROUP = "INSERT INTO fcscrossgroup_group(fcs_crossgroup_id, gid) values ";
    	INSERT_FCSCROSSGROUP_FILES = "INSERT INTO fcscrossgroup_files(fcs_crossgroup_id, gid, fcc_id, fid) values ";
    	INSERT_FCSCROSSGROUP_FCC = "INSERT INTO fcscrossgroup_fcc(fcs_crossgroup_id, fcc_id ) values ";
    	INSERT_FCSINGROUP_GROUP = "INSERT INTO fcsingroup_group(fcs_ingroup_id, gid) values ";
    	INSERT_FCSINGROUP_FCC = "INSERT INTO fcsingroup_fcc(fcs_ingroup_id, fcc_id) values ";
    	INSERT_FILESTRUCTURES = "INSERT INTO file_structures(fcs_id, instances, members) values ";
    	INSERT_FILESTRUCTURES_FILES = "INSERT INTO file_structures_files(fcs_id, fid, tc, pc, support) values ";
    	INSERT_FILESTRUCTURES_SCC = "INSERT INTO file_structures_scc(fid, sccid, scc_instance_id) values ";
    	INSERT_FCSINGROUP_FILES = "INSERT INTO fcsingroup_files(fcs_ingroup_id, gid, fcc_id, fcsingroup_instance_id, fid) values ";

    }
    
    public static void executeTransaction(String sql)
    {
		try
			{
			    if (sql.indexOf("\"") != -1)
			    {
				sql = sql.substring(0, sql.length() - 1);
			    }
			    sql = sql + ";";
			    Statement st = CloneRepository.getConnection().createStatement();
			    st.execute("use "+ CloneRepository.getDBName()+";");
			    st.execute(sql);
			    st.close();
			} 
		catch (Exception e)
			{
			    System.err.println(e.getMessage());
			    e.printStackTrace();
			}
    }
    
    public static void insertFILE_STRUCTURES(int fcsid, int instances, String members)
    {
	INSERT_FILESTRUCTURES += "( \"" + fcsid + "\" , \"" + instances + "\", \"" + members + "\"),";
    }
    
    public static void insertFILE_STRUCTURES_SCC(int fid, int sccid, int scc_instance_id)
    {
	INSERT_FILESTRUCTURES_SCC += "( \"" + fid + "\" , \"" + sccid + "\", \"" + scc_instance_id + "\"),";
    }
    
    public static void insertFILE_STRUCTURES_FILES(int fcsid, int fid, int tc, double pc, int support)
    {
	INSERT_FILESTRUCTURES_FILES += "( \"" + fcsid + "\" , \"" + fid + "\", \"" + tc + "\", \"" + pc + "\", \"" + support + "\"  ),";
    }
    
    public static void insertFCC(int fcc_id, double atc, double apc, int members)
    {
	INSERT_FCC += "( \"" + fcc_id + "\" , \"" + atc + "\", \"" + apc + "\", \"" + members + "\"  ),";
    }

    public static void insertFCS_CrossDir(int fcs_crossdir_id, int members)
    {
	INSERT_FCS_CROSSDIR += "( \"" + fcs_crossdir_id + "\", \"" + members + "\"  ),";
    }

    public static void insertFCS_InDir(int fcs_indir_id, int members)
    {
	INSERT_FCS_INDIR += "( \"" + fcs_indir_id + "\" , \"" + members + "\"  ),";
    }

    public static void insertFCSCrossDir_Dir(int fcs_crossdir_id, int did)
    {
	INSERT_FCSCROSSDIR_DIR += "( \"" + fcs_crossdir_id + "\" , \"" + did + "\"  ),";
    }

    public static void insertFCSCrossDir_Files(int fcs_crossdir_id, int did, int fcc_id, int fid)
    {
	INSERT_FCSCROSSDIR_FILES += "( \"" + fcs_crossdir_id + "\" , \"" + did + "\" , \"" + fcc_id + "\" , \"" + fid
		+ "\" ),";
    }

    public static void insertFCSCrossDir_FCC(int fcs_crossdir_id, int fcc_id)
    {
	INSERT_FCSCROSSDIR_FCC += "( \"" + fcs_crossdir_id + "\" , \"" + fcc_id + "\"  ),";
    }

    public static void insertFCSInDir_Dir(int fcs_indir_id, int did)
    {
	INSERT_FCSINDIR_DIR += "( \"" + fcs_indir_id + "\" , \"" + did + "\"  ),";
    }

    public static void insertFCSInDir_FCC(int fcs_indir_id, int fcc_id)
    {
	INSERT_FCSINDIR_FCC += "( \"" + fcs_indir_id + "\" , \"" + fcc_id + "\"  ),";
    }

    public static void insertFCSInDir_Files(int fcs_indir_id, int did, int fcc_id, int fcsindir_instance_id, int fid)
    {
	INSERT_FCSINDIR_FILES += "( \"" + fcs_indir_id + "\" , \"" + did + "\" , \"" + fcc_id + "\" ,\""
		+ fcsindir_instance_id + "\" , \"" + fid + "\"  ),";
    }
    
    public static void insertFCC_SCC(int scc_id, int fcc_id)
    {
	INSERT_FCC_SCC += "( \"" + scc_id + "\" , \"" + fcc_id + "\"  ),";
    }

    public static void insertFCC_Instance(int fcc_instance_id, int fcc_id, int fid, double tc, double pc)
    {
	INSERT_FCC_INSTANCE += "( \"" + fcc_instance_id + "\" ,\"" + fcc_id + "\" , \"" + fid + "\" , \"" + tc
		+ "\" , \"" + pc + "\", \"" + ProjectInfo.getDidFromFid(fid) + "\" , \"" + ProjectInfo.getGidFromFid(fid) + "\"  ),";
    }

    public static void insertFCC_Dir(int fcc_id, int did)
    {
	INSERT_FCC_DIR += "( \"" + fcc_id + "\" , \"" + did + "\"  ),";
    }
    
    public static void insertFCSInGroup_Files(int fcs_ingroup_id, int gid, int fcc_id, int fcsingroup_instance_id,
    	    int fid)
        {
    	INSERT_FCSINGROUP_FILES += "( \"" + fcs_ingroup_id + "\" , \"" + gid + "\" , \"" + fcc_id + "\" ,\""
    		+ fcsingroup_instance_id + "\" , \"" + fid + "\"  ),";
        }

        public static void insertFCS_CrossGroup(int fcs_crossgroup_id, int members)
        {
    	INSERT_FCS_CROSSGROUP += "( \"" + fcs_crossgroup_id + "\", \"" + members + "\"  ),";
        }

        public static void insertFCS_InGroup(int fcs_ingroup_id, int members)
        {
    	INSERT_FCS_INGROUP += "( \"" + fcs_ingroup_id + "\" , \"" + members + "\"  ),";
        }

        public static void insertFCSCrossGroup_Group(int fcs_crossgroup_id, int gid)
        {
    	INSERT_FCSCROSSGROUP_GROUP += "( \"" + fcs_crossgroup_id + "\" , \"" + gid + "\"  ),";
        }

        public static void insertFCSCrossGroup_Files(int fcs_crossgroup_id, int gid, int fcc_id, int fid)
        {
    	INSERT_FCSCROSSGROUP_FILES += "( \"" + fcs_crossgroup_id + "\" , \"" + gid + "\" , \"" + fcc_id + "\" , \""
    		+ fid + "\" ),";
        }

        public static void insertFCSCrossGroup_FCC(int fcs_crossgroup_id, int fcc_id)
        {
    	INSERT_FCSCROSSGROUP_FCC += "( \"" + fcs_crossgroup_id + "\" , \"" + fcc_id + "\"  ),";
        }

        public static void insertFCSInGroup_Group(int fcs_ingroup_id, int gid)
        {
    	INSERT_FCSINGROUP_GROUP += "( \"" + fcs_ingroup_id + "\" , \"" + gid + "\"  ),";
        }

        public static void insertFCSInGroup_FCC(int fcs_ingroup_id, int fcc_id)
        {
    	INSERT_FCSINGROUP_FCC += "( \"" + fcs_ingroup_id + "\" , \"" + fcc_id + "\"  ),";
        }

        public static void insertFCC_Group(int fcc_id, int gid)
        {
    	INSERT_FCC_GROUP += "( \"" + fcc_id + "\" , \"" + gid + "\"  ),";
        }
    
    
	public static void populateDatabase(int dlm)
	{
		try
		{
			StringTokenizer st, st1, st2;
		    String line, fid, filePath = null;
		    
		    String temp;

		    filePath = Directories.getAbsolutePath(Directories.FILE_CLUSTER);
		    File file4 = new File(filePath);
		    FileInputStream filein4 = new FileInputStream(file4);
		    BufferedReader stdin4 = new BufferedReader(new InputStreamReader(filein4));
		    while ((line = stdin4.readLine()) != null)
		    {
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line, ";");
			    String cid = st.nextToken();
			    int fcc_id = Integer.parseInt(cid);
			    String support = st.nextToken();
			    String sccs = stdin4.readLine();
			    st2 = new StringTokenizer(sccs, ",");
			    Vector<String> sccVector = new Vector<>();
			    while (st2.hasMoreTokens())
			    {
				sccVector.add(st2.nextToken());
			    }
			    for (int i = 0; i < sccVector.size(); i++)
			    {
				insertFCC_SCC(Integer.parseInt(sccVector.get(i)), fcc_id);
			    }
			    int sup = (new Integer(support)).intValue();
			    double atc = 0;
			    double apc = 0;
			    for (int i = 0; i < sup; i++)
			    {
				line = stdin4.readLine();
				st1 = new StringTokenizer(line, ";,");
				fid = st1.nextToken();
				String tk = st1.nextToken();
				String coverage = st1.nextToken();
				atc += Double.parseDouble(tk);
				apc += Double.parseDouble(coverage);
				insertFCC_Instance(i, fcc_id, Integer.parseInt(fid), Double.parseDouble(tk),
					Double.parseDouble(coverage));
			    }
			    atc = atc / sup;
			    apc = apc / sup;
			    insertFCC(fcc_id, atc, apc, sup);
			}
		    }

		    if (!INSERT_FCC_SCC.equalsIgnoreCase("INSERT INTO fcc_scc(scc_id, fcc_id) values "))
		    {
			executeTransaction(INSERT_FCC_SCC);
		    }
		    if (!INSERT_FCC_INSTANCE.equalsIgnoreCase(
			    "INSERT INTO fcc_instance(fcc_instance_id, fcc_id, fid, tc, pc, did, gid) values "))
		    {
			executeTransaction(INSERT_FCC_INSTANCE);
		    }
		    if (!INSERT_FCC.equalsIgnoreCase("INSERT INTO fcc(fcc_id, atc, apc, members) values "))
		    {
			executeTransaction(INSERT_FCC);
		    }
		    
		    // File Structures Parser for FileClones across or within files
		    
		    filePath = Directories.getAbsolutePath(Directories.FILE_STRUCTURES);
		    File file21 = new File(filePath);
		    int fcsid=-1,numberOfInstances=-1;
		    String members="";
		    FileInputStream filein21 = new FileInputStream(file21);
		    BufferedReader stdin21 = new BufferedReader(new InputStreamReader(filein21));
		    while ((line = stdin21.readLine()) != null)
		    {
		    	st=new StringTokenizer(line," ");
		    	fcsid=Integer.parseInt(st.nextToken());
		    	numberOfInstances=Integer.parseInt(st.nextToken());
		    	
		    	line=stdin21.readLine();
		    	members=line;
		    	
		    	insertFILE_STRUCTURES(fcsid,numberOfInstances,members);
		    	
		    	int fileid=-1,sccid=-1,scc_instance_id=-1,tc=-1;
		    	double pc=-1;
		    	StringTokenizer st21,st22,st23,st24;
		    	for(int u=0;u<numberOfInstances;u++)
		    	{
		    		line=stdin21.readLine();
		    		line=line.replace(" ","");
		    		
		    		st1=new StringTokenizer(line,":");
		    		fileid=Integer.parseInt(st1.nextToken());
		    		
		    		String test= st1.nextToken();
		    		st21=new StringTokenizer(test,"]");
		    		
		    		st24=new StringTokenizer(members,",");
			    		while(st24.hasMoreTokens())
				    	{
			    			st24.nextToken();
			    			String token=st21.nextToken();
			    			if(token.equals("") || token.equals(" "))
			    			{
			    				token= st21.nextToken();
			    			}
			    			token=token.replace("[","");
				    		st22=new StringTokenizer(token,",");
				    		
				    		while(st22.hasMoreTokens())
				    		{
				    			st23=new StringTokenizer(st22.nextToken(), ".");
				    			sccid=Integer.parseInt(st23.nextToken());
				    			scc_instance_id=Integer.parseInt(st23.nextToken());
				    			insertFILE_STRUCTURES_SCC(fileid,sccid,scc_instance_id);
				    		}
				    	}
			    	
			    		String check=st21.nextToken();
			    	st23=new StringTokenizer(check,"|");
			    	tc=Integer.parseInt(st23.nextToken().replace("(","").replace(",",""));
			    	
			    	st22=new StringTokenizer(st23.nextToken(),"%");
			    	pc=Double.parseDouble(st22.nextToken());
			    	int support = Integer.parseInt(st22.nextToken().replace("(", "").replace(")", ""));
			    	
			    	insertFILE_STRUCTURES_FILES(fcsid,fileid,tc,pc,support);
			    	
			    	u += (support-1);
		    	}
		    	line=stdin21.readLine();
		    }
		    
		    if (!INSERT_FILESTRUCTURES.equalsIgnoreCase("INSERT INTO file_structures(fcs_id, instances, members) values "))
		    {
			executeTransaction(INSERT_FILESTRUCTURES);
		    }
		    if (!INSERT_FILESTRUCTURES_FILES.equalsIgnoreCase("INSERT INTO file_structures_files(fcs_id, fid, tc, pc, support) values "))
		    {
			executeTransaction(INSERT_FILESTRUCTURES_FILES);
		    }
		    if (!INSERT_FILESTRUCTURES_SCC.equalsIgnoreCase("INSERT INTO file_structures_scc(fid, sccid, scc_instance_id) values "))
		    {
			executeTransaction(INSERT_FILESTRUCTURES_SCC);
		    }
		    
		    
		    filePath = Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_DIR_EX);
		    File file13 = new File(filePath);
		    FileInputStream filein13 = new FileInputStream(file13);
		    BufferedReader stdin13 = new BufferedReader(new InputStreamReader(filein13));
		    int fccId,size=0,dId;
		    int cluster_size;
		    Vector<String> fccs = null;
		    while ((line = stdin13.readLine()) != null)
		    {
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line);
			    String sid = st.nextToken();
			    String number = st.nextToken();
			    int nm = Integer.parseInt(number);
			    line = stdin13.readLine();
			    st1 = new StringTokenizer(line, ",");
			    size = st1.countTokens();
			    fccs = new Vector<>();
			    cluster_size = 0;
			    String pre = "", temp1;
			    for (int i = 0; i < size; i++)
			    {
					temp1 = st1.nextToken();
					//if (!pre.equalsIgnoreCase(temp1))
					{
					    fccs.add(temp1);
					    cluster_size++;
					}
					insertFCSCrossDir_FCC(Integer.parseInt(sid), Integer.parseInt(temp1));
					pre = temp1;
			    }
			    for (int i = 0; i < nm; i++)
			    {
				    line = stdin13.readLine();
					line = stdin13.readLine();
					dId = Integer.parseInt(line.trim());
					insertFCSCrossDir_Dir(Integer.parseInt(sid), dId);
					
					for(int k=0; k<cluster_size; k++)
					{
					    fccId = Integer.parseInt(fccs.get(k).trim());
					    line = stdin13.readLine();
					    st2 = new StringTokenizer(line, ",");
					    while (st2.hasMoreTokens())
					    {
							fid = st2.nextToken();
							insertFCSCrossDir_Files(Integer.parseInt(sid), dId, fccId, Integer.parseInt(fid));
					    }
					}
			    }
			    line = stdin13.readLine();
			    insertFCS_CrossDir(Integer.parseInt(sid), nm);
			}
		    }

		    if (!INSERT_FCSCROSSDIR_FCC
			    .equalsIgnoreCase("INSERT INTO fcscrossdir_fcc(fcs_crossdir_id, fcc_id ) values "))
		    {
			executeTransaction(INSERT_FCSCROSSDIR_FCC);
		    }
		    if (!INSERT_FCSCROSSDIR_DIR.equalsIgnoreCase("INSERT INTO fcscrossdir_dir(fcs_crossdir_id, did) values "))
		    {
			executeTransaction(INSERT_FCSCROSSDIR_DIR);
		    }
		    if (!INSERT_FCSCROSSDIR_FILES
			    .equalsIgnoreCase("INSERT INTO fcscrossdir_files(fcs_crossdir_id, did, fcc_id, fid) values "))
		    {
			executeTransaction(INSERT_FCSCROSSDIR_FILES);
		    }
		    if (!INSERT_FCS_CROSSDIR.equalsIgnoreCase("INSERT INTO fcs_crossdir(fcs_crossdir_id, members) values "))
		    {
			executeTransaction(INSERT_FCS_CROSSDIR);
		    }
		    
		    filePath = Directories.getAbsolutePath(Directories.IN_DIR_STRUCTURE);
		    File file14 = new File(filePath);
		    FileInputStream filein14 = new FileInputStream(file14);
		    BufferedReader stdin14 = new BufferedReader(new InputStreamReader(filein14));
		    temp = null;
		    int count = 0;
		    int count0 = 0;
		    int count1 = 0;
		    fccs = null;
		    size = ProjectInfo.getDirectorySize();
		    int fileSize = 0;
		    for (int i = 0; i < size; i++)
		    {
			line = stdin14.readLine();
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line, "()");
			    int loop = st.countTokens() / 2;
			    for (int j = 0; j < loop; j++)
			    {
				int inst = (new Integer(st.nextToken())).intValue();
				String pat = st.nextToken();
				StringTokenizer stPat = new StringTokenizer(pat, ",");
				cluster_size = 0;
				String pre = "";
				fccs = new Vector<>();
				while (stPat.hasMoreTokens())
				{
				    temp = stPat.nextToken();
				    if (!pre.equalsIgnoreCase(temp))
				    {
					fccs.add(temp);
					cluster_size++;
				    }
				    insertFCSInDir_FCC(count, Integer.parseInt(temp));
				    pre = temp;
				}

				Vector<String> files = null;
				dId = count0;
				for (int k = 0; k < cluster_size; k++)
				{
				    fccId = Integer.parseInt(fccs.get(k));
				    files = ProjectInfo.getFCSInDir_Files(dId, fccId);
				    if (files != null)
				    {
					fileSize = files.size();
					count1 = 0;
					for (int n = 0; n < inst; n++)
					{
					    for (int q = 0; q < fileSize / inst; q++)
					    {
						insertFCSInDir_Files(count, dId, fccId, n, Integer.parseInt(files.get(count1)));
						count1++;
					    }
					}
				    }
				}
				insertFCS_InDir(count, inst);
				insertFCSInDir_Dir(count, count0);
				count++;
			    }
			}
			count0++;
		    }

		    if (!INSERT_FCS_INDIR.equalsIgnoreCase("INSERT INTO fcs_indir(fcs_indir_id, members) values "))
		    {
			executeTransaction(INSERT_FCS_INDIR);
		    }
		    if (!INSERT_FCSINDIR_FCC.equalsIgnoreCase("INSERT INTO fcsindir_fcc(fcs_indir_id, fcc_id) values "))
		    {
			executeTransaction(INSERT_FCSINDIR_FCC);
		    }
		    if (!INSERT_FCSINDIR_DIR.equalsIgnoreCase("INSERT INTO fcsindir_dir(fcs_indir_id, did) values "))
		    {
			executeTransaction(INSERT_FCSINDIR_DIR);
		    }
		    if (!INSERT_FCSINDIR_FILES.equalsIgnoreCase(
			    "INSERT INTO fcsindir_files(fcs_indir_id, did, fcc_id, fcsindir_instance_id, fid) values "))
		    {
			executeTransaction(INSERT_FCSINDIR_FILES);
		    }

		    filePath = Directories.getAbsolutePath(Directories.FILE_CLONES_BY_DIR);
		    File file15 = new File(filePath);
		    FileInputStream filein15 = new FileInputStream(file15);
		    BufferedReader stdin15 = new BufferedReader(new InputStreamReader(filein15));
		    int dirPosn = 0;
		    while ((line = stdin15.readLine()) != null)
		    {
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line, ",");
			    while (st.hasMoreTokens())
			    {
				String s2 = st.nextToken().trim();
				if (!s2.equalsIgnoreCase(""))
				{
				    insertFCC_Dir(Integer.parseInt(s2), dirPosn);
				}
			    }
			}
			dirPosn++;
		    }

		    if (!INSERT_FCC_DIR.equalsIgnoreCase("INSERT INTO fcc_dir(fcc_id, did) values "))
		    {
			executeTransaction(INSERT_FCC_DIR);
		    }

		    int gId;
		    filePath = Directories.getAbsolutePath(Directories.FILE_STRUCTURE_CROSS_GROUP_EX);
		    File file16 = new File(filePath);
		    FileInputStream filein16 = new FileInputStream(file16);
		    BufferedReader stdin16 = new BufferedReader(new InputStreamReader(filein16));
		    while ((line = stdin16.readLine()) != null)
		    {
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line);
			    String sid = st.nextToken();
			    String number = st.nextToken();
			    int nm = Integer.parseInt(number);
			    line = stdin16.readLine();
			    st1 = new StringTokenizer(line, ",");
			    size = st1.countTokens();
			    fccs = new Vector<>();
			    cluster_size = 0;
			    String pre = "", temp1;
			    for (int i = 0; i < size; i++)
			    {
				temp1 = st1.nextToken();
				if (!pre.equalsIgnoreCase(temp1))
				{
				    fccs.add(temp1);
				    cluster_size++;
				}
				insertFCSCrossGroup_FCC(Integer.parseInt(sid), Integer.parseInt(temp1));
				pre = temp1;
			    }
			    for (int i = 0; i < nm; i++)
			    {
				line = stdin16.readLine();
				line = stdin16.readLine();
				if (line != null)
				{
				    gId = Integer.parseInt(line.trim());
				    insertFCSCrossGroup_Group(Integer.parseInt(sid), gId);
				    for (int j = 0; j < cluster_size; j++)
				    {
					fccId = Integer.parseInt(fccs.get(j).trim());
					line = stdin16.readLine();
					st2 = new StringTokenizer(line, ",");
						while (st2.hasMoreTokens())
						{
						    fid = st2.nextToken();
						    insertFCSCrossGroup_Files(Integer.parseInt(sid), gId, fccId, Integer.parseInt(fid));
						}
				    }
				} 
				else
					{
	
					}
			    }
			    line = stdin16.readLine();
			    insertFCS_CrossGroup(Integer.parseInt(sid), nm);
			}
		    }

		    if (!INSERT_FCSCROSSGROUP_FCC
			    .equalsIgnoreCase("INSERT INTO fcscrossgroup_fcc(fcs_crossgroup_id, fcc_id ) values "))
		    {
			executeTransaction(INSERT_FCSCROSSGROUP_FCC);
		    }
		    if (!INSERT_FCSCROSSGROUP_GROUP
			    .equalsIgnoreCase("INSERT INTO fcscrossgroup_group(fcs_crossgroup_id, gid) values "))
		    {
			executeTransaction(INSERT_FCSCROSSGROUP_GROUP);
		    }
		    if (!INSERT_FCSCROSSGROUP_FILES
			    .equalsIgnoreCase("INSERT INTO fcscrossgroup_files(fcs_crossgroup_id, gid, fcc_id, fid) values "))
		    {
			executeTransaction(INSERT_FCSCROSSGROUP_FILES);
		    }
		    if (!INSERT_FCS_CROSSGROUP
			    .equalsIgnoreCase("INSERT INTO fcs_crossgroup(fcs_crossgroup_id, members) values "))
		    {
			executeTransaction(INSERT_FCS_CROSSGROUP);
		    }

		    filePath = Directories.getAbsolutePath(Directories.IN_GROUP_STRUCTURE);
		    File file17 = new File(filePath);
		    FileInputStream filein17 = new FileInputStream(file17);
		    BufferedReader stdin17 = new BufferedReader(new InputStreamReader(filein17));
		    temp = null;
		    count = 0;
		    count0 = 0;
		    count1 = 0;
		    fccs = null;
		    size = ProjectInfo.getGroupSize();
		    for (int i = 0; i < size; i++)
		    {
			line = stdin17.readLine();
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line, "()");
			    int loop = st.countTokens() / 2;
			    for (int j = 0; j < loop; j++)
			    {
				int inst = (new Integer(st.nextToken())).intValue();
				String pat = st.nextToken();
				StringTokenizer stPat = new StringTokenizer(pat, ",");
				cluster_size = 0;
				String pre = "";
				fccs = new Vector<>();
				while (stPat.hasMoreTokens())
				{
				    temp = stPat.nextToken();
				    if (!pre.equalsIgnoreCase(temp))
				    {
					fccs.add(temp);
					cluster_size++;
				    }
				    insertFCSInGroup_FCC(count, Integer.parseInt(temp));
				    pre = temp;
				}

				Vector<String> files = null;
				gId = count0;
				for (int k = 0; k < cluster_size; k++)
				{
				    fccId = Integer.parseInt(fccs.get(k));
				    files = ProjectInfo.getFCSInGroup_Files(gId, fccId);
				    if (files != null)
				    {
					fileSize = files.size();
					count1 = 0;
					for (int n = 0; n < inst; n++)
					{
					    for (int q = 0; q < fileSize / inst; q++)
					    {
						insertFCSInGroup_Files(count, gId, fccId, n,
							Integer.parseInt(files.get(count1)));
						count1++;
					    }
					}
				    }
				}
				insertFCS_InGroup(count, inst);
				insertFCSInGroup_Group(count, count0);
				count++;
			    }
			}
			count0++;
		    }

		    if (!INSERT_FCS_INGROUP.equalsIgnoreCase("INSERT INTO fcs_ingroup(fcs_ingroup_id, members) values "))
		    {
			executeTransaction(INSERT_FCS_INGROUP);
		    }
		    if (!INSERT_FCSINGROUP_FCC.equalsIgnoreCase("INSERT INTO fcsingroup_fcc(fcs_ingroup_id, fcc_id) values "))
		    {
			executeTransaction(INSERT_FCSINGROUP_FCC);
		    }
		    if (!INSERT_FCSINGROUP_GROUP.equalsIgnoreCase("INSERT INTO fcsingroup_group(fcs_ingroup_id, gid) values "))
		    {
			executeTransaction(INSERT_FCSINGROUP_GROUP);
		    }
		    if (!INSERT_FCSINGROUP_FILES.equalsIgnoreCase(
			    "INSERT INTO fcsingroup_files(fcs_ingroup_id, gid, fcc_id, fcsingroup_instance_id, fid) values "))
		    {
			executeTransaction(INSERT_FCSINGROUP_FILES);
		    }

		    filePath = Directories.getAbsolutePath(Directories.FILE_CLONES_BY_GROUP);
		    File file18 = new File(filePath);
		    FileInputStream filein18 = new FileInputStream(file18);
		    BufferedReader stdin18 = new BufferedReader(new InputStreamReader(filein18));
		    int groupPosn = 0;
		    while ((line = stdin18.readLine()) != null)
		    {
			if (!line.equalsIgnoreCase(""))
			{
			    st = new StringTokenizer(line, ",");
			    while (st.hasMoreTokens())
			    {
				String s2 = st.nextToken().trim();
				if (!s2.equalsIgnoreCase(""))
				{
				    insertFCC_Group(Integer.parseInt(s2), groupPosn);
				}
			    }
			}
			groupPosn++;
		    }

		    if (!INSERT_FCC_GROUP.equalsIgnoreCase("INSERT INTO fcc_group(fcc_id, gid) values "))
		    {
			executeTransaction(INSERT_FCC_GROUP);
		    }
		    

		    filein4.close();
		    //filein13.close();
		    filein14.close();
		    filein15.close();
		    //filein16.close();
		    filein17.close();
		    filein18.close();
		    
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
