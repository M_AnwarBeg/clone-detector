package cmt.cddc.clonerunmanager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.structures.sGroup;

public class CloneRunInfoReader {
	 private static  ArrayList<CloneRunConfiguration> cloneRunConfigurations = new ArrayList<CloneRunConfiguration>();
	 static ArrayList<ArrayList<String>> cloneRunNames = new ArrayList<ArrayList<String>>();
	// static HashSet <String> newlyAddedFiles = new HashSet<String>();
	 static Hashtable <Integer, String> allCloneRunFiles = new Hashtable<Integer, String>();
	 static ArrayList<String> databaseNames = new ArrayList<String>();
	 
	 public static ArrayList<ArrayList<String>> getCloneRunNames(){
	 try {
			 cloneRunNames.clear();
			// newlyAddedFiles.clear();
			 allCloneRunFiles.clear(); 
			 
			 Statement stmt = CloneRepository.getConnection().createStatement();
			 stmt.execute("use "+CloneRepository.getDBName());
			 String query = "SELECT COUNT(fname), gid FROM FILE GROUP BY gid ORDER BY gid ASC;";
			 ResultSet rs = stmt.executeQuery(query);
			 ArrayList<String> FileNames = new ArrayList<String>();
			 while(rs.next()) {
				 int fileCount = rs.getInt(1);
					int gid = rs.getInt(2);
					String query02 = "SELECT file.fid, file.fname, file.gid, file_directory.did, directory.dname FROM FILE "
							+ "INNER JOIN file_directory ON file.fid = file_directory.fid INNER JOIN DIRECTORY ON file_directory.did = "
							+ "directory.did where file.gid = "+gid+";";
					Statement stmt2 = CloneRepository.getConnection().createStatement();
					ResultSet rs2 = stmt2.executeQuery(query02);
					while(rs2.next()) {
						
						
						String name =rs2.getString(5).replaceAll("/", "\\\\")+"\\"+rs2.getString(2);
						
						if(name.endsWith(";")) {
							name = name.substring(0, (name.length()-1));
							
						}
						FileNames.add(name);
						allCloneRunFiles.put(rs2.getInt(1), name);
					//	newlyAddedFiles.add(name);
						
						
						
					}
					cloneRunNames.add(FileNames);
					FileNames = new ArrayList<String>();
					
				 
			 }
			 
		 }catch(Exception ex) {
			 ex.printStackTrace();
		 }
		 
		 return cloneRunNames;
		 
	 }
	 
	 
	public static HashMap<Integer, ArrayList<String>> getCloneRunFiles() {
		HashMap<Integer, ArrayList<String>> mapGroupToFile = new HashMap<Integer, ArrayList<String>>();
		try {
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use " + CloneRepository.getDBName());
			String query02 = "SELECT file.fid, file.fname, file.gid, file_directory.did, directory.dname FROM FILE "
					+ "INNER JOIN file_directory ON file.fid = file_directory.fid INNER JOIN DIRECTORY ON file_directory.did = "
					+ "directory.did ;";
			ResultSet rs2 = stmt.executeQuery(query02);
			while (rs2.next()) {
				String absolutefilePath = rs2.getString(5).replaceAll("/", "\\\\") + "\\" + rs2.getString(2);
				int groupId = rs2.getInt(2);
				if (mapGroupToFile.containsKey(groupId)) {
					ArrayList<String> groupFiles = mapGroupToFile.get(rs2.getInt(2));
					groupFiles.add(absolutefilePath);
				} else {
					ArrayList<String> groupFiles = new ArrayList<String>();
					mapGroupToFile.put(groupId, groupFiles);
					groupFiles.add(absolutefilePath);
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mapGroupToFile;
	}
	 
	 
	public static Map<Integer, sGroup> getGroups() {
		Map<Integer, sGroup> mapGroupToFile = new TreeMap<Integer,sGroup>();
		try {
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.execute("use " + CloneRepository.getDBName());
		/*	String query02 = "SELECT file.fid, file.fname, file.gid, file_directory.did, directory.dname FROM FILE "
					+ "INNER JOIN file_directory ON file.fid = file_directory.fid INNER JOIN DIRECTORY ON file_directory.did = "
					+ "directory.did ;";
			*/
			String query02 = "SELECT file.fid, file.fname, file.gid, file_directory.did, directory.dname, groupinfo.gname, groupinfo.gpath FROM FILE " + 
					"INNER JOIN file_directory ON file.fid = file_directory.fid INNER JOIN DIRECTORY ON " + 
					"file_directory.did = directory.did INNER JOIN groupinfo ON groupinfo.gid = file.gid  ;";
			
			ResultSet rs2 = stmt.executeQuery(query02);
			while (rs2.next()) {
				int fileId  = rs2.getInt(1);
				int groupId = rs2.getInt(3);
				String absolutefilePath = rs2.getString(5).replaceAll("/", "\\\\") + "\\" + rs2.getString(2);
				String rootFolder = rs2.getString(7).trim().replaceAll("/", "\\\\"); 
				String groupName = rs2.getString(6).trim();
				if (mapGroupToFile.containsKey(groupId)) {
					
					sGroup group = mapGroupToFile.get(groupId);
					group.getGroupFileIds().put(absolutefilePath,fileId);
				} else {  
					sGroup group = new sGroup();
					group.setGroupID(groupId);
					group.setRootPath(rootFolder);
					group.setGroupName(groupName);
					group.getGroupFileIds().put(absolutefilePath,fileId);
					mapGroupToFile.put(groupId, group);
	
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mapGroupToFile;
	}
	
	 
	 
	 
	 
	 
	 

	 /*	 public static HashSet<String> getExistingFileNames(){
		 return newlyAddedFiles;
	 } */ 
	 
	 

	 public static Hashtable<Integer, String> getFileListID(){
		 return allCloneRunFiles;
	 }
		private static void loadCloneRunsConfigurations() {
			cloneRunConfigurations.clear();
			try {
				databaseNames = new ArrayList<String>();
				Statement st = CloneRepository.getConnection().createStatement();
				String sqlQuery = "show Databases;";
				ResultSet rsDataBaseNames = st.executeQuery(sqlQuery);
				String databaseName = ""; 
				while(rsDataBaseNames.next()) {
					databaseName = rsDataBaseNames.getString("Database"); 
					String clonerepository= "clonerepository";
					if(databaseName.contains(clonerepository)) {
						databaseNames.add(databaseName);				}
				}
				for(int i =0; i<databaseNames.size();i++) {
					
					String sqlDBNameQuery = "use "+databaseNames.get(i)+";";
					String sqlSysconfQuery = "select * from sysconf;";
					Long time = Long.parseLong(databaseNames.get(i).substring((databaseNames.get(i).lastIndexOf("_")+1), databaseNames.get(i).length()));
					st.execute(sqlDBNameQuery);
					ResultSet rsSysConfig = st.executeQuery(sqlSysconfQuery);
					CloneRunConfiguration cloneRunConfig = new CloneRunConfiguration();
					cloneRunConfig.setCloneRepositoryName(databaseNames.get(i));
					while(rsSysConfig.next()) {					
						//cloneRunConfig.setLangChoice(rs.getString("languageselected"));
						cloneRunConfig.setProjectName(rsSysConfig.getString("projectname"));		
					//	System.out.println("Clone Run Name: "+rsSysConfig.getString("projectname"));
						cloneRunConfig.setProjectDiscrp(rsSysConfig.getString("projectdiscrp"));
					//	System.out.println("Clone Run Discp: "+rsSysConfig.getString("projectdiscrp"));
						
						//cloneRunConfig.setLangChoice(rs.getString("languageselected"));
						cloneRunConfig.setProjectDiscrp(rsSysConfig.getString("projectdiscrp"));					
						cloneRunConfig.setGroupCheck(rsSysConfig.getInt("groupcheck"));
						cloneRunConfig.setThreshold(rsSysConfig.getInt("threshold"));
						cloneRunConfig.setMinFClusP(rsSysConfig.getInt("min_fileclusp"));
						cloneRunConfig.setMinFClusT(rsSysConfig.getInt("min_fileclust"));
						cloneRunConfig.setMinMClusP(rsSysConfig.getInt("min_methodclusp"));
						cloneRunConfig.setMinMClusT(rsSysConfig.getInt("min_methodclust"));
						cloneRunConfig.setCreationTime(Long.parseLong(rsSysConfig.getString("createdate")));
						cloneRunConfig.setModificationTime(Long.parseLong(rsSysConfig.getString("lastupdate")));
						cloneRunConfig.setEnableFraming(rsSysConfig.getBoolean("enableframing"));
						ArrayList<ArrayList<Integer>> equateTokens = getEquateTokens();
						cloneRunConfig.setEquateTokens(equateTokens);
						ArrayList<Integer> supressedTokens = getSupressedTokens();
						cloneRunConfig.setSupressTokensIds(supressedTokens);
						cloneRunConfigurations.add(cloneRunConfig);
					}
					rsSysConfig.close();
				}
				rsDataBaseNames.close();
				
			}
			catch(SQLException ex) {
				 ex.printStackTrace();
				
			}
		}	
		public static ArrayList<CloneRunConfiguration> getCloneRuns() {
			loadCloneRunsConfigurations();
			return cloneRunConfigurations;
		}
		public static ArrayList<ArrayList<Integer>> getEquateTokens() {	
			Map<Integer, List<Integer>> rules = new HashMap<Integer, List<Integer>>();
			ResultSet resultSet = null;
			try {
			Statement st = CloneRepository.getConnection().createStatement();
			String  equateTokensquery= "select * from sysconfig_equate_tokens";
			 resultSet = st.executeQuery(equateTokensquery);		
			 while(resultSet.next()) {
					int ruleNumber = resultSet.getInt("rule_number"); 
					int token_id = resultSet.getInt("token_id");
					if(rules.containsKey(ruleNumber)) {
						ArrayList<Integer> rule = (ArrayList<Integer>) rules.get(ruleNumber);
						rule.add(token_id);	
						}
					else {
						List<Integer> rule = new ArrayList<Integer>();
						rule.add(token_id);
						rules.put(ruleNumber, rule);					
					}
			}			
			}
			catch(SQLException sqlException) {
				sqlException.printStackTrace();
			}		
			catch(Exception exp) {
			 exp.printStackTrace();
			}
			ArrayList<ArrayList<Integer>> equateTokens = new ArrayList<ArrayList<Integer>>();		
			 for (Entry<Integer, List<Integer>> entry : rules.entrySet()) {
				 List<Integer> rule = entry.getValue();
				 equateTokens.add((ArrayList<Integer>) rule);
			}
			 return equateTokens;
		}
		
		public static ArrayList<Integer> getSupressedTokens() {
			ArrayList<Integer> supressedTokenValues = new ArrayList<Integer>();
			ResultSet resultSet = null;
			try {
			Statement st = CloneRepository.getConnection().createStatement();
			String  equateTokensquery= "select * from sysconfig_supresstoken";
			 resultSet = st.executeQuery(equateTokensquery);		
			 while(resultSet.next()) {
					supressedTokenValues.add(resultSet.getInt("token_id"));
			 }			
			}
			catch(SQLException sqlException) {
				sqlException.printStackTrace();
			}		
			catch(Exception exp) {
			 exp.printStackTrace();
			}
			return supressedTokenValues;
			
		}	



}
