package cmt.cddc.clonerunmanager;

import java.util.ArrayList;
import antlr.auto.gen.java.JavaLexer;

public class CloneRunConfiguration {
	private int langChoice;
	private ArrayList<ArrayList<Integer>> equateTokens;
	private int threshold;
	private int groupCheck;
	private ArrayList<Integer> supressTokensIds = new ArrayList<Integer>();
	private static boolean[] supressTokens = new boolean[JavaLexer.ruleNames.length + 1];
	private int minFClusP = 0;
	private int minFClusT = 0;
	private int minMClusP = 0;
	private int minMClusT = 0;
	private String projectName;
	private String projectDiscrp;
	private long creationTime;
	private long modificationTime;
	private String cloneRepositoryName;
	private Boolean enableFraming = false;

	public Boolean getEnableFraming() {
		return enableFraming;
	}

	public void setEnableFraming(Boolean enableFraminf) {
		enableFraming = enableFraminf;
	}

	public String getCloneRepositoryName() {
		return cloneRepositoryName;
	}

	public void setCloneRepositoryName(String cloneRepositoryName) {
		this.cloneRepositoryName = cloneRepositoryName;
	}

	public int getLangChoice() {
		return langChoice;
	}

	public void setLangChoice(int langChoice) {
		this.langChoice = langChoice;
	}

	public ArrayList<ArrayList<Integer>> getEquateTokens() {
		return equateTokens;
	}

	public void setEquateTokens(ArrayList<ArrayList<Integer>> equateTokens) {
		this.equateTokens = equateTokens;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}

	public int getGroupCheck() {
		return groupCheck;
	}

	public void setGroupCheck(int groupCheck) {
		this.groupCheck = groupCheck;
	}

	public ArrayList<Integer> getSupressTokensIds() {
		return supressTokensIds;
	}

	public void setSupressTokensIds(ArrayList<Integer> supressTokensIds) {
		this.supressTokensIds = supressTokensIds;
	}

	public int getMinFClusP() {
		return minFClusP;
	}

	public void setMinFClusP(int minFClusP) {
		this.minFClusP = minFClusP;
	}

	public int getMinFClusT() {
		return minFClusT;
	}

	public void setMinFClusT(int minFClusT) {
		this.minFClusT = minFClusT;
	}

	public int getMinMClusP() {
		return minMClusP;
	}

	public void setMinMClusP(int minMClusP) {
		this.minMClusP = minMClusP;
	}

	public int getMinMClusT() {
		return minMClusT;
	}

	public void setMinMClusT(int minMClusT) {
		this.minMClusT = minMClusT;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectDiscrp() {
		return projectDiscrp;
	}

	public void setProjectDiscrp(String projectDiscrp) {
		this.projectDiscrp = projectDiscrp;
	}

	public long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}

	public long getModificationTime() {
		return modificationTime;
	}

	public void setModificationTime(long modificationTime) {
		this.modificationTime = modificationTime;
	}

	public static boolean[] getSupressTokens() {
		return supressTokens;
	}

	public static void setSupressTokens(boolean[] supressTokens) {
		CloneRunConfiguration.supressTokens = supressTokens;
	}

}
