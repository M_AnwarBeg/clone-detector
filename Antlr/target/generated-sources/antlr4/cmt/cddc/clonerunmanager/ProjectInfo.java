package cmt.cddc.clonerunmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.fileclones.FileCloneWriterDB;
import cmt.cddc.methodclones.MethodCloneWriterDB;
import cmt.cddc.simpleclones.SimpleCloneWriterDB;
import cmt.cddc.structures.sFile;
import cmt.common.Directories;

public class ProjectInfo{
    private static String INSERT_DIRECTORY;
    private static String INSERT_FILE_DIRECTORY;
    private static String INSERT_PROJECT_CONFIGURATION;
    private static String projectName;
    private static String projectDiscription;
    private static String supressedTokens = "";
    private static String equateTokens = "";
    private static String Language;
    private static String tym;
    private static int threshold;
    private static int minMethodClusp;
    private static int minMethodClust;
    private static int minFileClusp;
    private static int minFileClust;
    private static int groupchk;
    private static Hashtable<Integer,sFile> inputFileList =  new Hashtable<Integer, sFile>();
    
    public static void loadInputFiles() {
    	try {
    		Statement st = CloneRepository.getConnection().createStatement();
    		String sql = "select fd.fid, d.* , f.fname from file_directory AS fd"
    				+ " inner join DIRECTORY as d on fd.did = d.did"
    				+ " inner join file as f on f.fid = fd.fid order by fd.fid asc";
    		st.execute("use "+ CloneRepository.getDBName()+";");
    		ResultSet rs = st.executeQuery(sql);
    		sFile file;
    		int i=0;
    		while(rs.next()) {
    			String name = rs.getString("fname");
    			if(name.contains(";")) {
    				name= name.substring(0, name.length()-1);
    			}
    			file = new sFile(name); 
    			file.setDirID(rs.getInt("did"));
    			file.setFileID(rs.getInt("fid"));
    			file.setFullPath(rs.getString("dname"));
    			inputFileList.put(i, file);
    			i++;
    		}
    		
    		
        	
    	}catch(Exception ex) {
    		ex.printStackTrace();
    	}
    	
    }
    
    
    public static String getInputFile(int fid) {
    	sFile fileInfo = inputFileList.get(fid);
    	String path = fileInfo.getFullPath()+"/"+fileInfo.getFileName();
    	return path;
    	
    }
    
    public static void getSysConf() {
    	projectName = "\""+SysConfig.getProjectName()+"\"";
    	projectDiscription = "";
    	if(SysConfig.getProjectDiscrp().equals("")) {
    		projectDiscription = "\""+"NULL"+"\"";
    	}else {
    		projectDiscription = "\""+SysConfig.getProjectDiscrp()+"\"";
    	}
    	
    	
    	threshold = SysConfig.getThreshold();
    	minFileClusp = SysConfig.getMinFClusP();
    	minFileClust = SysConfig.getMinFClusT();
    	minMethodClusp = SysConfig.getMinMClusP();
    	minMethodClust = SysConfig.getMinMClusT();
    	groupchk = SysConfig.getGroupCheck();
    	tym = SysConfig.getSystemTime();
    	System.out.println(tym);
    	System.out.println(tym.length());
    	int lang = SysConfig.getLangChoice();
    	if (lang == 0)
		{
		   Language = "\"Java"+"\"";
		} else if (lang == 1)
		{
			Language = "\"C++"+"\"";
		} else if (lang == 3)
		{
			Language = "\"CSHARP"+"\"";
		} else if (lang == 4)
		{
			Language = "\"RUBY"+"\"";
		} else if (lang == 5)
		{
			Language = "\"PHP"+"\"";
		} else if (lang == 6)
		{
			Language = "\"TXT"+"\"";
		} else
		{
			Language = null;
		    
		}
    	
    	boolean a[] = SysConfig.getSupressTokens();
    	for(int i =0; i<a.length;i++) {
    		if(a[i]) {
    			String id = String.valueOf(i);
    			if(supressedTokens.equals("")) {
    				supressedTokens = "\""+id;
    				
    			}
    			else
				{
    				if(!supressedTokens.contains(id)) {
    					supressedTokens += ", "+ id;
    				}
					
				}
    		}
    	}
    	if(supressedTokens.length() ==0) {
    		supressedTokens = "\"NULL\"";
    		
    	}
    	
    	
    	
    	
    	
    	ArrayList<ArrayList<Integer>> equateToken = SysConfig.getEquateTokens();
    	equateTokens = "";
    	for(int i =0; i<equateToken.size();i++) {
    		ArrayList<Integer> ruleIDs = equateToken.get(i);
    		for(int j =0; j<ruleIDs.size(); j++) {
    			if(equateTokens.equals("")) {
    				equateTokens = "\""+String.valueOf(ruleIDs.get(j));
    				
    			}
    			else if(equateTokens.contains(";") && equateTokens.lastIndexOf(";") == (equateTokens.length()-1)){
    				
    				equateTokens += " "+String.valueOf(ruleIDs.get(j));
    				
    				
    			}
    			else {
    				equateTokens += ", "+String.valueOf(ruleIDs.get(j));
    			}
    		}
    		equateTokens += ";";
    	}
    	if(!equateTokens.equals("")) {
    		equateTokens+="\"";
    	}
    	else {
    		equateTokens = "\""+"NULL"+"\"";
    	}
    	
    }
    
    public static void initQuery()
    {	
    	INSERT_DIRECTORY = "INSERT INTO directory(did, dname) values ";
    	INSERT_FILE_DIRECTORY = "INSERT INTO file_directory(fid, did, length,endLine) values ";
    	INSERT_PROJECT_CONFIGURATION = "INSERT INTO sysconf(languageselected, projectname, projectdiscrp, supresstokens, equatetokens,"
    			+ " threshold, groupcheck, min_fileclusp, min_fileclust, min_methodclusp, min_methodclust, createdate, lastupdate) values ";
    	
    }
	
	public static void insertDirectory(int did, String dname)
    {
	dname = dname.replaceAll("\\\\", "/");
	INSERT_DIRECTORY += "( \"" + did + "\", \"" + dname + "\"),";
    }

    public static void insertFile_Directory(int fid, int did, int length,int endLine)
    {
	INSERT_FILE_DIRECTORY += "( \"" + fid + "\" , \"" + did + "\", \"" + length + "\", \"" + endLine + "\" ),";
    }
    public static void insertProjectConfig() {
    	INSERT_PROJECT_CONFIGURATION +="("+Language+","+projectName+", "+projectDiscription+", "+supressedTokens+", "+equateTokens+", "+threshold
    			+", "+groupchk+", "+minFileClusp+", "+minFileClust+", "+minMethodClusp+", " +minMethodClust+", "+tym+", "+tym+"),";
    	executeTransaction(INSERT_PROJECT_CONFIGURATION);
    }
    
    public static void executeTransaction(String sql)
    {
	try
		{
		    if (sql.indexOf("\"") != -1)
		    {
			sql = sql.substring(0, sql.length() - 1);
		    }
		    sql = sql + ";";
		    Statement st = CloneRepository.getConnection().createStatement();
		    st.execute("use "+CloneRepository.getDBName()+";");
		    st.execute(sql);
		    st.close();
		}
	catch (Exception e)
		{
		    System.err.println(e.getMessage());
		    e.printStackTrace();
		}
    }
	
    public static void populateDatabase(int dlm)
    {
    	   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
    	   
    	   //System.out.println("Inserting directories "+dtf.format(LocalDateTime.now())); 
    	populateDirectory();
    	//System.out.println("Inserting input files "+dtf.format(LocalDateTime.now())); 
		populateFile();
		//System.out.println("Inserting input files directories"+dtf.format(LocalDateTime.now())); 
		populateFileDirectory();
		//System.out.println("Inserting simple clones"+dtf.format(LocalDateTime.now())); 
		SimpleCloneWriterDB.populateDatabase(dlm , false);
		//System.out.println("Inserting method clones"+dtf.format(LocalDateTime.now()));
		MethodCloneWriterDB.populateDatabase(dlm);
		//System.out.println("Inserting file clones"+dtf.format(LocalDateTime.now()));
		FileCloneWriterDB.populateDatabase(dlm); 
		//populateTokens();
		//populateTokens();
		//insertProjectConfig();
		//System.out.println("Inserting clone-run configurations"+dtf.format(LocalDateTime.now()));
		CloneRunInfoWriter.insertCloneRunConfiguration();
    }
    
    public static void clearDatabase()
    {
    	/*Statement istmt;
		try 
		{
			istmt = CloneRepository.getConnection().createStatement();
			istmt.execute("use "+CloneRepository.getDBName());
			ResultSet rs=istmt.executeQuery("SELECT Concat('TRUNCATE TABLE ',table_schema,'.',TABLE_NAME, ';')FROM INFORMATION_SCHEMA.TABLES where  table_schema in ('CloneDatabase');");
			String truncateDB="";
			while(rs.next())
			{
				truncateDB+=rs.getString(1);
			}
			
			StringTokenizer st=new StringTokenizer(truncateDB,";");
			while(st.hasMoreTokens())
			{
				istmt.execute(st.nextToken());
			}
			rs.close();
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}*/
    }
    private static void populateTokens()
    {
    	try
    	{
    		File cfile = new File(Directories.getAbsolutePath(Directories.COMBINEDTOKENSFILE));
			FileInputStream cfilein = new FileInputStream(cfile);
			BufferedReader cstdin = new BufferedReader(new InputStreamReader(cfilein));
			
			String cline=cstdin.readLine();
			cline=cstdin.readLine();
			
			PreparedStatement cstmt = null;
			cstmt=CloneRepository.getConnection().prepareStatement("use "+CloneRepository.getDBName()+";");
			cstmt.execute();
			while(cline!=null)
			{
				String[] parts = cline.split("\\s+");
				String []text=cline.split(parts[8]);
				text[text.length-1]=text[text.length-1].replaceFirst("\\s+","");
				//text[text.length-1]=text[text.length-1].replace("\\","\\\\");
				
				cstmt=CloneRepository.getConnection().prepareStatement(("insert into tokens (number,fid,line,col,cls,mthd,rep,tokenId,text) values ("+Integer.parseInt(parts[1])
				+","+Integer.parseInt(parts[2])
				+","+Integer.parseInt(parts[3])
				+","+Integer.parseInt(parts[4])
				+","+Integer.parseInt(parts[5])
				+","+Integer.parseInt(parts[6])
				+","+Integer.parseInt(parts[7])
				+","+Integer.parseInt(parts[8])+",?)"));
				
						String textValue=text[text.length-1];
						cstmt.setString(1,textValue);
						
						cstmt.execute();
						
				cline=cstdin.readLine();
				
			}
			
			cfilein.close();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    private static void populateFileDirectory()
    {
    	StringTokenizer st;
	    String line, filePath = null;
    	try
    	{
    		filePath = Directories.getAbsolutePath(Directories.FILESINFOFILE);
    	    File file3 = new File(filePath);
    	    FileInputStream filein3 = new FileInputStream(file3);
    	    BufferedReader stdin3 = new BufferedReader(new InputStreamReader(filein3));
    	    int countForFid = 0;
    	    line = stdin3.readLine();
    	    while (line != null)
    	    {
    		st = new StringTokenizer(line, ",");
    		st.nextToken();
    		int tempId = new Integer(st.nextToken().trim()).intValue();
    		int length = new Integer(st.nextToken().trim()).intValue();
    		int endLine = new Integer(st.nextToken().trim()).intValue();
    		insertFile_Directory(countForFid, tempId, length,endLine);
    		countForFid++;
    		line = stdin3.readLine();
    	    }

    	    if (!INSERT_FILE_DIRECTORY.equalsIgnoreCase("INSERT INTO file_directory(fid, did, length, endLine) values "))
    	    {
    		executeTransaction(INSERT_FILE_DIRECTORY);
    	    }
    	    filein3.close();
    	}
    	catch(Exception e)
    	{
    		
    	}
    }
    private static void populateFile()
    {
    	try
    	{
	    	File ifiles = new File(Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES));
			FileInputStream ifilesin = new FileInputStream(ifiles);
			BufferedReader istdin = new BufferedReader(new InputStreamReader(ifilesin));
			
			String iline=istdin.readLine();
			
			int fileID=0;
			int groupID=0;
			
			Statement istmt = CloneRepository.getConnection().createStatement();
			istmt.execute("use "+CloneRepository.getDBName());
			while(iline!=null)
			{
				
				if(iline.contains(";"))
				{
					groupID++;
					iline=istdin.readLine();
					continue;
				}
				File f=new File(iline);
				istmt.execute("insert into file(fid,fname,gid) values("+fileID+",'"+f.getName()+"',"+groupID+")");
				fileID++;
						
				iline=istdin.readLine();
				
			}
			
			ifilesin.close();
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    private static void populateDirectory()
    {
    	try
    	{
    		StringTokenizer st;
    	    String line, filePath = null;

    	    filePath = Directories.getAbsolutePath(Directories.DIRSINFOFILE);
    	    File file12 = new File(filePath);
    	    FileInputStream filein12 = new FileInputStream(file12);
    	    BufferedReader stdin12 = new BufferedReader(new InputStreamReader(filein12));
    	    String dName;
    	    int dId;
    	    while ((line = stdin12.readLine()) != null)
	    	{
	    		if (!line.equalsIgnoreCase(""))
	    		{
	    		    st = new StringTokenizer(line, ",");
	    		    dId = Integer.parseInt(st.nextToken().trim());
	    		    dName = st.nextToken();
	    		    insertDirectory(dId, dName);
	    		}
	    	}

    	    if (!INSERT_DIRECTORY.equalsIgnoreCase("INSERT INTO directory(did, dname) values "))
    	    {
    		executeTransaction(INSERT_DIRECTORY);
    	    }

    	}
    	catch(Exception e)
    	{
    		
    	}
    }
	public static Vector<String> getFCSInDir_Files(int did, int fcc_id)
	{
	Vector<String> list = null;
	String fid = null;
	
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select distinct fcc_instance.fid " + "from fcc_instance "
		    + "where fcc_instance.did = " + did + " and fcc_instance.fcc_id = " + fcc_id + ";");
	    list = new Vector<>();
	    while (results.next())
	    {
		fid = results.getString("fid");
		list.add(fid);
	    }
	
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	    System.err.println(e.getMessage());
	}
	
	return list;
	}

	public static Vector<String> getFCSInGroup_Files(int gid, int fcc_id)
	{
	Vector<String> list = null;
	String fid = null;
	
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select distinct fcc_instance.fid " + " from fcc_instance "
		    + " where fcc_instance.gid = " + gid + " and fcc_instance.fcc_id = " + fcc_id + ";");
	    list = new Vector<>();
	    while (results.next())
	    {
		fid = results.getString("fid");
		list.add(fid);
	    }
	
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	    System.err.println(e.getMessage());
	}
	
	return list;
	}

	/*
	 * Returns the method objectName of a method given the method id
	 */
	public static String getMethodName(int mid)
	{
	String methodName = null;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select mname from method where mid = " + mid + ";");
	    if (results.next())
	    {
		methodName = results.getString("mname");
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return methodName;
	}

	/*
	 * Returns the file objectName of a file given the file id
	 */
	public static String getFileName(int fid)
	{
	String fileName = null;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select fname from file where fid = " + fid + ";");
	    if (results.next())
	    {
		fileName = results.getString("fname");
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return fileName;
	}

	public static int getDidFromFid(int fid)
	{
	int did = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select did " + " from file_directory " + " where fid = " + fid + ";");
	    if (results.next())
	    {
		did = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	
	return did;
	}

	public static int getFidFromMid(int mid)
	{
	int fid = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select fid " + " from method_file " + " where mid = " + mid + ";");
	    if (results.next())
	    {
		fid = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	
	return fid;
	}

	public static int getGidFromFid(int fid)
	{
	int gid = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select file.gid " + " from file " + " where file.fid = " + fid + ";");
	    if (results.next())
	    {
		gid = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	
	return gid;
	}

	/*
	 * Returns the objectName of a directory given the directory id
	 */
	public static String getDirName(int did)
	{
	String dirName = null;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select dname from directory where did = " + did + ";");
	    if (results.next())
	    {
		dirName = results.getString("dname");
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return dirName;
	}

	public static int getMethodTokenSize(int mid)
	{
	int size = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select tokens " + " from method " + " where mid = " + mid + ";");
	    if (results.next())
	    {
		size = results.getInt(1);
	    }
	
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	
	return size;
	}

	public static int getGroupTokenSize(int gid)
	{
	int size = 0;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select distinct a.fid, a.length " + "from file_directory a, file b "
		    + "where b.gid = " + gid + " and a.fid = b.fid " + ";");
	    while (results.next())
	    {
		size += results.getInt(2);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return size;
	}

	public static int getDirTokenSize(int did)
	{
	int size = 0;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select length " + "from file_directory " + "where did = " + did + ";");
	    while (results.next())
	    {
		size += results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return size;
	}

	/*
	 * Returns the size of the input file list
	 */
	public static int getDirectorySize()
	{
	int size = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select count(*) from directory;");
	    if (results.next())
	    {
		size = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return size;
	}

	public static int getFileTokenSize(int fid)
	{
	int size = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select length " + "from file_directory " + "where fid = " + fid + ";");
	    if (results.next())
	    {
		size = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return size;
	}

	public static int getGroupSize()
	{
	int size = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select count(distinct gid) from file;");
	    if (results.next())
	    {
		size = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	}
	return size;
	}

	public static int getFileSize()
	{
	int size = -1;
	try
	{
	    Connection dbConn = CloneRepository.getConnection();
	    Statement s = dbConn.createStatement();
	    s.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = s.executeQuery("select count(*) from file;");
	    if (results.next())
	    {
		size = results.getInt(1);
	    }
	    s.close();
	} catch (Exception e)
	{
	    e.printStackTrace();
	}
	return size;
	}

	public static int getMethodId(int fid, int sl, int el)
	{
	try
	{
	    String s;
	    int msl, mel, mid;
	    Vector<String> content = new Vector<>();
	    String fileName = getFileName(fid);
	    File f = new File(fileName);
	    FileInputStream fstream = new FileInputStream(f);
	    @SuppressWarnings("resource")
	    BufferedReader in = new BufferedReader(new InputStreamReader(fstream));
	    while (in.ready())
	    {
		content.add(in.readLine());
	    }
	    while (sl <= el)
	    {
		s = content.get(sl - 1).trim();
		if (s.equalsIgnoreCase(""))
		{
		    sl++;
		} else if ((s.length() >= 1 && s.substring(0, 1).equalsIgnoreCase("}")) || (s.length() >= 2
			&& (s.substring(0, 2).equalsIgnoreCase("//") || s.substring(0, 2).equalsIgnoreCase("/*"))))
		{
		    sl++;
		} else
		{
		    break;
		}
	    }
	    while (sl <= el)
	    {
		s = content.get(el - 1).trim();
		if (s.equalsIgnoreCase(""))
		{
		    el--;
		} else if ((s.length() >= 1 && s.substring(0, 1).equalsIgnoreCase("{")) || (s.length() >= 2
			&& (s.substring(0, 2).equalsIgnoreCase("//") || s.substring(0, 2).equalsIgnoreCase("*/"))))
		{
		    el--;
		} else
		{
		    break;
		}
	    }
	    Connection dbConn = CloneRepository.getConnection();
	    Statement st = dbConn.createStatement();
	    st.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet results = st.executeQuery(
		    "select method.startline, " + "method.endline, method.mid " + "from method, method_file "
			    + "where method.mid = method_file.mid " + "and method_file.fid = " + fid + ";");
	    while (results.next())
	    {
		msl = results.getInt(1);
		mel = results.getInt(2);
		mid = results.getInt(3);
		if (msl <= sl && mel >= el)
		{
		    return mid;
		}
	    }
	
	    st.close();
	} catch (FileNotFoundException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	} catch (SQLException e)
	{
	    e.printStackTrace();
	}
	return -1;
	}
	
	
	 public static String getFilePath(int fid) throws IOException
	 {
		BufferedReader reader = null;
		FileReader frdr = null;
		try
		{		
			 String path = Directories.INPUTFILE;
			 path = Directories.getAbsolutePath(path);
			 frdr = new FileReader(path);
			 reader = new BufferedReader(frdr);
			 
			 int index = 0;
		    String line = null;
		    while ((line = reader.readLine()) != null)
		    {
			if (index == fid)
			{
			    return line;
			}
			index++;
		    }
		}
		
		catch (FileNotFoundException e)
		{
		    System.out.println("file not found");
		}

		catch (Exception e)
		{
		    System.out.println("Error finding file Name");
		}
		finally
		{
			reader.close();
		} 
		return "N/A";
	    }

	
	

}
