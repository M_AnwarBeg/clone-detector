package cmt.cddc.clonerunmanager;



import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;
import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.common.Directories;

public class CloneRunInfoWriter {

	private static String INSERT_PROJECT_CONFIGURATION;
    private static String INSERT_EQUATE_TOKENS;
    private static String INSERT_SURPESS_TOKENS;
    private static String INSERT_UPDATED_FILES;
    private static String INSERT_UPDATED_FILES_DIRECTORY;
    private static String INSERT_UPDATED_DIRECTORY;
    private static String DELETE_FILES;
    private static String DELETE_DIRECTORIES;
    private static String UPDATE_TIME; 
    
    public static void updateFramingFlag(String oldProject, String newProject) {
    	String updateOldTable = "update sysconf set enableframing = false;";
    	String updateNewTable = "update sysconf set enableframing = true;";
    	try {
    		Statement st = CloneRepository.getConnection().createStatement();
		    st.execute("use "+ oldProject+";");
		    st.executeUpdate(updateOldTable);
		    st.execute("use "+ newProject+";");
		    st.executeUpdate(updateNewTable);
    	}
    	catch(Exception e) {
    		e.printStackTrace(); 
    	}
    }
    	public static void updateFramingFlag(String oldProject) {
        	String updateOldTable = "update sysconf set enableframing = false;";
        	try {
        		Statement st = CloneRepository.getConnection().createStatement();
    		    st.execute("use "+ oldProject+";");
    		    st.executeUpdate(updateOldTable);
        	}
        	catch(Exception e) {
        		e.printStackTrace();
        	}
    	 
    	 
    }
    public static void addFramingFlag( String project) {
    	
    	String updateNewTable = "update sysconf set enableframing = true;";
    	try {
    		Statement st = CloneRepository.getConnection().createStatement();
		    
		    st.execute("use "+ project+";");
		    st.executeUpdate(updateNewTable);
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    	}
    	 
    	
    }
    
    public static void updateTime() {
    	long tym = System.currentTimeMillis();
    	UPDATE_TIME = "update sysconf set lastupdate = "+"'"+tym+"'";//+ "where projectname = "+"'"+SysConfig.getProjectName()+"';";
    	try {
    		Statement stmnt = CloneRepository.getConnection().createStatement();
    		stmnt.executeQuery("use "+ CloneRepository.getDBName());
    		stmnt.executeUpdate(UPDATE_TIME);
    	}
    	catch(Exception ex){
    		ex.printStackTrace();
    	}
    	
    }
    
     public static void deleteFileData(ArrayList<Integer> fileIds) {
    	 
    	for(Integer fileId : fileIds) {
    		ArrayList <Integer> DirID = new ArrayList<Integer>();
    		DELETE_FILES = "delete from file where fid = "+fileId+";";
			
			String countDir = "SELECT COUNT(fid), did FROM file_directory GROUP BY did;";
			String DirCheck = "Select did from directory;";
			
			
			try {
				Statement stmt01 = CloneRepository.getConnection().createStatement();
				stmt01.executeQuery("use "+CloneRepository.getDBName());
				stmt01.executeUpdate(DELETE_FILES);
				
				ResultSet rs02 = stmt01.executeQuery(countDir);
				while(rs02.next()) {
					DirID.add(rs02.getInt(2));
					
				}
				Statement stmt02 = CloneRepository.getConnection().createStatement();
				stmt02.executeQuery("use "+CloneRepository.getDBName());
				ResultSet rs03 = stmt02.executeQuery(DirCheck);
				while(rs03.next()) {
					int did = rs03.getInt(1);
					if(!DirID.contains(did)) {
						Statement stmt03 = CloneRepository.getConnection().createStatement();
						stmt03.executeQuery("use "+CloneRepository.getDBName());
						DELETE_DIRECTORIES = "delete from directory where did = "+did+";";
						stmt03.executeUpdate(DELETE_DIRECTORIES);
					}
				}
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			
    	}
    	
    }
     
     
    public static void  deleteGroup(int groupId) {
    	String deleteQuery =   "delete from groupinfo where gid = "+groupId+";";
		try {    	
			Statement stmt = CloneRepository.getConnection().createStatement();
			stmt.executeQuery("use "+CloneRepository.getDBName());
			stmt.executeUpdate(deleteQuery);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    } 
     
    public static void updateFileData(Hashtable <Integer,ArrayList<String>> newFiles) {
    	int fid=0, did=0, gid =0;
    	String directory = "";
    	String query01 = "Select fid FROM file ORDER BY fid DESC LIMIT 1";
    	String query02 = "Select did, dname FROM directory ORDER BY did DESC LIMIT 1";
    	String query10 = "Select dname FROM directory ";
    	
    	ArrayList <String> allDirectories = new ArrayList <String>();
    		for(Map.Entry entry: newFiles.entrySet()) {
    			gid = (Integer) entry.getKey();
    			ArrayList <String> filesList = (ArrayList<String>)entry.getValue();
    			for(String fileName : filesList) {
    			//	directory = fileName.substring(0, fileName.lastIndexOf("/"));
    				directory = fileName.substring(0, fileName.lastIndexOf("\\"));
    			//	fileName = fileName.substring((fileName.lastIndexOf("/")+1), fileName.length());
    				fileName = fileName.substring((fileName.lastIndexOf("\\")+1), fileName.length());
    				try {
    					Statement stmt = CloneRepository.getConnection().createStatement();
    					stmt.executeQuery("use "+CloneRepository.getDBName());
    					ResultSet rs = stmt.executeQuery(query01);
    					while(rs.next()) {
    						fid = rs.getInt(1);
    						fid++;
    					}
    					Statement stmt10 = CloneRepository.getConnection().createStatement();
    					stmt10.executeQuery("use "+CloneRepository.getDBName());
    					ResultSet rs10 = stmt10.executeQuery(query10);
    					while(rs10.next()) {
    						allDirectories.add(rs10.getString(1));
    					}
    					
    					if(!allDirectories.contains(directory)) {
    			    		allDirectories.add(directory);
    			    		Statement stmt01 = CloneRepository.getConnection().createStatement();
        					stmt01.executeQuery("use "+CloneRepository.getDBName());
        					ResultSet rs01 = stmt.executeQuery(query02);
        					while(rs01.next()) {
        						did = rs01.getInt(1);
        						
        						did++;
        					}
        					Statement stmt03 = CloneRepository.getConnection().createStatement();
        					

        					directory = directory.replaceAll("\\\\", "/");        					
        					INSERT_UPDATED_DIRECTORY = "insert into directory (did, dname) values ("+did+",'"+directory+"')";
        					stmt03.execute(INSERT_UPDATED_DIRECTORY);        					
    			    	}
    					else {
    						Statement stmt01 = CloneRepository.getConnection().createStatement();
        					stmt01.executeQuery("use "+CloneRepository.getDBName());
        					ResultSet rs01 = stmt.executeQuery("select did from directory where dname = '"+directory+"'");
        					while(rs01.next()) {
        						did = rs01.getInt(1);
        						
        						//did++;
        					}
    					}
    					
    					Statement stmt02 = CloneRepository.getConnection().createStatement();
    					stmt02.executeQuery("use "+CloneRepository.getDBName());
    					INSERT_UPDATED_FILES = "insert into file (fid, fname, gid) values ("+fid+",'"+fileName+"',"+gid+");";
    					INSERT_UPDATED_FILES_DIRECTORY = "insert into file_directory (fid, did, length, endLine) values ((select fid from file where fid = "+fid+"),(select did from directory where did = "+did+"),0,0);";
    			    	
    			    	stmt02.execute(INSERT_UPDATED_FILES);
    			    	/*if(!allDirectories.contains(directory)) {
    			    		allDirectories.add(directory);
    			    		stmt02.execute(query05);
    			    	}*/
    			    	stmt02.execute(INSERT_UPDATED_FILES_DIRECTORY);
    			    	
    			    	
    					
    				}
    				catch(Exception ex) {
    					ex.printStackTrace();
    				}
    				
    				
    				
    				
    			}
    			
    		}
    		
    		
    		
    	
    	
    	
    	
    }

 
    
    public static void initQuery()
    {	
    		INSERT_PROJECT_CONFIGURATION = "INSERT INTO sysconf(languageselected, projectname, projectdiscrp,"
    			+ " threshold, groupcheck, min_fileclusp, min_fileclust, min_methodclusp, min_methodclust, createdate, lastupdate, enableframing) values ";
        INSERT_EQUATE_TOKENS = "INSERT INTO sysconfig_equate_tokens(rule_number,token_id) values";
    	INSERT_SURPESS_TOKENS = "INSERT INTO sysconfig_supresstoken(token_id) values";
    }
    
	
	private static void insertSurpressedTokens(ArrayList<Integer> supressedTokens) {
	    for(Integer tokenId: supressedTokens) {
	    	String insertStatement = INSERT_SURPESS_TOKENS + "(" + tokenId +  ")";  
	    	CloneRepository.executeTransaction(insertStatement);
	    	}
	    }
	    
	    private static void insertEquateTokens(ArrayList<ArrayList<Integer>> equateTokens)
	    {    	
	      int ruleNumer = 0;
	      for(ArrayList<Integer> rule: equateTokens) {
	    	  for(Integer tokenId: rule) {
	    		  String insertStatement =   INSERT_EQUATE_TOKENS +"(" + ruleNumer + "," + tokenId + ")";
	    		  CloneRepository.executeTransaction(insertStatement);
	    	  }
	    	  ruleNumer = ruleNumer+1;    	 
	      }    	
	    }
	    public static void insertCloneRunConfiguration() {
	    	
	    	initQuery(); 	
	    	String projectName = "\""+SysConfig.getProjectName()+"\"";
	    	String projectDiscription = "";
	    	if(SysConfig.getProjectDiscrp().equals("")) {
	    		projectDiscription = "\""+"NULL"+"\"";
	    	}else {
	    		projectDiscription = "\""+SysConfig.getProjectDiscrp()+"\"";
	    	}
	    	int threshold = SysConfig.getThreshold();
	    	int minFileClusp = SysConfig.getMinFClusP();
	    	int minFileClust = SysConfig.getMinFClusT();
	    	int minMethodClusp = SysConfig.getMinMClusP();
	    	int minMethodClust = SysConfig.getMinMClusT();
	    	int groupchk = SysConfig.getGroupCheck();	    	 	
	    	String creationTime = String.valueOf(System.currentTimeMillis());
	       	String Language = "";
	    	int lang = SysConfig.getLangChoice();
	    	if (lang == 0)
			{
			   Language = "\"Java"+"\"";
			} else if (lang == 1)
			{
				Language = "\"C++"+"\"";
			} else if (lang == 3)
			{
				Language = "\"CSHARP"+"\"";
			} else if (lang == 4)
			{
				Language = "\"RUBY"+"\"";
			} else if (lang == 5)
			{
				Language = "\"PHP"+"\"";
			} else if (lang == 6)
			{
				Language = "\"TXT"+"\"";
			} else
			{
				Language = null;
			    
			}    	
	    	boolean supressedTokensList[] = SysConfig.getSupressTokens();
	    	ArrayList<Integer>supressedTokenIds = new ArrayList<Integer>();
	    	for(int i =0; i<supressedTokensList.length;i++) {    		
	    		if(supressedTokensList[i])
	    			supressedTokenIds.add(i);    		
	    	}    	
	    	insertSurpressedTokens(supressedTokenIds);
	    	ArrayList<ArrayList<Integer>> equateTokensIds = SysConfig.getEquateTokens();   	
	    	insertEquateTokens(equateTokensIds);
	    	
	    	
	    	String inssertConfigSql = INSERT_PROJECT_CONFIGURATION;
	    	inssertConfigSql +="("+Language+","+projectName+", "+projectDiscription+", "+threshold
	    			+", "+groupchk+", "+minFileClusp+", "+minFileClust+", "+minMethodClusp+", " +minMethodClust+", "+creationTime+", "+creationTime+", false),";
	    	CloneRepository.executeTransaction(inssertConfigSql);
	    
	    
	    
	    }
	
	
	public static void writeCloneRunInputFiles(Vector<String[]> fileGroups) {
		try
		{
		    String pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_FILES);
		  //  System.out.println("Here: "+pathStr);
		    File file = new File(pathStr);
		    file.createNewFile();
		    FileOutputStream fileout = new FileOutputStream(file);
		    PrintWriter stdout = new PrintWriter(fileout);

		    for (int i = 0; i < fileGroups.size(); i++)
		    {
			for (int j = 0; j < fileGroups.get(i).length; j++)
			{
				
				fileGroups.get(i)[j]=fileGroups.get(i)[j].replaceAll("/", "\\\\");
			    if (j == fileGroups.get(i).length - 1 && i != fileGroups.size() - 1)
			    {
				stdout.println(fileGroups.get(i)[j] + ";");
			    } else
			    {
				stdout.println(fileGroups.get(i)[j]);
			    }
			}
		    }
		    stdout.flush();
		    stdout.close();
		    fileout.close();

		   /* pathStr = Directories.getAbsolutePath(Directories.CLONES_INPUT_CLUSTER_PTRS);
		    file = new File(pathStr);
		    file.createNewFile();
		    fileout = new FileOutputStream(file);
		    stdout = new PrintWriter(fileout);
		    stdout.print(fpcInt + "," + ftcInt + "," + mpcInt + "," + mtcInt);
		    stdout.flush();
		    stdout.close();
		    fileout.close();*/
		} catch (Exception e)
		{
		    System.err.println(e.getMessage());
		    e.printStackTrace();
		}
		
	}
	
}
