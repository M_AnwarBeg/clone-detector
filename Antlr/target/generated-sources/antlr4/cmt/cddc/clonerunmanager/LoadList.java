package cmt.cddc.clonerunmanager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import org.eclipse.mylyn.commons.notifications.core.AbstractNotification;
import org.eclipse.mylyn.commons.notifications.ui.NotificationsUi;
import org.eclipse.mylyn.internal.commons.notifications.ui.popup.NotificationPopup;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wb.swt.SWTResourceManager;
import org.jgrapht.experimental.alg.color.GreedyColoring;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.CloneDetector;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonedetectioninitializer.VisualizationSettingsActionDelegate;
import cmt.cddc.simpleclones.Utilities;
import cmt.clonedetectionSettingsUI.CloneTrackingFormPage;
import cmt.common.Directories;
import cmt.common.NotificationPopUpUI;
import cmt.common.ProjectImages;
import cmt.ctc.mongodb.ClearMongo;
import cmt.cvac.views.FilterSimpleClones;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.TreeEditor;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;

public class LoadList {
	public static Composite container;
	private Table table;
	private Text text;
	public static Shell shell;
	private static org.eclipse.swt.graphics.Color gray;
	long[] a;
	private static Tree tree;
	Display display;
	public static Boolean framingEnabledFlag = false;
	public static String creationTime = "";
	public static String framingBaseline = "";
	public static String projectName = "";
	static ArrayList<String> SkipTokens = new ArrayList<String>();
	Hashtable<Integer, String> tokenData = new Hashtable<Integer, String>();
	ArrayList<CloneRunConfiguration> cloneRunConfiguration;
	ArrayList<Long> createTym = new ArrayList<Long>();
	ArrayList<Boolean> framingFlags = new ArrayList<Boolean>();
	int index;

	/**
	 * Create the dialog.
	 * 
	 */
	// private static Table table;
	public LoadList(/* Shell parentShell */) {
		super();
		try {
			/*
			 * FileReader frdr = null; BufferedReader buff = null; frdr = new
			 * FileReader(Directories.getAbsolutePath(Directories.FramingBaseLine)); buff =
			 * new BufferedReader(frdr); framingBaseline=buff.readLine();
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public ArrayList<String> getEquateTokens(ArrayList<ArrayList<Integer>> equateTokens) {
		ArrayList<String> equatedTokens = new ArrayList<String>();

		if (equateTokens.isEmpty())
			return null;
		for (ArrayList<Integer> rule : equateTokens) {
			StringBuilder equatedTokenRule = new StringBuilder();
			for (Integer tokenId : rule) {
				equatedTokenRule.append(tokenData.get(tokenId)).append("=");
			}
			equatedTokens.add(equatedTokenRule.deleteCharAt(equatedTokenRule.length() - 1).toString());
		}
		return equatedTokens;
	}

	public String getSupressedTokens(ArrayList<Integer> supressedTokenIds) {
		StringBuilder supressedTokensList = new StringBuilder();

		for (Integer tokenId : supressedTokenIds) {
			supressedTokensList.append(tokenData.get(tokenId)).append(",");
		}
		return supressedTokensList.deleteCharAt(supressedTokensList.length() - 1).toString();
	}

	public void readLanguageTokens(String a) {
		try {
			String filePath = Directories.getAbsolutePath(a);
			File file = new File(filePath);
			FileInputStream filein = new FileInputStream(file);
			BufferedReader stdin = new BufferedReader(new InputStreamReader(filein));
			String line = "";
			while ((line = stdin.readLine()) != null) {
				int last = line.lastIndexOf("=");
				int endIndex = last;
				int beginIndex = last + 1;
				int length = line.length();
				String token = line.substring(0, endIndex);
				String tokenID = line.substring(beginIndex, length);
				String IDS = "";
				if (tokenData.containsKey(Integer.parseInt(tokenID))) {
					if (token.contains("'")) {
						if (token.lastIndexOf("'") == (token.length() - 1)) {
							IDS = token.substring(1, (token.length() - 1));
							if (!SkipTokens.contains(IDS)) {
								tokenData.put(Integer.parseInt(tokenID), token);
							} else {
								tokenData.remove(Integer.parseInt(tokenID));
							}
						}
					}
				} else {
					if (!SkipTokens.contains(token)) {
						tokenData.put(Integer.parseInt(tokenID), token);
					}
				}
			}
		} catch (Exception ex) {
		}
	}

		private void SetSkipToken(String token) {
		SkipTokens.add(token);
	}

	// tokensNotToShow.

	public void SkipToken() {
		String[] token = { "Identifier", "(", ")", "throws", ";", "{", "}", "class", "interface", "=", "]" };
		for (int i = 0; i < token.length; i++) {
			SetSkipToken(token[i]);
		}
	}

	public String convertTime(long time) {
		Date date = new Date(time);
		Format format = new SimpleDateFormat("EEEE, dd, MMM, yyyy, hh:mm a");
		return format.format(date);
	}

	/**
	 * Create contents of the dialog.
	 * 
	 * @param parent
	 */
	protected void createContents() {

		shell = new Shell(SWT.CLOSE | SWT.TITLE);
		cloneRunConfiguration = CloneRunInfoReader.getCloneRuns();
		shell.setText("Select a Clonerun from the list below.");
		shell.setSize(575, 620);
		table = new Table(shell, SWT.BORDER | SWT.FULL_SELECTION | SWT.SINGLE);
		table.setBounds(10, 25, 160, 546);
		TableColumn column = new TableColumn(table, SWT.None);
		column.setWidth(155);
		cloneRunConfiguration = CloneRunInfoReader.getCloneRuns();
		int i = 0;
		for (CloneRunConfiguration config : cloneRunConfiguration) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, config.getProjectName());
			createTym.add(config.getCreationTime());
			framingFlags.add(config.getEnableFraming());
			if (config.getEnableFraming()) {
				Color clr = display.getSystemColor(SWT.COLOR_GRAY);
				framingEnabledFlag = true;
				createDirectories();
				item.setBackground(clr);
				framingBaseline = CloneRunInfoReader.databaseNames.get(i);
			}
			i++;

		}

		Menu popupMenu = new Menu(table);
		MenuItem Item = new MenuItem(popupMenu, SWT.None);
		Item.setText("Enable Tracking and Framing");
		table.setMenu(popupMenu);
		popupMenu.addMenuListener(new MenuAdapter() {
			public void menuShown(MenuEvent e) {
				index = table.getSelectionIndex();
				if (framingFlags.get(index)) {
					Item.setText("Disable Tracking and Framing");
				} else {
					Item.setText("Enable Tracking and Framing");
					Item.setEnabled(true);
				}
			}
		});
		Item.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event arg0) {
				// TODO Auto-generated method stub
				if (framingEnabledFlag) {
					if (framingFlags.get(index)) {
						MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES | SWT.NO);
						messageBox.setText("Warning");
						messageBox.setMessage("Clones will no longer be tracked and ART Templates will be Deleted.\nAre you sure you want to disable clone tracking and framming?");
						int buttonID = messageBox.open();
						switch (buttonID) {
						case SWT.YES: {	
							Color clr = display.getSystemColor(SWT.COLOR_WHITE);
							TableItem tableItem = table.getItem(index);
							tableItem.setBackground(0, clr);
							framingFlags.set(index,false);
							framingEnabledFlag = false; 
							CloneRunInfoWriter.updateFramingFlag(CloneRunInfoReader.databaseNames.get(index));  
							disableTrackingandFraming();
					    }
						case SWT.NO:{
						}
					}
						
					}
					else {
						MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES | SWT.NO);
						messageBox.setText("Warning");
						messageBox.setMessage("Clones will no longer be tracked and ART Templates will be Deleted.\nAre you sure you want to switch baseline clonerun?");
						int buttonID = messageBox.open();
						switch (buttonID) {
						case SWT.YES: {
							int oldIndex = 0, newIndex = 0;
							for (int i = 0; i < framingFlags.size(); i++) {
								Boolean flag = framingFlags.get(i);
								if (flag) {
									oldIndex = i;
									TableItem itemOld = table.getItem(i);
									Color clr = display.getSystemColor(SWT.COLOR_WHITE);
									itemOld.setBackground(0, clr);
									framingFlags.set(i, false);

									break;
								}
							}
							newIndex = table.getSelectionIndex();
							TableItem itemOld = table.getItem(newIndex);
							Color clr = display.getSystemColor(SWT.COLOR_GRAY);
							itemOld.setBackground(0, clr);
							newIndex = table.getSelectionIndex();
							framingBaseline = CloneRunInfoReader.databaseNames.get(newIndex);
							CloneRunInfoWriter.updateFramingFlag(CloneRunInfoReader.databaseNames.get(oldIndex),
									CloneRunInfoReader.databaseNames.get(newIndex));
							framingFlags.set(newIndex, true);

						}
							
						// saves changes ...
						case SWT.NO:
							// exits here ...
							break;
						/*
						 * case SWT.CANCEL: // does nothing ...
						 */ }
					}

					

				} 
				else {
					enableTrackingandFraming();
						
				}
			}
			});
		Group grpCloneInformation = new Group(shell, SWT.NONE);
		grpCloneInformation.setText("Clone Information");
		grpCloneInformation.setBounds(176, 25, 373, 520);

		Label lblSimpleClones = new Label(grpCloneInformation, SWT.NONE);
		lblSimpleClones.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblSimpleClones.setBounds(10, 20, 163, 15);
		lblSimpleClones.setText("Simple Clones :");

		Label lblThreshold = new Label(grpCloneInformation, SWT.NONE);
		lblThreshold.setBounds(10, 41, 163, 15);
		lblThreshold.setText("Threshold :");

		Label lblNewLabel = new Label(grpCloneInformation, SWT.NONE);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblNewLabel.setBounds(10, 62, 163, 15);
		lblNewLabel.setText("Method Clones :");

		Label lblMinimumSimilarityCoverage = new Label(grpCloneInformation, SWT.NONE);
		lblMinimumSimilarityCoverage.setBounds(10, 83, 163, 15);
		lblMinimumSimilarityCoverage.setText("Minimum Similarity Coverage :");

		Label lblMaximumSimilarityCoverage = new Label(grpCloneInformation, SWT.NONE);
		lblMaximumSimilarityCoverage.setBounds(10, 104, 163, 15);
		lblMaximumSimilarityCoverage.setText("Minimum Token Coverage :");

		Label lblFileClones = new Label(grpCloneInformation, SWT.NONE);
		lblFileClones.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblFileClones.setBounds(10, 129, 163, 15);
		lblFileClones.setText("File Clones :");

		Label label = new Label(grpCloneInformation, SWT.NONE);
		label.setText("Minimum Similarity Coverage :");
		label.setBounds(10, 150, 163, 15);

		Label label_1 = new Label(grpCloneInformation, SWT.NONE);
		label_1.setText("Minimum Token Coverage :");
		label_1.setBounds(10, 171, 163, 15);

		Label lblSuppressedTokkens = new Label(grpCloneInformation, SWT.NONE);
		lblSuppressedTokkens.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblSuppressedTokkens.setBounds(10, 192, 163, 83);
		lblSuppressedTokkens.setText("Suppressed Tokkens :");

		Label lblEquateTokens = new Label(grpCloneInformation, SWT.NONE);
		lblEquateTokens.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblEquateTokens.setBounds(10, 281, 163, 83);
		lblEquateTokens.setText("Equate Tokens :");

		Label lblCreateDate = new Label(grpCloneInformation, SWT.NONE);
		lblCreateDate.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblCreateDate.setBounds(10, 370, 163, 15);
		lblCreateDate.setText("Create Date :");

		Label lblLastUpdate = new Label(grpCloneInformation, SWT.NONE);
		lblLastUpdate.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblLastUpdate.setBounds(10, 391, 163, 15);
		lblLastUpdate.setText("Last Update :");

		Label lblDiscription = new Label(grpCloneInformation, SWT.NONE);
		lblDiscription.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblDiscription.setBounds(10, 412, 163, 15);
		lblDiscription.setText("Discription :");

		text = new Text(grpCloneInformation, SWT.BORDER);
		text.setBounds(10, 433, 353, 72);
		text.setEditable(false);

		Label lblthreshold01 = new Label(grpCloneInformation, SWT.NONE);
		lblthreshold01.setBounds(180, 41, 184, 15);
		lblthreshold01.setText("");

		Label methodSimilarityCoverage = new Label(grpCloneInformation, SWT.NONE);
		methodSimilarityCoverage.setText("");
		methodSimilarityCoverage.setBounds(180, 83, 184, 15);

		Label methodtokenCoverage = new Label(grpCloneInformation, SWT.NONE);
		methodtokenCoverage.setText("");
		methodtokenCoverage.setBounds(180, 104, 184, 15);

		Label fileSimilarityCoverage = new Label(grpCloneInformation, SWT.NONE);
		fileSimilarityCoverage.setText("");
		fileSimilarityCoverage.setBounds(180, 150, 184, 15);

		Label fileTokenCoverage = new Label(grpCloneInformation, SWT.NONE);
		fileTokenCoverage.setText("");
		fileTokenCoverage.setBounds(180, 171, 184, 15);

		List list = new List(grpCloneInformation, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		list.setBounds(179, 281, 184, 83);

		List list_1 = new List(grpCloneInformation, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		list_1.setBounds(179, 192, 184, 83);
		// list_1.set

		Label CreateDate = new Label(grpCloneInformation, SWT.NONE);
		CreateDate.setText("");
		CreateDate.setBounds(180, 370, 184, 15);

		Label LastUpDate = new Label(grpCloneInformation, SWT.NONE);
		LastUpDate.setText("");
		LastUpDate.setBounds(180, 391, 184, 15);

		Button btnNewButton = new Button(shell, SWT.CANCEL);
		btnNewButton.setBounds(474, 546, 75, 25);
		btnNewButton.setText("Cancel");
		btnNewButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				LoadListSetting.isOpen = false;
				shell.close();

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		Button btnNewButton_1 = new Button(shell, SWT.NONE);
		btnNewButton_1.setBounds(393, 546, 75, 25);
		btnNewButton_1.setText("Delete");
		btnNewButton_1.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				MessageBox messageBox = new MessageBox(shell, SWT.ICON_WARNING | SWT.YES | SWT.NO);
				messageBox.setText("Warning");
				messageBox.setMessage(
						"You are about to Delete the Clone Run and all of its data.\nAre you sure to Continue?");
				int buttonID = messageBox.open();
				switch (buttonID) {
				case SWT.YES: {
					String string;
					TableItem[] selection = table.getSelection();

					string = selection[0].getText();

					for (CloneRunConfiguration config : cloneRunConfiguration) {
						if (config.getProjectName().equals(string)
								&& config.getCreationTime() == createTym.get(table.getSelectionIndex())) {
							{
								CloneRepository.dropDatabase(config.getCloneRepositoryName());

								// if selected project is enabled for framing then delete frame repo as well
								if (config.getEnableFraming()) {
									disableTrackingandFraming();
								}
								break;
							}
						}

					}
					table.removeAll();
					createTym.clear();
					framingFlags.clear();
					cloneRunConfiguration = CloneRunInfoReader.getCloneRuns();
					int i = 0;
					for (CloneRunConfiguration config : cloneRunConfiguration) {

						TableItem item = new TableItem(table, SWT.NONE);
						item.setText(0, config.getProjectName());
						createTym.add(config.getCreationTime());
						framingFlags.add(config.getEnableFraming());
						if (config.getEnableFraming()) {
							Color clr = display.getSystemColor(SWT.COLOR_GRAY);
							framingEnabledFlag = true;
							createDirectories();
							item.setBackground(0, clr);
							framingBaseline = CloneRunInfoReader.databaseNames.get(i);
						}
						i++;

					}

				}
				// saves changes ...
				case SWT.NO:
					// exits here ...
					break;
				/*
				 * case SWT.CANCEL: // does nothing ...
				 */ }

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		Button btnOpen = new Button(shell, SWT.NONE);
		btnOpen.setBounds(312, 546, 75, 25);
		btnOpen.setText("Open");

		btnOpen.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

				cloneRunSelection(true);
				/*
				 * String name; TableItem[] selection = table.getSelection();
				 * 
				 * name = selection[0].getText();
				 * 
				 * for(CloneRunConfiguration config : cloneRunConfiguration) {
				 * if(config.getProjectName().equals(name)&& config.getCreationTime()==
				 * createTym.get(table.getSelectionIndex())) { projectName = name; creationTime
				 * = String.valueOf(createTym.get(table.getSelectionIndex())); creationTime =
				 * creationTime.substring(0, (creationTime.length()-1)); ClonesReader reader =
				 * null; VisualizationSettingsActionDelegate.reader = null;
				 * setActiveCloneRun(config); LoadListSetting.isOpen = false; shell.close();
				 * 
				 * 
				 * }
				 * 
				 * }
				 */
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});
		/*
		 * Button btnUpdate = new Button(shell, SWT.NONE); btnUpdate.setText("Update");
		 * btnUpdate.setBounds(312, 546, 75, 25);
		 * 
		 * btnUpdate.addSelectionListener(new SelectionListener() {
		 * 
		 * @Override public void widgetSelected(SelectionEvent arg0) { // TODO
		 * Auto-generated method stub //Shell shell01 = new Shell();
		 * 
		 * String string; TableItem[] selection = table.getSelection();
		 * 
		 * string = selection[0].getText();
		 * 
		 * for(CloneRunConfiguration config : cloneRunConfiguration) {
		 * if(config.getProjectName().equals(string) && config.getCreationTime()==
		 * createTym.get(table.getSelectionIndex())) { setActiveCloneRun(config);
		 * //shell.close(); break; }
		 * 
		 * }
		 * 
		 * Shell shell = new Shell(); CloneRunUpdateTemp temp = new
		 * CloneRunUpdateTemp(shell,0); temp.open();
		 * 
		 * // CloneRunUpdate update = new CloneRunUpdate(); AbstractNotification
		 * notification = new AbstractNotification ("my.event" ) {
		 * 
		 * public String getLabel() { return "My Label"; } public String
		 * getDescription(){ return "My Description"; }
		 * 
		 * @Override public <T> T getAdapter(Class<T> arg0) { // TODO Auto-generated
		 * method stub return null; }
		 * 
		 * @Override public Date getDate() { // TODO Auto-generated method stub return
		 * null; } };
		 * Notifications.getService().notify(Collections.singletonList(notification)) ;
		 * //run();
		 * 
		 * // update.open();
		 * 
		 * 
		 * }
		 * 
		 * @Override public void widgetDefaultSelected(SelectionEvent arg0) { // TODO
		 * Auto-generated method stub
		 * 
		 * } });
		 */

		table.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				btnOpen.setEnabled(true);
				//btnUpdate.setEnabled(true);
				lblthreshold01.setText("");
				CreateDate.setText("");
				LastUpDate.setText("");
				methodSimilarityCoverage.setText("");
				methodtokenCoverage.setText("");
				fileSimilarityCoverage.setText("");
				fileTokenCoverage.setText("");
				list.removeAll();
				list_1.removeAll();
				text.setText("");
				String string;
				TableItem[] selection = table.getSelection();

				string = selection[0].getText();

				for (CloneRunConfiguration config : cloneRunConfiguration) {
					SkipToken();
					readLanguageTokens(Directories.TOKEN_JAVA);
					if (config.getProjectName().equals(string)
							&& config.getCreationTime() == createTym.get(table.getSelectionIndex())) {
						lblthreshold01.setText(String.valueOf(config.getThreshold()));
						CreateDate.setText(convertTime(config.getCreationTime()));
						LastUpDate.setText(convertTime(config.getModificationTime()));
						methodSimilarityCoverage.setText(String.valueOf(config.getMinMClusP()));
						methodtokenCoverage.setText(String.valueOf(config.getMinMClusT()));
						fileSimilarityCoverage.setText(String.valueOf(config.getMinFClusP()));
						fileTokenCoverage.setText(String.valueOf(config.getMinFClusT()));
						String projectDiscrp = String.valueOf(config.getProjectDiscrp());
						if (!projectDiscrp.equals("NULL")) {
							text.setText(projectDiscrp);
						}

						if (!config.getSupressTokensIds().isEmpty()) {
							String supressedToken = getSupressedTokens(config.getSupressTokensIds());

							String[] array = supressedToken.split(",");
							for (int i = 0; i < array.length; i++) {
								list_1.add(array[i]);

							}
						}

						if (!config.getEquateTokens().isEmpty()) {
							ArrayList<String> equateToken = getEquateTokens(config.getEquateTokens());
							for (int i = 0; i < equateToken.size(); i++) {
								list.add(equateToken.get(i));

							}
						}

					}

				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		shell.addListener(SWT.Close, new Listener() {
			public void handleEvent(Event event) {
				LoadListSetting.isOpen = false;
				event.doit = true;
			}

		});
		if (table.getSelection().length == 0) {
			//btnUpdate.setEnabled(false);
			btnOpen.setEnabled(false);
		} else {
			//btnUpdate.setSelection(true);
			btnOpen.setEnabled(false);
		}

	}

	public static Shell getList() {
		return shell;
	}

	private void cloneRunSelection(boolean closeShell) {
		String name;
		TableItem[] selection = table.getSelection();
		name = selection[0].getText();		for (CloneRunConfiguration config : cloneRunConfiguration) {
			if (config.getProjectName().equals(name)
					&& config.getCreationTime() == createTym.get(table.getSelectionIndex())) {
				projectName = name;
				creationTime = String.valueOf(createTym.get(table.getSelectionIndex()));
				creationTime = creationTime.substring(0, (creationTime.length() - 1));
				ClonesReader reader = null;
				VisualizationSettingsActionDelegate.reader = null;
				setActiveCloneRun(config);
				LoadListSetting.isOpen = false;
				if (closeShell)
					shell.close();
			}
		}
	}

	public static void createDirectories() {
		new File(Directories.getAbsolutePath(Directories.SccFrames)).mkdir();
		new File(Directories.getAbsolutePath(Directories.framingOutput)).mkdir();
		new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccTemplate)).mkdir();
		new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsTemplate)).mkdir();
		new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_fccTemplate)).mkdir();
		new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccSPC)).mkdir();
		new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsSPC)).mkdir();
		new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_fccSPC)).mkdir();
	}

	public void open() {
		display = Display.getDefault();
		createContents();
		try {
			Image small = new Image(shell.getDisplay(), Directories.getAbsolutePath(Directories.LOAD_LIST));
			shell.setImage(small);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		Monitor primary = display.getPrimaryMonitor();
		Rectangle bounds = primary.getBounds();
		Rectangle rect = shell.getBounds();
		int x = bounds.x + (bounds.width - rect.width) / 2;
		int y = bounds.y + (bounds.height - rect.height) / 2;

		shell.setLocation(x, y);
		shell.open();
		// isOpen = true;
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/*
	 * public static void run() { try { NotificationPopUpUI popup = new
	 * NotificationPopUpUI(NotificationQuartzJobHelper.getInstance().getCurrentDisp(
	 * )PlatformUI.getWorkbench().getDisplay()); popup.open();
	 * 
	 * //popup.setDelayClose(0L); popup.setFadingEnabled(true); long tym =
	 * System.currentTimeMillis(); popup.setDelayClose(tym+5000);
	 * 
	 * }catch(Exception ex) { ex.printStackTrace(); }
	 * 
	 * }
	 */

	public void setActiveCloneRun(CloneRunConfiguration config) {
		if (config.getEnableFraming()) {
			SysConfig.setEquateTokens(config.getEquateTokens());
			SysConfig.setSupressTokens(config.getSupressTokens());
			SysConfig.setThreshold(config.getThreshold());
			SysConfig.setMinFClusP(config.getMinFClusP());
			SysConfig.setMinFClusT(config.getMinFClusT());
			SysConfig.setMinMClusP(config.getMinMClusP());
			SysConfig.setMinMClusT(config.getMinMClusP());
			framingBaseline = config.getCloneRepositoryName();
			CloneRepository.setDBName(config.getCloneRepositoryName());
		} else {
			SysConfig.setEquateTokens(config.getEquateTokens());
			SysConfig.setSupressTokens(config.getSupressTokens());
			SysConfig.setThreshold(config.getThreshold());
			SysConfig.setMinFClusP(config.getMinFClusP());
			SysConfig.setMinFClusT(config.getMinFClusT());
			SysConfig.setMinMClusP(config.getMinMClusP());
			SysConfig.setMinMClusT(config.getMinMClusP());

			CloneRepository.setDBName(config.getCloneRepositoryName());
		}

	}
	
	
	
	
	
	private void enableTrackingandFraming() {
		cloneRunSelection(false);
		String dbName = CloneRepository.getDBName();
		String[] parts = dbName.split("_");
		String execTimeInMilliSec = parts[2];
		// Creating date format
		DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z");
		Date execDate = new Date(Long.parseLong(execTimeInMilliSec));
		// Formatting Date according to the given format
		System.out.println(simple.format(execDate));
		try {
			if (!Utilities.checkIfAnyFileIsUpdatedAfterExecDate(execDate)) {
				MessageBox messageBox = new MessageBox(shell, SWT.ERROR_FAILED_EXEC | SWT.OK);
				messageBox.setText("");
				messageBox.setMessage("Can not set as baseline! \n Files updated after clonerun. ");
				int buttonID = messageBox.open();
			} 
		else {
		int index = table.getSelectionIndex();
		framingEnabledFlag = true;
		CloneTrackingFormPage.isIncremental = true;
		createDirectories();
		TableItem testItem = table.getItem(index);
		Color clr = display.getSystemColor(SWT.COLOR_GRAY);
		testItem.setBackground(0, clr);
		framingBaseline = CloneRunInfoReader.databaseNames.get(index);
		CloneRunInfoWriter
				.addFramingFlag(CloneRunInfoReader.databaseNames.get(table.getSelectionIndex()));
		CloneRepository.createFrameRepo();
		ArrayList<ArrayList<String>> cloneRunFiles = CloneRunInfoReader.getCloneRunNames();
		Vector<String[]> fileGroups = new Vector<String[]>();
		for (ArrayList<String> groupFilesList : cloneRunFiles) {
			String[] groupFilesArray = new String[groupFilesList.size()];
			groupFilesArray = groupFilesList.toArray(groupFilesArray);
			fileGroups.add(groupFilesArray);
		}
		CloneRunInfoWriter.writeCloneRunInputFiles(fileGroups);
		CloneDetector.detectClones();
		//shell.close();
		
		} 
		}catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	private void disableTrackingandFraming() {
		
		try {
			CloneRepository.openConnection();
			Statement stmt = CloneRepository.conn.createStatement();
			stmt.execute("use framerepository;");
			stmt.execute("truncate table fccframes;");
			stmt.execute("truncate table framedescription;");
			stmt.execute("truncate table framehierarchy;");
			stmt.execute("truncate table frames;");
			stmt.execute("truncate table mccframes;");
			stmt.execute("truncate table mcsframes;");
			stmt.execute("truncate table sccframes;");

			File f = new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccSPC));
			File[] listOfFiles = f.listFiles();

			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					listOfFiles[i].delete();
				}
			}

			f = new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccTemplate));
			File[] listOfFiles2 = f.listFiles();

			for (int i = 0; i < listOfFiles2.length; i++) {
				if (listOfFiles2[i].isFile()) {
					listOfFiles2[i].delete();
				}
			}

			f = new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_fccSPC));
			File[] listOfFiles3 = f.listFiles();

			for (int i = 0; i < listOfFiles3.length; i++) {
				if (listOfFiles3[i].isFile()) {
					listOfFiles3[i].delete();
				}
			}

			f = new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_fccTemplate));
			File[] listOfFiles4 = f.listFiles();

			for (int i = 0; i < listOfFiles4.length; i++) {
				if (listOfFiles4[i].isFile()) {
					listOfFiles4[i].delete();
				}
			}

			f = new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsSPC));
			File[] listOfFiles5 = f.listFiles();

			for (int i = 0; i < listOfFiles5.length; i++) {
				if (listOfFiles5[i].isFile()) {
					listOfFiles5[i].delete();
				}
			}

			f = new File(Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsTemplate));
			File[] listOfFiles6 = f.listFiles();

			for (int i = 0; i < listOfFiles6.length; i++) {
				if (listOfFiles6[i].isFile()) {
					listOfFiles6[i].delete();
				}
			}

			f = new File(Directories.getAbsolutePath(Directories.SccFrames));
			File[] listOfFiles7 = f.listFiles();

			for (int i = 0; i < listOfFiles7.length; i++) {
				if (listOfFiles7[i].isFile()) {
					listOfFiles7[i].delete();
				}
			}

		} catch (Exception e) {

		}

		ClearMongo.clearMongoDB();
	}
	
	
	
	
	
	
	

}
