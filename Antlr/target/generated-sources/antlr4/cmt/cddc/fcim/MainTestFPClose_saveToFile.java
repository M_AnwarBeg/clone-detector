package cmt.cddc.fcim;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Example of how to use FPClose from the source code and
 * the result to a file.
 * @author Philippe Fournier-Viger (Copyright 2015)
 */
public class MainTestFPClose_saveToFile {

	public static void getStructClones_SPMFAlgo(String inputFile, String outputFile , double minsup) throws FileNotFoundException, IOException{
		// the file paths
		//String input = "output\\clonesByFileNormal.txt";  // the database
		//String output = ".\\output\\SPMFOutput.txt";//".//SPMFOutput.txt";  // the path for saving the frequent itemsets found
		
		//double minsup = 0.4; // means a minsup of 2 transaction (we used a relative support)

		// Applying the algorithm
		AlgoFPClose algo = new AlgoFPClose();
		algo.runAlgorithm(inputFile, outputFile, minsup);
		algo.printStats();
	}
}
