package cmt.cddc.methodclones;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.simpleclones.CloneInstance;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sMethod;
import cmt.cfc.mccframe.GraphSolver;
import cmt.cfc.mccframe.MCCsAnalyser;
import cmt.common.Directories;



public class MethodCloneInstance extends CloneInstance{
	private double coverage;
    private int methodCloneClassID;
    private sMethod method;
    private ArrayList<String> analysisDetails;
    private Collection<Set<Integer>> sortedCliques;
    private Collection<Set<Integer>> allCliques;
    private ArrayList<SimpleCloneInstance> SCCs_Contained;
    private ArrayList<SimpleCloneInstance> newSccs_Contained;
    private int[][] Adjeceny_Matrix_For_Overlaps;
    private int fid;
	
    public MethodCloneInstance(int mCCID, double coverage2, sMethod method_, int fid)
    {
	// TODO Auto-generated constructor stub
	Adjeceny_Matrix_For_Overlaps = null;
	coverage = coverage2;
	methodCloneClassID = mCCID;
	method = method_;
	sortedCliques = new ArrayList<>();
	allCliques = new ArrayList<>();
	analysisDetails = new ArrayList<>();
	SCCs_Contained = new ArrayList<>();
	newSccs_Contained = new ArrayList<>();
	this.fid = fid;
    } public MethodCloneInstance(int mCCID, double coverage2, sMethod method_)
    {
	// TODO Auto-generated constructor stub
	Adjeceny_Matrix_For_Overlaps = null;
	coverage = coverage2;
	methodCloneClassID = mCCID;
	method = method_;
	sortedCliques = new ArrayList<>();
	allCliques = new ArrayList<>();
	analysisDetails = new ArrayList<>();
	SCCs_Contained = new ArrayList<>();
	newSccs_Contained = new ArrayList<>();
	
    }
    
    public void addSCCs_Contained(SimpleCloneInstance sccInstanceCustom)
    {
	// TODO Auto-generated method stub
	this.SCCs_Contained.add(sccInstanceCustom);
    }
    
    public int getFID() {
    	return fid;
    }

    
    public double getCoverage()
    {
	return coverage;
    }

    public void setCoverage(double coverage)
    {
	this.coverage = coverage;
    }

    public int getMethodCloneID()
    {
	return methodCloneClassID;
    }

    public void setMethodCloneID(int methodCloneID)
    {
	methodCloneClassID = methodCloneID;
    }

    public sMethod getMethod()
    {
	return method;
    }

    public void setMethod(sMethod clone)
    {
	method = clone;
    }

    public ArrayList<String> getAnalysisDetails()
    {
	return analysisDetails;
    }

    public void setAnalysisDetails(ArrayList<String> analysis)
    {
	analysisDetails = analysis;
    }
    public ArrayList<SimpleCloneInstance> getSCCs()
    {
	return SCCs_Contained;
    }

    public void setSCCs(ArrayList<SimpleCloneInstance> sCCs)
    {
	SCCs_Contained = sCCs;
    }

    
    public int[][] getAdjeceny_Matrix_For_Overlaps()
    {
	return Adjeceny_Matrix_For_Overlaps;
    }

    public void setAdjeceny_Matrix_For_Overlaps(int[][] adjeceny_Matrix_For_Overlaps)
    {
	Adjeceny_Matrix_For_Overlaps = adjeceny_Matrix_For_Overlaps;
    }

    public ArrayList<SimpleCloneInstance> getNewSccs_Contained()
    {
	return newSccs_Contained;
    }

    public void setNewSccs_Contained(ArrayList<SimpleCloneInstance> newSccs_Contained)
    {
	this.newSccs_Contained = newSccs_Contained;
    }

    public Collection<Set<Integer>> getSortedCliques()
    {
	return sortedCliques;
    }

    public void setSortedCliques(Collection<Set<Integer>> sortedCliques)
    {
	this.sortedCliques = sortedCliques;
    }

    public Collection<Set<Integer>> getAllCliques()
    {
	return allCliques;
    }

    public void setAllCliques(Collection<Set<Integer>> allCliques)
    {
	this.allCliques = allCliques;
    }
    
    
    public void DisplayAnalysisDetails() throws IOException
    {
	// TODO Auto-generated method stub

	if (this.getSCCs().size() == 1)
	{
	    String path = "\\My Output\\";
	    path = Directories.getAbsolutePath(path);
	    FileWriter name = new FileWriter(path + "test1.txt", true);
	    BufferedWriter out = new BufferedWriter(name);
	    out.write(this.getMethod().getCodeSegment());
	    out.write("************THIS IS THE END OF THE METHOD*****************");
	    out.write("\n");
	    for (SimpleCloneInstance instance : this.getSCCs())
	    {
		out.write(instance.getCodeSegment());
		out.write("***********THIS IS THE END OF THIS SCC CLONE INSTANCE CODE**************");
	    }
	    out.write("\n");
	    for (SimpleCloneInstance instance : getNewSccs_Contained())
	    {
		out.write(instance.getCodeSegment());
		out.write("***********THIS IS THE END OF THIS NEW SCC CLONE INSTANCE CODE**************");
	    }

	    for (int i = 0; i < getAnalysisDetails().size(); i++)
	    {
		out.write("\n");
		out.write("AT INDEX : " + i + " : " + getAnalysisDetails().get(i));
	    }
	    out.write("****************** THATS IT FOR THIS SPECIFIC METHOD CLONE INSTANCE*********************");
	    out.write("\n");
	    out.close();
	}
	if (this.getSCCs().size() == 2)
	{
	    String path = "\\My Output\\";
	    path = Directories.getAbsolutePath(path);
	    FileWriter name = new FileWriter(path + "test2.txt", true);
	    BufferedWriter out = new BufferedWriter(name);
	    out.write(this.getMethod().getCodeSegment());
	    out.write("************THIS IS THE END OF THE METHOD*****************");
	    out.write("\n");
	    for (SimpleCloneInstance instance : this.getSCCs())
	    {
		out.write(instance.getCodeSegment());
		out.write("***********THIS IS THE END OF THIS SCC CLONE INSTANCE CODE**************");
	    }
	    out.write("\n");
	    for (SimpleCloneInstance instance : getNewSccs_Contained())
	    {
		out.write(instance.getCodeSegment());
		out.write("***********THIS IS THE END OF THIS NEW SCC CLONE INSTANCE CODE**************");
	    }

	    for (int i = 0; i < getAnalysisDetails().size(); i++)
	    {
		out.write("\n");
		out.write("AT INDEX : " + i + " : " + getAnalysisDetails().get(i));
	    }
	    out.write("****************** THATS IT FOR THIS SPECIFIC METHOD CLONE INSTANCE*********************");
	    out.write("\n");
	    out.close();
	}

	if (this.getSCCs().size() > 2)
	{
	    String path = "\\My Output\\";
	    path = Directories.getAbsolutePath(path);
	    FileWriter name = new FileWriter(path + "test3.txt", true);
	    BufferedWriter out = new BufferedWriter(name);
	    out.write(this.getMethod().getCodeSegment());
	    out.write("************THIS IS THE END OF THE METHOD*****************");
	    out.write("\n");
	    for (SimpleCloneInstance instance : this.getSCCs())
	    {
		out.write(instance.getCodeSegment());
		out.write("***********THIS IS THE END OF THIS SCC CLONE INSTANCE CODE**************");
	    }
	    out.write("\n");
	    for (SimpleCloneInstance instance : getNewSccs_Contained())
	    {
		out.write(instance.getCodeSegment());
		out.write("***********THIS IS THE END OF THIS NEW SCC CLONE INSTANCE CODE**************");
	    }

	    for (int i = 0; i < getAnalysisDetails().size(); i++)
	    {
		out.write("\n");
		out.write("AT INDEX : " + i + " : " + getAnalysisDetails().get(i));
	    }
	    out.write("****************** THATS IT FOR THIS SPECIFIC METHOD CLONE INSTANCE*********************");
	    out.write("\n");
	    out.close();
	}

    }
    
    public void analyzeMethodCloneInstance(MethodClones parentClone) throws IOException
    {
	String path = ProjectInfo.getFilePath(getMethod().getFileID());
    //path = "D:\\runtime-EclipseApplication\\clones\\src\\clones\\CurrentAccount.java";
	GraphSolver.SolveGraph(this);
	getAnalysisDetails().clear();
	for (SimpleCloneInstance k : getNewSccs_Contained())
	{
	    getAnalysisDetails().add(" ");
	    getAnalysisDetails().add(Integer.toString(k.getSCCID()) + "," + k.getCodeSegment());
	}
	getAnalysisDetails().add(" ");
	try
	{
	    if (getMethod().getStartToken() == getNewSccs_Contained().get(0).getStartingIndex())
	    {
		String completelinecode = MethodClonesReader.getCodeSegment(path, getMethod().getStartToken(), 1,
			getMethod().getStartToken()+1, 1, null);
		
		String stringtobeadded="";
		
		if(completelinecode.length() >= getNewSccs_Contained().get(0).getStartCol())
		{
			stringtobeadded = completelinecode.substring(0, getNewSccs_Contained().get(0).getStartCol());  //changed -1 to +1
		}
		else
		{
			stringtobeadded = completelinecode;
		}
		getAnalysisDetails().set(0, stringtobeadded);
	    } else
	    {
		getAnalysisDetails().set(0,
				MethodClonesReader.getCodeSegment(path, getMethod().getStartToken(), 1,
				getNewSccs_Contained().get(0).getStartingIndex(),
				getNewSccs_Contained().get(0).getStartCol() - 1, null));
	    }
	    // need to analyze this
	    getAnalysisDetails().set(getAnalysisDetails().size() - 1,
	    		MethodClonesReader.getCodeSegment(path, MCCsAnalyser.getSCCInstanceWithLastLine(this).getEndingIndex(),
			    MCCsAnalyser.getSCCInstanceWithLastLine(this).getEndCol() + 1, getMethod().getEndToken() + 1,
			    1, null));
	    int currIndex = 2;
	    for (int i = 2; i < getAnalysisDetails().size() - 2; i = i + 2)
	    {
		SimpleCloneInstance PrevInstance = MCCsAnalyser.getInstance(getAnalysisDetails().get(currIndex - 1), this);
		SimpleCloneInstance NextInstance = MCCsAnalyser.getInstance(getAnalysisDetails().get(currIndex + 1), this);
		boolean check = MCCsAnalyser.checkInBetweenSCCInstances(this, currIndex, PrevInstance, NextInstance,
			path);
		if (check == true)
		{
		    currIndex = currIndex + 2;
		}
		if (currIndex > getAnalysisDetails().size() - 3)
		{
		    break;
		}
	    }
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
    }

    

}
