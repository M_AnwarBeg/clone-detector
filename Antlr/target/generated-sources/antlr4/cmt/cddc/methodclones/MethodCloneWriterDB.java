package cmt.cddc.methodclones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.Statement;
import java.util.StringTokenizer;
import java.util.Vector;

import cmt.cddc.clonedetectioninitializer.InputFileReader;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.common.Directories;
import cmt.cddc.CloneRepository.CloneRepository;

public class MethodCloneWriterDB {
	
	private static String INSERT_MCC;
	private static String INSERT_MCC_SCC;
    private static String INSERT_MCC_INSTANCE;

    private static String INSERT_MCC_FILE;

    private static String INSERT_MCS_CROSSFILE;
    private static String INSERT_MCSCROSSFILE_FILE;
    private static String INSERT_MCSCROSSFILE_METHODS;
    private static String INSERT_MCSCROSSFILE_MCC;
    private static String INSERT_METHOD;
    private static String INSERT_METHOD_FILE;
    
	public static void InitQuery()
	{
		InputFileReader.populateGroupInfoTable();
		INSERT_MCC_FILE = "INSERT INTO mcc_file(mcc_id, fid) values ";
		INSERT_MCC_INSTANCE = "INSERT INTO mcc_instance(mcc_instance_id, mcc_id, mid, tc, pc, fid, did, gid) values ";
		INSERT_MCC_SCC = "INSERT INTO mcc_scc(mcc_id, scc_id) values ";
		INSERT_MCS_CROSSFILE = "INSERT INTO mcs_crossfile(mcs_crossfile_id, members) values ";
		INSERT_MCSCROSSFILE_FILE = "INSERT INTO mcscrossfile_file(mcs_crossfile_id, fid, did, gid) values ";
		INSERT_MCSCROSSFILE_METHODS = "INSERT INTO mcscrossfile_methods(mcs_crossfile_id, fid, mcc_id, mid) values ";
		INSERT_MCSCROSSFILE_MCC = "INSERT INTO mcscrossfile_mcc(mcs_crossfile_id, mcc_id) values ";
		INSERT_METHOD = "INSERT INTO method(mid, mname, tokens, startline, endline) values ";
		INSERT_METHOD_FILE = "INSERT INTO method_file(mid, fid, startline, endline) values ";
		INSERT_MCC = "INSERT INTO mcc(mcc_id, atc, apc, members) values ";
		
	}
	public static void insertMethod(int mid, String mname, int tokens, int startline, int endline)
    {
	INSERT_METHOD += "( \"" + mid + "\" , \"" + mname + "\", \"" + tokens + "\", \"" + startline + "\", \""
		+ endline + "\"  ),";
    }
	
	public static void insertMethod_File(int mid, int fid, int startline, int endline)
    {
	INSERT_METHOD_FILE += "( \"" + mid + "\" , \"" + fid + "\", \"" + startline + "\", \"" + endline + "\"  ),";
    }

	public static void insertMCC_Instance(int mcc_instance_id, int mcc_id, int mid, double tc, double pc)
    {
	int fid = ProjectInfo.getFidFromMid(mid);
	INSERT_MCC_INSTANCE += "( \"" + mcc_instance_id + "\" , \"" + mcc_id + "\", \"" + mid + "\", \"" + tc + "\", \""
		+ pc + "\", \"" + fid + "\", \"" + ProjectInfo.getDidFromFid(fid) + "\", \"" + ProjectInfo.getGidFromFid(fid) + "\"  ),";
    }
	
	public static void insertMCC(int mcc_id, double atc, double apc, int members)
    {
	INSERT_MCC += "( \"" + mcc_id + "\" , \"" + atc + "\", \"" + apc + "\", \"" + members + "\"  ),";
    }

    public static void insertMCC_SCC(int mcc_id, int scc_id)
    {
	INSERT_MCC_SCC += "( \"" + mcc_id + "\" , \"" + scc_id + "\"  ),";
    }
    
    public static void insertMCC_File(int mcc_id, int fid)
    {
	INSERT_MCC_FILE += "( \"" + mcc_id + "\" , \"" + fid + "\"  ),";
    }
    
    public static void insertMCS_CrossFile(int mcs_crossfile_id, int members)
    {
	INSERT_MCS_CROSSFILE += "( \"" + mcs_crossfile_id + "\" , \"" + members + "\"  ),";
    }
	
    public static void insertMCSCrossFile_MCC(int mcs_crossfile_id, int mcc_id)
    {
	INSERT_MCSCROSSFILE_MCC += "( \"" + mcs_crossfile_id + "\" , \"" + mcc_id + "\"  ),";
    }
    
    public static void insertMCSCrossFile_File(int mcs_crossfile_id, int fid)
    {
	INSERT_MCSCROSSFILE_FILE += "( \"" + mcs_crossfile_id + "\" , \"" + fid + "\", \"" + ProjectInfo.getDidFromFid(fid)
		+ "\", \"" + ProjectInfo.getGidFromFid(fid) + "\"  ),";
    }
    
    public static void insertMCSCrossFile_Methods(int mcs_crossfile_id, int fid, int mcc_id, int mid)
    {
	INSERT_MCSCROSSFILE_METHODS += "( \"" + mcs_crossfile_id + "\" , \"" + fid + "\", \"" + mcc_id + "\" , \"" + mid
		+ "\" ),";
    }
    
	public static void executeTransaction(String sql)
    {
		try
			{
			    if (sql.indexOf("\"") != -1)
			    {
				sql = sql.substring(0, sql.length() - 1);
			    }
			    sql = sql + ";";
			    Statement st = CloneRepository.getConnection().createStatement();
			    st.execute("use "+CloneRepository.getDBName()+";");
			    st.execute(sql);
			    st.close();
			} 
		catch (Exception e)
			{
			    System.err.println(e.getMessage());
			    e.printStackTrace();
			}
    }
	
	public static void populateDatabase(int dlm)
	{
		StringTokenizer st, st1, st2;
	    String line, fid, filePath = null;
	    String mid, mName, sLine, eLine, token;
		try
			{
				filePath = Directories.getAbsolutePath(Directories.METHODSINFOFILE);
				File file6 = new File(filePath);
				FileInputStream filein6 = new FileInputStream(file6);
				BufferedReader stdin6 = new BufferedReader(new InputStreamReader(filein6));
				while ((line = stdin6.readLine()) != null)
				{
				    if (!line.equalsIgnoreCase(""))
				    {
					st = new StringTokenizer(line, ";");
					mid = st.nextToken().trim();
					fid = st.nextToken().trim();
					mName = st.nextToken().trim();
					sLine = st.nextToken().trim();
					eLine = st.nextToken().trim();
					token = st.nextToken().trim();
					insertMethod(Integer.parseInt(mid), mName, Integer.parseInt(token), Integer.parseInt(sLine),
						Integer.parseInt(eLine));
					insertMethod_File(Integer.parseInt(mid), Integer.parseInt(fid), Integer.parseInt(sLine),
						Integer.parseInt(eLine));
				    }
				}

				if (!INSERT_METHOD
					.equalsIgnoreCase("INSERT INTO method(mid, mname, tokens, startline, endline) values "))
				{
				    executeTransaction(INSERT_METHOD);
				}
				if (!INSERT_METHOD_FILE
					.equalsIgnoreCase("INSERT INTO method_file(mid, fid, startline, endline) values "))
				{
				    executeTransaction(INSERT_METHOD_FILE);
				}

				filein6.close();
				
				String temp;
				Vector<String> sccs;
				
				filePath = Directories.getAbsolutePath(Directories.METHODCLUSTERSXX);
				File file9 = new File(filePath);
				FileInputStream filein9 = new FileInputStream(file9);
				BufferedReader stdin9 = new BufferedReader(new InputStreamReader(filein9));
				double atc = 0;
				double apc = 0;
				while ((line = stdin9.readLine()) != null)
				{
				    if (!line.equalsIgnoreCase(""))
				    {
					st = new StringTokenizer(line, ";");
					String mCid = st.nextToken().trim();
					String mSupport = st.nextToken().trim();
					temp = stdin9.readLine();
					sccs = new Vector<>();
					st2 = new StringTokenizer(temp, ",");
					while (st2.hasMoreTokens())
					{
					    sccs.add(st2.nextToken());
					}
					int mcc_id = Integer.parseInt(mCid);
					int mSup = (new Integer(mSupport)).intValue();
					atc = 0;
					apc = 0;
					for (int i = 0; i < mSup; i++)
					{
					    line = stdin9.readLine();
					    st1 = new StringTokenizer(line, ";,");
					    mid = st1.nextToken().trim();
					    String mTk = st1.nextToken().trim();
					    String mCoverage = st1.nextToken().trim();
					    mName = ProjectInfo.getMethodName(new Integer(mid).intValue());
					    atc += Double.parseDouble(mTk);
					    apc += Double.parseDouble(mCoverage);
					    insertMCC_Instance(i, mcc_id, Integer.parseInt(mid), Double.parseDouble(mTk),
						    Double.parseDouble(mCoverage));
					}
					atc = atc / mSup;
					apc = apc / mSup;
					for (int i = 0; i < sccs.size(); i++)
					{
					    insertMCC_SCC(mcc_id, Integer.parseInt(sccs.get(i)));
					}
					insertMCC(mcc_id, atc, apc, mSup);
				    }
				}

				if (!INSERT_MCC.equalsIgnoreCase("INSERT INTO mcc(mcc_id, atc, apc, members) values "))
				{
				    executeTransaction(INSERT_MCC);
				}
				if (!INSERT_MCC_INSTANCE.equalsIgnoreCase(
					"INSERT INTO mcc_instance(mcc_instance_id, mcc_id, mid, tc, pc, fid, did, gid) values "))
				{
				    executeTransaction(INSERT_MCC_INSTANCE);
				}
				if (!INSERT_MCC_SCC.equalsIgnoreCase("INSERT INTO mcc_scc(mcc_id, scc_id) values "))
				{
				    executeTransaction(INSERT_MCC_SCC);
				}
				
				filePath = Directories.getAbsolutePath(Directories.METHOD_CLONES_BY_FILE);
				File file10 = new File(filePath);
				FileInputStream filein10 = new FileInputStream(file10);
				BufferedReader stdin10 = new BufferedReader(new InputStreamReader(filein10));

				int filePosn = 0;
				while ((line = stdin10.readLine()) != null)
				{
				    if (!line.equalsIgnoreCase(""))
				    {
					st = new StringTokenizer(line, ",");
					while (st.hasMoreTokens())
						{
						    String s2 = st.nextToken().trim();
						    if (!s2.equalsIgnoreCase(""))
						    {
							insertMCC_File(Integer.parseInt(s2), filePosn);
						    }
						}
				    }
				    filePosn++;
				}

				if (!INSERT_MCC_FILE.equalsIgnoreCase("INSERT INTO mcc_file(mcc_id, fid) values "))
					{
					    executeTransaction(INSERT_MCC_FILE);
					}
				
				filePath = Directories.getAbsolutePath(Directories.CROSS_FILE_METHOD_STRUCTURE_EX);
				File file11 = new File(filePath);
				FileInputStream filein11 = new FileInputStream(file11);
				BufferedReader stdin11 = new BufferedReader(new InputStreamReader(filein11));
				int cluster_size,size=0;
				Vector<String> mccs = null;
				int mccId,fId;
				while ((line = stdin11.readLine()) != null)
				{
				    if (!line.equalsIgnoreCase(""))
				    {
					st = new StringTokenizer(line);
					String sid = st.nextToken().trim();
					int mcs_crossfile_id = Integer.parseInt(sid);
					String number = st.nextToken().trim();
					int nm = Integer.parseInt(number);
					insertMCS_CrossFile(mcs_crossfile_id, nm);
					line = stdin11.readLine();
					st1 = new StringTokenizer(line, ",");
					size = st1.countTokens();
					mccs = new Vector<>();
					cluster_size = 0;
					String pre = "", temp1;
					for (int i = 0; i < size; i++)
					{
					    temp1 = st1.nextToken();
					    if (!pre.equalsIgnoreCase(temp1))
					    {
						mccs.add(temp1);
						cluster_size++;
					    }
					    insertMCSCrossFile_MCC(mcs_crossfile_id, Integer.parseInt(temp1));
					    pre = temp1;
					}
					for (int i = 0; i < nm; i++)
					{
					    line = stdin11.readLine();
					    line = stdin11.readLine();
					    fId = Integer.parseInt(line.trim());
					    insertMCSCrossFile_File(mcs_crossfile_id, fId);
					    for (int j = 0; j < cluster_size; j++)
					    {
						mccId = Integer.parseInt(mccs.get(j).trim());
						line = stdin11.readLine();
						st2 = new StringTokenizer(line, ",");
						while (st2.hasMoreTokens())
						{
						    mid = st2.nextToken();
						    insertMCSCrossFile_Methods(mcs_crossfile_id, fId, mccId, Integer.parseInt(mid));
						}
					    }
					}
					line = stdin11.readLine();
				    }
				}

				if (!INSERT_MCS_CROSSFILE
					.equalsIgnoreCase("INSERT INTO mcs_crossfile(mcs_crossfile_id, members) values "))
				{
				    executeTransaction(INSERT_MCS_CROSSFILE);
				}
				if (!INSERT_MCSCROSSFILE_MCC
					.equalsIgnoreCase("INSERT INTO mcscrossfile_mcc(mcs_crossfile_id, mcc_id) values "))
				{
				    executeTransaction(INSERT_MCSCROSSFILE_MCC);
				}
				if (!INSERT_MCSCROSSFILE_FILE
					.equalsIgnoreCase("INSERT INTO mcscrossfile_file(mcs_crossfile_id, fid, did, gid) values "))
				{
				    executeTransaction(INSERT_MCSCROSSFILE_FILE);
				}
				if (!INSERT_MCSCROSSFILE_METHODS.equalsIgnoreCase(
					"INSERT INTO mcscrossfile_methods(mcs_crossfile_id, fid, mcc_id, mid) values "))
				{
				    executeTransaction(INSERT_MCSCROSSFILE_METHODS);
				}
				
				filein9.close();
				filein10.close();
				filein11.close();
			}
		catch(Exception e)
			{
			
			}
		}


	public static void insertFrame(int mccID, int mccInstanceID, int frameID, String ARTPath, int syncedFlag) {
	try { 
		Statement stmt = CloneRepository.getConnection().createStatement();
		
    	stmt.execute("use "+CloneRepository.getDBName());
    	
    	String query1 = "update mcc set framed = 1, art_files_path = '"+ARTPath+"' where mcc_id="+mccID+";";
    	//Insert_framed_mcc_details+frameID+","+mccID+","+'\"+ARTPath+ '\" +")";
    	stmt.executeUpdate(query1,Statement.RETURN_GENERATED_KEYS);
    	for (int i=0; i< mccInstanceID;i++) {
    		String query = "update mcc_instance set synced = 1 where mcc_instance_id = "+i+";";//Insert_framed_mcc+ mccID+","+i+","+syncedFlag+")";
    		stmt.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
    		
    		
    		
    	}
		
	}
	catch(Exception e) {
		e.printStackTrace();
	}
	
	
}
	
	 public static void deleteFrame(int mccID) {
	    	try {
	    		String query = "update mcc set framed =0 where mcc_id ="+mccID+";";
	        	String query2 = "update mcc_instance set synced =-1 where mcc_id ="+mccID+";";
	        	Statement stmt =  CloneRepository.getConnection().createStatement();
	        	stmt.executeUpdate(query,Statement.RETURN_GENERATED_KEYS);
	        	stmt.executeUpdate(query2, Statement.RETURN_GENERATED_KEYS);
	    	}
	    	catch(Exception ex) {
	    		ex.printStackTrace();
	    	}
	    }
	
	
}
