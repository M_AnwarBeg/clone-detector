package cmt.cddc.methodclones;

import java.util.ArrayList;
import java.util.Iterator;

import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;

public class MethodCloneStructure {
	private int mcsId;
	private ArrayList<Integer> vCloneClasses;
	private ArrayList<MethodClones> mccs;
	
	public MethodCloneStructure() {
		mcsId = -1;
		vCloneClasses = new ArrayList<Integer>();
		mccs = new ArrayList<MethodClones>();	
	}



	public int getMcsId() {
		return mcsId;
	}

	public void setMcsId(int mcsId) {
		this.mcsId = mcsId;
	}



	public ArrayList<Integer> getvCloneClasses() {
		return vCloneClasses;
	}



	public void setvCloneClasses(ArrayList<Integer> vCloneClasses) {
		this.vCloneClasses = vCloneClasses;
	}

	public ArrayList<MethodClones> getMccs() {
		return mccs;
	}

	public void addToStructure(int mccId)
	{
		this.vCloneClasses.add(mccId);
	}
	
	public void setMccs(ArrayList<MethodClones> mccs) {
		this.mccs = mccs;
	}


	public void addToMethodCloneClass(MethodClones mcc)
	{
		this.mccs.add(mcc);
	}
	
	
	
	
	public MethodClones getMCC(int mccId) {
		Iterator mcclistItr = mccs.iterator();
		while(mcclistItr.hasNext()) {
			MethodClones mcc = (MethodClones) mcclistItr.next();
			if(mcc.getClusterID() == mccId) {
				return mcc;
			}
			
		}
		return null;
	}
	
	
}
