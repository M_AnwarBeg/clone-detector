package cmt.cddc.methodclones;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.common.CommonUtility;
import cmt.cddc.fcim.MainTestFPClose_saveToFile;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.MCStruct;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;
import cmt.common.Directories;

public final class StructuralClones_MethodClusters 
{
	public static ArrayList<MethodClones> detectMethodClusters(ArrayList<SimpleClone> simpleClones)
	{	
		ArrayList<MethodClones> methodClusters = new ArrayList<MethodClones>();
		try 
		{			
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.CLONES_METHOD_NORMAL),Directories.getAbsolutePath(Directories.CROSS_METHOD_CLONE_STURCTURES) , 0);
			ArrayList<String> scsAcrossMethods = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_METHOD_CLONE_STURCTURES));
			if(scsAcrossMethods != null && scsAcrossMethods.size() > 0)
			{								
				//for every structure
				for(int i = 0 ; i < scsAcrossMethods.size() ; i++)
				{
					String [] line_parts = scsAcrossMethods.get(i).split("#");
					if(line_parts.length <= 1)
						continue;
					
					String [] support_temp = line_parts[1].split(" ");
					int support = Integer.parseInt(support_temp[1]); //the support is the count of occurrences
					
					if(support <= 1)
						continue;
					
					String [] temp = line_parts[0].split(" ");
					if(temp.length <= 0)
						continue;	
						
					ArrayList<String> clone_ids_spmf = new ArrayList<String>();
					Collections.addAll(clone_ids_spmf, temp);	
						
					//create a cluster
					MethodClones pMCluster= new MethodClones();						
					ArrayList<Integer> vCloneClasses = CommonUtility.convertStrArrListToInt(clone_ids_spmf , false);
					//sort the vector because the result is not ordered
					Collections.sort(vCloneClasses);
					pMCluster.setvCloneClasses(vCloneClasses);		
					
                    
	                //take the first clone class
	                int CC = pMCluster.getvCloneClassAt(0) / 100;    
	                    
	                //find all Methods that have this clone class
	                //use list for ease of deletion in middle
	                List<Integer> lTempMethods = new ArrayList<Integer>();
	                for (int k = 0; k < simpleClones.get(CC).getInstancesSize(); k++) {
	                    int methodID = simpleClones.get(CC).getInstanceAt(k).getMethodId();
	                    //some instances of this clone may be outside any method i.e., methodID = -1
	                    if (methodID >= 0) {
	                        lTempMethods.add(simpleClones.get(CC).getInstanceAt(k).getMethodId());
	                    }
	                }
	                //remove adjacent same file IDs
	                Set<Integer> hs_files = new HashSet<Integer>(lTempMethods);
	                lTempMethods.clear();    
	                lTempMethods.addAll(hs_files);
	                
	                //sort the files
	                Collections.sort(lTempMethods);
	
	                //for each Method in temporary list
	                for (int id = 0 ; id < lTempMethods.size() ; id++) 
	                {
	                    int methodID =lTempMethods.get(id);
	                    //if the Method does not have all members of this structure
	                    if(!(sMethod_List.getsMethodAt(methodID).getCloneClasses().containsAll(pMCluster.getvCloneClasses())))
	                    {
	                        lTempMethods.remove(id);
	                        id--;
	                    }                                               
	                }
	                
		            //group check here
		            if(!satisfyGroupCheck(lTempMethods))
		            	continue;
		            
		            //now temp list has only those Methods that have this complete 
		            //structure
		            //put these Method IDs in the cluster
		            pMCluster.setvMethods((ArrayList<Integer>)lTempMethods);

		            //lTempMethods is not used anymore
		            //lTempMethods.clear();
		            
		            for (int it = 0; it < pMCluster.getvMethodsSize(); it++) 
		            {
		                int methodID = pMCluster.getvMethodAt(it);
		                //cout<<"method # "<<methodID<<endl;
		                //find percentage coverage of this Method with this structure
		                //concern : send structure...with repetitions..not unique list
		                
		                sMarkedMethod sMM = new sMarkedMethod();
		                sMM.setMethodID(methodID);
		                int iCover = findMethodCoverage(sMM , pMCluster.getvCloneClasses(), simpleClones , true);
		                pMCluster.addsMarkedMethod(sMM);

		                pMCluster.addvMethodsToken(iCover);

		                int iMethodTokenCount = sMethod_List.getsMethodAt(methodID).getEndToken() - sMethod_List.getsMethodAt(methodID).getStartToken() + 1;
		                float iCoverP = ((float) iCover / (float) iMethodTokenCount)* 100;
		                pMCluster.addvMethodsCoverage(iCoverP);
		            }
		            //right place to insert the cluster in the list
		            pMCluster.setClusterID(methodClusters.size());
		            methodClusters.add(pMCluster);
				}
			}
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return methodClusters;		
	}
	
	private static boolean satisfyGroupCheck(List<Integer> lTempMethods)
	{
		if(lTempMethods == null || lTempMethods.size() <= 0)
			return false;
		
		if(SysConfig.getGroupCheck() == 2)
		{			
			boolean bSatisfyGroupCheck = false;
            int methodID = lTempMethods.get(0);
            int firstMethodGroup = sFile_List.getsFileAt(sMethod_List.getsMethodAt(methodID).getFileID()).getGroupID();
            for (int id = 0 ; id < lTempMethods.size() ; id++) {
                methodID = lTempMethods.get(id);
                int curMethodGroup = sFile_List.getsFileAt(sMethod_List.getsMethodAt(methodID).getFileID()).getGroupID();
                if (firstMethodGroup != curMethodGroup) {
                    bSatisfyGroupCheck = true;
                }
            }
            return bSatisfyGroupCheck;
		}
		else
			return true;
	}
	public static ArrayList<MethodClones> detectMethodClustersIncremental(ArrayList<SimpleClone> simpleClones, MCStruct MCC_SCCStructure)
	{	
		ArrayList<MethodClones> methodClusters = new ArrayList<MethodClones>();
		try 
		{			
			
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.CLONES_METHOD_NORMAL),Directories.getAbsolutePath(Directories.CROSS_METHOD_CLONE_STURCTURES) , 0.1);
			ArrayList<String> scsAcrossMethods = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_METHOD_CLONE_STURCTURES));
			for(String FCIMCluster : scsAcrossMethods)
			{
				MethodClones pMCluster= new MethodClones();		
				String [] line_parts = FCIMCluster.split("#");
				if(line_parts.length <= 1)
					continue;
				
				String [] support_temp = line_parts[1].split(" ");
				int support = Integer.parseInt(support_temp[1]); //the support is the count of occurrences
				
				if(support <= 1)
					continue;
				
				String [] temp = line_parts[0].split(" ");
				if(temp.length <= 0)
					continue;	
					
				ArrayList<String> clone_ids_spmf = new ArrayList<String>();
				Collections.addAll(clone_ids_spmf, temp);	
				
				
								
				ArrayList<Integer> vCloneClasses = CommonUtility.convertStrArrListToInt(clone_ids_spmf , true);
				//sort the vector because the result is not ordered
				Collections.sort(vCloneClasses);
				pMCluster.setvCloneClasses(vCloneClasses);
				//get methods for VcloneClasses. a method must contains all of vcloneclassess
				ArrayList<Integer> vMethodIds = MCC_SCCStructure.getMethodsBySCCIDs(vCloneClasses);
				pMCluster.setvMethods(vMethodIds);
				methodClusters.add(pMCluster);

				for(sMethod method : sMethod_List.getMethodsList())
				{
					method.setTokenCount(method.getEndToken() - method.getStartToken() +1);
				}

				for (int it = 0; it < pMCluster.getvMethodsSize(); it++) 
				{
					int methodID = pMCluster.getvMethodAt(it);
					//cout<<"method # "<<methodID<<endl;
					//find percentage coverage of this Method with this structure
					//concern : send structure...with repetitions..not unique list

					sMarkedMethod sMM = new sMarkedMethod();
					sMM.setMethodID(methodID);
					int iCover = 0;
					//findMethodCoverage(sMM , pMCluster.getvCloneClasses(), simpleClones , true);
					
					//findMethodCoverage(sMM , pMCluster.getvCloneClasses(), simpleClones , true);
					ArrayList<SimpleCloneInstance> sciInMethods = new ArrayList<>();
					for(SimpleClone clone : simpleClones)
					{
						if(vCloneClasses.contains(clone.getSSCId()))
						{
							for(SimpleCloneInstance sci : clone.getInstances())
							{
								if(sci.getMethodId() == methodID)
								{
									sciInMethods.add(sci);
								}
							}
						}
					}
					if(sciInMethods.size() > 1)
					{
						Integer lastIndex = 0;
						Integer smallestIndex = 0;
						SimpleCloneInstance smallest=sciInMethods.get(0);
						for(int j = 1; j < sciInMethods.size() ; j++)
						{
							if(smallest.getStartingIndex() < sciInMethods.get(j).getStartingIndex())
							{
								smallest = sciInMethods.get(j);
							}
						}
						iCover = smallest.getEndingIndex() - smallest.getStartingIndex();
						lastIndex = smallest.getEndingIndex();
						smallestIndex = smallest.getStartingIndex();
						sciInMethods.remove(smallest);
						
						for(int i  = 0 ; i <sciInMethods.size() ; i++)
						{
							if(sciInMethods.get(i).getStartingIndex() > lastIndex)
							{
								iCover += sciInMethods.get(i).getEndingIndex() -  sciInMethods.get(i).getStartingIndex();
								lastIndex = sciInMethods.get(i).getEndingIndex();
								smallestIndex = sciInMethods.get(i).getStartingIndex();
							}
							else if(sciInMethods.get(i).getStartingIndex() >= smallestIndex && sciInMethods.get(i).getEndingIndex() <= lastIndex)
							{
								//ignore complete overlaping case
							}
							else if(sciInMethods.get(i).getStartingIndex() >= smallestIndex && sciInMethods.get(i).getEndingIndex() > lastIndex)
							{
								iCover += sciInMethods.get(i).getEndingIndex() - lastIndex;
								lastIndex = sciInMethods.get(i).getEndingIndex();
							}
						}
						
						
					}
					else if (sciInMethods.size() == 1)
					{
						iCover = sciInMethods.get(0).getEndingIndex() - sciInMethods.get(0).getStartingIndex();
					}
					pMCluster.addsMarkedMethod(sMM);

					pMCluster.addvMethodsToken(iCover);

					int iMethodTokenCount = sMethod_List.getMethodWithId(methodID).getTokenCount(); // .getEndToken() - sMethod_List.getMethodAtId(methodID).getStartToken() + 1;
					float iCoverP = ((float) iCover / (float) iMethodTokenCount)* 100;
					pMCluster.addvMethodsCoverage(iCoverP);
				}
			}
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return methodClusters;
	}

	public static int findMethodCoverage(sMarkedMethod smm , ArrayList<Integer> vCloneClasses , ArrayList<SimpleClone> simpleClones , boolean isTransformedIds) {

	    int iMethodStartToken = sMethod_List.getsMethodAt(smm.getMethodID()).getStartToken(); 
	    int iMethodTokenCount = sMethod_List.getsMethodAt(smm.getMethodID()).getEndToken() - iMethodStartToken + 1;	    

	    smm.setvBMarkedTokensToFalse(new Boolean[iMethodTokenCount]);
	    int iCoverage = 0;
	    int i = 0;
	    //for every unique member of the vector
	    while (i < vCloneClasses.size()) {
	        
	        int CC = 0 , divisor = 0;
			if(isTransformedIds)
			{
				CC = vCloneClasses.get(i) / 100;
				divisor = 100;
			}
			else
			{
				CC = vCloneClasses.get(i);
				divisor = 1;
			}
	        int count = 0;
	        //count the same members
	        while (i < vCloneClasses.size() && vCloneClasses.get(i)/divisor == CC) {
	            count++;
	            i++;
	        }
	        for (int j = 0; j < simpleClones.get(CC).getInstancesSize(); j++) {
	            if (simpleClones.get(CC).getInstanceAt(j).getMethodId() == smm.getMethodID() && count > 0) {
	                count--;
	                int startToken = simpleClones.get(CC).getInstanceAt(j).getStartingIndex();	                
	                int endToken = simpleClones.get(CC).getInstanceAt(j).getEndingIndex();
	                for (int k = startToken; k <= endToken; k++) {
	                	if(!smm.getvBMarkedTokenAt(k - iMethodStartToken))
	                	{
		                	smm.setvBMarkedTokenAt(k - iMethodStartToken, true);
		                	iCoverage++;
	                	}
	                }
	            }
	        }
	    }
        
	    return iCoverage;
	}
	
	public static void pruneMethodClusters(ArrayList<MethodClones> methodClusters, boolean isIncremetal)
	{
		FC_FirstPruning(methodClusters);
		sort(methodClusters);
		FC_SecondPruning(methodClusters);
		if(!isIncremetal)
			FC_ThirdPruning(methodClusters);
	}

	private static void FC_IncrmentalThirdProoning(ArrayList<MethodClones> methodClusters) {
		for(int i = 0 ; i < methodClusters.size() ; i++)
		{
			MethodClones cluster1= methodClusters.get(i);
			
			for(int j = i+1 ; j < methodClusters.size() ; j++)
			{
				MethodClones cluster2 = methodClusters.get(j);
				if(cluster1.getvMethods().containsAll(cluster2.getvMethods()) && cluster1.getvMethods().size() > cluster2.getvMethods().size())
				{
					cluster1.setbSignificant(true);
					break;
				}
			}
			if (!cluster1.isbSignificant()) 
			{
				methodClusters.remove(i);
	            i--;
	        }
		}	
	}

	private static void sort(ArrayList<MethodClones> methodClusters)
	{
		Comparator<MethodClones> customComp = new MethodClones();
		methodClusters.sort(customComp);		
	}
	
	//prune all clusters with coverage less than threshold
	private static void FC_FirstPruning(ArrayList<MethodClones> methodClusters) 
	{
		for(int i = 0 ; i < methodClusters.size() ; i++)
		{
			MethodClones cluster= methodClusters.get(i);
			for(int j = 0 ; j < cluster.vMethodsCoverageSize() ; j++)
			{
				if (cluster.getvMethodCoverageAt(j) > SysConfig.getMinMClusP() && cluster.getvMethodTokensAt(j) > SysConfig.getMinMClusT()) 
				{
					cluster.setbSignificant(true);
	                break;
	            }
			}
			if (!cluster.isbSignificant()) 
			{
				methodClusters.remove(i);	            	            
	            i--;	            
	            
	        }
		}
	}
	
	//prune all clusters whose methods are all present in another single cluster
	private static void FC_SecondPruning(ArrayList<MethodClones> methodClusters) 
	{
		for(int i = 0 ; i < methodClusters.size() ; i++)
		{
			MethodClones cluster1= methodClusters.get(i);
			
			for(int j = i+1 ; j < methodClusters.size() ; j++)
			{
				MethodClones cluster2 = methodClusters.get(j);
				if(cluster2.getvMethods().containsAll(cluster1.getvMethods()))
				{
					cluster1.setbSignificant(false);
					break;
				}
			}
			if (!cluster1.isbSignificant()) 
			{
				methodClusters.remove(i);
	            i--;
	        }
		}		
	}

	//prune clusters whose all methods are present in other different clusters
	private static void FC_ThirdPruning(ArrayList<MethodClones> methodClusters) 
	{
		for(int i = 0 ; i < methodClusters.size() ; i++)
		{
			MethodClones cluster1= methodClusters.get(i);
			
			//for each Method in this cluster
			for (int j = 0; j < cluster1.getvMethodsSize(); j++) 
			{
	            int methodID = cluster1.getvMethodAt(j);
	            
	            //look for this Method in all other clusters
	            for (int k = i+1; k < methodClusters.size(); k++) 
	            {
	            	MethodClones cluster2 = methodClusters.get(k);
					
					//if the method is found in 2nd cluster...break   
					if(cluster2.getvMethods().contains(methodID))
					{
						cluster1.setbSignificant(false);
						break;
					}
	            }
	            //if method not found in any cluster...this cluster is significant
				//if(cluster1.isbSignificant())
				//	break;
			}
			if(!cluster1.isbSignificant())
			{
				methodClusters.remove(i);
	            i--;
			}
		}
	}

	//lists which method clone classes are represented in each file
	public static void updateFilesWithMethodClones(ArrayList<MethodClones> methodClusters)
	{
		//for each method cluster
		for(MethodClones cluster : methodClusters)
		{
			//for each method in this cluster
	        for (int j = 0; j < cluster.getvMethodsSize(); j++) 
	        {
	        	int normalMC = (cluster.getClusterID())*100;
	            int methodID = cluster.getvMethodAt(j); 
	            //update the corresponding file
	            int fileID = sMethod_List.getsMethodAt(methodID).getFileID(); 
	            int numberMC = sFile_List.getsFileAt(fileID).getvCloneMethodClassSize();
	            int lastMC = 0;
	            
	            if (numberMC > 0) 
	            {
	                lastMC = sFile_List.getsFileAt(fileID).getvCloneMethodClassAt(numberMC - 1);
	                if (lastMC / 100 == cluster.getClusterID()) 
	                {
	                    normalMC = lastMC + 1;
	                }
	            }
	            sFile_List.getsFileAt(fileID).addvCloneMethodClass(normalMC);	            
	        }
		}
	}
	
	public static void findCrossFileCloneMethodStructures()
	{
		try 
		{
			MainTestFPClose_saveToFile.getStructClones_SPMFAlgo(Directories.getAbsolutePath(Directories.METHOD_CLONES_BY_FILE_NORMAL),Directories.getAbsolutePath(Directories.CROSS_FILE_METHOD_STRUCTURE), 0.1);
		
		} catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
