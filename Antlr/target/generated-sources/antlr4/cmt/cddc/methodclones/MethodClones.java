package cmt.cddc.methodclones;

import java.util.ArrayList;
import java.util.Comparator;

public class MethodClones implements Comparator<MethodClones> {
	private int methodCloneId;
	private ArrayList<Integer> vCloneClasses;
	private ArrayList<Integer> vMethods;
	private ArrayList<sMarkedMethod> vMarkedMethods;
	private ArrayList<Integer> vMethodsToken;
	private ArrayList<Float> vMethodsCoverage;
	private boolean bSignificant;

	private int instanceCount;
	private ArrayList<MethodCloneInstance> mccInstances;
	private boolean framed;
	private double averageTokenCount;
	private double averagePercentageCount;

	

	public MethodClones() {
		this.methodCloneId = -1;
		this.vCloneClasses = new ArrayList<Integer>();
		this.vMethods = new ArrayList<Integer>();
		this.vMarkedMethods = new ArrayList<sMarkedMethod>();
		this.vMethodsToken = new ArrayList<Integer>();
		this.vMethodsCoverage = new ArrayList<Float>();
		this.bSignificant = false;
		mccInstances = new ArrayList<>();
	}

	public MethodClones(int methodCloneId, int instanceCount, ArrayList<Integer> vCloneClasses) {
		// TODO Auto-generated constructor stub
		this.instanceCount = instanceCount;
		this.methodCloneId = methodCloneId;
		this.vCloneClasses = vCloneClasses;
		mccInstances = new ArrayList<>();
	}

	public MethodClones(int methodCloneId, double averageTokenCount, double averagePercentageCount, int instanceCount, ArrayList<Integer> vCloneClasses) {
		// TODO Auto-generated constructor stub
		this.instanceCount = instanceCount;
		this.methodCloneId = methodCloneId;
		this.vCloneClasses = vCloneClasses;
		this.averagePercentageCount = averagePercentageCount;
		this.averageTokenCount = averageTokenCount;
		mccInstances = new ArrayList<>();
	}
	
	public int getMCCInstanceCount() {
		return instanceCount;
	}

	public void setMCCInstanceCount(int count) {
		this.instanceCount = count;
	}

	public int getClusterID() {
		return methodCloneId;
	}

	public void setClusterID(int clusterID) {
		this.methodCloneId = clusterID;
	}

	public ArrayList<Integer> getvCloneClasses() {
		return vCloneClasses;
	}

	public void setvCloneClasses(ArrayList<Integer> vCloneClasses) {
		this.vCloneClasses = vCloneClasses;
	}

	public ArrayList<Integer> getvMethods() {
		return vMethods;
	}

	public void setvMethods(ArrayList<Integer> vMethods) {
		this.vMethods = vMethods;
	}

	public ArrayList<sMarkedMethod> getvMarkedMethods() {
		return vMarkedMethods;
	}

	public void setvMarkedMethods(ArrayList<sMarkedMethod> vMarkedMethods) {
		this.vMarkedMethods = vMarkedMethods;
	}

	public ArrayList<Integer> getvMethodsToken() {
		return vMethodsToken;
	}

	public void setvMethodsToken(ArrayList<Integer> vMethodsToken) {
		this.vMethodsToken = vMethodsToken;
	}

	public ArrayList<Float> getvMethodsCoverage() {
		return vMethodsCoverage;
	}

	public void setvMethodsCoverage(ArrayList<Float> vMethodsCoverage) {
		this.vMethodsCoverage = vMethodsCoverage;
	}

	public boolean isbSignificant() {
		return bSignificant;
	}

	public void setbSignificant(boolean bSignificant) {
		this.bSignificant = bSignificant;
	}

	public int getvCloneClassesSize() {
		return this.vCloneClasses.size();
	}

	public int getvCloneClassAt(int index) {
		return this.vCloneClasses.get(index);
	}

	public int getvMethodsSize() {
		return this.vMethods.size();
	}

	public int getvMarkedMethodsSize() {
		return this.vMarkedMethods.size();
	}

	public void addvCloneClass(int cloneClass) {
		this.vCloneClasses.add(cloneClass);
	}

	public void addvMethod(int methodId) {
		this.vMethods.add(methodId);
	}

	public int getvMethodAt(int index) {
		return this.vMethods.get(index);
	}

	public int getvMethodTokensAt(int index) {
		return this.vMethodsToken.get(index);
	}

	public Float getvMethodCoverageAt(int index) {
		return this.vMethodsCoverage.get(index);
	}

	public void addvMethodsToken(int tokensCount) {
		this.vMethodsToken.add(tokensCount);
	}

	public void addvMethodsCoverage(float cov) {
		this.vMethodsCoverage.add(cov);
	}

	public void addsMarkedMethod(sMarkedMethod smm) {
		this.vMarkedMethods.add(smm);
	}

	public int vMethodsCoverageSize() {
		return this.vMethodsCoverage.size();
	}

	public float MaxCoverage() {
		float maxCov = 0;
		for (float cov : vMethodsCoverage) {
			if (cov > maxCov)
				maxCov = cov;
		}
		return maxCov;
	}

	public ArrayList<MethodCloneInstance> getMCCInstances() {
		return mccInstances;
	}

	public void setMCCInstances(ArrayList<MethodCloneInstance> mccInstances) {
		this.mccInstances = mccInstances;
	}

	public void addMCCInstance(MethodCloneInstance instance) {
		this.mccInstances.add(instance);
	}

	@Override
	public int compare(MethodClones arg0, MethodClones arg1) {
		// implements the logic how two method clusters are compared
		int retVal = 0;
		if (arg0.getvMethodsSize() < arg1.getvMethodsSize()) {
			retVal = 1;
		} else if (arg0.getvMethodsSize() == arg1.getvMethodsSize()) {
			if (arg0.MaxCoverage() < arg1.MaxCoverage()) {
				retVal = 1;
			}
		}
		return retVal;
	}

	public boolean isFramed() {
		return framed;
	}

	public void setFramed(boolean framed) {
		this.framed = framed;
	}
	
	public double getAverageTokenCount() {
		return averageTokenCount;
	}

	public void setAverageTokenCount(double averageTokenCount) {
		this.averageTokenCount = averageTokenCount;
	}

	public double getAveragePercentageCount() {
		return averagePercentageCount;
	}

	public void setAveragePercentageCount(double averagePercentageCount) {
		this.averagePercentageCount = averagePercentageCount;
	}
}
