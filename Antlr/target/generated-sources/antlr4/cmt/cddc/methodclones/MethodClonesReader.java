package cmt.cddc.methodclones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;
import cmt.common.Directories;
import cmt.cvac.viewobjects.PrimaryMcsObject;
import cmt.cvac.viewobjects.SecondaryMcsObject;


public class MethodClonesReader {
	
	private ArrayList<MethodClones> mccList;
    private static ArrayList<sMethod> methodList;
    private static ArrayList<MethodClones> filteredMCCList;
    private static ArrayList<sMethod> filteredMethodList;
    private static ArrayList<PrimaryMcsObject> mcsList;
    private static ArrayList<PrimaryMcsObject> filteredmcsList;
    static SecondaryMcsObject secobj;
    static PrimaryMcsObject preobj;
    private static ArrayList<MethodCloneStructure> methodCloneStructure;
    
    
    
    
      public ArrayList<MethodCloneStructure> getMethodCloneStructures(){
    	  loadMethodCloneStructure();
    	  return methodCloneStructure;
    	  
    	  
      }
    
    
    public MethodClonesReader() {
    	mccList = new ArrayList<MethodClones>();
    	filteredMCCList = new ArrayList<MethodClones>();
    	methodList = new ArrayList<sMethod>();
    	filteredMethodList = new ArrayList<sMethod>();
    	mcsList=new ArrayList<PrimaryMcsObject>();
    	filteredmcsList = new ArrayList<PrimaryMcsObject>();
    	methodCloneStructure = new ArrayList<MethodCloneStructure>();
    	
    	
    }
    
    public static ArrayList<PrimaryMcsObject> getMcsList()
    {
    	return mcsList;
    }
    public static ArrayList<PrimaryMcsObject> getFilteredMcsList()
    {
    	return filteredmcsList;
    }
    public void loadMcs()
    { 
    	try
    	{
	    	Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+ CloneRepository.getDBName()+";");
	
		    ResultSet rs = stmt.executeQuery("select * from mcs_crossfile");
		    int i=0;
		    while(rs.next())
		    {
		    	PrimaryMcsObject object=new PrimaryMcsObject();
		    	object.setMcsId(rs.getInt(1));
		    	object.setNofInstances(rs.getInt(2));
		    	object.setId(i);
		    	Statement stmt2 = CloneRepository.getConnection().createStatement();
			    stmt2.execute("use "+ CloneRepository.getDBName()+";");
		
			    ResultSet rs2 = stmt2.executeQuery("select mcc_id from mcscrossfile_mcc where mcs_crossfile_id="+rs.getInt(1));
			    
			    while(rs2.next())
			    {
			    	object.addToStructure(rs2.getInt(1));
			    }
			    
			    int id=0;
			    Statement stmt3 = CloneRepository.getConnection().createStatement();
			    stmt3.execute("use "+ CloneRepository.getDBName()+";");
		
			    ResultSet rs3 = stmt3.executeQuery("select * from mcscrossfile_file where mcs_crossfile_id="+rs.getInt(1));
			    
			    while(rs3.next())
			    {
			    	Statement stmt4 = CloneRepository.getConnection().createStatement();
				    stmt4.execute("use "+ CloneRepository.getDBName()+";");
			
				    ResultSet rs4 = stmt4.executeQuery("select fname from file where fid="+rs3.getInt(2));
				    rs4.next();

			    	object.addToCloneList(new SecondaryMcsObject(rs3.getInt(1), rs3.getInt(2),rs3.getInt(3),rs3.getInt(4),rs4.getString(1)));
			    	id++;
			    }
			    mcsList.add(object);
			    i++;
		    }
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    
    public static void loadMethodCloneStructure() {
    	try
    	{
		    ResultSet rsMcs = CloneRepository.invokeActiveCloneRepository().executeQuery("select * from mcs_crossfile");
		    while(rsMcs.next())
		    {
		    	MethodCloneStructure mcs = new MethodCloneStructure();
		    	mcs.setMcsId(rsMcs.getInt(1));	
		    	ResultSet rsMcsStructure = CloneRepository.invokeActiveCloneRepository().executeQuery("select mcc_id from mcscrossfile_mcc where mcs_crossfile_id="+rsMcs.getInt(1));
			    while(rsMcsStructure.next())
			    {
			    	mcs.addToStructure(rsMcsStructure.getInt(1));
			    }
			    CloneRepository.invokeActiveCloneRepository().execute("use "+ CloneRepository.getDBName()+";");
			    ResultSet rsMcsMethods = CloneRepository.invokeActiveCloneRepository().executeQuery("select * from mcscrossfile_methods where mcs_crossfile_id="+rsMcs.getInt(1));
			   
			    
			    while(rsMcsMethods.next())
			    {
			    	int mccId = rsMcsMethods.getInt(3);
			    	MethodClones methodClone = mcs.getMCC(mccId);
			    	if(methodClone==null) {
			    		methodClone = new MethodClones();
			    		methodClone.setClusterID(mccId);
				    	mcs.addToMethodCloneClass(methodClone);

			    	}
			    	int methodId = rsMcsMethods.getInt(4);
			    	Iterator methodListItr = methodList.iterator();
			    	while(methodListItr.hasNext()) {
			    		sMethod method  = (sMethod) methodListItr.next();
			    		if(methodId == method.getMethodId())
			    		{
					    	methodClone.addMCCInstance(new MethodCloneInstance(mccId, -1, method));
			    			break;
			    		}			    			
			    	}
			    }
			    	
	    		methodCloneStructure.add(mcs);
		  }
    	}    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    
    
    
    
    
    
    
    
    
    
    public static void filterMCS(String minMembers, String maxMembers, String location, String locationIDS, String mccIDS) {
    	String query, query2,query3 = "";
    	boolean mcsfilterFlag = false;
    	//boolean locationflag = !location.equals("-1");
    	String locationID[];
    	String mccID[];
    	ArrayList<Integer> mcsIds = new ArrayList<Integer>();
    	query = "select mcs_crossfile_id from mcs_crossfile where ";
    	query2 = "select distinct mcs_crossfile_id from mcscrossfile_file where ";
    	query3 = "select distinct mcs_crossfile_id from mcscrossfile_mcc where ";
    	if(!minMembers.isEmpty()) {
    		if(mcsfilterFlag) {
    			query +="or mcs_crossfile.members >= "+minMembers+" ";
    		}
    		else {
    			query +="mcs_crossfile.members >= "+minMembers+" ";
    			mcsfilterFlag = true;
    		}
    	}
    	if(!maxMembers.isEmpty()) {
    		if(mcsfilterFlag) {
    			query +="or mcs_crossfile.members <= "+maxMembers+" ";
    		}
    		else {
    			query +="mcs_crossfile.members <= "+maxMembers+" ";
    			mcsfilterFlag = true;
    		}
    		
    	}
    	if(Integer.valueOf(location)>=0) {
    		locationID = locationIDS.split(",");
    		for(int i = 0; i<locationID.length;i++) {
    			if(mcsfilterFlag) {
    				switch (Integer.valueOf(location)) {
    				case 0 :
    					query2 +="or gid = "+Integer.valueOf(locationID[i])+" ";
    					break;	
    					
    				case 1:
    					query2 +="or did = "+Integer.valueOf(locationID[i])+" ";
    					break;
    				case 2:
    					query2 += "or fid = "+Integer.valueOf(locationID[i])+" ";
    					break;
    				}
    					
    			}
    			else {
    				switch (Integer.valueOf(location)) {
    				case 0 :
    					query2 +="gid = "+Integer.valueOf(locationID[i])+" ";
    					mcsfilterFlag = true;
    					break;	
    					
    				case 1:
    					query2 +="did = "+Integer.valueOf(locationID[i])+" ";
    					mcsfilterFlag = true;
    					break;
    				case 2:
    					query2 += "fid = "+Integer.valueOf(locationID[i])+" ";
    					mcsfilterFlag = true;
    					break;
    				}
    				
    			}
    		}
    	}
    		if(!mccIDS.isEmpty()) {
        		mccID = mccIDS.split(",");
        		for(int i =0;i<mccID.length;i++) {
        			if(mcsfilterFlag) {
        				query3 += "or mcc_id = "+Integer.valueOf(mccID[i])+" ";
        			}
        			else {
        				query3 += "mcc_id = "+Integer.valueOf(mccID[i])+" ";
        				mcsfilterFlag = true;
        				
        			}
        		}
        		
        	}
    		query+=";";
    		query3+=";";
    		query2+=";";
    		try {
    			ArrayList<Integer> structureList = new ArrayList<Integer>();
    			int MCSID, MCSInstanceID, numberofInstances;
    			
    			if(Integer.valueOf(location)>=0) {
    				Statement stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				ResultSet rsLocation = stmnt.executeQuery(query2);
    				while(rsLocation.next()) {
    					int id = rsLocation.getInt(1);
    					if(!mcsIds.contains(id)) {
    						mcsIds.add(id);
    						
    					}
    				}
    				
    				
    			}
    			if(!minMembers.isEmpty()) {
    				Statement stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				ResultSet rsLocation = stmnt.executeQuery(query);
    				while(rsLocation.next()) {
    					int id = rsLocation.getInt(1);
    					if(!mcsIds.contains(id)) {
    						mcsIds.add(id);
    						
    					}
    				}
    				
    			}
    			if(!maxMembers.isEmpty()) {
    				Statement stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				ResultSet rsMCSInstanceCount = stmnt.executeQuery(query);
    				while(rsMCSInstanceCount.next()) {
    					int id = rsMCSInstanceCount.getInt(1);
    					if(!mcsIds.contains(id)) {
    						mcsIds.add(id);
    						
    					}
    				}
    			}
    			if(!mccIDS.isEmpty()) {
    				Statement stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				ResultSet rsMCCId = stmnt.executeQuery(query3);
    				while(rsMCCId.next()) {
    					int id = rsMCCId.getInt(1);
    					if(!mcsIds.contains(id)) {
    						mcsIds.add(id);
    						
    					}
    				}
    			}
    			String getMembers ="Select members from mcs_crossfile where mcs_crossfile_id = ";
    			//PrimaryMcsObject obj =new P 
    			String getStructure = "select mcc_id from mcscrossfile_mcc where mcs_crossfile_id = ";
    			String getMCSInstances ="select mcscrossfile_file.mcs_crossfile_id, mcscrossfile_file.fid,mcscrossfile_file.did, mcscrossfile_file.gid,file.fname from mcscrossfile_file\r\n" + 
    					"inner join file on mcscrossfile_file.fid = file.fid \r\n" + 
    					"where mcscrossfile_file.mcs_crossfile_id = ";
    			String getFileName = "select fname from file where fid = ";
    			
    			for(int i =0; i<mcsIds.size();i++) {
    				int members = 0;
        			ArrayList<Integer> structure = new ArrayList<Integer>();
        			
        			
    				getMembers += mcsIds.get(i)+";";
    				getStructure += mcsIds.get(i)+";";
    				getMCSInstances += mcsIds.get(i)+";";
    				
    				Statement stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				ResultSet rs = stmnt.executeQuery(getMembers);
    				while(rs.next()) {
    					members = rs.getInt(1);
    				}
    				/*rs.close();*/
    				stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				rs = stmnt.executeQuery(getStructure);
    				while(rs.next()) {
    					structure.add(rs.getInt(1));
    				}
    				preobj = new PrimaryMcsObject(i ,mcsIds.get(i), structure, members);
    				
    				stmnt = CloneRepository.getConnection().createStatement();
    				stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    				rs = stmnt.executeQuery(getMCSInstances);
    				int instanceCounter = 0;
    				while(rs.next()) {
    					getFileName += rs.getInt(2)+";";
    					int fid = rs.getInt(2);
    					int did = rs.getInt(3);
    					int gid = rs.getInt(4);
    					/*Statement stmnt01 = CloneRepository.getConnection().createStatement();
    					stmnt.executeQuery("use "+ CloneRepository.getDBName()+";");
    					ResultSet rs01 = stmnt01.executeQuery(getFileName);
    					while(rs01.next()) {*/
    						secobj = new SecondaryMcsObject(instanceCounter, fid, did, gid, rs.getString(5));
    					//}
    					//rs01.close();
    					
    					preobj.addToCloneList(secobj);
    					instanceCounter++;
    					
    					
    				}
    				filteredmcsList.add(preobj);
    				getMembers ="Select members from mcs_crossfile where mcs_crossfile_id = ";
        			//PrimaryMcsObject obj =new P 
        			getStructure = "select mcc_id from mcscrossfile_mcc where mcs_crossfile_id = ";
        			getMCSInstances ="select mcscrossfile_file.mcs_crossfile_id, mcscrossfile_file.fid,mcscrossfile_file.did, mcscrossfile_file.gid,file.fname from mcscrossfile_file\r\n" + 
        					"inner join file on mcscrossfile_file.fid = file.fid " + 
        					"where mcscrossfile_file.mcs_crossfile_id = ";
        			getFileName = "select fname from file where fid = ";
    				
    				
    				
    				
    			}
    		}
    			
    		
    		catch(Exception e) {
    			e.printStackTrace();
    		}
    		
    	
    	
    	
    	
    	
    	
    }
    public static void filterMethodClones(String minMembers, String maxMembers, String locationId, String location, String minATC, String maxATC, String minAPC, String maxAPC) 
    {
    	String query="";
    	String IDS[]= {};
    	boolean mccfilterFlag = false;
    	
    	if(!location.isEmpty()) {

			query = "select mcc.mcc_id, mcc.atc, mcc.apc, mcc.members, mcc_file.fid from mcc "
					+"inner join mcc_file on mcc_file.mcc_id = mcc.mcc_id where ";

			if(!locationId.isEmpty()) {
				 
				 if(locationId.contains(",")) {
					  IDS = locationId.split(",");
				 }
				 if(IDS.length>0) {
					 for(int i =0;i<IDS.length;i++) {
						 if(mccfilterFlag) {
							 query += "or mcc_file.fid = "+IDS[i]+ " ";
						 }
						 else {
							 query += "mcc_file.fid = "+IDS[i]+ " ";
							 mccfilterFlag = true;
						 }
						 
					 }
				 }
				 else {
					 if(mccfilterFlag) {
						 query += "or mcc_file.fid = "+locationId+ " ";
					 }
					 else {
						 query += "mcc_file.fid = "+locationId+ " ";
						 mccfilterFlag = true;
					 }
				 }
			}
    	}
    	else {
    		query = "select mcc.mcc_id, mcc.atc, mcc.apc, mcc.members from mcc where ";
    	}
    	if(!minMembers.isEmpty()) {
    		if(mccfilterFlag) {
				query += "and mcc.members >= "+minMembers+" ";
			 }
			 else {
				 query += "mcc.members >= "+minMembers+" ";
				 mccfilterFlag = true;
			 }
    	}
    	if(!maxMembers.isEmpty()) {
    		if(mccfilterFlag) {
				query += "and mcc.members <= "+maxMembers+" ";
			 }
			 else {
				 query += "mcc.members <= "+maxMembers+" ";
				 mccfilterFlag = true;
			 }
    	}
    	if(!minATC.isEmpty()) {
    		if(mccfilterFlag) {
				query += "and mcc.atc >= "+minATC+" ";
			 }
			 else {
				 query += "mcc.atc >= "+minATC+" ";
				 mccfilterFlag = true;
			 }
    	}
    	if(!maxATC.isEmpty()) {
    		if(mccfilterFlag) {
				query += "and mcc.atc <= "+maxATC+" ";
			 }
			 else {
				 query += "mcc.atc <= "+maxATC+" ";
				 mccfilterFlag = true;
			 }
    	}
    	if(!minAPC.isEmpty()) {
    		if(mccfilterFlag) {
				query += "and mcc.apc >= "+minAPC+" ";
			 }
			 else {
				 query += "mcc.apc >= "+minAPC+" ";
				 mccfilterFlag = true;
			 }
    	}
    	if(!maxAPC.isEmpty()) {
    		if(mccfilterFlag) {
				query += "and mcc.apc <= "+maxAPC+" ";
			 }
			 else {
				 query += "mcc.apc <= "+maxAPC+" ";
				 mccfilterFlag = true;
			 }
    	}
    	query += "group by mcc.mcc_id;";
    	try {
    		ArrayList<Integer> SCCIDs = null;
    	    int mccId = -99;
    	    int instances = -99;
    	    int methodId = -99;
    	    int fid = -1;
    	    double coverage = -99.00;
    	    double averageTokenCount = 0 ;
    	    double averagePercentageCount = 0;
    	    
    	    Statement stmt4 = CloneRepository.getConnection().createStatement();
 		    stmt4.execute("use "+ CloneRepository.getDBName()+";");
 		    ResultSet rs4 = stmt4.executeQuery("select m.*,mf.fid from method m, method_file mf where m.mid =  mf.mid ;");

 		    while (rs4.next())
 		    {
 		    	sMethod m = new sMethod(rs4.getString(2), rs4.getInt(1), rs4.getInt(6), rs4.getInt(4), rs4.getInt(5),
 				rs4.getInt(3));
 		    	filteredMethodList.add(m);
 		    }
    		Statement stmt = CloneRepository.getConnection().createStatement();
        	stmt.executeQuery("use "+CloneRepository.getDBName()+";");
        	ResultSet rs = stmt.executeQuery(query);
        	while(rs.next()) {
        		mccId = rs.getInt(1);
        		averageTokenCount = rs.getDouble(2);
        		averagePercentageCount = rs.getDouble(3);
        		instances = rs.getInt(4);
        		SCCIDs = new ArrayList<>();
        		
        		Statement stmt2 = CloneRepository.getConnection().createStatement();
        		stmt2.execute("use "+ CloneRepository.getDBName()+";");
        		ResultSet rs2 = stmt2.executeQuery("select scc_id from mcc_scc where mcc_id=" + mccId);

        		while (rs2.next())
        		{
        		    SCCIDs.add(rs2.getInt(1));
        		}
        		
        		
        		Statement stmt3 = CloneRepository.getConnection().createStatement();
        		stmt3.execute("use "+ CloneRepository.getDBName()+";");
        		ResultSet rs3 = stmt3.executeQuery("select mid,pc,fid from mcc_instance where mcc_id=" + mccId);

        		while (rs3.next())
        		{
        		    methodId = rs3.getInt(1);
        		    coverage = rs3.getDouble(2);
        		    fid = rs3.getInt(3);

        		    sMethod method = null;
        		   /* for(int j =0; j <methodList.size();j++) {
        		    	
        		    }*/
        		    for (int i = 0; i < filteredMethodList.size(); i++)
        		    {
        			if (filteredMethodList.get(i).getMethodId() == methodId)
        			{
        			    method = filteredMethodList.get(i);
        			}
        		    }
        		    boolean added = false;
        		    MethodCloneInstance temp = new MethodCloneInstance(methodId, coverage, method, fid);
        		    MethodClones temp2 = new MethodClones(mccId, averageTokenCount, averagePercentageCount,instances, SCCIDs);
        		    for (int i = 0; i < filteredMCCList.size(); i++)
        		    {
        			if (filteredMCCList.get(i).getClusterID() == mccId)
        			{
        				filteredMCCList.get(i).addMCCInstance(temp);
        			    added = true;
        			}
        		    }
        		    if (!added)
        		    {
        			temp2.addMCCInstance(temp);
        			filteredMCCList.add(temp2);
        		    }
        		}
        		
        		
        		
        	}
    		
    	}
    	catch(Exception ex) {
    		ex.printStackTrace();
    	}
    	
    	
    }
    
    
	public void  MethodsInfoParser_loadMethodList() {
    try
	{
		    Statement stmt = CloneRepository.getConnection().createStatement();
		    stmt.execute("use "+ CloneRepository.getDBName()+";");
		    ResultSet rs = stmt.executeQuery("select m.*,mf.fid from method m, method_file mf where m.mid=mf.mid");
		    while (rs.next())
		    {
			sMethod m = new sMethod(rs.getString(2), rs.getInt(1), rs.getInt(6), rs.getInt(4), rs.getInt(5),
				rs.getInt(3));
			methodList.add(m);
		    }
	} 
	catch (Exception e1)
	{
		    e1.printStackTrace();
		}
	}
    
    
    public void MethodClustersXXParser_loadMethodList()
    {

	try
	{
	    ArrayList<Integer> SCCIDs = null;
	    int mccId = -99;
	    int instances = -99;
	    int methodId = -99;
	    int fid = -1;
	    double coverage = -99.00;
	    double averageTokenCount = 0 ;
	    double averagePercentageCount = 0;

	    Statement stmt = CloneRepository.getConnection().createStatement();
	    stmt.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet rs = stmt.executeQuery("select mcc_id,atc,apc,members from mcc;");

	    while (rs.next())
	    {
		mccId = rs.getInt(1);
		averageTokenCount = rs.getDouble(2);
		averagePercentageCount = rs.getDouble(3);
		instances = rs.getInt(4);
		SCCIDs = new ArrayList<>();

		Statement stmt2 = CloneRepository.getConnection().createStatement();
		stmt2.execute("use "+ CloneRepository.getDBName()+";");
		ResultSet rs2 = stmt2.executeQuery("select scc_id from mcc_scc where mcc_id=" + mccId);

		while (rs2.next())
		{
		    SCCIDs.add(rs2.getInt(1));
		}

		Statement stmt3 = CloneRepository.getConnection().createStatement();
		stmt3.execute("use "+ CloneRepository.getDBName()+";");
		ResultSet rs3 = stmt3.executeQuery("select  mid,pc,fid from mcc_instance where mcc_id=" + mccId);

		while (rs3.next())
		{
		    methodId = rs3.getInt(1);
		    coverage = rs3.getDouble(2);
		    fid = rs3.getInt(3);
		    sMethod method = null;
		    for (int i = 0; i < methodList.size(); i++)
		    {
			if (methodList.get(i).getMethodId() == methodId)
			{
			    method = methodList.get(i);
			}
		    }
		    boolean added = false;
		    MethodCloneInstance temp = new MethodCloneInstance(mccId, coverage, method,fid);
		    MethodClones temp2 = new MethodClones(mccId, averageTokenCount, averagePercentageCount,instances, SCCIDs);
		    for (int i = 0; i < mccList.size(); i++)
		    {
			if (mccList.get(i).getClusterID() == mccId)
			{
			    mccList.get(i).addMCCInstance(temp);
			    added = true;
			}
		    }
		    if (!added)
		    {
			temp2.addMCCInstance(temp);
			mccList.add(temp2);
		    }
		}
	    }
	} catch (SQLException e1)
	{
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}

    }
    
    public  ArrayList<MethodClones> getMethodClones(){
    	return mccList;
    	
    }
    
    
    
    public ArrayList<sMethod> getMethods()
	{
		return methodList;
	}
    
    public void ClonesByMethodParser_loadMethodList()
    {	
	try
	{
	    Statement stmt =CloneRepository.getConnection().createStatement();
	    stmt.execute("use "+ CloneRepository.getDBName()+";");
	    ResultSet rs = stmt.executeQuery("select * from scc_method;");

	    while (rs.next())
	    {
	    	getMethods().get(rs.getInt(2)).addCloneClass(rs.getInt(1));
	    }
	} catch (SQLException e1)
	{
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	}
    	
    }
    
    
    public static void updateMETHODS(ArrayList<sMethod> methods) throws Exception
    {
	for (int i = 0; i < methods.size(); i++)
	{
	    int fnum = methods.get(i).getFileID();
	    String fname = ProjectInfo.getFilePath(fnum);
	    int start_line = methods.get(i).getStartToken();
	    int end_line = methods.get(i).getEndToken();
	    String code_set = getCodeSegment(fname, start_line, end_line);
	    methods.get(i).setCodeSegment(code_set);
	}

    }
    
    public static String getCodeSegmentForColoring(String path, int start_line, int start_col, int end_line, int end_col,
    	    SimpleCloneInstance inst) throws IOException
        {
    	// if(inst.getSCCID()==8)
    	// {
    	// int x=0;
    	// }
    	
    	File check = new File(path);
    	FileReader frdr = null;
    	BufferedReader buff = null;
    	String data = "";

    	try {
    	if (check.exists())
    	{
    	    frdr = new FileReader(path);
    	    buff = new BufferedReader(frdr);

    	} else if (!check.exists())
    	{
    	    String path2 = Directories.CHECKFILE;
    	    path2 = Directories.getAbsolutePath(path2);
    	    frdr = new FileReader(path2);
    	    buff = new BufferedReader(frdr);
    	    System.out.println("File not found: " + path2);
    	}
    	int line_number = 0;
    	//try
    	//{
    	    String line = null;
    	    while ((line = buff.readLine()) != null)
    	    {
    		line_number++;
    		//line = line.replace("\t", "        "); // to handle the error occur due to space and \t
    		if (start_line == end_line)
    		{
    		    if (line_number == start_line)
    		    {
    			if (end_col <= line.length() && start_col <= line.length())
    			{
    			    line = line.substring(start_col - 1, end_col);
    			}
    			data += (line + "\n");
    			frdr.close();
    			buff.close();
    			return data;
    		    }
    		} else if (line_number >= start_line && line_number <= end_line)
    		{
    		    if (line_number == start_line)
    		    {
    			if (start_col - 1 <= line.length())
    			{
    			    // line=line.replace("\t"," "); // to handle the error occur due to space and \t
    			    line = line.substring(start_col - 1);
    			}

    		    } else if (line_number == end_line)
    		    {
    			if (inst != null)
    			{
    			    if (end_col <= line.length())
    			    {
    				String temp = line.substring(0, end_col);
    				line = temp;
    			    }
    			} else
    			{
    			    if (end_col < 2)
    			    {
    				line = "";
    			    } else
    			    {
    			    	System.out.println("LINE IS "+ line);
    			    	line = line.substring(0,end_col); 
    			    }
    			}
    		    }
    		    data += (line + "\n");
    		}
    	    }
    	} catch (Exception e)
    	{
    	    e.printStackTrace();
    	} finally
    	{
    	    frdr.close();
    	    buff.close();

    	}
    	return data;
        }
    
    public static String getCodeSegment(String path, int start_line, int start_col, int end_line, int end_col,
    	    SimpleCloneInstance inst) throws IOException
        {
    	// if(inst.getSCCID()==8)
    	// {
    	// int x=0;
    	// }
    	
    	File check = new File(path);
    	FileReader frdr = null;
    	BufferedReader buff = null;
    	String data = "";

    	try {
    	if (check.exists())
    	{
    	    frdr = new FileReader(path);
    	    buff = new BufferedReader(frdr);

    	} else if (!check.exists())
    	{
    	    String path2 = Directories.CHECKFILE;
    	    path2 = Directories.getAbsolutePath(path2);
    	    frdr = new FileReader(path2);
    	    buff = new BufferedReader(frdr);
    	    System.out.println("File not found: " + path2);
    	}
    	int line_number = 0;
    	//try
    	//{
    	    String line = null;
    	    while ((line = buff.readLine()) != null)
    	    {
    		line_number++;
    		line = line.replace("\t", "        "); // to handle the error occur due to space and \t
    		if (start_line == end_line)
    		{
    		    if (line_number == start_line)
    		    {
    			if (end_col <= line.length() && start_col <= line.length())
    			{
    			    line = line.substring(start_col - 1, end_col);
    			}
    			data += (line + "\n");
    			frdr.close();
    			buff.close();
    			return data;
    		    }
    		} else if (line_number >= start_line && line_number <= end_line)
    		{
    		    if (line_number == start_line)
    		    {
    			if (start_col - 1 <= line.length())
    			{
    			    // line=line.replace("\t"," "); // to handle the error occur due to space and \t
    			    line = line.substring(start_col - 1);
    			}

    		    } else if (line_number == end_line)
    		    {
    			if (inst != null)
    			{
    			    if (end_col <= line.length())
    			    {
    				String temp = line.substring(0, end_col);
    				line = temp;
    			    }
    			} else
    			{
    			    if (end_col < 2)
    			    {
    				line = "";
    			    } else
    			    {
    			    	System.out.println("LINE IS "+ line);
    			    	if(line.length() > end_col)
    			    	{
    			    		line = line.substring(0, end_col);
    			    	}
    			    }
    			}
    		    }
    		    data += (line + "\n");
    		}
    	    }
    	} catch (Exception e)
    	{
    	    e.printStackTrace();
    	} finally
    	{
    	    frdr.close();
    	    buff.close();

    	}
    	return data;
        }

    
    public static String getCodeSegment(String path, int start_line, int end_line) throws Exception
    {
	// TODO Auto-generated method stub
	FileReader frdr =null;
	BufferedReader reader = null;
	 frdr = new FileReader(path);
	 reader = new BufferedReader(frdr);
	
	String data = "";
 
//	reader = new BufferedReader(frdr);

	String line;
	int linenumber = 0;
	while ((line = reader.readLine()) != null)
	{
	    if (linenumber >= start_line - 1 && linenumber <= end_line)
	    {
		data += line + "\n";
	    }
	    linenumber++;
	}
	frdr.close();
	reader.close();
	return data;
    }

	public static ArrayList<sMethod> getFilteredMethods() {
		// TODO Auto-generated method stub
		return filteredMethodList;
	}

	
	

	public static ArrayList<MethodClones> getFilteredMethodClones() {
		// TODO Auto-generated method stub
		return filteredMCCList;
	}
 
	
    
    

    }