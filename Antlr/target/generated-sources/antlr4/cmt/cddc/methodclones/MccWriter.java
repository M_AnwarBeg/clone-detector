package cmt.cddc.methodclones;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cmt.cddc.common.CommonUtility;
import cmt.cddc.fileclones.StructuralClones_FileClusters;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod_List;
import cmt.common.Directories;

public final class MccWriter 
{
	public static void writeMethodClusters(ArrayList<MethodClones> methodClusters , boolean prunedClusters)
	{
		if(prunedClusters)
			writeMethodClusters(Directories.getAbsolutePath(Directories.METHOD_CLUSTER_XX), true , methodClusters);
		else			
			writeMethodClusters(Directories.getAbsolutePath(Directories.METHOD_CLUSTER), false , methodClusters);		
	}
	
	private static void writeMethodClusters(String path , boolean bSignificantOnly , ArrayList<MethodClones> methodClusters)
	{

		PrintWriter writer;
		try 
		   {
			   
			   writer = new PrintWriter(path, "UTF-8");
			   if(methodClusters != null && methodClusters.size() > 0)
				{
				   for(MethodClones cluster : methodClusters)
				   {
					   if(bSignificantOnly && !cluster.isbSignificant())
						   continue;
					   
					   writer.println(cluster.getClusterID() + ";" + cluster.getvMethodsSize());
					   
					   for(int cc: cluster.getvCloneClasses())
						   writer.print(cc/100 + ",");
					   				
					   writer.println();
					   
					   for(int i=0 ; i< cluster.getvMethodsSize() ; i++)
					   {
						   writer.print(cluster.getvMethodAt(i) + ";" + cluster.getvMethodTokensAt(i) + ",");
						   writer.printf("%.2f", cluster.getvMethodCoverageAt(i));
						   writer.println();
					   }
					   writer.println();
				   }
				}
			   writer.close();
		   }
		   catch (FileNotFoundException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 
		   catch (UnsupportedEncodingException e) 
		   {
			   // TODO Auto-generated catch block
			   e.printStackTrace();
		   } 	   
	}

	public static void writeMethodClonesByFiles()
	{
	   PrintWriter writer_file;
  	   PrintWriter writer_normalfile;
  	   try 
	   {
		   writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.METHOD_CLONES_BY_FILE), "UTF-8");
		   writer_normalfile = new PrintWriter(Directories.getAbsolutePath(Directories.METHOD_CLONES_BY_FILE_NORMAL) , "UTF-8");
		   
		   for(sFile file : sFile_List.getFilesList())
		   {
			   for(int i = 0 ; i < file.getvCloneMethodClassSize() ; i++)
			   {
				   writer_file.print(file.getvCloneMethodClassAt(i)/100 + ",");
				   writer_normalfile.print(file.getvCloneMethodClassAt(i) + " ");
			   }
			   writer_file.println();
			   writer_normalfile.println();
		   }
		   writer_file.close();
		   writer_normalfile.close();
	   }
  	   catch (FileNotFoundException e) 
	   {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   } 
	   catch (UnsupportedEncodingException e) 
	   {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
	   }  
  	   
	}
		
	public static void writeCrossFileCloneMethodStructures(ArrayList<MethodClones> methodClusters)
	{
		PrintWriter writer_file;
		try 
		{
			writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.CROSS_FILE_METHOD_STRUCTURE_EX), "UTF-8");						
			ArrayList<String> mcsAcrossFiles = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_FILE_METHOD_STRUCTURE));
			//for every structure
		    for (int i = 0, validMCS = 0; i < mcsAcrossFiles.size(); i++) 
		    {
		    	String [] line_parts = mcsAcrossFiles.get(i).split("#");
				if(line_parts.length > 1)
				{
					String [] temp = line_parts[0].split(" ");
					if(temp.length <= 0)
						continue;
					
					ArrayList<String> mcc_temp = new ArrayList<String>();
					Collections.addAll(mcc_temp, temp);
					
 					String [] support_temp = line_parts[1].split(" ");
					int support = Integer.parseInt(support_temp[1]);
					
					if(support <= 1)
					   continue;
					
					//sort the list because the result is not ordered
					ArrayList<Integer> vMethodCloneClasses = CommonUtility.convertStrArrListToInt(mcc_temp , false);
					Collections.sort(vMethodCloneClasses);
					
					//take the first method clone class
			        int MCCId = vMethodCloneClasses.get(0) / 100;	
			        
			        //find all files that have this clone method class
			        //use list for ease of deletion in middle			          
			        List<Integer> iTempFiles = new ArrayList<Integer>();
			        for (MethodClones cluster : methodClusters) 
			        {
			            if (cluster.getClusterID() == MCCId) 
			            {
			                for (int j = 0; j < cluster.getvMethodsSize(); j++) 
			                {
			                    int methodID = cluster.getvMethodAt(j);
			                    int fileID = sMethod_List.getsMethodAt(methodID).getFileID();
			                    iTempFiles.add(fileID);
			                }
			                break;
			            }
			        }			        
			       //remove adjacent same file IDs
			       Set<Integer> hs = new HashSet<Integer>(iTempFiles);			       
			       iTempFiles.clear();
			       iTempFiles.addAll(hs);			        
			       //sort the files 
			       Collections.sort(iTempFiles);	
			       
			       //for each file in temporary list
		           for (int id = 0 ; id < iTempFiles.size() ; id++) 
		           {
		              int fileID = iTempFiles.get(id);			                

		              //if the file does not have all members of this structure
		              //remove this file from temp list
		              if(!(sFile_List.getsFileAt(fileID).getvCloneMethodClasses().containsAll(vMethodCloneClasses)))
	                  {
		                 	iTempFiles.remove(id);
		                	fileID--;
		              }			                			               
		           }    

			        /*if(!satisfyGroupCheck(iTempFiles))
			        	continue;*/	
			        
			        writer_file.print(validMCS++ + " ");
			        writer_file.println(support);
			        
			        for (int j = 0; j < vMethodCloneClasses.size(); j++) {
			        	writer_file.print((vMethodCloneClasses.get(j)/100) + ",");
			        }
			        
			        writer_file.println();
			        
			        for (int id = 0 ; id < iTempFiles.size() ; id++) 
			        {
			            int fileID = iTempFiles.get(id);
			            writer_file.println();
			            writer_file.println(fileID);
			            
			            //for each method clone class in this structure
			            for (int n = 0; n < vMethodCloneClasses.size(); n++) 
			            {
			                //if it is the same method clone class...skip
			                if (n >= 1 && vMethodCloneClasses.get(n) / 100 == vMethodCloneClasses.get(n - 1) / 100) 
			                    continue;
			                   
			                int MCC = vMethodCloneClasses.get(n) / 100;			                
			                //find the clone method IDs
			                for (MethodClones cluster : methodClusters) 
			                {
			                    if (cluster.getClusterID() == MCC) 
			                    {
			                    	for (int j = 0; j < cluster.getvMethodsSize(); j++) 
			                    	{
			                            int methodID = cluster.getvMethodAt(j);
			                            if (sMethod_List.getsMethodAt(methodID).getFileID() == fileID){
			                            	writer_file.print(methodID + ",");
			                            }
			                        }
			                    }
			                }
			                writer_file.println();
			            }
			        }
			        writer_file.println("***********************************************");
				}
		    }
		    writer_file.close();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static boolean satisfyGroupCheck(List<Integer> iTempFiles)
    {
    	 boolean bSatisfyGroupCheck = false;
         int fileID = iTempFiles.get(0);
         int firstFileGroup = sFile_List.getsFileAt(fileID).getGroupID();
         for (int i = 0 ; i < iTempFiles.size() ; i++) 
         {
             fileID = iTempFiles.get(i);
             int curFileGroup = sFile_List.getsFileAt(fileID).getGroupID();
             if (firstFileGroup != curFileGroup) 
             {
                 bSatisfyGroupCheck = true;
             }
         }                
		return bSatisfyGroupCheck;    	
    }
	
    public static void writeMethodStructures(ArrayList<SimpleClone> simpleClones)
    {
    	PrintWriter writer_file;
    	try 
		{
	    		int structureCount=0;
		  		writer_file = new PrintWriter(Directories.getAbsolutePath(Directories.METHOD_STRUCTURE), "UTF-8");
		  		ArrayList<ArrayList<Integer>> crossPatterns = new ArrayList<ArrayList<Integer>>();
		  		ArrayList<String> crossMethodStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CROSS_METHOD_CLONE_STURCTURES));
		  		ArrayList<String> clonesByMethod = CommonUtility.readFile(Directories.getAbsolutePath(Directories.CLONESBYMETHODFILENORMAL));
		  		ArrayList<String> inMethodStructures = CommonUtility.readFile(Directories.getAbsolutePath(Directories.IN_METHOD_STRUCTURE));
				if(crossMethodStructures != null && crossMethodStructures.size() > 0)
				{
					//for every structure
					for(int i = 0 ; i < crossMethodStructures.size() ; i++)
					{
				        String [] line_parts = crossMethodStructures.get(i).split("#");
						if(line_parts.length <= 1)
							continue;
	
						String [] support_temp = line_parts[1].split(" ");
						int support = Integer.parseInt(support_temp[1]);  //the support is the count of occurrences
						
						if(support <= 1)
							continue;
						
						String [] temp = line_parts[0].split(" ");
						if(temp.length <= 0)
							continue;	
						
						ArrayList<String> crossMethodPattern_temp = new ArrayList<String>();
						Collections.addAll(crossMethodPattern_temp, temp);	
						ArrayList<Integer> crossMethodPattern = CommonUtility.convertStrArrListToInt(crossMethodPattern_temp , true);
						Collections.sort(crossMethodPattern);
						crossPatterns.add(crossMethodPattern);
						ArrayList<Integer> methodsContainingCrossMethodPattern = new ArrayList<Integer>();
						
						for(int j =0 ; j < clonesByMethod.size() ; j++)
						{
							String [] clonesList = clonesByMethod.get(j).split(" ");
							ArrayList<String> clonesList_temp = new ArrayList<String>();
							Collections.addAll(clonesList_temp, clonesList);	
							ArrayList<Integer> clonesByMethodList = CommonUtility.convertStrArrListToInt(clonesList_temp , true);
							boolean isFirstMatch = true;
							
							while(true)
							{		
								boolean patternMatched = true;
								for(int z=0 ; z < crossMethodPattern.size() ; z++)
								{					
									int element = crossMethodPattern.get(z);
									if(Collections.frequency(crossMethodPattern, element) > Collections.frequency(clonesByMethodList , element))
									{
										patternMatched = false;	
										break;
									}
								}
								
								if(patternMatched)
								{
									if(isFirstMatch)
										isFirstMatch=false;
									else
										support++;
									
									methodsContainingCrossMethodPattern.add(j);
									for(int k =0 ; k < crossMethodPattern.size() ; k++)
										clonesByMethodList.remove(crossMethodPattern.get(k));
								}
								else
									break;
							}
						}
						writer_file.println(structureCount + " " + support);
					    
						methodStructWriter_HelperMethod(writer_file , crossMethodPattern , methodsContainingCrossMethodPattern,simpleClones);
						writer_file.println();
						structureCount++;
					}										
			    }
			    for(int x=0 ; x < inMethodStructures.size() ; x++)
				{
					String [] inMethodStruc = inMethodStructures.get(x).split("\\(");
					for(int y=0;y < inMethodStruc.length ; y++)
					{
						boolean repeatingPattern = false;
						if(inMethodStruc[y].isEmpty() == false && inMethodStruc[y] != "" && inMethodStruc[y] != null)
						{
							String [] p = inMethodStruc[y].split("\\)");
							int totalOccurenceCount = Integer.parseInt(p[0]);
							String [] temp = p[1].split(",");
							ArrayList<String> inMethodPattern_temp = new ArrayList<String>();
							Collections.addAll(inMethodPattern_temp, temp);	
							ArrayList<Integer> inMethodPattern = CommonUtility.convertStrArrListToInt(inMethodPattern_temp , false);
							
							for(int c = 0 ;c < crossPatterns.size() ; c++)
							{								
								if(crossPatterns.get(c).equals(inMethodPattern))
								{
									repeatingPattern= true;
									break;
								}
							}
							if(!repeatingPattern)
							{
								ArrayList<Integer> methodsContainingInMethodPattern = new ArrayList<Integer>();
								for(int c=0; c < totalOccurenceCount ; c++)
									methodsContainingInMethodPattern.add(x);
								
								writer_file.println(structureCount + " " + totalOccurenceCount);							
								methodStructWriter_HelperMethod(writer_file ,inMethodPattern, methodsContainingInMethodPattern,simpleClones);
								writer_file.println();
								structureCount++;
							}
						}
					}				
				}			
	  		    writer_file.close();		
		}
	 	catch (FileNotFoundException e) 
		{
			 // TODO Auto-generated catch block
	        e.printStackTrace();
		} 
		catch (UnsupportedEncodingException e) 
		{
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}  
    }

	private static void methodStructWriter_HelperMethod(PrintWriter writer_file, ArrayList<Integer> pattern, ArrayList<Integer> methodsContainingPattern, ArrayList<SimpleClone> simpleClones) 
	{
		for(int z = 0 ; z < pattern.size() ; z++)
			writer_file.print(pattern.get(z) + ",");
		writer_file.println();
		
		Collections.sort(methodsContainingPattern);
		
		int repeatingMethodCount = 1;
		for(int m = 0 ; m< methodsContainingPattern.size() ; m++)
		{
			int methodID = methodsContainingPattern.get(m);
			
			if(m != methodsContainingPattern.size() - 1  && methodID == methodsContainingPattern.get(m+1))
			{
				repeatingMethodCount++;				
				continue;
			}
			else if(m == methodsContainingPattern.size() -1 || methodID != methodsContainingPattern.get(m+1))
			{				
					writer_file.print(methodID + ":");		
			}
			
			for(int id=0 ; id< pattern.size() ; id++)
			{
				writer_file.print(" [");
				int cloneId=pattern.get(id);
				
				SimpleClone clone = simpleClones.get(cloneId);
				for(int instanceID = 0 ;  instanceID < clone.getInstancesSize() ; instanceID++)
				{
					SimpleCloneInstance instance = clone.getInstanceAt(instanceID);
					
					if(instance.getMethodId() == methodID)
						writer_file.print(cloneId + "." + instanceID + ",");
				}
				writer_file.print("] ,");				
			}

			sMarkedMethod sMM = new sMarkedMethod();
            sMM.setMethodID(methodID);
            int iCover = StructuralClones_MethodClusters.findMethodCoverage(sMM ,pattern, simpleClones , false);
            int iMethodTokenCount = sMethod_List.getsMethodAt(methodID).getEndToken() - sMethod_List.getsMethodAt(methodID).getStartToken() + 1;
            float totalCovPercentage = ((float) iCover / (float) iMethodTokenCount)* 100;
	        writer_file.print("("+ iMethodTokenCount + "," );
	        writer_file.printf("%.2f", totalCovPercentage); 
	        writer_file.print("%" +")" + "(" + repeatingMethodCount + ")");
	        writer_file.println();
	        repeatingMethodCount = 1;
	   }		
	}
}
