package cmt.cddc.methodclones;

import java.util.Arrays;

public class sMarkedMethod
{
	private int methodID;
	private Boolean[] vBMarkedTokens;
	
	public sMarkedMethod()
	{
		this.methodID = -1;
		this.vBMarkedTokens = null;
	}
	
	public Boolean[] getvBMarkedTokens() {
		return vBMarkedTokens;
	}
	public void setvBMarkedTokens(Boolean[] vBMarkedTokens) {
		this.vBMarkedTokens = vBMarkedTokens;
	}
	public void setvBMarkedTokensToFalse(Boolean[] vBMarkedTokens) {
		this.vBMarkedTokens = vBMarkedTokens;
		Arrays.fill(this.vBMarkedTokens, Boolean.FALSE);
	}
	public boolean getvBMarkedTokenAt(int index)
	{
		return this.vBMarkedTokens[index];
	}
	public void setvBMarkedTokenAt(int index , boolean val)
	{
		this.vBMarkedTokens[index] = val;
	}
	public int getMethodID() {
		return methodID;
	}
	public void setMethodID(int methodID) {
		this.methodID = methodID;
	}
	
}

