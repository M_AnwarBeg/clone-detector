package cmt.cddc.antlrparser;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonedetectioninitializer.BasicOutputWriter;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonedetectioninitializer.VisualizationSettingsActionDelegate;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.fileclones.StructuralClones_FileClusters;
import cmt.cddc.langtokenizer.InitJava;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.StructuralClones_MethodClusters;
import cmt.cddc.simpleclones.CloneDetector_PSY;
import cmt.cddc.simpleclones.CloneExtractor_PSY;
import cmt.cddc.simpleclones.IncrementalCloneHandler;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneWriter;
import cmt.cddc.simpleclones.SimpleCloneWriterDB;
import cmt.cddc.simpleclones.SimpleClonesReader;
import cmt.cddc.simpleclones.TokensList;
import cmt.cddc.simpleclones.Utilities;
import cmt.cddc.structures.MCStruct;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod_List;
import cmt.cfc.incremental.IncrementalObserver;
import cmt.clonedetectionSettingsUI.CloneTrackingFormPage;
import cmt.ctc.incrementalclones.TrackInfo;
import cmt.ctc.incrementalclones.TrackInfos;
import cmt.ctc.mongodb.AllFilesContent;
import cmt.ctc.mongodb.SequenceInfo;
import cmt.ctc.mongodb.Stopwatch;

public class AntlrParser implements IAdapterParser{
        
    /**
     * It will parse the code and will return the lexed String
     * (process of separating a stream of characters into
     * different words, which in computer science we call 
     * 'tokens') 
     */  	
    
    @SuppressWarnings({ "deprecation", "static-access" })
	@Override
    public TokensList InitializeParser (AllFilesContent inputFiles)
    {    
    	 //InitC c = new InitC();
        //InitCSharp cSharp = new InitCSharp();
        //InitPhp p = new InitPhp();
        switch (SysConfig.getLangChoice())
        {
	        case 0:
	        {
	        	InitJava j = new InitJava(); 
	        	j.createLexerArr(inputFiles);
	        	
	            //return j.getTokensList();
	        	
	        	Stopwatch stopwatch=new Stopwatch();
                //ArrayList<CloneInfo> clone=new ArrayList<CloneInfo>();
                ArrayList<SimpleClone> clone=new ArrayList<SimpleClone>();
                ArrayList<String> files=new ArrayList<String>();
                Map<String, ArrayList<SequenceInfo>> fileLU=j.getfilelookup();
                TrackInfos trackinfo = new TrackInfos();
                //int count=-1;
                //int clone_id=0;
                int class_id=0;
             	//ArrayList<Integer> tokentrack = new ArrayList<>();
             	ArrayList<Integer> updatedOrNewlyAddedFileIds = new ArrayList<Integer>();
              
                MongoClient mongo;
				try 
				{
					mongo = new MongoClient("localhost:27017");
					DB db = mongo.getDB("admin");
					db.cleanCursors(true);
					db.getCollection("ByFiles").resetIndexCache(); 
					if(!j.firstrun && CloneTrackingFormPage.isIncremental)
					{
							    //for(int i=0;i<j.getUpdateFiles().size();i++)
						        for(int i=0 ; i < inputFiles.getUpdatedAndNewlyAddedFiles().size() ; i++)
								{
									//String file=j.getUpdateFiles().get(i);
						        	String file = inputFiles.getUpdatedAndNewlyAddedFiles().get(i);
						        	
						        	int fileId = sFile_List.getFileIdAgainstFileName(file);
						        	if((sFile_List.getFile(fileId)).getUpdated() || (sFile_List.getFile(fileId)).isNewlyAddedFile())
						        		updatedOrNewlyAddedFileIds.add(fileId);
									
									ArrayList<SequenceInfo>hashseq=fileLU.get(file);
									if(hashseq != null)
									{
										for(int k=0;k<hashseq.size();k++)
										{
												BasicDBObject whereQuery = new BasicDBObject();
												DBCollection collection = db.getCollection("ByFiles");        	                  
												List<DBObject> rs = new ArrayList<DBObject> (); 
												whereQuery.put("sequencehash", hashseq.get(k).sequencehash);
												DBCursor cursor = collection.find(whereQuery);
												while (cursor.hasNext()) 
											    {
													rs.add(cursor.next());
											 	}
												if(trackinfo.getTrackInfosSize() > 0 && trackinfo.getTrackAt(trackinfo.getTrackInfosSize()-1).getTrackInstancesSize() > 1)
												{
													IncrementalCloneHandler.compareCloneFiles(trackinfo, rs);
													for(int t=0;t<trackinfo.getTrackInfosSize();t++)
													{														
														if(!trackinfo.getTrackAt(t).isMatchFlag())
														{
																clone.add(new SimpleClone(class_id,trackinfo.getTrackAt(t).getTokentrack()));
																IncrementalCloneHandler.cloneDetected(clone, trackinfo.getTrackAt(t).getTrackinfos(), class_id);
															
																for(TrackInfo s:trackinfo.getTrackAt(t).getTrackinfos())
																{
																	if(!files.contains(s.filename))
																	{
																		files.add(s.filename);
																	}
																}
																class_id++;
																trackinfo.removeTrackAt(t);
																t--;
														}															
														else if(trackinfo.getTrackAt(t).isMatchFlag())
														{
															for(int n=0; n<trackinfo.getTrackAt(t).getTrackInstancesSize(); n++)
															{																
																	for(int m=0;m<rs.size();m++)
																	{
																		DBObject DataObj = rs.get(m);
																		if(DataObj.get("filename").equals(trackinfo.getTrackAt(t).getTrackInfoAt(n).filename) && 
																				((Integer)(DataObj.get("sequenceindex")))==(trackinfo.getTrackAt(t).getTrackInfoAt(n).seqindex+1))
																		{
																			trackinfo.getTrackAt(t).getTrackinfos().set(n, new TrackInfo(trackinfo.getTrackAt(t).getTrackInfoAt(n).filename, trackinfo.getTrackAt(t).getTrackInfoAt(n).startsequenceindex,trackinfo.getTrackAt(t).getTrackInfoAt(n).stopsequenceindex+1, trackinfo.getTrackAt(t).getTrackInfoAt(n).startline, (Integer)DataObj.get("stopline"), trackinfo.getTrackAt(t).getTrackInfoAt(n).startindex, (Integer)DataObj.get("stopindex"),trackinfo.getTrackAt(t).getTrackInfoAt(n).method_id,trackinfo.getTrackAt(t).getTrackInfoAt(n).seqindex+1 , trackinfo.getTrackAt(t).getTrackInfoAt(n).file_id));
																		}
																	}
															} 
		             							
															if(trackinfo.getTrackAt(t).getTokenTrackSize() < 1)// tokentrack.isEmpty())
															{
																trackinfo.getTrackAt(t).setTokentrack(hashseq.get(k).token);
																//tokentrack=hashseq.get(k).token;
															}
				 											else
															{
																trackinfo.getTrackAt(t).addTokenTrack(hashseq.get(k).token.get(SysConfig.getThreshold()-1));//31));
																//tokentrack.add(hashseq.get(k).token.get(31));
															}
														}													
													}
													if(trackinfo.getTrackInfosSize() > 0 && trackinfo.getTrackAt(trackinfo.getTrackInfosSize()-1).getTrackInstancesSize() != rs.size() && rs.size()>1) 
													{
														IncrementalCloneHandler.updateTrackInfo(trackinfo, rs);
													}             					
												}
												if(trackinfo.getTrackInfosSize() < 1 && rs.size() > 1)
												{
													IncrementalCloneHandler.updateTrackInfo(trackinfo, rs);
													IncrementalCloneHandler.maintainOldTracks(rs);
												}
									}
								}
								if(trackinfo.getTrackInfosSize() > 0 && trackinfo.getTrackAt(0).getTrackInstancesSize() > 1)
								{
									for(int g=0;g<trackinfo.getTrackInfosSize();g++)
									{										
										clone.add(new SimpleClone(class_id,trackinfo.getTrackAt(g).getTokentrack()));
										IncrementalCloneHandler.cloneDetected(clone, trackinfo.getTrackAt(g).getTrackinfos(), class_id);
										for(TrackInfo s:trackinfo.getTrackAt(g).getTrackinfos())
										{
											if(!files.contains(s.filename))
											{
												files.add(s.filename);
											}
										}
										class_id++;
										trackinfo.getTrackInfos().clear();
									}
								}
							} 
							TokensList token_list =j.getTokensList();
							token_list = Utilities.markRepetitions(token_list);// markRepetitions(token_list);
							IncrementalCloneHandler.pruneDuplicateClones(clone);							
							
							//Removing deleted files clones...
							if(CloneTrackingFormPage.getDeletedFileID() != null && CloneTrackingFormPage.getDeletedFileID().length > 0)
							{
								ArrayList<Integer> deletedFileList = (ArrayList<Integer>) Arrays.stream(CloneTrackingFormPage.getDeletedFileID()).boxed().collect(Collectors.toList());
								SimpleCloneWriterDB.removeClones(deletedFileList , true);
							}
							
							ArrayList<SimpleClone> finalClones = SimpleCloneWriterDB.removeDuplicateClones(updatedOrNewlyAddedFileIds , clone);
							IncrementalCloneHandler.reorderSimpleClonesInstances(finalClones, updatedOrNewlyAddedFileIds);
							CloneExtractor_PSY.checkForRuns(finalClones, token_list);
							//ArrayList<SimpleClone> clones_temp = IncrementalCloneHandler.extractClonesConsistingOfUpdatedOrNewlyAddedFilesOnly(finalClones, updatedOrNewlyAddedFileIds);
							/*finalClones.addAll(*/Utilities.checkIfDetectedCloneIsARepetition(finalClones, token_list , true , updatedOrNewlyAddedFileIds);							
							(new CloneDetector_PSY()).updateTokenListWithMethodIds(token_list);
							//save final clones in sql db...
							
							SimpleCloneWriter.writeInMethodStructures();
							IncrementalCloneHandler.saveMethodList(sMethod_List.getMethodsList());
							
							
							
							
							//ArrayList<SimpleClone> clon=checkIfDetectedCloneIsARepetition(clone,token_list);
							//---------Temporary commented -> Increment cloneDetect = new Increment(); 
							SimpleCloneWriter.writeSimpleCloneFiles(finalClones,token_list ,true);
							//BasicOutputWriter.writeBasicOutputFiles(token_list);
							BasicOutputWriter.createCombinedTokens_File(token_list);
							IncrementalCloneHandler.saveSimpleClones(finalClones);
							SimpleCloneWriterDB.InitQuery();
							SimpleCloneWriterDB.populateDatabase(0 , true);
							
							
							
							//Adil
							ArrayList<Integer> updatedOrNewlyAddedFileMethods = IncrementalCloneHandler.MethodsFromFiles(updatedOrNewlyAddedFileIds);
							Stopwatch stoper = new Stopwatch();
							MCStruct methodToCloneStrucuture = new MCStruct();
							methodToCloneStrucuture.FillStructureIncremental(SimpleClonesReader.ReadIncrementalMCCDetectionData(),SimpleClonesReader.ReadIncrementalMCCClusterData(), finalClones);
							methodToCloneStrucuture.updateMethodsWithSimpleClones(finalClones);
							methodToCloneStrucuture.RemoveMCClonesofMethods(updatedOrNewlyAddedFileMethods);
							methodToCloneStrucuture.FillStructure(sMethod_List.getMethodsList());
							SimpleCloneWriter.CreateIncrementalMethodFiles(methodToCloneStrucuture, updatedOrNewlyAddedFileMethods);
							ArrayList<MethodClones> methodClusters = StructuralClones_MethodClusters.detectMethodClustersIncremental(finalClones,methodToCloneStrucuture);
							StructuralClones_MethodClusters.pruneMethodClusters(methodClusters, true);
							IncrementalCloneHandler.WriteUpdatedMethodstoDB(updatedOrNewlyAddedFileIds,updatedOrNewlyAddedFileMethods);
							ArrayList<Integer> _mccs = IncrementalCloneHandler.WriteIncrementalMCCStructure(methodClusters,updatedOrNewlyAddedFileIds,finalClones.size(),methodToCloneStrucuture);
							//wirte MCC to MySQL db//
							System.out.println("Time Taken to Compute Incremental MCC detection and DB insertion : "+stoper.elapsedTime());

							//newSimpleClonesDetected all newly detected clone ids//updatedOrNewlyAddedFileIds//

							HashMap<Integer,ArrayList<Integer>> result = SimpleClonesReader.ReadSccAgainstFiles(finalClones,updatedOrNewlyAddedFileIds);
							HashMap<Integer,Integer> unModifiedFileCoverage = SimpleClonesReader.ReadCoverageForUnModifiedFiles(updatedOrNewlyAddedFileIds);
							//sccs against fileids use to create a file for fcim input//
							ArrayList<Integer> _fccs = new ArrayList<>();
							if(updatedOrNewlyAddedFileIds.size() > 0)
							{
								ArrayList<FileCluster> fileClusters = StructuralClones_FileClusters.detectFileClustersInc(result,finalClones,unModifiedFileCoverage);
								StructuralClones_FileClusters.pruneFileClusters(fileClusters);
								_fccs = IncrementalCloneHandler.WriteIncrementalFCCStructure(fileClusters,updatedOrNewlyAddedFileIds);
							}
							//Adil
							
							if(_mccs.size() >0 || _fccs.size() > 0)
							{
								VisualizationSettingsActionDelegate.reader = new ClonesReader();
								VisualizationSettingsActionDelegate.reader.readClones();
								IncrementalObserver.Notify(_mccs, _fccs);
							}
							
							
							
							
							//cloneDetect.updateMethodsWithSimpleClones(clone);
							
			   			    //-------------------------------------- METHOD CLUSTERS / MCC ---------------------------------------------//
							//---------Temporary commented -> ArrayList<MethodClones> methodClusters = StructuralClones_MethodClusters.detectMethodClusters(clone);
							
							/*
							MccWriter.writeMethodClusters(methodClusters , false);
							   
							StructuralClones_MethodClusters.pruneMethodClusters(methodClusters);
							MccWriter.writeMethodClusters(methodClusters , true);  //writing pruned method clusters...
							   
							StructuralClones_MethodClusters.updateFilesWithMethodClones(methodClusters);
							MccWriter.writeMethodClonesByFiles();
							   
							StructuralClones_MethodClusters.findCrossFileCloneMethodStructures();
							MccWriter.writeCrossFileCloneMethodStructures(methodClusters);
							   
							*/
							return token_list;
					}
                } 
				catch (UnknownHostException e) 
				{
                		// TODO Auto-generated catch block
                		e.printStackTrace();
                }
				try 
				{
					IncrementalCloneHandler.saveMethodList(sMethod_List.getMethodsList());
				} 
				catch (UnknownHostException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("total time taken for comparison"+stopwatch.elapsedTime());
                return j.getTokensList();            			
	        }
            case 1:
              //  p.createLexerArr(input); 
              //  return p.getArr();
            case 2:
               // cSharp.createLexerArr(input);
                // return cSharp.getArr();
            case 3:
                //c.createLexerArr(input);
                //return c.getArr();            
            default:
                return null;
        }      
    }    
}
