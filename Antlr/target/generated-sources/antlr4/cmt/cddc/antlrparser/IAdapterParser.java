package cmt.cddc.antlrparser;
import java.util.ArrayList;

import cmt.cddc.simpleclones.TokensList;
import cmt.ctc.mongodb.AllFilesContent;

public interface IAdapterParser {
    TokensList InitializeParser(AllFilesContent inputFiles);
}
