package cmt.cddc.CloneRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;

import cmt.cddc.fileclones.FileCloneWriterDB;
import cmt.cddc.methodclones.MethodCloneWriterDB;
import cmt.cddc.simpleclones.SimpleCloneWriterDB;
import cmt.cfc.mccframe.CliqueSelectionWizard;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
//import com.mysql.jdbc.Statement;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.clonerunmanager.LoadList;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileCloneWriterDB;
import cmt.cddc.methodclones.MethodCloneWriterDB;
import cmt.cddc.simpleclones.SimpleCloneWriterDB;
import cmt.common.Directories;

public class CloneRepository {
    
	public static Connection conn;
	private static final String FILENAME = Directories.getAbsolutePath(Directories.DATABASE);
	static String[] fileContent = {""};
	static String[] fileContent2 = {""};
	static String dbName= "";
	static String systemTime= "";
	private static int frameId=-1;
    
	public static void incFrameId()
	{
		frameId++;
	}
	public static int getFrameId()
	{
		try
			{
				openConnection();
				createFrameRepo();
				Statement stmt = conn.createStatement();
		    	stmt.executeQuery("use framerepository;");
		    	ResultSet rs=stmt.executeQuery("select MAX(fid) from frames;");
		    	
		    	if(rs.next())
		    	{
		    		frameId=rs.getInt(1);
		    	}
		    	else
		    	{
		    		frameId=0;
		    	}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		return frameId;
	}
	public static void createSchema() {
		try {
			   String str = FileUtils.readFileToString(new File(FILENAME));
			   
			   fileContent = str.split(";");
			   /*for (int i =0; i<fileContent.length;i++) {
				   System.out.println(fileContent[i]);
				   
			   }*/
			 } catch (IOException e) {
			    e.printStackTrace();
			}
		
	}
	public static void setDBName() {
		systemTime = '_'+SysConfig.getSystemTime();
		dbName = SysConfig.getProjectName()+'_'+"CloneRepository"+systemTime;
	}
	public static void setDBName(String name) {
		dbName = name;
	}
	public static String getDBName() {
		return dbName;
	}
	public static void dropDatabase(String name) {
		try {
			Statement stmt = conn.createStatement();
			String sql = "Drop Database "+name;
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	public static void anotateFrames()
	{
		if(CloneRepository.getDBName().equals(LoadList.framingBaseline))
		{
			try
			{
				openConnection();
				Statement stmt = conn.createStatement();
		    	stmt.executeQuery("use framerepository;");
		    	ResultSet rs=stmt.executeQuery("select mccid from mccframes;");
		    	
		    	while(rs.next())
		    	{
		    		int mccid=rs.getInt(1);
		    		CliqueSelectionWizard.markMethods(mccid);
		    	}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
    	
	}
	public static void createFrameRepo()
	{
		try {
			    String str = FileUtils.readFileToString(new File(Directories.getAbsolutePath(Directories.FRAME_DATABASE)));
			    fileContent2 = str.split(";");
			    
			    for(int i=0;i<fileContent2.length;i++)
			    {
			    	Statement stmt = conn.createStatement();
			    	stmt.execute(fileContent2[i]);
			    }
			    
			    Statement stmt = conn.createStatement();
		    	stmt.executeQuery("use framerepository;");
		    	ResultSet rs=stmt.executeQuery("select MAX(fid) from frames;");
		    	
		    	if(rs.next())
		    	{
		    		frameId=rs.getInt(1);
		    	}
		    	else
		    	{
		    		frameId=0;
		    	}
			 } 
		
		catch (Exception e) 
			{
			    e.printStackTrace();
			}
	}
	public static void getNameofDB() {
		
		try
		{
			
		    java.sql.Statement stmt = conn.createStatement();
		    for (int i =0; i<fileContent.length;i++) {
				   
				   java.util.Date today = new java.util.Date();
				   
				   if(fileContent[i].contains("CREATE DATABASE")) {
					  
					   
					   fileContent[i] = "CREATE DATABASE `"+dbName+"`";
					   
					   
				   }
				   else if(fileContent[i].contains("USE")) {
					  
					  
					   fileContent[i] = "USE `"+dbName+"`;";
					 
					   
					   
				   }
				   else {
					   fileContent[i] += ";";
					   
					  
				   }
				  // System.out.println(fileContent[i]);
				  
				 
				   stmt.executeUpdate(fileContent[i]);
				  
				   
			   }
		    
		} 
	catch (Exception e)
		{
		    System.err.println("Cannot connect to database server" + e.getMessage());
		    e.printStackTrace();
		}
	}
	
    public static void initQuery()
    {
    	SimpleCloneWriterDB.InitQuery();
    	MethodCloneWriterDB.InitQuery();
    	FileCloneWriterDB.InitQuery();
    	ProjectInfo.initQuery();
    	ProjectInfo.getSysConf();
    }
    
    public static void populateDatabase(int dlm)
    {
    	ProjectInfo.populateDatabase(dlm);
    }
    
    public static void clearDatabase()
    {
    	ProjectInfo.clearDatabase();
    }
    
    public static void openConnection()
    {
		conn = null;
		try
			{
				String url = "jdbc:mysql://localhost:3306/";
			    Class.forName("com.mysql.jdbc.Driver").newInstance();
			    conn = DriverManager.getConnection(url, "root", "root");
			    
			} 
		catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException  e)
			{
			    System.err.println("Cannot connect to database server" + e.getMessage());
			    System.err.println("Error Code" + ((SQLException) e).getErrorCode() );
			    
			    e.printStackTrace();
			}
    }
    
    public static void closeConnection()
    {
		if (conn != null)
		{
		    try
			    {
					conn.close(); 
					conn = null; 
			    } 
		    catch (Exception e)
			    { 
		    		/* ignore close errors */
			    }
		}
    }
    public static Connection getConnection()

    {
    	if(conn==null)
    	try {
     	if(conn == null ||conn.isClosed())
    	
    	{
    		openConnection();
    	}
    	}
    	catch(Exception exp) {
    		exp.printStackTrace();
    	}
    	return conn;
    }

    
    public static Statement invokeActiveCloneRepository() {
    	Statement activationStatement = null;
		try {
			activationStatement = CloneRepository.getConnection().createStatement();
			activationStatement.execute("use "+ CloneRepository.getDBName()+";");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return activationStatement;
    }
    
    public static void executeTransaction(String sql)
    {
		try
			{
			    if (sql.indexOf("\"") != -1)
			    {
				sql = sql.substring(0, sql.length() - 1);
			    }
			    sql = sql + ";";
			    Statement st = CloneRepository.getConnection().createStatement();
			    st.execute("use "+ CloneRepository.getDBName()+";");
			    st.execute(sql);
			    st.close();
			} 
		catch (Exception e)
			{
			    System.err.println(e.getMessage());
			    e.printStackTrace();
			}
    }


}
