package cmt.cddc.CloneRepository;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.viewers.IStructuredSelection;

import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileClonesReader;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleClonesReader;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod;
import cmt.cfc.fccframe.FCCAnayser;
import cmt.cfc.mccframe.MCSAnalyzer;
import cmt.cvac.viewobjects.FccList;

public class ClonesReader {
	
	static CloneRepository repository;
	public static Boolean readClonesFlag = false;

	 private static ArrayList<SimpleClone> sccList=new ArrayList<>();
	 
	 private static ArrayList<SimpleClone> filteredSccList=new ArrayList<>();
	 private static ArrayList<sMethod> methodList=new ArrayList<>();
	 private static ArrayList<MethodClones> mccList=new ArrayList<>();
	 private static ArrayList<sMethod> filteredMethodList=new ArrayList<>();
	 private static ArrayList<MethodClones> filteredMccList=new ArrayList<>();
	 private static FccList fccList=new FccList();
	 private static sFile_List fileList = new sFile_List();
	 private static FccList filteredFCCList = new FccList();
	 private static sFile_List filteredFileList = new sFile_List();	 
	 private static ArrayList<FileCluster> fileClones = new ArrayList<FileCluster>();
	 
	 private static ArrayList<MethodCloneStructure> methodCloneStructures = new ArrayList<MethodCloneStructure>();
	 
    // private static ArraList
	 
	 public ClonesReader(){
	 }
	 
	 
	 public static void readClones() {
		 try { 
			 readClonesFlag = true;
		 repository.openConnection();
		 ProjectInfo.loadInputFiles();
		 SimpleClonesReader simpleClonesReader = new SimpleClonesReader();
		 simpleClonesReader.CloneParser_loadMethodList();
		// simpleClonesReader.CombinedTokensParser_loadMethodList();
		 MethodClonesReader methodClonesReader = new MethodClonesReader(); 
		 methodClonesReader.MethodsInfoParser_loadMethodList();
		 methodClonesReader.ClonesByMethodParser_loadMethodList();
		 methodClonesReader.MethodClustersXXParser_loadMethodList();
		 FileClonesReader fileClonesReader = new FileClonesReader();
		 fileClonesReader.fileClustersXXParser_loadMethodList();
		 fileClonesReader.loadFiles();
		 fileClonesReader.loadFileClones();
		 fileClonesReader.loadFcs();
		 fileClonesReader.loadFcsInDirs();
		 fileClonesReader.loadFcsCrossGroup();
		 fileClonesReader.loadFcsInGroup();
		 methodClonesReader.loadMcs();
		 
		 
		 
		 fccList=fileClonesReader.getList();
		 filteredFCCList = fileClonesReader.getFilteredList();
		 sccList = simpleClonesReader.getClones();
		 filteredSccList = simpleClonesReader.getFilteredSimpleClone();
		 
		 mccList = methodClonesReader.getMethodClones();
		 filteredMccList = MethodClonesReader.getFilteredMethodClones();
		 filteredMethodList = MethodClonesReader.getFilteredMethods();
		 methodList = methodClonesReader.getMethods();
		 fileList = fileClonesReader.getFileList();
		 fileClones = fileClonesReader.getFileClones();
		 methodCloneStructures = methodClonesReader.getMethodCloneStructures();

		// MCSAnalyzer analyzer = new MCSAnalyzer();
		// analyzer.analyzeMCS(methodCloneStructures.get(0));
		 populateSCCs();
		
		 updateMETHODS(methodList);
			
		 populateFileCloneSCC();
		// repository.closeConnection(repository.getConnection());

		 
		 repository.closeConnection();
		 }
		 catch(Exception exp) {
			 exp.printStackTrace();
		 }
	 }
	 
	 public static ArrayList<sMethod> getMethods()
	 {
		return methodList;
	  }
	 
	 public static ArrayList<SimpleClone> getSimpleClones()
	 {
		return sccList;
	  }
	 public static ArrayList<SimpleClone> getFilteredSimpleClones(){
		 return filteredSccList;
	 }
	 
	
	 public static ArrayList<MethodClones> getMethodClones()
	 {
		return mccList;
	  }
	 
	 public static ArrayList<MethodClones> getFilteredMethodClones()
	 {
		return filteredMccList;
	  }
	 
	 public static FccList getFileClones()
	 {
		 return fccList;
	 }
	 
	 public static void updateMETHODS(ArrayList<sMethod> mETHODS) throws Exception
	 {
		// TODO Auto-generated method stub
		 for (int i = 0; i < mETHODS.size(); i++)
		 {
		    int fnum = mETHODS.get(i).getFileID();
		    String fname = ProjectInfo.getInputFile(fnum);
		    int start_line = mETHODS.get(i).getStartToken();
		    int end_line = mETHODS.get(i).getEndToken();
		    String code_set = MethodClonesReader.getCodeSegment(fname, start_line, end_line);
		    mETHODS.get(i).setCodeSegment(code_set);
		}
	  }

	 private static SimpleCloneInstance getSCCInst(int sCCID, int fid, int sl, int el)
	 {
	 for (int i = 0; i < sccList.size(); i++)
	 {
	     for (int j = 0; j < sccList.get(i).getSCCInstances().size(); j++)
	     {
	 	int SCCIDin = sccList.get(i).getSSCId();
	 	int fnum = sccList.get(i).getSCCInstances().get(j).getFileId();
	 	int start = sccList.get(i).getSCCInstances().get(j).getStartingIndex();
	 	int end = sccList.get(i).getSCCInstances().get(j).getEndingIndex();
	 	if (sCCID == SCCIDin && fnum == fid && sl <= start && el >= end)
	 	{
	 	    return sccList.get(i).getSCCInstances().get(j);
	 	}
	     }
	 }
	 return null;
	 }
	 
	 private static SimpleCloneInstance getSCCInst(int sCCID,int fid) {
		 
		// System.out.println("Finding SSC ID: "+ sCCID +" " + "Fid: "+ fid);
		 for (int i = 0; i < sccList.size(); i++)
		 {
		    for (int j = 0; j < sccList.get(i).getSCCInstances().size(); j++)
		    {
		 	int SCCIDin = sccList.get(i).getSSCId();
		 	int fnum = sccList.get(i).getSCCInstances().get(j).getFileId();
		 	
		// 	System.out.println("SSC ID: "+ SCCIDin +" " + "Fid: "+ fnum);
						 	
			if (sCCID == SCCIDin && fnum == fid)
			{
				return sccList.get(i).getSCCInstances().get(j);
			}
		    }
		 }
		 return null;
		 
	 }
	 
	 
	  public static void populateSCCs()
	    {
			for (int i = 0; i < mccList.size(); i++)
			{
		
			    for (int j = 0; j < mccList.get(i).getMCCInstances().size(); j++)
			    {
				for (int k = 0; k < mccList.get(i).getvCloneClasses().size(); k++)
				{
				    int SCID = mccList.get(i).getvCloneClasses().get(k);
				    int fnum =mccList.get(i).getMCCInstances().get(j).getMethod().getFileID();
				   mccList.get(i).getMCCInstances().get(j).getMethod().getMethodName();
				    int sl = mccList.get(i).getMCCInstances().get(j).getMethod().getStartToken();
				    int el = mccList.get(i).getMCCInstances().get(j).getMethod().getEndToken();
				    System.out.println(SCID);
				    SimpleCloneInstance add = getSCCInst(SCID, fnum, sl, el);
				    if(add == null) {
				    	getSCCInst(SCID, fnum, sl, el);
				    	System.out.println("NULL");
				    }
				    else {
				    mccList.get(i).getMCCInstances().get(j).addSCCs_Contained(add);
				    }
				    
				    }
			    }
			}
	    }
	  public static  ArrayList<FileCluster> getFileCloneList(){
		  return fileClones;
	  }
	  
	  public static void populateFileCloneSCC()
	  {
		for (int i = 0; i < fileClones.size(); i++)
		{
		    for (int j = 0; j < fileClones.get(i).getFCCInstance().size(); j++)
			{
				for (int k = 0; k < fileClones.get(i).getvCloneClasses().size(); k++)
				{
				    int SCID = fileClones.get(i).getvCloneClasses().get(k);
				    int fnum =fileClones.get(i).getFCCInstance().get(j).getFileId();
				    System.out.println(SCID);
				    SimpleCloneInstance instance =  getSCCInst(SCID, fnum);
				    if(instance == null) {
				    	System.out.println("Could not find SimpleCloneInstance");
				    }
				    else {
				    	fileClones.get(i).getFCCInstance().get(j).addSimpleCloneInstance(instance);
				    }
				    
				 }
			   }
		}
		
		
	//	try {
	//	  FCCAnayser.analyzeFileClone(fileClones.get(0)); 
	//	}
	//	catch(Exception exp) {
			 
	//	}
		
	 }
	  
	  
	  public static MethodCloneStructure getMethodCloneStructure(int mcsId) {
		  
		  Iterator mcsListItr = methodCloneStructures.iterator();
		  while(mcsListItr.hasNext()) {
			  MethodCloneStructure mcs = (MethodCloneStructure) mcsListItr.next();
			  if(mcs.getMcsId() == mcsId) {
				  return methodCloneStructures.get(mcsId);
			  }
			  
		  }
		  return null;
		  
	  }
	  


	public static FccList getFilteredFileClones() {
		// TODO Auto-generated method stub
		return filteredFCCList;
	}


	
	  
	  
	  
	  
	  
	 
}
