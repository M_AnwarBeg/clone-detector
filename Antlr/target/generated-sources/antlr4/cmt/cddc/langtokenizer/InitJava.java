package cmt.cddc.langtokenizer;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.Token;
import org.apache.commons.codec.digest.DigestUtils;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoException;

import antlr.auto.gen.java.*;
import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.simpleclones.Tokens;
import cmt.cddc.simpleclones.TokensList;
import cmt.cddc.structures.sClass;
import cmt.cddc.structures.sClass_List;
import cmt.cddc.structures.sFile;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;
import cmt.clonedetectionSettingsUI.CloneTrackingFormPage;
import cmt.ctc.mongodb.AllFilesContent;
import cmt.ctc.mongodb.FileInfo;
import cmt.ctc.mongodb.SequenceHash;
import cmt.ctc.mongodb.SequenceInfo;
import cmt.ctc.mongodb.Stopwatch;
import cmt.ctc.mongodb.TokenDetails;


enum JAVAMETHODSTATE {
    JMS0(0), //initial state
    JMS1(1), //identifier found
    JMS2(2), //first ( found
    JMS3(3), //formal parameters
    JMS4(4), //closing ) found
    JMS5(5), //throws found
    JMS6(6), //throws clause
    JMS_START(7), //first opening { found
    JMS_BODY(8), //inside method body
    JMS_END(9);//closing } found
    
	private int numVal;

	JAVAMETHODSTATE(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
    }
}


enum JAVAMETHODINPUT {
    IIDENT(0),
    ILPAREN(1),
    IRPAREN(2),
    ITHROWS(3),
    ISEMI(4),
    ILCURLY(5),
    IRCURLY(6),
    IOTHER(7);
	private int numVal;

	JAVAMETHODINPUT(int numVal) {
        this.numVal = numVal;
    }

    public int getNumVal() {
        return numVal;
}
}


enum JAVALISTSTATE {
    JLS_START,
    JLS_BODY,
    JLS_END,
};

class sEquivalentToken {
    int OriginalToken, transformedToken;
    boolean suppress;

    sEquivalentToken()  {
    	OriginalToken=0;
    	transformedToken=0;
    	suppress=false;
    }

    sEquivalentToken(int val1, int val2)  {
    	OriginalToken=val1;
    	transformedToken=val2;
    	suppress=false;
    }

    sEquivalentToken(int val)  {
    	OriginalToken=val;
    	transformedToken=val;
    	suppress=false;
    }
};


public class InitJava
{
	
    //static public ArrayList<sMethod> vMethods = new ArrayList<sMethod>(); 
    //ArrayList<Integer> vTokens = new ArrayList<Integer>(); 
    //ArrayList<sClass> vClass = new ArrayList<sClass>(); 
    //static public ArrayList<sFile> vFiles = new ArrayList<sFile>(); 
	
   /* ArrayList<Integer> vTokenLines= new ArrayList<Integer>() ;
    ArrayList<Integer> vTokenColumns= new ArrayList<Integer>() ;
	//new vectors
    ArrayList<Integer> vTokenFile= new ArrayList<Integer>() ;
    ArrayList<Integer> vTokenClass= new ArrayList<Integer>() ;
    ArrayList<Integer> vTokenMethod= new ArrayList<Integer>() ;
	
    ArrayList<String> vTokenText= new ArrayList<String>();*/
	MongoClient mongo = null;
	TokensList tokensList = new TokensList();
	
	JAVAMETHODSTATE jms = JAVAMETHODSTATE.JMS0;
	JAVAMETHODSTATE jms_next = JAVAMETHODSTATE.JMS0;
	
	static ArrayList<sEquivalentToken> vTokenEquality= new ArrayList<sEquivalentToken>() ;

	private JAVAMETHODSTATE[][] JM_FSM ={
		    /*     IDENT,   LPAREN,   RPAREN,    THROWS,    SEMI,     LCURLY,   RCURLY,    IOTHER
		    /*S0*/
		    {JAVAMETHODSTATE.JMS1, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0},
		    /*S1*/
		    {JAVAMETHODSTATE.JMS1, JAVAMETHODSTATE.JMS2, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0},
		    /*S2*/
		    {JAVAMETHODSTATE.JMS3, JAVAMETHODSTATE.JMS3, JAVAMETHODSTATE.JMS4, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS3},
		    /*S3*/
		    {JAVAMETHODSTATE.JMS3, JAVAMETHODSTATE.JMS3, JAVAMETHODSTATE.JMS4, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS3},
		    /*S4*/
		    {JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS_START, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0},
		    /*S5*/
		    {JAVAMETHODSTATE.JMS6, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0},
		    /*S6*/
		    {JAVAMETHODSTATE.JMS6, JAVAMETHODSTATE.JMS6, JAVAMETHODSTATE.JMS6, JAVAMETHODSTATE.JMS5, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS_START, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS6},
		    /*S7*/
		    {JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_END, JAVAMETHODSTATE.JMS_BODY},
		    /*S8*/
		    {JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY, JAVAMETHODSTATE.JMS_BODY},
		    /*S9*/
		    {JAVAMETHODSTATE.JMS1, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0, JAVAMETHODSTATE.JMS0}
		};

	
	
	

    private JavaLexer lexer;
    //private List<Tokens> arr = new ArrayList<Tokens>();

    JAVAMETHODSTATE GetJMState(final JAVAMETHODSTATE JMS,int Token) {
        JAVAMETHODINPUT jmi;
        
        switch (Token) {
            case JavaLexer.Identifier/*103*/: jmi = JAVAMETHODINPUT.IIDENT;
                break;
            case JavaLexer.LPAREN/*60*/: jmi = JAVAMETHODINPUT.ILPAREN;
                break;
            case JavaLexer.RPAREN /*61*/: jmi = JAVAMETHODINPUT.IRPAREN;
                break;
            case JavaLexer.THROWS /*47*/: jmi = JAVAMETHODINPUT.ITHROWS;
                break;
            case JavaLexer.SEMI /*66*/: jmi = JAVAMETHODINPUT.ISEMI;
                break;
            case JavaLexer.LBRACE /*62*/: jmi = JAVAMETHODINPUT.ILCURLY;
                break;
            case JavaLexer.RBRACE /*63*/: jmi = JAVAMETHODINPUT.IRCURLY;
                break;
            default: jmi = JAVAMETHODINPUT.IOTHER;
        }
        return JM_FSM[JMS.getNumVal()][jmi.getNumVal()];
    }
    

    static int curlyCount_jcs = 0;
    static boolean newClassFound = false;
    static int startToken = 0;
    //find the first class of the file
    static int firstClass=0;
    
    JAVACLASSSTATE DetectJavaClass(Token rT,int nToken,boolean newFile) {
    	JAVACLASSSTATE jcs = JAVACLASSSTATE.JCS_BODY;
        sClass pClass;
       // sClass* pClass = 0;
        if (newFile) curlyCount_jcs = 0;
        //int t = rT->getType();

        if (rT.getType() == JavaLexer.CLASS /*11*/ || rT.getType() == JavaLexer.INTERFACE/*2*/) {
            int nClass = sClass_List.getClassListSize(); //vClass.size();
            //nested classes not supported...
            //previous class must end before next start
            if (nClass > 0) {
                if (((sClass)sClass_List.getsClassAt(nClass - 1)).getEndToken() < 0) return jcs;
            }
            newClassFound = true;
            startToken = nToken;
            jcs = JAVACLASSSTATE.JCS_START;
        }
        else if (rT.getType() == JavaLexer.Identifier/*103*/ && newClassFound) {
            pClass = new sClass();
            pClass.setClassName(rT.getText());
            pClass.setnCurly(curlyCount_jcs);
            pClass.setStartToken(startToken);
            pClass.setEndToken(-1);
            if (newFile) firstClass = sClass_List.getClassListSize();
            sClass_List.addsClass(pClass);
            newClassFound = false;
           jcs = JAVACLASSSTATE.JCS_NAME;
       } else if (rT.getType() == JavaLexer.LBRACE /*62*/) {
            curlyCount_jcs++;
        } else if (rT.getType() == JavaLexer.RBRACE /*63*/) {
            curlyCount_jcs--;
            for (int i = firstClass; i < sClass_List.getClassListSize(); i++) {
               if (curlyCount_jcs == ((sClass)sClass_List.getsClassAt(i)).getnCurly()) {
                   if (((sClass)sClass_List.getsClassAt(i)).getEndToken() == -1) {
                   	((sClass)sClass_List.getsClassAt(i)).setEndToken( nToken + 1);
                      jcs = JAVACLASSSTATE.JCS_END;
                   }
               }
           }
       }
        return jcs;
    }
    
    enum JAVACLASSSTATE {
        JCS_START,
        JCS_NAME,
        JCS_BODY,
        JCS_END,
    };

    
    int TransformToken(int InputTokenType) {
        return vTokenEquality.get(InputTokenType).transformedToken;
    }
    
    void StoreTokenInfo(int TokenType, int TokenLine,
            int TokenColumn, int TokenFile,
            int TokenClass, int TokenMethod,
            String TokenText) {
       /* if (offset) {
            vector<int>::iterator itVT = vTokens.begin() + offset;
            vTokens.insert(itVT, TokenType);
            vector<int>::iterator itVTL = vTokenLines.begin() + offset;
            vTokenLines.insert(itVTL, TokenLine);
            vector<int>::iterator itVTC = vTokenColumns.begin() + offset;
            vTokenColumns.insert(itVTC, TokenColumn);
            vector<int>::iterator itVTF = vTokenFile.begin() + offset;
            vTokenFile.insert(itVTF, TokenFile);
            vector<int>::iterator itVTCl = vTokenClass.begin() + offset;
            vTokenClass.insert(itVTCl, TokenClass);
            vector<int>::iterator itVTM = vTokenMethod.begin() + offset;
            vTokenMethod.insert(itVTM, TokenMethod);
            for (int i = offset + 1; i < vTokenMethod.size(); i++) {
                vTokenMethod[i] = TokenMethod;
            }

            vector<string>::iterator itVTT = vTokenText.begin() + offset;
            //hamid : for debuging only...to generate CombinedTokens.txt
            vTokenText.insert(itVTT,TokenText);
        }
        else {*/
    
    	if(TokenText == "STARTMETHOD")
    	{
    		if(methodStart != 0)
    		{
    			int tokenCheck = tokensList.getTokenAt(methodStart-1).getType();
    			//Backtracking until we reach the start of method...
				while(tokenCheck != TokenType-1 /*if token under consideration is not equal to end of previous method*/ && 
				      tokenCheck != JavaLexer.SEMI && tokenCheck != JavaLexer.COMMENT && tokenCheck != JavaLexer.LINE_COMMENT && 
				      tokenCheck != JavaLexer.LBRACE && tokenCheck != JavaLexer.RBRACE)
				{
					methodStart--;
					if((methodStart - 1) >= 0)
						tokenCheck = tokensList.getTokenAt(methodStart-1).getType();
					else
						break;
				} 	   				    				    			 			
    		}    
    		 Tokens t= new Tokens();
             t.setType(TokenType);
             t.setText(TokenText);
             t.setTokenColumn(TokenColumn);
             t.setTokenClass(TokenClass);
             t.setTokenFile(TokenFile);
             t.setTokenMethod(TokenMethod);
             //t.setTokenLine(TokenLine);
             tokensList.addTokenAt(methodStart, t); 
             
             for(int i = methodStart+1 ; i < tokensList.getTokensListSize() ; i++)
            	 tokensList.getTokenAt(i).setTokenMethod(TokenMethod);
             
             ((sMethod)sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() -1)).setStartToken(methodStart+1);                          
    	}    	
    	else
    	{                     
            Tokens t= new Tokens();
            t.setType(TokenType);
            t.setText(TokenText);
            t.setTokenColumn(TokenColumn);
            t.setTokenClass(TokenClass);
            t.setTokenFile(TokenFile);
            t.setTokenMethod(TokenMethod);
            if(!(TokenText.contains("ENDMETHOD") || TokenText.contains("ENDFILE") || TokenText.contains("STARTFILE") || TokenText.contains("STARTCLASS") || TokenText.contains("ENDCLASS")))
            	t.setTokenLine(TokenLine);
            tokensList.addToken(t);
    	}  
    }
    
    int curlyCount = 0;
	int parenCount = 0;
	String methodName = "";
	int methodStart = 0;
	String methodParameters = "";
     JAVAMETHODSTATE DetectJavaMethod(Token rT)
    {
        /*JAVAMETHODSTATE jms = JAVAMETHODSTATE.JMS0;
    	JAVAMETHODSTATE jms_next = JAVAMETHODSTATE.JMS0;
    	int curlyCount = 0;
    	int parenCount = 0;
    	String methodName = "";*/
    	
    	sMethod pMethod = new sMethod();

        int type = rT.getType();
        //implementation of the FSM for detecting method boundaries
        jms_next = GetJMState(jms, type);

        //This may be the method name
        if (jms_next == JAVAMETHODSTATE.JMS1) {
        	methodParameters = "";
            methodName = rT.getText(); 
            methodStart = tokensList.getTokensListSize() - 1;
        }
        if((jms_next != JAVAMETHODSTATE.JMS4) && (jms == JAVAMETHODSTATE.JMS3 || jms_next == JAVAMETHODSTATE.JMS3))
        {
        	methodParameters = methodParameters + rT.getText() + " ";
        }
        //this is the beginning of method body
        if ((jms == JAVAMETHODSTATE.JMS4 || jms == JAVAMETHODSTATE.JMS6) && jms_next == JAVAMETHODSTATE.JMS_START) {

            pMethod.setMethodName(methodName);
            pMethod.setMethodSig(methodParameters);
            sMethod_List.addsMethod(pMethod);
            curlyCount = 1;
        }
        //when we are inside formal parameters
        if (jms_next == JAVAMETHODSTATE.JMS2) {
            parenCount = 1;
        }
        if (jms == JAVAMETHODSTATE.JMS3) {
            if (rT.getType() == JavaLexer.LPAREN /*60*/) {
                parenCount++;
            } 
        }

        //when we are inside method body
        if (jms == JAVAMETHODSTATE.JMS_BODY) {
            if (rT.getType() == JavaLexer.LBRACE /*62*/) {
                curlyCount++;
            } else if (rT.getType() == JavaLexer.RBRACE /*63*/) {
                curlyCount--;
                if (curlyCount == 0) {
                    //only transition to ENDMETHOD state
                    jms_next = JAVAMETHODSTATE.JMS_END;
                }
            }
        }
        jms = jms_next;
        return jms;
    }
    
     JAVALISTSTATE jls = JAVALISTSTATE.JLS_END;
	 int curlyCount_jls = 0;
     JAVALISTSTATE DetectJavaList(Token rT, int nToken) {
    	   
    	    if (rT.getType() == JavaLexer.LBRACE /*62*/) {
    	        if (jls == JAVALISTSTATE.JLS_START) {
    	            jls = JAVALISTSTATE.JLS_BODY;
    	        }
    	        if (((Integer)(tokensList.getTokenAt(nToken-1).getType()) == JavaLexer.ASSIGN /*69*/) || ((Integer)(tokensList.getTokenAt(nToken-1).getType()) == JavaLexer.RBRACK/*65*/)) {
    	            jls = JAVALISTSTATE.JLS_START;
    	        }
    	        if (jls == JAVALISTSTATE.JLS_BODY || jls == JAVALISTSTATE.JLS_START) {
    	        	curlyCount_jls++;
    	        }
    	    } else if (rT.getType() == JavaLexer.RBRACE /*63*/) {
    	        if (jls == JAVALISTSTATE.JLS_START || jls == JAVALISTSTATE.JLS_BODY) {
    	        	curlyCount_jls--;
    	            jls = JAVALISTSTATE.JLS_BODY;
    	            if (curlyCount_jls == 0) {
    	                jls = JAVALISTSTATE.JLS_END;
    	            }
    	        }
    	    }
    	    else if (rT.getType() != JavaLexer.RBRACE /*63*/) {
    	        if (jls == JAVALISTSTATE.JLS_START || jls == JAVALISTSTATE.JLS_BODY) {
    	            jls = JAVALISTSTATE.JLS_BODY;
    	        }
    	    }
    	    return jls;
    	}

    public static boolean firstrun=false;
    Map<String, ArrayList<SequenceInfo>> lookupbyfile_newAddedFiles = new HashMap<String, ArrayList<SequenceInfo>>();
    Map<String, ArrayList<SequenceInfo>> lookupbyfile_updatedFiles = new HashMap<String, ArrayList<SequenceInfo>>();
  	Map<String, ArrayList<FileInfo>> lookupbyhash = new HashMap<String, ArrayList<FileInfo>>();
    ArrayList<String> updatedfiles = null;
	public ArrayList<String> getUpdateFiles()
    {
        return updatedfiles;	
    }
	
	int counter=0;
    int seqindex= - SysConfig.getThreshold();//0;
    boolean check = false;
    String tokenizestring="";
    ArrayList<SequenceHash> ins = new ArrayList<SequenceHash>();
    //ArrayList<TokenDetails> token_detail = new ArrayList<TokenDetails>();
    ArrayList<Integer> tokens = new ArrayList<Integer>();
    @SuppressWarnings({ "deprecation", "resource" })
    public void createLexerArr(AllFilesContent filesContent)
    {   
    	if(filesContent == null )//|| inputFiles.size() <= 0)
    		return;
    	
    	int methodBoundaryMarker=getMethodBoundaryMarker() ;    	
    	firstrun=filesContent.getrunflag();
    	
    	boolean newFilesAddedCheck = false;
		if(filesContent.getNewfiles().size() > 0)
			newFilesAddedCheck = true;
    	    	
    	if(filesContent.getrunflag() || newFilesAddedCheck)
    	{
    		int highestMethodId = 0;
    		if(newFilesAddedCheck)    		
    			highestMethodId = getHighestMethodIdFromDB(getMongoDB());
    		
    		boolean bUniqueMethods = false;
    		//special tokens for file/method sentinels...for java 120
        	int nUniqueSentinel = getUniqueSentinel(); //120;        	
        	ANTLRInputStream input;
        	
    		if(filesContent == null || filesContent.getOld().size() <= 0 && filesContent.getNewfiles().size() <= 0)
        		return;
    		
    		ins = new ArrayList<SequenceHash>();
    		int nToken = 0;
    		
    		int newFileIndex=0;
    		for (int i = 0; i < sFile_List.getFilesListSize(); i++)
        	{
    			if(newFilesAddedCheck && !sFile_List.getsFileAt(i).isNewlyAddedFile())
    	        	continue;
    			
    			if(newFilesAddedCheck)
    				input = new ANTLRInputStream(filesContent.getNewcontent().get(newFileIndex++));
    			else
    				input = new ANTLRInputStream(filesContent.getOld().get(i));
    	        lexer = new JavaLexer(input);	   
    	    		
		        Token rT;
		        int token_type = 0;
    	        int token_line = 0;
    	        int token_start = 0;
    	        int token_stop = 0;
		        int l = 1;
		        int c = 1;
		      	int semiORcurly = 0;
		        boolean newFile = true;
		        int currentClass = -1;
		        int currentMethod = -1;	       
		        String token_text= null;
		        counter=0;
		         
		        //put start of file sentinel in input vector
		        StoreTokenInfo(++nUniqueSentinel,l, c, i, -1, -1,"STARTFILE " + sFile_List.getsFileAt(i).getFullPath());
		        counter++;
		        check = false;
		        sFile_List.getsFileAt(i).setStartToken(nToken++);
	  
		        //token_detail = new ArrayList<TokenDetails>();
    	        //tokenizestring="";
    	        
    	        seqindex = seqindex + SysConfig.getThreshold() ; //0;
    	        
		        JAVAMETHODSTATE jms;
		        JAVALISTSTATE jls;
		        JAVACLASSSTATE jcs;
		        //tokens = new ArrayList<Integer>(); 
		        while (!(rT = lexer.nextToken()).getText().equals("<EOF>")) 
		        {
		        	token_type = rT.getType();
		        	token_text = rT.getText();		        	
		        	
		        	if (SysConfig.getSupressTokens()[token_type]) continue;
	
 		            l = rT.getLine();
		            c = rT.getCharPositionInLine();
		            jcs = DetectJavaClass(rT, nToken, newFile);
		            jms = DetectJavaMethod(rT);
		            jls = DetectJavaList(rT, nToken);	          	            
    	        	
		            token_line = rT.getLine();
    	        	token_start = rT.getCharPositionInLine();
    	        	token_stop = rT.getCharPositionInLine();
    	        	//token_detail.add(new TokenDetails(token_type,token_line, token_start, token_stop,currentMethod , i));   
    	        	
		            if (jls != JAVALISTSTATE.JLS_BODY) {
		                if (jcs == JAVACLASSSTATE.JCS_START) {
		                    currentClass = sClass_List.getClassListSize();	                  
		                    
		                  //insert special token marking class beginning
		             StoreTokenInfo(++nUniqueSentinel,l, c, i, currentClass, -1,"STARTCLASS");
		             counter++;
					 check = false;
					 nToken++;		                    
		                   
		                } else if (jcs == JAVACLASSSTATE.JCS_NAME) {
		                    if (newFile) newFile = false;	                    
		                    ((sClass)sClass_List.getsClassAt(currentClass)).setFileName(((sFile)sFile_List.getsFileAt(i)).getFileName());
		                }
		                if(!check && CloneTrackingFormPage.isIncremental)
		                	computeHashes(newFilesAddedCheck, i  ,token_line ,token_stop);
		                
		                //remember last ; or } or {
		                if (jms == JAVAMETHODSTATE.JMS0 || jms == JAVAMETHODSTATE.JMS_END) {
		                    if (rT.getType() == JavaLexer.SEMI /*66*/ || rT.getType() == JavaLexer.RBRACE /*63*/
		                            || rT.getType() == JavaLexer.LBRACE /*62*/) {
		                        semiORcurly = nToken;
		                    }
		                }
		                else if (jms == JAVAMETHODSTATE.JMS_START) {
		                    //if nUniqueSentinel:
		                    //insert special token marking method beginning
		                    l = tokensList.getTokenAt(semiORcurly).getTokenLine();
		                    c = tokensList.getTokenAt(semiORcurly).getTokenColumn();
		                    currentMethod = highestMethodId++;//sMethod_List.getMethodListSize() - 1 ;
		                    ((sMethod)sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1)).setMethodId(currentMethod);
		                    
                            StoreTokenInfo(bUniqueMethods ? ++nUniqueSentinel : methodBoundaryMarker,l, c, i, currentClass,currentMethod,"STARTMETHOD");	   
                            counter++;
                            check = false;
                        
		                    //((sMethod)sMethod_List.getsMethodAt(currentMethod)).setStartToken(semiORcurly + 2);
		                    ((sMethod)sMethod_List.getMethodWithId(currentMethod)).setEndToken(-1);
		                    ((sMethod)sMethod_List.getMethodWithId(currentMethod)).setFileID(i);
		                    //((sMethod)sMethod_List.getMethodAtId(currentMethod)).setMethodId(currentMethod);
		                    
		                    //store method info in files too
		                    ((sFile)sFile_List.getsFileAt(i)).addvMethod(currentMethod);	                  
                            nToken++;
		                   
		                    //added for start of class token
    	                    //token_type=methodBoundaryMarker;
    	                    
		                    //to get the actual token info for {
		                    l = rT.getLine();
		                    c = rT.getCharPositionInLine();
		                }
		                
		                if(!check && CloneTrackingFormPage.isIncremental)
		                	computeHashes(newFilesAddedCheck, i  ,token_line ,token_stop);
		                
		                //insert the TRANSFORMED token in token vector	             
  		                StoreTokenInfo(token_type,l, c, i, currentClass, currentMethod,rT.getText());
  		                tokensList.getTokenAt(tokensList.getTokensListSize()-1).setTokenStart(token_start);
	                    tokensList.getTokenAt(tokensList.getTokensListSize()-1).setTokenStop(token_stop);
 		                counter++;
 		                check = false;
		                nToken++;
		                
		                if(!check && CloneTrackingFormPage.isIncremental)
		                	computeHashes(newFilesAddedCheck, i  ,token_line ,token_stop);
		                
		                if (jcs == JAVACLASSSTATE.JCS_END) {
		                    StoreTokenInfo(++nUniqueSentinel,l, c, i, currentClass, -1,"ENDCLASS");
		                    counter++;
		                    check = false;
		                    currentClass = -1;
		                    semiORcurly = nToken;
		                    nToken++;
		                }
		                
		                if(!check && CloneTrackingFormPage.isIncremental)
		                	computeHashes(newFilesAddedCheck, i ,token_line ,token_stop);
		                
		                if (jms == JAVAMETHODSTATE.JMS_END) {
		                	((sMethod)sMethod_List.getMethodWithId(currentMethod)).setEndToken(nToken - 1);
		                	//((sMethod)sMethod_List.getsMethodAt(currentMethod)).setMethodId(methodId++);		                    
		                    //insert special token marking method end
                    StoreTokenInfo(bUniqueMethods ? ++nUniqueSentinel : methodBoundaryMarker++,l, c, i,currentClass, currentMethod,"ENDMETHOD");
                    counter++;	
					check = false;
		                    //added for start of class token
    	                    //token_type=methodBoundaryMarker;
    	                    
		                    currentMethod = -1;
		                    semiORcurly = nToken;
		                    nToken++;
		                }		               
		            }    	        	
    	        	 
		            if(!check && CloneTrackingFormPage.isIncremental)
		            	computeHashes(newFilesAddedCheck, i  ,token_line ,token_stop);    	        	
		        }
		        
		        //put end of file sentinel in input vector
		        StoreTokenInfo(++nUniqueSentinel,l, c, i, -1, -1,"ENDFILE " + sFile_List.getsFileAt(i).getFullPath());
	            sFile_List.getsFileAt(i).setEndToken(nToken);
	            nToken++;	
        	}    		
    		this.equateTokens(); 
    		try
    		{               
             	//First time insertion into Database
    			 Stopwatch stopwatch = new Stopwatch();          	 
 	        	 //MongoClient mongo = new MongoClient("localhost:27017");
                    //DB db = mongo.getDB("admin");
    			    DB db = getMongoDB();
                    DBCollection collection = db.getCollection("ByFiles");
             		for(SequenceHash s:ins)
             		{  
     				    BasicDBObject document = new BasicDBObject(); 
     				    //InsertThread T=new InsertThread(collection,s);
     				    // T.start();
     			        document.put("filename", s.filename);
     			        document.put("sequenceindex", s.sequenceindex);
     			        document.put("sequencehash", s.sequencehash);
     			        document.put("startline", s.start_line);
     			        document.put("stopline", s.stop_line);
     			        document.put("startindex", s.start_index);
     			        document.put("stopindex", s.stop_index);
     			        document.put("methodid", s.method_id);
     			        document.put("fileid", s.file_id);
     			        InsertInDB(collection,document);
     			     } 
             		
             		if(CloneTrackingFormPage.isIncremental)
             		{
	             		 // Save Method Boundary marker
	             		 saveBoundaryMarkers(methodBoundaryMarker,nUniqueSentinel);
	             		 System.out.println("first time insertion :"+stopwatch.elapsedTime());
             		}
             		 /*DBCollection FileCollection = db.getCollection("Files");
             		 BasicDBObject document = new BasicDBObject();
             		 document.put("MethodBoundary",methodBoundaryMarker);
             		 InsertInDB(FileCollection,document);*/
     	  }
    	  catch (MongoException e)
    	  { 
    	 	  System.out.println(e);
          } 
       }
       if(!filesContent.getrunflag() && filesContent.getUpdatedfiles().size() > 0)
       {
    	    ins = new ArrayList<SequenceHash>();
    		for(int j=0;j<2;j++)
        	{
        		if(j==0 && (filesContent == null || filesContent.getUpdated().size() <= 0))
            		continue;
        		else if(j==1 && (filesContent == null || filesContent.getOld().size() <= 0))
        			continue;
        		if(j==1)
        		{
        			continue;
        		}
        	}     	
    		DB data_base = getMongoDB(); 		
        	updatedfiles=filesContent.getUpdatedfiles();
        	methodBoundaryMarker = getMethodBoundaryMarker();	        		
        	int methodcount=0 ;//, fixedMethodCount = 0;			
        	methodcount = getHighestMethodIdFromDB(data_base);
        	
			//fixedMethodCount =  methodcount;
			boolean bUniqueMethods = false;
            int nUniqueSentinel = getUniqueSentinel(); //120;
            ANTLRInputStream input;
            int nToken = 0;
            java.sql.PreparedStatement pstmtdelete = null;
            java.sql.PreparedStatement pstmtadd = null;
            for (int i = 0; i < filesContent.getUpdated().size(); i++)
            {
            	int fileId = sFile_List.getFileIdAgainstFileName(filesContent.getUpdatedfiles().get(i));
            	BasicDBObject doc = new BasicDBObject();
            	doc.put("FileId", fileId);
            	DBCollection methodInfo = data_base.getCollection("MethodInfo");
                DBCursor curs = methodInfo.find(doc);
                
                ArrayList<sMethod> methodsFromDB = new ArrayList<sMethod>();
                while(curs.hasNext()) {
                    DBObject o = curs.next();
                    sMethod method = new sMethod();
                    method.setMethodName((String) o.get("MethodName"));
                    method.setMethodSig((String) o.get("MethodSignature"));
                    method.setMethodId((int) o.get("MethodId"));
                    method.setFileID((int) o.get("FileId"));
                    method.setFilePath((String) o.get("FilePath"));
                    method.setStartToken((int) o.get("StartToken"));
                    method.setEndToken((int) o.get("EndToken"));
                    
                    methodsFromDB.add(method);
                }
               
            	methodInfo.remove(doc);

            	input = new ANTLRInputStream(filesContent.getUpdated().get(i));
        	    lexer = new JavaLexer(input);	     
        	        
        	    //special tokens for file/method sentinels...for java 120
        	    Token rT;
        	    int token_type = 0;
        	    int token_line=0;
        	    int token_start=0;
        	    int token_stop=0;
        	    int l = 1;
        	    int c = 1;
        	    int semiORcurly = 0;
        	    boolean newFile = true;
        	    int currentClass = -1;
        	    int currentMethod = -1;	       
        	    ArrayList<TokenDetails> token_detail = new ArrayList<TokenDetails>();
        	    
        	    sFile_List.getsFileAt(fileId).setStartToken(nToken++);
        	  
        	    tokenizestring="";
        	    counter=0; //1;
        	    seqindex=0;
        	    //ArrayList<SequenceHash> ins = new ArrayList<SequenceHash>();

        	    JAVAMETHODSTATE jms;
        	    JAVALISTSTATE jls;
        	    JAVACLASSSTATE jcs;
        	    //ArrayList<Integer> tokens = new ArrayList<Integer>(); 
        	    
        	    //put start of file sentinel in input vector
        	    StoreTokenInfo(++nUniqueSentinel,l, c, fileId, -1, -1,"STARTFILE " + sFile_List.getsFileAt(fileId).getFullPath());
        	    counter++;
		        check = false;
		        
        	    while (!(rT = lexer.nextToken()).getText().equals("<EOF>")) 
        	    {
        	       token_type = rT.getType();
        	       l = rT.getLine();
        	       c = rT.getCharPositionInLine();
        	       jcs = DetectJavaClass(rT, nToken, newFile);
        	       jms = DetectJavaMethod(rT);
        	       jls = DetectJavaList(rT, nToken);	          
        	           
        	       token_line = rT.getLine();
     	           token_start = rT.getCharPositionInLine();
     	           token_stop = rT.getCharPositionInLine();
     	          
        	       if (jls != JAVALISTSTATE.JLS_BODY) 
        	       {
        	            if (jcs == JAVACLASSSTATE.JCS_START) 
        	            {
        	                 currentClass = sClass_List.getClassListSize();	                  
        	                    
        	                  //insert special token marking class beginning
        	                  StoreTokenInfo(++nUniqueSentinel,l, c, fileId, currentClass, -1,"STARTCLASS");
        	                  counter++;
        	  		          check = false;
        	                  nToken++;   
        	                  
        	                  if(!check && CloneTrackingFormPage.isIncremental)  		            	
                	        	  computeHashesOfUpatedFiles(fileId , (filesContent.getUpdatedfiles().get(i)) , token_line , token_stop);
        	             } 
        	            else if (jcs == JAVACLASSSTATE.JCS_NAME) 
        	            {
        	                  if (newFile) newFile = false;
        	                  sClass_List.getsClassAt(currentClass).setFileName(sFile_List.getsFileAt(fileId).getFileName());
        	            }        	            
        	            //remember last ; or } or {
        	            if (jms == JAVAMETHODSTATE.JMS0 || jms == JAVAMETHODSTATE.JMS_END) 
        	            {
        	                 if (rT.getType() == JavaLexer.SEMI /*66*/ || rT.getType() == JavaLexer.RBRACE /*63*/ || rT.getType() == JavaLexer.LBRACE /*62*/) 
        	                 {
        	                      semiORcurly = nToken;
        	                 }
        	            }
        	            else if (jms == JAVAMETHODSTATE.JMS_START) 
        	            {
        	            	  //fixedMethodCount = methodcount;
        	            	  sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1).setFileID(sFile_List.getsFileAt(fileId).getFileID());
        	            	  if(updateMethods(methodsFromDB , methodcount))
        	            		  methodcount++;
        	            	
        	                  //if nUniqueSentinel:
        	                  //insert special token marking method beginning
        	                  l = tokensList.getTokenAt(semiORcurly).getTokenLine();
        	                  c = tokensList.getTokenAt(semiORcurly).getTokenColumn();
        	                  if(sMethod_List.getMethodListSize() > 0)
        	                	  currentMethod = sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1).getMethodId();
        	                  else
        	                	  currentMethod = methodcount - 1;//sMethod_List.getMethodListSize() - 1; 
        	                  //currentMethod++;
        	                  StoreTokenInfo(bUniqueMethods ? ++nUniqueSentinel : methodBoundaryMarker,l, c, fileId, currentClass,currentMethod,"STARTMETHOD");	                    
        	                  counter++;
        	  		          check = false;
        	  		          
        	                  sMethod_List.getMethodWithId(currentMethod).setStartToken(semiORcurly + 2);
        	                  sMethod_List.getMethodWithId(currentMethod).setMethodId(currentMethod);//+methodcount);
        	                  //sMethod_List.getMethodWithId(currentMethod).setFileID(sFile_List.getsFileAt(fileId).getFileID());
        	                  sMethod_List.getMethodWithId(currentMethod).setEndToken(-1);
        	                  //sMethod_List.getsMethodAt(currentMethod).setFileID(i);

        	                  //store method info in files too
        	                  sFile_List.getsFileAt(fileId).getvMethods().add(currentMethod);                  
        	                  nToken++;
        	                   
        	                  //added for start of class token
        	                  //token_type=methodBoundaryMarker;
        	                    
        	                  //to get the actual token info for 
        	                  l = rT.getLine();
        	                  c = rT.getCharPositionInLine();
        	                  
        	                  if(!check && CloneTrackingFormPage.isIncremental)  		            	
                	        	  computeHashesOfUpatedFiles(fileId , (filesContent.getUpdatedfiles().get(i)) , token_line , token_stop);
        	             }
        	             //insert the TRANSFORMED token in token vector	             
        	             StoreTokenInfo(token_type,l, c, fileId, currentClass, currentMethod,rT.getText());
        	             counter++;
   	  		             check = false;
   	  		          
        	             tokensList.getTokenAt(tokensList.getTokensListSize()-1).setTokenStart(token_start);
 	                     tokensList.getTokenAt(tokensList.getTokensListSize()-1).setTokenStop(token_stop);
        	             nToken++;
        	                 
        	             if(!check && CloneTrackingFormPage.isIncremental)  		            	
           	        	  computeHashesOfUpatedFiles(fileId , (filesContent.getUpdatedfiles().get(i)) , token_line , token_stop);
        	             
        	             if (jcs == JAVACLASSSTATE.JCS_END) 
        	             {
        	                 StoreTokenInfo(++nUniqueSentinel,l, c, fileId, currentClass, -1,"ENDCLASS");	                   
        	                 currentClass = -1;
        	                 semiORcurly = nToken;
        	                 nToken++;    
        	                 
        	                 if(!check && CloneTrackingFormPage.isIncremental)  		            	
                 	        	  computeHashesOfUpatedFiles(fileId , (filesContent.getUpdatedfiles().get(i)) , token_line , token_stop);
        	             }        	                     	             
        	             if (jms == JAVAMETHODSTATE.JMS_END) 
        	             {
        	                sMethod_List.getMethodWithId(currentMethod).setEndToken(nToken - 1);
        	                
        	                //if nUniqueSentinel: 
        	                //insert special token marking method end
        	                StoreTokenInfo(bUniqueMethods ? ++nUniqueSentinel : methodBoundaryMarker++,l, c, fileId,currentClass, currentMethod,"ENDMETHOD");
        	                counter++;
      	  		          	check = false;
        	                //added for start of class token
        	                token_type=methodBoundaryMarker;                 
        	                //currentMethod = -1;
        	                semiORcurly = nToken;
        	                nToken++;        	               
        	             }
        	          } 
        	          
        	          /*tokens.add(token_type);
        	          token_line = rT.getLine();
        	          token_start = rT.getCharPositionInLine();
        	          token_stop = rT.getCharPositionInLine();*/
        	          
        	          if(token_type != methodBoundaryMarker)
        	        	  if (SysConfig.getSupressTokens()[token_type]) continue;
        	          
        	          
        	          if(!check && CloneTrackingFormPage.isIncremental)  		            	
        	        	  computeHashesOfUpatedFiles(fileId , (filesContent.getUpdatedfiles().get(i)) , token_line , token_stop);        	                 	            
        	        }
   //     	        updateMethods(methodsFromDB , methodcount);
        	        try
        	        {          	        	
        	        	//remove updated files from db
        	        	// uncomment 
        	        	//commented because taking alot of time

        	        	 Stopwatch stopwatch = new Stopwatch();
        	        	 MongoClientOptions options = MongoClientOptions.builder()
        	                     .threadsAllowedToBlockForConnectionMultiplier(100)
        	                     .build();

                         DB db = mongo.getDB("admin");
                         DBCollection collection = db.getCollection("ByFiles");
                         BasicDBObject whereQuery = new BasicDBObject();
            			for(SequenceHash s:ins){  
            				
            				 whereQuery.put("filename", s.filename);
         					 collection.remove(whereQuery);
            			}             			
            		    System.out.println("removal of updated files from db time"+stopwatch.elapsedTime());            			
            		}
        	        catch(MongoException e)
        	        {
            			System.out.println(e);
            		} 
        	        catch (Exception e) 
        	        {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
        	        try
        	        {  
        	        	 Stopwatch stopwatch = new Stopwatch();
                         DB db = mongo.getDB("admin");
                         DBCollection collection = db.getCollection("ByFiles");
                  		 for(SequenceHash s:ins)
                  		 {  
                  			BasicDBObject document = new BasicDBObject();
          			        document.put("filename", s.filename);
          			        document.put("sequenceindex", s.sequenceindex);
          			        document.put("sequencehash", s.sequencehash);
          			        document.put("startline", s.start_line);
          			        document.put("stopline", s.stop_line);
        			        document.put("startindex", s.start_index);
        			        document.put("stopindex", s.stop_index);
        			        document.put("methodid", s.method_id);
        			        document.put("fileid", s.file_id);
          			        InsertInDB(collection,document);
                  		 } 
                  		 System.out.println("insertion of updated token in database"+stopwatch.elapsedTime());
	        	      }
        	          catch(MongoException e)
        	          { 
        	        	  System.out.println(e);
        	          } 
        	          catch (Exception e) 
        	          {
							// TODO Auto-generated catch block
							e.printStackTrace();
        	          } 
        	        
        	        /*
        	         	lookup for simple clones identification
        	        */
        	        //put end of file sentinel in input vector
                    StoreTokenInfo(++nUniqueSentinel,l, c, fileId, -1, -1,"ENDFILE " + sFile_List.getsFileAt(fileId).getFullPath());
                    sFile_List.getsFileAt(fileId).setEndToken(nToken);
                    nToken++;	                  
            	}            	
                this.equateTokens();  
                
                // Save Method Boundary marker
        		saveBoundaryMarkers(methodBoundaryMarker,nUniqueSentinel);
        	}
            int [] deletedFiles = CloneTrackingFormPage.getDeletedFileID();
       		if(deletedFiles != null && deletedFiles.length > 0)
       		{
       			DB db = mongo.getDB("admin");
       			
       			DBCollection collection1 = db.getCollection("ByFiles");
                BasicDBObject whereQuery1 = new BasicDBObject();
                
                DBCollection collection2 = db.getCollection("FileInfo");
			    BasicDBObject whereQuery2 = new BasicDBObject();
					
			    DBCollection collection3 = db.getCollection("MethodInfo");
				BasicDBObject whereQuery3 = new BasicDBObject();
					
       			for(int r = 0 ; r < deletedFiles.length ; r++)
       			{
       				int removedFileId = deletedFiles[r];

       			    //Removal of Hashes from DB of deleted files...                    
    			    whereQuery1.put("fileid", removedFileId);
 					collection1.remove(whereQuery1);    
 					
 					//Removal of file information from DB of deleted files.. 					
    			    whereQuery2.put("fileId", removedFileId);
    			    collection2.remove(whereQuery2); 
    			    
    			    //Removal of deleted files methods from DB...    			    
    			    whereQuery3.put("FileId", removedFileId);
    			    collection3.remove(whereQuery3); 
       			}
       		}
    }
    
    public int getHighestMethodIdFromDB(DB data_base)
    {
    	int methodcount = 0;    	
    	//Get highest Method Id from Database
		DBCollection col = data_base.getCollection("MethodInfo");
		DBCursor dbcursor=col.find().sort( new BasicDBObject( "MethodId" , -1 ) ).limit(1);
						
		List<DBObject> res = new ArrayList<DBObject> (); 
		while (dbcursor.hasNext()) 
		{
			res.add(dbcursor.next());
		}
		for(int n=0; n<res.size(); n++) 
  		{
    	       DBObject obj=res.get(n);
    	       methodcount=(int) obj.get( "MethodId" );
  		}
		methodcount++;	
		return methodcount;
    }
    
    private void populateTokenizeStringAndTokensList()
    {
    	tokenizestring="";
    	tokens = new ArrayList<Integer>();
    	for(int i =  tokensList.getTokensListSize() - SysConfig.getThreshold() ; i < tokensList.getTokensListSize() ; i++)
    	{
    		tokenizestring = tokenizestring + " " + tokensList.getTokenAt(i).getType();
    		tokens.add(tokensList.getTokenAt(i).getType());
    	}
    }
        
    private void computeHashes(boolean newFilesAddedCheck, int fileId , int token_line , int token_stop)
    {
     	if(counter>=SysConfig.getThreshold())   //32)
    	{     		
     		int index = tokensList.getTokensListSize() - SysConfig.getThreshold();
    		String file =(sFile_List.getsFileAt(fileId).getFileName());  	
    		populateTokenizeStringAndTokensList();
    		String md5Hex = DigestUtils.md5Hex(tokenizestring).toUpperCase();
    		
    		ins.add(new SequenceHash(file,seqindex,md5Hex,tokensList.getTokenAt(index).getTokenLine(),token_line,tokensList.getTokenAt(index).getTokenStart(),token_stop,tokensList.getTokenAt(index).getTokenMethod() , tokensList.getTokenAt(index).getTokenFile()));    		
    		/////////////////////////////////////////////////FOR NEWLY ADDED FILES/////////////////////////////////////////////////////
    		if(newFilesAddedCheck)
    		{
    		    	if(!lookupbyfile_newAddedFiles.containsKey(file))
	        		{
        		     	 ArrayList<Integer> newList = new ArrayList<Integer>();
        		     	 newList=makeDeepCopyInteger(tokens);
	        			 ArrayList<SequenceInfo> si=new ArrayList<SequenceInfo>();
	        			 
	        			 SequenceInfo info=new SequenceInfo(seqindex, md5Hex,tokensList.getTokenAt(index).getTokenLine(),token_line,tokensList.getTokenAt(index).getTokenStart(),token_stop,newList);
	        			 si.add(info);
	        			 lookupbyfile_newAddedFiles.put(file, si);
	        			 //token_detail.remove(0);
	        		 }
	        		 else
	        		 {         	        			 
	        			 ArrayList<Integer> newList = new ArrayList<Integer>();
       			         newList=makeDeepCopyInteger(tokens);
	        			 ArrayList<SequenceInfo> si=lookupbyfile_newAddedFiles.get(file);
	        			 SequenceInfo info=new SequenceInfo(seqindex, md5Hex,tokensList.getTokenAt(index).getTokenLine(),token_line,tokensList.getTokenAt(index).getTokenStart(),token_stop,newList);
	        			 si.add(info);

	        			 lookupbyfile_newAddedFiles.put(file, si);
	        			 //token_detail.remove(0);
	        		 }
    		}
    		//else
    			//token_detail.remove(0);
    		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
       		//String md5Hex = DigestUtils.md5Hex(tokenizestring).toUpperCase();
    		//ins.add(new SequenceHash((sFile_List.getsFileAt(i)).getFileName(),seqindex,md5Hex,token_detail.get(0).token_line,token_line,token_detail.get(0).start_index,token_stop,token_detail.get(0).method_id , token_detail.get(0).file_id));
    		seqindex++;    	        		
    		//tokens.remove(0);
    		//tokenizestring=tokenizestring.substring(tokenizestring.indexOf(' ') + 1);
    	}
     	check = true;
    	//counter++;
    }
    
    private void computeHashesOfUpatedFiles(int fileId , String file , int token_line , int token_stop)
    {    	
         if(counter>=SysConfig.getThreshold()) //32)
         {
        	//String file = (filesContent.getUpdatedfiles().get(i));
        	int index = tokensList.getTokensListSize() - SysConfig.getThreshold();
        	populateTokenizeStringAndTokensList();
       		String md5Hex = DigestUtils.md5Hex(tokenizestring).toUpperCase();
       		
       		ins.add(new SequenceHash(file,seqindex,md5Hex,tokensList.getTokenAt(index).getTokenLine(),token_line,tokensList.getTokenAt(index).getTokenStart(),token_stop,tokensList.getTokenAt(index).getTokenMethod() , tokensList.getTokenAt(index).getTokenFile()));
       		if(!lookupbyfile_updatedFiles.containsKey(file))
        	{
       		    ArrayList<Integer> newList = new ArrayList<Integer>();
       			newList=makeDeepCopyInteger(tokens);
        		ArrayList<SequenceInfo> si=new ArrayList<SequenceInfo>();
        			 
        		SequenceInfo info=new SequenceInfo(seqindex, md5Hex,tokensList.getTokenAt(index).getTokenLine(),token_line,tokensList.getTokenAt(index).getTokenStart(),token_stop,newList);
        		si.add(info);
        		lookupbyfile_updatedFiles.put(file, si);
        		//token_detail.remove(0);
        	}
        	else
        	{         	        			 
        		ArrayList<Integer> newList = new ArrayList<Integer>();
      			newList=makeDeepCopyInteger(tokens);
        		ArrayList<SequenceInfo> si=lookupbyfile_updatedFiles.get(file);
        		SequenceInfo info=new SequenceInfo(seqindex, md5Hex,tokensList.getTokenAt(index).getTokenLine(),token_line,tokensList.getTokenAt(index).getTokenStart(),token_stop,newList);
        		si.add(info);

        		lookupbyfile_updatedFiles.put(file, si);
        		//token_detail.remove(0);
        	}
       		if(!lookupbyhash.containsKey(md5Hex))
        	{
        		ArrayList<FileInfo> fi=new ArrayList<FileInfo>();
        		FileInfo info=new FileInfo(seqindex, file,fileId);
        		fi.add(info);
        	    lookupbyhash.put(md5Hex, fi);
        	}
        	else
            {
        		ArrayList<FileInfo> fi=lookupbyhash.get(md5Hex);
        		FileInfo info=new FileInfo(seqindex, file,fileId);
        		fi.add(info);
        		lookupbyhash.put(md5Hex, fi);
        	}
       	    //tokens.remove(0); 

       		seqindex++;
       		//tokenizestring=tokenizestring.substring(tokenizestring.indexOf(' ') + 1);
       	}
        check = true;
    }
    
    private DB getMongoDB()
    {    	
    	if(mongo == null)
    	{
			try 
			{
				mongo = new MongoClient("localhost:27017");
			}
			catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
    	}
		return mongo.getDB("admin");
    }
    
    private int getMethodBoundaryMarker()
    {
    	int methodBoundaryMarker = 116;
    	DB data_base = getMongoDB();
        DBCollection Filecollection = data_base.getCollection("BoundaryMarkers");
        if(Filecollection != null)
        {
	        List<DBObject> result = new ArrayList<DBObject> (); 
	        DBCursor cursor = Filecollection.find();
			while (cursor.hasNext()) 
			{
				result.add(cursor.next());
			}
			for(int n=0; n<result.size(); n++) 
			{
		        DBObject obj=result.get(n);
		        methodBoundaryMarker=(int) obj.get( "MethodBoundary" );
			}
        }
		return methodBoundaryMarker;
    }
    
    private int getUniqueSentinel()
    {
    	int nUniqueSentinel = 120; 
    	DB data_base = getMongoDB();
        DBCollection Filecollection = data_base.getCollection("BoundaryMarkers");
        if(Filecollection != null)
        {
	        List<DBObject> result = new ArrayList<DBObject> (); 
	        DBCursor cursor = Filecollection.find();
			while (cursor.hasNext()) 
			{
				result.add(cursor.next());
			}
			for(int n=0; n<result.size(); n++) 
			{
		        DBObject obj=result.get(n);
		        nUniqueSentinel=(int) obj.get( "UniqueSentinel" );
			}
        }
		return nUniqueSentinel;
    }
    
    private boolean updateMethods(ArrayList<sMethod> methodsFromDB , int methodCounter )
    { 
    	//for(int i = 0; i < sMethod_List.getMethodListSize() ; i++)
	    //{    	
    		boolean set = false;    		
    		for(int j = 0 ; j < methodsFromDB.size() ; j++)
    		{
	    		if(sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1/*i*/).getMethodName().equals(methodsFromDB.get(j).getMethodName()) && 
	    				sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1 /*i*/).getMethodSig().equals(methodsFromDB.get(j).getMethodSig()) && 
	    				sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1 /*i*/).getFileID() == methodsFromDB.get(j).getFileID())
	    		{
	    			sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1 /*i*/).setMethodId(methodsFromDB.get(j).getMethodId());
	    			set = true;
	    			break;
	    		}	    		
    		}
    		if(!set)
    		{
    			sMethod_List.getsMethodAt(sMethod_List.getMethodListSize() - 1 /*i*/).setMethodId(methodCounter);
    			return true;
    		}
    		else
    			return false;
    	//}
    }
    
    private void saveBoundaryMarkers(int methodBoundaryMarker , int uniqueSentinel)
    {
    	 //Remove if there is any previous record...    	 
		 DBCollection FileCollection = getMongoDB().getCollection("BoundaryMarkers");
		 FileCollection.drop();
		 //Save Method Boundary marker
		 BasicDBObject document = new BasicDBObject();		
		 document.put("MethodBoundary",methodBoundaryMarker);
		 document.put("UniqueSentinel",uniqueSentinel);
		 InsertInDB(FileCollection,document);
    }
    
	private static void InsertInDB(DBCollection collection,BasicDBObject document)
	{
		collection.insert(document);
	}

    private ArrayList<Integer> makeDeepCopyInteger(ArrayList<Integer> old)
    {
		  ArrayList<Integer> copy = new ArrayList<Integer>(old.size());
		  for(Integer i : old)
		  {
		        copy.add(new Integer(i));
		  }
		  return copy;
	}
        
    public Map<String, ArrayList<SequenceInfo>> getfilelookup()
    {
    	Map<String, ArrayList<SequenceInfo>> lookupbyfile = new HashMap<String, ArrayList<SequenceInfo>>();
    	lookupbyfile.putAll(lookupbyfile_newAddedFiles);
    	lookupbyfile.putAll(lookupbyfile_updatedFiles);
        return lookupbyfile;	
    }
    
    public TokensList getTokensList()
    {
        return tokensList;	
    }
    
    private void equateTokens()
    {
    	if(SysConfig.getEquateTokens().size() != 0 && tokensList.getTokensListSize() != 0)
    	{
	    	for(int j=0 ; j< SysConfig.getEquateTokens().size() ; j++)
	    	{
	    		ArrayList<Integer> temp = SysConfig.getEquateTokens().get(j);
	    		for(int k = 1 ; k< temp.size() ; k++)
	            {
	          	    for(int i=0;i<tokensList.getTokensListSize(); i++)
	                {
		          	  if(temp.get(k).equals(tokensList.getTokenAt(i).getType()))
		          	  {
		          		tokensList.getTokenAt(i).setType(temp.get(0));
		          	  }
	                }
	            }	                 
	        }
    	}
    }

	public void createLexerArr(String codeSegment)
    {   	
    	if(codeSegment == null)
    		return;
    	
    	int methodBoundaryMarker=116;    	
    	boolean bUniqueMethods = false;
    	int nUniqueSentinel = 120;
    	ANTLRInputStream input;
  
    	int nToken = 0;
    		input = new ANTLRInputStream(codeSegment);
	        lexer = new JavaLexer(input);	     
	        
	        //special tokens for file/method sentinels...for java 120
	        
	        Token rT;
	        int type = 0;
	        int l = 1;
	        int c = 1;
	      	int semiORcurly = 0;
	        boolean newFile = true;
	        int currentClass = -1;
	        int currentMethod = -1;	       
	
	        //put start of file sentinel in input vector
	      //  StoreTokenInfo(++nUniqueSentinel,l, c, 0, -1, -1,"STARTFILE " + sFile_List.getsFileAt(0).getFullPath());
	       // sFile_List.getsFileAt(0).setStartToken(nToken++);
	  
	        JAVAMETHODSTATE jms;
	        JAVALISTSTATE jls;
	        JAVACLASSSTATE jcs;
	        while (!(rT = lexer.nextToken()).getText().equals("<EOF>")) 
	        {
	        	type = rT.getType();
	        	if (SysConfig.getSupressTokens()[type]) continue;
	
	            l = rT.getLine();
	            c = rT.getCharPositionInLine();
	            jcs = DetectJavaClass(rT, nToken, newFile);
	            jms = DetectJavaMethod(rT);
	            jls = DetectJavaList(rT, nToken);	          
	            
	            if (jls != JAVALISTSTATE.JLS_BODY) {
	                if (jcs == JAVACLASSSTATE.JCS_START) {
	                    currentClass = sClass_List.getClassListSize();	                  
	                    
	                  //insert special token marking class beginning
	                    StoreTokenInfo(++nUniqueSentinel,l, c, 0, currentClass, -1,"STARTCLASS");
	                    nToken++;
	                    
	                   
	                } else if (jcs == JAVACLASSSTATE.JCS_NAME) {
	                    if (newFile) newFile = false;	                    
	                    ((sClass)sClass_List.getsClassAt(currentClass)).setFileName(((sFile)sFile_List.getsFileAt(0)).getFileName());
	                }
	                //remember last ; or } or {
	                if (jms == JAVAMETHODSTATE.JMS0 || jms == JAVAMETHODSTATE.JMS_END) {
	                    if (rT.getType() == JavaLexer.SEMI /*66*/ || rT.getType() == JavaLexer.RBRACE /*63*/
	                            || rT.getType() == JavaLexer.LBRACE /*62*/) {
	                        semiORcurly = nToken;
	                    }
	                }
	                else if (jms == JAVAMETHODSTATE.JMS_START) {
	                    //if nUniqueSentinel:
	                    //insert special token marking method beginning
	                    l = tokensList.getTokenAt(semiORcurly).getTokenLine();
	                    c = tokensList.getTokenAt(semiORcurly).getTokenColumn();
	                    currentMethod = sMethod_List.getMethodListSize() - 1 ;
	                    StoreTokenInfo(bUniqueMethods ? ++nUniqueSentinel : methodBoundaryMarker,l, c, 0, currentClass,currentMethod,"STARTMETHOD");	                    
	                    
	                    //((sMethod)sMethod_List.getsMethodAt(currentMethod)).setStartToken(semiORcurly + 2);
	                    ((sMethod)sMethod_List.getsMethodAt(currentMethod)).setEndToken(-1);
	                    ((sMethod)sMethod_List.getsMethodAt(currentMethod)).setFileID(0);
	
	                    //store method info in files too
	                    ((sFile)sFile_List.getsFileAt(0)).addvMethod(currentMethod);	                  
	                    nToken++;
	                   
	                    //to get the actual token info for {
	                    l = rT.getLine();
	                    c = rT.getCharPositionInLine();
	                }
	                //insert the TRANSFORMED token in token vector	             
	                StoreTokenInfo(type,l, c, 0, currentClass, currentMethod,rT.getText());
	                nToken++;
	                
	                if (jcs == JAVACLASSSTATE.JCS_END) {
	                   // StoreTokenInfo(++nUniqueSentinel,l, c, 0, currentClass, -1,"ENDCLASS");	                   
	                    currentClass = -1;
	                    semiORcurly = nToken;
	                    nToken++;
	                }
	                if (jms == JAVAMETHODSTATE.JMS_END) {
	                	((sMethod)sMethod_List.getsMethodAt(currentMethod)).setEndToken(nToken - 1);
	                
	                    //if nUniqueSentinel: 
	                    //insert special token marking method end
	                   StoreTokenInfo(bUniqueMethods ? ++nUniqueSentinel : methodBoundaryMarker++,l, c, 0,currentClass, currentMethod,"ENDMETHOD");
	                    	                    
	                    currentMethod = -1;
	                    semiORcurly = nToken;
	                    nToken++;
	                }
	            } 
	        }   
	        //put end of file sentinel in input vector
        //    StoreTokenInfo(++nUniqueSentinel,l, c, 0, -1, -1,"ENDFILE " + sFile_List.getsFileAt(0).getFullPath());
         //   sFile_List.getsFileAt(0).setEndToken(nToken);
         //   nToken++;	                  
    	
       // this.equateTokens();        
    }	
}
