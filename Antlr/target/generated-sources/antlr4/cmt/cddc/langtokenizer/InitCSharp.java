package cmt.cddc.langtokenizer;
import java.util.ArrayList;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.Token;
import antlr.auto.gen.csharp.*;

public class InitCSharp
{
    
    private CSharpLexer lexer;
    //private List<Tokens> arr = new ArrayList<Tokens>();
    private ArrayList<Integer> arr = new ArrayList<Integer>();

    public void createLexerArr(ANTLRInputStream input)
    {
        lexer = new CSharpLexer(input);
        Token t =  lexer.nextToken();
        while(!(t.getText().equals("<EOF>")))
        {
            //Tokens s = new Tokens();
            //s.setText(t.getText());
            //s.setType(t.getType());
            arr.add(t.getType());  
            t = lexer.nextToken();

        }
    }
    
    public ArrayList<Integer> getArr()
    {
        return arr;
    }
    
    public void setArr(ArrayList<Integer> arr)
    {
        this.arr = arr;
    }
    

}
