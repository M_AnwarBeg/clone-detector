package cmt.cddc.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.structures.sFile_List;
import cmt.cddc.structures.sMethod_List;

public final class CommonUtility {
	public static ArrayList<String> readFile(String filePath)
	{
		ArrayList<String> output = new ArrayList<String>();
		 try 
	     {
			 File file = new File(filePath);
			 FileReader fileReader = new FileReader(file);
			 BufferedReader bufferedReader = new BufferedReader(fileReader);			 
			 String line;
			 while ((line = bufferedReader.readLine()) != null) 
			 {
				 output.add(line);				
			}
			fileReader.close();	
	     }
	     catch(IOException e) 
	     {
		     // TODO Auto-generated catch block
			 e.printStackTrace();
	     }
		 return output;
	}
	
	public static List<Integer> getFilesContainingStructure(ArrayList<SimpleClone> simpleClones , ArrayList<Integer> vCloneClasses)
	{
		//take the first clone class
        int CC = vCloneClasses.get(0) / 100;
        
		//find all files that have this clone class
        //use list for ease of deletion in middle
        List<Integer> ITempFiles = new ArrayList<Integer>();
       
        for (int k = 0; k < simpleClones.get(CC).getInstancesSize() ; k++) {
        	ITempFiles.add(simpleClones.get(CC).getInstanceAt(k).getFileId());			            
        }
        
        //remove adjacent same file IDs
	    Set<Integer> hs_files = new HashSet<Integer>(ITempFiles);
	    ITempFiles.clear();	
	    ITempFiles.addAll(hs_files);
	    
	    //sort the files
	    Collections.sort(ITempFiles);

	    //for each file in temporary list
	    for (int id = 0 ; id < ITempFiles.size() ; id++) 
    	{
            int holderID = ITempFiles.get(id);
            //if the file does not have all members of this structure
            
            if(!(sFile_List.getsFileAt(holderID).getCloneClasses().containsAll(vCloneClasses)))
            {
            	ITempFiles.remove(id);  //remove this file from temp list
            	id--;
            }
    	}
	    return ITempFiles;
	}
	
	public static ArrayList<Integer> convertStrArrListToInt(ArrayList<String> strArr , boolean transformIds)
	{
		ArrayList<Integer> intArr = new ArrayList<Integer>();
		for(String str : strArr)
		{
			if(str.isEmpty() == false && str != null && str != "")
			{
				if(transformIds)
					intArr.add(Integer.parseInt(str) / 100);   //converting transformed scc Ids to original scc ids... 
				else
					intArr.add(Integer.parseInt(str));
				
			}
		}
		return intArr;
	}
}
