package cmt.cddc.computeallruns;


public class Repetition {

	 public char type ;
	 public int period ;
	 public int initpos ;
	 public int endpos ;
	 
//	 public Repetition(char type, int period, int initpos, int endpos){
//		 this.type = type;
//		 this.period = period;
//		 this.initpos = initpos;
//		 this.endpos = endpos;
 //   }
	 	 
	public char getType() {
		return type;
	}
	public void setType(char type) {
		this.type = type;
	}
	public int getPeriod() {
		return period;
	}
	public void setPeriod(int period) {
		this.period = period;
	}
	public int getInitpos() {
		return initpos;
	}
	public void setInitpos(int initpos) {
		this.initpos = initpos;
	}
	public int getEndpos() {
		return endpos;
	}
	public void setEndpos(int endpos) {
		this.endpos = endpos;
	}
	
}
