package cmt.cddc.computeallruns;

import cmt.cddc.clonedetectioninitializer.SysConfig;
import cmt.cddc.langtokenizer.InitJava;
import cmt.cddc.psy.PSY_clones;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.TokensList;
import cmt.cddc.structures.sFile_List;

public class ComputeAllRuns 
{
	public boolean isRepeatRepetitious(PSY_clones pRepeat, float RNR , int [] suffixArray , TokensList token_list) 
	{
	    boolean bRepetitious = false;
	    int s = pRepeat.left_boundry;
	    int startToken = suffixArray[s];
	    int endToken = startToken + pRepeat.length - 1;
	    //see if it contains repetitions
	    int nRepetition = 0;
	    int totalRepetition = 0;
	    int maxRepetition = 0;
	    for (int r = startToken; r <= endToken; r++) {
	        if (token_list.getTokenAt(r).getTokenRepetition()) {  //vRepetitions[r]
	            nRepetition++;
	            totalRepetition++;
	        } else {
	            maxRepetition = nRepetition;
	            nRepetition = 0;
	        }
	    }
	    if (nRepetition > maxRepetition) maxRepetition = nRepetition;
	    //killing repeat based on the percentage of repetitions
	    //just like CCFinder...RNR...but we only compute for one clone instance
	    /* :)RNR (ratio of non-repeated tokens):
	     Ratio (percentage) of tokens that are not included in repeated part 
	    of a code fragment of the code clone. */
	    RNR = (float) (1.0 - ((float) totalRepetition / (float) pRepeat.length));
	    if (RNR <=  SysConfig.getRNRThreshold()) { //0.5) {
	        bRepetitious = true;
	    }        //todo: maybe i should remove/change this check??
	    else //if(totalRepetition > 1)
	    {
	        //cout<<endl<<"clone class ID:"<<pCloneClass->ID<<endl;
	        //TODO: this method should only be called for first clone instance
	    		bRepetitious = MarkRunsInRepeat(pRepeat, RNR, suffixArray , token_list);
	    }
	    return bRepetitious;
	}

	public boolean isRepeatRepetitious(SimpleClone clone , TokensList token_list)
	{
		boolean bRepetitious = false;
		float RNR = 0;
		if(clone != null && clone.getInstancesSize() > 0)
		{
		    int startToken = clone.getInstanceAt(0).getStartingIndex();
		    int endToken = clone.getInstanceAt(0).getEndingIndex();
		    //see if it contains repetitions
		    int nRepetition = 0;
		    int totalRepetition = 0;
		    int maxRepetition = 0;
		    
		    if(startToken >= 0 && endToken >= 0)
		    {
			    for (int r = startToken; r <= endToken; r++) {
			        if (token_list.getTokenAt(r).getTokenRepetition()) {  //vRepetitions[r]
			            nRepetition++;
			            totalRepetition++;
			        } else {
			            maxRepetition = nRepetition;
			            nRepetition = 0;
			        }
			    }
			    if (nRepetition > maxRepetition) maxRepetition = nRepetition;
			    //killing repeat based on the percentage of repetitions
			    //just like CCFinder...RNR...but we only compute for one clone instance
			    /* :)RNR (ratio of non-repeated tokens):
			     Ratio (percentage) of tokens that are not included in repeated part 
			    of a code fragment of the code clone. */
			    RNR = (float) (1.0 - ((float) totalRepetition / (float) (endToken - startToken + 1)));
			    if (RNR <=  SysConfig.getRNRThreshold()) { //0.5) {
			        bRepetitious = true;
			    }        //todo: maybe i should remove/change this check??
			    else //if(totalRepetition > 1)
			    {
			        //cout<<endl<<"clone class ID:"<<pCloneClass->ID<<endl;
			        //TODO: this method should only be called for first clone instance
			    	
			    		bRepetitious = MarkRunsInRepeat(clone, token_list);
			    }
		    }
		}
		return bRepetitious;		
	}
	
	private boolean MarkRunsInRepeat(PSY_clones pRepeat, float RNR , int [] suffixArray , TokensList token_list) 
	{
		 	boolean bRepetitious = false;
		    //no need to recompute runs in the part that is already marked
		    //static vector<bool> vbTokensAnalyzed(vRepetitions);
		    int s = pRepeat.left_boundry;
		    int startToken = suffixArray[s];
		    int length = pRepeat.length;
		    int endToken = startToken + length - 1;
		   
		    int [] arr = new int[length];
		    for(int i = 0 ; i < length ; i++)
		    {
		    	arr[i] = token_list.getTokenAt(i+startToken).getType();
		    }
		    	AllRuns ar = new AllRuns();
		    	Repetition[] allRuns = ar.getRuns(arr);
			    for (Repetition run : allRuns) 
			    {
			        int repLen = run.getEndpos() - run.getInitpos();
			        int repPeriod = run.getPeriod();
			        float repExp = ((float) repLen) / repPeriod;
	
			        if (repExp >= 2.0 && repPeriod < SysConfig.getThreshold()) {
			            int repStart = run.getInitpos();
			            int repEnd = run.getEndpos();
	
			            //mark these runs in all clone instances of this clone class
			            for (int j = pRepeat.left_boundry; j <= pRepeat.right_boundry; j++) {
			                int start = suffixArray[j];
			                for (int k = 0; k < repLen; k++) {
			                	token_list.getTokenAt(k + start + repStart).setTokenRepetition(true);		                	
			                }
			            }
			        }		       
			    }
	
			    int nRepetition = 0;
			    int totalRepetition = 0;
			    int maxRepetition = 0;
			    for (int r = startToken; r <= endToken; r++) {
			        if (token_list.getTokenAt(r).getTokenRepetition()) {
			            nRepetition++;
			            totalRepetition++;
			        } else {
			            maxRepetition = nRepetition;
			            nRepetition = 0;
			        }
			    }
			    if (nRepetition > maxRepetition) maxRepetition = nRepetition;
			    //killing repeat based on the percentage of repetitions
			    RNR = (float) (1.0 - ((float) totalRepetition / (float) length));
			    if (RNR <= SysConfig.getRNRThreshold()) { //0.5) {
			        bRepetitious = true;
			    }			    
		    return bRepetitious;
	}
	
	private boolean MarkRunsInRepeat(SimpleClone clone , TokensList token_list) 
	{
		boolean bRepetitious = false;
	    //no need to recompute runs in the part that is already marked
	    //static vector<bool> vbTokensAnalyzed(vRepetitions);
				
		int startToken = clone.getInstanceAt(0).getStartingIndex();
		int endToken = clone.getInstanceAt(0).getEndingIndex();
	    int length = endToken - startToken + 1;
	    
		int [] arr = new int[length];
	    for(int i = 0 ; i < length ; i++)
	    {
	    	arr[i] = token_list.getTokenAt(i+startToken).getType();
	    }
	    
	    AllRuns ar = new AllRuns();
    	Repetition[] allRuns = ar.getRuns(arr);
	    for (Repetition run : allRuns) 
	    {
	        int repLen = run.getEndpos() - run.getInitpos();
	        int repPeriod = run.getPeriod();
	        float repExp = ((float) repLen) / repPeriod;

	        if (repPeriod > 1 && repExp >= 2.0 && repPeriod < SysConfig.getThreshold()) 
	        {
	            int repStart = run.getInitpos();
	            int repEnd = run.getEndpos();

	          //mark these runs in all clone instances of this clone class
	            for (int j = 0; j < clone.getInstancesSize() ; j++) 
	            {
	            	if(!InitJava.firstrun && !sFile_List.getFile(clone.getInstanceAt(j).getFileId()).isNewlyAddedFile() && !sFile_List.getFile(clone.getInstanceAt(j).getFileId()).getUpdated())
	            		continue;
	            	
	            	int start = clone.getInstanceAt(j).getStartingIndex();
	                for (int k = 0; k < repLen; k++) {
	                	token_list.getTokenAt(k + start + repStart).setTokenRepetition(true);		                	
	                }     	            	
	            }	         
	        }	           
	    }		       
	    int nRepetition = 0;
	    int totalRepetition = 0;
	    int maxRepetition = 0;
	    for (int r = startToken; r <= endToken; r++) {
	        if (token_list.getTokenAt(r).getTokenRepetition()) {
	            nRepetition++;
	            totalRepetition++;
	        } else {
	            maxRepetition = nRepetition;
	            nRepetition = 0;
	        }
	    }
	    if (nRepetition > maxRepetition) maxRepetition = nRepetition;
	    //killing repeat based on the percentage of repetitions
	    float RNR = (float) (1.0 - ((float) totalRepetition / (float) length));
	    if (RNR <= SysConfig.getRNRThreshold()) { //0.5) {
	        bRepetitious = true;
	    }		
		return bRepetitious;		
	}
}