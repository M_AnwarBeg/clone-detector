package cmt.cddc.computeallruns;

import cmt.common.Directories;

public class AllRuns {
	static {
			//System.loadLibrary("computeAllRuns");		
			//System.load("C:\\Users\\cs\\Documents\\NetBeansProjects\\ComputeAllRuns\\dist\\Debug\\MinGW64-Windows\\libComputeAllRuns.dll");
		    System.load(Directories.getAbsolutePath(Directories.COMPUTE_ALL_RUNS_LIB));
	}
		
    //public native static Repetition[] getmReps(String inputSeq, int inputLength);
    public native static Repetition[] getmReps(int[] inputSeq);
    
    public Repetition[] getRuns(int[] inputSeq){
    	Repetition[] repetitions  = null;
    	try 
    	{    	
	    	 repetitions = getmReps(inputSeq);	    	 
    	}
    	catch(Exception e) {
    		
    		System.out.println(e.getMessage());
    		
    		    	}
    /*	for(Repetition repitition :repetitions){
         	System.out.println("Type: "+repitition.type);
             System.out.println("Period:"+repitition.period);
             System.out.println("InitPos:"+repitition.initpos);
             System.out.println("EndPos:"+repitition.endpos);
         } */
        return repetitions;
	}
    
	/*public static void main(String[] args) 
	{	
		String inputSeq = "gcctatttatttatttggt";
		int inputLength = inputSeq.length();
		AllRuns obj = new AllRuns();
		Repetition[] reps = obj.getRuns(inputSeq,inputLength);		
	}*/
}
