package cmt.common;
import java.net.URL;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.osgi.framework.Bundle;
import handlers.CDDCplugin;

public class Directories {
	public static final String COMPUTE_ALL_RUNS_LIB = "\\Resources\\libComputeAllRuns.dll";
	public static final String CLONES_INPUT_FILES = "\\CloneMiner\\input\\InputFiles.txt";
	public static final String CLONES_INPUT_FILES_GROUP = "\\CloneMiner\\input\\InputFilesGroups.txt";
    public static final String CLONES_INPUT_SUPPRESSED = "\\CloneMiner\\input\\Suppressed.txt";
    public static final String CLONES_INPUT_EQUALTOKEN = "\\CloneMiner\\input\\EqualTokens.txt";
    public static final String CLONES_INPUT_CLUSTER_PTRS = "\\CloneMiner\\input\\ClusterParameters.txt";
    public static final String CLONES_OUTPUT = "\\CloneMiner\\output\\Clones.txt";
   // public static final String CLONESFILE = "\\CloneMiner\\output\\Clones.txt";
    
    public static final String CLONES_CLUSTER = "\\CloneMiner\\output\\FileClusters.txt";
    public static final String METHOD_STRUCTURE = "\\CloneMiner\\output\\MethodStructures.txt";
    public static final String FILE_STRUCTURES = "\\CloneMiner\\output\\FileStructures.txt";
    public static final String DIR_STRUCTURES = "\\CloneMiner\\output\\DirStructures.txt";
    public static final String GROUP_STRUCTURES = "\\CloneMiner\\output\\GroupStructures.txt";
    public static final String IN_FILE_STRUCTURE = "\\CloneMiner\\output\\InfileStructures.txt";
    public static final String IN_METHOD_STRUCTURE = "\\CloneMiner\\output\\InMethodStructures.txt";
    public static final String CLONES_RNR = "\\CloneMiner\\output\\clonesRNR.txt";
    public static final String CLONESFILE = "\\CloneMiner\\output\\Clones.txt";
    public static final String CLONES_BY_FILE ="\\CloneMiner\\output\\clonesByFile.txt";
    public static final String CLONES_BY_FILE_NORMAL ="\\CloneMiner\\output\\clonesByFileNormal.txt";
    public static final String CLONES_METHOD = "\\CloneMiner\\output\\ClonesByMethod.txt";
    public static final String CLONES_METHOD_NORMAL = "\\CloneMiner\\output\\ClonesByMethodNormal.txt";
    public static final String COMBINEDTOKENSFILE = "\\CloneMiner\\output\\CombinedTokens.txt";
    public static final String DIRSINFOFILE = "\\CloneMiner\\output\\DirsInfo.txt";
    //public static final String CLONES_CLUSTER = "\\CloneMiner\\output\\FileClusters.txt";
    public static final String FILE_CLUSTER = "\\CloneMiner\\output\\FileClusters.txt";
    public static final String FILE_CLUSTER_XX = "\\CloneMiner\\output\\FileClustersXX.txt";
    public static final String METHOD_CLUSTER = "\\CloneMiner\\output\\MethodClusters.txt";
    public static final String METHOD_CLUSTER_XX = "\\CloneMiner\\output\\MethodClustersXX.txt";
    public static final String METHOD_CLONES_BY_FILE = "\\CloneMiner\\output\\MethodClonesByFiles.txt";
    public static final String METHOD_CLONES_BY_FILE_NORMAL = "\\CloneMiner\\output\\MethodClonesByFilesNormal.txt";
    public static final String CROSS_METHOD_CLONE_STURCTURES ="\\CloneMiner\\output\\CrossMethodCloneStructures.txt";
    public static final String CROSS_FILE_METHOD_STRUCTURE_EX = "\\CloneMiner\\output\\CrossFileCloneMethodStructuresEx.txt";
    public static final String CROSS_FILE_METHOD_STRUCTURE = "\\CloneMiner\\output\\CrossFileCloneMethodStructures.txt";
    public static final String CROSS_FILE_CLONE_STRUCTURE = "\\CloneMiner\\output\\CrossFileCloneStructures.txt";
    public static final String FILE_INFO = "\\CloneMiner\\output\\FilesInfo.txt";
    public static final String FILE_STRUCTURE_CROSS_DIR_EX = "\\CloneMiner\\output\\CrossDirsCloneFileStructuresEx.txt";
    public static final String FILE_STRUCTURE_CROSS_DIR = "\\CloneMiner\\output\\CrossDirsCloneFileStructures.txt";
    public static final String IN_DIR_STRUCTURE = "\\CloneMiner\\output\\InDirsCloneFileStructures.txt";
    public static final String FILE_CLONES_BY_DIR = "\\CloneMiner\\output\\FileClonesByDirs.txt";
    public static final String FILE_CLONES_BY_DIR_NORMAL = "\\CloneMiner\\output\\FileClonesByDirsNormal.txt";
    public static final String FILE_STRUCTURE_CROSS_GROUP_EX = "\\CloneMiner\\output\\CrossGroupsCloneFileStructuresEx.txt";
    public static final String FILE_STRUCTURE_CROSS_GROUP = "\\CloneMiner\\output\\CrossGroupsCloneFileStructures.txt";
    public static final String IN_GROUP_STRUCTURE = "\\CloneMiner\\output\\InGroupCloneFileStructures.txt";
    public static final String FILE_CLONES_BY_GROUP = "\\CloneMiner\\output\\FileClonesByGroups.txt";
    public static final String FILE_CLONES_BY_GROUP_NORMAL = "\\CloneMiner\\output\\FileClonesByGroupsNormal.txt";
    public static final String INPUTFILE = "CloneMiner\\input\\InputFiles.txt";
    public static final String CHECKFILE = "\\check\\check.txt";
    public static final String FILESINFOFILE = "\\CloneMiner\\output\\FilesInfo.txt";
    public static final String CLONE_MINER = "\\CloneMiner\\clones.exe";
    public static final String CLONE_MINER_WM = "\\CloneMiner";
    public static final String CLONE_MINER_DIR = "\\CloneMiner";
    public static final String ART_PATH = "\\ART_Output\\ART_1.1.4.bat";
    public static final String ART_PATH_DIR = "\\ART_Output";
    public static final String METHODSINFOFILE = "\\CloneMiner\\output\\MethodsInfo.txt";
    public static final String CLONESBYMETHODFILE = "\\CloneMiner\\output\\ClonesByMethod.txt";
    public static final String CLONESBYMETHODFILENORMAL = "\\CloneMiner\\output\\ClonesByMethodNormal.txt";
    public static final String METHODCLUSTERSXX = "\\CloneMiner\\output\\MethodClustersXX.txt";
    public static final String TOKEN_JAVA = "\\target\\generated-sources\\antlr4\\antlr\\auto\\gen\\java\\JavaLexer.tokens";
    public static final String TOKEN_CPP = "target\\generated-sources\\antlr4\\antlr\\auto\\gen\\c\\CLexer.tokens";
    public static final String TOKEN_CSHARP = "target\\generated-sources\\antlr4\\antlr\\auto\\gen\\csharp\\csharpLexer.tokens";
    public static final String TOKEN_RUBBY = "\\Resources\\RubyTokenTypes.txt";
    public static final String TOKEN_PHP = "\\target\\generated-sources\\antlr4\\antlr\\auto\\gen\\php\\PHPParserLexer.tokens";
    public static final String TOKEN_TXT = "\\Resources\\TXTLexerTokenTypes.txt";
    public static final String REPETITIONS = "\\CloneMiner\\output\\Repetitions.txt";
    public static final String DATABASE = "\\CloneRepository.sql";
    public static final String FRAME_DATABASE = "\\FrameRepository.sql";
    public static final String LOAD_LIST = "\\icons\\list.png";
    private static final Bundle myBundle = CDDCplugin.getDefault().getBundle();
	public static final String FramingBaseLine = "\\Resouces\\FramingBaseLine";
	public static final String SccFrames = "\\ART_Output\\SCC_Frames\\";
	public static final String framingOutput = "\\ART_Output\\Output\\";
	public static final String OUTPUT_PATH_mcsTemplate = "\\ART_Output\\MCS_Template\\";
	public static final String OUTPUT_PATH_mccTemplate = "\\ART_Output\\MCC_Template\\";
	public static final String OUTPUT_PATH_mcc = "\\ART_Output\\";
	public static final String ART_OUTPUT_PATH = "\\ART_Output\\Output";
	public static final String OUTPUT_PATH_fccTemplate = "\\ART_Output\\FCC_Template\\";
	public static final String OUTPUT_PATH_mcsSPC = "\\ART_Output\\MCS_SPC\\";
	public static final String OUTPUT_PATH_mccSPC = "\\ART_Output\\MCC_SPC\\";
	public static final String OUTPUT_PATH_fccSPC = "\\ART_Output\\FCC_SPC\\";
    
    
    //Deployment Version
    /*public static String getAbsolutePath(String fp)
    {
    	String eclipseExecutablePath = System.getProperty("eclipse.launcher");
    	int index = eclipseExecutablePath.lastIndexOf("\\");
    	String absolutePath = eclipseExecutablePath.substring(0,index);    	
    	absolutePath =  absolutePath +"\\cmt\\" + fp ;
    	return absolutePath;
		
    }*/
     
    
    //Development Version
   public static String getAbsolutePath(String fp)
    {
		String pathStr = null;
		try
		{		
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			URL url = Directories.class.getProtectionDomain().getCodeSource()
		            .getLocation();
		//    System.out.println(url);
		    /*IPath path = new Path(FileLocator.toFileURL(myBundle.getEntry(fp)).getPath());*/
			//IPath path = newIPath();
		    pathStr = url.getPath()+fp;
		   // System.out.println(pathStr);	
		} 
		catch (Exception e)
		{
		    e.printStackTrace();
		    System.err.println(e.getMessage());
		}	
		return pathStr;
    	
    	
    }
    
}
