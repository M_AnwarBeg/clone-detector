package cmt.common;

import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.mylyn.commons.ui.dialogs.AbstractNotificationPopup;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseTrackAdapter;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;

public class NotificationPopUpUI extends AbstractNotificationPopup {

	
	private String message = "";
	public NotificationPopUpUI(Display display) {
		super(display);
		// TODO Auto-generated constructor stub
	}
	 @Override
	  protected void createContentArea(Composite composite)
	  {
	    composite.setLayout(new GridLayout(1, true));
	    
	    Label lblMessage = new Label(composite, 0);
	    lblMessage.setText(this.message); 
	    
	  }
	 
	 public void setMessage(String message) {
		 this.message = message;
		 
	 }  
	 
	 
	  @Override
	  protected String getPopupShellTitle()
	  {
	    return "CMT Notification";
	  }
	  
	  @Override
	  protected Image getPopupShellImage(int maximumHeight)
	  {
	    return null;
	  }

}
