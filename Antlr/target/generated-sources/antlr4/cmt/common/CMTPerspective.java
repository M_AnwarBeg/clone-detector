package cmt.common;

import org.eclipse.ui.IFolderLayout; 
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

public class CMTPerspective implements IPerspectiveFactory {
    public void createInitialLayout(IPageLayout layout) {
    String editorArea = layout.getEditorArea();
     //   layout.addView("cmt.cvac.views.SccView", IPageLayout.LEFT, 0.2f, IPageLayout.ID_EDITOR_AREA);        

		IFolderLayout clonesFolder = layout.createFolder("clones",
				IPageLayout.BOTTOM, 0.6f, editorArea);
		
		clonesFolder.addView("cmt.cvac.views.SccView");
		clonesFolder.addView("cmt.cvac.views.SccView");
		clonesFolder.addView("cmt.cvac.views.SccView");
		clonesFolder.addView("cmt.cvac.views.SccView");

    }
}
 