package cmt.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import com.google.googlejavaformat.java.*;

public class Indentation {
	
	/*public static int SPACES = 0;*/
	
	public static void indentFccOutput (String fileN) throws Exception{
		File f  = new File(fileN);
    	FileInputStream filein11 = new FileInputStream(f);
		BufferedReader stdin11 = new BufferedReader(new InputStreamReader(filein11));
		String code = "";
		String line = "";
		
		while ((line = stdin11.readLine()) != null)
		{
			code += line;
		}
		f.delete();
		
		// Google Java format //
		    		Formatter formatter = new Formatter();
		    	      try {
		    	        code = formatter.formatSource(code);
		    	        System.out.println(code);
		    	        
		    	        File f2  = new File(fileN);
		    	        PrintWriter writer_file = new PrintWriter(f2, "UTF-8");
		    	        writer_file.print(code);
		    	        writer_file.close();
		    	      } 
		    	      catch (Exception e) {
		    	    	MessageConsole myConsole = new MessageConsole("My Console", null);
		    	  	    ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ myConsole });
		    	  	    ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
		    	  	    final MessageConsoleStream stream = myConsole.newMessageStream();
		    	  	    stream.setColor(new Color(Display.getDefault(), new RGB(255, 0, 0)));
		    	  	    stream.setActivateOnWrite(true);
		    	  	    stream.println("Syntax of output files is not correct.");
		    	        System.out.println("Syntax of output files is not correct.");
		    	    }
		    	    
		    	// End //
		
	}
    public static void indentMccOutput (String fileN) throws Exception {

    	File f  = new File(fileN);
    	FileInputStream filein11 = new FileInputStream(f);
		BufferedReader stdin11 = new BufferedReader(new InputStreamReader(filein11));
		String code = "";
		String line = "";
		
		while ((line = stdin11.readLine()) != null)
		{
			code += line;
		}
		f.delete();
		
		// Eclipse default indentation //
		
			/*CodeFormatter codeFormatter = ToolFactory.createCodeFormatter(null);
    		
			code = "public class abc{"+code+"}";
    		IDocument doc = new Document(code);
    		TextEdit textEdit = codeFormatter.format(CodeFormatter.K_COMPILATION_UNIT, doc.get(), 0, doc.get().length(), 0, null);
    		try {
    			textEdit.apply(doc);
    			System.out.println(doc.get());
    		} 
    		catch (Exception e) 
    		{
    			e.printStackTrace();
    		}*/
		
    	// End //	
		
		// Google Java format //
		
		code = "public class abc {"+code+"}";
    		Formatter formatter = new Formatter();
    	      try {
    	        code = formatter.formatSource(code);
    	        
    	        String [] subCode = code.split("public class abc \\{");
    	        code = subCode[1].substring(0, subCode[1].length()-2);
    	        System.out.println(code);
    	        
    	        File f2  = new File(fileN);
    	        PrintWriter writer_file = new PrintWriter(f2, "UTF-8");
    	        writer_file.print(code);
    	        writer_file.close();
    	      } 
    	      catch (Exception e) {
    	    	  	MessageConsole myConsole = new MessageConsole("My Console", null);
	    	  	    ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ myConsole });
	    	  	    ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
	    	  	    final MessageConsoleStream stream = myConsole.newMessageStream();
	    	  	    stream.setColor(new Color(Display.getDefault(), new RGB(255, 0, 0)));
	    	  	    stream.setActivateOnWrite(true);
	    	  	    stream.println("Syntax of output files is not correct.");
    	        System.out.println("Syntax of output files is not correct.");
    	    }
    	    
    	// End //
    }
}
