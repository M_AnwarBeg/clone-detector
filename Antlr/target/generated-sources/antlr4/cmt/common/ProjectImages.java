package cmt.common;



public class ProjectImages {
	
	public static final String CLONE_DETECTION = "\\icons\\clonedetection24px.png";
	public static final String CLONES_FRAMING = "\\icons\\Framing.png";
    public static final String CLONES_FRAMEDELETION = "\\icons\\deleteframe.png";
    public static final String CLONES_FRAMEEDIT = "\\icons\\editframe.png";
    public static final String CLONES_TREEMAPVISUALIZATION = "\\icons\\treemap.png";
    public static final String CLONES_FILTERATION = "\\icons\\filter.png";
    public static final String CLONES_UNFILTERATION = "\\icons\\unfilter.png";
    public static final String DELETE_FRAME = "\\icons\\deleteframe.png";
    public static final String CLONE_COMPARISON = "\\icons\\compare.png";
    public static final String GO_TO_FILE_LOCATION = "\\icons\\goto.png";
    public static final String UPDATE_CLONES = "\\icons\\updateclone24px.png";

    public static final String JAVA_ECLIPSE = "\\icons\\javaeclipse.png";

    public static final String FOLDER = "\\icons\\folder.png";

    
	
    
}
