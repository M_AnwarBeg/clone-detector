package cmt.cfc.incremental;

import java.io.File;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cfc.fccframe.FCCTemplate;
import cmt.cfc.mccframe.MCCTemplate;
import cmt.cfc.mccframe.MCSTemplate;
import cmt.cfc.utility.FileHandler;
import cmt.cvac.views.FccView;
import cmt.cvac.views.MccView;
import cmt.cvac.views.McsView;

public class IncrementalNotificationUI extends Dialog {

	protected Object result;
	protected Shell shell;
	private Table unsyncClasses;
	private Table unsyncInstances;
	private static int selectedId;
	private static int selectedInstId;
	private static int selectedIndex;
	private static boolean isSelectionMcc;
	private static boolean isSelectionMcs;

	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public IncrementalNotificationUI(Shell parent, int style) {
		super(parent, style);
		setText("SWT Dialog");
	}

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		shell.layout();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		
		shell = new Shell( SWT.CLOSE | SWT.TITLE);
		shell.setText("Sync Clones");

		//shell = new Shell(getParent(), getStyle());
		shell.setSize(450, 300);
		shell.setText(getText());
		
		List listUnsyncInstances = new List(shell, SWT.BORDER);
		listUnsyncInstances.setBounds(254, 56, 111, 127);
		
		List listUnsyncClasses = new List(shell, SWT.BORDER);
		listUnsyncClasses.setBounds(63, 56, 111, 127);
		
		for(int i=0; i<FrameSynchronizer.unsyncMccs.size(); i++) {
			listUnsyncClasses.add("MCC-"+FrameSynchronizer.unsyncMccs.get(i));
		}
		
		for(int i=0; i<FrameSynchronizer.unsyncFccs.size(); i++) {
			listUnsyncClasses.add("FCC-"+FrameSynchronizer.unsyncFccs.get(i));
		}
		
		for(int i=0; i<FrameSynchronizer.unsyncMcs.size(); i++) {
			listUnsyncClasses.add("MCS-"+FrameSynchronizer.unsyncMcs.get(i));
		}
		
		Button View = new Button(shell, SWT.NONE);
		View.setBounds(59, 219, 75, 25);
		View.setText("View");
		View.setEnabled(false);
		
		View.addListener(SWT.MouseDown, new Listener() {
			public void handleEvent(Event e) {
				if(isSelectionMcc) {
					openMccInstance(selectedId, selectedInstId);
				}
				else if(isSelectionMcs) {
					openMcsInstance(selectedId, selectedInstId);
				}
				else {
					openFccInstance(selectedId, selectedInstId);
				}
			}
		});
		
		Button View_All = new Button(shell, SWT.NONE);
		View_All.setBounds(290, 219, 75, 25);
		View_All.setText("View All");
		View_All.setEnabled(false);
		
		View_All.addListener(SWT.MouseDown, new Listener() {
			public void handleEvent(Event e) {
				if(isSelectionMcc) {
					openMcc(selectedId);
				}
				else if(isSelectionMcs) {
					openMcs(selectedId);
				}
				else {
					openFcc(selectedId);
				}
			}
		});
		
		Button Sync = new Button(shell, SWT.NONE);
		Sync.setBounds(175, 219, 75, 25);
		Sync.setText("Sync");
		Sync.setEnabled(false);
		
		Sync.addListener(SWT.MouseDown, new Listener() {
			public void handleEvent(Event e) {
				try
				{
					int result = -1;
					if(isSelectionMcc) {
						result = MccView.deleteMCC(selectedId);
						MCCTemplate.genMCCTemplate(selectedId);
						listUnsyncClasses.remove(selectedIndex);
					}
					
					// come here after deletion of MCS
					else if(isSelectionMcs) {
						result = McsView.deleteMCS(selectedId);
						MCSTemplate.generateTemplate(selectedId);
						listUnsyncClasses.remove(selectedIndex);
					}
					else {
						result = FccView.deleteFCC(selectedId);
						FCCTemplate.genFCCTemplate(selectedId);
						listUnsyncClasses.remove(selectedIndex);
					}
					
					listUnsyncInstances.removeAll();
					
					if(listUnsyncClasses.getItemCount() == 0)
					{
						View.setEnabled(false);
						View_All.setEnabled(false);
						Sync.setEnabled(false);
					}
				}
				catch(Exception i)
				{
					i.printStackTrace();
				}
			}
		});
		
		listUnsyncClasses.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				
				
				// TODO Auto-generated method stub
				selectedIndex = listUnsyncClasses.getSelectionIndex();
				String selection = listUnsyncClasses.getItem(listUnsyncClasses.getSelectionIndex());
				String[] idStr = selection.split("-");
				
				listUnsyncInstances.removeAll();
				
				if(idStr[0].equals("MCC"))
				{
					isSelectionMcc = true;
					ArrayList<String> mccInstances = getMccInstances(Integer.parseInt(idStr[1]));
					selectedId = Integer.parseInt(idStr[1]);
					
					for(int k=0; k<mccInstances.size(); k++)
					{
						listUnsyncInstances.add(mccInstances.get(k));
					}
				}
				else if(idStr[0].equals("FCC"))
				{
					isSelectionMcc = false;
					ArrayList<String> fccInstances = getFccInstances(Integer.parseInt(idStr[1]));
					selectedId = Integer.parseInt(idStr[1]);
					
					for(int k=0; k<fccInstances.size(); k++)
					{
						listUnsyncInstances.add(fccInstances.get(k));
					}
				}
				else if(idStr[0].equals("MCS"))
				{
					isSelectionMcc = false;
					isSelectionMcs = true;
					ArrayList<String> mcsInstances = getMcsInstances(Integer.parseInt(idStr[1]));
					selectedId = Integer.parseInt(idStr[1]);
					
					for(int k=0; k<mcsInstances.size(); k++)
					{
						listUnsyncInstances.add(mcsInstances.get(k));
					}
				}
				Sync.setEnabled(true);
				View.setEnabled(false);
				View_All.setEnabled(true);

						
			}
		});
		
		listUnsyncInstances.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				
				
				// TODO Auto-generated method stub
				Sync.setEnabled(false);
				View.setEnabled(true);
				View_All.setEnabled(false);
				selectedInstId = listUnsyncInstances.getSelectionIndex();
						
			}
		});
 
	}
	public static void openMcs(int mcsId)
	{
		try
		{
			for(int i=0; i<MethodClonesReader.getMcsList().get(mcsId).getCloneList().size(); i++)
			{
				File file = new File(ProjectInfo.getInputFile(MethodClonesReader.getMcsList().get(mcsId).getCloneList().get(i).getFId()));
		 
			    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
			    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			    IDE.openEditorOnFileStore(page,fileStore);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void openFcc(int fccId)
	{
		try
		{
			
			for(int i=0; i<ClonesReader.getFileCloneList().get(fccId).getFCCInstance().size(); i++)
			{
				File file = new File(ClonesReader.getFileCloneList().get(fccId).getFCCInstance().get(i).getFile().getFullPath());
		 
			    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
			    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			    IDE.openEditorOnFileStore(page,fileStore);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void openMcc(int mccId)
	{
		try
		{
			int index=-1;
			for(int j=0; j<ClonesReader.getMethodClones().size(); j++)
			{
				if(ClonesReader.getMethodClones().get(j).getClusterID() == mccId)
				{
					index=j;
				}
			}
			for(int i=0; i<ClonesReader.getMethodClones().get(index).getMCCInstances().size(); i++)
			{
				File file = new File(ClonesReader.getMethodClones().get(index).getMCCInstances().get(i).getMethod().getFilePath());
		 
			    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
			    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			    IDE.openEditorOnFileStore(page,fileStore);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void openMcsInstance(int mcsId, int mcsInstId)
	{
		try
		{
			File file = new File(ProjectInfo.getInputFile(MethodClonesReader.getMcsList().get(mcsId).getCloneList().get(mcsInstId).getFId()));
	 
		    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
		    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		    IDE.openEditorOnFileStore(page,fileStore);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void openMccInstance(int mccId, int mccInstId)
	{
		try
		{
			int index=-1;
			for(int j=0; j<ClonesReader.getMethodClones().size(); j++)
			{
				if(ClonesReader.getMethodClones().get(j).getClusterID() == mccId)
				{
					index=j;
				}
			}
			File file = new File(ClonesReader.getMethodClones().get(index).getMCCInstances().get(mccInstId).getMethod().getFilePath());
	 
		    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
		    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		    IDE.openEditorOnFileStore(page,fileStore);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void openFccInstance(int fccId, int fccInstId)
	{
		try
		{
			File file = new File(ClonesReader.getFileCloneList().get(fccId).getFCCInstance().get(fccInstId).getFile().getFullPath());
	 
		    IFileStore fileStore = EFS.getLocalFileSystem().getStore(file.toURI());
		    IWorkbenchPage page =PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
		    IDE.openEditorOnFileStore(page,fileStore);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static ArrayList<String> getMccInstances(int id)
	{
		 ArrayList<String> names = new ArrayList<String>();
		 for(int i=0; i<ClonesReader.getMethodClones().size(); i++)
		 {
			 if(ClonesReader.getMethodClones().get(i).getClusterID() == id)
			 {
				 for(int j=0; j<ClonesReader.getMethodClones().get(i).getMCCInstances().size(); j++)
				 {
					 names.add(ClonesReader.getMethodClones().get(i).getMCCInstances().get(j).getMethod().getMethodName());
				 }
			 }
		 }
		 //ClonesReader.getMethodClones().get(0).getMCCInstances().get(0).getMethod().getMethodName();
		 return names;
	}
	
	public static ArrayList<String> getFccInstances(int id)
	{
		 ArrayList<String> names = new ArrayList<String>();
		 for(int i=0; i<ClonesReader.getFileCloneList().size(); i++)
		 {
			 if(ClonesReader.getFileCloneList().get(i).getClusterID() == id)
			 {
				 for(int j=0; j<ClonesReader.getFileCloneList().get(i).getFCCInstance().size(); j++)
				 {
					 names.add(ClonesReader.getFileCloneList().get(i).getFCCInstance().get(j).getFileName());
				 }
			 }
		 }
		 //ClonesReader.getMethodClones().get(0).getMCCInstances().get(0).getMethod().getMethodName();
		 return names;
	}
	
	public static ArrayList<String> getMcsInstances(int id)
	{
		 ArrayList<String> names = new ArrayList<String>();
		 for(int i=0; i<MethodClonesReader.getMcsList().size(); i++)
		 {
			 if(MethodClonesReader.getMcsList().get(i).getMcsId() == id)
			 {
				 for(int j=0; j<MethodClonesReader.getMcsList().get(i).getCloneList().size(); j++)
				 {
					 names.add(MethodClonesReader.getMcsList().get(i).getCloneList().get(j).getFileName());
				 }
			 }
		 }
		 //ClonesReader.getMethodClones().get(0).getMCCInstances().get(0).getMethod().getMethodName();
		 return names;
	}
}
