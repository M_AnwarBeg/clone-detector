package cmt.cfc.incremental;

import java.util.ArrayList;

public class IncrementalObserver {
	
	public static ArrayList<Integer> mccs = new ArrayList<Integer>();
	public static ArrayList<Integer> fccs = new ArrayList<Integer>();
	public static ArrayList<Integer> mcs = new ArrayList<Integer>();
	
	public static void Notify(ArrayList<Integer> _mccs, ArrayList<Integer> _fccs) {
		
		mccs.addAll(_mccs);
		fccs.addAll(_fccs);
		getUpdatedClones();
	}
	
	public static void getUpdatedClones() {
		
		FrameSynchronizer.checkSync(mccs, fccs, mcs);
	}

}
