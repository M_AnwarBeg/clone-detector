package cmt.cfc.incremental;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import cmt.cddc.CloneRepository.CloneRepository;

public class FrameSynchronizer {
	
	public static ArrayList<Integer> unsyncMccs = new ArrayList<Integer>();
	public static ArrayList<Integer> unsyncFccs = new ArrayList<Integer>();
	public static ArrayList<Integer> unsyncMcs = new ArrayList<Integer>();
	
	public static void checkSync (ArrayList<Integer> mccs, ArrayList<Integer> fccs, ArrayList<Integer> mcs) {
		
		try
		{
			// checking MCC's if any MCC is already framed.
			Statement st = CloneRepository.getConnection().createStatement();
			Statement st3 = CloneRepository.getConnection().createStatement();
			st3.execute("use framerepository;");
			st.execute("use framerepository;");
			ResultSet rs = st.executeQuery("select * from mccframes;");
			
			int check = -1;
			int fid = -1;
			
			while(rs.next())
			{
				check = rs.getInt(1);
				fid = rs.getInt(2);
				for(int i=0; i<mccs.size(); i++)
				{
					if(check == mccs.get(i))
					{
						st3.execute("update frames set issynced = 0 where fid = "+fid);
						unsyncMccs.add(check);
					}
				}
			}
			
			// checking FCC's if any FCC is already framed.
						Statement st2 = CloneRepository.getConnection().createStatement();
						st2.execute("use framerepository;");
						Statement st4 = CloneRepository.getConnection().createStatement();
						st4.execute("use framerepository;");
						ResultSet rs2 = st2.executeQuery("select * from fccframes;");
						
						check = -1;
						fid = -1;
						
						while(rs2.next())
						{
							check = rs2.getInt(1);
							fid = rs2.getInt(2);
							for(int i=0; i<fccs.size(); i++)
							{
								if(check == fccs.get(i))
								{
									st4.execute("update frames set issynced = 0 where fid = "+fid);
									unsyncFccs.add(check);
								}
							}
						}
						
			// checking MCS's if any MCS is already framed.
						Statement st5 = CloneRepository.getConnection().createStatement();
						st5.execute("use framerepository;");
						Statement st6 = CloneRepository.getConnection().createStatement();
						st6.execute("use framerepository;");
						ResultSet rs3 = st5.executeQuery("select * from mcsframes;");
						
						check = -1;
						fid = -1;
						
						while(rs3.next())
						{
							check = rs3.getInt(1);
							fid = rs3.getInt(2);
							for(int i=0; i<mcs.size(); i++)
							{
								if(check == mcs.get(i))
								{
									st6.execute("update frames set issynced = 0 where fid = "+fid);
									unsyncMcs.add(check);
								}
							}
						}
						
						/*JButton[] Buttons = new JButton[2];
						
						Buttons[0] = new JButton("View/Update");//creates Button 1 and sets text 
						Buttons[1] = new JButton("Ignore");//creates button 2 and sets text
				         
				        
				      //MouseHandler class
				        final class MouseHandler implements MouseListener
				         
				        {
				            //MouseClicked(empty)
				            public void mouseClicked(MouseEvent e)
				            {
				     
				            }
				             
				            //If mouseEntered, display on Button
				            public void mouseEntered(MouseEvent e)
				            {
				                
				            }
				             
				            //If mouseExited, display on Button
				            public void mouseExited(MouseEvent e)
				            {
				                
				            }
				             
				            //If mousePressed, display on Button
				            public void mousePressed(MouseEvent e)
				            {
				            	if(e.getSource() == Buttons[0])
				                {
				                    Buttons[0].setText("CHECKED");
				                    IncrementalNotificationUI test = new IncrementalNotificationUI(new Shell(), 0);
				                    test.open();
				                    // open the next GUI i.e incrementalNotificationsGUI
				                    Display.getDefault().syncExec(new Runnable() {
				                        public void run() {
						                    IncrementalNotificationUI test = new IncrementalNotificationUI(new Shell(), 0);
						                    test.open();
				                        }
				                    });
				                }
				            }
				             
				            //If mouseReleased, display message on Button
				            public void mouseReleased(MouseEvent e)
				            {
				                
				            }
				        }
				        
				        MouseHandler mouseH = new MouseHandler();//new MouseHandler object
				        Buttons[0].addMouseListener(mouseH);//add listener to Button[0]
				        Buttons[1].addMouseListener(mouseH);//add listener to Button[1]
				    	  JFrame f = new JFrame();

				    	  int response  =  JOptionPane.showOptionDialog(f,
				        "               These buttons have MouseListeners attached to them.               ",
				        "MouseListener Dialog",
				        JOptionPane.YES_NO_CANCEL_OPTION,
				        JOptionPane.PLAIN_MESSAGE,
				        null,
				        Buttons,
				        null);//Creates a JOptionPane using all three buttons
*/						
						if(unsyncFccs.size() > 0 || unsyncMccs.size() > 0)
						{
							int response  =  JOptionPane.showConfirmDialog(null,"Some clones are unsynced.\n" +
				    		         "Do you want to sync them? \n", 
				    		        "Unsynced Clones",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				    	    if (response == JOptionPane.NO_OPTION) 
				    	    {
				    	    
				    	    } 
				    	    else if (response == JOptionPane.YES_OPTION) 
				    	    {
				    	    	
				    	    	IncrementalNotificationUI test = new IncrementalNotificationUI(new Shell(), 0);
			                    test.open();
				    	    	
				    	    } 
				    	    else if (response == JOptionPane.CLOSED_OPTION) 
				    	    {
				    	      
				    	    }
						}  
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
