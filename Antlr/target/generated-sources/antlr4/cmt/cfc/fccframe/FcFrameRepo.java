package cmt.cfc.fccframe;

import java.util.ArrayList;

public class FcFrameRepo {
	
	private int fcc_id;
	private ArrayList<Integer> sccs;
	
	public FcFrameRepo() 
	{
		this.fcc_id=-1;
		this.sccs=new ArrayList<Integer>();
	}
	
	public void setFccId(int id)
	{
		fcc_id=id;
	}
	
	public int getFccId()
	{
		return fcc_id;
	}
	
	public void addScc(int sccid)
	{
		this.sccs.add(sccid);
	}
	
	public ArrayList<Integer> getSccs()
	{
		return sccs;
	}

}
