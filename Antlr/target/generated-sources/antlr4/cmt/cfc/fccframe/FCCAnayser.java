package cmt.cfc.fccframe;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleClonesReader;


public class FCCAnayser {
	
	 public static void analyze(FileCluster fcc) throws IOException
	 {
		ArrayList<FileCloneInstance> fccInstances = fcc.getFCCInstance();
		for (FileCloneInstance fci : fccInstances)
		{
		    sortMethodCloneInstance(fci);
		    analyzeForOverlaps(fci); // check all the partial,complete overlaps for this specific instance
		}
		stabilizeAdjacencyMartix(fcc);
		for (FileCloneInstance fci : fccInstances)
		{
		   fci.analyzFileCloneInstance(fcc); // Now analyze each file clone instance
		}
	  }


	  	private static void sortMethodCloneInstance(FileCloneInstance fci)
	    {// This method will sort the SCCIDS according to their startlines
		Collections.sort(fci.getSCCs());
	    }

	  
	  	private static void analyzeForOverlaps(FileCloneInstance fci)
	    {
		// TODO Auto-generated method stub
		int[][] temp = new int[fci.getSCCs().size()][fci.getSCCs().size()];
		fci.setAdjeceny_Matrix_For_Overlaps(temp);
		adjencyMatrixInitializer(fci.getAdjeceny_Matrix_For_Overlaps(), fci);
	    }

	  	private static void adjencyMatrixInitializer(int[][] adjeceny_Matrix_For_Overlaps, FileCloneInstance fci)
	    {
		// TODO Auto-generated method stub
		for (int i = 0; i < fci.getSCCs().size(); i++)
		{
		    for (int j = i; j < fci.getSCCs().size(); j++)
		    {
			fci.getSCCs().get(i).compareSCCInstances(adjeceny_Matrix_For_Overlaps, fci.getSCCs().get(j), i, j);
		    }
		}
	    
	    }
	  	
	    public static SimpleCloneInstance getSCCInstanceWithLastLine(FileCloneInstance mci)
	    {
		int minimum = 0;
		SimpleCloneInstance last = null;
		for (SimpleCloneInstance scc : mci.getNewSccs_Contained())
		{
		    if (scc.getEndingIndex() > minimum)
		    {
			minimum = scc.getEndingIndex();
			last = scc;
		    }
		}
		return last;
	    }
	  
	  
	    private static void stabilizeAdjacencyMartix(FileCluster clone)
	    {
		// TODO Auto-generated method stub
		for (int i = 0; i < clone.getFCCInstance().get(0).getSCCs().size(); i++)
		{
		    for (int j = 0; j < clone.getFCCInstance().get(0).getSCCs().size(); j++)
		    {
			int temp = clone.getFCCInstance().get(0).getAdjeceny_Matrix_For_Overlaps()[i][j];
			for (FileCloneInstance mci : clone.getFCCInstance())
			{
			    if (temp != mci.getAdjeceny_Matrix_For_Overlaps()[i][j])
			    {
				for (FileCloneInstance mcii : clone.getFCCInstance())
				{
				    mcii.getAdjeceny_Matrix_For_Overlaps()[i][j] = 0;
				}
			    }
			}
		    }
		}
	    }
	  	
	    public static boolean checkInBetweenSCCInstances(FileCloneInstance fci, int index, SimpleCloneInstance prevInstance,
	    	    SimpleCloneInstance nextInstance, String path) throws IOException
	    {
	    	// TODO Auto-generated method stub
	    	//if (prevInstance.getEndLine() == nextInstance.getStartLine()
	    	if (prevInstance.getEndingIndex() == nextInstance.getStartingIndex()		
	    			
	    		&& prevInstance.getEndCol() == nextInstance.getStartCol())
	    	{
	    	    nextInstance.setStartCol(nextInstance.getStartCol() + 1);
	    	    nextInstance.updateCode(path);
	    	}

	    	//if (prevInstance.getEndLine() >= nextInstance.getEndLine()) // COMPLETE OVERLAP
	    	if (prevInstance.getEndingIndex() >= nextInstance.getEndingIndex()) // COMPLETE OVERLAP
	    	{
	    	    fci.getAnalysisDetails().remove(index + 1);
	    	    fci.getAnalysisDetails().remove(index + 1);
	    	    return false;
//	    	} else if (prevInstance.getEndLine() > nextInstance.getStartLine()) // PARTIAL OVERLAP
	    	} else if (prevInstance.getEndingIndex() > nextInstance.getStartingIndex()) // PARTIAL OVERLAP
	    	
	    	    {
	    	    fci.getAnalysisDetails().set(index, "PARTIALOVERLAP");
	    	    while (true)
	    	    {
	    		System.out.println("OVERLAP DETECTED. BIG PROBLEM");
	    	    }
	    	    // return true;
//	    	} else if (prevInstance.getEndLine() == nextInstance.getStartLine())// SAME LINE
	    	} else if (prevInstance.getEndingIndex() == nextInstance.getStartingIndex())// SAME LINE
	    		{
	    	    if (prevInstance.getEndCol() > nextInstance.getStartCol())// PARTIAL OVERLAP ON SAME ENDING LINES
	    	    {
	    		fci.getAnalysisDetails().set(index, "PARTIALOVERLAP");
	    		int x = 0;
	    		while (x < 100)
	    		{
	    		    System.out.println("OVERLAP DETECTED. BIG PROBLEM");
	    		}
	    		return true;
	    	    } else if (prevInstance.getEndCol() < nextInstance.getStartCol() - 1)// GAP ON SAME LINE BETWEEN CONSECUTIVE
	    										 // SCC INSTANCES
	    	    {
	    		int effectivestartcol = prevInstance.getEndCol() + 1;
	    		int effectivecol = nextInstance.getStartCol() - 1;
	    		fci.getAnalysisDetails().set(index,
	    	/*		("GAP" + "," + prevInstance.getEndLine() + "," + effectivestartcol + ","
	    				+ nextInstance.getStartLine() + "," + effectivecol + ","
	    				+ ClonesParser.getCodeSegment(path, prevInstance.getEndLine(), effectivestartcol,
	    					nextInstance.getStartLine(), effectivecol, null)));*/
	    		
	    		("GAP" + "," + prevInstance.getEndingIndex() + "," + effectivestartcol + ","
	    				+ nextInstance.getStartingIndex() + "," + effectivecol + ","
	    				+ SimpleClonesReader.getCodeSegment(path, prevInstance.getEndingIndex(), effectivestartcol,
	    					nextInstance.getStartingIndex(), effectivecol, null)));
	    		
	    		return true;
	    		// mci.getAnalysisDetails().set(index,("GAP" + "," + effectivestartcol + "," +
	    		// prevInstance.getEnd_col() + "," + nextInstance.getStart_line()
	    		// + ","+ effectivecol + "," + Parser.getCodeSegment(path,
	    		// prevInstance.getEnd_line(), effectivestartcol, nextInstance.getStart_line(),
	    		// effectivecol,null)));
	    		// return true;
	    	    } else // The TWO SCCINSTANCES HAVE NOTHING IN BETWEEN THEM AS THE SECOND STARTS RIGHT
	    		   // AFTER THE FIRST ONE
	    	    {
	    		fci.getAnalysisDetails().set(index, "");
	    		return true;
	    	    }
//	    	} else if (prevInstance.getEndLine() < nextInstance.getStartLine()) // GAPPED CLONES WITH ATLEAST 1 LINE IN
	    									    // BETWEEN THEM
	    	
	        } else if (prevInstance.getEndingIndex() < nextInstance.getStartingIndex()) // GAPPED CLONES WITH ATLEAST 1 LINE IN
	    	    // BETWEEN THEM

	    	
	    	{
	    	    int effectivestartcol = prevInstance.getEndCol() + 1;
	    	    int effectivecol = nextInstance.getStartCol() - 1;
	    	    fci.getAnalysisDetails().set(index,
	    		    ("GAP" + "," + prevInstance.getEndingIndex() + "," + effectivestartcol + ","
	    			    + nextInstance.getStartingIndex() + "," + effectivecol + ","
	    			    + SimpleClonesReader.getCodeSegment(path, prevInstance.getEndingIndex(), effectivestartcol,
	    				    nextInstance.getStartingIndex(), effectivecol, null)));
	    	    return true;
	    	}
	    	return true;

	        }


	    public static SimpleCloneInstance getInstance(String string, FileCloneInstance fci)
	    {
		// TODO Auto-generated method stub
		String[] parts = string.split(",");
		SimpleCloneInstance scc = getSCC(Integer.parseInt(parts[0]), fci);
		return scc;

	    }
	    
	    
	    private static SimpleCloneInstance getSCC(int sccid, FileCloneInstance mci)
	    {
		for (SimpleCloneInstance scc : mci.getNewSccs_Contained())
		{
		    if (sccid == scc.getSCCID())
		    {
			return scc;
		    }
		}
		return null;
	    }
}


