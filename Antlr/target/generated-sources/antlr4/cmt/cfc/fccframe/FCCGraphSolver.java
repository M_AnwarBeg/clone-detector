package cmt.cfc.fccframe;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import cmt.cddc.fileclones.FileCloneInstance;


public class FCCGraphSolver
{

    public static void SolveGraph(FileCloneInstance fci)
	{
		// TODO Auto-generated method stub
		int[][] matrix = fci.getAdjeceny_Matrix_For_Overlaps();
		UndirectedGraph<Integer, DefaultEdge> graph = new SimpleGraph<Integer, DefaultEdge>(DefaultEdge.class);
		addVertices(fci, graph);
		addEdges(fci, matrix, graph);
		Collection<Set<Integer>> set = findAllCliques(graph); // THIS ALGORITHM WILL FIND ALL CLIQUES
		// System.out.println("size : " + set.size());
		findWeightedCliqueSets(fci, set);
		// mci.getSortedCliques().add(null);
		/*
		 * if(mci.getSCCs().size()>1) { System.out.println(); }
		 */
	}

    private static void findWeightedCliqueSets(FileCloneInstance fci, Collection<Set<Integer>> set)
    {
	fci.getSortedCliques().clear();
	int count = 0;
	Iterator<Set<Integer>> iterator1 = set.iterator();
	while (iterator1.hasNext()) // ITERATE THROUGH CLIQUES
	{
	    fci.getAllCliques().add(iterator1.next());
	}
	// mci.setAllCliques(set);
	// System.out.println(set.isEmpty());
	while (!set.isEmpty())
	{
	    // System.out.println("lol");
	    Iterator<Set<Integer>> iterator = set.iterator();
	    int maxweight = 0;
	    int currweight;
	    Set<Integer> maxweightedclique = null;
	    Iterator<Integer> itr;
	    while (iterator.hasNext()) // ITERATE THROUGH CLIQUES
	    {
		Set<Integer> currset = iterator.next();
		currweight = 0;
		itr = currset.iterator();
		while (itr.hasNext()) // ITERATE THROUGH EACH INDIVIDUAL CLIQUE
		{
		    int index = itr.next();
		    // System.out.println("haha " + mci.getSCCs().get(index).getCode().length());
		    if (fci.getSCCs().get(index).getCodeSegment().length() == 0)
		    {
			fci.getSCCs().get(index);
		    }
		    currweight = currweight + fci.getSCCs().get(index).getCodeSegment().length();
		}
		// System.out.println(currweight+ " " + maxweight);
		if (currweight > maxweight)
		{
		    maxweight = currweight;
		    // System.out.println(maxweight);
		    maxweightedclique = currset;
		    // count++;
		}
	    }
	    count++;
	    if (count == 1)
	    {
		fci.maxclique = maxweightedclique;
		Iterator<Integer> itr2 = maxweightedclique.iterator();
		addNewSCCInstances(fci, itr2);
	    }
	    /*
	     * if(mci.getSCCs().size()>1) { System.out.println("hehehe "); }
	     */
	    fci.getSortedCliques().add(maxweightedclique);
	    set.remove(maxweightedclique);
	}
    }

    private static void addNewSCCInstances(FileCloneInstance fci, Iterator<Integer> itr2)
    {
	while (itr2.hasNext()) // NOW ADD THE NEW SCC INSTNACES
	{
	    int index = itr2.next();
	    fci.getNewSccs_Contained().add(fci.getSCCs().get(index));
	}
    }

    private static Collection<Set<Integer>> findAllCliques(UndirectedGraph<Integer, DefaultEdge> graph)
    {
	BronKerboschCliqueFinder<Integer, DefaultEdge> check = new BronKerboschCliqueFinder<Integer, DefaultEdge>(graph);
	Collection<Set<Integer>> set = check.getAllMaximalCliques();
	return set;
    }

    private static void addVertices(FileCloneInstance fci, UndirectedGraph<Integer, DefaultEdge> graph)
    {
	for (int i = 0; i < fci.getSCCs().size(); i++) // ADD ALL SCC INSTANCE CUSTOMS ( AS VERTICES )
	{
	    graph.addVertex(i);
	}
    }

    private static void addEdges(FileCloneInstance fci, int[][] matrix, UndirectedGraph<Integer, DefaultEdge> graph)
    {
	for (int i = 0; i < fci.getSCCs().size(); i++) // ADD ALL EDGES NOW
	{
	    for (int j = 0; j < fci.getSCCs().size(); j++)
	    {
		if (matrix[i][j] == 1)
		{
		    graph.addEdge(i, j);
		}
	    }
	}
    }

}