package cmt.cfc.fccframe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.structures.sMethod;
import cmt.cvac.views.FccView;
import cmt.cvac.views.MccView;

public class CliqueSelectionWizardFcc extends Wizard implements INewWizard
{

    CliqueSelectionWizardPage1Fcc page1;
    CliqueSelectionWizardPage2Fcc page2;
    IViewPart view;

    ArrayList<Integer> clique;
    FileCluster fcc;
    int fccid = -1;
    private int fccIndex = 0;

    public CliqueSelectionWizardFcc()
    {

	super();
	view = null;
    }

    public void setFCCID(int _mccid)
    {
	this.fccid = _mccid;

    }

    public int getFCCID()
    {
	return fccid;

    }

    @Override
    public void addPages()
    {

	page1 = new CliqueSelectionWizardPage1Fcc(this);
	addPage(page1);

	page2 = new CliqueSelectionWizardPage2Fcc(this);
	addPage(page2);

    }

    public CliqueSelectionWizardPage2Fcc getPage2()
    {
	return page2;
    }

    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
	// TODO Auto-generated method stub

	this.setNeedsProgressMonitor(true);
	this.setHelpAvailable(false);
	this.setForcePreviousAndNextButtons(true);

    }

    @Override
    public boolean performFinish()
    {

	/*
	 * File file=new File(path); String[]entries = file.list(); for(String s:
	 * entries){ File currentFile = new File(file.getPath(),s);
	 * currentFile.delete(); }
	 */

	markMethods();
	setFramed();
	final FccView fccView = (FccView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
		.findView("fccView");
	int i = fccView.primaryFccViewer.getTable().getSelectionIndex();
	setForeground(fccView, i);
	setBackground(fccView, i);
	/*enableEditFrameButton(fccView);
	enableDeleteFrameButton(fccView);
	disbaleCliqueSelectionButton(fccView);*/
	return true;
    }

    private void disbaleCliqueSelectionButton(final MccView mccView)
    {
	mccView.cliqueSelectionWizardAction.setEnabled(false);
    }

    private void enableEditFrameButton(final MccView mccView)
    {
	mccView.editFrameWizardAction.setEnabled(true);
    }
    private void enableDeleteFrameButton(final MccView mccView)
    {
	mccView.deleteFrameAction.setEnabled(true);
    }

    private void setBackground(final FccView fccView, int i)
    {
	final org.eclipse.swt.graphics.Color green = new org.eclipse.swt.graphics.Color(null, 192, 255, 192);
	fccView.primaryFccViewer.getTable().getItem(i).setBackground(green);
    }

    private void setForeground(final FccView fccView, int i)
    {
	Display display = this.getShell().getDisplay();
	final org.eclipse.swt.graphics.Color blue = display.getSystemColor(SWT.COLOR_BLUE);
	fccView.primaryFccViewer.getTable().getItem(i).setForeground(blue);
    }

    private void setFramed()
    {
	ClonesReader.getFileCloneList().get(fccIndex).setFramed(true);
    }

    @Override
    public boolean performCancel()
    {

	/*
	 * File file=new File(path); String[]entries = file.list(); for(String s:
	 * entries){ File currentFile = new File(file.getPath(),s);
	 * currentFile.delete(); }
	 */
	return true;
    }

    @Override
    public boolean canFinish()
    {
	// if(getContainer().getCurrentPage() != page5)
	// return false;
	// else
	// return true;

	if (getContainer().getCurrentPage() == page2)
	{
	    return true;
	} else
	{
	    return false;
	}

    }

    @Override
    public void createPageControls(Composite pageContainer)
    {
    }

    private void markMethods()
    {
	/*mcc = ClonesReader.getFileCloneList().get(fccIndex);
	for (MethodCloneInstance instance : fcc.getMCCInstances())
	{
	    sMethod currentMethod = instance.getMethod();
	    File fileToOpen = new File(currentMethod.getFilePath());
	    String text = getFileContent(fileToOpen);
	    IFile ifile = getIFile(fileToOpen);
	    try
	    {
		IMarker marker = ifile.createMarker("com.plugin.clonemanager.customtextmarker");
		int startChar = text.indexOf(currentMethod.getCodeSegment());
		int offset = currentMethod.getCodeSegment().length();
		marker.setAttribute(IMarker.CHAR_START, startChar+2);
		marker.setAttribute(IMarker.CHAR_END, startChar+2 + offset);
		marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
		marker.setAttribute(IMarker.LOCATION, "1");
		marker.setAttribute(IMarker.LINE_NUMBER, currentMethod.getStartToken());
		marker.setAttribute(IMarker.LINE_NUMBER, currentMethod.getEndToken());
		marker.setAttribute(IMarker.DONE, true);
	    } catch (Exception e)
	    {
		e.printStackTrace();
	    }
	}*/
    }

    private IFile getIFile(File fileToOpen)
    {
	IPath location = Path.fromOSString(fileToOpen.getAbsolutePath());
	IWorkspace workspace = ResourcesPlugin.getWorkspace();
	return workspace.getRoot().getFileForLocation(location);
    }

    private String getFileContent(File fileToOpen)
    {
	String content = "";
	try
	{
	    FileInputStream fis = new FileInputStream(fileToOpen);
	    byte[] data = new byte[(int) fileToOpen.length()];
	    fis.read(data);
	    fis.close();
	    content = new String(data, "UTF-8");
	} catch (IOException e2)
	{
	    e2.printStackTrace();
	}
	return content;
    }

    public int getMccIndex()
    {
	return fccIndex;
    }

    public void setFccIndex(int fccIndex)
    {
	this.fccIndex = fccIndex;
    }

    private IMarker[] findMarkers(IResource target) throws CoreException
    {
	String type = "com.plugin.clonemanager.customtextmarker";
	IMarker[] markers = target.findMarkers(type, true, IResource.DEPTH_INFINITE);
	return markers;
    }

}
