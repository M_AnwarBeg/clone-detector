package cmt.cfc.fccframe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.CloneRepository.ProjectInfo;
import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleClonesReader;
import cmt.cfc.sccframe.SCCTemplate;

public class CliqueSelectionWizardPage1Fcc extends WizardPage implements Listener
{
    public static FileCluster fcc;
    public static ArrayList<Integer> SccsSelected;
    public static ArrayList<Integer> SccsSelectedForVisualization;
	
    private ArrayList<StyleRange> arr1;
    private ArrayList<StyleRange> arr2;
    int mccid = -1, instance1id, instance2id;
    private StyleRange style;
    private StyleRange style2;
    ArrayList<Color> colorsused;
    private StyledText text_1;
    Combo combo, combo_1, combo_2;
    StyledText text;
    Label label, lblNewLabel;
    CliqueSelectionWizardPage2Fcc page2;
    CliqueSelectionWizardFcc wizard;

    /**
     * Create the wizard.
     */
    public CliqueSelectionWizardPage1Fcc(CliqueSelectionWizardFcc cliqueSelectionWizard)
    {
	super("wizardPage");
	setPageComplete(true);
	colorsused = new ArrayList<Color>();
	SccsSelected = new ArrayList<Integer>();
	SccsSelectedForVisualization = new ArrayList<Integer>();
	setTitle("Method Clone selected is ");
	setDescription("Select the Clique of Simple Clones that you wish to view and refactor using ART\n");
	this.wizard = cliqueSelectionWizard;
	this.mccid = cliqueSelectionWizard.getFCCID();
	this.text = null;
    }

    public void setMethod() throws IOException
    {
	// TODO Auto-generated method stub
	System.out.println("THE MCCID I GET IS: " + mccid);
	if (mccid == -1)
	{
	    System.out.println("SOME PROBLEM");
	}
	for (int i = 0; i <  ClonesReader.getFileCloneList().size(); i++)
	{
	    if (ClonesReader.getFileCloneList().get(i).getClusterID() == mccid)
	    {
		fcc =  ClonesReader.getFileCloneList().get(i);
		break;
	    }
	}
    }

    /**
     * Create contents of the wizard.
     *
     * @param parent
     */
    @Override
    public void createControl(Composite parent)
    {
	Composite container = new Composite(parent, SWT.BORDER);
	container.setLayout(new FormLayout());
	try
	{
	    setMethod();
	    setTitle("Method Clone selected is " + mccid);
	} catch (IOException e2)
	{
	    // TODO Auto-generated catch block
	    e2.printStackTrace();
	}
	setControl(container);
	((WizardDialog) this.getWizard().getContainer()).setPageSize(400, 400);

	text = new StyledText(container, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
	// fd_text_1.left = new FormAttachment(text, 546);
	FormData fd_text = new FormData();
	fd_text.top = new FormAttachment(0);
	text.setLayoutData(fd_text);
	fd_text.right = new FormAttachment(100, -464);
	combo = new Combo(container, SWT.READ_ONLY);
	fd_text.left = new FormAttachment(combo, 0, SWT.LEFT);
	FormData fd_combo = new FormData();
	fd_combo.bottom = new FormAttachment(100, -5);
	fd_combo.left = new FormAttachment(0, 8);
	combo.setLayoutData(fd_combo);
	text_1 = new StyledText(container, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
	fd_text.right = new FormAttachment(text_1, -10);
	FormData fd_text_1 = new FormData();
	fd_text_1.bottom = new FormAttachment(text, 0, SWT.BOTTOM);
	fd_text_1.top = new FormAttachment(0);
	text_1.setLayoutData(fd_text_1);

	label = new Label(container, SWT.NONE);
	fd_text_1.left = new FormAttachment(label, -108, SWT.LEFT);
	fd_text_1.right = new FormAttachment(label, 0, SWT.RIGHT);
	label.setText("coverage: ");
	FormData fd_label = new FormData();
	fd_label.right = new FormAttachment(100, -10);
	fd_label.bottom = new FormAttachment(combo, 0, SWT.BOTTOM);
	label.setLayoutData(fd_label);

	lblNewLabel = new Label(container, SWT.NONE);
	fd_label.left = new FormAttachment(lblNewLabel, 334);
	FormData fd_lblNewLabel = new FormData();
	fd_lblNewLabel.right = new FormAttachment(100, -721);
	fd_lblNewLabel.left = new FormAttachment(combo, 88);
	fd_lblNewLabel.bottom = new FormAttachment(combo, 0, SWT.BOTTOM);
	lblNewLabel.setLayoutData(fd_lblNewLabel);
	lblNewLabel.setText("coverage: ");

	Button btnDiff = new Button(container, SWT.NONE);
	btnDiff.addSelectionListener(new SelectionAdapter() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		try {
			SetDiff();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("done");
	    }
	});
	Button btnSelect = new Button(container, SWT.NONE);
	btnSelect.addSelectionListener(new SelectionAdapter() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		try {
			SetData();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("done");
	    }
	});
	FormData fd_btnSelect = new FormData();
	fd_btnSelect.top = new FormAttachment(text, 3);
	btnSelect.setLayoutData(fd_btnSelect);
	btnSelect.setText("Commonalities");
	FormData fd_btnDiff = new FormData();
	fd_btnDiff.top = new FormAttachment(btnSelect, 2);
	fd_btnDiff.left= new FormAttachment(44);
	btnDiff.setLayoutData(fd_btnDiff);
	btnDiff.setText("Differences");

	combo_1 = new Combo(container, SWT.NONE);
	fd_text.bottom = new FormAttachment(combo_1, -6);
	FormData fd_combo_1 = new FormData();
	fd_combo_1.bottom = new FormAttachment(lblNewLabel, -6);
	combo_1.setLayoutData(fd_combo_1);

	combo_2 = new Combo(container, SWT.NONE);
	fd_btnSelect.right = new FormAttachment(combo_2, -138);
	FormData fd_combo_2 = new FormData();
	fd_combo_2.left = new FormAttachment(0, 631);
	fd_combo_2.bottom = new FormAttachment(label, -6);
	combo_2.setLayoutData(fd_combo_2);
	Populate_Drop_Down();

	Label lblSelectClique = new Label(container, SWT.NONE);
	fd_combo_1.left = new FormAttachment(lblSelectClique, 118);
	FormData fd_lblSelectClique = new FormData();
	fd_lblSelectClique.bottom = new FormAttachment(combo, -6);
	fd_lblSelectClique.left = new FormAttachment(combo, 0, SWT.LEFT);
	lblSelectClique.setLayoutData(fd_lblSelectClique);
	lblSelectClique.setText("Select Clique");
	try 
	{
		SetData();
	} 
	catch (Exception e1)
	{
		e1.printStackTrace();
	}
	combo.addListener(SWT.Selection, new Listener() {
	    @Override
	    public void handleEvent(Event e)
	    {
	    }
	});
    }

    @Override
    public IWizardPage getNextPage()
    {

	//CloneDB.loadResultsForFraming(mcc);

	/*try
	{
	    MCCsAnalyser.modifymcc(mcc, SccsSelected);
	    for (MethodCloneInstance instance : mcc.getMCCInstances())
	    {
		instance.DisplayAnalysisDetails();
	    }
	    System.out.println(mcc.getClusterID());
	    MCCTemplate.genMCCTemplate(mcc.getClusterID());
	} catch (IOException e)
	{
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	page2 = wizard.getPage2();
	//SampleHandler.runFileHandler(mcc.getClusterID());
	FileHandler fileHandler = new FileHandler(mcc.getClusterID());//FileHandler.runFileHandler(mcc.getClusterID());
	page2.totalTabs = fileHandler.listOfARTFiles.size();
	return page2;*/
    	return null;

    }

    @Override
    public boolean canFlipToNextPage()
    {
	
	    return false;
	
    }
    private void SetData() throws Exception
    {
	// TODO Auto-generated method stub
	setClique();
	setCode();
	setLabels();
	try
	{
	    Highlight();
	} catch (IOException e1)
	{
	    // TODO Auto-generated catch block
	    e1.printStackTrace();
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    }
    private void SetDiff() throws Exception
    {
		// TODO Auto-generated method stub
		setClique();
		setCode();
		setLabels();
		try
			{
			highlightdiff3();
			}	
		catch (IOException e1)
			{
			    // TODO Auto-generated catch block
	    e1.printStackTrace();
	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    void Populate_Drop_Down()
    {
	// TODO Auto-generated method stub
	Collection<Set<Integer>> cliques = fcc.getFCCInstance().get(0).getSortedCliques();
	String[] cliqes = new String[cliques.size()];
	String[] instances = new String[fcc.getFCCInstance().size()];
	for (int i = 0; i < cliques.size(); i++)
	{
	    int effective = i + 1;
	    cliqes[i] = "Clique" + " " + effective;
	}
	for (int i = 0; i < fcc.getFCCInstance().size(); i++)
	{
	    int effective = i + 1;
	    instances[i] = "Instance: " + effective;
	}
	combo_1.setItems(instances);
	combo_1.setText(instances[0]);
	combo_2.setItems(instances);
	combo_2.setText(instances[1]);
	combo.setItems(cliqes);
	combo.setText(cliqes[0]);
    }
    
    private void highlightdiff3() throws Exception {
		ArrayList<SimpleCloneInstance> mci1=fcc.getFCCInstance().get(instance1id).getSCCs();
		ArrayList<SimpleCloneInstance> mci2=fcc.getFCCInstance().get(instance2id).getSCCs();;
		
//		String code1=mcc.getMCCInstances().get(instance1id).getMethod().getCodeSegment();
		String path1=ProjectInfo.getFilePath(fcc.getFCCInstance().get(instance1id).getFileId());
		
//		String code2=mcc.getMCCInstances().get(instance2id).getMethod().getCodeSegment();
		String path2=ProjectInfo.getFilePath(fcc.getFCCInstance().get(instance2id).getFileId());
		
		//for(Integer k : SccsSelectedForVisualization)
		{
			arr1=new ArrayList<StyleRange>();
			arr2=new ArrayList<StyleRange>();
			
			arr1.add(style);
			arr2.add(style2);
			
			Color temp = new Color(null,255,255,255);
			int length=0;
			int start=0;
			for(int i=0;i<mci1.size();i++)
			{
				 Statement stmt = CloneRepository.getConnection().createStatement();
				 stmt.execute("use "+CloneRepository.getDBName());
				 ResultSet rs = stmt.executeQuery("select scc_id from scc_instance where scc_instance_id="+mci1.get(i).getSCCID()+";");
				 rs.next();
				 ArrayList<SimpleClone> sccList = SimpleClonesReader.getClones();
				 
				 for(int k=0;k<sccList.size();k++)
				 {
					 if(sccList.get(k).getSSCId()==rs.getInt(1))
					 {
						SCCTemplate.TokenizeSCC(sccList.get(k));
					 }
				 }
 				
				 text.setStyleRange(style);
				 text_1.setStyleRange(style2);
				 
				 for(int y=0;y<mci1.get(i).getTokens().size();y++)
				 {
					 if(!(mci1.get(i).getTokens().get(y).equals(mci2.get(i).getTokens().get(y))))
					 {
						 	String intermediatecode=MethodClonesReader.getCodeSegmentForColoring(path1,1,1,mci1.get(i).getTokensLine().get(y),mci1.get(i).getTokensColumn().get(y),null);
						 	intermediatecode=intermediatecode.replace("\t","        ");
							start=intermediatecode.length()-1;
							
							/*intermediatecode=MethodClonesReader.getCodeSegment(path1,1,1,mcc.getMCCInstances().get(instance1id).getMethod().getStartToken(),1,null);
							start +=intermediatecode.length();*/
							
							
							//text.setStyleRanges(text.getStyleRanges());
							
						 	StyleRange tempStyle = new StyleRange();
						 	tempStyle.start = start;
						 	tempStyle.length = mci1.get(i).getTokens().get(y).length();
						 	tempStyle.foreground= new Color(null,255,255,255);
						 	tempStyle.background = text.getStyleRangeAtOffset(start).background;
						 	arr1.add(tempStyle);
							text.setStyleRange(tempStyle);
							text.getText();
							
							intermediatecode=MethodClonesReader.getCodeSegmentForColoring(path2,1,1,mci2.get(i).getTokensLine().get(y),mci2.get(i).getTokensColumn().get(y),null);
							intermediatecode=intermediatecode.replace("\t","        ");
							start=intermediatecode.length()-1;
							
							/*intermediatecode=MethodClonesReader.getCodeSegment(path1,1,1,mcc.getMCCInstances().get(instance1id).getMethod().getStartToken(),1,null);
							start +=intermediatecode.length();*/
							
							//text_1.setStyleRanges(text_1.getStyleRanges());
							
							tempStyle = new StyleRange();
							tempStyle.start = start;
							tempStyle.length = mci2.get(i).getTokens().get(y).length();
							tempStyle.foreground= new Color(null,255,255,255);
							tempStyle.background = text_1.getStyleRangeAtOffset(start).background;
							arr2.add(tempStyle);
							text_1.setStyleRange(tempStyle);
					 }
				 }
				 /*StyleRange[] array1=new StyleRange[arr1.size()];
				 StyleRange[] array2=new StyleRange[arr1.size()];
				 
				 for(int w=0;w<arr1.size();w++)
				 {
					 array1[w]=arr1.get(w);
					 array2[w]=arr2.get(w);
				 }
				 
				 text.setStyleRanges(new int[arr1.size()],array1);
				 text_1.setStyleRanges(new int[arr1.size()],array2);*/
				 /*text.setStyleRanges(array1);
				 text_1.setStyleRanges(array2);*/
			}
		}
    }
    
    private void highlight1(ArrayList<Color> colorsused) throws Exception {
		FileCloneInstance fci=fcc.getFCCInstance().get(instance1id);
		String code=MethodClonesReader.getCodeSegment(cmt.cddc.clonerunmanager.ProjectInfo.getInputFile(fci.getFileId()),1,fci.getFile().getEndLine());
		String path=ProjectInfo.getFilePath(fci.getFileId());
		for(Integer k : SccsSelectedForVisualization)
		{
			int i1=0,i2=0,i3=0;
			while(i1+i2+i3<100 || i1+i2+i3>550)
			{
				Random r1 = new Random();
				i1 = r1.nextInt(255);
				Random r2 = new Random();
				i2 = r2.nextInt(255);
				Random r3 = new Random();
				i3 = r3.nextInt(255);	
			}
			Color temp = new Color(null,0,175,255);
			colorsused.add(temp);
			int length=0;
			int start=0;
			for(SimpleCloneInstance scc: fci.getSCCs())
			{
				if(scc.getSCCID()==k)
				{
					length=scc.getCodeSegment().length()-1;
					if(fci.getFile().getFileStartToken()==scc.getStartingIndex())
					{
						String completelinecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,fci.getFile().getFileStartToken()+1,1,null);
						String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
						start= intermediatecode.length();
					}
					else
					{
						String intermediatecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
						start=intermediatecode.length()-1;	
					}
				}
			}
			style = new StyleRange();
			style.start = start;
			style.length = length;
			style.fontStyle = SWT.BOLD;
			style.background = temp;
			text.setStyleRange(style);
		}
	}
    
    private void highlightdiff1() throws Exception {
		FileCloneInstance fci=fcc.getFCCInstance().get(instance1id);
		String code=MethodClonesReader.getCodeSegment(ProjectInfo.getInputFile(fci.getFileId()),1,fci.getFile().getEndLine());
		String path=ProjectInfo.getFilePath(fci.getFileId());
		for(Integer k : SccsSelectedForVisualization)
		{
			Color temp = new Color(null,255,255,255);
			int length=0;
			int start=0;
			for(SimpleCloneInstance scc: fci.getSCCs())
			{
 				if(scc.getSCCID()==k)
				{
					length=scc.getCodeSegment().length()-1;
					if(fci.getFile().getFileStartToken()==scc.getStartingIndex())
					{
						String completelinecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,fci.getFile().getFileStartToken()+1,1,null);
						String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
						start= intermediatecode.length();
					}
					else
					{
						
						String intermediatecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
						start=intermediatecode.length()-1;	
					}
				}
			}
			style = new StyleRange();
			style.start = start;
			style.length = length;
			style.fontStyle = SWT.BOLD;
			style.background = temp;
			text.setStyleRange(style);
			
		}
	}
    
    private void HighlightDiff() throws Exception
    {
    	int i1=0,i2=0,i3=0;
		while(i1+i2+i3<100 || i1+i2+i3>550)
		{
			Random r1 = new Random();
			i1 = r1.nextInt(255);
			Random r2 = new Random();
			i2 = r2.nextInt(255);
			Random r3 = new Random();
			i3 = r3.nextInt(255);	
		}
		Color temp = new Color(null,i1,i2,i3);
		
		String code=MethodClonesReader.getCodeSegment(ProjectInfo.getInputFile(fcc.getFCCInstance().get(instance1id).getFileId()),1,fcc.getFCCInstance().get(instance1id).getFile().getEndLine()).replace("\t","        ");
		style = new StyleRange();
		style.start = 0;
		style.length = code.length();
		style.fontStyle = SWT.BOLD;
		style.background = temp;
		text.setStyleRange(style);
		
		code=MethodClonesReader.getCodeSegment(ProjectInfo.getInputFile(fcc.getFCCInstance().get(instance1id).getFileId()),1,fcc.getFCCInstance().get(instance2id).getFile().getEndLine()).replace("\t","        ");
		style = new StyleRange();
		style.start = 0;
		style.length = code.length();
		style.fontStyle = SWT.BOLD;
		style.background = temp;
		text_1.setStyleRange(style);
		
		highlightdiff1();
		highlightdiff2();
    }
    private void Highlight() throws Exception
    {
    	colorsused.clear();
    	highlight1(colorsused);
    	highlight2(colorsused);
    }

    private void highlight2(ArrayList<Color> colorsused) throws Exception {
		FileCloneInstance fci=fcc.getFCCInstance().get(instance2id);
		String code=MethodClonesReader.getCodeSegment(cmt.cddc.clonerunmanager.ProjectInfo.getInputFile(fci.getFileId()),1,fci.getFile().getEndLine());
		String path=ProjectInfo.getFilePath(fci.getFileId());
		int count=0;
		for(Integer k : SccsSelectedForVisualization)
		{
			Color temp = colorsused.get(count);
			colorsused.add(temp);
			int length=0;
			int start=0;
			for(SimpleCloneInstance scc: fci.getSCCs())
			{
				if(scc.getSCCID()==k)
				{
					length=scc.getCodeSegment().length()-1;
					if(fci.getFile().getFileStartToken()==scc.getStartingIndex())
					{
						String completelinecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,fci.getFile().getFileStartToken()+1,1,null);
						String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
						start= intermediatecode.length();
					}
					else
					{
						String intermediatecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
						start=intermediatecode.length()-1;	
					}							
				}
			}
			
			style2 = new StyleRange();
			style2.start = start; 
			style2.length = length;
			style2.fontStyle = SWT.BOLD;
			style2.background = temp;
			text_1.setStyleRange(style2);
			count++;
		}
	}
    
    private void highlightdiff2() throws Exception {
		FileCloneInstance fci=fcc.getFCCInstance().get(instance2id);
		String code=MethodClonesReader.getCodeSegment(ProjectInfo.getInputFile(fci.getFileId()),1,fci.getFile().getEndLine());
		String path=ProjectInfo.getFilePath(fci.getFileId());
		for(Integer k : SccsSelectedForVisualization)
		{
			Color temp = new Color(null,255,255,255);
			int length=0;
			int start=0;
			for(SimpleCloneInstance scc: fci.getSCCs())
			{
				if(scc.getSCCID()==k)
				{
					length=scc.getCodeSegment().length()-1;
					if(fci.getFile().getFileStartToken()==scc.getStartingIndex())
					{
						String completelinecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,fci.getFile().getFileStartToken()+1,1,null);
						String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
						start= intermediatecode.length();
					}
					else
					{
						String intermediatecode=MethodClonesReader.getCodeSegment(path,fci.getFile().getFileStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
						start=intermediatecode.length()-1;	
					}							
				}
			}
			
			style = new StyleRange();
			style.start = start; 
			style.length = length;
			style.fontStyle = SWT.BOLD;
			style.background = temp;
			text_1.setStyleRange(style);
		}
	}
    
    private void setLabels()
    {
	System.out.println("setting labels");
	int scccount1 = 0;
	int scccount2 = 0;
	FileCloneInstance mcc1 = fcc.getFCCInstance().get(instance1id);
	FileCloneInstance mcc2 = fcc.getFCCInstance().get(instance2id);
	for (Integer k : SccsSelected)
	{
	    for (SimpleCloneInstance scc : mcc1.getSCCs())
	    {
		if (scc.getSCCID() == k)
		{
		    scccount1 = scccount1 + scc.getCodeSegment().length();
		    break;
		}
	    }
	    for (SimpleCloneInstance scc : mcc2.getSCCs())
	    {
		if (scc.getSCCID() == k)
		{
		    scccount2 = scccount2 + scc.getCodeSegment().length();
		    break;
		}
	    }

	}
	int methodcount1 = fcc.getFCCInstance().get(instance1id).getTokenCount();
	int methodcount2 = fcc.getFCCInstance().get(instance2id).getTokenCount();
	double percentage1 = (double) scccount1 / (double) methodcount1;
	double percentage2 = (double) scccount2 / (double) methodcount2;
	percentage1 = percentage1 * 100;
	percentage2 = percentage2 * 100;
	DecimalFormat df = new DecimalFormat("#.#");
	String percent1 = df.format(percentage1);
	String percent2 = df.format(percentage2);
	lblNewLabel.setText("coverage: " + percent1 + "%");
	label.setText("coverage: " + percent2 + "%");
    }

    private void setCode() throws Exception
    {
	// TODO Auto-generated method stub
	System.out.println("setting code");
	String id1 = combo_1.getText();
	String[] parts = id1.split(" ");
	instance1id = Integer.parseInt(parts[1]) - 1;
	text.setText(getFileContent(new File(cmt.cddc.clonerunmanager.ProjectInfo.getInputFile(fcc.getFCCInstance().get(instance1id).getFileId()))).replace("\t","        ").replace("\r",""));
	//text.setText(MethodClonesReader.getCodeSegment(ProjectInfo.getInputFile(fcc.getFCCInstance().get(instance1id).getFileId()),1,fcc.getFCCInstance().get(instance1id).getFile().getEndLine()).replace("\t","        "));
	String id2 = combo_2.getText();
	String[] parts2 = id2.split(" ");
	instance2id = Integer.parseInt(parts2[1]) - 1;
	text_1.setText(getFileContent(new File(cmt.cddc.clonerunmanager.ProjectInfo.getInputFile(fcc.getFCCInstance().get(instance2id).getFileId()))).replace("\t","        ").replace("\r",""));
	//text_1.setText(MethodClonesReader.getCodeSegment(ProjectInfo.getInputFile(fcc.getFCCInstance().get(instance1id).getFileId()),1,fcc.getFCCInstance().get(instance1id).getFile().getEndLine()).replace("\t","        "));
    }

    private static String getFileContent(File fileToOpen)
	 {
		 String content = "";
			try
				{
				    FileInputStream fis = new FileInputStream(fileToOpen);
				    byte[] data = new byte[(int) fileToOpen.length()];
				    fis.read(data);
				    fis.close();
				    content = new String(data, "UTF-8");
				} 
			catch (IOException e2)
				{
				    e2.printStackTrace();
				}
			return content; 
	 }
    
    private void setVisulizationClique(int cliqueNumber)
    {
    	int count=0;
    	Iterator<Set<Integer>> visulaizationIterator = fcc.getFCCInstance().get(0).getSortedCliques().iterator();
    	
    	while(visulaizationIterator.hasNext())
    	{
    		count++;
    		if(count==cliqueNumber)
    		{
		    	Set<Integer> visualizationSet=visulaizationIterator.next();
				while (visulaizationIterator.hasNext())   //comment by SAUD to resolve framing issue
				{
					visualizationSet.addAll(visulaizationIterator.next());
				}
				
				Iterator<Integer> itr = visualizationSet.iterator();
				while (itr.hasNext())
				{
				    int sccid = fcc.getFCCInstance().get(0).getSCCs().get(itr.next()).getSCCID();
				    SccsSelectedForVisualization.add(sccid);
				}
				break;
    		}
    		else
    		{
    			visulaizationIterator.next();
    		}
    	}
    }
    
    private void setClique()
    {
	// TODO Auto-generated method stub
	System.out.println("setting clique");
	SccsSelected.clear();
	SccsSelectedForVisualization.clear();
	String n_id = combo.getText();
	String[] parts = n_id.split(" ");
	int cliquenumber;
	// System.out.println(Integer.parseInt(parts[1]));
	Iterator<Set<Integer>> iterator = fcc.getFCCInstance().get(0).getSortedCliques().iterator();
	int count = 0;
	cliquenumber = Integer.parseInt(parts[1]);
	while (iterator.hasNext()) // ITERATE THROUGH CLIQUES
	{
	    count++;
	    if (count == cliquenumber)
	    {
		Set<Integer> set = iterator.next();
		
		Iterator<Integer> itr = set.iterator();
		while (itr.hasNext())
		{
		    int sccid = fcc.getFCCInstance().get(0).getSCCs().get(itr.next()).getSCCID();
		    SccsSelected.add(sccid);
		}
		setVisulizationClique(cliquenumber);
		break;
	    } else
	    {
		iterator.next();
	    }
	}
    }

    @Override
    public void handleEvent(Event event)
    {
	// TODO Auto-generated method stub

    }

}
