	package cmt.cfc.fccframe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.wizard.WizardDialog;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodCloneWriterDB;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cfc.mccframe.MCIContainer;
import cmt.cfc.mccframe.MCSTemplate;
import cmt.cfc.mccframe.RenamingWizard;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.FramingStructure;
import cmt.cfc.sccframe.SCCTemplate;
import cmt.cfc.sccframe.SCCTemplateNew;
import cmt.cfc.utility.Initializer;
import cmt.common.Directories;
import cmt.cvac.views.FccView;

public class FCCTemplate {

	public static int level = 0;
	public static String artPath="";
    final static String OUTPUT_PATH = "\\ART_Output\\";
    final String JAVA_OUTPUT_PATH = "\\ART_Output\\Output\\";
    static int curMCCID = -1;
    static int curFileID = 0;
    static int curMethodID = 0;
    static int curVariableID = 0;
    static ArrayList<String> FramePaths;
    static ArrayList<Integer> MCC_Settings;
    static ArrayList<FileCloneInstance>  fci_List;
    static ArrayList<MCIContainer> MCI_Container_List;
    static ArrayList<SimpleCloneInstance> SCCInstanceList;
    static ArrayList<SCCTemplate> SCCTemplates;
    public static boolean fccTrigger=false;
    public static ArrayList<FramingStructure> FrameSt;
    public static boolean isComplete=false;
    static ArrayList<Integer> sccsIncluded= new ArrayList<Integer>();
    static ArrayList<SCCTemplateNew> SCCTemplatesNew;
    public static FileCluster fCluster;
    
    
    
    public FCCTemplate(int FCCID, FileCluster fcc)
    {
    	fccTrigger=true;
	SCCInstanceList = new ArrayList<SimpleCloneInstance>();
	FramePaths = new ArrayList<String>();
	SCCTemplates = new ArrayList<SCCTemplate>();
	MCC_Settings =     new ArrayList<Integer>();
	fci_List =   fcc.getFCCInstance();       // new ArrayList<FileCloneInstance>();
	MCI_Container_List = new ArrayList<MCIContainer>();
	MCC_Settings.add(1);
	MCC_Settings.add(1);
	curMCCID = FCCID;
	SCCTemplatesNew=new ArrayList<SCCTemplateNew>();
	fCluster=fcc;
	
		for(int i=0; i<fcc.getFCCInstance().get(0).getNewSccs_Contained().size();i++)
		{
			sccsIncluded.add(fcc.getFCCInstance().get(0).getNewSccs_Contained().get(i).getSCCID());
		}
	
	
	
	
		try
		{
			CloneRepository.getFrameId();
			CloneRepository.openConnection();
			Statement stmt2 = CloneRepository.conn.createStatement();
			stmt2.execute("use framerepository;");
			ResultSet rs = stmt2.executeQuery("select fid from fccframes where fccid="+curMCCID);
			
			if(rs.next())
			{
				System.out.println("Selected FCC is already framed.");
			}
			else
			{	//Ubaid
				FrameSt= new ArrayList<FramingStructure>();
				extractSimpleClones(fcc);
				
				isComplete(sccsIncluded);
				if(isComplete)
				{
				 RenamingWizard wizard = new RenamingWizard();
				 wizard.clearFrame();
				
				 
				
				wizard.setFrameStructure(FrameSt);
				WizardDialog dialog1 = new WizardDialog(null,wizard);
				dialog1.setBlockOnOpen(true);
			    dialog1.open();
			       
			        
			        
			        
//			        int returnCode = dialog1.open();
//			        if(returnCode == Dialog.OK)
//			          System.out.println(wizard.getPageCount());
//			        else
//			          System.out.println("Cancelled");
					

				
				}
				

//				for(int i=0; i<FrameSt.size(); i++)
//				{
//				extractSimpleClonesUpdated(fcc,FrameSt.get(i));
//					
//				}
//				generateTemplate();
				FrameSt.clear();
				fccTrigger=false;
				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }

    
    //Ubaid
    public static boolean isComplete(ArrayList<Integer> scc)
    {
    		for(int i=0; i<FrameSt.size(); i++)
    		{
    		if(sccsIncluded.contains(FrameSt.get(i).getSCCId()))
    			isComplete=true;
    		else
    			isComplete=false;
    			
    		}
    		
    		
    		
    	return isComplete;	
    		
    }
    
    
    public static FileCluster getFileClones()
    {
    	return fCluster;
    }
    
    
    
    public static void clearAll()
    {
    	level = 0;
    	artPath="";
       
         curMCCID = -1;
        curFileID = 0;
        curMethodID = 0;
        curVariableID = 0;
        FramePaths.clear();
        MCC_Settings.clear();
        fci_List.clear();
        MCI_Container_List.clear();
        SCCInstanceList.clear();
        SCCTemplates.clear();
        fccTrigger=false;
        FrameSt.clear();
        isComplete=false;
        sccsIncluded.clear();
        SCCTemplatesNew.clear();
        fCluster=null;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    public void extractSimpleClones(FileCluster fcc)
    {
	boolean firstPass = true;
	ArrayList<String> baseFormat = fci_List.get(0).getAnalysisDetails();
	for (int SCCID : fcc.getvCloneClasses())
	{
	    if (!isIncluded(SCCID, baseFormat))
	    {
		continue;
	    }

	    // Create frames for all required SCCIDs //
	    String path = Directories.getAbsolutePath(OUTPUT_PATH);
	    String filename = SCCID + "_Frame.art";
	    SCCTemplate template = null;
	    if (!firstPass)
	    {
		MCC_Settings.set(1, 0);
	    }
	    firstPass = false;
	    SimpleClone SimpleClone = findSCC(SCCID);
	    if (SimpleClone != null)
	    {
		template = new SCCTemplate(SCCID, ClonesReader.getSimpleClones().get(SCCID), MCC_Settings, fcc.getClusterID());
		FccView.repo.get(FccView.repo.size()-1).addScc(SCCID);
	    } else
	    {
		System.out.println("Critical error, cannot find SCC!");
	    }
	    SCCTemplates.add(template);
	    FramePaths.add(path + filename);
	}
	// All Frames Of SCCs Obtained! //
	System.out.println("I have all the SCC_Templates!");

	// Spend A Little Time Extracting SCCID List From New Analysis List //
	int MCI_Index = 0;
	int counter = 0;
	for (FileCloneInstance fci : fci_List)
	{
	    MCIContainer container = new MCIContainer(SCCTemplates);
	    counter = 0;
	    System.out.println("*******MCI #" + MCI_Index + "*******");
	    for (String codeSegment : fci.getAnalysisDetails())
	    {
		if (counter % 2 != 0) // If Odd Index Encountered //
		{
		    // Give This Portion To MCI_Container //
 
		}
		counter++;
	    }
	    MCI_Container_List.add(container);
	    MCI_Index++;
	    System.out.println("************************");
	}
	System.out.println("Done processing");
	
    }

    
    //Ubaid
    public static void extractSimpleClonesUpdated(FileCluster fcc,FramingStructure fs)
    {
	boolean firstPass = true;
	ArrayList<String> baseFormat = fci_List.get(0).getAnalysisDetails();
	for (int SCCID : fcc.getvCloneClasses())
	{
	    if (!isIncluded(SCCID, baseFormat))
	    {
		continue;
	    }

	    // Create frames for all required SCCIDs //
	    String path = Directories.getAbsolutePath(OUTPUT_PATH);
	    String filename = SCCID + "_Frame.art";
	    SCCTemplateNew template = null;
	    if (!firstPass)
	    {
		MCC_Settings.set(1, 0);
	    }
	    firstPass = false;
	    SimpleClone SimpleClone = findSCC(SCCID);
	    if (SimpleClone != null)
	    {
	    	if(SCCID==fs.getSCCId())
	    	{
			template = new SCCTemplateNew(SCCID, ClonesReader.getSimpleClones().get(SCCID), MCC_Settings, fcc.getClusterID(),fs);
			FccView.repo.get(FccView.repo.size()-1).addScc(SCCID);
			SCCTemplatesNew.add(template); 
	    	}
	    } else
	    {
		System.out.println("Critical error, cannot find SCC!");
	    }
	    
	    FramePaths.add(path + filename);
	}
	// All Frames Of SCCs Obtained! //
	System.out.println("I have all the SCC_Templates!");

	// Spend A Little Time Extracting SCCID List From New Analysis List //
	int MCI_Index = 0;
	int counter = 0;
	for (FileCloneInstance fci : fci_List)
	{
	    MCIContainer container = new MCIContainer(SCCTemplates);
	    counter = 0;
	    System.out.println("*******MCI #" + MCI_Index + "*******");
	    for (String codeSegment : fci.getAnalysisDetails())
	    {
		if (counter % 2 != 0) // If Odd Index Encountered //
		{
		    // Give This Portion To MCI_Container //
 
		}
		counter++;
	    }
	    MCI_Container_List.add(container);
	    MCI_Index++;
	    System.out.println("************************");
	}
	System.out.println("Done processing");
	
 }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public static SimpleClone findSCC(int SCCID)
    {
	ArrayList<SimpleClone> SCCList = ClonesReader.getSimpleClones();
	for (SimpleClone SimpleClone : SCCList)
	{
	    if (SimpleClone.getSSCId() == SCCID)
	    {
		return SimpleClone;
	    }
	}
	return null;
    }

    public static void generateTemplate()
    {
	String path = Directories.getAbsolutePath(OUTPUT_PATH);

	// String pathTemp = CloneManagementPlugin.getAbsolutePath(OUTPUT_PATH_Temp);

	curFileID = curMCCID;
	try
			{
				
			    
			    String SPC_PATH = "\\FCC_SPC\\" + curFileID + "_FCC_SPC.art";
			    String templatePath="\\FCC_Template\\"+ curFileID + "_FCC_template.art";
			   
			    FileWriter spc_file = new FileWriter(path + SPC_PATH, true);
			    BufferedWriter spc_output = new BufferedWriter(spc_file);
			    
			    FileWriter template_file = new FileWriter(path + templatePath, true);
		
			    BufferedWriter templateOutput = new BufferedWriter(template_file);
		
			    String templateFileName= curFileID + "_FCC_template.art";
			    // FileWriter spc_file_temp = new FileWriter(pathTemp + SPC_PATH, true);
			    // BufferedWriter spc_output_temp = new BufferedWriter(spc_file_temp);
			    // ====== START OF SPC ===== //
			    // Specify SPC details //
			    // The output filename //
			    String fileName = "fileName";
			    String fileNameValue = "FCCID_" + curMCCID + "_Inst"; // Can ask user to specify method name //
			    setValues(spc_output, fileName, fileNameValue); // Step 1
			    specifyMainBody(templateOutput,spc_output, fileName, fileNameValue,templateFileName);// Step 2
			    artPath=".\\FCC_Template\\" + SPC_PATH;
			    //RunART(".\\FCC_Template\\" + SPC_PATH); // Run ART Processor
			    String artPath =path + "FCC_" + curFileID + "/";
			    int a = fci_List.size();
			    
			    Statement stmt = CloneRepository.conn.createStatement();
			    stmt.execute("use framerepository;");
			    stmt.execute("insert into frames(path,level,links,issynced) values (\""+Directories.getAbsolutePath(path+SPC_PATH)+"\",5,1,1)");
			    stmt.execute("insert into frames(path,level,links,issynced) values (\""+Directories.getAbsolutePath(path+templatePath)+"\",6,1,1)");
			    
			    int spcid= CloneRepository.getFrameId()-1;
			    stmt.execute("insert into fccframes(fccid,fid) values("+curMCCID+","+spcid+")");
			    
			    MethodCloneWriterDB.insertFrame(curMCCID, a, curMCCID, artPath, 1);
		
			} catch (Exception e)
			{
			    e.printStackTrace();
			}
    }

    private static void specifyMainBody(BufferedWriter templateOutput,BufferedWriter spc_output, String fileName, String fileNameValue,String TemplateFileName) throws IOException
    {

	ArrayList<String> BaseFormat = fci_List.get(0).getAnalysisDetails();
	// int BaseFormatCounter = 0;

	// In While Loop //

	spc_output.write("\n#while ");
	level++;

	specifyWhileVaribles(spc_output, BaseFormat);

	specifyFileName(spc_output, fileName);
	
	String templatePath = Directories.getAbsolutePath("\\ART_Output\\");
	templatePath+="\\FCC_template\\";
	
	spc_output.write("\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	spc_output.write("#adapt: \"" + templatePath + TemplateFileName+"\"");

	specifyFirstGappedRegion(templateOutput, fileNameValue);

	specifyRemainingGappedRegions(templateOutput, fileNameValue, BaseFormat);
	
	spc_output.write("\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	
	spc_output.write("#endadapt");
	
	level--;
	spc_output.write("\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	spc_output.write("#endwhile");
	spc_output.close();
	templateOutput.close();
    }

    private static void specifyRemainingGappedRegions(BufferedWriter spc_output, String fileNameValue,
	    ArrayList<String> BaseFormat) throws IOException
    {
	for (int index = 0; index < BaseFormat.size(); index++)
	{
	    if (index % 2 != 0) // Adapt XX_SCC_Frame
	    {
		String FRAME_PATH = BaseFormat.get(index).split(",")[0] + "_SCC_Frame.art";
		
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
		spc_output.write("#adapt: " + "\""+Directories.getAbsolutePath(Directories.SccFrames+FRAME_PATH)+"\"" + "\n");
		
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}

		spc_output.write("#select fileName\n");
		level++;
			
		for (int numInst = 0; numInst < fci_List.size(); numInst++)
		{
			for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    spc_output.write("#option " + fileNameValue + "_" + numInst + "\n");
		    level++;
		    
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    
		    spc_output.write(
			    "#insert break_" + BaseFormat.get(index).split(",")[0] + "\n");// +
														    // ":");
		    
		    if(fci_List.get(numInst).getAnalysisDetails().size()>index+1)
		    {
			    if ((index + 1 == BaseFormat.size() - 1)) // If Last Index
			    {
			    	String checkForSpecialChars=fci_List.get(numInst).getAnalysisDetails().get(index + 1).replace("?","\\?");
				    //checkForSpecialChars=checkForSpecialChars.replace("@","\\@");
				// if any error comes due to last closing bracket un-comment this
				    spc_output.write(checkForSpecialChars);
				    spc_output.write("");
			    } else
			    {
				String[] tempArray = fci_List.get(numInst).getAnalysisDetails().get(index + 1).split(",");
				System.out.println("temparray len: " + tempArray.length);
				if (tempArray.length > 1)
				{
				    int len = tempArray[0].length() + tempArray[1].length() + tempArray[2].length()
					    + tempArray[3].length() + tempArray[4].length() + 5;
				    
				    String checkForSpecialChars=fci_List.get(numInst).getAnalysisDetails().get(index + 1).replace("?","\\?");
				    //checkForSpecialChars=checkForSpecialChars.replace("@","\\@");
				    spc_output.write(checkForSpecialChars.substring(len));
				}
			    }
		    }
		    
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    spc_output.write("#endinsert\n");
		    level--;
		    
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    
		    spc_output.write("		#endoption\n");
		}
		level--;
		
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
		
		spc_output.write("#endselect\n");
		spc_output.write("#endadapt\n");
	    }
	}
    }

    private static void specifyFirstGappedRegion(BufferedWriter spc_output, String fileNameValue) throws IOException
    {
	// spc_output.write("#break break_start\n");

    	for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
    	spc_output.write("#select fileName\n");
    	level++;
	// Insert First Gapped Region //
	for (int numInst = 0; numInst < fci_List.size(); numInst++)
	{
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
		level++;
	    spc_output.write("#option " + fileNameValue + "_" + numInst + "\n");
	    // spc_output.write(" #insert-after break_start" +
	    // (MCI_List.get(numInst).getAnalysisDetails().get(0)));
	    // spc_output.write(" #endinsert\n");
	    String checkForSpecialChars=fci_List.get(numInst).getAnalysisDetails().get(0).replace("?","\\?");
	    checkForSpecialChars=checkForSpecialChars.replace("@","\\@");
	    
	    for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
	    spc_output.write(checkForSpecialChars+"\n");
	    
	    level--;
	    for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
	    spc_output.write("#endoption\n");
	}
	
	level--;
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	spc_output.write("#endselect\n");
    }

    private static void specifyFileName(BufferedWriter spc_output, String fileName) throws IOException
    {
	spc_output.write("fileName\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	spc_output.write("#output " +"?@path?" +"?@" + fileName + "?" + "\".java\"" + "\n");
    }

    private static void specifyWhileVaribles(BufferedWriter spc_output, ArrayList<String> BaseFormat) throws IOException
    {
	for (SCCTemplate Template : SCCTemplates)
	{
	    if (Template.DynamicTokens.size() == 0)
	    {
		continue;
	    }

	    if (isIncluded(Template.curSCCID, BaseFormat) == false)
	    {
		continue;
	    }

	    for (DynamicToken tk : Template.DynamicTokens)
	    {
		if (tk.marked)
		{
		    continue;
		}
		spc_output.write(tk.getVarName() + ", ");
	    }
	}
    }

    private static void setValues(BufferedWriter spc_output, String fileName, String fileNameValue) throws IOException
    {

	setFileNames(spc_output, fileName, fileNameValue);
	// setFileNames(spc_output_temp, fileName, fileNameValue);
	// Set Variables For Each SCC Instance //
	spc_output.write("% Here I will set all the place-holder variables\n\n");

	// spc_output_temp.write("% Here I will set all the place-holder
	// variables\n\n");
	// int counter = 0;
	//setPlaceHolderVariables(spc_output);
	setVariables(spc_output);
    }

    private static void setVariables(BufferedWriter spc_output) throws IOException
    {
    	
    	for (SCCTemplate Template : SCCTemplates)
    	{
    	    if (Template.DynamicTokens.size() == 0)
    	    {
    		continue;
    	    }

    	  

    	    for (DynamicToken tk : Template.DynamicTokens)
    	    {
    		if (tk.marked || tk.isRepeated())
    		{
    		    continue;
    		}
    		//spc_output.write(tk.getVarName() + ", ");
    		spc_output.write("#set " + tk.getVarName() + " = ");
    		
    		
    		 for (int _index = 0; _index < fci_List.size(); _index++)
 		    {
 			String doubleComma = "";
 			if (tk.isCDoubleComma() || tk.tokenValues.get(_index).contains("\""))
 			{
 			    doubleComma = tk.tokenValues.get(_index).replace("\"", "\\\"");
 			    spc_output.write("\"" + doubleComma + "\"");
 			} else
 			{
 			    spc_output.write("\"" + tk.tokenValues.get(_index) + "\"");
 			}

 			// spc_output_temp.write("\"" + tk.tokenValues.get(_index) + "\"");

 			if (_index != fci_List.size() - 1)
 			{
 			    spc_output.write(", ");

 			    // spc_output_temp.write(", ");
 			} else
 			{
 			    spc_output.write("\n");

 			    // spc_output_temp.write("\n");
 			}
 		    }
 		    
 		    tk.setRepeated(true);
 		}
    		
    		
    		
    	    }
    	}	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	

    
    
    
    private void setPlaceHolderVariables(BufferedWriter spc_output) throws IOException
    {
	for (MCIContainer Container : MCI_Container_List)
	{
	    for (String SCCInfo : Container.SCC_Order)
	    {
		String[] InfoArray = SCCInfo.split(",");
		int SCCID = Integer.parseInt(InfoArray[0]);
		Integer.parseInt(InfoArray[1]);
		SCCTemplate Template = Container.getTemplate(SCCID);

		for (DynamicToken tk : Template.DynamicTokens)
		{
		    if (tk.marked || tk.isRepeated())
		    {
			continue;
		    }
		    spc_output.write("#set " + tk.getVarName() + " = ");
		    System.out.println("#set " + tk.getVarName() + " = ");

		    // spc_output_temp.write("#set " + tk.varName + " = ");
		    for (int _index = 0; _index < fci_List.size(); _index++)
		    {
			String doubleComma = "";
			if (tk.isCDoubleComma() || tk.tokenValues.get(_index).contains("\""))
			{
			    doubleComma = tk.tokenValues.get(_index).replace("\"", "\\\"");
			    spc_output.write("\"" + doubleComma + "\"");
			} else
			{
			    spc_output.write("\"" + tk.tokenValues.get(_index) + "\"");
			}

			// spc_output_temp.write("\"" + tk.tokenValues.get(_index) + "\"");

			if (_index != fci_List.size() - 1)
			{
			    spc_output.write(", ");

			    // spc_output_temp.write(", ");
			} else
			{
			    spc_output.write("\n");

			    // spc_output_temp.write("\n");
			}
		    }
		    
		    tk.setRepeated(true);
		}
	    }
	}
    }

    private static void setFileNames(BufferedWriter spc_output, String fileName, String fileNameValue) throws IOException
    {
	spc_output.write("#set " + fileName + " = ");
	for (int _index = 0; _index < fci_List.size(); _index++)
	{
	    spc_output.write("\"" + fileNameValue + "_" + _index + "\"");
	    if (_index != fci_List.size() - 1)
	    {
		spc_output.write(", ");
	    } else
	    {
		spc_output.write("\n");
	    }
	}
	spc_output.write("\n");
	spc_output.write("#set path =");
    spc_output.write("\"");
    spc_output.write(Directories.getAbsolutePath(Directories.framingOutput));
    spc_output.write("\""+"\n");
    }

    public static void RunART(String filename)
    {
	Initializer.ExecuteART(filename);
    }

    private static void refresh(String SPC_path)
    {
	File f1 = new File(SPC_path);
	f1.delete();
    }

    public static boolean isIncluded(int SCCID, ArrayList<String> BaseFormat)
    {
	boolean isSet = false;
	for (int index = 0; index < BaseFormat.size(); index++)
	{
	    if (index % 2 != 0)
	    {
		if (Integer.parseInt(BaseFormat.get(index).split(",")[0]) == SCCID)
		{
		    isSet = true;
		}
	    }
	}
	return isSet;
    }

    public static String RemoveLine(String inputText)
    {
	Pattern p;
	Matcher m;
	System.out.println(" Input Text : " + inputText);
	p = Pattern.compile("\n");
	m = p.matcher(inputText);
	String str = m.replaceAll("");
	System.out.println(" OUtput Text: " + str);
	return str;
    }

    
    public static void genFCCTemplate(int fccId)
    {   
    	
    	for (FileCluster fcc : ClonesReader.getFileCloneList())
    	{
    		if (fcc.getClusterID() == fccId)
    		{
    			System.out.println("About to generate FCC Template for FCCID: " + fccId);
    			new FCCTemplate(fccId, fcc);
    			return;
    		}
	}
	System.out.println("Could not find specified FCCID!");
    }
    


}
