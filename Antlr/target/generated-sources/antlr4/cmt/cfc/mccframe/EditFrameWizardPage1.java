package cmt.cfc.mccframe;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;

import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.utility.ExternalThread;
import cmt.cfc.utility.FileHandler;

public class EditFrameWizardPage1 extends WizardPage
{

    int totalTabs;// = FileHandler.fileHandler.listOfARTFiles.size();
    public static MethodClones mcc;
    ArrayList<String> variableNamesList;

    ArrayList<TabItem> tabItems = new ArrayList<TabItem>();
    ArrayList<StyledText> textARTCodes = new ArrayList<StyledText>();
    int mccId;
    Button Cancel;
    Button Compile;
    Button update;
    Button reset;
    Button showWizard;
    StyledText ARTCode;
    StyledText ARTWarning;
    TabFolder tabFolder;
    Composite composite;
    Table table;
    EditFrameWizardPage2 page2;
    EditFrameWizard wizard;

    protected EditFrameWizardPage1(int mccId, EditFrameWizard editFrameWizard)
    {
	// TODO Auto-generated constructor stub

	super("ART Code Frame");
	this.mccId = mccId;
	this.wizard = editFrameWizard;
	FileHandler.fileHandler = new FileHandler("MCC_Template\\+MCC_"+mccId);
	FileHandler.fileHandler.listOfJAVAFiles.size();
	totalTabs = FileHandler.fileHandler.listOfARTFiles.size();
	setTitle("ART Code Frame");
	setDescription("ART code is displayed;\n" + "User can update ART Code here.");

    }

    @Override
    public void createControl(Composite parent)
    {
	composite = new Composite(parent, SWT.NONE);
	GridLayout gridLayout = new GridLayout(1, false);
	composite.setLayout(gridLayout);
	GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
	gridData.grabExcessHorizontalSpace = true;
	new Label(composite, SWT.NULL);

	// tabs work starts here

	GridData TabgridData = new GridData(GridData.FILL_HORIZONTAL | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	tabFolder = new TabFolder(composite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	TabgridData.heightHint = 250;
	// TabgridData.widthHint = 50;
	tabFolder.setLayoutData(TabgridData);
	insertTabs(composite, tabFolder);
	// buttons work starts here
	Composite buttonsLayout = new Composite(composite, SWT.NONE);
	FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
	fillLayout.spacing = 10;
	// fillLayout.
	buttonsLayout.setLayout(fillLayout);
	// Text area for error message
		GridData textArea = new GridData(GridData.FILL_HORIZONTAL);
		ARTWarning = new StyledText(composite, SWT.H_SCROLL|SWT.V_SCROLL);
		textArea.heightHint = 100;
		
		ARTWarning.setLayoutData(textArea);
		ARTWarning.setText("Errors will be displayed here.");
		ARTWarning.setEditable(false);

		Compile = new Button(buttonsLayout, SWT.PUSH);
		Compile.setText("Compile");
		Compile.setEnabled(false);
	reset = new Button(buttonsLayout, SWT.PUSH);
	reset.setText("Reset");
	reset.setEnabled(true);
	reset.addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub
		/*** Insert file contents in text field *****/
		// System.out.println("are we here ? ");

		for (int j = 0; j < totalTabs; j++)
		{
		    ArrayList<String> fileContents = new ArrayList<String>(
		    		FileHandler.fileHandler.getFileContents(FileHandler.fileHandler.artFilesPaths.get(j)));

		    if (!fileContents.isEmpty())
		    {
			insertText(textARTCodes.get(j), fileContents);
		    }
		    

		}
		
		setPageComplete(true);
		Compile.setEnabled(false);
		ARTWarning.setText("Errors details of ART files will be display here.");

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});

	Compile.addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    	FileHandler.fileHandler.createTempFile(FileHandler.fileHandler, textARTCodes);// (SampleHandler.fileHandler.listOfVCLFiles.size(),
		      // SampleHandler.fileHandler,
		      // textVCLCodes);

//	    	FileHandler.fileHandler.createUpdatedTempJavaFiles(mccId);
			// System.out.println("changes are done!! ");
			// EditFrameWizardPage2 edt = new EditFrameWizardPage2(mccId);
			// edt.updatetabs();*/
	    	if(ExternalThread.errorPrompt != "" && ExternalThread.errorPrompt.contains("Error"))
			 {
				 //if there is some string of error in this then show & don't process further ... else clear error prompt that no irrelevant things are shown
	    		ARTWarning.setText(ExternalThread.errorPrompt );
					//errorBox.open(); 
					System.err.println("Error detected before join" + ExternalThread.errorPrompt );
					//break;
					setPageComplete(false);
			 }
			 else
			 {
				 ExternalThread.errorPrompt = "";
				 FileHandler.fileHandler.moveTempFiles(FileHandler.fileHandler);
				 setPageComplete(true);
			 }
			

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub
	    }
	});
	/// buttons ends here
	setControl(composite);
    }

    @Override
    public IWizardPage getNextPage()
    {

	// System.out.println("hey is getnextpage working??");
	TabItem abc;
	StyledText textARTCode;
	TabFolder tabFolder2;
	Composite composite2;
	page2 = wizard.getPage2();
	page2.getTabItems();
	tabFolder2 = page2.tabFolder;
	composite2 = page2.composite;

	for (int i = 0; i < page2.totalTabs; i++)
	{

	    abc = page2.getTabItems().get(i);
	    abc.setText(FileHandler.fileHandler.listOfJAVAFiles.get(i));
	    textARTCode = new StyledText(tabFolder2, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	    textARTCode.setLayoutData(composite2);
	    textARTCode.setEditable(false);
	    textARTCode.setText("");
	    textARTCode.setEditable(true);
	    abc.setControl(textARTCode);

	    ArrayList<String> fileContents = new ArrayList<String>(
	    		FileHandler.fileHandler.getFileContents(FileHandler.fileHandler.javaFilesPaths.get(i)));

	    if (!fileContents.isEmpty())
	    {
		page2.insertText(textARTCode, fileContents);
	    }

	}

	return page2;

    }

    public void insertTabs(Composite composite, TabFolder tabFolder)
    {
	TabItem tabItem;

	StyledText textARTCode;

	for (int i = 0; i < totalTabs; i++)
	{
	    tabItem = new TabItem(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

	    tabItem.setText(FileHandler.fileHandler.listOfARTFiles.get(i));

	    textARTCode = new StyledText(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	    textARTCode.setLayoutData(composite);
	    textARTCode.setEditable(false);
	    tabItem.setControl(textARTCode);
	    textARTCode.setText("");
	    textARTCode.setEditable(true);

	    ArrayList<String> fileContents = new ArrayList<String>(
	    		FileHandler.fileHandler.getFileContents(FileHandler.fileHandler.artFilesPaths.get(i)));

	    if (!fileContents.isEmpty())
	    {
		insertText(textARTCode, fileContents);
	    }
	    tabItems.add(tabItem);
	    textARTCodes.add(textARTCode);
	}
    }

    /*********** Populate text field with file contents *************/
    public void insertText(StyledText ARTCode, ArrayList<String> textContents)
    {
	ARTCode.setText(""); // Removes old contents
	for (int i = 0; i < textContents.size(); i++)
	{
	    ARTCode.append(textContents.get(i) + "\n");
	}
	ARTCode.addModifyListener(new ModifyListener() {
		
		@Override
		public void modifyText(ModifyEvent e) {
			// TODO Auto-generated method stub
			setPageComplete(false);
			Compile.setEnabled(true);
			
		}
	});
    }
    

}
