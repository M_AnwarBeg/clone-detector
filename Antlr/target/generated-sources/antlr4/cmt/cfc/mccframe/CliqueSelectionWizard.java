package cmt.cfc.mccframe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.structures.sMethod;
import cmt.cvac.views.MccView;

public class CliqueSelectionWizard extends Wizard implements INewWizard
{

    CliqueSelectionWizardPage1 page1;
    CliqueSelectionWizardPage2 page2;
    IViewPart view;

    ArrayList<Integer> clique;
    static MethodClones mcc;
    int mccid = -1;
    private int mccIndex = 0;

    public CliqueSelectionWizard()
    {

	super();
	view = null;
    }

    public void setMCCID(int _mccid)
    {
	this.mccid = _mccid;

    }

    public int getMCCID()
    {
	return mccid;

    }

    @Override
    public void addPages()
    {

	page1 = new CliqueSelectionWizardPage1(this);
	addPage(page1);

	page2 = new CliqueSelectionWizardPage2(this);
	addPage(page2);

    }

    public CliqueSelectionWizardPage2 getPage2()
    {
	return page2;
    }

    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
	// TODO Auto-generated method stub

	this.setNeedsProgressMonitor(true);
	this.setHelpAvailable(false);
	this.setForcePreviousAndNextButtons(true);

    }

    @Override
    public boolean performFinish()
    {

	/*
	 * File file=new File(path); String[]entries = file.list(); for(String s:
	 * entries){ File currentFile = new File(file.getPath(),s);
	 * currentFile.delete(); }
	 */
	setFramed();
	final MccView mccView = (MccView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
		.findView("mccView");
	int i = mccView.primaryMccViewer.getTable().getSelectionIndex();
	setForeground(mccView, i);
	setBackground(mccView, i);
	enableEditFrameButton(mccView);
	enableDeleteFrameButton(mccView);
	disbaleCliqueSelectionButton(mccView);
	return true;
    }

    private void disbaleCliqueSelectionButton(final MccView mccView)
    {
	mccView.cliqueSelectionWizardAction.setEnabled(false);
    }

    private void enableEditFrameButton(final MccView mccView)
    {
	mccView.editFrameWizardAction.setEnabled(true);
    }
    private void enableDeleteFrameButton(final MccView mccView)
    {
	mccView.deleteFrameAction.setEnabled(true);
    }

    private void setBackground(final MccView mccView, int i)
    {
	final org.eclipse.swt.graphics.Color green = new org.eclipse.swt.graphics.Color(null, 192, 255, 192);
	mccView.primaryMccViewer.getTable().getItem(i).setBackground(green);
    }

    private void setForeground(final MccView mccView, int i)
    {
	Display display = this.getShell().getDisplay();
	final org.eclipse.swt.graphics.Color blue = display.getSystemColor(SWT.COLOR_BLUE);
	mccView.primaryMccViewer.getTable().getItem(i).setForeground(blue);
    }

    private void setFramed()
    {
	ClonesReader.getMethodClones().get(mccIndex).setFramed(true);
    }

    @Override
    public boolean performCancel()
    {

	/*
	 * File file=new File(path); String[]entries = file.list(); for(String s:
	 * entries){ File currentFile = new File(file.getPath(),s);
	 * currentFile.delete(); }
	 */
	return true;
    }

    @Override
    public boolean canFinish()
    {
	// if(getContainer().getCurrentPage() != page5)
	// return false;
	// else
	// return true;

	if (getContainer().getCurrentPage() == page2)
	{
	    return true;
	} else
	{
	    return false;
	}

    }

    @Override
    public void createPageControls(Composite pageContainer)
    {
    }

    public static void markMethods(int m)
    {
    	for(int i=0; i<ClonesReader.getMethodClones().size(); i++)
    	{
    		if(ClonesReader.getMethodClones().get(i).getClusterID() == m)
    		{
    			mcc = ClonesReader.getMethodClones().get(i);
    		}
    	}
	for (MethodCloneInstance instance : mcc.getMCCInstances())
	{
	    sMethod currentMethod = instance.getMethod();
	    File fileToOpen = new File(currentMethod.getFilePath());
	    String text = getFileContent(fileToOpen);
	    IFile ifile = getIFile(fileToOpen);
	    try
	    {
		IMarker marker = ifile.createMarker("com.plugin.clonemanager.customtextmarker");
		/*int startChar = text.indexOf(currentMethod.getCodeSegment());
		int offset = currentMethod.getCodeSegment().length();*/
		marker.setAttribute(IMarker.LINE_NUMBER, currentMethod.getStartToken());
		//marker.setAttribute(IMarker.CHAR_START, startChar+2);
		//marker.setAttribute(IMarker.CHAR_END, startChar+2 + offset);
		marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
		marker.setAttribute(IMarker.LOCATION, "1");
		marker.setAttribute(IMarker.DONE, true);
	    } catch (Exception e)
	    {
	    	//e.printStackTrace();
	    }
	}
    }

    private static IFile getIFile(File fileToOpen)
    {
	IPath location = Path.fromOSString(fileToOpen.getAbsolutePath());
	IWorkspace workspace = ResourcesPlugin.getWorkspace();
	return workspace.getRoot().getFileForLocation(location);
    }

    private static String getFileContent(File fileToOpen)
    {
	String content = "";
	try
	{
	    FileInputStream fis = new FileInputStream(fileToOpen);
	    byte[] data = new byte[(int) fileToOpen.length()];
	    fis.read(data);
	    fis.close();
	    content = new String(data, "UTF-8");
	} catch (IOException e2)
	{
	    e2.printStackTrace();
	}
	return content;
    }

    public int getMccIndex()
    {
	return mccIndex;
    }

    public void setMccIndex(int mccIndex)
    {
	this.mccIndex = mccIndex;
    }

    private IMarker[] findMarkers(IResource target) throws CoreException
    {
	String type = "com.plugin.clonemanager.customtextmarker";
	IMarker[] markers = target.findMarkers(type, true, IResource.DEPTH_INFINITE);
	return markers;
    }

}
