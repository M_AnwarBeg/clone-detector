package cmt.cfc.mccframe;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.LoadListSetting;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.SCCTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.awt.Color;
import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.custom.CCombo;
import javax.swing.JTable;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.layout.TableColumnLayout;
public class BreaksUI extends Dialog {
	
	
	protected Object result;
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	//ArrayList<Integer> breakNames = new ArrayList<Integer>();
	private Text text;
	private Text text_1;
	private Table table;
	public static ArrayList<FrameBreak> breakNames = new ArrayList<FrameBreak>();
	
	
	
	
	
	public BreaksUI(Shell parent, int style) {
		super(parent, style);
		setText("Rename Breaks");
	}	
	
	public Object open() {
		createContents();
		shell.open();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	//set variables for renaming
//	public void setVariablesForNaming(SCCTemplate temp) {
//		this.temp = temp;
//		  StringBuilder structure = new StringBuilder();
//			
//			//for(Integer sccId: mcc.getNewSccs_Contained().g) {
//				//structure.append(sccId).append(",");
//			}		
			
	
	
	
	public void renameBreak(MethodClones MCC ) {
		
		
		breakNames = new ArrayList<FrameBreak>();
		
		ArrayList<Integer> newSccs = new ArrayList<Integer>();
		newSccs=getSCCs(MCC.getMCCInstances().get(0));
		for(int i=0; i<newSccs.size(); i++)
		{	
			FrameBreak fr= new FrameBreak();
		fr.setName("break_" + newSccs.get(i));
		fr.setSCCId(newSccs.get(i));
		
		breakNames.add(i, fr);
		}
		
	}
		
	
	public ArrayList<Integer> getSCCs(MethodCloneInstance m )
	{
		ArrayList<Integer> scc = new ArrayList<Integer>();
		for(int i=0; i<m.getNewSccs_Contained().size(); i++)
		{
			scc.add(m.getNewSccs_Contained().get(i).getSCCID());
			
		}
		
		
		return scc;
		
	}
	
	private void createContents() {
		// TODO Auto-generated method stub
		shell = new Shell( SWT.CLOSE | SWT.TITLE);
		shell.setText("Mapp MCS.");
		

		//shell = new Shell(getParent(), getStyle());
		shell.setSize(850, 482);
		shell.setText(getText());
		
		Group group = new Group(shell, SWT.NONE);
		group.setBounds(10, 28, 824, 415);
		
		text = new Text(group, SWT.BORDER);
		text.setBounds(108, 40, 131, 26);
		
		text_1 = new Text(group, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text_1.setBounds(388, 97, 357, 209);

		
		text_1.setEditable(false);
		text_1.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		Label lblName = new Label(group, SWT.NONE);
		lblName.setBounds(33, 43, 69, 26);
		lblName.setText("Break Name");
		
		Label lblcodeSegment = new Label(group, SWT.NONE);
		lblName.setBounds(388, 80, 210, 26);
		lblName.setText("Code Segment of the Selected Break");
		
		Button btnDone = new Button(group, SWT.NONE);
		btnDone.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnDone.setBounds(426, 363, 75, 25);
		btnDone.setText("Next >");
		
		Button btnReplace = new Button(group, SWT.NONE);
		btnReplace.setBounds(268, 40, 75, 26);
		btnReplace.setText("Replace");
		
		table = new Table(group, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(79, 97, 233, 209);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnBreaks = new TableColumn(table, SWT.NONE);
		tblclmnBreaks.setWidth(100);
		tblclmnBreaks.setText("Breaks");
		
		TableColumn tblclmnSccs = new TableColumn(table, SWT.NONE);
		tblclmnSccs.setWidth(257);
		tblclmnSccs.setText("SCC's");
		
		table.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				TableItem item= table.getItem(table.getSelectionIndex());			
				String sccNum = item.getText();
				StringTokenizer st = new StringTokenizer(sccNum, "_");
				st.nextToken();
				sccNum = st.nextToken();
				
				for(int i=0; i<MCCTemplate.MCI_List.get(0).getAnalysisDetails().size(); i++)
				{
					String check = MCCTemplate.MCI_List.get(0).getAnalysisDetails().get(i);
					st = new StringTokenizer(MCCTemplate.MCI_List.get(0).getAnalysisDetails().get(i), ",");
					check = st.nextToken();
					if(sccNum.equals(check))
					{
						text_1.redraw();
						st = new StringTokenizer(MCCTemplate.MCI_List.get(0).getAnalysisDetails().get(++i), ",");
						while(st.hasMoreTokens())
						{
							check = st.nextToken();
						}
						text_1.setText(check);
					}
				}
			}
		});
		
		
		 final TableColumn [] columns = table.getColumns ();
		 for (int i = 0; i < breakNames.size(); i++)
		{	
		TableItem item= new TableItem(table,SWT.NONE);
		//item.setText("break_"+(breakNames.get(i)).toString());
		item.setText(breakNames.get(i).getName());
		item.setText(1,(Integer.toString((breakNames.get(i)).getSCCId())));
		}
		 
		btnReplace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
			
			TableItem item= table.getItem(table.getSelectionIndex());			
			
			item.setText(text.getText());
			breakNames.set(table.getSelectionIndex(), new FrameBreak(text.getText(),
								breakNames.get(table.getSelectionIndex()).getSCCId()));	
				
				
			}
		});
		
		
		
	}
}
