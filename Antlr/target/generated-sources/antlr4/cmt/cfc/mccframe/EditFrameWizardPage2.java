package cmt.cfc.mccframe;

import java.util.ArrayList;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;


import cmt.cfc.utility.FileHandler;



public class EditFrameWizardPage2 extends WizardPage
{

    Button Save;
    Button update;
    Button reset;
    Button showWizard; 
    StyledText ARTCode;
    TabFolder tabFolder;
    Composite composite;
    Table table;
    int totalTabs = FileHandler.fileHandler.listOfJAVAFiles.size();
    public ArrayList<TabItem> tabItems = new ArrayList<TabItem>();

    public ArrayList<TabItem> getTabItems()
    {
	return tabItems;
    }

    public void setTabItems(ArrayList<TabItem> tabItems)
    {
	this.tabItems = tabItems;
    }

    ArrayList<StyledText> textARTCodes = new ArrayList<StyledText>();

    protected EditFrameWizardPage2(int mccId)
    {
	// TODO Auto-generated constructor stub

	super("ART Code Frame");
	FileHandler.fileHandler = new FileHandler("MCC_Template\\+MCC_"+mccId);
	FileHandler.fileHandler.listOfJAVAFiles.size();
	setTitle("ART Code Frame");
	setDescription("Updated Java Code");

    }

    @Override
    public void createControl(Composite parent)
    {
	// TODO Auto-generated method stub
	composite = new Composite(parent, SWT.NONE);
	GridLayout gridLayout = new GridLayout(1, false);
	composite.setLayout(gridLayout);
	GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
	gridData.grabExcessHorizontalSpace = true;
	new Label(composite, SWT.NULL);
	// tabs work starts here

	GridData TabgridData = new GridData(GridData.FILL_HORIZONTAL | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	tabFolder = new TabFolder(composite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	TabgridData.heightHint = 250;
	// TabgridData.widthHint = 50;
	tabFolder.setLayoutData(TabgridData);
	insertTabs(composite, tabFolder);
	System.out.println("when we come here????");
	setControl(composite);

    }

    public void insertTabs(Composite composite, TabFolder tabFolder)
    {
    	TabItem tabItem;

    	StyledText textARTCode;
    	for (int i = 0; i < totalTabs; i++)
    	{
    	    tabItem = new TabItem(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

    	    tabItem.setText(FileHandler.fileHandler.listOfJAVAFiles.get(i));

    	    textARTCode = new StyledText(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
    	    textARTCode.setLayoutData(composite);
    	    textARTCode.setEditable(false);
    	    // textARTCode.setText("");
    	    //textARTCode.setEditable(true);
    	    tabItem.setControl(textARTCode);

    	    /*
    	     * ArrayList<String> fileContents = new ArrayList<String>(
    	     * SampleHandler.fileHandler.getFileContents(SampleHandler.fileHandler.
    	     * javaFilesPaths .get(i))); if (!fileContents.isEmpty()) {
    	     * insertText(textARTCode, fileContents); }
    	     */
    	    tabItems.add(tabItem);
    	    textARTCodes.add(textARTCode);
    	    //textARTCodes.setEditable(false);
    	}
    	
    	setTabItems(tabItems);
        }

    public void insertText(StyledText ARTCode, ArrayList<String> textContents)
    {
	ARTCode.setText(""); // Removes old contents
	for (int i = 0; i < textContents.size(); i++)
	{
	    ARTCode.append(textContents.get(i) + "\n");
	    ARTCode.setEditable(false);
	}
    }

    public void updatetabs()
    {
	System.out.print("and now how many tabs we have ??? " + totalTabs);
	tabItems = getTabItems();
	tabItems.get(1);

	for (int i = 0; i < totalTabs; i++)
	{
	    // abc = this.tabItems.get(i);
	    // tabItem.setText(SampleHandler.fileHandler.listOfJAVAFiles.get(i));

	}

    }

}
