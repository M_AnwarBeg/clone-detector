package cmt.cfc.mccframe;
import java.io.*;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.LoadListSetting;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.SCCTemplate;
import cmt.common.Directories;

import org.eclipse.swt.widgets.Composite;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.custom.CCombo;
import javax.swing.JTable;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.swt.events.DragDetectListener;
import org.eclipse.swt.events.DragDetectEvent;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
public class WizardPage3 extends WizardPage implements Listener  {
	
	private RenamingWizard wizard;
	protected Object result;
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	ArrayList<FrameBreak> breakNames = new ArrayList<FrameBreak>();
	private Text text_1;
	int index;
	String path=Directories.getAbsolutePath(Directories.SccFrames);
	 String frame="";
	 String firstLine="*";
	 int dragLength=0;
	static ArrayList<Integer> sccs= new ArrayList<Integer>();
	public WizardPage3(RenamingWizard wizard) {
		// TODO Auto-generated constructor stub
		super("Final");
		
		setTitle("Edit Frame");
		setDescription("Frame generated: ");
		this.wizard = wizard;
	}
			
	
	public void includeSccs()
	{
		sccs=MCCTemplate.sccsIncluded;
		
	}
	
	String getFrameCode(String fName)
	{
		String frCode="";
		String line = null;
		try {
            // FileReader reads text files in the default encoding.
            FileReader fileReader = new FileReader(path+fName);

            // Always wrap FileReader in BufferedReader.
            BufferedReader bufferedReader = 
                new BufferedReader(fileReader);

            while((line = bufferedReader.readLine()) != null) {
               // System.out.println(line);
            	if(firstLine.contentEquals("*"))
            	{
            	firstLine=line;
            	}
                frCode=frCode.concat(line);
                frCode=frCode.concat("\n");
            }   

            // Always close files.
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fName + "'");                  
            // Or we could just do this: 
            // ex.printStackTrace();
        }
		
		return frCode;
	}
	
	
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		Composite container = new Composite(parent, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		setControl(container);
		index=0;
		includeSccs();
		
			
		text_1 = new Text(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		
		text_1.setBounds(10, 53, 413, 228);
		
		
		
        // This will reference one line at a time
		
		
		String fileName;
		fileName=(Integer.toString(sccs.get(index))+"_SCC_Frame.art");
		//fileName="0_SCC_Frame.art";
		frame=getFrameCode(fileName);
		 
			
			text_1.setText(frame);
			
			 text_1.setEditable(true);
			    text_1.setBackground(container.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		frame=null;
		fileName=null;
		
		
		
		
		
		
		
		
		Label lblSccId = new Label(container, SWT.NONE);
		lblSccId.setBounds(10, 22, 54, 15);
		lblSccId.setText("SCC ID:");
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setBounds(70, 22, 86, 15);
		lblNewLabel.setText(Integer.toString(sccs.get(index)));
		
		Button btnNextFrame = new Button(container, SWT.NONE);
		btnNextFrame.setBounds(337, 298, 86, 25);
		btnNextFrame.setText("Next Frame");
		Button buttonPrev = new Button(container, SWT.NONE);
		buttonPrev.setText("Previous Frame");
		buttonPrev.setBounds(245, 298, 86, 25);
		buttonPrev.setEnabled(false);
		
		Button btnSaveFrame = new Button(container, SWT.NONE);
		btnSaveFrame.setBounds(448, 298, 86, 25);
		btnSaveFrame.setText("Save Frame");
		btnSaveFrame.setEnabled(false);
		if(sccs.size()==1)
		{
			btnNextFrame.setEnabled(false);
			buttonPrev.setEnabled(false);
		}
		
		
		
//		text_1.addModifyListener(new ModifyListener() {
//			public void modifyText(ModifyEvent arg0) {
//				btnSaveFrame.setEnabled(true);
//			}
//			
//		});
//		
		
		
		text_1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				btnSaveFrame.setEnabled(true);
			}
		});
		
		
		
		
		
		
		//Next Frame
		btnNextFrame.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				
				btnSaveFrame.setEnabled(false);
				
				
				
				
				if(index+1<sccs.size())
				{
					index++;
				}
				if(index+1==sccs.size())
				{
					btnNextFrame.setEnabled(false);
					buttonPrev.setEnabled(true);
				}
				else
				{
					btnNextFrame.setEnabled(true);
					buttonPrev.setEnabled(true);
					
				}
				
				
					String fileName;
					fileName=(Integer.toString(sccs.get(index))+"_SCC_Frame.art");
					lblNewLabel.setText(Integer.toString(sccs.get(index))); 
				frame=getFrameCode(fileName);
				text_1.clearSelection();
				text_1.redraw();
				text_1.setText(frame);
				
				 text_1.setEditable(true);
				    text_1.setBackground(container.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				frame=null;
				fileName=null;
				
			}
		});
		
		
		
		
		
		
		
		
		
		
		
		buttonPrev.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				btnSaveFrame.setEnabled(false);
				
				
				
				if(index-1>=0)
				{
					index--;
				}
				if(index==0)
				{
					buttonPrev.setEnabled(false);
					btnNextFrame.setEnabled(true);
				}
				else
				{
					buttonPrev.setEnabled(true);
					btnNextFrame.setEnabled(true);
				}
				
					String fileName;
					fileName=(Integer.toString(sccs.get(index))+"_SCC_Frame.art");
					lblNewLabel.setText(Integer.toString(sccs.get(index))); 
				frame=getFrameCode(fileName);
				text_1.clearSelection();
				text_1.redraw();
				text_1.setText(frame);
				
				 text_1.setEditable(true);
				    text_1.setBackground(container.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				frame=null;
				fileName=null;
			}
		});
		
		
		
		btnSaveFrame.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				String frameToSave="";
				frameToSave=text_1.getText();
				String fName=lblNewLabel.getText();
				 try{    
			           FileWriter fw=new FileWriter(path+fName+"_SCC_Frame.art");    
			           fw.write(frameToSave);    
			           fw.close();    
			           
			           
			           JFrame f= new JFrame();
			           JOptionPane.showMessageDialog(f,"Frame Saved"); 
			           btnSaveFrame.setEnabled(false);
			           
			           
			          }
				 catch(Exception a)
				 		{		
			        	  	System.out.println(a);
			        	}  
				
				
				
				
				
			}
		});
		

		
		
		
		
		
	}
	
	 public IWizardPage getPage()
	 	{
			return WizardPage3.this;
		 
	    }
	


	@Override
	public void handleEvent(Event arg0) {
		// TODO Auto-generated method stub
		
	}
}
