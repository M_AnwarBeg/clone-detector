package cmt.cfc.mccframe;

import java.util.ArrayList;

public class McFrameRepo {
	
	private int mcc_id;
	private ArrayList<Integer> sccs;
	
	public McFrameRepo() 
	{
		this.mcc_id=-1;
		this.sccs=new ArrayList<Integer>();
	}
	
	public void setMccId(int id)
	{
		mcc_id=id;
	}
	
	public int getMccId()
	{
		return mcc_id;
	}
	
	public void addScc(int sccid)
	{
		this.sccs.add(sccid);
	}
	
	public ArrayList<Integer> getSccs()
	{
		return sccs;
	}

}
