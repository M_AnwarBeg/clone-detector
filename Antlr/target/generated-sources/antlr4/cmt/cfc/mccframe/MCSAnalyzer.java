package cmt.cfc.mccframe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;

public class MCSAnalyzer {
		
		
		
	public static ArrayList<Integer> analyzeMCS(MethodCloneStructure mcs) {
		
		ArrayList<Integer> mcsStructure = mcs.getvCloneClasses();
		HashMap<Integer,Integer> IdOccuranceCount = countInstancePerFile(mcs.getvCloneClasses());
		
		ArrayList<MethodClones> mccsInMCS = mcs.getMccs();
		Iterator mccItr = mccsInMCS.iterator();
		
		HashMap<Integer, List<Integer>> structurePerFile = new HashMap<Integer, List<Integer>>();
		while(mccItr.hasNext()) {
			MethodClones mcc = (MethodClones) mccItr.next();
			Iterator mccInstanceItr = mcc.getMCCInstances().iterator();
			while(mccInstanceItr.hasNext()) {
				MethodCloneInstance mccInstance = (MethodCloneInstance) mccInstanceItr.next();
				Integer fileId = mccInstance.getMethod().getFileID();
				if(structurePerFile.get(fileId)==null)
				{
					ArrayList<Integer> structure = new ArrayList<Integer>();
					structure.add(mcc.getClusterID());
					structurePerFile.put(fileId, structure);					
				}
				
				else {
					structurePerFile.get(fileId).add(mcc.getClusterID());
				}
			}
		}
		
		ArrayList<Integer> markFilesForMapping = new ArrayList<Integer>();
		for (HashMap.Entry<Integer, List<Integer>> entry : structurePerFile.entrySet()) {
			Integer fileId = entry.getKey();
			List<Integer> structure = entry.getValue();
			if(!compareMap(IdOccuranceCount,countInstancePerFile(structure)))
				markFilesForMapping.add(fileId);
		}
		
		return markFilesForMapping;
	}
	
	
	private static HashMap<Integer,Integer> countInstancePerFile(List<Integer> structure){
		HashMap<Integer,Integer> IdOccuranceCount = new HashMap<Integer,Integer>();		
		for(int mccId : structure) {
			Integer count = IdOccuranceCount.get(mccId);
			if (count != null) {
				count = count + 1;
				IdOccuranceCount.put(mccId, count);
			} else {
				IdOccuranceCount.put(mccId, new Integer(1));
			}
		}
		return IdOccuranceCount;		
	}
	
	
	/*private boolean matchStructure(HashMap<Integer,Integer> requiredStructure, List<Integer> CheckStructure)
	{
		HashMap<Integer,Integer> IdOccuranceCount = new HashMap<Integer,Integer>();
		
		for(int mccId : mcs.getvCloneClasses()) {
			Integer count = IdOccuranceCount.get(mccId);
			if (count != null) {
				count = count + 1;
			} else {
				IdOccuranceCount.put(mccId, new Integer(1));
			}
	}*/
		
		
		
	public static boolean compareMap(Map<Integer, Integer> map1, Map<Integer, Integer> map2) {
	    if (map1 == null || map2 == null)
	        return false;

	    for (Integer key : map2.keySet()) {
	        if (map1.get(key)==null)
	            return false;
	    }    
	    
	    for (Integer key : map2.keySet()) {
	      Integer value = map1.get(key);
	    	if(!(map2.get(key).equals(value)))
	    		return false;
	    }	   
	    return true;
	}
	
	
	
	
}
