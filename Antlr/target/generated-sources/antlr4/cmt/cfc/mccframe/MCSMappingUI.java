package cmt.cfc.mccframe;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.LoadListSetting;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;

import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.awt.Color;
import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
public class MCSMappingUI extends Dialog {

	protected Object result;
	protected Shell shell;
	private MethodCloneStructure mcs;
	private int fileInProcess = 0;
	private int totalFiles;
	private String targetStructure ;
	private static ArrayList<ArrayList<Integer>> checkCompleteMapping = new ArrayList<ArrayList<Integer>>();
	String[] selectedMethods;
	private int currentMccId=0;
	/**
	 * Create the dialog.
	 * @param parent
	 * @param style
	 */
	public MCSMappingUI(Shell parent, int style) {
		super(parent, style);
		setText("Mapping Structure");
	}
	

	/**
	 * Open the dialog.
	 * @return the result
	 */
	public Object open() {
		createContents();
		shell.open();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	
	
	// This should be improved.
	private void markSelectedMethods() {
	for(int i = 0 ; i < selectedMethods.length ; i++ ) {	
	 String[] methodDetails = selectedMethods[i].split("~");
		for(MethodClones mcc: mcs.getMccs()) {
		 for(MethodCloneInstance mci : mcc.getMCCInstances()) {
			 if(mci.getMethod().getMethodName().equals(methodDetails[1])) {
				 mci.getMethod().setMark(true);
			 }
		 }
		 
	 }
	}
	}
	
	
	public void setMethodCloneStructure(MethodCloneStructure mcs) {
		this.mcs = mcs;
		  StringBuilder structure = new StringBuilder();
			
			for(Integer mccId: mcs.getvCloneClasses()) {
				structure.append(mccId).append(",");
			}		
			structure.deleteCharAt(structure.length() - 1).toString();		
			targetStructure = structure.toString();
	} 
		
	
	
	private ArrayList<String> extractMCCMethodsInCurrentFile(int mccId){
		
		Iterator mccItr = mcs.getMccs().iterator();
		MethodClones mcc = null;
		while(mccItr.hasNext()) {
		 mcc = (MethodClones) mccItr.next();
		 if(mcc.getClusterID() == mccId) {
			break;
		 }	
		}
		ArrayList<String> methodNames = new ArrayList<String>();
		for(MethodCloneInstance mci : mcc.getMCCInstances()) {
			if(mci.getMethod().getFileID()== MCSTemplate.markFilesForMapping.get(fileInProcess)) {
				String methodName = mci.getMethod().getMethodName();
				methodNames.add(methodName);	
			}
		}
		return methodNames;
	}
	
	private String extractMethodBody(int mccId,String methodName) {
		
		String code = null;
		Iterator mccItr = mcs.getMccs().iterator();
		MethodClones mcc = null;
		while(mccItr.hasNext()) {
			 mcc = (MethodClones) mccItr.next();
			 if(mcc.getClusterID() == mccId) {
				break;
			 }	
		}
		
		for(MethodCloneInstance mci : mcc.getMCCInstances()) {
			if(mci.getMethod().getFileID()== MCSTemplate.markFilesForMapping.get(fileInProcess)) {
				if((mci.getMethod().getMethodName().compareTo(methodName))==0)
				{
				code = mci.getMethod().getCodeSegment();
				}
			}
		}
		return code;
	}
	
	
	
	
	
	
	
	
	/**
	 * Create contents of the dialog.
	 */
	private void createContents() {
		
		totalFiles = MCSTemplate.markFilesForMapping.size();
		
		shell = new Shell( SWT.CLOSE | SWT.TITLE);
		shell.setText("Mapp MCS.");
		

		//shell = new Shell(getParent(), getStyle());
		shell.setSize(735, 657);
		shell.setText(getText());
		
	
		
		
		
		Group group = new Group(shell, SWT.NONE);
		group.setBounds(10, 29, 707, 529);
		
		List list_mccIds = new List(group, SWT.BORDER);
		list_mccIds.setBounds(10, 80, 138, 154);
		
		
		for(MethodClones mcc : mcs.getMccs()) {
			int mccId = mcc.getClusterID();
			list_mccIds.add("MCC_"+ Integer.toString(mccId));
		}
		
		List list_methods = new List(group, SWT.BORDER);
		list_methods.setBounds(192, 80, 138, 154);
		 	
		
		/*
		 * JScrollPane pane = new JScrollPane();
		 * pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		 * pane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		 * JTextArea textArea = new JTextArea();
		 * textArea.setText("Clone Code Goes Here"); textArea.setBounds(325, 80, 247,
		 * 154); textArea.setBorder(new LineBorder(Color.BLACK));
		 * textArea.setEditable(false); pane.setBounds(325, 80, 247, 154);
		 * pane.getViewport().setBackground(Color.WHITE);
		 * pane.getViewport().add(textArea); textArea.add(pane);
		 */
		    	    
		    
		   
		    Text codeText= new Text(group, SWT.BORDER|SWT.H_SCROLL | SWT.V_SCROLL);
		    codeText.setVisible(true);
		    codeText.setBounds(10, 270, 677, 250);
//		    codeText.setText("Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n"
//		    		+ "Clone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes HereClone Code Goes Here\n");
		    codeText.setEditable(false);
		    codeText.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		    
		    
		    
		    
		list_methods.addListener(SWT.Selection, new Listener()
				{
			public void handleEvent(Event e)
			{
				String codeSegment;
				String selection = list_methods.getItem(list_methods.getSelectionIndex());
				codeSegment= extractMethodBody(currentMccId,selection);
				
			
				codeText.redraw();
				codeText.setText(codeSegment);
			}		
});
		
		
		
		
		list_mccIds.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				
				
				// TODO Auto-generated method stub
				String selection = list_mccIds.getItem(list_mccIds.getSelectionIndex());
				String[] mccIdStr = selection.split("_");
				currentMccId=Integer.parseInt(mccIdStr[1]);
				ArrayList<String> methodNames = extractMCCMethodsInCurrentFile(Integer.parseInt(mccIdStr[1]));

				list_methods.removeAll();
				
				for(String mname : methodNames) {
					list_methods.add(mname);
					
				}		
			}
		});
		
		
		
		
		List list_inputStructure = new List(group, SWT.BORDER);
		list_inputStructure.setBounds(439, 80, 248, 154);
		
		Button button_addMethod = new Button(group, SWT.NONE);
		button_addMethod.setBounds(363, 127, 55, 25);
		button_addMethod.setText(">>");
		
		button_addMethod.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				try
				{
					boolean check = true;
					String selectionMethod = list_methods.getItem(list_methods.getSelectionIndex());
					String selectionMCC = list_mccIds.getItem(list_mccIds.getSelectionIndex());
					
					for(int i=0; i<list_inputStructure.getItemCount(); i++)
					{
						if((selectionMCC +"~"+selectionMethod+"~"+ProjectInfo.getFileName(MCSTemplate.markFilesForMapping.get(fileInProcess))).equals(list_inputStructure.getItem(i)))
								{
									check = false;
								}
					}
					if(check)
					{
						boolean check2 = false;
						
						String[] mcc = selectionMCC.split("_");
						for(int i=0; i<checkCompleteMapping.get(fileInProcess).size(); i++)
						{
							if(checkCompleteMapping.get(fileInProcess).get(i) == Integer.parseInt(mcc[1]))
							{
								checkCompleteMapping.get(fileInProcess).remove(i);
								checkCompleteMapping.get(fileInProcess).add(i, -1);
								check2 = true;
								break;
							}
						}
						
						if(check2)
						{
							list_inputStructure.add(selectionMCC +"~"+selectionMethod+"~"+ProjectInfo.getFileName(MCSTemplate.markFilesForMapping.get(fileInProcess)));
						}
					}
				}
				catch(Exception e)
				{
					// do nothing
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		Button button_removeMethod = new Button(group, SWT.NONE);
		button_removeMethod.setBounds(363, 172, 55, 25);
		button_removeMethod.setText("<<");
		
		button_removeMethod.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				try
				{
					
					String mccString = list_inputStructure.getItem(list_inputStructure.getSelectionIndex());
					String[] split = mccString.split("~");
					String[] mcc = split[0].split("_");
					
					for(int i=0; i<checkCompleteMapping.get(fileInProcess).size(); i++)
					{
						if(checkCompleteMapping.get(fileInProcess).get(i) == -1)
						{
							checkCompleteMapping.get(fileInProcess).remove(i);
							checkCompleteMapping.get(fileInProcess).add(i, Integer.parseInt(mcc[1]));
							break;
						}
					}
					
					list_inputStructure.remove(list_inputStructure.getSelectionIndex());
				}
				catch(Exception e)
				{
					// do nothing
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		Label lblFile = new Label(group, SWT.NONE);
		lblFile.setBounds(10, 10, 55, 15);
		lblFile.setText("File");
		
		Label lblFilename = new Label(group, SWT.NONE);
		lblFilename.setBounds(71, 10, 150, 15);
		lblFilename.setText(ProjectInfo.getFileName(MCSTemplate.markFilesForMapping.get(fileInProcess)));
		
		Label lblRequiredStructure = new Label(group, SWT.NONE);
		lblRequiredStructure.setBounds(10, 31, 110, 15);
		lblRequiredStructure.setText("Required Structure");
		
		Label lblList = new Label(group, SWT.NONE);
		lblList.setBounds(126, 31, 110, 15);
		lblList.setText(targetStructure);
		
		
		
		
		
		// initializing checkComplete list
		
		ArrayList<Integer> innerList;
		String[] split = targetStructure.split(",");
		
		checkCompleteMapping.clear();
		
		for(int i=0; i<totalFiles; i++)
		{
			innerList = new ArrayList<Integer>();
			
			for(int j=0; j<split.length; j++)
			{
				innerList.add(Integer.parseInt(split[j]));
			}
			checkCompleteMapping.add(innerList);
		}
		
		//
		
		Label lblMcc = new Label(group, SWT.NONE);
		lblMcc.setBounds(10, 59, 55, 15);
		lblMcc.setText("MCC IDs");
		
		Label lblMethods = new Label(group, SWT.NONE);
		lblMethods.setBounds(192, 59, 55, 15);
		lblMethods.setText("Methods");
		
		Label lblCode = new Label(group,SWT.NONE);
		lblCode.setBounds(10, 249, 75, 15);
		lblCode.setText("Method Body");
		
		
		Label lblPattern = new Label(group, SWT.NONE);
		lblPattern.setBounds(439, 59, 58, 15);
		lblPattern.setText("Required Structure : ");
		
		Group group_1 = new Group(shell, SWT.NONE);
		group_1.setBounds(10, 563, 707, 55);
		
		Button btn_previousFile = new Button(group_1, SWT.NONE);
		btn_previousFile.setBounds(417, 20, 78, 25);
		btn_previousFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(fileInProcess >=1)
				{
					fileInProcess--;
					lblFilename.setText(ProjectInfo.getFileName(MCSTemplate.markFilesForMapping.get(fileInProcess)));
					list_methods.removeAll();
				}
			}
		});
		btn_previousFile.setText("Previous File");
		
		Button btn_nextFile = new Button(group_1, SWT.NONE);
		btn_nextFile.setBounds(509, 20, 75, 25);
		btn_nextFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(totalFiles > fileInProcess + 1)
				{
					fileInProcess++;
					lblFilename.setText(ProjectInfo.getFileName(MCSTemplate.markFilesForMapping.get(fileInProcess)));
					list_methods.removeAll();
				}
			}
		});
		btn_nextFile.setText("Next File");
		
		Button btn_done = new Button(group_1, SWT.NONE);
		btn_done.setBounds(601, 20, 84, 25);
		btn_done.setText("Done");
		
		
	btn_done.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
				boolean check = true;
				
				for(int i=0; i<checkCompleteMapping.size(); i++)
				{
					for(int j=0; j< checkCompleteMapping.get(i).size(); j++)
					{
						if(checkCompleteMapping.get(i).get(j) != -1)
						{
							check = false;
						}
					}
				}
				if(check)
				{
					selectedMethods =list_inputStructure.getItems();
					markSelectedMethods();
					shell.close();
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Required structure is not completed.", "Error" , JOptionPane.INFORMATION_MESSAGE);
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});

	}
}
