package cmt.cfc.mccframe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.FramingStructure;
import cmt.cfc.sccframe.SCCTemplate;
import cmt.common.NotificationPopUpUI;
import cmt.cfc.fccframe.FCCTemplate;
import cmt.cfc.mccframe.*;


public class RenamingWizard extends Wizard implements INewWizard {
	
	
	public MCCTemplate mccTemp; 
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	
	
	public static ArrayList<FramingStructure> FrameStructure= new ArrayList<FramingStructure>();
	
	public static ArrayList<FrameBreak> breakNames = new ArrayList<FrameBreak>();
	ArrayList<Integer> sccFrames = new ArrayList<Integer>();
	ArrayList<String> filesToOpen=new ArrayList<String>();	
	
	
	
	

	public static ArrayList<String> varNames = new ArrayList<String>();
	public static ArrayList<String> varNewNames = new ArrayList<String>();
	ArrayList<DynamicToken> tokens = new ArrayList<DynamicToken>();
	
	
	private void setMCI(MethodCloneInstance m)
	{
		mcc=m;
	}
	
	private MethodCloneInstance getMCI()
	{
		return mcc;
	}
	
	public void setFrameStructure(ArrayList<FramingStructure> Fr)
	{
		FrameStructure= Fr;
	}
	
	public static ArrayList<FramingStructure> getFrameStructure()
	{
		return FrameStructure;
	}
	
	
	
	private void setSCCTemplate(SCCTemplate scctemp)
	{
		temp= scctemp;
	}

	private SCCTemplate getSCCTemplate() {
		return temp;
	}

	
	public ArrayList<DynamicToken> getTokens()
	{
		return tokens;
	}
	
	private ArrayList<String> getVarNames()
	{
		return varNames;
	}
	
	private ArrayList<String> getVarNewNames()
	{
		return varNewNames;
	}
	
	private ArrayList<String> getFilesToOpen()
	{
		return filesToOpen;
	}
	
	private ArrayList<Integer> getSccFrames()
	{
		return sccFrames;
	}
	
	public static ArrayList<FrameBreak> getBreakNames()
	{
		return breakNames;
		
	}
	
	private void setBreakNames(ArrayList<FrameBreak> br)
	{
		breakNames= br;
	}
	
	private void setSccFrames(ArrayList<Integer> sc)
	{
		sccFrames=sc;
	}
	private void setFiles(ArrayList<String> files)
	{
		filesToOpen=files;
	}
	
	private void setVar(ArrayList<String> v1)
	{
		varNames= v1;
	}
	
	private void setNewVar(ArrayList<String> v2)
	{
		varNewNames=v2;
	}
	
	public void setTokens(ArrayList<DynamicToken> tk)
	{
		tokens=tk;
	}
	WizardPage0 page0;
	WizardPage1 page1;
	WizardPage2 page2;
	WizardPage3 page3;
	

	public RenamingWizard() {
		super();

	}

	
	



	

	@Override
	public void addPages() {
		//page0= new WizardPage0(this);
		page1 = new WizardPage1(this);
		page2 = new WizardPage2(this);
		page3 = new WizardPage3(this);
		
		
		//addPage(page0);
		addPage(page1);
		addPage(page2);
		addPage(page3);
		

	
	}

	/*
	 * public void refresh_page4() { page4 = new
	 * CloneDetectionSettingsWizardPage4(this); }
	 */
	public WizardPage1 getPage1() {
		return page1;
	}

	public WizardPage2 getPage2() {
		return page2;
	}

	public WizardPage3 getPage3() {
		
		return page3;
		//return page3;
	}

	public WizardPage0 getPage0() {
		return page0;
	}



	@Override
	public boolean canFinish() {
		if (getContainer().getCurrentPage() == page3 )
			
			return true;
		else
			return false;
		/*
		 * if (getContainer().getCurrentPage() == page1) { return true; } else { return
		 * false; }
		 */
	}

	public void clearFrame()
	{
		FrameStructure.clear();
		WizardPage2.breakName.clear();
	}

	@Override
	public boolean performFinish() {
		
		
		if(MCCTemplate.mccTrigger==true)
    	{
			MCCTemplate.FrameSt = FrameStructure ;
			MCCTemplate.generateTemplate();
			MCCTemplate.clearAll();
    	}
    	
    	if(FCCTemplate.fccTrigger==true)
    	{	
    		FCCTemplate.FrameSt=FrameStructure;
    		FCCTemplate.generateTemplate();
    		//FCCTemplate.clearAll();
    	}
		
		
		//setFrameStructure(FrameSt);
		page2.sccS.clear();
		page3.sccs.clear();
		
		
		//clearFrame();
		
		
//		Job job = new Job("Framing") {
//
//			@Override
//			protected IStatus run(IProgressMonitor arg0) {
//
//				
//				syncWithUi();
//				return Status.OK_STATUS;
//
//			}
//		};
//
//		job.schedule();
//
 		return true;
	}

	

	private void syncWithUi() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {

				
				NotificationPopUpUI popup = new  NotificationPopUpUI(/*NotificationQuartzJobHelper.getInstance().getCurrentDisp()*/PlatformUI.getWorkbench().getDisplay());
			    popup.open();
			}
		});

	}



	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		System.out.println("Preparing wizard..");
		this.setWindowTitle("Frame Renaming");
		this.setNeedsProgressMonitor(true);
		this.setHelpAvailable(false);
		this.setForcePreviousAndNextButtons(true);
	}

	@Override
	public void createPageControls(Composite pageContainer) {
	}

	@Override
	public boolean performCancel() {
		

		return super.performCancel();
	}






	public void setSavePage3(boolean b) {
		// TODO Auto-generated method stub

	}








	public void open() {
		// TODO Auto-generated method stub
		 
		
		   
		    
		
	}

}
