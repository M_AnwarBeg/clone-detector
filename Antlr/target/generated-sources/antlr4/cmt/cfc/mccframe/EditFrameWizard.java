package cmt.cfc.mccframe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.structures.sMethod;
import cmt.cfc.utility.FileHandler;



public class EditFrameWizard extends Wizard implements INewWizard
{

    EditFrameWizardPage1 page1;
    EditFrameWizardPage2 page2;
    public int mccID;
    public int mccIndex;

    public EditFrameWizard()
    {
	// TODO Auto-generated constructor stub

	super();

    }

    @Override
    public void addPages()
    {

	// page1 = new EditFrameWizardPage1(mccID);
	page1 = new EditFrameWizardPage1(mccID, this);
	addPage(page1);

	page2 = new EditFrameWizardPage2(mccID);
	addPage(page2);

    }

    public EditFrameWizardPage2 getPage2()
    {
	return page2;
    }

    @Override
    public void init(IWorkbench workbench, IStructuredSelection selection)
    {
	// TODO Auto-generated method stub
    }

    @Override
    public boolean performFinish()
    {
	ClonesReader.getMethodClones().get(mccIndex);
	int count = 0;

	for (MethodCloneInstance instance : ClonesReader.getMethodClones().get(mccIndex).getMCCInstances())
	{
	    sMethod currentMethod = instance.getMethod();
	    File methodFile = new File(currentMethod.getFilePath());
	    IFile ifile = getIFile(methodFile);
	    String oldFileContent = getFileContent(methodFile);
	    // oldFileContent = formatCode(oldFileContent);
	    String oldCodeSegment = currentMethod.getCodeSegment();
	    String newCodeSegment = getFileContent(new File(FileHandler.fileHandler.javaFilesPaths.get(count)))
		    .trim();
	    /// System.out.println("<<<<<<<<<<<<<<<<OLD>>>>>>>>>>>>>>>>>>");
	    // System.out.println(oldCodeSegment);
	    // System.out.println("<<<<<<<<<<<<<<<<NEW>>>>>>>>>>>>>>>>>>");
	    // System.out.println(newCodeSegment);

	    String[] parts = oldCodeSegment.split("\\r?\\n");
	    String[] mainFile = oldFileContent.split("\\r?\\n");
	    int oldCodeSize = parts.length;
	    int linesMatched = 0;
	    int startLine = 0;
	    int endLine = 0;
	    System.out.println("Clone Code Size : " + oldCodeSize);
	    for (int j = 0; j < mainFile.length - 1; j++)
	    {

		// for(int i=0;i<parts.length-1;i++)
		if (oldCodeSize > 0)
		{
		    int i = 0;
		    if (mainFile[j].trim().equals(parts[i].trim()) == true)
		    {
			linesMatched++;
			for (i = 1; i < oldCodeSize; i++)
			{
			    j++;
			    if (mainFile[j].trim().equals(parts[i].trim()) == true)
			    {
				linesMatched++;

				// System.out.println("Main Code : "+mainFile[j].trim());
				// System.out.println("Clone Code : "+parts[i].trim());
				// System.out.println("Lines Matched : "+linesMatched);
				if (linesMatched == oldCodeSize)
				{
				    startLine = j + 2 - oldCodeSize;
				    endLine = j + 1;
				    System.out.println(("File Name : " + methodFile));
				    System.out.println("Start : " + (startLine));
				    System.out.println("End : " + (endLine));

				}
			    }
			}

		    } else
		    {
			linesMatched = 0;
		    }
		}
	    }

	    currentMethod.setCodeSegment(newCodeSegment);
	    currentMethod.setStartToken(startLine);
	    currentMethod.setEndToken(endLine);
	    ClonesReader.getMethodClones().get(mccIndex).getMCCInstances().get(count).setMethod(currentMethod);

	    /*
	     * for(int sccID : mcc.getSCS()){ SCC currentSCC =
	     * SampleHandler.f.sccList.get(sccID);//wrong for(SCCInstance sccInstance:
	     * currentSCC.getSCCInstances()){ //AHMAD your TASK // Check
	     * sccInstance.getCodeSegment() in oldCodeSegment, if that exits find its new
	     * location //and update start and end line number and refresh table according
	     * to this new information.. } }
	     */
	    String newFileContent = oldFileContent.replace(oldCodeSegment, newCodeSegment);
	    // newFileContent = formatCode(newFileContent);
	    // System.out.println("<<<<<<<<<<<<<<<<FILE CONTENT>>>>>>>>>>>>>>>>>>");
	    // System.out.println(newFileContent);
	    // System.out.println("<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>");
	    writeUpdatedContent(currentMethod, newFileContent);

	    refreshResource(ifile);

	    markNewContent(currentMethod, ifile, newFileContent);

	    count++;
	}
	return true;
    }

    private void refreshResource(IFile ifile)
    {
	try
	{
	    ifile.refreshLocal(IResource.DEPTH_ZERO, null);
	} catch (Exception ex)
	{
	    ex.printStackTrace();
	}
    }

    private void markNewContent(sMethod currentMethod, IFile ifile, String newFileContent)
    {
	try
	{
	    deleteExistingMarkers(ifile);
	    // System.out.println("here");
	    markMethod(currentMethod, ifile, newFileContent);
	} catch (CoreException e)
	{
	    e.printStackTrace();
	}
    }

    private void markMethod(sMethod currentMethod, IFile ifile, String newFileContent) throws CoreException
    {
	int startChar = newFileContent.indexOf(currentMethod.getCodeSegment());
	int offset = currentMethod.getCodeSegment().length();
	IMarker marker;
	marker = ifile.createMarker("com.plugin.clonemanager.customtextmarker");
	marker.setAttribute(IMarker.CHAR_START, startChar);
	marker.setAttribute(IMarker.CHAR_END, startChar + offset);
    }

    private void writeUpdatedContent(sMethod currentMethod, String newFileContent)
    {
	try
	{
	    PrintWriter out = new PrintWriter(currentMethod.getFilePath());
	    out.print(newFileContent);
	    out.close();
	} catch (FileNotFoundException e)
	{
	    e.printStackTrace();
	}
    }

    public boolean canFinsih()
    {

	if (getContainer().getCurrentPage() == page2)
	{
	    return true;
	} else
	{
	    return false;
	}

    }

    private IFile getIFile(File fileToOpen)
    {
	IPath location = Path.fromOSString(fileToOpen.getAbsolutePath());
	IWorkspace workspace = ResourcesPlugin.getWorkspace();
	return workspace.getRoot().getFileForLocation(location);
    }

    private String getFileContent(File fileToOpen)
    {
	String content = "";
	try
	{
	    FileInputStream fis = new FileInputStream(fileToOpen);
	    byte[] data = new byte[(int) fileToOpen.length()];
	    fis.read(data);
	    fis.close();
	    content = new String(data, "UTF-8");
	} catch (IOException e2)
	{
	    e2.printStackTrace();
	}
	return content;
    }

    private IMarker[] findMarkers(IResource target) throws CoreException
    {
	String type = "org.eclipse.core.resources.methodclonesmarker";
	IMarker[] markers = target.findMarkers(type, true, IResource.DEPTH_INFINITE);
	return markers;
    }

    private void deleteExistingMarkers(IFile ifile) throws CoreException
    {
	IMarker[] markers = findMarkers(ifile);
	for (IMarker marker : markers)
	{
	    marker.delete();
	}
    }

}
