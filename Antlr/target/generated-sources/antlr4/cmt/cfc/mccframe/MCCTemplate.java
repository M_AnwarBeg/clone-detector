package cmt.cfc.mccframe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.widgets.Shell;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneWriterDB;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.FramingStructure;
import cmt.cfc.sccframe.SCCTemplate;
import cmt.cfc.sccframe.SCCTemplateNew;
import cmt.cfc.utility.Initializer;
import cmt.common.Directories;
import cmt.cvac.views.MccView;


public class MCCTemplate
{
    final String JAVA_OUTPUT_PATH = "\\ART_Output\\Output\\";
    
    public static int level = 0;
    static int curMCCID = -1;
    static int curFileID = 0;
    static int curMethodID = 0;
    static int curVariableID = 0;
    private static int currentSccToBeFramed = -1;
    static ArrayList<String> FramePaths;
    static ArrayList<Integer> MCC_Settings;
    public static ArrayList<MethodCloneInstance> MCI_List;
    static ArrayList<MCIContainer> MCI_Container_List;
    static ArrayList<SimpleCloneInstance> SCCInstanceList;
    static ArrayList<Integer> sccsIncluded= new ArrayList<Integer>();
    static ArrayList<SCCTemplate> SCCTemplates;
    static ArrayList<SCCTemplateNew> SCCTemplatesNew;
    public static ArrayList<DynamicToken> DynamicTokens=new ArrayList<DynamicToken>();
    public static ArrayList<FramingStructure> FrameSt;
    public static boolean mccTrigger=false;
    public static MethodClones methodClones=new MethodClones();
    public static boolean isComplete=false;
    public MCCTemplate(int MCCID, MethodClones MCC)
    {
    	methodClones=MCC;
    	mccTrigger=true;
	    currentSccToBeFramed = 0;
		SCCInstanceList = new ArrayList<SimpleCloneInstance>();
		FramePaths = new ArrayList<String>();
		SCCTemplates = new ArrayList<SCCTemplate>();
		SCCTemplatesNew = new ArrayList<SCCTemplateNew>();
		MCC_Settings = new ArrayList<Integer>();
		MCI_List = MCC.getMCCInstances();
		MCI_Container_List = new ArrayList<MCIContainer>();
		MCC_Settings.add(1);
		MCC_Settings.add(1);
	
		curMCCID = MCCID;


		for(int i=0; i<MCC.getMCCInstances().get(0).getNewSccs_Contained().size();i++)
		{
			sccsIncluded.add(MCC.getMCCInstances().get(0).getNewSccs_Contained().get(i).getSCCID());
		}
  	
   	

	
	
	
	try
	{
		
		CloneRepository.getFrameId();
		CloneRepository.openConnection();
		Statement stmt2 = CloneRepository.conn.createStatement();
		stmt2.execute("use framerepository;");
		ResultSet rs = stmt2.executeQuery("select fid from mccframes where mccid="+curMCCID);
		
		if(rs.next())
		{
			int fid=rs.getInt(1);
			ResultSet rs2 = stmt2.executeQuery("select links from frames where fid="+fid);
			rs2.next();
			int links=rs2.getInt(1);
			links++;
			if(MCSTemplate.callFromMCS)
			{
				stmt2.execute("update frames set links="+links+" where fid="+fid);
				MCSTemplate.templateNames.add(curFileID + "_MCC_template.art");
				MCSTemplate.callFromMCS=false;
			}
			else
			{
				System.out.println("Selected MCC is already framed.");
			}
		}
		else
		{
			FrameSt= new ArrayList<FramingStructure>();
			extractSimpleClones(MCC);
			//Ubaid
			isComplete(sccsIncluded);
			if(isComplete)
			{
			 RenamingWizard wizard = new RenamingWizard();
			 wizard.clearFrame();
			 
			//WizardPage2 wiz= new WizardPage2(wizard);
			//wiz.renameBreak(MCCTemplate.getMC(MCCID));
			 //wizard.setTokens(FrameSt.get(0).getVariables());
			 
			
			 wizard.setFrameStructure(FrameSt);
			 //WizardPage0.getCode(MCC);
			WizardDialog dialog1 = new WizardDialog(null,wizard);
			dialog1.setBlockOnOpen(true);
			
			
			
			//wiz1.renameVar(MCCTemplate.getTokens());
		 	//wiz.renameBreak(MCCTemplate.getMC(ClonesReader.getMethodClones().get(mccIndex).getClusterID()));
		 	
		        
		        
		        dialog1.open();
		       
		        
		        
		        
//		        int returnCode = dialog1.open();
//		        if(returnCode == Dialog.OK)
//		          System.out.println(wizard.getPageCount());
//		        else
//		          System.out.println("Cancelled");
				

			
			}
			
			//generateTemplate();

			FrameSt.clear();
			mccTrigger=false;
		}
	}
	catch(Exception e)
	{
		e.printStackTrace();
	}

    }

   //Ubaid
    
    public static MethodClones getMethodClones()
    {
    	return methodClones;
    }
    
    public static void clearAll()
    {
    	level = 0;
        curMCCID = -1;
        curFileID = 0;
        curMethodID = 0;
        curVariableID = 0;
        currentSccToBeFramed = -1;
        FramePaths.clear();
        MCC_Settings.clear();;
        MCI_List.clear();
        MCI_Container_List.clear();
        SCCInstanceList.clear();
        sccsIncluded.clear();
        SCCTemplates.clear();
        SCCTemplatesNew.clear();
        DynamicTokens.clear();
        FrameSt.clear();
        mccTrigger=false;
        methodClones=new MethodClones();
        isComplete=false;
    }
    
    
    
    
    
    
    
    
    
    
    
    //ubaid
    
    public static void extractSimpleClonesUpdated(MethodClones mcc,FramingStructure fs)
    {
	boolean firstPass = true;
	ArrayList<String> baseFormat = MCI_List.get(0).getAnalysisDetails();
	for (int SCCID : mcc.getvCloneClasses())
	{
	    if (!isIncluded(SCCID, baseFormat))
	    {
		continue;
	    }

	    // Create frames for all required SCCIDs //
	    String path = Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccTemplate);
	    String filename = SCCID + "_Frame.art";
	    SCCTemplateNew template = null;
	    if (!firstPass)
	    {
		MCC_Settings.set(1, 0);
	    }
	    firstPass = false;
	    SimpleClone SimpleClone = findSCC(SCCID);
	    if (SimpleClone != null)
	    {
	    	if(SCCID==fs.getSCCId())
	    	{
	    		template = new SCCTemplateNew(SCCID, ClonesReader.getSimpleClones().get(SCCID), MCC_Settings, mcc.getClusterID(),fs);
	    		SCCTemplatesNew.add(template);
	    	}
		//MccView.repo.get(MccView.repo.size()-1).addScc(SCCID);
	    } else
	    {
		System.out.println("Critical error, cannot find SCC!");
	    }
	    //DynamicTokens=template.getDt();
//	    DynamicTokens.clear();
	    DynamicTokens=fs.getVariables();
	    
	    FramePaths.add(path + filename);
	}
	// All Frames Of SCCs Obtained! //
	System.out.println("I have all the SCC_Templates!");

	// Spend A Little Time Extracting SCCID List From New Analysis List //
	int MCI_Index = 0;
	int counter = 0;
	for (MethodCloneInstance MCI : MCI_List)
	{	
		//Ubaid
	    MCIContainer container = new MCIContainer(SCCTemplates);
		
	    counter = 0;
	    System.out.println("*******MCI #" + MCI_Index + "*******");
	    for (String codeSegment : MCI.getAnalysisDetails())
	    {
		if (counter % 2 != 0) // If Odd Index Encountered //
		{
		    // Give This Portion To MCI_Container //
		    container.mapIndexes(codeSegment);
		}
		counter++;
	    }
	    MCI_Container_List.add(container);
	    MCI_Index++;
	    System.out.println("************************");
	}
	System.out.println("Done processing");
	
	
    }
    
    
    
    
    
    
    
    
    public void extractSimpleClones(MethodClones mcc)
    {
	boolean firstPass = true;
	ArrayList<String> baseFormat = MCI_List.get(0).getAnalysisDetails();
	for (int SCCID : mcc.getvCloneClasses())
	{
	    if (!isIncluded(SCCID, baseFormat))
	    {
		continue;
	    }

	    // Create frames for all required SCCIDs //
	    String path = Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccTemplate);
	    String filename = SCCID + "_Frame.art";
	    SCCTemplate template = null;
	    if (!firstPass)
	    {
		MCC_Settings.set(1, 0);
	    }
	    firstPass = false;
	    SimpleClone SimpleClone = findSCC(SCCID);
	    if (SimpleClone != null)
	    {
		template = new SCCTemplate(SCCID, ClonesReader.getSimpleClones().get(SCCID), MCC_Settings, mcc.getClusterID());
		//MccView.repo.get(MccView.repo.size()-1).addScc(SCCID);
	    } else
	    {
		System.out.println("Critical error, cannot find SCC!");
	    }
	    //DynamicTokens=template.getDt();
	    SCCTemplates.add(template);
	    FramePaths.add(path + filename);
	}
	// All Frames Of SCCs Obtained! //
	System.out.println("I have all the SCC_Templates!");

	// Spend A Little Time Extracting SCCID List From New Analysis List //
	int MCI_Index = 0;
	int counter = 0;
	for (MethodCloneInstance MCI : MCI_List)
	{
	    MCIContainer container = new MCIContainer(SCCTemplates);
	    counter = 0;
	    System.out.println("*******MCI #" + MCI_Index + "*******");
	    for (String codeSegment : MCI.getAnalysisDetails())
	    {
		if (counter % 2 != 0) // If Odd Index Encountered //
		{
		    // Give This Portion To MCI_Container //
		    container.mapIndexes(codeSegment);
		}
		counter++;
	    }
	    MCI_Container_List.add(container);
	    MCI_Index++;
	    System.out.println("************************");
	}
	System.out.println("Done processing");
    }

    public static SimpleClone findSCC(int SCCID)
    {
	ArrayList<SimpleClone> SCCList = ClonesReader.getSimpleClones();
	for (SimpleClone SimpleClone : SCCList)
	{
	    if (SimpleClone.getSSCId() == SCCID)
	    {
		return SimpleClone;
	    }
	}
	return null;
    }

    public static void generateTemplate()
    {
	String path = Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcc);

	// String pathTemp = CloneManagementPlugin.getAbsolutePath(OUTPUT_PATH_Temp);

	curFileID = curMCCID;
			try
				{
				// Commented because of changing the folders of SPC and template files (line 180-183) (Saud)
				
				    /*new File(path + "\\MCC_" + curFileID + "\\").mkdir();
				    String SPC_PATH = "\\MCC_" + curFileID + "\\" + curFileID + "_MCC_SPC.art";
				    
				    String templatePath="\\MCC_" + curFileID + "\\" + curFileID + "_MCC_template.art";*/
				
					//new File(path + "\\MCC_" + curFileID + "\\").mkdir();
				    String SPC_PATH = "\\MCC_SPC\\" + curFileID + "_MCC_SPC.art";
				    
				    String templatePath="\\MCC_Template\\" + curFileID + "_MCC_template.art";
			
				    // refresh(path + SPC_PATH); // Delete files if they already exist // //commented because we should not delete any already exisiting SPC as user may have changed it (Saud)
			
				    // refresh(pathTemp+SPC_PATH);
			
				    FileWriter spc_file = new FileWriter(path + SPC_PATH, true);
			 
				    BufferedWriter spc_output = new BufferedWriter(spc_file);
				    
				    FileWriter template_file = new FileWriter(path + templatePath, true);
			
				    BufferedWriter templateOutput = new BufferedWriter(template_file);
			
				    String templateFileName= curFileID + "_MCC_template.art";
				    
				    MCSTemplate.templateNames.add(templateFileName);
				    // FileWriter spc_file_temp = new FileWriter(pathTemp + SPC_PATH, true);
			
				    // BufferedWriter spc_output_temp = new BufferedWriter(spc_file_temp);
			
				    // ====== START OF SPC ===== //
				    // Specify SPC details //
			
				    // The output filename //
				    String fileName = "MCC_fileName_"+curMCCID;
				    String fileNameValue = "MCCID_" + curMCCID + "_Inst"; // Can ask user to specify method name //
			
				    setValues(spc_output, fileName, fileNameValue); // Step 1
			
				    specifyMainBody(templateOutput,spc_output, fileName, fileNameValue,templateFileName);// Step 2
			
			//	    RunART(".\\MCC_Template\\" + SPC_PATH); // Run ART Processor
				    String artPath =path + "MCC_" + curFileID + "/";
				    int a = MCI_List.size();
				    
				    Statement stmt = CloneRepository.conn.createStatement();
				    stmt.execute("use framerepository;");
				   stmt.execute("insert into frames(path,level,links,issynced) values (\""+Directories.getAbsolutePath(path+SPC_PATH)+"\",3,1,1)");
				    stmt.execute("insert into frames(path,level,links,issynced) values (\""+Directories.getAbsolutePath(path+templatePath)+"\",4,1,1)");
				    
				    int spcid= CloneRepository.getFrameId()-1;
				    stmt.execute("insert into mccframes(mccid,fid) values("+curMCCID+","+spcid+")");
				    
				    MethodCloneWriterDB.insertFrame(curMCCID, a, curMCCID, artPath, 1);
				    //clearAll();
				    
				} 
			catch (Exception e)
				{
				    e.printStackTrace();
				}
			}

    private static void specifyMainBody(BufferedWriter templateOutput,BufferedWriter spc_output, String fileName, String fileNameValue,String TemplateFileName) throws IOException
    {

	ArrayList<String> BaseFormat = MCI_List.get(0).getAnalysisDetails();
	// int BaseFormatCounter = 0;

	// In While Loop //

	spc_output.write("\n#while ");
	level++;
	
	specifyWhileVaribles(spc_output, BaseFormat);

	specifyFileName(spc_output, fileName);
	
	String templatePath = Directories.getAbsolutePath("\\ART_Output\\");
	templatePath+="\\MCC_template\\";
	
	spc_output.write("\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	spc_output.write("#adapt: \"" + templatePath + TemplateFileName+"\"");
	
	specifyFirstGappedRegion(templateOutput, fileNameValue);

	specifyRemainingGappedRegions(templateOutput, fileNameValue, BaseFormat);
	
	spc_output.write("\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	
	spc_output.write("#endadapt");
	
	level--;
	spc_output.write("\n");
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	
	spc_output.write("#endwhile");
	spc_output.close();
	templateOutput.close();
    }

    private static void specifyRemainingGappedRegions(BufferedWriter spc_output, String fileNameValue,
	    ArrayList<String> BaseFormat) throws IOException
    {
    	
    	String fileName = "MCC_fileName_"+curMCCID;
	for (int index = 0; index < BaseFormat.size(); index++)
	{
		String path=Directories.getAbsolutePath(Directories.SccFrames);
	    if (index % 2 != 0) // Adapt XX_SCC_Frame
	    {
		String FRAME_PATH = BaseFormat.get(index).split(",")[0] + "_SCC_Frame.art";
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
		spc_output.write("#adapt: " + "\"" +path+ FRAME_PATH + "\"" + "\n");

		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
		spc_output.write("#select "+fileName+"\n");
		level++;
		for (int numInst = 0; numInst < MCI_List.size(); numInst++)
		{
			for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
			spc_output.write("#option " +"\""  + fileNameValue + "_" + numInst +"\"" + "\n");
			level++;
		    //spc_output.write("		#option " + fileNameValue + "_" + numInst + "\n");
		    
			for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
			
			
			for(int i=0; i<FrameSt.size(); i++)
			{
				if(Integer.parseInt(BaseFormat.get(index).split(",")[0]) == FrameSt.get(i).getSCCId())
				{
					spc_output.write(
							"#insert " +FrameSt.get(i).breakName + "\n");// +
														    // ":");
				}
			}
		    
		    if(MCI_List.get(numInst).getAnalysisDetails().size()>index+1)
		    {
			    if ((index + 1 == BaseFormat.size() - 1)) // If Last Index
			    {
			    	String checkForSpecialChars=MCI_List.get(numInst).getAnalysisDetails().get(index + 1).replace("?","\\?");
				    //checkForSpecialChars=checkForSpecialChars.replace("@","\\@");
				// if any error comes due to last closing bracket un-comment this
			    	/*for(int i=0; i<level; i++)
			    	{
			    		spc_output.write("\t");
			    	}*/
			    	if(!checkForSpecialChars.contentEquals("}\n"))
			    	{
				    spc_output.write(checkForSpecialChars);
				    spc_output.write("");
			    	}
			    } else
			    {
				String[] tempArray = MCI_List.get(numInst).getAnalysisDetails().get(index + 1).split(",");
				System.out.println("temparray len: " + tempArray.length);
				if (tempArray.length > 1)
				{
				    int len = tempArray[0].length() + tempArray[1].length() + tempArray[2].length()
					    + tempArray[3].length() + tempArray[4].length() + 5;
				    
				    String checkForSpecialChars=MCI_List.get(numInst).getAnalysisDetails().get(index + 1).replace("?","\\?");
				    //checkForSpecialChars=checkForSpecialChars.replace("@","\\@");
				    /*for(int i=0; i<level; i++)
			    	{
			    		spc_output.write("\t");
			    	}*/
				  //  if(!checkForSpecialChars.contentEquals("}\n"))
				    {
				    spc_output.write(checkForSpecialChars.substring(len));
				    }
				}
			    }
		    }
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    spc_output.write("\n#endinsert\n");
		    
		    level--;
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    spc_output.write("#endoption\n");
		}
		
		level--;
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
		spc_output.write("#endselect\n");
		spc_output.write("#endadapt\n");
	    }
	}
    }

    private static void specifyFirstGappedRegion(BufferedWriter spc_output, String fileNameValue) throws IOException
    {
	// spc_output.write("#break break_start\n");

    	String fileName = "MCC_fileName_"+curMCCID;
    	for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
    	spc_output.write("#select "+fileName+"\n");
    	level++;

	// Insert First Gapped Region //
	for (int numInst = 0; numInst < MCI_List.size(); numInst++)
	{
		for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
	   spc_output.write("#option " +"\""  + fileNameValue + "_" + numInst +"\"" + "\n");
	   		level++;
	    // spc_output.write(" #insert-after break_start" +
	    // (MCI_List.get(numInst).getAnalysisDetails().get(0)));
	    // spc_output.write(" #endinsert\n");
	    String checkForSpecialChars=MCI_List.get(numInst).getAnalysisDetails().get(0).replace("?","\\?");
	    checkForSpecialChars=checkForSpecialChars.replace("@","\\@");
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
		    spc_output.write(checkForSpecialChars+"\n");
	    
		    level--;
		    for(int i=0; i<level; i++)
	    	{
	    		spc_output.write("\t");
	    	}
	    spc_output.write("#endoption\n");
	}
	level--;
	for(int i=0; i<level; i++)
	{
		spc_output.write("\t");
	}
	spc_output.write("#endselect\n");
    }

    
    public void checkDetailsofGap(ArrayList<MethodCloneInstance> MCI_List,int n)
    {
    	//Ubaid
    	//MCI_List.get(n).getAnalysisDetails().get(0)
    	
    	
    	
    	
    	
    }
    
    
    
    
    private static void specifyFileName(BufferedWriter spc_output, String fileName) throws IOException
    {
    	spc_output.write(fileName+"\n");
    	//spc_output.write("#output ?@path??@fileName?\".java\"\n");
    	
    	for(int i=0; i<level; i++)
    	{
    		spc_output.write("\t");
    	}
    	spc_output.write("#output " +"?@path?" +"?@" + fileName + "?" + "\".java\"" + "\n");
    }

    private static void specifyWhileVaribles(BufferedWriter spc_output, ArrayList<String> BaseFormat) throws IOException
    {
    for (SCCTemplate Template : SCCTemplates)
		{		
    		//Ubaid
    		for(int i=0; i<FrameSt.size(); i++)
    		{
    			if(Template.curSCCID==FrameSt.get(i).getSCCId())
    			{
    				Template.DynamicTokens=FrameSt.get(i).getVariables();
    			}
    		}
    		//update ends here
		    if (Template.DynamicTokens.size() == 0)
		    {
			continue;
		    }
	
		    if (isIncluded(Template.curSCCID, BaseFormat) == false)
		    {
			continue;
		    }
	
		    for (DynamicToken tk : Template.DynamicTokens)
		    {
			if (tk.marked || tk.isIgnored())
			{
			    continue;
			}
			spc_output.write(tk.getVarName() + ", ");
		    }
		}
    }

    private static void setValues(BufferedWriter spc_output, String fileName, String fileNameValue) throws IOException
    {

    	MCSTemplate.fileNames.clear();
	setFileNames(spc_output, fileName, fileNameValue);
	// setFileNames(spc_output_temp, fileName, fileNameValue);
	// Set Variables For Each SCC Instance //
	spc_output.write("% Here I will set all the place-holder variables\n\n");

	// spc_output_temp.write("% Here I will set all the place-holder
	// variables\n\n");
	// int counter = 0;
	setPlaceHolderVariables(spc_output);
    }

    private static void setPlaceHolderVariables(BufferedWriter spc_output) throws IOException
    {
		for (MCIContainer Container : MCI_Container_List)
		{

			for (String SCCInfo : Container.SCC_Order)
		    {
				String[] InfoArray = SCCInfo.split(",");
				int SCCID = Integer.parseInt(InfoArray[0]);
				Integer.parseInt(InfoArray[1]);
				SCCTemplate Template = Container.getTemplate(SCCID);
				
				//Ubaid
	    		for(int i=0; i<FrameSt.size(); i++)
	    		{
	    			if(Template.curSCCID==FrameSt.get(i).getSCCId())
	    			{
	    				Template.DynamicTokens=FrameSt.get(i).getVariables();
	    			}
	    		}
	    		//update ends here
				
				
				
				
				
				
				
				for (DynamicToken tk : Template.DynamicTokens)
				{
					
				    if (tk.marked || tk.isRepeated() || tk.isIgnored())
				    {
					continue;
				    }
				    spc_output.write("#set " + tk.getVarName() + " = ");
				    System.out.println("#set " + tk.getVarName() + " = ");
		
				    ///////////////////////////////////For MCS/////////////
				    Variable var=new Variable();
				    
				    var.name=tk.getVarName();
				    //////////////////////////////////////////////////////
				    // spc_output_temp.write("#set " + tk.varName + " = ");
				    for (int _index = 0; _index < MCI_List.size(); _index++)
				    {
						String doubleComma = "";
						if (tk.isCDoubleComma() || tk.tokenValues.get(_index).contains("\""))
						{
							for(int i=0; i<tk.tokenValues.size();i++)
							{
								if(MCI_List.get(_index).getNewSccs_Contained().get(0).getCodeSegmentToTokenze().contains(tk.tokenValues.get(i)))
								{
								    doubleComma = tk.tokenValues.get(i).replace("\"", "\\\"");
								    spc_output.write("\"" + doubleComma + "\"");
								    var.values.add(doubleComma);
								}
							}
						} 
						else
						{	
							for(int i=0; i<tk.tokenValues.size();i++)
								{
									if(MCI_List.get(_index).getNewSccs_Contained().get(0).getCodeSegmentToTokenze().contains(tk.tokenValues.get(i)))
									{
								    spc_output.write("\"" + tk.tokenValues.get(i) + "\"");
								    var.values.add(tk.tokenValues.get(_index));
									}
								}
						}
						
						 var.mark.add(true);
						 //Variables New
						 var.fid.add(tk.fid.get(_index));
						 var.mccId=curMCCID;
						 //var.fid.add(-1);
						// spc_output_temp.write("\"" + tk.tokenValues.get(_index) + "\"");
			
						if (_index != MCI_List.size() - 1)
						{
						    spc_output.write(", ");
			
						    // spc_output_temp.write(", ");
						} else
						{
						    spc_output.write("\n");
			
						    // spc_output_temp.write("\n");
						}
				    }
				    	
				    if(MCSTemplate.variables.size()!=0)
				    {
				    	
					    for(int h=0;h<MCSTemplate.variables.size();h++)
					    {
					    	if(MCSTemplate.variables.get(h).name.equals(var.name))
					    	{
					    		break;
					    	}
					    	if(h==MCSTemplate.variables.size()-1)
					    	{
					    		MCSTemplate.variables.add(var);
					    		
					    	}
					    }
				    }
				    else
				    {
				    	MCSTemplate.variables.add(var);
				    }
				    //tk.repeated = true;
				    tk.setRepeated(true);
				}
		    }
		}
		
		// if there is a problem in MCS framing then look at the above check that are there for MCSTemplate and copy them in the following loop
		for (SCCTemplate Template : SCCTemplates)
		{
			for (DynamicToken tk : Template.DynamicTokens)
			{
			    if (tk.marked || tk.isRepeated() || tk.isIgnored())
			    {
				continue;
			    }
			    spc_output.write("#set " + tk.getVarName() + " = ");
			    System.out.println("#set " + tk.getVarName() + " = ");
	
			    ///////////////////////////////////For MCS/////////////
			    Variable var=new Variable();
			    
			    var.name=tk.getVarName();
			    //////////////////////////////////////////////////////
			    // spc_output_temp.write("#set " + tk.varName + " = ");
			    for (int _index = 0; _index < MCI_List.size(); _index++)
			    {
					String doubleComma = "";
					if (tk.isCDoubleComma() || tk.tokenValues.get(_index).contains("\""))
					{
					    doubleComma = tk.tokenValues.get(_index).replace("\"", "\\\"");
					    spc_output.write("\"" + doubleComma + "\"");
					    var.values.add(doubleComma);
					   
					} else
					{
					    spc_output.write("\"" + tk.tokenValues.get(_index) + "\"");
					    var.values.add(tk.tokenValues.get(_index));
					    var.mark.add(false);
					    //var.fid.add(-1);
					}
					
					if (_index != MCI_List.size() - 1)
					{
					    spc_output.write(", ");
					} 
					else
					{
					    spc_output.write("\n");
					}
			    }
			    
			    tk.setRepeated(true);
			}
		}
    }

    //Ubaid
    public static boolean isComplete(ArrayList<Integer> scc)
    {
    		for(int i=0; i<FrameSt.size(); i++)
    		{
    		if(sccsIncluded.contains(FrameSt.get(i).getSCCId()))
    			isComplete=true;
    		else
    			isComplete=false;
    			
    		}
    		
    		
    		
    	return isComplete;	
    		
    }
    	
    
    
    
    
    
    
    
    private static void setFileNames(BufferedWriter spc_output, String fileName, String fileNameValue) throws IOException
    {
    	spc_output.write("#set " + fileName +" = ");
	for (int _index = 0; _index < MCI_List.size(); _index++)
	{
	    spc_output.write("\"" + fileNameValue + "_" + _index + "\"");
	    // change the next line according to the above line while doing MCS framing (Saud)
	    MCSTemplate.fileNames.add(fileNameValue+"_"+_index);
	    if (_index != MCI_List.size() - 1)
	    {
		spc_output.write(", ");
	    } else
	    {
		spc_output.write("\n");
	    }
	}
	spc_output.write("\n");
	spc_output.write("#set path =");
    spc_output.write("\"");
    spc_output.write(Directories.getAbsolutePath(Directories.framingOutput));
    spc_output.write("\""+"\n");
	
	
    }

    public static void RunART(String filename)
    {
	Initializer.ExecuteART(filename);
    }

    static void refresh(String SPC_path)
    {
	File f1 = new File(SPC_path);
	f1.delete();
    }

    public static boolean isIncluded(int SCCID, ArrayList<String> BaseFormat)
    {
	boolean isSet = false;
	for (int index = 0; index < BaseFormat.size(); index++)
	{
	    if (index % 2 != 0)
	    {
			if (Integer.parseInt(BaseFormat.get(index).split(",")[0]) == SCCID)
				{
				    isSet = true;
				}
	    }
	}
	return isSet;
    }

    public static String RemoveLine(String inputText)
    {
	Pattern p;
	Matcher m;
	System.out.println(" Input Text : " + inputText);
	p = Pattern.compile("\n");
	m = p.matcher(inputText);
	String str = m.replaceAll("");
	System.out.println(" OUtput Text: " + str);
	return str;
    }

    
    public static void genMCCTemplate(int MCCID)
    {
	for (MethodClones MC : ClonesReader.getMethodClones())
	{
	    if (MC.getClusterID() == MCCID)
	    {
		System.out.println("About to generate MCC Template for MCCID: " + MCCID);
		new MCCTemplate(MCCID, MC);
		return;
	    }
	}
	System.out.println("Could not find specified MCCID!");
    }
    
    
    public static MethodClones getMC(int MCCID) //Ubaid
    {
    	MethodClones methodClone = new MethodClones();
    	for (MethodClones MC : ClonesReader.getMethodClones())
    	{
    	    if (MC.getClusterID() == MCCID)
    	    {
    		
    		methodClone = MC;
    	    }
    	}
    	
    	
    	
    	return methodClone;
    }
    
    public static ArrayList<DynamicToken> getTokens()
    {
    	return DynamicTokens;
    
    }
    
    
    public static void setFrameStructure(ArrayList<FramingStructure> Fr)
	{
		FrameSt= Fr;
	}
    
    
  
    
}
