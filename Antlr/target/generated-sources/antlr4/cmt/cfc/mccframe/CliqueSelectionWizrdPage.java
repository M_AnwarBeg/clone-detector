package cmt.cfc.mccframe;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.jidesoft.utils.StringUtils;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.fileclones.FileCloneInstance;
import cmt.cddc.fileclones.FileCluster;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.SimpleClonesReader;
import cmt.cfc.sccframe.SCCTemplate;



public class CliqueSelectionWizrdPage {
	protected Shell shell;
	private StyledText text;
	private StyledText text_1;
	Combo leftDrop;
	Combo rightDrop;
	Button leftNext;
	Button rightNext;
	Button leftPrevious;
	Button rightPrevious;
	Button differences;
	//Button similarities;
	
	public static MethodClones mcc;
    public static ArrayList<Integer> SccsSelected = new ArrayList<Integer>();
    public static ArrayList<Integer> SccsSelectedForVisualization = new ArrayList<Integer>();
    
    int mccid = -1, mccIndex = 0, instance1id, instance2id;
    private StyleRange style;
    private StyleRange style2;
    
    private ArrayList<StyleRange> arr1;
    private ArrayList<StyleRange> arr2;
    
    ArrayList<Color> colorsused = new ArrayList<Color>();
    
    public void setMCCID(int mccID) {
    	this.mccid = mccID;
    }
    public int getMCCID() {
    	return this.mccid;
    }
    
    public void setMCCIndex(int mccIndex) {
    	this.mccIndex = mccIndex;
    }
    public int getMCCIndex() {
    	return this.mccIndex;
    }
    
    public void setMethod() throws IOException
    {
	// TODO Auto-generated method stub
	System.out.println("THE MCCID I GET IS: " + mccid);
	if (mccid == -1)
	{
	    System.out.println("SOME PROBLEM");
	}
	for (int i = 0; i <  ClonesReader.getMethodClones().size(); i++)
	{
	    if (ClonesReader.getMethodClones().get(i).getClusterID() == mccid)
	    {
		mcc =  ClonesReader.getMethodClones().get(i);
		break;
	    }
	}
    }
    
    
    
   
    
    

	/**
	 * Launch the application.
	 * @param args
	 */
	/*public static void main(String[] args) {
		try {
			face01 window = new face01();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	/**
	 * Close the window.
	 */
	public void close() {
		shell.close();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell(SWT.CLOSE | SWT.TITLE | SWT.MIN) ;
		//Composite container = new Composite(SWT.CLOSE | SWT.TITLE | SWT.MIN);
		shell.setSize(1075, 470);
		
		
		shell.setText("MCC Instances Comparison");
		
		try
		{
		    setMethod();
		   
		} catch (IOException e2)
		{
		    // TODO Auto-generated catch block
		    e2.printStackTrace();
		}
		
		text = new StyledText(shell, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		text.setBounds(10, 10, 514, 383);
		
		
		text_1 = new StyledText(shell, SWT.BORDER | SWT.READ_ONLY | SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		text_1.setBounds(544, 10, 514, 383);
		
		leftDrop = new Combo(shell, SWT.NONE);
		leftDrop.setBounds(174, 399, 91, 23);
		
		leftNext = new Button(shell, SWT.NONE);
		leftNext.setBounds(271, 399, 20, 25);
		leftNext.setText(">");
		leftNext.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				int a = leftDrop.getSelectionIndex();
				leftDrop.select(++a);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		leftPrevious = new Button(shell, SWT.NONE);
		leftPrevious.setBounds(148, 399, 20, 25);
		leftPrevious.setText("<");
		leftPrevious.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				int a = leftDrop.getSelectionIndex();
				leftDrop.select(--a);
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		rightDrop = new Combo(shell, SWT.NONE);
		rightDrop.setBounds(749, 399, 91, 23);
		Populate_Drop_Down();
		if(leftDrop.getSelectionIndex() == 1) {
			leftPrevious.setEnabled(false);
			leftNext.setEnabled(true);
		}
		if(leftDrop.getSelectionIndex() == rightDrop.getItemCount()) {
			leftNext.setEnabled(false);
			leftPrevious.setEnabled(true);
		}
		if(rightDrop.getSelectionIndex() == 1) {
			rightPrevious.setEnabled(false);
			rightNext.setEnabled(true);
		}
		if(rightDrop.getSelectionIndex() == rightDrop.getItemCount()) {
			rightNext.setEnabled(false);
			rightPrevious.setEnabled(true);
		}
		
		rightPrevious = new Button(shell, SWT.NONE);
		rightPrevious.setBounds(723, 399, 20, 25);
		rightPrevious.setText("<");
		rightPrevious.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				rightDrop.select((rightDrop.getSelectionIndex()-1));
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
				
			}
		});
		
		rightNext = new Button(shell, SWT.NONE);
		rightNext.setBounds(846, 399, 20, 25);
		rightNext.setText(">");
		rightNext.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				rightDrop.select((rightDrop.getSelectionIndex()+1));
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		differences = new Button(shell, SWT.NONE);
		differences.setBounds(493, 399, 75, 25);
		differences.setText("Differences");
		differences.addSelectionListener(new SelectionAdapter() {
			 @Override
			    public void widgetSelected(SelectionEvent e)
			    {
				try {
					SetDiff();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("done");
			    }
		});
		
		/*similarities = new Button(shell, SWT.NONE);
		similarities.setBounds(544, 399, 75, 25);
		similarities.setText("Similarities");
		similarities.addSelectionListener(new SelectionAdapter() {
		    @Override
		    public void widgetSelected(SelectionEvent e)
		    {
			try {
				SetData();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out.println("done");
		    }
		});*/
		
		try 
		{
			SetData();
		} 
		catch (Exception e1)
		{
			e1.printStackTrace();
		}

	}
	 void Populate_Drop_Down()
	    {
		// TODO Auto-generated method stub
		/*Collection<Set<Integer>> cliques = fcc.getFCCInstance().get(0).getSortedCliques();
		String[] cliqes = new String[cliques.size()];*/
		String[] instances = new String[mcc.getMCCInstances().size()];
		/*for (int i = 0; i < cliques.size(); i++)
		{
		    int effective = i + 1;
		    cliqes[i] = "Clique" + " " + effective;
		}*/
		for (int i = 0; i < mcc.getMCCInstances().size(); i++)
		{
		    int effective = i + 1;
		    instances[i] = "Instance: " + effective;
		}
		leftDrop.setItems(instances);
		leftDrop.setText(instances[0]);
		rightDrop.setItems(instances);
		rightDrop.setText(instances[1]);
		/*combo.setItems(cliqes);
		combo.setText(cliqes[0]);*/
	    }
	 private void SetData() throws Exception
	    {
		// TODO Auto-generated method stub
		//setClique();
		setCode();
		//setLabels();
		try
		{
		    Highlight();
		} catch (IOException e1)
		{
		    // TODO Auto-generated catch block
		    e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    }
	 private void Highlight() throws Exception
	    {
	    	colorsused.clear();
	    	highlight1(colorsused);
	    	highlight2(colorsused);
	    }
	 private void highlight2(ArrayList<Color> colorsused) throws IOException {
			MethodCloneInstance mci=mcc.getMCCInstances().get(instance2id);
			String code=mci.getMethod().getCodeSegment();
			String path=ProjectInfo.getFilePath(mci.getMethod().getFileID());
			int count=0;
			//for(Integer k : SccsSelectedForVisualization)
			{
				Color temp = colorsused.get(count);
				colorsused.add(temp);
				int length=0;
				int start=0;
				for(SimpleCloneInstance scc: mci.getSCCs())
				{
					//if(scc.getSCCID()==k)
					{
						length=scc.getCodeSegment().length()-1;
						if(mci.getMethod().getStartToken()==scc.getStartingIndex())
						{
							String completelinecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,mci.getMethod().getStartToken()+1,1,null);
							String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
							start= intermediatecode.length();
							
							completelinecode=MethodClonesReader.getCodeSegment(path,1,1,mci.getMethod().getStartToken(),1,null);
							start +=completelinecode.length();
						}
						else
						{
							String intermediatecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
							start=intermediatecode.length()-1;	
							
							intermediatecode=MethodClonesReader.getCodeSegment(path,1,1,mci.getMethod().getStartToken(),1,null);
							start +=intermediatecode.length();
						}							
					}
				}
				
				style2 = new StyleRange();
				style2.start = start; 
				style2.length = length;
				style2.fontStyle = SWT.BOLD;
				style2.background = temp;
				text_1.setStyleRange(style2);
				count++;
			}
		}
	 private void highlight1(ArrayList<Color> colorsused) throws IOException {
			MethodCloneInstance mci=mcc.getMCCInstances().get(instance1id);
			String code=mci.getMethod().getCodeSegment();
			String path=ProjectInfo.getFilePath(mci.getMethod().getFileID());
			//for(Integer k : SccsSelectedForVisualization)
			{
				int i1=0,i2=0,i3=0;
				while(i1+i2+i3<100 || i1+i2+i3>550)
				{
					Random r1 = new Random();
					i1 = r1.nextInt(255);
					Random r2 = new Random();
					i2 = r2.nextInt(255);
					Random r3 = new Random();
					i3 = r3.nextInt(255);	
				}
				Color temp = new Color(null,0,175,255);
				colorsused.add(temp);
				int length=0;
				int start=0;
				for(SimpleCloneInstance scc: mci.getSCCs())
				{
					//if(scc.getSCCID()==k)
					{
						length=scc.getCodeSegment().length()-1;
						if(mci.getMethod().getStartToken()==scc.getStartingIndex())
						{
							String completelinecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,mci.getMethod().getStartToken()+1,1,null);
							String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
							start= intermediatecode.length();
							completelinecode=MethodClonesReader.getCodeSegment(path,1,1,mci.getMethod().getStartToken(),1,null);
							start +=completelinecode.length();
							
						}
						else
						{
							String intermediatecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
							start=intermediatecode.length()-1;
							intermediatecode=MethodClonesReader.getCodeSegment(path,1,1,mci.getMethod().getStartToken(),1,null);
							start +=intermediatecode.length();
						}
					}
				}
				style = new StyleRange();
				style.start = start;
				style.length = length;
				style.fontStyle = SWT.BOLD;
				style.background = temp;
				text.setStyleRange(style);
			}
		}
	private void SetDiff() throws Exception
    {
		// TODO Auto-generated method stub
		//setClique();
		setCode();
		//setLabels();
		try
			{
			    HighlightDiff();
			}	
		catch (IOException e1)
			{
			    // TODO Auto-generated catch block
	    e1.printStackTrace();
	} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	private void HighlightDiff() throws Exception
    {
		try
		{
			//highlightdiff1(); 
			//highlightdiff2();
			highlightdiff3();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	private void highlightdiff1() throws IOException {
		MethodCloneInstance mci=mcc.getMCCInstances().get(instance1id);
		String code=mci.getMethod().getCodeSegment();
		String path=ProjectInfo.getFilePath(mci.getMethod().getFileID());
		//for(Integer k : SccsSelectedForVisualization)
		{
			Color temp = new Color(null,255,255,255);
			int length=0;
			int start=0;
			for(SimpleCloneInstance scc: mci.getSCCs())
			{
 				//if(scc.getSCCID()==k)
				{
					length=scc.getCodeSegment().length()-1;
					if(mci.getMethod().getStartToken()==scc.getStartingIndex())
					{
						String completelinecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,mci.getMethod().getStartToken()+1,1,null);
						String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
						start= intermediatecode.length();
					}
					else
					{
						
						String intermediatecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
						start=intermediatecode.length()-1;	
					}
				}
			}
			style = new StyleRange();
			style.start = start;
			style.length = length;
			style.fontStyle = SWT.BOLD;
			style.background = temp;
			text.setStyleRange(style);
			
		}
	}
	
	private void highlightdiff2() throws IOException {
		MethodCloneInstance mci=mcc.getMCCInstances().get(instance2id);
		String code=mci.getMethod().getCodeSegment();
		String path=ProjectInfo.getFilePath(mci.getMethod().getFileID());
		//for(Integer k : SccsSelectedForVisualization)
		{
			Color temp = new Color(null,255,255,255);
			int length=0;
			int start=0;
			for(SimpleCloneInstance scc: mci.getSCCs())
			{
				//if(scc.getSCCID()==k)
				{
					length=scc.getCodeSegment().length()-1;
					if(mci.getMethod().getStartToken()==scc.getStartingIndex())
					{
						String completelinecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,mci.getMethod().getStartToken()+1,1,null);
						String intermediatecode=completelinecode.substring(0,scc.getStartCol()-1);
						start= intermediatecode.length();
					}
					else
					{
						String intermediatecode=MethodClonesReader.getCodeSegment(path,mci.getMethod().getStartToken(),1,scc.getStartingIndex(),scc.getStartCol()-1,null);
						start=intermediatecode.length()-1;	
					}							
				}
			}
			
			style = new StyleRange();
			style.start = start; 
			style.length = length;
			style.fontStyle = SWT.BOLD;
			style.background = temp;
			text_1.setStyleRange(style);
		}
	}
	
	private void highlightdiff3() throws Exception {
		ArrayList<SimpleCloneInstance> mci1=mcc.getMCCInstances().get(instance1id).getSCCs();
		ArrayList<SimpleCloneInstance> mci2=mcc.getMCCInstances().get(instance2id).getSCCs();
		
//		String code1=mcc.getMCCInstances().get(instance1id).getMethod().getCodeSegment();
		String path1=ProjectInfo.getFilePath(mcc.getMCCInstances().get(instance1id).getMethod().getFileID());
		
//		String code2=mcc.getMCCInstances().get(instance2id).getMethod().getCodeSegment();
		String path2=ProjectInfo.getFilePath(mcc.getMCCInstances().get(instance2id).getMethod().getFileID());
		
		//for(Integer k : SccsSelectedForVisualization)
		{
			arr1=new ArrayList<StyleRange>();
			arr2=new ArrayList<StyleRange>();
			
			arr1.add(style);
			arr2.add(style2);
			
			Color temp = new Color(null,255,255,255);
			int length=0;
			int start=0;
			for(int i=0;i<mci1.size();i++)
			{
				 Statement stmt = CloneRepository.getConnection().createStatement();
				 stmt.execute("use "+CloneRepository.getDBName());
				 ResultSet rs = stmt.executeQuery("select scc_id from scc_instance where scc_instance_id="+mci1.get(i).getSCCID()+";");
				 rs.next();
				 ArrayList<SimpleClone> sccList = SimpleClonesReader.getClones();
				 
				 for(int k=0;k<sccList.size();k++)
				 {
					 if(sccList.get(k).getSSCId()==rs.getInt(1))
					 {
						SCCTemplate.TokenizeSCC(sccList.get(k));
					 }
				 }
 				
				 text.setStyleRange(style);
				 text_1.setStyleRange(style2);
				 
				 for(int y=0;y<mci1.get(i).getTokens().size();y++)
				 {
					 if(!(mci1.get(i).getTokens().get(y).equals(mci2.get(i).getTokens().get(y))))
					 {
						 	String intermediatecode=MethodClonesReader.getCodeSegmentForColoring(path1,1,1,mci1.get(i).getTokensLine().get(y),mci1.get(i).getTokensColumn().get(y),null);
						 	intermediatecode=intermediatecode.replace("\t","        ");
							start=intermediatecode.length()-1;
							
							/*intermediatecode=MethodClonesReader.getCodeSegment(path1,1,1,mcc.getMCCInstances().get(instance1id).getMethod().getStartToken(),1,null);
							start +=intermediatecode.length();*/
							
							
							//text.setStyleRanges(text.getStyleRanges());
							
						 	StyleRange tempStyle = new StyleRange();
						 	tempStyle.start = start;
						 	tempStyle.length = mci1.get(i).getTokens().get(y).length();
						 	tempStyle.foreground= new Color(null,255,255,255);
						 	tempStyle.background = text.getStyleRangeAtOffset(start).background;
						 	arr1.add(tempStyle);
							text.setStyleRange(tempStyle);
							text.getText();
							
							intermediatecode=MethodClonesReader.getCodeSegmentForColoring(path2,1,1,mci2.get(i).getTokensLine().get(y),mci2.get(i).getTokensColumn().get(y),null);
							intermediatecode=intermediatecode.replace("\t","        ");
							start=intermediatecode.length()-1;
							
							/*intermediatecode=MethodClonesReader.getCodeSegment(path1,1,1,mcc.getMCCInstances().get(instance1id).getMethod().getStartToken(),1,null);
							start +=intermediatecode.length();*/
							
							//text_1.setStyleRanges(text_1.getStyleRanges());
							
							tempStyle = new StyleRange();
							tempStyle.start = start;
							tempStyle.length = mci2.get(i).getTokens().get(y).length();
							tempStyle.foreground= new Color(null,255,255,255);
							tempStyle.background = text_1.getStyleRangeAtOffset(start).background;
							arr2.add(tempStyle);
							text_1.setStyleRange(tempStyle);
					 }
				 }
				 /*StyleRange[] array1=new StyleRange[arr1.size()];
				 StyleRange[] array2=new StyleRange[arr1.size()];
				 
				 for(int w=0;w<arr1.size();w++)
				 {
					 array1[w]=arr1.get(w);
					 array2[w]=arr2.get(w);
				 }
				 
				 text.setStyleRanges(new int[arr1.size()],array1);
				 text_1.setStyleRanges(new int[arr1.size()],array2);*/
				 /*text.setStyleRanges(array1);
				 text_1.setStyleRanges(array2);*/
			}
		}
	}
	
	private void setClique()
    {
	// TODO Auto-generated method stub
	System.out.println("setting clique");
	SccsSelected.clear();
	SccsSelectedForVisualization.clear();
	String n_id = "";//combo.getText();
	String[] parts = n_id.split(" ");
	int cliquenumber;
	// System.out.println(Integer.parseInt(parts[1]));
	Iterator<Set<Integer>> iterator = mcc.getMCCInstances().get(0).getSortedCliques().iterator();
	int count = 0;
	cliquenumber = Integer.parseInt(parts[1]);
	while (iterator.hasNext()) // ITERATE THROUGH CLIQUES
	{
	    count++;
	    if (count == cliquenumber)
	    {
		Set<Integer> set = iterator.next();
		
		Iterator<Integer> itr = set.iterator();
		while (itr.hasNext())
		{
		    int sccid = mcc.getMCCInstances().get(0).getSCCs().get(itr.next()).getSCCID();
		    SccsSelected.add(sccid);
		}
		setVisulizationClique(cliquenumber);
		break;
	    } else
	    {
		iterator.next();
	    }
	}
    }
	private void setVisulizationClique(int cliqueNumber)
    {
    	int count=0;
    	Iterator<Set<Integer>> visulaizationIterator = mcc.getMCCInstances().get(0).getSortedCliques().iterator();
    	
    	while(visulaizationIterator.hasNext())
    	{
    		count++;
    		if(count==cliqueNumber)
    		{
		    	Set<Integer> visualizationSet=visulaizationIterator.next();
				while (visulaizationIterator.hasNext())   //comment by SAUD to resolve framing issue
				{
					visualizationSet.addAll(visulaizationIterator.next());
				}
				
				Iterator<Integer> itr = visualizationSet.iterator();
				while (itr.hasNext())
				{
				    int sccid = mcc.getMCCInstances().get(0).getSCCs().get(itr.next()).getSCCID();
				    SccsSelectedForVisualization.add(sccid);
				}
				break;
    		}
    		else
    		{
    			visulaizationIterator.next();
    		}
    	}
    }
	 private void setCode() throws Exception
	    {
		 /*String file1= getFileContent(new File(mcc.getMCCInstances().get(instance1id).getMethod().getFilePath()));
		 String file2= getFileContent(new File(mcc.getMCCInstances().get(instance2id).getMethod().getFilePath()));*/
		// TODO Auto-generated method stub
		System.out.println("setting code");
		String id1 = leftDrop.getText();
		String[] parts = id1.split(" ");
		instance1id = Integer.parseInt(parts[1]) - 1;
		text.setText(getFileContent(new File(mcc.getMCCInstances().get(instance1id).getMethod().getFilePath())).replace("\t","        ").replace("\r",""));
		String id2 = rightDrop.getText();
		String[] parts2 = id2.split(" ");
		instance2id = Integer.parseInt(parts2[1]) - 1;
		text_1.setText(getFileContent(new File(mcc.getMCCInstances().get(instance2id).getMethod().getFilePath())).replace("\t","        ").replace("\r",""));
	    }

	 private static String getFileContent(File fileToOpen)
	 {
		 String content = "";
			try
				{
				    FileInputStream fis = new FileInputStream(fileToOpen);
				    byte[] data = new byte[(int) fileToOpen.length()];
				    fis.read(data);
				    fis.close();
				    content = new String(data, "UTF-8");
				} 
			catch (IOException e2)
				{
				    e2.printStackTrace();
				}
			return content; 
	 }
}
