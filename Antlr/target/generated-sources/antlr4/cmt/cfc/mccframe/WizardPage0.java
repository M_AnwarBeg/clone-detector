package cmt.cfc.mccframe;
import java.io.*;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.LoadListSetting;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.SCCTemplate;
import org.eclipse.swt.widgets.Composite;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.StyleRange;
import org.eclipse.swt.custom.StyledText;

import javax.swing.JTable;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.swt.events.DragDetectListener;
import org.eclipse.swt.events.DragDetectEvent;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
public class WizardPage0 extends WizardPage implements Listener  {
	
	private RenamingWizard wizard;
	protected Object result;
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	ArrayList<FrameBreak> breakNames = new ArrayList<FrameBreak>();
	static String CodeSegment=" ";
	static String EndLine="";
	private StyledText text_1;
	int index;
	String path="C:\\Users\\cs\\eclipse-workspace2\\CloneDetector\\Antlr\\ART_Output\\SCC_Frames\\";
	 String frame="";
	 String firstLine="*";
	 int dragLength=0;
	static ArrayList<Integer> sccs= new ArrayList<Integer>();
	public WizardPage0(RenamingWizard wizard) {
		// TODO Auto-generated constructor stub
		super("Final");
		
		setTitle("Edit Frame");
		setDescription("Frame generated: ");
		this.wizard = wizard;
	}
	
	public static void getCode(MethodClones mClones)
	{
		//CodeSegment=mClones.getMCCInstances().get(0).getNewSccs_Contained().get(0).getCodeSegment();
		CodeSegment=mClones.getMCCInstances().get(0).getNewSccs_Contained().get(0).getCodeSegmentToTokenze();
		
		
	}
	
	public void includeSccs()
	{
		sccs=MCCTemplate.sccsIncluded;
	}
	
	
	
	
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
		Composite container = new Composite(parent, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		setControl(container);
		index=0;
		//includeSccs();
		
			
		text_1 = new StyledText(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text_1.setBounds(10, 53, 413, 228);
		
		
		
        // This will reference one line at a time
		
		
	
		 
			
			text_1.setText(CodeSegment);
			
			 text_1.setEditable(true);
			    text_1.setBackground(container.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		
		
		
		
		
		
		
		
		
		Label lblSccId = new Label(container, SWT.NONE);
		lblSccId.setBounds(10, 22, 54, 15);
		lblSccId.setText("SCC ID:");
		
		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setBounds(70, 22, 86, 15);
		//lblNewLabel.setText(Integer.toString(sccs.get(index)));
		
		Button btnNextFrame = new Button(container, SWT.NONE);
		btnNextFrame.setBounds(449, 298, 86, 25);
		btnNextFrame.setText("Next Clone");
		
		
		//Next Frame
		btnNextFrame.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if(index+1<=sccs.size())
				{
					index++;
				}
				
					String fileName;
					fileName=(Integer.toString(sccs.get(index))+"_SCC_Frame.art");
					lblNewLabel.setText(Integer.toString(sccs.get(index))); 
				//frame=getFrameCode(fileName);
				//text_1.clearSelection();
			
				text_1.redraw();
				text_1.setText(frame);
				
				 text_1.setEditable(true);
				    text_1.setBackground(container.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				frame=null;
				fileName=null;
				
			}
		});
		
		
		
		
		
		
		
		
		
		
		Button button = new Button(container, SWT.NONE);
		button.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		button.setText("Previous Clone");
		button.setBounds(357, 298, 86, 25);

		Scale scale = new Scale(container, SWT.NONE);
		
		scale.setBounds(10, 287, 341, 36);
		scale.setMaximum(firstLine.length());
		scale.setMinimum(0);
		
		Button btnExcludeFirstline = new Button(container, SWT.CHECK);
		btnExcludeFirstline.setBounds(442, 53, 122, 16);
		btnExcludeFirstline.setText("Exclude FirstLine");
		
		
		btnExcludeFirstline.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
//				int end=0;
//				end=CodeSegment.indexOf("\n");
//				text_1.setSelection(0,end);
				int start=0;
				int end=0;
				
				if(btnExcludeFirstline.getSelection()==true)
				{
				 StyleRange style3 = new StyleRange();
				    
				    style3.background = text_1.getDisplay().getSystemColor(SWT.COLOR_YELLOW);
				    

				
				
				
				if(CodeSegment.endsWith("\n"))
				{
					CodeSegment=CodeSegment.substring(0, CodeSegment.length()-1);
					
				}
				start=0;
				end=CodeSegment.indexOf("\n");
				
				style3.start = start;
			    style3.length = end;
				
				
				text_1.setStyleRange(style3);
				
				}
				else if(btnExcludeFirstline.getSelection()==false)
				{
					text_1.redraw();
				}
			}
			
		});
		
		
		
		
		
		Button button_1 = new Button(container, SWT.CHECK);
		button_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				int start=0;
				int end=0;
				
				 StyleRange style3 = new StyleRange();
				    
				    style3.background = text_1.getDisplay().getSystemColor(SWT.COLOR_YELLOW);
				    

				
				
				
				if(CodeSegment.endsWith("\n"))
				{
					CodeSegment=CodeSegment.substring(0, CodeSegment.length()-1);
					
				}
				start=CodeSegment.lastIndexOf("\n")+1;

				button_1.getSelection();
				start=CodeSegment.lastIndexOf("\n")+1;
				end=text_1.getText().length();
				
				style3.start = start;
			    style3.length = CodeSegment.length()-start;
				
				
				text_1.setStyleRange(style3);
				//text_1.setSelection(start,end);
				
				
				
				
				
				
				
			}
		});
		button_1.setText("Exclude LastLine");
		button_1.setBounds(442, 265, 122, 16);
	
		
		
		
		
		
		
		
		
		
		scale.addDragDetectListener(new DragDetectListener() {
			public void dragDetected(DragDetectEvent arg0) {
				
				if(dragLength<firstLine.length())
				{
				dragLength=dragLength + scale.getIncrement();
				text_1.setSelection(0, dragLength);
				//text_1.selectAll();

				text_1.showSelection();
				}
				
			}
		});
		
		
		
		
		
	}
	
	 public IWizardPage getPage()
	 	{
			return WizardPage0.this;
		 
	    }
	


	@Override
	public void handleEvent(Event arg0) {
		// TODO Auto-generated method stub
		
	}
}
