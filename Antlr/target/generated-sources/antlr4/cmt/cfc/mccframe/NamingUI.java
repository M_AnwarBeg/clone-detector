package cmt.cfc.mccframe;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Table;

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.LoadListSetting;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.SCCTemplate;

import org.eclipse.swt.widgets.Label;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.awt.Color;
import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.custom.CCombo;
import javax.swing.JTable;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.layout.TableColumnLayout;
public class NamingUI extends Dialog {
	
	
	protected Object result;
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	public static ArrayList<String> varNames = new ArrayList<String>();
	public static ArrayList<String> varNewNames = new ArrayList<String>();
	private Text text;
	private Table table;
	ArrayList<DynamicToken> tokens = new ArrayList<DynamicToken>();
	
	
	
	
	
	
	public NamingUI(Shell parent, int style) {
		super(parent, style);
		setText("Rename Variables");
	}	
	
	public Object open() {
		createContents();
		shell.open();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	//set variables for renaming
//	public void setVariablesForNaming(SCCTemplate temp) {
//		this.temp = temp;
//		  StringBuilder structure = new StringBuilder();
//			
//			//for(Integer sccId: mcc.getNewSccs_Contained().g) {
//				//structure.append(sccId).append(",");
//			}		
			
	public void renameVar(ArrayList<DynamicToken> tk ) {
		
		varNames.clear();
		
		for (int j = 0; j < tk.size(); j++)
		{
			if (tk.get(j).marked || tk.get(j).isRepeated() || tk.get(j).isIgnored())
			{
				varNames.add(tk.get(j).getVarName());
			}
		}
		tokens=tk;
		
		
	}
		
	
	
	
	
	
	
	
	
	
	private void createContents() {
		// TODO Auto-generated method stub
		shell = new Shell( SWT.CLOSE | SWT.TITLE);
		shell.setText("Mapp MCS.");
		

		//shell = new Shell(getParent(), getStyle());
		shell.setSize(550, 482);
		shell.setText(getText());
		
		Group group = new Group(shell, SWT.NONE);
		group.setBounds(10, 28, 524, 415);
		
		text = new Text(group, SWT.BORDER);
		text.setBounds(108, 40, 131, 26);
		
		Label lblName = new Label(group, SWT.NONE);
		lblName.setBounds(33, 43, 69, 26);
		lblName.setText("Var Name");
		
		Button btnDone = new Button(group, SWT.NONE);
		btnDone.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnDone.setBounds(426, 363, 75, 25);
		btnDone.setText("Next >");
		
		Button btnReplace = new Button(group, SWT.NONE);
		btnReplace.setBounds(268, 40, 75, 26);
		btnReplace.setText("Replace");
		
		table = new Table(group, SWT.BORDER | SWT.FULL_SELECTION |SWT.V_SCROLL);
		table.setBounds(20, 109, 481, 188);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		
		TableColumn tblclmnDefaultName = new TableColumn(table, SWT.NONE);
		tblclmnDefaultName.setWidth(100);
		tblclmnDefaultName.setText("Default Name");
		tblclmnDefaultName.setData(varNames);
		TableColumn tblclmnNewName = new TableColumn(table, SWT.NONE);
		tblclmnNewName.setWidth(100);
		tblclmnNewName.setText("New Name");
		varNewNames.clear();
		varNames.clear();
		
		TableColumn tblclmnValues = new TableColumn(table, SWT.NONE);
		tblclmnValues.setWidth(276);
		tblclmnValues.setText("Values");
		table.setVisible(true);
		
		 final TableColumn [] columns = table.getColumns ();
		 TableItem item;
		 for (int i = 0; i < tokens.size(); i++)
		{	
		
			 ArrayList<String> str = new ArrayList<String>();
		if (tokens.get(i).marked || tokens.get(i).isRepeated() || tokens.get(i).isIgnored())
		{
		continue;
		}
		item= new TableItem(table,SWT.NONE);
		if(tokens.get(i).getVarName().startsWith("SC"))
		{
			
		item.setText(tokens.get(i).getVarName());
		varNames.add(tokens.get(i).getVarName());
		
		//column3
			for(int k=0; k<tokens.get(i).getTokenValues().size(); k++)
				{
				str.add(tokens.get(i).getTokenValues().get(k));
				varNewNames.add("`");
				//str.add(",");
			
				}
			item.setText(2,str.toString());
		
		}
		//item.setText(varNames.get(i));
		
		
	    for (int j=1; j<columns.length; j++) {
	      if(j==1)
	      {
	    	  item.setText (j, " ");
	      }

	  }
		
	}
		
		
		
		
		btnReplace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
			
			TableItem item= table.getItem(table.getSelectionIndex());			
			
			item.setText(1,text.getText());
			varNewNames.add(table.getSelectionIndex(), text.getText());	
			
			System.out.println("YES");
				
				
			}
		});
		
		
		
	}
}
