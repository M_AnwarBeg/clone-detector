package cmt.cfc.mccframe;

import java.util.ArrayList;

public class Variable {
	
	public String name;
	public ArrayList<String> values;
	public ArrayList<Boolean> mark;
	public ArrayList<Integer> fid;
	public int mccId;
	public int sccId;
	public ArrayList<Boolean> isProcessed;
	public Variable()
	{
		name="";
		values=new ArrayList<String>();
		mark= new ArrayList<Boolean>();
		fid = new ArrayList<Integer>();
		mccId=-1;
		sccId=-1;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String n)
	{
		name=n;
	}
	
	
		
}
