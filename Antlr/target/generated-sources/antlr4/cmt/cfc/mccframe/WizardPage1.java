package cmt.cfc.mccframe;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cfc.mccframe.RenamingWizard;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.FramingStructure;
import cmt.cfc.sccframe.SCCTemplate;

public class WizardPage1 extends WizardPage implements Listener{

	private RenamingWizard wizard;
	

	
	public ArrayList<String> varNames = new ArrayList<String>();
	private Text text;
	private Table table;
	int index;
	WizardPage2 page2;
	public ArrayList<DynamicToken> tokens = new ArrayList<DynamicToken>() ;
	public static ArrayList<FramingStructure> FrameStruct= new ArrayList<FramingStructure>(); 
	String replaceHistory=" ";
	public WizardPage1(RenamingWizard wizard) {
		// TODO Auto-generated constructor stub
		super("Rename Variables");
		
		setTitle("Rename Variables");
		setDescription("Rename Variables that are used in the SCC Frames");
		this.wizard = wizard;
	}


	
	
	
	public void renameVar(ArrayList<DynamicToken> tk ) {
		
		varNames = new ArrayList<String>();
		for (int j = 0; j < tk.size(); j++)
		{
			if (tk.get(j).marked || tk.get(j).isRepeated() || tk.get(j).isIgnored())
			{
				
			varNames.add(tk.get(j).getVarName());
			}
		}
		tokens=tk;
		
		
	}
	 public IWizardPage getNextPage()
	    {
		 	page2= wizard.getPage2();
			return page2;
		 
	    }
	
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub

		Composite container = new Composite(parent, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		setControl(container);
		FrameStruct=wizard.getFrameStructure();	


		
		renameVar(FrameStruct.get(index).getVariables());
		
		text = new Text(container, SWT.BORDER);
		text.setBounds(108, 40, 131, 26);
		Label label = new Label(container, SWT.NONE);
		label.setBounds(96, 10, 43, 15);
		
		Button btnNextFrame = new Button(container, SWT.NONE);
		btnNextFrame.setBounds(387, 303, 98, 25);
		btnNextFrame.setText("Next Frame ");
		
		label.setText(Integer.toString(FrameStruct.get(index).getSCCId()));
		
		
		
		
		
		Label lblName = new Label(container, SWT.NONE);
		lblName.setBounds(33, 43, 69, 26);
		lblName.setText("Var Name");
		
		Button btnReplace = new Button(container, SWT.NONE);
		btnReplace.setBounds(268, 40, 75, 26);
		btnReplace.setText("Replace");
		
		table = new Table(container, SWT.BORDER | SWT.FULL_SELECTION |SWT.V_SCROLL);
		table.setBounds(20, 109, 465, 188);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		
		
		TableColumn tblclmnDefaultName = new TableColumn(table, SWT.NONE);
		tblclmnDefaultName.setWidth(100);
		tblclmnDefaultName.setText("Default Name");
		tblclmnDefaultName.setData(varNames);
		TableColumn tblclmnNewName = new TableColumn(table, SWT.NONE);
		tblclmnNewName.setWidth(100);
		tblclmnNewName.setText("New Name");
		
		TableColumn tblclmnValues = new TableColumn(table, SWT.NONE);
		tblclmnValues.setWidth(276);
		tblclmnValues.setText("Values");
		table.setVisible(true);
		
			
		Button buttonPrev = new Button(container, SWT.NONE);
		buttonPrev.setEnabled(false);
		
		final TableColumn [] columns = table.getColumns ();
		TableItem item;
		
		
		if(FrameStruct.size()==1)
		{
			btnNextFrame.setEnabled(false);
			buttonPrev.setEnabled(false);
		}
		
		
		
		
		
		
		
		
		
		
		
		 
		 for (int i = 0; i < tokens.size(); i++)
		{	
		
			 ArrayList<String> str = new ArrayList<String>();
		if (tokens.get(i).marked || tokens.get(i).isRepeated() || tokens.get(i).isIgnored())
		{
		continue;
		}
		item= new TableItem(table,SWT.NONE);
//		if(tokens.get(i).getVarName().startsWith("SC"))
//		{
			
		item.setText(tokens.get(i).getVarName());
		
		//column3
			for(int k=0; k<tokens.get(i).getTokenValues().size(); k++)
				{
				str.add(tokens.get(i).getTokenValues().get(k));
				}
			item.setText(2,str.toString());
		
//		}
		
		
		
	    for (int j=1; j<columns.length; j++) {
	      if(j==1)
	      {
	      item.setText (j, " ");
	      }

	  }
		
	}
		 
		 btnNextFrame.addSelectionListener(new SelectionAdapter() {
				@Override
				public void widgetSelected(SelectionEvent e) {
				if(index+1<FrameStruct.size())
				{
				index++;
				}
				if(index+1==FrameStruct.size())
				{
					btnNextFrame.setEnabled(false);
					buttonPrev.setEnabled(true);
				}
				else
				{
					btnNextFrame.setEnabled(true);
					buttonPrev.setEnabled(true);
					
				}
				renameVar(FrameStruct.get(index).getVariables());	
				table.clearAll();
				table.removeAll();
				
				
				label.setText(Integer.toString(FrameStruct.get(index).getSCCId()));	
				//Populate the values for Next SCC
				
				final TableColumn [] columns = table.getColumns ();
				TableItem item; 
				for (int i = 0; i < tokens.size(); i++)
				{	
					 ArrayList<String> str = new ArrayList<String>();
				if (tokens.get(i).marked || tokens.get(i).isRepeated() || tokens.get(i).isIgnored())
				{
				continue;
				}
				item= new TableItem(table,SWT.NONE);
//				if(tokens.get(i).getVarName().startsWith("SC"))
//				{	
				item.setText(tokens.get(i).getVarName());
				//column3
					for(int k=0; k<tokens.get(i).getTokenValues().size(); k++)
						{
						str.add(tokens.get(i).getTokenValues().get(k));
						}
					item.setText(2,str.toString());
				
//				}
			    for (int j=1; j<columns.length; j++) {
			      if(j==1)
			      {
			      item.setText (j, " ");
			      }
			  }
			}		
		}
	});
			
		 
		 
		 
		 
		 
		 
		
		btnReplace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				TableItem item= table.getItem(table.getSelectionIndex());
			if(replaceHistory.contains(text.getText()))
			{
				 JFrame f= new JFrame();
		           JOptionPane.showMessageDialog(f,"Cannot Rename, Duplicate Variable","Alert",JOptionPane.WARNING_MESSAGE); 
				
			}
			else
			{
							
				item.setText(1,text.getText());
				replaceHistory=replaceHistory.concat(text.getText()+",");
				
				varNames.set(table.getSelectionIndex(), text.getText());	
				//Updating Generic Tokens
				for(int i=0; i<FrameStruct.get(index).getGenTokens().size();i++)
				{
					if((FrameStruct.get(index).getGenTokens().get(i).contains(item.getText())))
					{
						FrameStruct.get(index).getGenTokens().set(i,FrameStruct.get(index).getGenTokens().get(i).replace(item.getText(), text.getText())); 
						
						
					}
				}
				
				//Replacing Dynamic Tokens
				for(int i=0;i<FrameStruct.get(index).getVariables().size();i++)
				{
					if(FrameStruct.get(index).getVariables().get(i).getVarName().contentEquals(item.getText()))
					{
						FrameStruct.get(index).getVariables().get(i).setVarName(text.getText());
					}
				}
				
				FrameStruct.get(index);	
				text.setText("");
			}
		}
			
		});
		
		
		
		Label lblSccId = new Label(container, SWT.NONE);
		lblSccId.setBounds(33, 10, 55, 15);
		lblSccId.setText("SCC ID:");
		
		
		buttonPrev.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
				if(index-1>=0)
				{
				index--;
				}
				if(index==0)
				{
					buttonPrev.setEnabled(false);
					btnNextFrame.setEnabled(true);
				}
				else
				{
					buttonPrev.setEnabled(true);
					btnNextFrame.setEnabled(true);
				}
				renameVar(FrameStruct.get(index).getVariables());	
				table.clearAll();
				table.removeAll();
				label.setText(Integer.toString(FrameStruct.get(index).getSCCId()));
				
					
				//Populate the values for Previous SCC
				
				final TableColumn [] columns = table.getColumns ();
				TableItem item; 
				for (int i = 0; i < tokens.size(); i++)
				{	
					 ArrayList<String> str = new ArrayList<String>();
				if (tokens.get(i).marked || tokens.get(i).isRepeated() || tokens.get(i).isIgnored())
				{
				continue;
				}
				item= new TableItem(table,SWT.NONE);
//				if(tokens.get(i).getVarName().startsWith("SC"))
//				{	
				item.setText(tokens.get(i).getVarName());
				//column3
					for(int k=0; k<tokens.get(i).getTokenValues().size(); k++)
						{
						str.add(tokens.get(i).getTokenValues().get(k));
						}
					item.setText(2,str.toString());
				
//				}
			    for (int j=1; j<columns.length; j++) {
			      if(j==1)
			      {
			      item.setText (j, " ");
			      }
			  }
			}					
		}
	});
		buttonPrev.setText("Previous Frame ");
		buttonPrev.setBounds(285, 303, 96, 25);
		
		
		
		
		
	}

	@Override
	public void handleEvent(Event arg0) {
		// TODO Auto-generated method stub
		
	}	
}
