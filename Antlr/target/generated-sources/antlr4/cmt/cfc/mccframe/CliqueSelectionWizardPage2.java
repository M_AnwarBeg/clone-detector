
package cmt.cfc.mccframe;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.custom.TableEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import cmt.cfc.utility.FileHandler;

public class CliqueSelectionWizardPage2 extends WizardPage
{

    CliqueSelectionWizardPage1 page1;
    CliqueSelectionWizard wizard;
    int mccid = -1;
    private FileHandler fileHandler;
    // SampleHandler.runFileHandler(mcc.getMCCID());
    public int totalTabs; // totalTabs= SampleHandler.fileHandler.listOfARTFiles.size();
    private Boolean variablesExist = Boolean.TRUE.booleanValue();

    private ArrayList<String> variableNamesList; // List of variables existing in .art files
    private ArrayList<TabItem> tabItems = new ArrayList<TabItem>();
    private ArrayList<StyledText> textARTCodes = new ArrayList<StyledText>();
    private ArrayList<String> oldVariableNames = new ArrayList<String>();
    private ArrayList<String> updatedVariableNames = new ArrayList<String>();
    private ArrayList<String> variableIds = new ArrayList<String>();
    private ArrayList<String> variableNames = new ArrayList<String>();

    private Button save;
    private Button update;
    private Button reset;
    private Table table;

    protected CliqueSelectionWizardPage2(CliqueSelectionWizard cliqueSelectionWizard)
    {
	// TODO Auto-generated constructor stub

	super("ART Code Frame");
	setTitle("ART Code Frame");
	setDescription("ART code is displayed.\n" + "User can update the variable names.");

    }

    @Override
    public void createControl(Composite parent)
    {

	// TODO Auto-generated method stub
	/* new */

	Composite composite = new Composite(parent, SWT.NONE);
	GridLayout gridLayout = new GridLayout(1, false);
	composite.setLayout(gridLayout);
	GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
	gridData.grabExcessHorizontalSpace = true;

	fileHandler = new FileHandler("MCC_Template\\+MCC_"+CliqueSelectionWizardPage1.mcc.getClusterID());
	variableNamesList = new ArrayList<String>(fileHandler.getVariableNames());

	oldVariableNames = variableNamesList;
	totalTabs = fileHandler.listOfARTFiles.size();
	if (variableNamesList.get(0).equals(" "))
	{
	    variablesExist = Boolean.FALSE.booleanValue();
	}
	if (variablesExist)
	{
	    for (int i = 0; i < variableNamesList.size(); i++)
	    {

		int left = variableNamesList.get(i).indexOf("_");
		if (left != -1)
		{
		    variableIds.add(i, variableNamesList.get(i).substring(0, left));
		    variableNames.add(i, variableNamesList.get(i).substring(left + 1));
		} else
		{
		    variableIds.add(i, variableNamesList.get(i));
		    variableNames.add(i, "-");
		}
	    }
	}
	new Label(composite, SWT.NULL);

	// tabs work starts here

	GridData TabgridData = new GridData(GridData.FILL_HORIZONTAL | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	final TabFolder tabFolder = new TabFolder(composite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	TabgridData.heightHint = 150;
	tabFolder.setLayoutData(TabgridData);
	insertTabs(composite, tabFolder);

	// Variable table
	GridData tablegridData = new GridData(GridData.FILL_HORIZONTAL | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	tablegridData.horizontalSpan = 3;
	tablegridData.heightHint = 80;

	table = new Table(composite, SWT.FULL_SELECTION | SWT.HIDE_SELECTION);
	table.setHeaderVisible(true);
	TableColumn column1 = new TableColumn(table, SWT.NONE);
	TableColumn column3 = new TableColumn(table, SWT.NONE);
	TableColumn column2 = new TableColumn(table, SWT.NONE);
	column1.setText("Serial No.");
	column3.setText("Variable Id");
	column2.setText("Edit Variable Name");

	insertTable(); // Populate table with variable names

	column1.pack();
	column2.pack();
	column3.pack();
	final TableEditor editor = new TableEditor(table);
	editor.horizontalAlignment = SWT.LEFT;
	editor.grabHorizontal = true;
	editor.minimumWidth = 55;

	// editing the second column
	final int EDITABLECOLUMN = 2;

	table.setLayoutData(tablegridData);
	// variable table ends here
	// button table starts here
	// tabs work starts here

	Composite buttonsLayout = new Composite(composite, SWT.NONE);
	FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
	fillLayout.spacing = 10;
	// fillLayout.
	buttonsLayout.setLayout(fillLayout);

	update = new Button(buttonsLayout, SWT.PUSH);
	update.setText("Update");

	save = new Button(buttonsLayout, SWT.PUSH);
	save.setText("Save");

	reset = new Button(buttonsLayout, SWT.PUSH);
	reset.setText("Reset");

	/* click listeners */
	update.addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {

		updateTable();

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub
	    }
	});

	save.addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

		updateTable();
		updateCode();
		// updateVariableNames();

		reset.setEnabled(false);
		reset.setGrayed(true);

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub
	    }
	});

	reset.addSelectionListener(new SelectionListener() {
	    @Override
	    public void widgetSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub
		/*** Insert file contents in text field *****/
		ArrayList<String> fileContents;

		/******* Reset Tabs Text ********/
		for (int i = 0; i < textARTCodes.size(); i++)
		{
		    fileContents = new ArrayList<String>(fileHandler.getFileContents(fileHandler.artFilesPaths.get(i)));
		    insertText(textARTCodes.get(i), fileContents);

		}

		/***** Reset table ****/
		TableItem[] tableItems = table.getItems();
		variableNamesList = new ArrayList<String>(fileHandler.getVariableNames());
		for (int i = 0; i < tableItems.length; i++)
		{
		    TableItem item = table.getItem(i);
		    /*
		     * item.setText(new String[] { "" + i, variableNamesList.get(i),
		     * variableNamesList.get(i) });
		     */

		    int left = variableNamesList.get(i).indexOf("_");
		    if (left != -1)
		    {
			variableIds.add(i, variableNamesList.get(i).substring(0, left));
			variableNames.add(i, variableNamesList.get(i).substring(left + 1));
		    } else
		    {
			variableIds.add(i, variableNamesList.get(i));
			variableNames.add(i, "-");
		    }

		    item.setText(new String[] { "" + i, variableIds.get(i), variableNames.get(i) });

		}

		reset.setFocus();
		reset.forceFocus();
		updateTable();

	    }

	    @Override
	    public void widgetDefaultSelected(SelectionEvent e)
	    {
		// TODO Auto-generated method stub

	    }
	});
	/// buttons ends here

	setControl(composite);

	// If editable variables exist then table.addSelectionListener will work
	if (!variableNamesList.get(0).equals(" "))
	{
	    table.addSelectionListener(new SelectionAdapter() {
		@Override
		public void widgetSelected(SelectionEvent e)
		{
		    // Clean up any previous editor control
		    Control oldEditor = editor.getEditor();
		    reset.setGrayed(false);
		    reset.setEnabled(true);
		    if (oldEditor != null)
		    {
			oldEditor.dispose();
		    }
		    // Identify the selected row
		    TableItem item = (TableItem) e.item;
		    if (item == null)
		    {
			return;
		    }
		    // The control that will be the editor must be a child of the
		    // Table
		    Text newEditor = new Text(table, SWT.NONE);

		    newEditor.setText(item.getText(EDITABLECOLUMN));
		    newEditor.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e)
			{
			    // TODO Auto-generated method stub
			    Text text = (Text) editor.getEditor();
			    editor.getItem().setText(EDITABLECOLUMN, text.getText());
			}
		    });
		    newEditor.selectAll();
		    newEditor.setFocus();
		    editor.setEditor(newEditor, item, EDITABLECOLUMN);
		}

	    });
	}

	else
	{
	    reset.setGrayed(true);
	    reset.setEnabled(false);
	    save.setGrayed(true);
	    save.setEnabled(false);
	    update.setGrayed(true);
	    update.setEnabled(false);

	    MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_INFORMATION | SWT.OK);
	    messageBox.setText("Information");
	    messageBox.setMessage("No editable variable exists in this code!");
	    messageBox.open();

	}
    }

    /****** Insert tabs of art files *********/
    private void insertTabs(Composite composite, TabFolder tabFolder)
    {
	TabItem tabItem;
	StyledText textARTCode;
	for (int i = 0; i < totalTabs; i++)
	{
	    tabItem = new TabItem(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

	    tabItem.setText(fileHandler.listOfARTFiles.get(i));
	    textARTCode = new StyledText(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	    textARTCode.setLayoutData(composite);
	    textARTCode.setEditable(false);
	    tabItem.setControl(textARTCode);
	    textARTCode.setText("");

	    ArrayList<String> fileContents = new ArrayList<String>(
		    fileHandler.getFileContents(fileHandler.artFilesPaths.get(i)));

	    if (!fileContents.isEmpty())
	    {
		insertText(textARTCode, fileContents);
	    }
	    tabItems.add(tabItem);
	    textARTCodes.add(textARTCode);
	}
    }

    /****** Populate table with variable names *********/
    private void insertTable()
    {

	if (variablesExist)
	{
	    for (int i = 0; i < variableNamesList.size(); i++)
	    {
		TableItem item = new TableItem(table, SWT.NONE);
		item.setText(new String[] { "" + (i + 1), variableIds.get(i), variableNames.get(i) });

	    }
	}
    }

    /****** Update table contents after user changes *********/
    private void updateTable()
    {
	String chkVariableName;
	Boolean valid = Boolean.TRUE.booleanValue();
	TableItem[] tableItems = table.getItems();

	updatedVariableNames = new ArrayList<String>();
	for (int i = 0; i < tableItems.length; i++)
	{

	    variableNames.set(i, tableItems[i].getText(2));
	    chkVariableName = tableItems[i].getText(2);
	    valid = isValidIdentifier(chkVariableName);

	    if (!chkVariableName.equals("-"))
	    {
		if (valid)
		{
		    updatedVariableNames.add(i, variableIds.get(i) + '_' + variableNames.get(i));
		} else
		{

		    MessageBox messageBox = new MessageBox(new Shell(), SWT.ICON_WARNING | SWT.OK);
		    messageBox.setText("Error Message");
		    messageBox.setMessage("Invalid Variable Name: " + tableItems[i].getText(2)
			    + "\nEither place a valid identifier or \'-\' for nothing.");
		    messageBox.open();
		    updatedVariableNames.add(i, variableIds.get(i));
		    tableItems[i].setText(2, "-");
		}
	    } else
	    {
		updatedVariableNames.add(i, variableIds.get(i));
	    }
	}

	/**** Update code's text field ****/
	updateText();
    }

    /****** Update Variable names *********/
    private void updateVariableNames()
    {
	TableItem[] tableItems = table.getItems();
	ArrayList<String> newVariableNames = new ArrayList<String>();

	for (int i = 0; i < tableItems.length; i++)
	{
	    if (!tableItems[i].getText(2).equals("-"))
	    {
		newVariableNames.add(tableItems[i].getText(1) + "_" + tableItems[i].getText(2));
	    } else
	    {
		newVariableNames.add(tableItems[i].getText(1));
	    }
	    variableNamesList.set(i, newVariableNames.get(i));
	}
	oldVariableNames = variableNamesList;
    }

    /****** Updates styled text field *********/
    private void updateText()
    {
	ArrayList<String> oldARTCode;// = new ArrayList<String>();
	ArrayList<String> newARTCode;// = new ArrayList<String>();

	for (int j = 0; j < textARTCodes.size(); j++)
	{
	    newARTCode = new ArrayList<String>();
	    oldARTCode = new ArrayList<String>(Arrays.asList(textARTCodes.get(j).getText().split("\n")));
	    String line = "";
	    for (int i = 0; i < oldARTCode.size(); i++)
	    {
		line = oldARTCode.get(i);
		for (int k = 0; k < variableNamesList.size(); k++)
		{

		    if (!oldVariableNames.get(k).equals(updatedVariableNames.get(k)))
		    {

			line = line.replaceAll(oldVariableNames.get(k), updatedVariableNames.get(k));
		    }

		}
		newARTCode.add(line);
	    }

	    /** Update text field with updated contents **/
	    insertText(textARTCodes.get(j), newARTCode);
	}
	for (int i = 0; i < oldVariableNames.size(); i++)
	{
	    oldVariableNames.set(i, updatedVariableNames.get(i));
	}

    }

    /****************** Update .ART files contents *************/
    private void updateCode()
    {

	for (int i = 0; i < fileHandler.artFilesPaths.size(); i++)
	{
	    fileHandler.updateFileContents(fileHandler, textARTCodes);
	}

	updateVariableNames();

    }

    /*********** Populate text field with file contents *************/
    private void insertText(StyledText ARTCode, ArrayList<String> textContents)
    {
	ARTCode.setText(""); // Removes old contents
	for (int i = 0; i < textContents.size(); i++)
	{
	    ARTCode.append(textContents.get(i) + "\n");
	}
    }

    /*********** Check validity of variable name entered by user **********/
    private static boolean isValidIdentifier(String s)
    {
	if (s.isEmpty())
	{
	    return false;
	}

	for (int i = 0; i < s.length(); i++)
	{
	    if (!Character.isJavaIdentifierPart(s.charAt(i)))
	    {
		return false;
	    }
	}
	return true;
    }

    @Override
    public boolean canFlipToNextPage()
    {
	return false;
    }
}
