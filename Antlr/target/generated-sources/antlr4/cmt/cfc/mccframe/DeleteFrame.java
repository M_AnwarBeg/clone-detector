package cmt.cfc.mccframe;

import javax.swing.JFrame;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.WindowEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;

import cmt.cddc.methodclones.MethodCloneWriterDB;
import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.utility.FileHandler;



public class DeleteFrame {
	int totalTabs;
    public static MethodClones mcc;
    FileHandler fileHandler;
    ArrayList<TabItem> tabItems = new ArrayList<TabItem>();
    String textJAVACodes = "";
    JTextArea JavaCode = new JTextArea();
    public int mccId;
   	public int mccIndex;
   	boolean flag = true;
   	JFrame f1= new JFrame();
   	

    private JTabbedPane tabs = new JTabbedPane();
    
	public DeleteFrame(int mccID){
		mccId = mccID;
		
		fileHandler = new FileHandler("MCC_Template\\+MCC_"+mccID);
		totalTabs = fileHandler.listOfJAVAFiles.size();
		insertTabs();
		f1.setLocationByPlatform(true);
		f1.setSize(600,600);
	f1.setVisible(true);
	}
	private JPanel createPanel() {
		JPanel panel = new JPanel(new GridBagLayout());
		//JPanel panel = new JPanel(new GridLayout(2,1));
		addtextarea(panel);
		
		return panel;

	}
	
private void addtextarea(JPanel panel) {
	GridBagConstraints c = new GridBagConstraints();
	c.gridy = 0;
		c.gridx = 0;
		c.insets = new Insets(1, 1, 1, 1);
	c.anchor = GridBagConstraints.NORTH;
		
		JTextArea JavaCode1 = new JTextArea(25,25);
		
		JavaCode1.setLineWrap(true);
		JavaCode1.setWrapStyleWord(true);
		
		//panel.is
		//JavaCode1.Scrol;
		
		//JScrollPane scrol = new JScrollPane(JavaCode1);
		JScrollPane scrollPane=new JScrollPane(JavaCode1, 
				   ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,  
				   ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBackground(Color.BLUE);
		
		JavaCode1.setText(textJAVACodes);
		JavaCode1.setEditable(false);
		//JavaCode1.setAlignmentX(10);
		//JavaCode1.setAlignmentY(10);
		//panel.add(scrol, GridBagConstraints.BOTH);
		JButton deleteButton = new JButton("Delete");
	    JButton cancelButton = new JButton("Cancel");
		panel.add(scrollPane);
		
		c.gridy=1;
		c.gridx= 0;
		c.anchor = GridBagConstraints.CENTER;
		//c.gridwidth =2;
		panel.add(deleteButton, c);
		c.gridy=2;
		c.gridx= 0;
		//panel.add(deleteButton, c);
		panel.add(cancelButton, c);
		panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Java Code"));
		
		deleteButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				int dialogButton = JOptionPane.YES_NO_OPTION;
	            //JOptionPane.showConfirmDialog (null, "You are about to unframe ?","Warning",dialogButton);
	            int dialogResult = JOptionPane.showConfirmDialog (null, "You are about to unframe this clone.\nAre you sure to continue?","Warning",dialogButton);
	            if(dialogResult == JOptionPane.YES_OPTION){
	            	MethodCloneWriterDB.deleteFrame(mccId);
	            	close();
	            	
	            	            	
	            }
			}
		});
		cancelButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				close();
			}
		});
		
		f1.add(panel);
		
    	

		
		
	}
	public void close() {
		f1.setVisible(false);
	}
	
	
	
	public void insertTabs()
    {
	

	for (int i = 0; i < totalTabs; i++)
	{
		ArrayList<String> fileContents = new ArrayList<String>(
			    fileHandler.getFileContents(fileHandler.javaFilesPaths.get(i)));
		 if (!fileContents.isEmpty())
		    {
			insertText(JavaCode, fileContents);
		    }
		 textJAVACodes = JavaCode.getText();
		tabs.addTab(fileHandler.listOfJAVAFiles.get(i), createPanel());
	       	    
	}
	f1.add(tabs);
	
	
	
    }
	 /*********** Populate text field with file contents *************/
    public void insertText(JTextArea JavaCode, ArrayList<String> textContents)
    {
	JavaCode.setText(""); // Removes old contents
	for (int i = 0; i < textContents.size(); i++)
	{
	    JavaCode.append(textContents.get(i) + "\n");
	}
    }
	
	

}
