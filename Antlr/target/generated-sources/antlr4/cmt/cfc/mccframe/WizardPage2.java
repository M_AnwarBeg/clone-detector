package cmt.cfc.mccframe;

import java.util.ArrayList;

import cmt.cfc.fccframe.FCCTemplate;
import cmt.cfc.sccframe.FramingStructure;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import java.util.StringTokenizer;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.sccframe.SCCTemplate;

public class WizardPage2 extends WizardPage implements Listener {

	
	private RenamingWizard wizard;
	WizardPage3 page3;
	protected Object result;
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	private Text text;
	private Table table;
	public static ArrayList<FrameBreak> breakNames = new ArrayList<FrameBreak>();
	public static ArrayList<String> breakName = new ArrayList<String>();
	public static ArrayList<Integer> sccS = new ArrayList<Integer>();
	public static ArrayList<FramingStructure> FrameStruct= new ArrayList<FramingStructure>(); 
	
	public WizardPage2(RenamingWizard wizard) {
		// TODO Auto-generated constructor stub
		super("Renaming");
		
		setTitle("Rename Break Names");
		setDescription("Break Names are used in the SCC Frames");
		this.wizard = wizard;
		
	}

	public void renameBreak(MethodClones MCC ) {
		
		breakNames = new ArrayList<FrameBreak>();
		
		ArrayList<Integer> newSccs = new ArrayList<Integer>();
		newSccs=getSCCs(MCC.getMCCInstances().get(0));
		for(int i=0; i<newSccs.size(); i++)
		{	
			FrameBreak fr= new FrameBreak();
		fr.setName("break_" + newSccs.get(i));
		fr.setSCCId(newSccs.get(i));
		
		breakNames.add(i, fr);
		}
		
	}
		
	public void renameBreaks(ArrayList<FramingStructure> frameSt)
	{
		for(int i=0; i<wizard.getFrameStructure().size();i++)
		{
			breakName.add(wizard.getFrameStructure().get(i).breakName);
			sccS.add(wizard.getFrameStructure().get(i).getSCCId());
		}	
	}
	
//	public IWizardPage getNextPage()
//    {
////		if (getContainer().getCurrentPage().equals(wizard.getPage2())  )
////		{
////			 for(int i=0; i<MCCTemplate.FrameSt.size(); i++)
////				{
////				MCCTemplate.extractSimpleClonesUpdated(MCCTemplate.getMethodClones(),MCCTemplate.FrameSt.get(i));	
////				}	
////				MCCTemplate.generateTemplate();
////				
////		}
//				page3= wizard.getPage3();
//				return page3;	
//	 
//    }
	
	public IWizardPage getNextPage() {
        //kind of hack to detect without overriding WizardDialog#nextPressed()
        boolean isNextPressed = "nextPressed".equalsIgnoreCase(Thread.currentThread().getStackTrace()[2].getMethodName());
        if (isNextPressed) {
        	
        	boolean validatedNextPress = this.nextPressed();
            if (!validatedNextPress) {
                return this;
            }
        	
        	
        	
        	if(MCCTemplate.mccTrigger==true)
        	{
        		
        	 for(int i=0; i<MCCTemplate.FrameSt.size(); i++)
				{
				MCCTemplate.extractSimpleClonesUpdated(MCCTemplate.getMethodClones(),MCCTemplate.FrameSt.get(i));	
				}	
				//MCCTemplate.generateTemplate();
        	}
        	//MCCTemplate.clearAll();
        	if(FCCTemplate.fccTrigger==true)
        	{
        		for(int i=0; i<FCCTemplate.FrameSt.size(); i++)
				{
				FCCTemplate.extractSimpleClonesUpdated(FCCTemplate.getFileClones(),FCCTemplate.FrameSt.get(i));	
				}	
				
        	}
        	
        	
        	
            
        }
        //return super.getNextPage();
        return wizard.getPage3();
    }
	 protected boolean nextPressed() {
	        boolean validatedNextPressed = true;
	        try {
	            //your checking here: remote connection, local directory/file creation, showing dialogs, etc... 
	        } catch (Exception ex) {
	            System.out.println("Error validation when pressing Next: " + ex);
	        }
	        return validatedNextPressed;
	    }
	
	
	
	

	
	
	
	public ArrayList<Integer> getSCCs(MethodCloneInstance m )
	{
		ArrayList<Integer> scc = new ArrayList<Integer>();
		for(int i=0; i<m.getNewSccs_Contained().size(); i++)
		{
			scc.add(m.getNewSccs_Contained().get(i).getSCCID());
			
		}
		
		
		return scc;
		
	}
	
	
	
	
	@Override
	public void createControl(Composite parent) {
		// TODO Auto-generated method stub
	
		Composite container = new Composite(parent, SWT.CLOSE | SWT.TITLE | SWT.MIN);
		setControl(container);

		FrameStruct=RenamingWizard.getFrameStructure();	
		renameBreaks(RenamingWizard.getFrameStructure());
		
		Text text_1 = new Text(container, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text_1.setBounds(305, 86, 259, 209);
		
		
		text = new Text(container, SWT.BORDER );
		text.setBounds(109, 26, 131, 26);
		
		Label lblName = new Label(container, SWT.NONE);
		lblName.setBounds(34, 29, 69, 26);
		lblName.setText("Break Name");
		
		Button btnReplace = new Button(container, SWT.NONE);
		btnReplace.setBounds(269, 26, 75, 26);
		btnReplace.setText("Replace");
		
		table = new Table(container, SWT.BORDER | SWT.FULL_SELECTION);
		table.setBounds(33, 86, 233, 209);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnBreaks = new TableColumn(table, SWT.NONE);
		tblclmnBreaks.setWidth(100);
		tblclmnBreaks.setText("Breaks");
		
		

		
		text_1.setEditable(false);
		text_1.setBackground(container.getDisplay().getSystemColor(SWT.COLOR_WHITE));
		
		
		TableColumn tblclmnSccs = new TableColumn(table, SWT.NONE);
		tblclmnSccs.setWidth(257);
		tblclmnSccs.setText("SCC's");
		
		Label lblCodeAfterBreak = new Label(container, SWT.NONE);
		lblCodeAfterBreak.setBounds(305, 65, 123, 15);
		lblCodeAfterBreak.setText("Code after break:");
		
		
		 final TableColumn [] columns = table.getColumns ();
//		 for (int i = 0; i < breakNames.size(); i++)
//		{	
//		TableItem item= new TableItem(table,SWT.NONE);
//		//item.setText("break_"+(breakNames.get(i)).toString());
//		item.setText(breakNames.get(i).getName());
//		item.setText(1,(Integer.toString((breakNames.get(i)).getSCCId())));
//		}
		
		 
//		 for(int i=0; i<wizard.getFrameStructure().size();i++)
//		 {
//			 TableItem item= new TableItem(table,SWT.NONE);
//			 item.setText(wizard.getFrameStructure().get(i).breakName);
//			 item.setText(1,(Integer.toString(wizard.getFrameStructure().get(i).getSCCId())));
//		 }
		 
		 
		 for (int i = 0; i < breakName.size(); i++)
		{	
		TableItem item= new TableItem(table,SWT.NONE);
		//item.setText("break_"+(breakNames.get(i)).toString());
		item.setText(breakName.get(i));
		item.setText(1,(Integer.toString(sccS.get(i))));
		} 
		 
		 
		 
		 
		 
		 
		
		 table.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {

					TableItem item= table.getItem(table.getSelectionIndex());			
					String sccNum = item.getText();
					StringTokenizer st = new StringTokenizer(sccNum, "_");
					st.nextToken();
					sccNum = st.nextToken();
					
					for(int i=0; i<MCCTemplate.MCI_List.get(0).getAnalysisDetails().size(); i++)
					{
						String check = MCCTemplate.MCI_List.get(0).getAnalysisDetails().get(i);
						st = new StringTokenizer(MCCTemplate.MCI_List.get(0).getAnalysisDetails().get(i), ",");
						check = st.nextToken();
						if(sccNum.equals(check))
						{
							text_1.redraw();
							st = new StringTokenizer(MCCTemplate.MCI_List.get(0).getAnalysisDetails().get(++i), ",");
							while(st.hasMoreTokens())
							{
								check = st.nextToken();
							}
							text_1.setText(check);
						}
					}
				}
			});
		
		
		
		
		
		
		
		
		
		
		
		btnReplace.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				
			
			TableItem item= table.getItem(table.getSelectionIndex());			
			
			item.setText(text.getText());
			FrameStruct.get(table.getSelectionIndex()).setBreak(text.getText());
			FrameStruct.get(0);
			//breakNames.set(table.getSelectionIndex(), text.getText());	
				text.setText("");
				
			}
		});
		
		
		
	}
		
		
		
		
		
		
		
		
		
		
		
		
		
	

	@Override
	public void handleEvent(Event arg0) {
		// TODO Auto-generated method stub
		
	}
}
