package cmt.cfc.mccframe;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;

import cmt.cddc.methodclones.MethodClones;
import cmt.cfc.utility.FileHandler;


public class DeleteFrameWizardPage1 extends WizardPage{// implements DeleteFrame{
	
	int totalTabs = FileHandler.fileHandler.listOfJAVAFiles.size();
    public static MethodClones mcc;
    
    ArrayList<TabItem> tabItems = new ArrayList<TabItem>();
    ArrayList<StyledText> textJAVACodes = new ArrayList<StyledText>();
    int mccId;
    StyledText JavaCode;
    Button Delete;
    Button Cancel;
    TabFolder tabFolder;
    Composite composite;
    DeleteFrameWizard wizard;
    TabItem tabItem;

    
	public DeleteFrameWizardPage1(int mccID, DeleteFrameWizard deleteFrameWizard) {
		// TODO Auto-generated constructor stub
		super("Delete Framed Java Code Here");
		this.mccId = mccId;
		this.wizard = deleteFrameWizard;
		FileHandler.fileHandler = new FileHandler("MCC_Template\\+MCC_"+mccId);
		FileHandler.fileHandler.listOfJAVAFiles.size();
		setTitle("Framed Java Files");
		setDescription("Framed JAVA code is displayed.\n" + "User can delete JAVA code here.");
	}
    @Override
    public void createControl(Composite parent)
    {
	composite = new Composite(parent, SWT.NONE);
	GridLayout gridLayout = new GridLayout(1, false);
	composite.setLayout(gridLayout);
	GridData gridData = new GridData(GridData.FILL_HORIZONTAL);
	gridData.grabExcessHorizontalSpace = true;
	new Label(composite, SWT.NULL);

	// tabs work starts here

	GridData TabgridData = new GridData(GridData.FILL_HORIZONTAL | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	tabFolder = new TabFolder(composite, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	TabgridData.heightHint = 250;
	// TabgridData.widthHint = 50;
	tabFolder.setLayoutData(TabgridData);
	insertTabs(composite, tabFolder);
	// buttons work starts here
	Composite buttonsLayout = new Composite(composite, SWT.NONE);
	FillLayout fillLayout = new FillLayout(SWT.HORIZONTAL);
	fillLayout.spacing = 10;
	// fillLayout.
	buttonsLayout.setLayout(fillLayout);
	
//		Delete = createDeleteButton(composite);
//		Cancel = createCancelButton(composite);
		

	/// buttons ends here
	setControl(composite);
    }
      


		    	
				

	
    
    
    public void insertTabs(Composite composite, TabFolder tabFolder)
    {
	

	for (int i = 0; i < totalTabs; i++)
	{
	    tabItem = new TabItem(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

	    tabItem.setText(FileHandler.fileHandler.listOfJAVAFiles.get(i));

	    JavaCode = new StyledText(tabFolder, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
	    JavaCode.setLayoutData(composite);
	    JavaCode.setEditable(false);
	    tabItem.setControl(JavaCode);
	    JavaCode.setText("");
	    JavaCode.setEditable(true);

	    ArrayList<String> fileContents = new ArrayList<String>(
	    		FileHandler.fileHandler.getFileContents(FileHandler.fileHandler.javaFilesPaths.get(i)));

	    if (!fileContents.isEmpty())
	    {
		insertText(JavaCode, fileContents);
	    }
	    tabItems.add(tabItem);
	    textJAVACodes.add(JavaCode);
	}
    }
    
    /*********** Populate text field with file contents *************/
    public void insertText(StyledText JavaCode, ArrayList<String> textContents)
    {
	JavaCode.setText(""); // Removes old contents
	for (int i = 0; i < textContents.size(); i++)
	{
	    JavaCode.append(textContents.get(i) + "\n");
	}
	
    
	/*@Override
	public int func() {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public void createButtonsForButtonBar(Composite parent) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Button createCancelButton(Composite parent) {
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(IDialogConstants.CANCEL_LABEL);
		setButtonLayoutData(button);
		button.setFont(parent.getFont());
		button.setData(Integer.valueOf(IDialogConstants.CANCEL_ID));
		//button.addSelectionListener(cancelListener);
		return button;
		
	}
	@Override
	public Button createDeleteButton(Composite parent) {
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText("Delete");
		setButtonLayoutData(button);
		button.setFont(parent.getFont());
		//button.setData(Integer.valueOf(IDialogConstants.CANCEL_ID));
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				int dialogButton = JOptionPane.YES_NO_OPTION;
	            //JOptionPane.showConfirmDialog (null, "You are about to unframe ?","Warning",dialogButton);
	            int dialogResult = JOptionPane.showConfirmDialog (null, "You are about to unframe this clone.\nAre you sure to continue?","Warning",dialogButton);
	            if(dialogResult == JOptionPane.YES_OPTION){
	            	CloneDB.deleteFrame(mccId);
	            	Delete.setEnabled(false);
				
			}
	            }

			@Override
			public void widgetDefaultSelected(SelectionEvent e){
				// TODO Auto-generated method stub
				
			}
			
		});
		*/
		
		//return button;
	}
	
	
	

	

}
