package cmt.cfc.mccframe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.methodclones.MethodClonesReader;
import cmt.common.Directories;
import cmt.cvac.viewobjects.PrimaryMcsObject;
import cmt.cvac.views.MccView;



public class MCSTemplate {
	
	final String OUTPUT_PATH = "\\ART_Output\\Output\\";
	
	public static int level = 0;
	public static ArrayList <Variable> variables=new ArrayList<Variable>(); 
	public static int currIndex=0;
	public static ArrayList<String> templateNames=new ArrayList<String>();
	public static ArrayList<String> fileNames=new ArrayList<String>();
	public static ArrayList<String> filesToOpen=new ArrayList<String>();
	public static ArrayList<Integer> markFilesForMapping = new ArrayList<Integer>();
	public static boolean callFromMCS=false;
	

	
	public static void generateTemplate(int mcsId)
	{
		ArrayList<PrimaryMcsObject> list=MethodClonesReader.getMcsList(); 
		
		MethodCloneStructure mcs = ClonesReader.getMethodCloneStructure(mcsId);
		markFilesForMapping = MCSAnalyzer.analyzeMCS(mcs);
		ArrayList<MethodClones> mccsToInclude = null;
		
		
		
		/*
			for (int i=0;i<list.get(mcsId).getStructure().size();i++)
			{
			
				for(int t=0;t<ClonesReader.getMethodClones().size();t++)
				{
					if(ClonesReader.getMethodClones().get(t).getClusterID()==list.get(mcsId).getStructure().get(i)) {
					 System.out.println(ClonesReader.getMethodClones().get(t).getClusterID());
					    callFromMCS=true;
		             MCCTemplate.genMCCTemplate(ClonesReader.getMethodClones().get(t).getClusterID());
				}
			}
		}
		writeSpc(mcsId);
		
		*/
		
		
		
		
		
		templateNames.clear();
		 
		variables.clear();
		currIndex=0;
		try
		{
			CloneRepository.getFrameId();
			CloneRepository.openConnection();
			Statement stmt2 = CloneRepository.conn.createStatement();
			stmt2.execute("use framerepository;");
			ResultSet rs = stmt2.executeQuery("select fid from mcsframes where mcsid="+mcsId);
			
			if(rs.next())
			{
				System.out.println("Selected method clone is already framed."); // change it to pop up later.
			}
			else
			{
					for (int i=0;i<list.get(mcsId).getStructure().size();i++)
					{
						try
						{
							int j=0;
							while(true)
							{
								if(list.get(mcsId).getStructure().get(i)==ClonesReader.getMethodClones().get(j).getClusterID())
								{
									for(int m=0;m<ClonesReader.getMethodClones().size();m++)
									{
										if(ClonesReader.getMethodClones().get(m).getClusterID()==list.get(mcsId).getStructure().get(i))
										{
											MCCsAnalyser.analyze(ClonesReader.getMethodClones().get(m));
											CliqueSelectionWizard cliqueWizard = new CliqueSelectionWizard();
											cliqueWizard.setMCCID(ClonesReader.getMethodClones().get(m).getClusterID());
										     cliqueWizard.setMccIndex(m);
										     ISelection selection = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService().getSelection();
											IStructuredSelection selectionToPass = StructuredSelection.EMPTY;
											if (selection instanceof IStructuredSelection)
											{
												selectionToPass = (IStructuredSelection) selection;
											}
											cliqueWizard.init(PlatformUI.getWorkbench(), selectionToPass);
											Shell shell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
											WizardDialog dialog = new WizardDialog(shell, cliqueWizard);
											
											    dialog.create();
											    
											MCCsAnalyser.modifymcc(ClonesReader.getMethodClones().get(m), CliqueSelectionWizardPage1.SccsSelected);
											for (MethodCloneInstance instance :ClonesReader.getMethodClones().get(m).getMCCInstances())
										    {
												instance.DisplayAnalysisDetails();
										    }
										    System.out.println(ClonesReader.getMethodClones().get(m).getClusterID());
										    callFromMCS=true;
                                            MCCTemplate.genMCCTemplate(ClonesReader.getMethodClones().get(m).getClusterID());
                                            McFrameRepo newMC = new McFrameRepo();
                                            newMC.setMccId(ClonesReader.getMethodClones().get(m).getClusterID());
                                            MccView.repo.add(newMC);
										}
									}
								}
								if(j<ClonesReader.getMethodClones().size()-1)
								{
							    	j++;
							    }
								else
								{
									break;
								}
							}
						}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				}
			}
			
			//writeSpc(mcsId);
			
			
			if(!markFilesForMapping.isEmpty()) {
				
				try
				{
					Shell shell = new Shell();	
					MCSMappingUI mcsMapping_UI = new MCSMappingUI(shell,0);
					mcsMapping_UI.setMethodCloneStructure(mcs);
					mcsMapping_UI.open();
			} catch (Exception e)
				{
				    e.printStackTrace();
				    System.err.println(e.getMessage());
				}
				
			}
			// Marking Instances
			for(int t=0;t<ClonesReader.getMethodClones().size();t++) 
			{
				
			    int size =ClonesReader.getMethodClones().get(t).getMCCInstances().size();
			    for(int ins=0; ins<size; ins++)
			    {
			    	if(!(markFilesForMapping.contains(ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getFileID())))
			    	{
			    	if(search(ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getFileID(),mcsId)) {
			    		ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().setMark(true);
			    		
			    		}
			    	else
			    		{
			    		ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().setMark(false);
			    		}
			    	}
			    		
			    
			    }
			    
			    for(int ins=0; ins<size; ins++)
			    {
			  //Marking Variables
		    	if(ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getMark()==false)
		    	{
		    		for(int i=0; i<variables.size(); i++)
		    		{
		    			if(variables.get(i).mark.size()>list.get(mcsId).getNumberOfInstance() && ins<variables.get(i).mark.size() && variables.get(i).mccId==ClonesReader.getMethodClones().get(t).getClusterID())
		    			{		
		    				variables.get(i).mark.set(ins, false);
		    			}
		    		}
		    	}
		    	/*{
		    		int a= ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getFileID();	
		    		
				    
				 
				    	for(int i=0; i<variables.size(); i++)
				    	{
				    		
				    		variables.get(i).fid.add(ins, -1);
				    		variables.get(i).fid.set(ins, a);
				    	}
		    	}*/
			    }
		  
			}
			System.out.print("Done");
				writeSpc(mcsId);
				
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
			
		
	}
	
	public static boolean repeatMCS(int mcsId) {
		boolean flag=false;
		
		ArrayList<PrimaryMcsObject> list=MethodClonesReader.getMcsList();
		for(int i=0; i<list.get(mcsId).getStructure().size();i++)
		{
			if(isRepeating(list.get(mcsId).getStructure().get(i),mcsId))
					{
						flag= true;
						break;
					}
		}
		
		return flag;
	}

	//To be improved and tested
	public static boolean search(int file,int mcsId)
	{
		boolean flag=false;
		ArrayList<Integer> files = fileIds(mcsId);
		
		for(int i=0;i<files.size();i++)
		{
			
			if(file==files.get(i))	
			flag=true;
		}	
		return flag;
	}
	
	public static ArrayList<Integer> fileIds(int mcsId)
	{
	
		ArrayList<Integer> filesInMcs= new ArrayList <Integer>();
		ArrayList<PrimaryMcsObject> list=MethodClonesReader.getMcsList();
		for (int i=0;i<list.get(mcsId).getCloneList().size();i++)
		{
			filesInMcs.add(list.get(mcsId).getCloneList().get(i).getFId());    
		}
		
		
		return filesInMcs;
	}
	
	
	public static int trueInstances(ArrayList <Variable> var,int index)
	{
		int count = 0;
		for (int i=0; i<var.get(index).mark.size(); i++)
		{
			if(var.get(index).mark.get(i)==true)
			{
				count++;
			}
		}
		
		return count;
	}
	
	public static boolean isRepeating(int a,int mcsId) {
		ArrayList<PrimaryMcsObject> list=MethodClonesReader.getMcsList();
		int occuranceCount=0;
		for(int i=0; i<list.get(mcsId).getStructure().size();i++)
		{
			
			if(a==list.get(mcsId).getStructure().get(i))
			{
				occuranceCount++;
			}
		}
		
		if(occuranceCount>1)
			return true;
		else 
			return false;
		
	}
	
	public static boolean isInMCS(int mccId,int mcsId)
	{
		ArrayList<PrimaryMcsObject> list=MethodClonesReader.getMcsList();
		boolean flag=false;
		if( list.get(mcsId).getStructure().contains(mccId))
		{
			flag=true;
		}
		return flag;
		
		
		
		
	}
	
	
	
	
	
	public static void writeSpc(int mcsId)
	{
		ArrayList<PrimaryMcsObject> list=MethodClonesReader.getMcsList();
		int nOfInstances= list.get(mcsId).getNumberOfInstance();
		try
		{
			String path = Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsSPC);
			String path_temp = Directories.getAbsolutePath(Directories.OUTPUT_PATH_mcsTemplate);
			String path_mcc= Directories.getAbsolutePath(Directories.OUTPUT_PATH_mccTemplate);
			 //new File(path + "\\MCS_" + mcsId + "\\").mkdir();
			    String SPC_PATH = "\\" + mcsId + "_MCS_SPC.art";
			    
			    String templatePath="\\" + mcsId + "_MCS_template.art";
	
			    MCCTemplate.refresh(path + SPC_PATH); // Delete files if they already exist //
	
	
			    FileWriter spc_file = new FileWriter(path + SPC_PATH, true);
		 
			    BufferedWriter spc_output = new BufferedWriter(spc_file);
			    
			    FileWriter template_file = new FileWriter(path_temp + templatePath, true);
	
			    BufferedWriter templateOutput = new BufferedWriter(template_file);
			    
			    spc_output.write("#set fileName = ");
			    for(int j=0;j<nOfInstances;j++)
			    {
			    	//spc_output.write("\""+Directories.getAbsolutePath(Directories.framingOutput) + "MCSID_"+mcsId+"_Instance_"+j+"\"");
			    	spc_output.write("\"MCSID_"+mcsId+"_Instance_"+j+"\"");
			    	filesToOpen.add(Directories.getAbsolutePath(Directories.framingOutput)+"MCSID_"+mcsId+"_Instance_"+j);
			    	if(j!=nOfInstances-1)
			    	{
			    		spc_output.write(",");
			    	}
			    	else
			    	{
			    		spc_output.write("\n");
			    	}
			    }
			    
			    for(int t=0;t<ClonesReader.getMethodClones().size();t++) {
			    	if(isInMCS((ClonesReader.getMethodClones().get(t).getClusterID()),mcsId)) {
				    	if(!isRepeating(ClonesReader.getMethodClones().get(t).getClusterID(),mcsId))
				    	{
					    spc_output.write("#set MCC_fileName_"+ClonesReader.getMethodClones().get(t).getClusterID()+"= " );
				    	}
					    int size =ClonesReader.getMethodClones().get(t).getMCCInstances().size();
					    for(int ins=0; ins<size; ins++)
						    {  		
						    	if(!isRepeating(ClonesReader.getMethodClones().get(t).getClusterID(),mcsId))
						    	{
						    	if(ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getMark()==true) {	
						    	spc_output.write("\"" +"MCCID_"+ClonesReader.getMethodClones().get(t).getClusterID()+"_Inst_"+ins+"\"");
						    	
						    	if(ins!=size-1)
							    	{
							    		spc_output.write(",");
							    	}
							    	else
							    	{
							    		spc_output.write("\n");
							    	}
						    	}
						    }
					    }
			    }
		   }
			    //MCSTemplate.fileNames.clear();
			    spc_output.write("% Here I will set all the place-holder variables\n\n");
			    spc_output.write("#set path =");
			    spc_output.write("\"");
			    spc_output.write(Directories.getAbsolutePath(Directories.framingOutput));
			    spc_output.write("\""+"\n");
			    for(int k=0,varCount=0;k<variables.size() && trueInstances(variables,k)==nOfInstances;k++)
			    {
			    	spc_output.write("#set " + variables.get(k).name + " = ");
			    	
			    	for(int l=0;l<variables.get(k).values.size();l++)
			    	{
			    		
			    		
			    		{
			    		if(variables.get(k).mark.get(l)==true && trueInstances(variables,k)==nOfInstances )	
			    		{	
			    		spc_output.write("\"" + variables.get(k).values.get(l) + "\"");
			    		varCount++;
			    		if(l != variables.get(k).values.size()-1 )
				    		{
				    			spc_output.write(",");
							}
			    		else
							{
							    spc_output.write("\n");
							}
			    		}
			    		}
			    	}
			    }
			    //spc_output.write("#while fileName1\n");
			    //spc_output.write("#output ?@fileName1?\".java\"\n");
			    spc_output.write("#while ");
			    level++;
			    for(int i=0; i<variables.size() && trueInstances(variables,i) ==nOfInstances;i++)
			    { 
			    	spc_output.write(variables.get(i).name+",");	
			    }
			    for(int t=0;t<ClonesReader.getMethodClones().size() && !isRepeating(ClonesReader.getMethodClones().get(t).getClusterID(),mcsId);t++) {
			    	if(isInMCS((ClonesReader.getMethodClones().get(t).getClusterID()),mcsId))
				    spc_output.write("MCC_fileName_"+ClonesReader.getMethodClones().get(t).getClusterID()+"," );
			    }
			    spc_output.write("fileName");
			    
			    spc_output.write("\n");
			    for(int i=0; i<level; i++)
		    	{
		    		spc_output.write("\t");
		    	}
			    spc_output.write("#output ?@path??@fileName?\".java\"\n");
			    for(int t=0;t<ClonesReader.getMethodClones().size(); t++)
			    {
			    if(!isRepeating(ClonesReader.getMethodClones().get(t).getClusterID(),mcsId)   ) {
			    	if(isInMCS((ClonesReader.getMethodClones().get(t).getClusterID()),mcsId)){
			    
					    spc_output.write("\n");
					    for(int i=0; i<level; i++)
				    	{
				    		spc_output.write("\t");
				    	}
					    spc_output.write("#adapt: \""+path_mcc + ClonesReader.getMethodClones().get(t).getClusterID() + "_MCC_template.art\"\n");
					    
					    for(int i=0; i<level; i++)
				    	{
				    		spc_output.write("\t");
				    	}
					    
					    spc_output.write("#endadapt\n\n");
			    	}
			    
			    }
			    else
			    {
			    	//spc_output.write("\n#adapt: \""+path_temp + ClonesReader.getMethodClones().get(t).getClusterID() + "_MCC_MCS.art\"\n");
				    //spc_output.write("#endadapt\n\n");
			    	for(int i=0; i<level; i++)
			    	{
			    		spc_output.write("\t");
			    	}
			    	spc_output.write("#select fileName\n");
			    	level++;
			    	

			    	 for(int j=0,f=0;j<nOfInstances && f<fileIds(mcsId).size();j++,f++) {
			    		 
			    		 for(int i=0; i<level; i++)
			    	    	{
			    	    		spc_output.write("\t");
			    	    	}
			    		 spc_output.write("#option ");
			    		 spc_output.write("\""+ "MCSID_"+mcsId+"_Instance_"+j +"\"");
			    		 level++;
			    		 
			    		 
			    		 //writing Instances
			    		 int size =ClonesReader.getMethodClones().get(t).getMCCInstances().size();
			    		 int instCount=0;
			    		
			    		// for(int f=0; f<fileIds(mcsId).size(); f++)
			    		 
						    {
						     spc_output.write("\n");
						     for(int i=0; i<level; i++)
						    	{
						    		spc_output.write("\t");
						    	}
			    			 spc_output.write("#set MCC_fileName_"+ClonesReader.getMethodClones().get(t).getClusterID()+"= " );
			    			 for(int ins=0; ins<size; ins++)
						    	 {
						    	if(ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getMark()==true) {
						    		if(fileIds(mcsId).get(f) == (ClonesReader.getMethodClones().get(t).getMCCInstances().get(ins).getMethod().getFileID()))
						    		{
						    	spc_output.write("\"" +"MCCID_"+ClonesReader.getMethodClones().get(t).getClusterID()+"_Inst_"+ins+"\"");
						    		instCount++;
						    	if(ins!=size-1 && instCount<nOfInstances)
							    	{
							    		spc_output.write(",");
							    	}
						    		
							    	else
							    	{
							    		spc_output.write("\n");
							    	}
						    	}
						    		
						    	}
						    }	 			    			 
						 }
					
				{
				
					 for(int k=0,varCount=0; k<variables.size() ;k++)
					    {	
						 	if(trueInstances(variables,k)>nOfInstances && variables.get(k).mccId==ClonesReader.getMethodClones().get(t).getClusterID())
						 	{	
						 		
						 		for(int i=0; i<level; i++)
						    	{
						    		spc_output.write("\t");
						    	}
					    	spc_output.write("#set " + variables.get(k).name + " = ");
					    	
					    	for(int l=0;l<variables.get(k).values.size();l++)
					    	{
					    		
					    		
					    		{
					    		if(variables.get(k).mark.get(l)==true && variables.get(k).fid.get(l)==fileIds(mcsId).get(f)  )	
					    		{	
					    		spc_output.write("\"" + variables.get(k).values.get(l) + "\"");
					    		varCount++;
					    		
					    		
					    		if(l != variables.get(k).values.size()-1 )
						    		{
						    			spc_output.write(", ");
									}
					    		else
									{
									    spc_output.write("\n");
									}
					    		}
					    		}
					    	}
					    }	
					
					    }

						    }
							spc_output.write("\n");
							for(int i=0; i<level; i++)
					    	{
					    		spc_output.write("\t");
					    	}
					    	spc_output.write("#endoption\n\n");
					    	level--;
			    	 }
			    	 for(int i=0; i<level; i++)
				     	{
				     		spc_output.write("\t");
				     	}
			    	 level--;
			    	 spc_output.write("#endselect\n\n");
			    	 for(int i=0; i<level; i++)
			     	{
			     		spc_output.write("\t");
			     	}
			    	 spc_output.write("#while ");
			    	 level++;
			    	 for(int i=0; i<level; i++)
			     	{
			     		spc_output.write("\t");
			     	}
			    	 //variables for the current Instance
			    	 for(int i=0; i<variables.size() ;i++)
					    { 
					    if(variables.get(i).mccId==ClonesReader.getMethodClones().get(t).getClusterID())
						    {
						    	spc_output.write(variables.get(i).name+",");	
						    }
					    }
			    	 spc_output.write("MCC_fileName_"+ClonesReader.getMethodClones().get(t).getClusterID());
			    	 spc_output.write("\n\n");
			    	 for(int i=0; i<level; i++)
				     	{
				     		spc_output.write("\t");
				     	}
			    	 spc_output.write("#adapt: \""+path_mcc + ClonesReader.getMethodClones().get(t).getClusterID() + "_MCC_template.art\"\n");
			    	 for(int i=0; i<level; i++)
				     	{
				     		spc_output.write("\t");
				     	}	
			    	 spc_output.write("#endadapt\n\n");
			    	 
			    	 level--;
			    	 for(int i=0; i<level; i++)
				     	{
				     		spc_output.write("\t");
				     	}
					 spc_output.write("#endwhile\n ");

			    }

			   }
			    level--;
			    for(int i=0; i<level; i++)
		    	{
		    		spc_output.write("\t");
		    	}
			    spc_output.write("#endwhile\n");
			    
			    
			    Statement stmt = CloneRepository.conn.createStatement();
			    stmt.execute("use framerepository;");
			    stmt.execute("insert into frames(path,level,links,issynced) values (\""+Directories.getAbsolutePath(path+SPC_PATH)+"\",7,1,1)");
			    
			    int spcid= CloneRepository.getFrameId();
			    stmt.execute("insert into mcsframes(mcsid,fid) values("+mcsId+","+spcid+")");
			    
		/////////................Template file...................//////////////////////
			    
					    /*for(int h=0;h<templateNames.size();h++)
					    {
					    	StringTokenizer st=new StringTokenizer(templateNames.get(h),"_");
					    	String  folderName="MCC_"+st.nextToken();
					    	
					    	path = Directories.getAbsolutePath("\\ART_Output\\MCC_Template\\"+folderName+"\\");
					    	templateOutput.write("#adapt: \""+path+templateNames.get(h)+"\"\n");
					    	templateOutput.write("#endadapt\n");
					    }*/
					    spc_output.close();
					    templateOutput.close(); 
			    
			    
			  
			    
			 // MCCTemplate.RunART(".\\MCS_Template\\" + SPC_PATH); // Run ART Processor
		}
	
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
}
	}
