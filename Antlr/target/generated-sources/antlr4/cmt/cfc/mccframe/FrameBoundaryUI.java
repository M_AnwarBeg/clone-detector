package cmt.cfc.mccframe;

import java.io.*;
import javax.swing.*; 
import javax.swing.event.*; 

import cmt.cddc.CloneRepository.ClonesReader;
import cmt.cddc.clonerunmanager.LoadListSetting;
import cmt.cddc.clonerunmanager.ProjectInfo;
import cmt.cddc.methodclones.MethodCloneInstance;
import cmt.cddc.methodclones.MethodCloneStructure;
import cmt.cddc.methodclones.MethodClones;
import cmt.cddc.simpleclones.SimpleClonesReader;
import cmt.cfc.sccframe.DynamicToken;
import cmt.cfc.sccframe.SCCTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.awt.Color;
import javax.swing.JOptionPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.custom.CCombo;
import javax.swing.JTable;
import org.eclipse.swt.custom.TableCursor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.layout.TableColumnLayout;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DragDetectListener;
import org.eclipse.swt.events.DragDetectEvent;
public class FrameBoundaryUI extends Dialog {
	
	
	protected Object result;
	protected Shell shell;
	private SCCTemplate temp;
	private MethodCloneInstance mcc;
	private Text text_1;
	String code;
	String firstLine;
	String endLine;
	
	MethodClones m;
	
	
	public FrameBoundaryUI(Shell parent, int style) {
		super(parent, style);
		setText("Edit SCC Boundaries");
	}	
	
	public Object open() {
		createContents();
		shell.open();
		Display display = getParent().getDisplay();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return result;
	}

	//set variables for renaming
//	public void setVariablesForNaming(SCCTemplate temp) {
//		this.temp = temp;
//		  StringBuilder structure = new StringBuilder();
//			
//			//for(Integer sccId: mcc.getNewSccs_Contained().g) {
//				//structure.append(sccId).append(",");
//			}		
			
	
	
	
	public void renameBreak(MethodClones MCC ) {
		
		try
		{
			MCC.getvCloneClasses();
			m=MCC;
//			code = SimpleClonesReader.getCodeSegmentToTokenize(MCC.getMCCInstances().get(0).getNewSccs_Contained().get(0).getFilePath(),
//					MCC.getMCCInstances().get(0).getNewSccs_Contained().get(0).getStartLine(),
//					MCC.getMCCInstances().get(0).getNewSccs_Contained().get(0).getEndLine());
//			
//			code=MCC.getMCCInstances().get(0).getNewSccs_Contained().get(0).getCodeSegment();
//			firstLine = code.subSequence(0, 10).toString();
			
		
		}
		catch(Exception e)
		{
			
		}
	}
		
	
	public ArrayList<String> getSCCs(MethodCloneInstance mci )
	{
		ArrayList<String> scc = new ArrayList<String>();
		for(int i=0; i<mci.getNewSccs_Contained().size(); i++)
		{
			scc.add(Integer.toString(mci.getNewSccs_Contained().get(i).getSCCID()));
			
		}
		
		
		return scc;
		
	}
	
	public ArrayList<String> extractInstances(int a)
	{
		ArrayList<String> sccInstances = new ArrayList<String>();
		
		
		for(int i=0; i<m.getMCCInstances().size(); i++)
		{
			for(int j=0; j<m.getMCCInstances().get(i).getNewSccs_Contained().size();j++)
			{
				if(m.getMCCInstances().get(i).getNewSccs_Contained().get(j).getSCCID()==a)
				{
				sccInstances.add(Integer.toString((m.getMCCInstances().get(i).getNewSccs_Contained().get(j).getFileId())));
				}
			}
		}	
		
		return sccInstances;
		
	}
	
	
	public String extractCode(int f)
	{
		String code="";
		
		
		for(int i=0; i<m.getMCCInstances().size(); i++)
		{
			for(int j=0; j<m.getMCCInstances().get(i).getNewSccs_Contained().size();j++)
			{
				if(m.getMCCInstances().get(i).getNewSccs_Contained().get(j).getFileId()==f)
				{
				code=(m.getMCCInstances().get(i).getNewSccs_Contained().get(j).getCodeSegment());
				}
			}
		}	
		
		return code;
		
	}
	
	private void createContents() {
		// TODO Auto-generated method stub
		shell = new Shell( SWT.CLOSE | SWT.TITLE);
		shell.setText("Mapp MCS.");
		ArrayList<String> SCCs = new ArrayList<String>();
		
		//shell = new Shell(getParent(), getStyle());
		shell.setSize(668, 562);
		shell.setText(getText());
		
		Group group = new Group(shell, SWT.NONE);
		group.setBounds(10, 28, 645, 495);
		
		text_1 = new Text(group, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		text_1.setBounds(10, 245, 477, 215);

		//text_1.setText(code);
		
		 text_1.setEditable(false);
		 text_1.setBackground(shell.getDisplay().getSystemColor(SWT.COLOR_WHITE));
				
		
		Label lblSccId = new Label(group, SWT.NONE);
		lblSccId.setBounds(10, 22, 72, 15);
		lblSccId.setText("SCC ID:");
		
		Button button_1 = new Button(group, SWT.NONE);
		button_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		button_1.setText("Select");
		button_1.setBounds(544, 460, 86, 25);
		
		List list = new List(group, SWT.BORDER);
		list.setBounds(10, 43, 98, 168);
		
		List list_1 = new List(group, SWT.BORDER);
		list_1.setBounds(143, 43, 98, 168);

		
		
		
		SCCs=getSCCs(m.getMCCInstances().get(0));
		
		for(int i=0;i<SCCs.size();i++)
		{
		list.add(SCCs.get(i));
		}
		
		

		list.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				
				
				// TODO Auto-generated method stub
				String selection = list.getItem(list.getSelectionIndex());
				
				ArrayList<String> sccInst = extractInstances(Integer.parseInt(selection));

				list_1.removeAll();
				
				for(String s : sccInst) {
					list_1.add(s);
					
				}		
			}
		});
		
		
		list_1.addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event e) {
				
				
				// TODO Auto-generated method stub
				String selection = list_1.getItem(list_1.getSelectionIndex());
				
				String codeSeg = extractCode(Integer.parseInt(selection));
				
				text_1.redraw();
				text_1.setText(codeSeg);
					
			}
		});

		
		Label lblInstances = new Label(group, SWT.NONE);
		lblInstances.setBounds(143, 22, 86, 15);
		lblInstances.setText("Instances");
		
		Label lblCodeSegment = new Label(group, SWT.NONE);
		lblCodeSegment.setBounds(10, 224, 98, 15);
		lblCodeSegment.setText("Code Segment");
		
		
		
		
	}
}
