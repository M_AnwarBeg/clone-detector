package cmt.cfc.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

import cmt.common.Directories;



public class Initializer
{
    private static Process art;
    public static BufferedReader errStream;
    public static BufferedReader outputStream;
    public static boolean flag;
	
  /*  private CloneMinerExecuter cloneMinerExecuter;
    private CloneMinerSettings minerSettings;
    public static Process miner;
    private static Process art;
    // private static int mintokensize = 50;
    // private static int methodunique=1;
    // private static int groupindex=0;
    public static ExternalThread errStream;
    public static ExternalThread outputStream;

    public Initializer()
    {
	minerSettings = new CloneMinerSettings();
	cloneMinerExecuter = new CloneMinerExecuter();
	// ExecuteCloneMiner();
	CloneDB.Initiate();
    }

    public void ExCloneMiner(IWizardContainer container, int _stc, boolean _isDelim, int _groupIndex)
    {
	cloneMinerExecuter.ExCloneMiner(container, _stc, _isDelim, _groupIndex, this);
    }
*/
    /*
     * public static void ExecuteCloneMiner() { try { String
     * cloneminerexecpath=Activator.getAbsolutePath(Directories.CLONE_MINER); String
     * cloneminerpath=Activator.getAbsolutePath(Directories.CLONE_MINER_DIR); final
     * String[] strArray = new String[4]; strArray[0] = cloneminerexecpath;
     * strArray[1] = "" + mintokensize; strArray[2] = "" + methodunique; strArray[3]
     * = "" + groupindex; File dir = new File(cloneminerpath); miner =
     * Runtime.getRuntime().exec(strArray, null, dir); errStream = new
     * ExternalThread(miner.getErrorStream()); outputStream = new
     * ExternalThread(miner.getInputStream()); errStream.start();
     * outputStream.start(); int result = miner.waitFor(); if(result != 0){
     * System.err.println("Clone Miner terminates with problems..."); } else{
     * errStream.join(); outputStream.join(); } }catch(Exception e) {
     * System.err.println(e.getMessage()); e.printStackTrace(); miner.destroy(); } }
     */

    public static void ExecuteART(String filename) // RUNS ART PROCESSOR FOR A SPECIFIC SPC FILE
    {
	try
	{
		
		flag=false;
		
		MessageConsole myConsole = new MessageConsole("My Console", null);
	    ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ myConsole });
	    ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
	    final MessageConsoleStream stream = myConsole.newMessageStream();
	    stream.setActivateOnWrite(true);
	    
	    String artexecpath =Directories.getAbsolutePath(Directories.ART_PATH);
	    String artpath = Directories.getAbsolutePath(Directories.ART_PATH_DIR);
	    final String[] strArray = new String[2];
	    strArray[0] = artexecpath;
	    strArray[1] = "" + filename;
	    File dir = new File(artpath);
	    
	    art = Runtime.getRuntime().exec(strArray, null, dir);
	    errStream = new BufferedReader(new 
			     InputStreamReader(art.getErrorStream()));
	    outputStream = new BufferedReader(new 
			     InputStreamReader(art.getInputStream()));
	   // errStream.start();
	   // outputStream.start();
	    
	    
	    
	    //Output on Primary Console "Standard Output"
	   
	    String s = null;
	    while ((s = outputStream.readLine()) != null) {
	        System.out.println(s); 
	        
	    }
	    
	    //Output on PlugIn Console
	    
	    s=null; 
	    while ((s = errStream.readLine()) != null) {
	        System.out.println(s);
	        if(s.contains("Error") || s.contains("error"))	
        	{
        	flag=true;
        	}
	        if(!(s.contentEquals("Picked up _JAVA_OPTIONS: -Xmx512M")))
	        stream.println(s);
	        
	    }
	    
	    int result = art.waitFor();
	    if (result != 0)
	    {
	    	System.err.println("ART PROCESSOR terminates with problems...");
	    } else
	    {
			errStream.close();
			outputStream.close();
	    }
	} catch (Exception e)
	{
		MessageConsole myConsole = new MessageConsole("My Console", null);
	    ConsolePlugin.getDefault().getConsoleManager().addConsoles(new IConsole[]{ myConsole });
	    ConsolePlugin.getDefault().getConsoleManager().showConsoleView(myConsole);
	    final MessageConsoleStream stream = myConsole.newMessageStream();
	    stream.setActivateOnWrite(true);
	    stream.println(e.getMessage());
		    try 
		    {
				stream.close();
			}
		    catch (IOException e1) 
		    {
				e1.printStackTrace();
			}
	    System.err.println(e.getMessage());
	    e.printStackTrace();
	    art.destroy();
	}
    }
    
    
    
    public static boolean errorFound()
    {	
    	return flag;	
    }
    
    

    /**
     * Set the value of CloneMiner Parameters
     */
 /*   public void setMinerSettings(int groupChoice, int groupSize, boolean methodAnalyze, int minScTok, int minFcTok,
	    int minFcTokPtg, int minMcTok, int minMcTokPtg)
    {
	minerSettings.setMinerSettings(groupChoice, groupSize, methodAnalyze, minScTok, minFcTok, minFcTokPtg, minMcTok,
		minMcTokPtg);
    }*/

   /* public void stop()
    {
	// needed when finishing up //
    }*/

}
