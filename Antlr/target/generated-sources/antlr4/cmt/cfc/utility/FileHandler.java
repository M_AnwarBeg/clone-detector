package cmt.cfc.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;

import cmt.common.Directories;





public class FileHandler
{

    public static File dir;
    String dirPath;
    public static FileHandler fileHandler;
    
    

    File[] listOfAllFiles;
    File[] listOfAllTempFiles;
    public ArrayList<String> listOfARTFiles;
    public ArrayList<String> artFilesPaths;
  public  ArrayList<String> listOfJAVAFiles;
  public ArrayList<String> javaFilesPaths;
  public   ArrayList<String> listOfTempARTFiles;
  public  ArrayList<String> tempARTFilesPath;
  public  ArrayList<String> listOfTempJavaFiles;
  public  ArrayList<String> tempJavaFilesPath;

    /**
     * Use a BufferedReader to read the file line-by-line using readLine(). Then
     * split each line on whitespace using String.split("\\s") and the size of the
     * resulting array is the total words count.
     */

    public FileHandler(String path)
    {

	listOfARTFiles = new ArrayList<String>();
	artFilesPaths = new ArrayList<String>();
	listOfJAVAFiles = new ArrayList<String>();
	javaFilesPaths = new ArrayList<String>();

	listOfTempJavaFiles = new ArrayList<String>();
	tempJavaFilesPath = new ArrayList<String>();
	listOfTempARTFiles = new ArrayList<String>();
	tempARTFilesPath = new ArrayList<String>();

	dirPath = Directories.getAbsolutePath("\\ART_Output\\"+ path + "\\");
	dir = new File(dirPath);

	getARTFilesList();
	getARTFilesPaths();
	getJavaFilesList();
	getJAVAFilesPaths();
	
	getTempARTFilesList();
	getTempARTFilesPaths();
	getTempJavaFilesList();
	getTempJAVAFilesPaths();
    }

    /*********** Gives names of all ART files in ART output folder ************/
    /*
     * public void getTempFilesList(){ listOfAllTempFiles = tempDir.listFiles(); for
     * (File file : listOfAllTempFiles) { if (file.isFile()) {
     * if(file.getName().toLowerCase().endsWith(".art")){
     * listOfTempARTFiles.add(file.getName()); } } } }
     */

    public void getARTFilesList()
    {
    	listOfARTFiles.clear();
	listOfAllFiles = dir.listFiles();

	for (File file : listOfAllFiles)
	{
	    if (file.isFile())
	    {
		if (file.getName().toLowerCase().endsWith(".art"))
		{
		    listOfARTFiles.add(file.getName());

		}
	    }
	}

    }
    /*********** Gives names of all Temporary VCL files in VCL output folder ************/
    public void getTempARTFilesList()
    {

	listOfAllTempFiles = dir.listFiles();

	for (File file : listOfAllTempFiles)
	{
	    if (file.isFile())
	    {
		if (file.getName().toLowerCase().endsWith(".art"))
		{
		    listOfTempARTFiles.add(file.getName());
		    //file.getName().

		}
	    }
	}

    }
    /*********** Gives names of all Temporary JAVA files in VCL output temp folder ************/
    public void getTempJavaFilesList()
    {

	listOfAllTempFiles = dir.listFiles();

	for (File file : listOfAllTempFiles)
	{
	    if (file.isFile())
	    {
		if (file.getName().toLowerCase().endsWith(".java"))
		{
		    listOfTempJavaFiles.add(file.getName());

		}
	    }
	}

    }
    
    /*********** Gives full path of all temporary VCL files ************/
    public void getTempARTFilesPaths()
    {
	for (int i = 0; i < listOfARTFiles.size(); i++)
	{
	    tempARTFilesPath.add(dirPath + "temp/" + listOfTempARTFiles.get(i));
	}

    }

    /********** get full path of temporary JAva files ************/
    public void getTempJAVAFilesPaths()
    {
	for (int i = 0; i < listOfTempJavaFiles.size(); i++)
	{
	    tempJavaFilesPath.add(dirPath + "\\" + listOfTempJavaFiles.get(i));
	}

    }

    public void getJavaFilesList()
    {
    	listOfJAVAFiles.clear();
	listOfAllFiles = dir.listFiles();

	for (File file : listOfAllFiles)
	{
	    if (file.isFile())
	    {
		if (file.getName().toLowerCase().endsWith(".java"))
		{
		    listOfJAVAFiles.add(file.getName());

		}
	    }
	}

    }

    /*********** Gives full path of all ART files ************/
    /*
     * public void getTempFilesPaths(){ for (int i=0; i<listOfTempARTFiles.size();
     * i++){ tempARTFilesPaths.add(tempDirPath+ "\\" +listOfTempARTFiles.get(i)); }
     * }
     */

    public void getARTFilesPaths()
    {
    	artFilesPaths.clear();
	for (int i = 0; i < listOfARTFiles.size(); i++)
	{
	    artFilesPaths.add(dirPath + "\\" + listOfARTFiles.get(i));
	}

    }

    /********** get full path of JAva files ************/
    public void getJAVAFilesPaths()
    {
	for (int i = 0; i < listOfJAVAFiles.size(); i++)
	{
	    javaFilesPaths.add(dirPath + "\\" + listOfJAVAFiles.get(i));
	}

    }
    /*********** Gives names of all temp VCL files in VCL output folder ************/
    
    public void getTempFilesList(){
  	  File file= new File(dir+"/temp/");
  	  listOfAllTempFiles = file.listFiles();
  	  for(File file1 : listOfAllTempFiles) {
  		  if (file1.isFile()) {
  			  if(file1.getName().toLowerCase().endsWith(".art")){
  				  listOfTempARTFiles.add(file1.getName()); 
  				  } 
  			  } 
  		  } 
  	  }

    /*********** Get variable names from ART files ************/
    public ArrayList<String> getVariableNames()
    {
	File file;
	ArrayList<String> variableNamesList = new ArrayList<String>();
	for (int i = 0; i < artFilesPaths.size(); i++)
	{
	    try
	    {
		file = new File(artFilesPaths.get(i));
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);

		String line = br.readLine();

		while (line != null)
		{

		    line = line.replaceAll("\\t", "");
		    String[] parts = line.split(" ");
		    for (int j = 0; j < parts.length; j++)
		    {
			if (parts[j].equals("#set"))
			{
			    if (!parts[j + 1].equalsIgnoreCase("filename"))
			    {
				variableNamesList.add(parts[j + 1]);
			    }
			}
		    }
		    line = br.readLine();
		}
		br.close();

	    } catch (FileNotFoundException e)
	    {
		e.printStackTrace();
	    } catch (IOException e)
	    {
		e.printStackTrace();
	    }
	}
	if (variableNamesList.isEmpty())
	{
	    variableNamesList.add(" ");
	}

	return variableNamesList;

    }

    /*********** Get ART file's code ************/
    public ArrayList<String> getFileContents(String fileName)
    {
	File file1 = new File(fileName);
	ArrayList<String> fileContents = new ArrayList<String>();
	try
	{
	    FileReader fr = new FileReader(file1);
	    BufferedReader br = new BufferedReader(fr);

	    String line = br.readLine();
	    while (line != null)
	    {
		fileContents.add(line);
		line = br.readLine();
	    }
	    br.close();
	} catch (FileNotFoundException e)
	{
	    e.printStackTrace();
	} catch (IOException e)
	{
	    e.printStackTrace();
	}
	return fileContents;
    }

    /*********** Updates ART file's code ************/

    public void updateFileContents(FileHandler fileHandler, ArrayList<StyledText> textARTCodes)
    {
	int size = fileHandler.listOfARTFiles.size();
	for (int i = 0; i < size; i++)
	{
	    FileWriter writer;
	    try
	    {
		writer = new FileWriter(fileHandler.artFilesPaths.get(i));
		writer.write(textARTCodes.get(i).getText());

		writer.write('\n');

		writer.close();
	    } catch (IOException e1)
	    {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	    }

	}

    }

    public void updateCode2(int size, FileHandler fileHandler, ArrayList<StyledText> textARTCodes)
    {
	// TODO Auto-generated method stub
	for (int i = 0; i < size - 1; i++)
	{
	    FileWriter writer;
	    try
	    {
		writer = new FileWriter(fileHandler.artFilesPaths.get(i));
		writer.write(textARTCodes.get(i).getText());

		writer.write('\n');

		writer.close();
	    } catch (IOException e1)
	    {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	    }

	}

    }
    public void createTempFile(FileHandler fileHandler, ArrayList<StyledText> textVCLCodes )
    {
    	
    	InputStream inStream = null;
    	OutputStream outStream = null;
    	int size = fileHandler.listOfARTFiles.size();
		new File(dir + "\\temp\\").mkdir();
    	for (int i = 0; i < size; i++)
    	{	
        	try{

        	    File afile =new File(artFilesPaths.get(i));
        	    File bfile =new File(dir+"\\temp\\"+listOfARTFiles.get(i));
        	    

        	    inStream = new FileInputStream(afile);
        	    outStream = new FileOutputStream(bfile);

        	    byte[] buffer = new byte[1024];

        	    int length;
        	    //copy the file content in bytes
        	    while ((length = inStream.read(buffer)) > 0){

        	    	outStream.write(buffer, 0, length);

        	    }

        	    inStream.close();
        	    outStream.close();

        	    //delete the original file
        	    //afile.delete();

        	    System.out.println("File is copied successful!");

        	}
        	catch(IOException e){
        	    e.printStackTrace();
        	}
        	
    	}
    	for (int i = 0; i < size; i++)
    	{
    	    FileWriter writer;
    	    try
    	    {
    		writer = new FileWriter(fileHandler.dir+"\\temp\\"+listOfARTFiles.get(i));
    		writer.write(textVCLCodes.get(i).getText());

    		writer.write('\n');

    		writer.close();
    	    } 
    	    catch (IOException e1)
    	    {
    		// TODO Auto-generated catch block
    		e1.printStackTrace();
    	    }

    	}
    /*	new ArrayList<TabItem>();
    	new ArrayList<StyledText>();
    	ExternalThread errStream;
    	ExternalThread outputStream;
    	Process vcl;
    	fileHandler = new FileHandler(mccId);
    	fileHandler.listOfVCLFiles.size();

    	// String filename = "\\vcl_output\\MCC_Template\\MCC_" + mccId + "\\"
    	// + mccId + "_MCC_SPC.art";
    	String vclexecpath = "\\vcl_output\\VCL_1.1.4.bat";
    	String vclpath = dir+"\\temp\\";
    	// System.out.println(filename);

    	// filename = CloneManagementPlugin.getAbsolutePath(filename);
    	vclexecpath = CloneManagementPlugin.getAbsolutePath(vclexecpath);
    	//vclpath = CloneManagementPlugin.getAbsolutePath(vclpath);
    	for (int i = 0; i < size; i++)
    	{
    	    try
    	    {
    		final String[] strArray = new String[2];
    		strArray[0] = vclexecpath;
    		strArray[1] = dir+"\\temp\\"+ vclFilesPaths.get(i);
    		System.out.println();
    		System.out.println("filename : "+dir+"\\temp\\" + vclFilesPaths.get(i));

    		File dir = new File(vclpath);
    		vcl = Runtime.getRuntime().exec(strArray, null, dir);
    		errStream = new ExternalThread(vcl.getErrorStream());
    		outputStream = new ExternalThread(vcl.getInputStream());
    		errStream.start();
    		outputStream.start();
    		int result = vcl.waitFor();
    		if (result != 0)
    		{
    		    System.err.println("VCL PROCESSOR terminates with problems...");
    		} else
    		{
    		    errStream.join();
    		    outputStream.join();
    		}

    	    } catch (Exception e)
    	    {
    		System.err.println(e.getMessage());
    		e.printStackTrace();
    		// vcl.destroy();
    	    }
    	}*/


    	
	
    
	
    }
 /*********** Updates VCL file's code ************/
    
    
    public void updateTempFileContents(FileHandler fileHandler, ArrayList<StyledText> textVCLCodes)
    {
	int size = fileHandler.listOfTempARTFiles.size();
	for (int i = 0; i < size; i++)
	{
	    FileWriter writer;
	    try
	    {
		writer = new FileWriter(fileHandler.tempARTFilesPath.get(i));
		writer.write(textVCLCodes.get(i).getText());

		writer.write('\n');

		writer.close();
	    } catch (IOException e1)
	    {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	    }

	}

    }
    public boolean moveTempFiles(FileHandler fileHandler) {
    	/*File file= new File(dir+"/temp/");
		 listOfAllTempFiles = file.listFiles();
		 for(File file1 : listOfAllTempFiles) {
			 if (file1.isFile()) {
				  if(file1.renameTo(new File(dir + file1.getName()))){
			    		System.out.println("File is moved successful!");
			    	   }
				  else{
			    		System.out.println("File is failed to move!");
			    	   } 
				  } 
			  }
    	return true;*/
    	InputStream inStream = null;
    	OutputStream outStream = null;
    	File file= new File(dir+"/temp/");
		 listOfAllTempFiles = file.listFiles();
    	
    	for (File file1 : listOfAllTempFiles)
    	{	
        	try{
        		
        	    File afile =new File(dir+"\\temp\\"+file1.getName());
        	    File bfile =new File(dir+"\\"+file1.getName());
        	    

        	    inStream = new FileInputStream(afile);
        	    outStream = new FileOutputStream(bfile);

        	    byte[] buffer = new byte[1024];

        	    int length;
        	    //copy the file content in bytes
        	    while ((length = inStream.read(buffer)) > 0){

        	    	outStream.write(buffer, 0, length);

        	    }

        	    inStream.close();
        	    outStream.close();

        	    //delete the original file
        	    afile.delete();

        	    System.out.println("File is copied successful!");

        	}
        	catch(IOException e){
        	    e.printStackTrace();
        	}
        	
        	
        	
    	}
    	file.delete();
    	return true;

    
    }
    

   /* public void createUpdatedTempJavaFiles(int mccId)
    {
//    	MessageBox errorBox = new MessageBox( new Shell(), SWT.CANCEL | SWT.OK);
//    	errorBox.setText("Compilation Status");
	new ArrayList<TabItem>();
	new ArrayList<StyledText>();
	ExternalThread errStream;
	ExternalThread outputStream;
	Process vcl;
	FileHandler fileHandler = new FileHandler(mccId);
	//fileHandler.listOfVCLFiles.size();
	fileHandler.listOfTempARTFiles.size();
	ExternalThread.errorPrompt  = "";
	// String filename = "\\vcl_output\\MCC_Template\\MCC_" + mccId + "\\"
	// + mccId + "_MCC_SPC.art";
	String vclexecpath = "\\art_output\\art_1.1.4.bat";
	String vclpath = "\\art_output";
	// System.out.println(filename);

	// filename = CloneManagementPlugin.getAbsolutePath(filename);
	vclexecpath = Directories.getAbsolutePath(vclexecpath);
	vclpath = Directories.getAbsolutePath(vclpath);
	//for (int i = 0; i < listOfVCLFiles.size(); i++)
	for (int i = 0; i < listOfTempARTFiles.size(); i++)	
	{
	    try
	    {
		final String[] strArray = new String[2];
		strArray[0] = vclexecpath;
		strArray[1] = "" + tempARTFilesPath.get(0);
		System.out.println();
		System.out.println("filename : " + tempARTFilesPath.get(0));

		File dir = new File(vclpath);
		vcl = Runtime.getRuntime().exec(strArray, null, dir);
		errStream = new ExternalThread(vcl.getErrorStream());
		outputStream = new ExternalThread(vcl.getInputStream());
		errStream.start();
		outputStream.start();
		int result = vcl.waitFor();
		if (result != 0)
		{
		    System.err.println("ART PROCESSOR terminates with problems...");
		} else
		{
		    errStream.join();
		    outputStream.join();
		}
		if(ExternalThread.errorPrompt != "" && ExternalThread.errorPrompt.contains("Error"))
		 {
			 //if there is some string of error in this then show & don't process further ... else clear error prompt that no irrelevant things are shown
				//errorBox.setMessage(ExternalThread.errorPrompt );
				//errorBox.open(); 
				System.err.println("Error detected before join" + ExternalThread.errorPrompt );
				//break;
		 }
		 else
		 {
			 ExternalThread.errorPrompt = "";
		 }
		

	    } catch (Exception e)
	    {
		System.err.println(e.getMessage());
		e.printStackTrace();
		// vcl.destroy();
	    }
	}
	 
    }*/

    /*public static void runFileHandler(int mccId)
    {
	  fileHandler = new FileHandler(mccId);
    }*/
    public ArrayList<String> getListOfARTFiles() {
    	return listOfARTFiles;
    }
    
   public ArrayList<String> getJavaFilesPaths(){
	   return javaFilesPaths;
	   
   }
   
   public ArrayList<String> getlistOfJAVAFiles(){
	   return listOfJAVAFiles;
	   
   }
   
  
    
}
