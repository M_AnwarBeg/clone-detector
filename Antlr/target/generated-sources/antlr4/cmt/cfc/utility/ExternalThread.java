package cmt.cfc.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.eclipse.ui.console.MessageConsoleStream;


public class ExternalThread extends Thread
{

    private InputStream inputStream;
    public static String errorPrompt;
    private MessageConsoleStream stream;
    
    public ExternalThread(InputStream is,MessageConsoleStream s)
    {
	inputStream = is;
	stream=s;
    }

    @Override
    public void run()
    {
	try
	{
	    InputStreamReader streamReader = new InputStreamReader(inputStream);
	    BufferedReader bufferReader = new BufferedReader(streamReader);
	    String line = null;
	    //errorPrompt = "";
	    while ((line = bufferReader.readLine()) != null)
	    {
	    	errorPrompt = errorPrompt + line + "\n";
	    	System.out.println(line);
	    	if(line.contains("Error"))
	    		stream.println(line);
	    }
    	//System.out.println("showing error prompt in external thread.java" + errorPrompt);
	} catch (IOException ex)
	{

	}
    }

}