package cmt.cfc.sccframe;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TableItem;

import cmt.cddc.CloneRepository.CloneRepository;
import cmt.cddc.langtokenizer.InitJava;
import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.simpleclones.SimpleCloneInstance;
import cmt.cddc.simpleclones.Tokens;
import cmt.cddc.simpleclones.TokensList;
import cmt.cfc.utility.Initializer;
import cmt.common.Directories;
import cmt.cfc.mccframe.BreaksUI;
import cmt.cfc.mccframe.MCCTemplate;
import cmt.cfc.mccframe.MCSMappingUI;
import cmt.cfc.mccframe.NamingUI;
import cmt.cfc.mccframe.RenamingWizard;
import cmt.cfc.mccframe.WizardPage1;
import cmt.cfc.mccframe.WizardPage2;
public class SCCTemplateNew
{
	
    String OUTPUT_PATH = "\\ART_Output\\";

    String OUTPUT_PATH_Temp = "\\ART_Output\\";

    public int curSCCID = -1;
    int curFileID = 0;
    int curClassID = 0;
    int curMethodID = 0;
    int curVariableID = 0;
    int MCC_Type = 0;
    int toOutput = 1;
    ArrayList<ArrayList<String>> InstanceList;
    ArrayList<ArrayList<String>> FormatInstances;
    ArrayList<String> GenericTokens;
    public ArrayList<DynamicToken> DynamicTokens;
    public ArrayList<DynamicToken> DynamicTk;
    public static ArrayList<String> varNames = new ArrayList<String>();
    ArrayList<DynamicToken> tokens = new ArrayList<DynamicToken>();
    public FramingStructure Fr= new FramingStructure();
    public int num=0;
    public SCCTemplateNew(int SCCID, SimpleClone SimpleClone, ArrayList<Integer> MCC_Settings, int mccId,FramingStructure Frame)
    {
	InstanceList = new ArrayList<ArrayList<String>>();
	FormatInstances = new ArrayList<ArrayList<String>>();
	GenericTokens = new ArrayList<String>();
	DynamicTokens = new ArrayList<DynamicToken>();
	DynamicTk = new ArrayList<DynamicToken>();
	curSCCID = SCCID;
	curFileID = mccId;
	
	//TokenizeSCC(SimpleClone);
	// Populate MCC_Settings Parameters //
	setMCCParameters(MCC_Settings);

	populateInstanceList(SimpleClone);
	for (int index = 0; index < InstanceList.size(); index++)
	{
	    //formatTokens(SimpleClone.getSCCInstances(), index);
	}
	//ubaid
	DynamicTokens=Frame.getVariables();
	
	
	
	//matchTokens(SimpleClone.getSCCInstances());
	
	
	
	
	
	generateTemplate(Frame);
	// generateTemplateTemp();
    }

    private void setMCCParameters(ArrayList<Integer> MCC_Settings)
    {
	if (MCC_Settings.size() > 0)
	{
	    MCC_Type = MCC_Settings.get(0);
	    toOutput = MCC_Settings.get(1);
	    if (MCC_Type == 1)
	    {
		//OUTPUT_PATH += "MCC_Template\\";
		//OUTPUT_PATH_Temp += "MCC_Template_Temp\\";
	    }
	}
    }

    public void formatTokens(ArrayList<SimpleCloneInstance> _InstanceList, int instanceIndex)
    {
	// System.out.println("About to format code segment for Instance #" +
	// instanceIndex);
	SimpleCloneInstance Inst = _InstanceList.get(instanceIndex);
	String codeSegment = Inst.getCodeSegment();
	ArrayList<String> tokenList = InstanceList.get(instanceIndex);
	ArrayList<String> FormattedList = new ArrayList<String>();
	int startIndex, endIndex, tokenLen, posFrom = 0;
	int indexOf = -1;
	// System.out.print(codeSegment);

	// Match intermediate tokens //
	for (int i = 0; i < tokenList.size(); i++)
	{
	    // Initialize tokens //
	    if (i + 1 >= tokenList.size())
	    {
		break;
	    }

	    String token1 = tokenList.get(i);
	    String token2 = tokenList.get(i + 1);
	    startIndex = 0;
	    endIndex = 0;
	    indexOf = -1;
	    tokenLen = 0;

	    // Convert ART keywords into original characters of the project language
	    // for matching intermediate tokens by removing '\\'
	    if (token1 == "\\?")
	    {
		token1 = "?";
	    } else if (token1 == "\\#")
	    {
		token1 = "#";
	    }

	    if (token2 == "\\?")
	    {
		token2 = "?";
	    } else if (token2 == "\\#")
	    {
		token2 = "#";
	    }

	    // Find ending index of first token //
	    tokenLen = token1.length();
	    indexOf = codeSegment.indexOf(token1, posFrom);
	    // System.out.println("First index(trying to find: '" + token1 + "') " +
	    // indexOf);

	    if (indexOf != -1)
	    {
		endIndex = indexOf + tokenLen;
		// posFrom = endIndex - 1;
		posFrom = endIndex;
	    } else
	    {
		System.out.println("I could not find the index of token (NEW) : " + token1);
	    }

	    // Find starting index of second token //
	    tokenLen = token2.length();
	    indexOf = codeSegment.indexOf(token2, posFrom);
	    // System.out.println("Second index(trying to find: '" + token2 + "') " +
	    // indexOf);

	    if (indexOf != -1)
	    {
		startIndex = indexOf;
	    } else
	    {
		System.out.println("I could not find the index of token (NEW):  " + token2);
	    }

	    // Store intermediary information //
	    if (endIndex <= startIndex && endIndex >= 0 && startIndex >= 0)
	    {
		String tempInfo = codeSegment.substring(endIndex, startIndex);
		FormattedList.add(tempInfo);
		// System.out.println("The intermediary information between '" + token1 + "' and
		// '" + token2 + "' is: ");
		// System.out.println("==|'" + tempInfo + "'|==");
	    } else
	    {
		System.out.println("Out of bound errors in SCC_Template_NEW");
	    }
	}
	FormattedList.add(""); // To handle some out-of-bounds exceptions

	// Formatting Saved in FormattedList //
	System.out.println("Formatting Saved For Instance #: " + instanceIndex + "!\n====================");
	FormatInstances.add(FormattedList);

    }

    public void matchTokens(ArrayList<SimpleCloneInstance> _InstanceList)
    {
	boolean firstPass = true;
	int tokenIndex = 0;

	// For all the instances of curSCCID //
	// System.out.println("Size: " + InstanceList.size());
	for (int i = 0; i < InstanceList.size(); i++)
	{
	    // For the first pass, add all tokens to GenericTokens //
	    if (firstPass)
	    {
		int checkC = 0; // for looping over formatted list

		for (String InstanceToken : InstanceList.get(i))
		{
		    GenericTokens.add(InstanceToken);
		    if (checkC < FormatInstances.get(0).size())
		    {
			if (FormatInstances.get(0).get(checkC) != "")
			{
			    GenericTokens.add("9999c" + checkC); // for the tokens of comments
			    checkC++;
			}
		    }
		}
		firstPass = false;
	    }
	    else
	    {
		boolean strFlag = false;
		tokenIndex = 0;
		for (int j = 0; j < InstanceList.get(i).size() && tokenIndex<GenericTokens.size(); j++)
		{
		    String InstanceToken = InstanceList.get(i).get(j);

		    if (!GenericTokens.get(tokenIndex).equals(InstanceToken)) // If tokens differ
		    {
			if (GenericTokens.get(tokenIndex).equals("9999")) // If you find 9999 at the index
			{
			    // Ignore, All Instances already populated //
			} 
			else if (GenericTokens.get(tokenIndex).contains("9999c"))
			{
			    // Dynamic tokens for comments
			    DynamicToken tk = new DynamicToken();
			    tk.tokenIndex = tokenIndex;

			    String cTokens[] = GenericTokens.get(tokenIndex).split("c");
			    int c = Integer.parseInt(cTokens[1]);
			    for (int instIndex = 0; instIndex < InstanceList.size(); instIndex++)
			    {
				String value = FormatInstances.get(instIndex).get(c);
				if (value.contains("\n"))
				{
				    value = value.replaceAll("\n", "");
				    tk.cNxtLine = true;
				}
				tk.tokenValues.add(value);
				tk.fid.add(_InstanceList.get(instIndex).getFileId());
			    }
			    
			    
//			    DynamicTokens.add(tk);
//			    DynamicTk.add(tk);
			    
			    
			    j--;

			} else
			{
			    // Once a dynamic token is detected //
			    DynamicToken tk = new DynamicToken();
			    tk.tokenIndex = tokenIndex;
			    for (int instIndex = 0; instIndex < InstanceList.size(); instIndex++)
			    {
				if (InstanceList.get(instIndex).get(j).contains("\""))
				{

				    // InstanceList.get(instIndex).set(j,
				    // InstanceList.get(instIndex).get(j).replace("\"", ""));
				    tk.cDoubleComma = true;
				    GenericTokens.set(tokenIndex, "9999str");
				    strFlag = true;
				}
				tk.tokenValues.add(InstanceList.get(instIndex).get(j));
				tk.fid.add(_InstanceList.get(instIndex).getFileId());
			    }
			    //DynamicTokens.add(tk);
			    if (!strFlag)
			    {
				GenericTokens.set(tokenIndex, "9999");
			    } else
			    {
				strFlag = false;
			    }
			}
		    }
		    
		    tokenIndex++;
		}
	    }
	}
	
	// code for removing blank or null variables (Saud) //
	for(int t=0; t<DynamicTokens.size(); t++)
	{
		boolean check = true;
		boolean spaceCheck = true;
		String value = DynamicTokens.get(t).tokenValues.get(0);
		
		// uncomment this if there is an error related to variable/dynamic tokens
		for(int u=0; u<DynamicTokens.get(t).tokenValues.size(); u++)
		{
			if(!(DynamicTokens.get(t).tokenValues.get(u).matches("[ ]+")))
			{
				spaceCheck = false;
			}
		}
		
		if(spaceCheck)
		{
			DynamicTokens.get(t).ignore = true;
		}
		
		for(int u=1; u<DynamicTokens.get(t).tokenValues.size(); u++)
		{
			if(!(DynamicTokens.get(t).tokenValues.get(u).equals(value)))
			{
				check = false;
			}
		}
		
		if(check)
		{
			DynamicTokens.get(t).ignore = true;
		}
	}

	// Set placeholder //
	int varCounter = 0;
	for (int index = 0; index < DynamicTokens.size(); index++)
	{
	    DynamicToken tk = DynamicTokens.get(index);
	    if (tk.marked)
	    {
		/*
		 * if(GenericTokens.get(tk.tokenIndex).equals("9999str")) {
		 * GenericTokens.set(tk.tokenIndex, "\\\"" + "?@" + tk.varName + "?" +"\\\"");
		 * continue; } else
		 */
		{
		    GenericTokens.set(tk.tokenIndex, "?@" + tk.varName + "?");
		    continue;
		}
	    }
//	    String token = tk.getValues().get(0);
//	    tk.varName = "SCC" + curSCCID + "V" + varCounter;

	    for (int j = 0; j < DynamicTokens.size(); j++)
	    {
		if (checkForMarked(j,index) && index != j)
		{
		    if (DynamicTokens.get(j).marked == false)
		    {
			DynamicTokens.get(j).varName = tk.varName;
			DynamicTokens.get(j).marked = true;
			
		    }
		}
	    }
	    /*
	     * if(GenericTokens.get(tk.tokenIndex).equals("9999str")) {
	     * GenericTokens.set(tk.tokenIndex, "\\\"" + "?@" + tk.varName + "?" +"\\\""); }
	     * else
	     */
	    {
		GenericTokens.set(tk.tokenIndex, "?@" + tk.varName + "?");
	    }
	    varCounter++;
	}
	

	 //Variable Naming UI (Ubaid)
//	   try
//			{
//		   
//		  
//				
//		  
//		   
//		   
//				Shell shell = new Shell();	
//				NamingUI rename_UI = new NamingUI(shell,0);
//				rename_UI.renameVar(DynamicTokens);
//				rename_UI.open();
//		  // renameVar(DynamicTokens);
//				for(int u=0; u<NamingUI.varNewNames.size(); u++)
//				{
//					if(NamingUI.varNewNames.get(u) != null && NamingUI.varNewNames.get(u) != "`")
//					{
//						for(int y=0; y<DynamicTokens.size(); y++)
//						{
//							if(DynamicTokens.get(y).varName == NamingUI.varNames.get(u))
//							{
//								DynamicTokens.get(y).varName = NamingUI.varNewNames.get(u);
//								DynamicTokens.get(y).oldName = NamingUI.varNames.get(u);
//							}
//						}
//					}
//				}
//			} 
//	   catch (Exception e)
//			{
//			    e.printStackTrace();
//			    System.err.println(e.getMessage());
//			}
//	
	
	   
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	for (int i = 0; i < GenericTokens.size(); i++)
	{
	    if (GenericTokens.get(i).contains("9999c"))
	    {
		GenericTokens.set(i, " ");
	    }
	}
    }

    private boolean checkForMarked(int j,int index)
    {
    	for(int inst=0;inst<InstanceList.size();inst++)
    	{
    		if(!(DynamicTokens.get(j).getValues().get(inst).equals(DynamicTokens.get(index).getValues().get(inst))))
    		{
    			return false;
    		}
    	}
    	return true;
    }
     
    public  ArrayList<DynamicToken> getDt()
    {
    	return DynamicTokens;
    	
    	
    	
    }
    public void checkIfExists(String InstanceToken, int tokenIndex)
    {
	for (DynamicToken tk : DynamicTokens)
	{
	    if (tk.tokenIndex == tokenIndex)
	    {
		tk.tokenValues.add(InstanceToken);
		return;
	    }
	}
    }

    public void populateInstanceList(SimpleClone simpleClone)
    {
	InstanceList.clear();
	ArrayList<SimpleCloneInstance> AllInstances = simpleClone.getSCCInstances();

	for (SimpleCloneInstance Instance : AllInstances)
	{
	    InstanceList.add(Instance.getTokens());
	}
    }

    public void generateTemplateTemp()
    {

	String path = Directories.getAbsolutePath(OUTPUT_PATH_Temp);
	ArrayList<String> BaseFormat = FormatInstances.get(0);
	try
	{
	    String SPC_PATH = curSCCID + "_SCC_SPC.art";
	    String ART_PATH = curSCCID + "_SCC_Frame.art";

	    refresh(path + SPC_PATH, path + ART_PATH); // Delete files if they already exist //

	    FileWriter art_file = new FileWriter(path + ART_PATH, true);
	    BufferedWriter output = new BufferedWriter(art_file);

	    /********* Commented to remove extra empty SCC_SPC.art files *********/
	    // FileWriter spc_file = new FileWriter(path + SPC_PATH, true);
	    // BufferedWriter spc_output = new BufferedWriter(spc_file);

	    // Read VariablesList //
	    curVariableID = 0;
	    ArrayList<String> VariableList = new ArrayList<String>();
	    for (int i = 0; i < DynamicTokens.size(); i++)
	    {
		VariableList.add("variable_" + curVariableID);
		curVariableID++;
	    }

	    /********* Commented to remove extra empty SCC_SPC.art files *********/
	    /*
	     * // ====== START OF SPC ===== // // Specify SPC details // if(MCC_Type != 1) {
	     * //spc_output.write("% SPC file for " + ART_PATH + "\n\n"); // The output
	     * filename + directory // String fileName = "fileName"; String fileNameValue =
	     * "SCCID_" + curSCCID + "_Inst"; // Can ask user to specify method name //
	     * String dirName = "."; //spc_output.write("#set " + fileName + " = "); for(int
	     * _index = 0; _index < InstanceList.size(); _index++) { spc_output.write("\"" +
	     * fileNameValue + "_" + _index + "\""); if(_index != InstanceList.size()-1)
	     * spc_output.write(", "); else spc_output.write("\n"); } // Set Variables //
	     * spc_output.
	     * write("% Here I will set the rest of the place-holder variables\n\n"); int
	     * index = 0; for(DynamicToken tk : DynamicTokens) { if(tk.marked) continue;
	     * spc_output.write("#set " + tk.varName + " = "); for(int _index = 0; _index <
	     * InstanceList.size(); _index++) { spc_output.write("\"" +
	     * tk.tokenValues.get(_index) + "\""); if(_index != InstanceList.size()-1)
	     * spc_output.write(", "); else spc_output.write("\n"); } } // In While Loop //
	     * spc_output.write("\n#while "); for(DynamicToken tk : DynamicTokens) {
	     * if(tk.marked) continue; spc_output.write(tk.varName + ", "); }
	     * spc_output.write("fileName\n"); // Adapt ART File //
	     * spc_output.write("#adapt: " + "\"" + ART_PATH + "\"" + "\n\n"); // Details
	     * about SPC here //
	     * spc_output.write("% You can specify options for this SPC File here\n\n");
	     * spc_output.write("#endadapt\n"); spc_output.write("#endwhile"); } // ======
	     * END OF SPC ======= // // ========================= //
	     */
	    // ====== START OF ART ===== //

	    // Specify ART output //

	    int formatCounter = 0;
	    for (String token : GenericTokens)
	    {
		output.write(token);
		if (formatCounter < BaseFormat.size() - 1)
		{
		    output.write(BaseFormat.get(formatCounter));
		} else
		{
		    System.out.println("Formatting does not exist (out of bounds)");
		}
		formatCounter++;
	    }

	    if (MCC_Type == 1)
	    {
		output.write("#break break_" + curSCCID);
	    }

	    output.flush();
	    output.close();

	    /********* Commeneted to remove extra empty SCC_SPC.art files *********/
	    // spc_output.flush();
	    // spc_output.close();

	    /*
	     * File f = new File(SPC_PATH); if(f.exists())
	     * System.out.println("SPC EXISTED!"); if(f.delete())
	     * System.out.println("SPC DELETE!");
	     */
	    curClassID++;
	    curMethodID++;
	    /*
	     * if(MCC_Type != 1) RunART(SPC_PATH); // Run ART Processor
	     */
	} catch (IOException e)
	{
	    e.printStackTrace();
	}

	// ====== END OF ART ===== //

    }

    public void generateTemplate(FramingStructure Frame)
    {
	String path = Directories.getAbsolutePath(OUTPUT_PATH);
	//FormatInstances.get(0);
	try
	{
		CloneRepository.getFrameId();
		CloneRepository.openConnection();
		Statement stmt = CloneRepository.conn.createStatement();
		stmt.execute("use framerepository;");
		ResultSet rs = stmt.executeQuery("select fid from sccframes where sccid="+curSCCID);
		
		if(rs.next())
		{
			int fid=rs.getInt(1);
			ResultSet rs2 = stmt.executeQuery("select links from frames where fid="+fid);
			rs2.next();
			int links=rs2.getInt(1);
			links++;
			stmt.execute("update frames set links="+links+" where fid="+fid);
		}
		else
		{
			int id=CloneRepository.getFrameId();
				id++;
				stmt.execute("insert into sccframes(sccid,fid) values("+curSCCID+","+id+")");
				stmt.execute("insert into frames (path,level,links,issynced) values (\""+Directories.getAbsolutePath(path+"\\SCC_Frames\\" + curSCCID + "_SCC_Frame.art")+"\",1,1,1"+")");
		}
	
	    //new File(path + "\\MCC_" + curFileID + "\\").mkdir();
	    String SPC_PATH = "\\SCC_Frames\\" + curSCCID + "_SCC_SPC.art";
	    String ART_PATH = "\\SCC_Frames\\" + curSCCID + "_SCC_Frame.art";

	    refresh(path + SPC_PATH, path + ART_PATH); // Delete files if they already exist //

	    FileWriter art_file = new FileWriter(path + ART_PATH, true);
	    BufferedWriter output = new BufferedWriter(art_file);

	    /********* Commented to remove extra empty SCC_SPC.art files *********/
	    // FileWriter spc_file = new FileWriter(path + SPC_PATH, true);
	    // BufferedWriter spc_output = new BufferedWriter(spc_file);
	    //Ubaid 19July
	    GenericTokens=Frame.getGenTokens();
	    // Read VariablesList //
	    curVariableID = 0;
	    ArrayList<String> VariableList = new ArrayList<String>();
	    for (int i = 0; i < DynamicTokens.size(); i++)
	    {
	    		VariableList.add("variable_" + curVariableID);
	    		curVariableID++;
	    		//saud
	    		for(int y=0; y<GenericTokens.size(); y++)
	    		{
	    			if((GenericTokens.get(y).contains(DynamicTokens.get(i).varName)) && (DynamicTokens.get(i).ignore))
	    			{
	    				if(DynamicTokens.get(i).cNxtLine)
	    				{
	    					GenericTokens.set(y, DynamicTokens.get(i).tokenValues.get(0)+"\\n");
	    				}
	    				else
	    				{
	    					GenericTokens.set(y, DynamicTokens.get(i).tokenValues.get(0));
	    				}
	    			}
//	    			else if(DynamicTokens.get(i).oldName != "`" && (GenericTokens.get(y).contains(DynamicTokens.get(i).oldName)) && (DynamicTokens.get(i).ignore))
//	    			{
//	    				if(DynamicTokens.get(i).cNxtLine)
//	    				{
//	    					GenericTokens.set(y, DynamicTokens.get(i).tokenValues.get(0)+"\\n");
//	    				}
//	    				else
//	    				{
//	    					GenericTokens.set(y, DynamicTokens.get(i).tokenValues.get(0));
//	    				}
//	    			}
	    		}
	    }

	    /********* Commented to remove extra empty SCC_SPC.art files *********/
	    /*
	     * // ====== START OF SPC ===== // // Specify SPC details // if(MCC_Type != 1) {
	     * //spc_output.write("% SPC file for " + ART_PATH + "\n\n"); // The output
	     * filename + directory // String fileName = "fileName"; String fileNameValue =
	     * "SCCID_" + curSCCID + "_Inst"; // Can ask user to specify method name //
	     * String dirName = "."; //spc_output.write("#set " + fileName + " = "); for(int
	     * _index = 0; _index < InstanceList.size(); _index++) { spc_output.write("\"" +
	     * fileNameValue + "_" + _index + "\""); if(_index != InstanceList.size()-1)
	     * spc_output.write(", "); else spc_output.write("\n"); } // Set Variables //
	     * spc_output.
	     * write("% Here I will set the rest of the place-holder variables\n\n"); int
	     * index = 0; for(DynamicToken tk : DynamicTokens) { if(tk.marked) continue;
	     * spc_output.write("#set " + tk.varName + " = "); for(int _index = 0; _index <
	     * InstanceList.size(); _index++) { spc_output.write("\"" +
	     * tk.tokenValues.get(_index) + "\""); if(_index != InstanceList.size()-1)
	     * spc_output.write(", "); else spc_output.write("\n"); } } // In While Loop //
	     * spc_output.write("\n#while "); for(DynamicToken tk : DynamicTokens) {
	     * if(tk.marked) continue; spc_output.write(tk.varName + ", "); }
	     * spc_output.write("fileName\n"); // Adapt ART File //
	     * spc_output.write("#adapt: " + "\"" + ART_PATH + "\"" + "\n\n"); // Details
	     * about SPC here //
	     * spc_output.write("% You can specify options for this SPC File here\n\n");
	     * spc_output.write("#endadapt\n"); spc_output.write("#endwhile"); } // ======
	     * END OF SPC ======= // // ========================= //
	     */
	    // ====== START OF ART ===== //

	    // Specify ART output //

	    for (String token : GenericTokens)
	    {
			/*
			 * if(token.contains("?@SC")) for(int i=0;i<DynamicTokens.size();i++) {
			 * if(token.contains(DynamicTokens.get(i).varName) &&
			 * (DynamicTokens.get(i).cDoubleComma==true)) { output.write("\""); } }
			 */
		    	boolean nextLine = false;
			if (token.contains("%"))
			{
			    token = token.replace("%", "\\%\n");
			}
			if(token.contains("\\n"))
			{
				token=token.replace("\\n", "");
				nextLine = true;
			}
			output.write(token);
			if (token.contains("?@SC") || token.startsWith("?@"))
			{
			    for (int i = 0; i < DynamicTokens.size(); i++)
			    {
					if (token.contains(DynamicTokens.get(i).varName) && (DynamicTokens.get(i).cNxtLine == true))
					{
					    output.write("\n");
					}
			    }
			    /*
			     * if(formatCounter < BaseFormat.size()-1)
			     * output.write(BaseFormat.get(formatCounter)); else
			     * System.out.println("Formatting does not exist (out of bounds)");
			     * formatCounter++;
			     */
			}
			else if(nextLine)
			{
				output.write("\n");
			}
	    }

	    if (MCC_Type == 1)
	    {	//Ubaid 
	    	
	    	output.write("#break " + Frame.breakName);
//	    	for(int i=0; i<Frame.size(); i++)
//	    	{
//	    		if(BreaksUI.breakNames.get(i).getSCCId() == curSCCID)
//	    		{
//	    			output.write("#break " + BreaksUI.breakNames.get(i).getName());
//	    		}
//	    	}
	    }

	    output.flush();
	    output.close();

	    /********* Commented to remove extra empty SCC_SPC.art files *********/
	    // spc_output.flush();
	    // spc_output.close();

	    /*
	     * File f = new File(SPC_PATH); if(f.exists())
	     * System.out.println("SPC EXISTED!"); if(f.delete())
	     * System.out.println("SPC DELETE!");
	     */
	    curClassID++;
	    curMethodID++;
	    /*
	     * if(MCC_Type != 1) RunART(SPC_PATH); // Run ART Processor
	     */
	} catch (Exception e)
	{
	    e.printStackTrace();
	}

	// ====== END OF ART ===== //
     }

    public void RunART(String filename)
    {
	Initializer.ExecuteART(filename);
    }

    
    public static void TokenizeSCC(SimpleClone simpleClone) {		
  		ArrayList<SimpleCloneInstance> AllInstances = simpleClone.getSCCInstances();
  		InitJava javaTokenizer = new InitJava();
  		for (SimpleCloneInstance Instance : AllInstances)
  		{
  			int startCol = Instance.getStartCol();
  			int endCol = Instance.getEndCol();
  			int shiftStartingLine = Instance.getStartingIndex() -1;
  			ArrayList<String> tokens = new ArrayList<String>();
  			ArrayList<Integer> tokensLine = new ArrayList<Integer>();
  			ArrayList<Integer> tokensColumn = new ArrayList<Integer>();
  			javaTokenizer.createLexerArr(Instance.getCodeSegmentToTokenze());;
  			TokensList tokenlist =  javaTokenizer.getTokensList();	
  			ArrayList<Tokens> antlrTokens = tokenlist.getTokens(); 

  			for(Tokens token : antlrTokens) {
  				if(token.getText().contains("STARTFILE")  ||
  				   token.getText().contains("STARTCLASS") ||
  				   token.getText().contains("ENDCLASS")   ||
  				   token.getText().contains("ENDFILE")    ||
  				   token.getText().contains("STARTMETHOD")||
  				   token.getText().contains("ENDMETHOD"))
  	                 		continue;
              	
  				boolean first=true;
  				boolean second=true;
  				if(token.getTokenLine() + shiftStartingLine == Instance.getStartingIndex()) {
  					if(token.getTokenColumn() >= startCol)
              		tokens.add(token.getText());
  					tokensLine.add(token.getTokenLine()+Instance.getStartingIndex()-1);
  					tokensColumn.add(token.getTokenColumn());
  					first=false;
  				}
  			
  	            else if(token.getTokenLine() + shiftStartingLine == Instance.getEndingIndex() ) {            		
  	            		 if(token.getTokenColumn() <= endCol)
  	             		tokens.add(token.getText());
  	            		tokensLine.add(token.getTokenLine()+Instance.getStartingIndex()-1);
  	  					tokensColumn.add(token.getTokenColumn());
  	  					second=false;
  	            }
  		            	
  	            if(first && second)
  	            	{
  	            		tokens.add(token.getText());
  						tokensLine.add(token.getTokenLine()+Instance.getStartingIndex()-1);
  						tokensColumn.add(token.getTokenColumn());
  	            	}
  				}
  	            antlrTokens.clear();
  	            Instance.setTokens(tokens);
  	            Instance.setTokensLine(tokensLine);
  	            Instance.setTokensColumn(tokensColumn);
  			}           
              
  		}
    
    
    
    private static void refresh(String SPC_path, String ART_path)
	    {
		File f1 = new File(SPC_path);
		File f2 = new File(ART_path);
		f1.delete();
		f2.delete();
	    }

}

