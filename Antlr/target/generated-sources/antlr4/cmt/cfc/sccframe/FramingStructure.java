package cmt.cfc.sccframe;
import java.util.ArrayList;

import cmt.cfc.mccframe.FrameBreak;
import cmt.cfc.mccframe.Variable;

public class FramingStructure {

	public ArrayList<DynamicToken> Variables;
	int sccId;
	public String breakName = new String();
	ArrayList<String> GenTokens;
	
	
	public FramingStructure()
	{
		Variables=new ArrayList<DynamicToken>();
		sccId=-1;
		breakName= "\n";
	}
	
	public void setGenTokens(ArrayList<String> gen)
	{
		GenTokens=gen;
	}
	
	public ArrayList<String> getGenTokens()
	{
		return GenTokens;
	}
	
	
	
	public void setVariables(ArrayList<DynamicToken> vr)
	{
		Variables=vr;
	}

	public void setSCCId(int id)
	{
		sccId=id;
	}
	
	
	public void setBreak(String br)
	{
		breakName=br;
	}
	
	
	public int getSCCId()
	{
		
		return sccId;
		
	}
	
	public ArrayList<DynamicToken> getVariables()
	{
		return Variables;
	}
	
	
	
	
	
}
