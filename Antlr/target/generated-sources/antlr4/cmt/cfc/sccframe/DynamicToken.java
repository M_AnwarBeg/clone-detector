package cmt.cfc.sccframe;

import java.util.ArrayList;

public class DynamicToken {
	int tokenIndex;
	String varName;
	String oldName;
	public boolean marked;
	boolean last;
	boolean repeated;
	boolean cNxtLine;
	boolean cDoubleComma;
	boolean ignore;
	public ArrayList<String> tokenValues;
	public ArrayList<Integer> fid;
	public int mccId;

	public DynamicToken() {
		oldName = "`";
		varName = "variable_" + 0;
		tokenIndex = -1;
		marked = false;
		last = false;
		repeated = false;
		cNxtLine = false;
		ignore = false;
		tokenValues = new ArrayList<String>();
		fid= new ArrayList<Integer>();
		mccId=-1;
	}

	public ArrayList<String> getValues() {
		return tokenValues;
	}

	public boolean checkifExists(String _token) {
		for (String token : tokenValues) {
			if (token.equals(_token)) {
				continue;
			} else {
				return true;
			}
		}

		return false;
	}

	public void printTokens() {
		System.out.println("===== DynamicToken =====");
		for (String val : tokenValues) {
			System.out.println(val);
		}
		System.out.println("Variable name: " + varName);
		System.out.println("=================");
	}

	public int getTokenIndex() {
		return tokenIndex;
	}

	public String getVarName() {
		return varName;
	}

	public boolean isMarked() {
		return marked;
	}
	
	public boolean isIgnored() {
		return ignore;
	}

	public boolean isLast() {
		return last;
	}

	public boolean isRepeated() {

		return repeated;

	}
	
	public void setRepeated(boolean repeated) {

		this.repeated = repeated;

	}
	

	public boolean isCNxtLine() {
		return cNxtLine;
	}

	public boolean isCDoubleComma() {
		return cDoubleComma;
	}

	public ArrayList<String> getTokenValues() {
		return tokenValues;
	}
	public void setVarName(String var)
	{
		varName=var;
	}

}
