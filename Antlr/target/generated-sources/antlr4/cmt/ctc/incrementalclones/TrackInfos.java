package cmt.ctc.incrementalclones;

import java.util.ArrayList;

public class TrackInfos {
	
	private ArrayList<Tracks> trackInfos = new ArrayList<Tracks>();

	public TrackInfos()
	{
		
	}
	
	public ArrayList<Tracks> getTrackInfos() {
		return trackInfos;
	}

	public void setTrackInfos(ArrayList<Tracks> trackInfos) {
		trackInfos = trackInfos;
	}
	
	public Tracks getTrackAt(int index)
	{
		return trackInfos.get(index);
	}
	
	public void addTrack(Tracks obj)
	{
		trackInfos.add(obj);
	}
	
	public void removeTrackAt(int index)
	{
		trackInfos.remove(index);
	}
	
	public int getTrackInfosSize()
	{
		return trackInfos.size();
	}
	
	public void setAllTracksMatchFlag(boolean flag)
	{
		for(int i = 0; i< trackInfos.size() ; i++)
			trackInfos.get(i).setMatchFlag(flag);
	}	
}
