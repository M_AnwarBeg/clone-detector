package cmt.ctc.incrementalclones;

import java.util.ArrayList;

public class Tracks {

	private ArrayList<TrackInfo> track_info = new ArrayList<TrackInfo>();
	private boolean matchFlag = false;
	private ArrayList<Integer> tokentrack = new ArrayList<>();
	
	public Tracks(ArrayList<TrackInfo> trackinfo)
	{
		track_info = trackinfo;
		matchFlag = false;
	}

	public ArrayList<TrackInfo> getTrackinfos() {
		return track_info;
	}
	public void setTrackinfos(ArrayList<TrackInfo> trackinfos) {
		track_info = trackinfos;
	}
	public TrackInfo getTrackInfoAt(int index){
		return track_info.get(index);
	}	
	public boolean isMatchFlag() {
		return matchFlag;
	}
	public void setMatchFlag(boolean flag) {
		matchFlag = flag;
	}
	public int getTrackInstancesSize(){
		return track_info.size();
	}
	public void removeTrackInfo(int index){
		track_info.remove(index);
	}

	public ArrayList<Integer> getTokentrack() {
		return tokentrack;
	}

	public void setTokentrack(ArrayList<Integer> tokentrack) {
		this.tokentrack = tokentrack;
	}
	
	public void addTokenTrack(int token)
	{
		this.tokentrack.add(token);
	} 
	
	public int getTokenTrackSize()
	{
		return this.tokentrack.size();
	}
	
	public int getTrackInfoSize()
	{
		return this.track_info.size();		
	}
	private ArrayList<Integer> getTrackInfosFileIds()
	{
		ArrayList<Integer> fileIds = new  ArrayList<Integer>();
		for(int i = 0; i < track_info.size() ; i++)
		{
			fileIds.add(track_info.get(i).file_id);
		}
		return fileIds;
	}
	public boolean containsAllFiles(ArrayList<Integer> filesList)
	{
		ArrayList<Integer> trackInfosFileIds = getTrackInfosFileIds();

		for(int i = 0; i < filesList.size() ; i++)
		{
			if(trackInfosFileIds.contains(filesList.get(i)))
				trackInfosFileIds.remove(filesList.get(i));
			else
				return false;
		}	
		
		return true;
	}
}
