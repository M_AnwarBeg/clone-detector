package cmt.ctc.incrementalclones;

import java.util.ArrayList;

public class CloneInfo {
	public CloneInfo(int ccid,int id , String file, int start_line, int end_line,int start_index,int stop_index,ArrayList<Integer> tokens)
	{	class_id=ccid;
		clone_id=id;
		//sequenceindex=seqindex;
		filename=file;
		startline=start_line;
		endline=end_line;
		start_index=start;
		stop_index=stop;
		tokensList=tokens;
		
	}
	public String getFileName()
	{
	     //include validation, logic, logging or whatever you like here
	    return this.filename;
	}
	public int class_id;
	public int clone_id;
	public String filename;
	public int startline;
	public int endline;
	public int start;
	public int stop;
	public ArrayList<Integer> tokensList;
}
