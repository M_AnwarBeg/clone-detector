package cmt.ctc.incrementalclones;


public class TrackInfo 
{
	public TrackInfo(String file,int start_seqindex,int stop_seqindex,int start_line,int end_line, int start_index, int stop_index, int methodid,int seq_index , int fileid)
	{
		startsequenceindex=start_seqindex;
		stopsequenceindex=stop_seqindex;
		seqindex=seq_index;
		filename=file;
		startline=start_line;
		endline=end_line;
		startindex=start_index;
		stopindex=stop_index;
		method_id=methodid;
		file_id = fileid; 
		match = false;
	}
	public int startsequenceindex;
	public int stopsequenceindex;
	public int seqindex;
	public String filename;
	public int startline;
	public int endline;
	public int startindex;
	public int stopindex;
	public int method_id;
	public int file_id;
	public boolean match;
};