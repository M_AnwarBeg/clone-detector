package cmt.ctc.mongodb;

import java.util.ArrayList;

public class AllFilesContent 
{
	    public ArrayList<String> getUpdatedAndNewlyAddedFiles()
	    {
		   ArrayList<String> updatedAndNewlyAddedFiles = new ArrayList<String>();
		   updatedAndNewlyAddedFiles.addAll(this.getUpdatedfiles());
		   updatedAndNewlyAddedFiles.addAll(this.getNewfiles());
		   return updatedAndNewlyAddedFiles;
	    }
	   
	    public void setUpdated(ArrayList<String> updatedfile) {
	        this.updatedcontent = updatedfile;
	    }

	    public ArrayList<String> getUpdated() {
	        return updatedcontent;
	    }

	    public void setOld(ArrayList<String> oldfile) {
	       this.oldcontent = oldfile;
	    }

	    public ArrayList<String> getOld() {
	       return oldcontent;
	    }
	    public boolean getrunflag() {
		       return firstrun;
		    }
	    public void setrunflag(boolean flag) {
		       firstrun=flag;
		    }
	    
	    public ArrayList<String> getUpdatedfiles() {
	        return updatedfiles;
	    }
	    
	    public void setUpdatedfiles(ArrayList<String> files) {
	        updatedfiles=files;
	    }
	    
	    public ArrayList<String> getOldfiles() {
	        return oldfiles;
	    }
	    
	    public void setOldfiles(ArrayList<String> files) {
	        oldfiles=files;
	    }	    
	
	    public ArrayList<String> getNewfiles() {
			return newfiles;
		}

		public void setNewfiles(ArrayList<String> newfiles) {
			this.newfiles = newfiles;
		}

		public ArrayList<String> getRemovedfiles() {
			return removedfiles;
		}

		public void setRemovedfiles(ArrayList<String> removedfiles) {
			this.removedfiles = removedfiles;
		}

		public ArrayList<String> getNewcontent() {
			return newcontent;
		}

		public void setNewcontent(ArrayList<String> newcontent) {
			this.newcontent = newcontent;
		}

	boolean firstrun=false;
	private ArrayList<String> updatedcontent;
	private ArrayList<String> oldcontent;
	private ArrayList<String> updatedfiles;
	private ArrayList<String> oldfiles;
	private ArrayList<String> newfiles;
	private ArrayList<String> newcontent;
	private ArrayList<String> removedfiles;
};