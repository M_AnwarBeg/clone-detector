package cmt.ctc.mongodb;

public class TokenDetails 
{
	public TokenDetails(int type,int line,int startindex, int stopindex,int methodid , int fileid)
	{
		token_type=type;
		token_line=line;
		start_index=startindex;
		stop_index=stopindex;
		method_id=methodid;
		file_id = fileid;
	}
	public int token_type;
	public int token_line;
	public int start_index;
	public int stop_index;
	public int method_id;
	public int file_id;
}
