package cmt.ctc.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;


class InsertThread extends Thread {
	   
	   private DBCollection collection;
	   private SequenceHash s;
	   InsertThread( DBCollection DBcollect,SequenceHash hash) {
		   collection = DBcollect;
		   s=hash;
	   }

	   public void run() {
		   BasicDBObject document = new BasicDBObject();
	        document.put("filename", s.filename);
	        document.put("sequenceindex", s.sequenceindex);
	        document.put("sequencehash", s.sequencehash);
	        document.put("startline", s.start_line);
	        document.put("stopline", s.start_line);
	        document.put("startindex", s.start_index);
	        document.put("stopindex", s.stop_index);
	        collection.insert(document);
	   }
	
	}