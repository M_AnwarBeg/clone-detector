package cmt.ctc.mongodb;

public class SequenceHash 
{
	public SequenceHash(String file,int seqindex, String hash,int startline,int stopline,int start,int stop,int methodid , int fileid)
	{
		filename=file;
		sequenceindex=seqindex;
		sequencehash=hash;
		start_line=startline;
		stop_line=stopline;
		start_index=start;
		stop_index=stop;
		method_id=methodid;
		file_id = fileid;
	}
	public String filename;
	public int sequenceindex;
	public String sequencehash;
	public int start_line;
	public int stop_line;
	public int start_index;
	public int stop_index;
	public int method_id;
	public int file_id;
};
