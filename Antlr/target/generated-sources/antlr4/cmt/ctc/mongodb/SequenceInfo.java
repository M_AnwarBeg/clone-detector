package cmt.ctc.mongodb;

import java.util.ArrayList;

public class SequenceInfo 
{
	public SequenceInfo(int seqindex, String hash,int startLine,int stopLine,int start_index,int stop_index,ArrayList<Integer> Token)
	{
		sequenceindex=seqindex;
		sequencehash=hash;
		start_line=startLine;
		stop_line=stopLine;
		token_start=start_index;
		token_stop=stop_index;
		token=Token;
	}
	public int sequenceindex;
	public String sequencehash;
	public int start_line;
	public int stop_line;
	public int token_start;
	public int token_stop;
	public ArrayList<Integer> token = new ArrayList<>();
}
