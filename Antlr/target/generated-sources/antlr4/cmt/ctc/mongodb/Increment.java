package cmt.ctc.mongodb;

import java.util.ArrayList;

import cmt.cddc.simpleclones.SimpleClone;
import cmt.cddc.structures.sMethod;
import cmt.cddc.structures.sMethod_List;

public class Increment {
	//lists which clone classes are represented in each file
		/*
	    public void updateFilesWithSimpleClones(ArrayList<SimpleClone> simpleClones)
	    {
	    	for (int i = 0; i < simpleClones.size(); i++) {
	            for (int j = 0; j < simpleClones.get(i).getInstancesSize(); j++) {
	                int normalCC = i * 100;
	                int fileID = simpleClones.get(i).getInstanceAt(j).getFileId();
	                int numberCC = sFile_List.getsFileAt(fileID).getCloneClassesSize();
	                int lastCC = 0;
	                if (numberCC > 0) {
	                    lastCC = sFile_List.getsFileAt(fileID).getCloneClassAt(numberCC - 1);
	                    if (lastCC / 100 == i) {
	                        normalCC = lastCC + 1;
	                    }
	                }
	                sFile_List.getsFileAt(fileID).addCloneClass(normalCC);
	            }
	        }
	    }
	    */
	    //lists which clone classes are represented in each method
	   
		public void updateMethodsWithSimpleClones(ArrayList<SimpleClone> simpleClones)
	    {
	    	for (int i = 0; i < simpleClones.size(); i++) {
	            for (int j = 0; j < simpleClones.get(i).getInstancesSize(); j++) {
	                int normalCC = i * 100;
	                int methodID = simpleClones.get(i).getInstanceAt(j).getMethodId();
	                sMethod Method=null;
	                if (methodID >= 0) {
	                    //int numberCC = sMethod_List.getsMethodAt(methodID).getCloneClassSize();
	                	 for(sMethod o : sMethod_List.getMethodsList()) {
	                	        if(o != null && o.getMethodId()==methodID) {
	                	        	Method=o;
	                	        	break;
	                	        }
	                	    }
	                	int numberCC = -1;
	                	
	                	if(Method != null)
	                	{
	                		System.out.println("---------Invalid method id detected--------");
	                		return;
	                	}
	                	else
	                	    numberCC = Method.getCloneClassSize();
	                	
	                    int lastCC = 0;
	                    if (numberCC > 0) {
	                       // lastCC = sMethod_List.getsMethodAt(methodID).getCloneClassAt(numberCC - 1);
	                    	lastCC = Method.getCloneClassAt(numberCC - 1);
	                        if (lastCC / 100 == i) {
	                            normalCC = lastCC + 1;
	                        }
	                    }
	                    Method.addCloneClass(normalCC);
	                   // sMethod_List.getsMethodAt(methodID).addCloneClass(normalCC);
	                }
	            }
	        }
	    	/*for (int i = 0; i < simpleClones.size(); i++) {
	            for (int j = 0; j < simpleClones.get(i).getInstancesSize(); j++) {
	                int normalCC = i * 100;
	                int methodID = simpleClones.get(i).getInstanceAt(j).getMethodId();
	                if (methodID >= 0) {
	                    int numberCC = sMethod_List.getsMethodAt(methodID).getCloneClassSize();
	                    int lastCC = 0;
	                    if (numberCC > 0) {
	                        lastCC = sMethod_List.getsMethodAt(methodID).getCloneClassAt(numberCC - 1);
	                        if (lastCC / 100 == i) {
	                            normalCC = lastCC + 1;
	                        }
	                    }
	                    sMethod_List.getsMethodAt(methodID).addCloneClass(normalCC);
	                }
	            }
	        }*/
	    }
}
