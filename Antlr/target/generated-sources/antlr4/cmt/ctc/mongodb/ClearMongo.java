package cmt.ctc.mongodb;

import com.mongodb.DB;
import com.mongodb.Mongo;

public class ClearMongo {
	
	public static void clearMongoDB() {
		try
		{
			Mongo mongo = new Mongo("localhost", 27017);
		
			DB database = mongo.getDB("admin");
			if(database!=null)
			{
				database.getCollection("BoundaryMarkers").drop();
				database.getCollection("ByFiles").drop();
				database.getCollection("ExecutionTime").drop();
				database.getCollection("FileInfo").drop();
				database.getCollection("MethodInfo").drop();
				database.getCollection("SimpleClones").drop();
				database.getCollection("SimpleClonesInstances").drop();

			}
			mongo.close();
		  
			System.out.println("Collections Cleared from Database admin");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	

}
