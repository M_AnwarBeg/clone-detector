package cmt.ctc.mongodb;

public class LineColTrack 
{
	public LineColTrack(int startline,int startcol)
	{
		start_col=startcol;
		start_line=startline;
	}

	public int start_line;
	public int start_col;

};