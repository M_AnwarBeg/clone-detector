package cmt.ctc.mongodb;

public class FileInfo 
{
	public FileInfo(int seqindex, String file , int fid)
	{
		sequenceindex=seqindex;
		filename=file;
		fileid = fid;
	}
	public int sequenceindex;
	public String filename;
	public int fileid;
}
