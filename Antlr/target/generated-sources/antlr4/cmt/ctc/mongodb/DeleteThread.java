package cmt.ctc.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;


class DeleteThread extends Thread {
	   
	private DBCollection collection;
    private SequenceHash s;
   
   DeleteThread(DBCollection DBcollect, SequenceHash hash) {
	   collection = DBcollect;
	   s=hash;
   }

   public void run() {
	   
	   try{
		   BasicDBObject whereQuery = new BasicDBObject();
		   whereQuery.put("filename", s.filename);
			 collection.remove(whereQuery);
	   }
	   
		 catch (RuntimeException e)
	    {
	      System.out.println("** RuntimeException from thread");

	      throw e;
	    } 
   }
}
