package handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;
import cmt.cddc.*;
import cmt.cddc.clonedetectioninitializer.CloneDetectionSettingsWizardDelegate;  
/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class SampleHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
		MessageDialog dialog =
			    new MessageDialog(window.getShell() , "My Title", null,
			    	    "Select Your Choice", MessageDialog.INFORMATION, new String[] { "Detect Clones",
			    	    	    "Visualize Code Clones", "Frame Code Clones" }, 0);
			int result = dialog.open();
			
		System.out.println("Result: " + result);
		if(result == 0)
		{
			try
			{
			
				/*Main s = new Main();
				s.main(null);*/
				CloneDetectionSettingsWizardDelegate s = new CloneDetectionSettingsWizardDelegate();
				//call clones main and further it would be done
				dialog.close(); 
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage());

			}
			
		}
		else if (result == 1)
		{
			//visualize clones
			dialog.close();
		}
		else
		{
			//frame code clones
			dialog.close();
		}
		return null;
    }
}
