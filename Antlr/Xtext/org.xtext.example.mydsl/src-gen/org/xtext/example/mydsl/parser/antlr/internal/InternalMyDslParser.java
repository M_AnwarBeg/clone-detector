package org.xtext.example.mydsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_EXPRESSION", "RULE_WS", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_ANY_OTHER", "'#adapt:'", "'#endadapt'", "'#insert-after'", "'#endinsert'", "'#setloop'", "','", "'#endsetloop'", "'#break'", "'#endbreak'", "'#option'", "'='", "'#endoption'", "'#select'", "'#endselect'", "'#output'", "'#while'", "'#endwhile'", "'%'", "'#set'", "'#outdir'", "'#outfile'", "'#remove'", "'#message'", "'#endmessage'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_EXPRESSION=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=6;
    public static final int RULE_WS=5;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }



     	private MyDslGrammarAccess grammarAccess;

        public InternalMyDslParser(TokenStream input, MyDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected MyDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalMyDsl.g:64:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalMyDsl.g:64:46: (iv_ruleModel= ruleModel EOF )
            // InternalMyDsl.g:65:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalMyDsl.g:71:1: ruleModel returns [EObject current=null] : ( (lv_commands_0_0= ruleCommands ) )* ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_commands_0_0 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:77:2: ( ( (lv_commands_0_0= ruleCommands ) )* )
            // InternalMyDsl.g:78:2: ( (lv_commands_0_0= ruleCommands ) )*
            {
            // InternalMyDsl.g:78:2: ( (lv_commands_0_0= ruleCommands ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12||LA1_0==14||LA1_0==21||LA1_0==24||(LA1_0>=26 && LA1_0<=27)||(LA1_0>=29 && LA1_0<=30)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:79:3: (lv_commands_0_0= ruleCommands )
            	    {
            	    // InternalMyDsl.g:79:3: (lv_commands_0_0= ruleCommands )
            	    // InternalMyDsl.g:80:4: lv_commands_0_0= ruleCommands
            	    {

            	    				newCompositeNode(grammarAccess.getModelAccess().getCommandsCommandsParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_3);
            	    lv_commands_0_0=ruleCommands();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getModelRule());
            	    				}
            	    				add(
            	    					current,
            	    					"commands",
            	    					lv_commands_0_0,
            	    					"org.xtext.example.mydsl.MyDsl.Commands");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleCommands"
    // InternalMyDsl.g:100:1: entryRuleCommands returns [String current=null] : iv_ruleCommands= ruleCommands EOF ;
    public final String entryRuleCommands() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCommands = null;


        try {
            // InternalMyDsl.g:100:48: (iv_ruleCommands= ruleCommands EOF )
            // InternalMyDsl.g:101:2: iv_ruleCommands= ruleCommands EOF
            {
             newCompositeNode(grammarAccess.getCommandsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommands=ruleCommands();

            state._fsp--;

             current =iv_ruleCommands.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommands"


    // $ANTLR start "ruleCommands"
    // InternalMyDsl.g:107:1: ruleCommands returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_SetCommand_0= ruleSetCommand | this_CommentCommand_1= ruleCommentCommand | this_WhileCommand_2= ruleWhileCommand | this_OutputCommand_3= ruleOutputCommand | this_SelectCommand_4= ruleSelectCommand | this_OptionCommand_5= ruleOptionCommand | this_InsertAfterCommand_6= ruleInsertAfterCommand | this_AdaptCommand_7= ruleAdaptCommand ) ;
    public final AntlrDatatypeRuleToken ruleCommands() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        AntlrDatatypeRuleToken this_SetCommand_0 = null;

        AntlrDatatypeRuleToken this_CommentCommand_1 = null;

        AntlrDatatypeRuleToken this_WhileCommand_2 = null;

        AntlrDatatypeRuleToken this_OutputCommand_3 = null;

        AntlrDatatypeRuleToken this_SelectCommand_4 = null;

        AntlrDatatypeRuleToken this_OptionCommand_5 = null;

        AntlrDatatypeRuleToken this_InsertAfterCommand_6 = null;

        AntlrDatatypeRuleToken this_AdaptCommand_7 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:113:2: ( (this_SetCommand_0= ruleSetCommand | this_CommentCommand_1= ruleCommentCommand | this_WhileCommand_2= ruleWhileCommand | this_OutputCommand_3= ruleOutputCommand | this_SelectCommand_4= ruleSelectCommand | this_OptionCommand_5= ruleOptionCommand | this_InsertAfterCommand_6= ruleInsertAfterCommand | this_AdaptCommand_7= ruleAdaptCommand ) )
            // InternalMyDsl.g:114:2: (this_SetCommand_0= ruleSetCommand | this_CommentCommand_1= ruleCommentCommand | this_WhileCommand_2= ruleWhileCommand | this_OutputCommand_3= ruleOutputCommand | this_SelectCommand_4= ruleSelectCommand | this_OptionCommand_5= ruleOptionCommand | this_InsertAfterCommand_6= ruleInsertAfterCommand | this_AdaptCommand_7= ruleAdaptCommand )
            {
            // InternalMyDsl.g:114:2: (this_SetCommand_0= ruleSetCommand | this_CommentCommand_1= ruleCommentCommand | this_WhileCommand_2= ruleWhileCommand | this_OutputCommand_3= ruleOutputCommand | this_SelectCommand_4= ruleSelectCommand | this_OptionCommand_5= ruleOptionCommand | this_InsertAfterCommand_6= ruleInsertAfterCommand | this_AdaptCommand_7= ruleAdaptCommand )
            int alt2=8;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt2=1;
                }
                break;
            case 29:
                {
                alt2=2;
                }
                break;
            case 27:
                {
                alt2=3;
                }
                break;
            case 26:
                {
                alt2=4;
                }
                break;
            case 24:
                {
                alt2=5;
                }
                break;
            case 21:
                {
                alt2=6;
                }
                break;
            case 14:
                {
                alt2=7;
                }
                break;
            case 12:
                {
                alt2=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:115:3: this_SetCommand_0= ruleSetCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getSetCommandParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_SetCommand_0=ruleSetCommand();

                    state._fsp--;


                    			current.merge(this_SetCommand_0);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:126:3: this_CommentCommand_1= ruleCommentCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getCommentCommandParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_CommentCommand_1=ruleCommentCommand();

                    state._fsp--;


                    			current.merge(this_CommentCommand_1);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:137:3: this_WhileCommand_2= ruleWhileCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getWhileCommandParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_WhileCommand_2=ruleWhileCommand();

                    state._fsp--;


                    			current.merge(this_WhileCommand_2);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:148:3: this_OutputCommand_3= ruleOutputCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getOutputCommandParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_OutputCommand_3=ruleOutputCommand();

                    state._fsp--;


                    			current.merge(this_OutputCommand_3);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:159:3: this_SelectCommand_4= ruleSelectCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getSelectCommandParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_SelectCommand_4=ruleSelectCommand();

                    state._fsp--;


                    			current.merge(this_SelectCommand_4);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalMyDsl.g:170:3: this_OptionCommand_5= ruleOptionCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getOptionCommandParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_OptionCommand_5=ruleOptionCommand();

                    state._fsp--;


                    			current.merge(this_OptionCommand_5);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalMyDsl.g:181:3: this_InsertAfterCommand_6= ruleInsertAfterCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getInsertAfterCommandParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_InsertAfterCommand_6=ruleInsertAfterCommand();

                    state._fsp--;


                    			current.merge(this_InsertAfterCommand_6);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalMyDsl.g:192:3: this_AdaptCommand_7= ruleAdaptCommand
                    {

                    			newCompositeNode(grammarAccess.getCommandsAccess().getAdaptCommandParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_AdaptCommand_7=ruleAdaptCommand();

                    state._fsp--;


                    			current.merge(this_AdaptCommand_7);
                    		

                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommands"


    // $ANTLR start "entryRuleAdaptCommand"
    // InternalMyDsl.g:206:1: entryRuleAdaptCommand returns [String current=null] : iv_ruleAdaptCommand= ruleAdaptCommand EOF ;
    public final String entryRuleAdaptCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleAdaptCommand = null;


        try {
            // InternalMyDsl.g:206:52: (iv_ruleAdaptCommand= ruleAdaptCommand EOF )
            // InternalMyDsl.g:207:2: iv_ruleAdaptCommand= ruleAdaptCommand EOF
            {
             newCompositeNode(grammarAccess.getAdaptCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAdaptCommand=ruleAdaptCommand();

            state._fsp--;

             current =iv_ruleAdaptCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdaptCommand"


    // $ANTLR start "ruleAdaptCommand"
    // InternalMyDsl.g:213:1: ruleAdaptCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#adapt:' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_OptionCommand_4= ruleOptionCommand | this_InsertAfterCommand_5= ruleInsertAfterCommand | this_SelectCommand_6= ruleSelectCommand )* kw= '#endadapt' ) ;
    public final AntlrDatatypeRuleToken ruleAdaptCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_2=null;
        Token this_WS_3=null;
        AntlrDatatypeRuleToken this_OptionCommand_4 = null;

        AntlrDatatypeRuleToken this_InsertAfterCommand_5 = null;

        AntlrDatatypeRuleToken this_SelectCommand_6 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:219:2: ( (kw= '#adapt:' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_OptionCommand_4= ruleOptionCommand | this_InsertAfterCommand_5= ruleInsertAfterCommand | this_SelectCommand_6= ruleSelectCommand )* kw= '#endadapt' ) )
            // InternalMyDsl.g:220:2: (kw= '#adapt:' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_OptionCommand_4= ruleOptionCommand | this_InsertAfterCommand_5= ruleInsertAfterCommand | this_SelectCommand_6= ruleSelectCommand )* kw= '#endadapt' )
            {
            // InternalMyDsl.g:220:2: (kw= '#adapt:' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_OptionCommand_4= ruleOptionCommand | this_InsertAfterCommand_5= ruleInsertAfterCommand | this_SelectCommand_6= ruleSelectCommand )* kw= '#endadapt' )
            // InternalMyDsl.g:221:3: kw= '#adapt:' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_OptionCommand_4= ruleOptionCommand | this_InsertAfterCommand_5= ruleInsertAfterCommand | this_SelectCommand_6= ruleSelectCommand )* kw= '#endadapt'
            {
            kw=(Token)match(input,12,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getAdaptCommandAccess().getAdaptKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_5); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getAdaptCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:233:3: (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_OptionCommand_4= ruleOptionCommand | this_InsertAfterCommand_5= ruleInsertAfterCommand | this_SelectCommand_6= ruleSelectCommand )*
            loop3:
            do {
                int alt3=6;
                switch ( input.LA(1) ) {
                case RULE_EXPRESSION:
                    {
                    alt3=1;
                    }
                    break;
                case RULE_WS:
                    {
                    alt3=2;
                    }
                    break;
                case 21:
                    {
                    alt3=3;
                    }
                    break;
                case 14:
                    {
                    alt3=4;
                    }
                    break;
                case 24:
                    {
                    alt3=5;
                    }
                    break;

                }

                switch (alt3) {
            	case 1 :
            	    // InternalMyDsl.g:234:4: this_EXPRESSION_2= RULE_EXPRESSION
            	    {
            	    this_EXPRESSION_2=(Token)match(input,RULE_EXPRESSION,FOLLOW_5); 

            	    				current.merge(this_EXPRESSION_2);
            	    			

            	    				newLeafNode(this_EXPRESSION_2, grammarAccess.getAdaptCommandAccess().getEXPRESSIONTerminalRuleCall_2_0());
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:242:4: this_WS_3= RULE_WS
            	    {
            	    this_WS_3=(Token)match(input,RULE_WS,FOLLOW_5); 

            	    				current.merge(this_WS_3);
            	    			

            	    				newLeafNode(this_WS_3, grammarAccess.getAdaptCommandAccess().getWSTerminalRuleCall_2_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalMyDsl.g:250:4: this_OptionCommand_4= ruleOptionCommand
            	    {

            	    				newCompositeNode(grammarAccess.getAdaptCommandAccess().getOptionCommandParserRuleCall_2_2());
            	    			
            	    pushFollow(FOLLOW_5);
            	    this_OptionCommand_4=ruleOptionCommand();

            	    state._fsp--;


            	    				current.merge(this_OptionCommand_4);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalMyDsl.g:261:4: this_InsertAfterCommand_5= ruleInsertAfterCommand
            	    {

            	    				newCompositeNode(grammarAccess.getAdaptCommandAccess().getInsertAfterCommandParserRuleCall_2_3());
            	    			
            	    pushFollow(FOLLOW_5);
            	    this_InsertAfterCommand_5=ruleInsertAfterCommand();

            	    state._fsp--;


            	    				current.merge(this_InsertAfterCommand_5);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 5 :
            	    // InternalMyDsl.g:272:4: this_SelectCommand_6= ruleSelectCommand
            	    {

            	    				newCompositeNode(grammarAccess.getAdaptCommandAccess().getSelectCommandParserRuleCall_2_4());
            	    			
            	    pushFollow(FOLLOW_5);
            	    this_SelectCommand_6=ruleSelectCommand();

            	    state._fsp--;


            	    				current.merge(this_SelectCommand_6);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            kw=(Token)match(input,13,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getAdaptCommandAccess().getEndadaptKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdaptCommand"


    // $ANTLR start "entryRuleInsertAfterCommand"
    // InternalMyDsl.g:292:1: entryRuleInsertAfterCommand returns [String current=null] : iv_ruleInsertAfterCommand= ruleInsertAfterCommand EOF ;
    public final String entryRuleInsertAfterCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInsertAfterCommand = null;


        try {
            // InternalMyDsl.g:292:58: (iv_ruleInsertAfterCommand= ruleInsertAfterCommand EOF )
            // InternalMyDsl.g:293:2: iv_ruleInsertAfterCommand= ruleInsertAfterCommand EOF
            {
             newCompositeNode(grammarAccess.getInsertAfterCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleInsertAfterCommand=ruleInsertAfterCommand();

            state._fsp--;

             current =iv_ruleInsertAfterCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInsertAfterCommand"


    // $ANTLR start "ruleInsertAfterCommand"
    // InternalMyDsl.g:299:1: ruleInsertAfterCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#insert-after' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_AdaptCommand_4= ruleAdaptCommand | this_SetCommand_5= ruleSetCommand | this_SelectCommand_6= ruleSelectCommand | this_OutputCommand_7= ruleOutputCommand | this_OutfileCommand_8= ruleOutfileCommand | this_OutdirCommand_9= ruleOutdirCommand | this_MessageCommand_10= ruleMessageCommand | this_WhileCommand_11= ruleWhileCommand | this_RemoveCommand_12= ruleRemoveCommand | this_BreakCommand_13= ruleBreakCommand | this_SetloopCommand_14= ruleSetloopCommand )* kw= '#endinsert' ) ;
    public final AntlrDatatypeRuleToken ruleInsertAfterCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_2=null;
        Token this_WS_3=null;
        AntlrDatatypeRuleToken this_AdaptCommand_4 = null;

        AntlrDatatypeRuleToken this_SetCommand_5 = null;

        AntlrDatatypeRuleToken this_SelectCommand_6 = null;

        AntlrDatatypeRuleToken this_OutputCommand_7 = null;

        AntlrDatatypeRuleToken this_OutfileCommand_8 = null;

        AntlrDatatypeRuleToken this_OutdirCommand_9 = null;

        AntlrDatatypeRuleToken this_MessageCommand_10 = null;

        AntlrDatatypeRuleToken this_WhileCommand_11 = null;

        AntlrDatatypeRuleToken this_RemoveCommand_12 = null;

        AntlrDatatypeRuleToken this_BreakCommand_13 = null;

        AntlrDatatypeRuleToken this_SetloopCommand_14 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:305:2: ( (kw= '#insert-after' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_AdaptCommand_4= ruleAdaptCommand | this_SetCommand_5= ruleSetCommand | this_SelectCommand_6= ruleSelectCommand | this_OutputCommand_7= ruleOutputCommand | this_OutfileCommand_8= ruleOutfileCommand | this_OutdirCommand_9= ruleOutdirCommand | this_MessageCommand_10= ruleMessageCommand | this_WhileCommand_11= ruleWhileCommand | this_RemoveCommand_12= ruleRemoveCommand | this_BreakCommand_13= ruleBreakCommand | this_SetloopCommand_14= ruleSetloopCommand )* kw= '#endinsert' ) )
            // InternalMyDsl.g:306:2: (kw= '#insert-after' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_AdaptCommand_4= ruleAdaptCommand | this_SetCommand_5= ruleSetCommand | this_SelectCommand_6= ruleSelectCommand | this_OutputCommand_7= ruleOutputCommand | this_OutfileCommand_8= ruleOutfileCommand | this_OutdirCommand_9= ruleOutdirCommand | this_MessageCommand_10= ruleMessageCommand | this_WhileCommand_11= ruleWhileCommand | this_RemoveCommand_12= ruleRemoveCommand | this_BreakCommand_13= ruleBreakCommand | this_SetloopCommand_14= ruleSetloopCommand )* kw= '#endinsert' )
            {
            // InternalMyDsl.g:306:2: (kw= '#insert-after' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_AdaptCommand_4= ruleAdaptCommand | this_SetCommand_5= ruleSetCommand | this_SelectCommand_6= ruleSelectCommand | this_OutputCommand_7= ruleOutputCommand | this_OutfileCommand_8= ruleOutfileCommand | this_OutdirCommand_9= ruleOutdirCommand | this_MessageCommand_10= ruleMessageCommand | this_WhileCommand_11= ruleWhileCommand | this_RemoveCommand_12= ruleRemoveCommand | this_BreakCommand_13= ruleBreakCommand | this_SetloopCommand_14= ruleSetloopCommand )* kw= '#endinsert' )
            // InternalMyDsl.g:307:3: kw= '#insert-after' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_AdaptCommand_4= ruleAdaptCommand | this_SetCommand_5= ruleSetCommand | this_SelectCommand_6= ruleSelectCommand | this_OutputCommand_7= ruleOutputCommand | this_OutfileCommand_8= ruleOutfileCommand | this_OutdirCommand_9= ruleOutdirCommand | this_MessageCommand_10= ruleMessageCommand | this_WhileCommand_11= ruleWhileCommand | this_RemoveCommand_12= ruleRemoveCommand | this_BreakCommand_13= ruleBreakCommand | this_SetloopCommand_14= ruleSetloopCommand )* kw= '#endinsert'
            {
            kw=(Token)match(input,14,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getInsertAfterCommandAccess().getInsertAfterKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_6); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getInsertAfterCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:319:3: (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_AdaptCommand_4= ruleAdaptCommand | this_SetCommand_5= ruleSetCommand | this_SelectCommand_6= ruleSelectCommand | this_OutputCommand_7= ruleOutputCommand | this_OutfileCommand_8= ruleOutfileCommand | this_OutdirCommand_9= ruleOutdirCommand | this_MessageCommand_10= ruleMessageCommand | this_WhileCommand_11= ruleWhileCommand | this_RemoveCommand_12= ruleRemoveCommand | this_BreakCommand_13= ruleBreakCommand | this_SetloopCommand_14= ruleSetloopCommand )*
            loop4:
            do {
                int alt4=14;
                switch ( input.LA(1) ) {
                case RULE_EXPRESSION:
                    {
                    alt4=1;
                    }
                    break;
                case RULE_WS:
                    {
                    alt4=2;
                    }
                    break;
                case 12:
                    {
                    alt4=3;
                    }
                    break;
                case 30:
                    {
                    alt4=4;
                    }
                    break;
                case 24:
                    {
                    alt4=5;
                    }
                    break;
                case 26:
                    {
                    alt4=6;
                    }
                    break;
                case 32:
                    {
                    alt4=7;
                    }
                    break;
                case 31:
                    {
                    alt4=8;
                    }
                    break;
                case 34:
                    {
                    alt4=9;
                    }
                    break;
                case 27:
                    {
                    alt4=10;
                    }
                    break;
                case 33:
                    {
                    alt4=11;
                    }
                    break;
                case 19:
                    {
                    alt4=12;
                    }
                    break;
                case 16:
                    {
                    alt4=13;
                    }
                    break;

                }

                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:320:4: this_EXPRESSION_2= RULE_EXPRESSION
            	    {
            	    this_EXPRESSION_2=(Token)match(input,RULE_EXPRESSION,FOLLOW_6); 

            	    				current.merge(this_EXPRESSION_2);
            	    			

            	    				newLeafNode(this_EXPRESSION_2, grammarAccess.getInsertAfterCommandAccess().getEXPRESSIONTerminalRuleCall_2_0());
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:328:4: this_WS_3= RULE_WS
            	    {
            	    this_WS_3=(Token)match(input,RULE_WS,FOLLOW_6); 

            	    				current.merge(this_WS_3);
            	    			

            	    				newLeafNode(this_WS_3, grammarAccess.getInsertAfterCommandAccess().getWSTerminalRuleCall_2_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalMyDsl.g:336:4: this_AdaptCommand_4= ruleAdaptCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getAdaptCommandParserRuleCall_2_2());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_AdaptCommand_4=ruleAdaptCommand();

            	    state._fsp--;


            	    				current.merge(this_AdaptCommand_4);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalMyDsl.g:347:4: this_SetCommand_5= ruleSetCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getSetCommandParserRuleCall_2_3());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_SetCommand_5=ruleSetCommand();

            	    state._fsp--;


            	    				current.merge(this_SetCommand_5);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 5 :
            	    // InternalMyDsl.g:358:4: this_SelectCommand_6= ruleSelectCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getSelectCommandParserRuleCall_2_4());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_SelectCommand_6=ruleSelectCommand();

            	    state._fsp--;


            	    				current.merge(this_SelectCommand_6);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 6 :
            	    // InternalMyDsl.g:369:4: this_OutputCommand_7= ruleOutputCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getOutputCommandParserRuleCall_2_5());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_OutputCommand_7=ruleOutputCommand();

            	    state._fsp--;


            	    				current.merge(this_OutputCommand_7);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 7 :
            	    // InternalMyDsl.g:380:4: this_OutfileCommand_8= ruleOutfileCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getOutfileCommandParserRuleCall_2_6());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_OutfileCommand_8=ruleOutfileCommand();

            	    state._fsp--;


            	    				current.merge(this_OutfileCommand_8);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 8 :
            	    // InternalMyDsl.g:391:4: this_OutdirCommand_9= ruleOutdirCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getOutdirCommandParserRuleCall_2_7());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_OutdirCommand_9=ruleOutdirCommand();

            	    state._fsp--;


            	    				current.merge(this_OutdirCommand_9);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 9 :
            	    // InternalMyDsl.g:402:4: this_MessageCommand_10= ruleMessageCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getMessageCommandParserRuleCall_2_8());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_MessageCommand_10=ruleMessageCommand();

            	    state._fsp--;


            	    				current.merge(this_MessageCommand_10);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 10 :
            	    // InternalMyDsl.g:413:4: this_WhileCommand_11= ruleWhileCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getWhileCommandParserRuleCall_2_9());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_WhileCommand_11=ruleWhileCommand();

            	    state._fsp--;


            	    				current.merge(this_WhileCommand_11);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 11 :
            	    // InternalMyDsl.g:424:4: this_RemoveCommand_12= ruleRemoveCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getRemoveCommandParserRuleCall_2_10());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_RemoveCommand_12=ruleRemoveCommand();

            	    state._fsp--;


            	    				current.merge(this_RemoveCommand_12);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 12 :
            	    // InternalMyDsl.g:435:4: this_BreakCommand_13= ruleBreakCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getBreakCommandParserRuleCall_2_11());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_BreakCommand_13=ruleBreakCommand();

            	    state._fsp--;


            	    				current.merge(this_BreakCommand_13);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 13 :
            	    // InternalMyDsl.g:446:4: this_SetloopCommand_14= ruleSetloopCommand
            	    {

            	    				newCompositeNode(grammarAccess.getInsertAfterCommandAccess().getSetloopCommandParserRuleCall_2_12());
            	    			
            	    pushFollow(FOLLOW_6);
            	    this_SetloopCommand_14=ruleSetloopCommand();

            	    state._fsp--;


            	    				current.merge(this_SetloopCommand_14);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            kw=(Token)match(input,15,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getInsertAfterCommandAccess().getEndinsertKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInsertAfterCommand"


    // $ANTLR start "entryRuleSetloopCommand"
    // InternalMyDsl.g:466:1: entryRuleSetloopCommand returns [String current=null] : iv_ruleSetloopCommand= ruleSetloopCommand EOF ;
    public final String entryRuleSetloopCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSetloopCommand = null;


        try {
            // InternalMyDsl.g:466:54: (iv_ruleSetloopCommand= ruleSetloopCommand EOF )
            // InternalMyDsl.g:467:2: iv_ruleSetloopCommand= ruleSetloopCommand EOF
            {
             newCompositeNode(grammarAccess.getSetloopCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSetloopCommand=ruleSetloopCommand();

            state._fsp--;

             current =iv_ruleSetloopCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSetloopCommand"


    // $ANTLR start "ruleSetloopCommand"
    // InternalMyDsl.g:473:1: ruleSetloopCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#setloop' this_EXPRESSION_1= RULE_EXPRESSION (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )* kw= '#endsetloop' ) ;
    public final AntlrDatatypeRuleToken ruleSetloopCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_3=null;


        	enterRule();

        try {
            // InternalMyDsl.g:479:2: ( (kw= '#setloop' this_EXPRESSION_1= RULE_EXPRESSION (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )* kw= '#endsetloop' ) )
            // InternalMyDsl.g:480:2: (kw= '#setloop' this_EXPRESSION_1= RULE_EXPRESSION (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )* kw= '#endsetloop' )
            {
            // InternalMyDsl.g:480:2: (kw= '#setloop' this_EXPRESSION_1= RULE_EXPRESSION (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )* kw= '#endsetloop' )
            // InternalMyDsl.g:481:3: kw= '#setloop' this_EXPRESSION_1= RULE_EXPRESSION (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )* kw= '#endsetloop'
            {
            kw=(Token)match(input,16,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getSetloopCommandAccess().getSetloopKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_7); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getSetloopCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:493:3: (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==17) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:494:4: kw= ',' this_EXPRESSION_3= RULE_EXPRESSION
            	    {
            	    kw=(Token)match(input,17,FOLLOW_4); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getSetloopCommandAccess().getCommaKeyword_2_0());
            	    			
            	    this_EXPRESSION_3=(Token)match(input,RULE_EXPRESSION,FOLLOW_7); 

            	    				current.merge(this_EXPRESSION_3);
            	    			

            	    				newLeafNode(this_EXPRESSION_3, grammarAccess.getSetloopCommandAccess().getEXPRESSIONTerminalRuleCall_2_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            kw=(Token)match(input,18,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getSetloopCommandAccess().getEndsetloopKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSetloopCommand"


    // $ANTLR start "entryRuleBreakCommand"
    // InternalMyDsl.g:516:1: entryRuleBreakCommand returns [String current=null] : iv_ruleBreakCommand= ruleBreakCommand EOF ;
    public final String entryRuleBreakCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleBreakCommand = null;


        try {
            // InternalMyDsl.g:516:52: (iv_ruleBreakCommand= ruleBreakCommand EOF )
            // InternalMyDsl.g:517:2: iv_ruleBreakCommand= ruleBreakCommand EOF
            {
             newCompositeNode(grammarAccess.getBreakCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBreakCommand=ruleBreakCommand();

            state._fsp--;

             current =iv_ruleBreakCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBreakCommand"


    // $ANTLR start "ruleBreakCommand"
    // InternalMyDsl.g:523:1: ruleBreakCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#break' this_EXPRESSION_1= RULE_EXPRESSION (this_AdaptCommand_2= ruleAdaptCommand | this_SetCommand_3= ruleSetCommand | this_SelectCommand_4= ruleSelectCommand | this_OutputCommand_5= ruleOutputCommand | this_OutfileCommand_6= ruleOutfileCommand | this_OutdirCommand_7= ruleOutdirCommand | this_MessageCommand_8= ruleMessageCommand | this_WhileCommand_9= ruleWhileCommand | this_RemoveCommand_10= ruleRemoveCommand | this_BreakCommand_11= ruleBreakCommand | this_SetloopCommand_12= ruleSetloopCommand )* kw= '#endbreak' ) ;
    public final AntlrDatatypeRuleToken ruleBreakCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        AntlrDatatypeRuleToken this_AdaptCommand_2 = null;

        AntlrDatatypeRuleToken this_SetCommand_3 = null;

        AntlrDatatypeRuleToken this_SelectCommand_4 = null;

        AntlrDatatypeRuleToken this_OutputCommand_5 = null;

        AntlrDatatypeRuleToken this_OutfileCommand_6 = null;

        AntlrDatatypeRuleToken this_OutdirCommand_7 = null;

        AntlrDatatypeRuleToken this_MessageCommand_8 = null;

        AntlrDatatypeRuleToken this_WhileCommand_9 = null;

        AntlrDatatypeRuleToken this_RemoveCommand_10 = null;

        AntlrDatatypeRuleToken this_BreakCommand_11 = null;

        AntlrDatatypeRuleToken this_SetloopCommand_12 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:529:2: ( (kw= '#break' this_EXPRESSION_1= RULE_EXPRESSION (this_AdaptCommand_2= ruleAdaptCommand | this_SetCommand_3= ruleSetCommand | this_SelectCommand_4= ruleSelectCommand | this_OutputCommand_5= ruleOutputCommand | this_OutfileCommand_6= ruleOutfileCommand | this_OutdirCommand_7= ruleOutdirCommand | this_MessageCommand_8= ruleMessageCommand | this_WhileCommand_9= ruleWhileCommand | this_RemoveCommand_10= ruleRemoveCommand | this_BreakCommand_11= ruleBreakCommand | this_SetloopCommand_12= ruleSetloopCommand )* kw= '#endbreak' ) )
            // InternalMyDsl.g:530:2: (kw= '#break' this_EXPRESSION_1= RULE_EXPRESSION (this_AdaptCommand_2= ruleAdaptCommand | this_SetCommand_3= ruleSetCommand | this_SelectCommand_4= ruleSelectCommand | this_OutputCommand_5= ruleOutputCommand | this_OutfileCommand_6= ruleOutfileCommand | this_OutdirCommand_7= ruleOutdirCommand | this_MessageCommand_8= ruleMessageCommand | this_WhileCommand_9= ruleWhileCommand | this_RemoveCommand_10= ruleRemoveCommand | this_BreakCommand_11= ruleBreakCommand | this_SetloopCommand_12= ruleSetloopCommand )* kw= '#endbreak' )
            {
            // InternalMyDsl.g:530:2: (kw= '#break' this_EXPRESSION_1= RULE_EXPRESSION (this_AdaptCommand_2= ruleAdaptCommand | this_SetCommand_3= ruleSetCommand | this_SelectCommand_4= ruleSelectCommand | this_OutputCommand_5= ruleOutputCommand | this_OutfileCommand_6= ruleOutfileCommand | this_OutdirCommand_7= ruleOutdirCommand | this_MessageCommand_8= ruleMessageCommand | this_WhileCommand_9= ruleWhileCommand | this_RemoveCommand_10= ruleRemoveCommand | this_BreakCommand_11= ruleBreakCommand | this_SetloopCommand_12= ruleSetloopCommand )* kw= '#endbreak' )
            // InternalMyDsl.g:531:3: kw= '#break' this_EXPRESSION_1= RULE_EXPRESSION (this_AdaptCommand_2= ruleAdaptCommand | this_SetCommand_3= ruleSetCommand | this_SelectCommand_4= ruleSelectCommand | this_OutputCommand_5= ruleOutputCommand | this_OutfileCommand_6= ruleOutfileCommand | this_OutdirCommand_7= ruleOutdirCommand | this_MessageCommand_8= ruleMessageCommand | this_WhileCommand_9= ruleWhileCommand | this_RemoveCommand_10= ruleRemoveCommand | this_BreakCommand_11= ruleBreakCommand | this_SetloopCommand_12= ruleSetloopCommand )* kw= '#endbreak'
            {
            kw=(Token)match(input,19,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getBreakCommandAccess().getBreakKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_8); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getBreakCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:543:3: (this_AdaptCommand_2= ruleAdaptCommand | this_SetCommand_3= ruleSetCommand | this_SelectCommand_4= ruleSelectCommand | this_OutputCommand_5= ruleOutputCommand | this_OutfileCommand_6= ruleOutfileCommand | this_OutdirCommand_7= ruleOutdirCommand | this_MessageCommand_8= ruleMessageCommand | this_WhileCommand_9= ruleWhileCommand | this_RemoveCommand_10= ruleRemoveCommand | this_BreakCommand_11= ruleBreakCommand | this_SetloopCommand_12= ruleSetloopCommand )*
            loop6:
            do {
                int alt6=12;
                switch ( input.LA(1) ) {
                case 12:
                    {
                    alt6=1;
                    }
                    break;
                case 30:
                    {
                    alt6=2;
                    }
                    break;
                case 24:
                    {
                    alt6=3;
                    }
                    break;
                case 26:
                    {
                    alt6=4;
                    }
                    break;
                case 32:
                    {
                    alt6=5;
                    }
                    break;
                case 31:
                    {
                    alt6=6;
                    }
                    break;
                case 34:
                    {
                    alt6=7;
                    }
                    break;
                case 27:
                    {
                    alt6=8;
                    }
                    break;
                case 33:
                    {
                    alt6=9;
                    }
                    break;
                case 19:
                    {
                    alt6=10;
                    }
                    break;
                case 16:
                    {
                    alt6=11;
                    }
                    break;

                }

                switch (alt6) {
            	case 1 :
            	    // InternalMyDsl.g:544:4: this_AdaptCommand_2= ruleAdaptCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getAdaptCommandParserRuleCall_2_0());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_AdaptCommand_2=ruleAdaptCommand();

            	    state._fsp--;


            	    				current.merge(this_AdaptCommand_2);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:555:4: this_SetCommand_3= ruleSetCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getSetCommandParserRuleCall_2_1());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_SetCommand_3=ruleSetCommand();

            	    state._fsp--;


            	    				current.merge(this_SetCommand_3);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalMyDsl.g:566:4: this_SelectCommand_4= ruleSelectCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getSelectCommandParserRuleCall_2_2());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_SelectCommand_4=ruleSelectCommand();

            	    state._fsp--;


            	    				current.merge(this_SelectCommand_4);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalMyDsl.g:577:4: this_OutputCommand_5= ruleOutputCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getOutputCommandParserRuleCall_2_3());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_OutputCommand_5=ruleOutputCommand();

            	    state._fsp--;


            	    				current.merge(this_OutputCommand_5);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 5 :
            	    // InternalMyDsl.g:588:4: this_OutfileCommand_6= ruleOutfileCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getOutfileCommandParserRuleCall_2_4());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_OutfileCommand_6=ruleOutfileCommand();

            	    state._fsp--;


            	    				current.merge(this_OutfileCommand_6);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 6 :
            	    // InternalMyDsl.g:599:4: this_OutdirCommand_7= ruleOutdirCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getOutdirCommandParserRuleCall_2_5());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_OutdirCommand_7=ruleOutdirCommand();

            	    state._fsp--;


            	    				current.merge(this_OutdirCommand_7);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 7 :
            	    // InternalMyDsl.g:610:4: this_MessageCommand_8= ruleMessageCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getMessageCommandParserRuleCall_2_6());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_MessageCommand_8=ruleMessageCommand();

            	    state._fsp--;


            	    				current.merge(this_MessageCommand_8);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 8 :
            	    // InternalMyDsl.g:621:4: this_WhileCommand_9= ruleWhileCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getWhileCommandParserRuleCall_2_7());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_WhileCommand_9=ruleWhileCommand();

            	    state._fsp--;


            	    				current.merge(this_WhileCommand_9);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 9 :
            	    // InternalMyDsl.g:632:4: this_RemoveCommand_10= ruleRemoveCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getRemoveCommandParserRuleCall_2_8());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_RemoveCommand_10=ruleRemoveCommand();

            	    state._fsp--;


            	    				current.merge(this_RemoveCommand_10);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 10 :
            	    // InternalMyDsl.g:643:4: this_BreakCommand_11= ruleBreakCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getBreakCommandParserRuleCall_2_9());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_BreakCommand_11=ruleBreakCommand();

            	    state._fsp--;


            	    				current.merge(this_BreakCommand_11);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 11 :
            	    // InternalMyDsl.g:654:4: this_SetloopCommand_12= ruleSetloopCommand
            	    {

            	    				newCompositeNode(grammarAccess.getBreakCommandAccess().getSetloopCommandParserRuleCall_2_10());
            	    			
            	    pushFollow(FOLLOW_8);
            	    this_SetloopCommand_12=ruleSetloopCommand();

            	    state._fsp--;


            	    				current.merge(this_SetloopCommand_12);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

            kw=(Token)match(input,20,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getBreakCommandAccess().getEndbreakKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBreakCommand"


    // $ANTLR start "entryRuleOptionCommand"
    // InternalMyDsl.g:674:1: entryRuleOptionCommand returns [String current=null] : iv_ruleOptionCommand= ruleOptionCommand EOF ;
    public final String entryRuleOptionCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOptionCommand = null;


        try {
            // InternalMyDsl.g:674:53: (iv_ruleOptionCommand= ruleOptionCommand EOF )
            // InternalMyDsl.g:675:2: iv_ruleOptionCommand= ruleOptionCommand EOF
            {
             newCompositeNode(grammarAccess.getOptionCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOptionCommand=ruleOptionCommand();

            state._fsp--;

             current =iv_ruleOptionCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOptionCommand"


    // $ANTLR start "ruleOptionCommand"
    // InternalMyDsl.g:681:1: ruleOptionCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#option' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_InsertAfterCommand_4= ruleInsertAfterCommand | kw= '=' )* kw= '#endoption' ) ;
    public final AntlrDatatypeRuleToken ruleOptionCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_2=null;
        Token this_WS_3=null;
        AntlrDatatypeRuleToken this_InsertAfterCommand_4 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:687:2: ( (kw= '#option' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_InsertAfterCommand_4= ruleInsertAfterCommand | kw= '=' )* kw= '#endoption' ) )
            // InternalMyDsl.g:688:2: (kw= '#option' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_InsertAfterCommand_4= ruleInsertAfterCommand | kw= '=' )* kw= '#endoption' )
            {
            // InternalMyDsl.g:688:2: (kw= '#option' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_InsertAfterCommand_4= ruleInsertAfterCommand | kw= '=' )* kw= '#endoption' )
            // InternalMyDsl.g:689:3: kw= '#option' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_InsertAfterCommand_4= ruleInsertAfterCommand | kw= '=' )* kw= '#endoption'
            {
            kw=(Token)match(input,21,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getOptionCommandAccess().getOptionKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_9); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getOptionCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:701:3: (this_EXPRESSION_2= RULE_EXPRESSION | this_WS_3= RULE_WS | this_InsertAfterCommand_4= ruleInsertAfterCommand | kw= '=' )*
            loop7:
            do {
                int alt7=5;
                switch ( input.LA(1) ) {
                case RULE_EXPRESSION:
                    {
                    alt7=1;
                    }
                    break;
                case RULE_WS:
                    {
                    alt7=2;
                    }
                    break;
                case 14:
                    {
                    alt7=3;
                    }
                    break;
                case 22:
                    {
                    alt7=4;
                    }
                    break;

                }

                switch (alt7) {
            	case 1 :
            	    // InternalMyDsl.g:702:4: this_EXPRESSION_2= RULE_EXPRESSION
            	    {
            	    this_EXPRESSION_2=(Token)match(input,RULE_EXPRESSION,FOLLOW_9); 

            	    				current.merge(this_EXPRESSION_2);
            	    			

            	    				newLeafNode(this_EXPRESSION_2, grammarAccess.getOptionCommandAccess().getEXPRESSIONTerminalRuleCall_2_0());
            	    			

            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:710:4: this_WS_3= RULE_WS
            	    {
            	    this_WS_3=(Token)match(input,RULE_WS,FOLLOW_9); 

            	    				current.merge(this_WS_3);
            	    			

            	    				newLeafNode(this_WS_3, grammarAccess.getOptionCommandAccess().getWSTerminalRuleCall_2_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalMyDsl.g:718:4: this_InsertAfterCommand_4= ruleInsertAfterCommand
            	    {

            	    				newCompositeNode(grammarAccess.getOptionCommandAccess().getInsertAfterCommandParserRuleCall_2_2());
            	    			
            	    pushFollow(FOLLOW_9);
            	    this_InsertAfterCommand_4=ruleInsertAfterCommand();

            	    state._fsp--;


            	    				current.merge(this_InsertAfterCommand_4);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalMyDsl.g:729:4: kw= '='
            	    {
            	    kw=(Token)match(input,22,FOLLOW_9); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getOptionCommandAccess().getEqualsSignKeyword_2_3());
            	    			

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            kw=(Token)match(input,23,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getOptionCommandAccess().getEndoptionKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOptionCommand"


    // $ANTLR start "entryRuleSelectCommand"
    // InternalMyDsl.g:744:1: entryRuleSelectCommand returns [String current=null] : iv_ruleSelectCommand= ruleSelectCommand EOF ;
    public final String entryRuleSelectCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSelectCommand = null;


        try {
            // InternalMyDsl.g:744:53: (iv_ruleSelectCommand= ruleSelectCommand EOF )
            // InternalMyDsl.g:745:2: iv_ruleSelectCommand= ruleSelectCommand EOF
            {
             newCompositeNode(grammarAccess.getSelectCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSelectCommand=ruleSelectCommand();

            state._fsp--;

             current =iv_ruleSelectCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSelectCommand"


    // $ANTLR start "ruleSelectCommand"
    // InternalMyDsl.g:751:1: ruleSelectCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#select' this_EXPRESSION_1= RULE_EXPRESSION (this_OptionCommand_2= ruleOptionCommand )* kw= '#endselect' ) ;
    public final AntlrDatatypeRuleToken ruleSelectCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        AntlrDatatypeRuleToken this_OptionCommand_2 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:757:2: ( (kw= '#select' this_EXPRESSION_1= RULE_EXPRESSION (this_OptionCommand_2= ruleOptionCommand )* kw= '#endselect' ) )
            // InternalMyDsl.g:758:2: (kw= '#select' this_EXPRESSION_1= RULE_EXPRESSION (this_OptionCommand_2= ruleOptionCommand )* kw= '#endselect' )
            {
            // InternalMyDsl.g:758:2: (kw= '#select' this_EXPRESSION_1= RULE_EXPRESSION (this_OptionCommand_2= ruleOptionCommand )* kw= '#endselect' )
            // InternalMyDsl.g:759:3: kw= '#select' this_EXPRESSION_1= RULE_EXPRESSION (this_OptionCommand_2= ruleOptionCommand )* kw= '#endselect'
            {
            kw=(Token)match(input,24,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getSelectCommandAccess().getSelectKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_10); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getSelectCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:771:3: (this_OptionCommand_2= ruleOptionCommand )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==21) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:772:4: this_OptionCommand_2= ruleOptionCommand
            	    {

            	    				newCompositeNode(grammarAccess.getSelectCommandAccess().getOptionCommandParserRuleCall_2());
            	    			
            	    pushFollow(FOLLOW_10);
            	    this_OptionCommand_2=ruleOptionCommand();

            	    state._fsp--;


            	    				current.merge(this_OptionCommand_2);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            kw=(Token)match(input,25,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getSelectCommandAccess().getEndselectKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSelectCommand"


    // $ANTLR start "entryRuleOutputCommand"
    // InternalMyDsl.g:792:1: entryRuleOutputCommand returns [String current=null] : iv_ruleOutputCommand= ruleOutputCommand EOF ;
    public final String entryRuleOutputCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOutputCommand = null;


        try {
            // InternalMyDsl.g:792:53: (iv_ruleOutputCommand= ruleOutputCommand EOF )
            // InternalMyDsl.g:793:2: iv_ruleOutputCommand= ruleOutputCommand EOF
            {
             newCompositeNode(grammarAccess.getOutputCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutputCommand=ruleOutputCommand();

            state._fsp--;

             current =iv_ruleOutputCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputCommand"


    // $ANTLR start "ruleOutputCommand"
    // InternalMyDsl.g:799:1: ruleOutputCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#output' this_EXPRESSION_1= RULE_EXPRESSION (this_WS_2= RULE_WS )* ) ;
    public final AntlrDatatypeRuleToken ruleOutputCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_WS_2=null;


        	enterRule();

        try {
            // InternalMyDsl.g:805:2: ( (kw= '#output' this_EXPRESSION_1= RULE_EXPRESSION (this_WS_2= RULE_WS )* ) )
            // InternalMyDsl.g:806:2: (kw= '#output' this_EXPRESSION_1= RULE_EXPRESSION (this_WS_2= RULE_WS )* )
            {
            // InternalMyDsl.g:806:2: (kw= '#output' this_EXPRESSION_1= RULE_EXPRESSION (this_WS_2= RULE_WS )* )
            // InternalMyDsl.g:807:3: kw= '#output' this_EXPRESSION_1= RULE_EXPRESSION (this_WS_2= RULE_WS )*
            {
            kw=(Token)match(input,26,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getOutputCommandAccess().getOutputKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_11); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getOutputCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:819:3: (this_WS_2= RULE_WS )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_WS) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:820:4: this_WS_2= RULE_WS
            	    {
            	    this_WS_2=(Token)match(input,RULE_WS,FOLLOW_11); 

            	    				current.merge(this_WS_2);
            	    			

            	    				newLeafNode(this_WS_2, grammarAccess.getOutputCommandAccess().getWSTerminalRuleCall_2());
            	    			

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputCommand"


    // $ANTLR start "entryRuleWhileCommand"
    // InternalMyDsl.g:832:1: entryRuleWhileCommand returns [String current=null] : iv_ruleWhileCommand= ruleWhileCommand EOF ;
    public final String entryRuleWhileCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleWhileCommand = null;


        try {
            // InternalMyDsl.g:832:52: (iv_ruleWhileCommand= ruleWhileCommand EOF )
            // InternalMyDsl.g:833:2: iv_ruleWhileCommand= ruleWhileCommand EOF
            {
             newCompositeNode(grammarAccess.getWhileCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWhileCommand=ruleWhileCommand();

            state._fsp--;

             current =iv_ruleWhileCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhileCommand"


    // $ANTLR start "ruleWhileCommand"
    // InternalMyDsl.g:839:1: ruleWhileCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#while' this_EXPRESSION_1= RULE_EXPRESSION ( (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION ) | this_WS_4= RULE_WS | this_OptionCommand_5= ruleOptionCommand | this_OutputCommand_6= ruleOutputCommand | this_SelectCommand_7= ruleSelectCommand | this_AdaptCommand_8= ruleAdaptCommand )* kw= '#endwhile' ) ;
    public final AntlrDatatypeRuleToken ruleWhileCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_3=null;
        Token this_WS_4=null;
        AntlrDatatypeRuleToken this_OptionCommand_5 = null;

        AntlrDatatypeRuleToken this_OutputCommand_6 = null;

        AntlrDatatypeRuleToken this_SelectCommand_7 = null;

        AntlrDatatypeRuleToken this_AdaptCommand_8 = null;



        	enterRule();

        try {
            // InternalMyDsl.g:845:2: ( (kw= '#while' this_EXPRESSION_1= RULE_EXPRESSION ( (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION ) | this_WS_4= RULE_WS | this_OptionCommand_5= ruleOptionCommand | this_OutputCommand_6= ruleOutputCommand | this_SelectCommand_7= ruleSelectCommand | this_AdaptCommand_8= ruleAdaptCommand )* kw= '#endwhile' ) )
            // InternalMyDsl.g:846:2: (kw= '#while' this_EXPRESSION_1= RULE_EXPRESSION ( (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION ) | this_WS_4= RULE_WS | this_OptionCommand_5= ruleOptionCommand | this_OutputCommand_6= ruleOutputCommand | this_SelectCommand_7= ruleSelectCommand | this_AdaptCommand_8= ruleAdaptCommand )* kw= '#endwhile' )
            {
            // InternalMyDsl.g:846:2: (kw= '#while' this_EXPRESSION_1= RULE_EXPRESSION ( (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION ) | this_WS_4= RULE_WS | this_OptionCommand_5= ruleOptionCommand | this_OutputCommand_6= ruleOutputCommand | this_SelectCommand_7= ruleSelectCommand | this_AdaptCommand_8= ruleAdaptCommand )* kw= '#endwhile' )
            // InternalMyDsl.g:847:3: kw= '#while' this_EXPRESSION_1= RULE_EXPRESSION ( (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION ) | this_WS_4= RULE_WS | this_OptionCommand_5= ruleOptionCommand | this_OutputCommand_6= ruleOutputCommand | this_SelectCommand_7= ruleSelectCommand | this_AdaptCommand_8= ruleAdaptCommand )* kw= '#endwhile'
            {
            kw=(Token)match(input,27,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getWhileCommandAccess().getWhileKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_12); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getWhileCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:859:3: ( (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION ) | this_WS_4= RULE_WS | this_OptionCommand_5= ruleOptionCommand | this_OutputCommand_6= ruleOutputCommand | this_SelectCommand_7= ruleSelectCommand | this_AdaptCommand_8= ruleAdaptCommand )*
            loop10:
            do {
                int alt10=7;
                switch ( input.LA(1) ) {
                case 17:
                    {
                    alt10=1;
                    }
                    break;
                case RULE_WS:
                    {
                    alt10=2;
                    }
                    break;
                case 21:
                    {
                    alt10=3;
                    }
                    break;
                case 26:
                    {
                    alt10=4;
                    }
                    break;
                case 24:
                    {
                    alt10=5;
                    }
                    break;
                case 12:
                    {
                    alt10=6;
                    }
                    break;

                }

                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:860:4: (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )
            	    {
            	    // InternalMyDsl.g:860:4: (kw= ',' this_EXPRESSION_3= RULE_EXPRESSION )
            	    // InternalMyDsl.g:861:5: kw= ',' this_EXPRESSION_3= RULE_EXPRESSION
            	    {
            	    kw=(Token)match(input,17,FOLLOW_4); 

            	    					current.merge(kw);
            	    					newLeafNode(kw, grammarAccess.getWhileCommandAccess().getCommaKeyword_2_0_0());
            	    				
            	    this_EXPRESSION_3=(Token)match(input,RULE_EXPRESSION,FOLLOW_12); 

            	    					current.merge(this_EXPRESSION_3);
            	    				

            	    					newLeafNode(this_EXPRESSION_3, grammarAccess.getWhileCommandAccess().getEXPRESSIONTerminalRuleCall_2_0_1());
            	    				

            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalMyDsl.g:875:4: this_WS_4= RULE_WS
            	    {
            	    this_WS_4=(Token)match(input,RULE_WS,FOLLOW_12); 

            	    				current.merge(this_WS_4);
            	    			

            	    				newLeafNode(this_WS_4, grammarAccess.getWhileCommandAccess().getWSTerminalRuleCall_2_1());
            	    			

            	    }
            	    break;
            	case 3 :
            	    // InternalMyDsl.g:883:4: this_OptionCommand_5= ruleOptionCommand
            	    {

            	    				newCompositeNode(grammarAccess.getWhileCommandAccess().getOptionCommandParserRuleCall_2_2());
            	    			
            	    pushFollow(FOLLOW_12);
            	    this_OptionCommand_5=ruleOptionCommand();

            	    state._fsp--;


            	    				current.merge(this_OptionCommand_5);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 4 :
            	    // InternalMyDsl.g:894:4: this_OutputCommand_6= ruleOutputCommand
            	    {

            	    				newCompositeNode(grammarAccess.getWhileCommandAccess().getOutputCommandParserRuleCall_2_3());
            	    			
            	    pushFollow(FOLLOW_12);
            	    this_OutputCommand_6=ruleOutputCommand();

            	    state._fsp--;


            	    				current.merge(this_OutputCommand_6);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 5 :
            	    // InternalMyDsl.g:905:4: this_SelectCommand_7= ruleSelectCommand
            	    {

            	    				newCompositeNode(grammarAccess.getWhileCommandAccess().getSelectCommandParserRuleCall_2_4());
            	    			
            	    pushFollow(FOLLOW_12);
            	    this_SelectCommand_7=ruleSelectCommand();

            	    state._fsp--;


            	    				current.merge(this_SelectCommand_7);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;
            	case 6 :
            	    // InternalMyDsl.g:916:4: this_AdaptCommand_8= ruleAdaptCommand
            	    {

            	    				newCompositeNode(grammarAccess.getWhileCommandAccess().getAdaptCommandParserRuleCall_2_5());
            	    			
            	    pushFollow(FOLLOW_12);
            	    this_AdaptCommand_8=ruleAdaptCommand();

            	    state._fsp--;


            	    				current.merge(this_AdaptCommand_8);
            	    			

            	    				afterParserOrEnumRuleCall();
            	    			

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            kw=(Token)match(input,28,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getWhileCommandAccess().getEndwhileKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhileCommand"


    // $ANTLR start "entryRuleCommentCommand"
    // InternalMyDsl.g:936:1: entryRuleCommentCommand returns [String current=null] : iv_ruleCommentCommand= ruleCommentCommand EOF ;
    public final String entryRuleCommentCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleCommentCommand = null;


        try {
            // InternalMyDsl.g:936:54: (iv_ruleCommentCommand= ruleCommentCommand EOF )
            // InternalMyDsl.g:937:2: iv_ruleCommentCommand= ruleCommentCommand EOF
            {
             newCompositeNode(grammarAccess.getCommentCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleCommentCommand=ruleCommentCommand();

            state._fsp--;

             current =iv_ruleCommentCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCommentCommand"


    // $ANTLR start "ruleCommentCommand"
    // InternalMyDsl.g:943:1: ruleCommentCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '%' (this_EXPRESSION_1= RULE_EXPRESSION )* ) ;
    public final AntlrDatatypeRuleToken ruleCommentCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:949:2: ( (kw= '%' (this_EXPRESSION_1= RULE_EXPRESSION )* ) )
            // InternalMyDsl.g:950:2: (kw= '%' (this_EXPRESSION_1= RULE_EXPRESSION )* )
            {
            // InternalMyDsl.g:950:2: (kw= '%' (this_EXPRESSION_1= RULE_EXPRESSION )* )
            // InternalMyDsl.g:951:3: kw= '%' (this_EXPRESSION_1= RULE_EXPRESSION )*
            {
            kw=(Token)match(input,29,FOLLOW_13); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getCommentCommandAccess().getPercentSignKeyword_0());
            		
            // InternalMyDsl.g:956:3: (this_EXPRESSION_1= RULE_EXPRESSION )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_EXPRESSION) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMyDsl.g:957:4: this_EXPRESSION_1= RULE_EXPRESSION
            	    {
            	    this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_13); 

            	    				current.merge(this_EXPRESSION_1);
            	    			

            	    				newLeafNode(this_EXPRESSION_1, grammarAccess.getCommentCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCommentCommand"


    // $ANTLR start "entryRuleSetCommand"
    // InternalMyDsl.g:969:1: entryRuleSetCommand returns [String current=null] : iv_ruleSetCommand= ruleSetCommand EOF ;
    public final String entryRuleSetCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleSetCommand = null;


        try {
            // InternalMyDsl.g:969:50: (iv_ruleSetCommand= ruleSetCommand EOF )
            // InternalMyDsl.g:970:2: iv_ruleSetCommand= ruleSetCommand EOF
            {
             newCompositeNode(grammarAccess.getSetCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSetCommand=ruleSetCommand();

            state._fsp--;

             current =iv_ruleSetCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSetCommand"


    // $ANTLR start "ruleSetCommand"
    // InternalMyDsl.g:976:1: ruleSetCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#set' this_EXPRESSION_1= RULE_EXPRESSION kw= '=' (this_EXPRESSION_3= RULE_EXPRESSION )? (kw= ',' this_EXPRESSION_5= RULE_EXPRESSION )* ) ;
    public final AntlrDatatypeRuleToken ruleSetCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_3=null;
        Token this_EXPRESSION_5=null;


        	enterRule();

        try {
            // InternalMyDsl.g:982:2: ( (kw= '#set' this_EXPRESSION_1= RULE_EXPRESSION kw= '=' (this_EXPRESSION_3= RULE_EXPRESSION )? (kw= ',' this_EXPRESSION_5= RULE_EXPRESSION )* ) )
            // InternalMyDsl.g:983:2: (kw= '#set' this_EXPRESSION_1= RULE_EXPRESSION kw= '=' (this_EXPRESSION_3= RULE_EXPRESSION )? (kw= ',' this_EXPRESSION_5= RULE_EXPRESSION )* )
            {
            // InternalMyDsl.g:983:2: (kw= '#set' this_EXPRESSION_1= RULE_EXPRESSION kw= '=' (this_EXPRESSION_3= RULE_EXPRESSION )? (kw= ',' this_EXPRESSION_5= RULE_EXPRESSION )* )
            // InternalMyDsl.g:984:3: kw= '#set' this_EXPRESSION_1= RULE_EXPRESSION kw= '=' (this_EXPRESSION_3= RULE_EXPRESSION )? (kw= ',' this_EXPRESSION_5= RULE_EXPRESSION )*
            {
            kw=(Token)match(input,30,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getSetCommandAccess().getSetKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_14); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            kw=(Token)match(input,22,FOLLOW_15); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getSetCommandAccess().getEqualsSignKeyword_2());
            		
            // InternalMyDsl.g:1001:3: (this_EXPRESSION_3= RULE_EXPRESSION )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==RULE_EXPRESSION) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalMyDsl.g:1002:4: this_EXPRESSION_3= RULE_EXPRESSION
                    {
                    this_EXPRESSION_3=(Token)match(input,RULE_EXPRESSION,FOLLOW_16); 

                    				current.merge(this_EXPRESSION_3);
                    			

                    				newLeafNode(this_EXPRESSION_3, grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_3());
                    			

                    }
                    break;

            }

            // InternalMyDsl.g:1010:3: (kw= ',' this_EXPRESSION_5= RULE_EXPRESSION )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==17) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalMyDsl.g:1011:4: kw= ',' this_EXPRESSION_5= RULE_EXPRESSION
            	    {
            	    kw=(Token)match(input,17,FOLLOW_4); 

            	    				current.merge(kw);
            	    				newLeafNode(kw, grammarAccess.getSetCommandAccess().getCommaKeyword_4_0());
            	    			
            	    this_EXPRESSION_5=(Token)match(input,RULE_EXPRESSION,FOLLOW_16); 

            	    				current.merge(this_EXPRESSION_5);
            	    			

            	    				newLeafNode(this_EXPRESSION_5, grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_4_1());
            	    			

            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSetCommand"


    // $ANTLR start "entryRuleOutdirCommand"
    // InternalMyDsl.g:1028:1: entryRuleOutdirCommand returns [String current=null] : iv_ruleOutdirCommand= ruleOutdirCommand EOF ;
    public final String entryRuleOutdirCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOutdirCommand = null;


        try {
            // InternalMyDsl.g:1028:53: (iv_ruleOutdirCommand= ruleOutdirCommand EOF )
            // InternalMyDsl.g:1029:2: iv_ruleOutdirCommand= ruleOutdirCommand EOF
            {
             newCompositeNode(grammarAccess.getOutdirCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutdirCommand=ruleOutdirCommand();

            state._fsp--;

             current =iv_ruleOutdirCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutdirCommand"


    // $ANTLR start "ruleOutdirCommand"
    // InternalMyDsl.g:1035:1: ruleOutdirCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#outdir' this_EXPRESSION_1= RULE_EXPRESSION ) ;
    public final AntlrDatatypeRuleToken ruleOutdirCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:1041:2: ( (kw= '#outdir' this_EXPRESSION_1= RULE_EXPRESSION ) )
            // InternalMyDsl.g:1042:2: (kw= '#outdir' this_EXPRESSION_1= RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1042:2: (kw= '#outdir' this_EXPRESSION_1= RULE_EXPRESSION )
            // InternalMyDsl.g:1043:3: kw= '#outdir' this_EXPRESSION_1= RULE_EXPRESSION
            {
            kw=(Token)match(input,31,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getOutdirCommandAccess().getOutdirKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_2); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getOutdirCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutdirCommand"


    // $ANTLR start "entryRuleOutfileCommand"
    // InternalMyDsl.g:1059:1: entryRuleOutfileCommand returns [String current=null] : iv_ruleOutfileCommand= ruleOutfileCommand EOF ;
    public final String entryRuleOutfileCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOutfileCommand = null;


        try {
            // InternalMyDsl.g:1059:54: (iv_ruleOutfileCommand= ruleOutfileCommand EOF )
            // InternalMyDsl.g:1060:2: iv_ruleOutfileCommand= ruleOutfileCommand EOF
            {
             newCompositeNode(grammarAccess.getOutfileCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutfileCommand=ruleOutfileCommand();

            state._fsp--;

             current =iv_ruleOutfileCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutfileCommand"


    // $ANTLR start "ruleOutfileCommand"
    // InternalMyDsl.g:1066:1: ruleOutfileCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#outfile' this_EXPRESSION_1= RULE_EXPRESSION ) ;
    public final AntlrDatatypeRuleToken ruleOutfileCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:1072:2: ( (kw= '#outfile' this_EXPRESSION_1= RULE_EXPRESSION ) )
            // InternalMyDsl.g:1073:2: (kw= '#outfile' this_EXPRESSION_1= RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1073:2: (kw= '#outfile' this_EXPRESSION_1= RULE_EXPRESSION )
            // InternalMyDsl.g:1074:3: kw= '#outfile' this_EXPRESSION_1= RULE_EXPRESSION
            {
            kw=(Token)match(input,32,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getOutfileCommandAccess().getOutfileKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_2); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getOutfileCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutfileCommand"


    // $ANTLR start "entryRuleRemoveCommand"
    // InternalMyDsl.g:1090:1: entryRuleRemoveCommand returns [String current=null] : iv_ruleRemoveCommand= ruleRemoveCommand EOF ;
    public final String entryRuleRemoveCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRemoveCommand = null;


        try {
            // InternalMyDsl.g:1090:53: (iv_ruleRemoveCommand= ruleRemoveCommand EOF )
            // InternalMyDsl.g:1091:2: iv_ruleRemoveCommand= ruleRemoveCommand EOF
            {
             newCompositeNode(grammarAccess.getRemoveCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRemoveCommand=ruleRemoveCommand();

            state._fsp--;

             current =iv_ruleRemoveCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRemoveCommand"


    // $ANTLR start "ruleRemoveCommand"
    // InternalMyDsl.g:1097:1: ruleRemoveCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#remove' this_EXPRESSION_1= RULE_EXPRESSION ) ;
    public final AntlrDatatypeRuleToken ruleRemoveCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;


        	enterRule();

        try {
            // InternalMyDsl.g:1103:2: ( (kw= '#remove' this_EXPRESSION_1= RULE_EXPRESSION ) )
            // InternalMyDsl.g:1104:2: (kw= '#remove' this_EXPRESSION_1= RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1104:2: (kw= '#remove' this_EXPRESSION_1= RULE_EXPRESSION )
            // InternalMyDsl.g:1105:3: kw= '#remove' this_EXPRESSION_1= RULE_EXPRESSION
            {
            kw=(Token)match(input,33,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getRemoveCommandAccess().getRemoveKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_2); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getRemoveCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRemoveCommand"


    // $ANTLR start "entryRuleMessageCommand"
    // InternalMyDsl.g:1121:1: entryRuleMessageCommand returns [String current=null] : iv_ruleMessageCommand= ruleMessageCommand EOF ;
    public final String entryRuleMessageCommand() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMessageCommand = null;


        try {
            // InternalMyDsl.g:1121:54: (iv_ruleMessageCommand= ruleMessageCommand EOF )
            // InternalMyDsl.g:1122:2: iv_ruleMessageCommand= ruleMessageCommand EOF
            {
             newCompositeNode(grammarAccess.getMessageCommandRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMessageCommand=ruleMessageCommand();

            state._fsp--;

             current =iv_ruleMessageCommand.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMessageCommand"


    // $ANTLR start "ruleMessageCommand"
    // InternalMyDsl.g:1128:1: ruleMessageCommand returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '#message' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION )* kw= '#endmessage' ) ;
    public final AntlrDatatypeRuleToken ruleMessageCommand() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_EXPRESSION_1=null;
        Token this_EXPRESSION_2=null;


        	enterRule();

        try {
            // InternalMyDsl.g:1134:2: ( (kw= '#message' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION )* kw= '#endmessage' ) )
            // InternalMyDsl.g:1135:2: (kw= '#message' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION )* kw= '#endmessage' )
            {
            // InternalMyDsl.g:1135:2: (kw= '#message' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION )* kw= '#endmessage' )
            // InternalMyDsl.g:1136:3: kw= '#message' this_EXPRESSION_1= RULE_EXPRESSION (this_EXPRESSION_2= RULE_EXPRESSION )* kw= '#endmessage'
            {
            kw=(Token)match(input,34,FOLLOW_4); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getMessageCommandAccess().getMessageKeyword_0());
            		
            this_EXPRESSION_1=(Token)match(input,RULE_EXPRESSION,FOLLOW_17); 

            			current.merge(this_EXPRESSION_1);
            		

            			newLeafNode(this_EXPRESSION_1, grammarAccess.getMessageCommandAccess().getEXPRESSIONTerminalRuleCall_1());
            		
            // InternalMyDsl.g:1148:3: (this_EXPRESSION_2= RULE_EXPRESSION )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_EXPRESSION) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalMyDsl.g:1149:4: this_EXPRESSION_2= RULE_EXPRESSION
            	    {
            	    this_EXPRESSION_2=(Token)match(input,RULE_EXPRESSION,FOLLOW_17); 

            	    				current.merge(this_EXPRESSION_2);
            	    			

            	    				newLeafNode(this_EXPRESSION_2, grammarAccess.getMessageCommandAccess().getEXPRESSIONTerminalRuleCall_2());
            	    			

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            kw=(Token)match(input,35,FOLLOW_2); 

            			current.merge(kw);
            			newLeafNode(kw, grammarAccess.getMessageCommandAccess().getEndmessageKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMessageCommand"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000006D205002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000001206030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x00000007ED29D030L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000007ED39D030L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000C04030L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000002200000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x000000007D225020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000020012L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000020002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000800000010L});

}