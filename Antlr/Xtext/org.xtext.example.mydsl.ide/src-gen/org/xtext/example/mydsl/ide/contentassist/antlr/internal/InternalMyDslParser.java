package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_EXPRESSION", "RULE_WS", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_ANY_OTHER", "'='", "'#adapt:'", "'#endadapt'", "'#insert-after'", "'#endinsert'", "'#setloop'", "'#endsetloop'", "','", "'#break'", "'#endbreak'", "'#option'", "'#endoption'", "'#select'", "'#endselect'", "'#output'", "'#while'", "'#endwhile'", "'%'", "'#set'", "'#outdir'", "'#outfile'", "'#remove'", "'#message'", "'#endmessage'"
    };
    public static final int RULE_STRING=8;
    public static final int RULE_EXPRESSION=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=6;
    public static final int RULE_WS=5;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalMyDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslParser.tokenNames; }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }


    	private MyDslGrammarAccess grammarAccess;

    	public void setGrammarAccess(MyDslGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleModel"
    // InternalMyDsl.g:53:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // InternalMyDsl.g:54:1: ( ruleModel EOF )
            // InternalMyDsl.g:55:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_1);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalMyDsl.g:62:1: ruleModel : ( ( rule__Model__CommandsAssignment )* ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:66:2: ( ( ( rule__Model__CommandsAssignment )* ) )
            // InternalMyDsl.g:67:2: ( ( rule__Model__CommandsAssignment )* )
            {
            // InternalMyDsl.g:67:2: ( ( rule__Model__CommandsAssignment )* )
            // InternalMyDsl.g:68:3: ( rule__Model__CommandsAssignment )*
            {
             before(grammarAccess.getModelAccess().getCommandsAssignment()); 
            // InternalMyDsl.g:69:3: ( rule__Model__CommandsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==13||LA1_0==15||LA1_0==22||LA1_0==24||(LA1_0>=26 && LA1_0<=27)||(LA1_0>=29 && LA1_0<=30)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:69:4: rule__Model__CommandsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Model__CommandsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getCommandsAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleCommands"
    // InternalMyDsl.g:78:1: entryRuleCommands : ruleCommands EOF ;
    public final void entryRuleCommands() throws RecognitionException {
        try {
            // InternalMyDsl.g:79:1: ( ruleCommands EOF )
            // InternalMyDsl.g:80:1: ruleCommands EOF
            {
             before(grammarAccess.getCommandsRule()); 
            pushFollow(FOLLOW_1);
            ruleCommands();

            state._fsp--;

             after(grammarAccess.getCommandsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCommands"


    // $ANTLR start "ruleCommands"
    // InternalMyDsl.g:87:1: ruleCommands : ( ( rule__Commands__Alternatives ) ) ;
    public final void ruleCommands() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:91:2: ( ( ( rule__Commands__Alternatives ) ) )
            // InternalMyDsl.g:92:2: ( ( rule__Commands__Alternatives ) )
            {
            // InternalMyDsl.g:92:2: ( ( rule__Commands__Alternatives ) )
            // InternalMyDsl.g:93:3: ( rule__Commands__Alternatives )
            {
             before(grammarAccess.getCommandsAccess().getAlternatives()); 
            // InternalMyDsl.g:94:3: ( rule__Commands__Alternatives )
            // InternalMyDsl.g:94:4: rule__Commands__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Commands__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getCommandsAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCommands"


    // $ANTLR start "entryRuleAdaptCommand"
    // InternalMyDsl.g:103:1: entryRuleAdaptCommand : ruleAdaptCommand EOF ;
    public final void entryRuleAdaptCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:104:1: ( ruleAdaptCommand EOF )
            // InternalMyDsl.g:105:1: ruleAdaptCommand EOF
            {
             before(grammarAccess.getAdaptCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleAdaptCommand();

            state._fsp--;

             after(grammarAccess.getAdaptCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdaptCommand"


    // $ANTLR start "ruleAdaptCommand"
    // InternalMyDsl.g:112:1: ruleAdaptCommand : ( ( rule__AdaptCommand__Group__0 ) ) ;
    public final void ruleAdaptCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:116:2: ( ( ( rule__AdaptCommand__Group__0 ) ) )
            // InternalMyDsl.g:117:2: ( ( rule__AdaptCommand__Group__0 ) )
            {
            // InternalMyDsl.g:117:2: ( ( rule__AdaptCommand__Group__0 ) )
            // InternalMyDsl.g:118:3: ( rule__AdaptCommand__Group__0 )
            {
             before(grammarAccess.getAdaptCommandAccess().getGroup()); 
            // InternalMyDsl.g:119:3: ( rule__AdaptCommand__Group__0 )
            // InternalMyDsl.g:119:4: rule__AdaptCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AdaptCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdaptCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdaptCommand"


    // $ANTLR start "entryRuleInsertAfterCommand"
    // InternalMyDsl.g:128:1: entryRuleInsertAfterCommand : ruleInsertAfterCommand EOF ;
    public final void entryRuleInsertAfterCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:129:1: ( ruleInsertAfterCommand EOF )
            // InternalMyDsl.g:130:1: ruleInsertAfterCommand EOF
            {
             before(grammarAccess.getInsertAfterCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleInsertAfterCommand();

            state._fsp--;

             after(grammarAccess.getInsertAfterCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleInsertAfterCommand"


    // $ANTLR start "ruleInsertAfterCommand"
    // InternalMyDsl.g:137:1: ruleInsertAfterCommand : ( ( rule__InsertAfterCommand__Group__0 ) ) ;
    public final void ruleInsertAfterCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:141:2: ( ( ( rule__InsertAfterCommand__Group__0 ) ) )
            // InternalMyDsl.g:142:2: ( ( rule__InsertAfterCommand__Group__0 ) )
            {
            // InternalMyDsl.g:142:2: ( ( rule__InsertAfterCommand__Group__0 ) )
            // InternalMyDsl.g:143:3: ( rule__InsertAfterCommand__Group__0 )
            {
             before(grammarAccess.getInsertAfterCommandAccess().getGroup()); 
            // InternalMyDsl.g:144:3: ( rule__InsertAfterCommand__Group__0 )
            // InternalMyDsl.g:144:4: rule__InsertAfterCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__InsertAfterCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getInsertAfterCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleInsertAfterCommand"


    // $ANTLR start "entryRuleSetloopCommand"
    // InternalMyDsl.g:153:1: entryRuleSetloopCommand : ruleSetloopCommand EOF ;
    public final void entryRuleSetloopCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:154:1: ( ruleSetloopCommand EOF )
            // InternalMyDsl.g:155:1: ruleSetloopCommand EOF
            {
             before(grammarAccess.getSetloopCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleSetloopCommand();

            state._fsp--;

             after(grammarAccess.getSetloopCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSetloopCommand"


    // $ANTLR start "ruleSetloopCommand"
    // InternalMyDsl.g:162:1: ruleSetloopCommand : ( ( rule__SetloopCommand__Group__0 ) ) ;
    public final void ruleSetloopCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:166:2: ( ( ( rule__SetloopCommand__Group__0 ) ) )
            // InternalMyDsl.g:167:2: ( ( rule__SetloopCommand__Group__0 ) )
            {
            // InternalMyDsl.g:167:2: ( ( rule__SetloopCommand__Group__0 ) )
            // InternalMyDsl.g:168:3: ( rule__SetloopCommand__Group__0 )
            {
             before(grammarAccess.getSetloopCommandAccess().getGroup()); 
            // InternalMyDsl.g:169:3: ( rule__SetloopCommand__Group__0 )
            // InternalMyDsl.g:169:4: rule__SetloopCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSetloopCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSetloopCommand"


    // $ANTLR start "entryRuleBreakCommand"
    // InternalMyDsl.g:178:1: entryRuleBreakCommand : ruleBreakCommand EOF ;
    public final void entryRuleBreakCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:179:1: ( ruleBreakCommand EOF )
            // InternalMyDsl.g:180:1: ruleBreakCommand EOF
            {
             before(grammarAccess.getBreakCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleBreakCommand();

            state._fsp--;

             after(grammarAccess.getBreakCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBreakCommand"


    // $ANTLR start "ruleBreakCommand"
    // InternalMyDsl.g:187:1: ruleBreakCommand : ( ( rule__BreakCommand__Group__0 ) ) ;
    public final void ruleBreakCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:191:2: ( ( ( rule__BreakCommand__Group__0 ) ) )
            // InternalMyDsl.g:192:2: ( ( rule__BreakCommand__Group__0 ) )
            {
            // InternalMyDsl.g:192:2: ( ( rule__BreakCommand__Group__0 ) )
            // InternalMyDsl.g:193:3: ( rule__BreakCommand__Group__0 )
            {
             before(grammarAccess.getBreakCommandAccess().getGroup()); 
            // InternalMyDsl.g:194:3: ( rule__BreakCommand__Group__0 )
            // InternalMyDsl.g:194:4: rule__BreakCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__BreakCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getBreakCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBreakCommand"


    // $ANTLR start "entryRuleOptionCommand"
    // InternalMyDsl.g:203:1: entryRuleOptionCommand : ruleOptionCommand EOF ;
    public final void entryRuleOptionCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:204:1: ( ruleOptionCommand EOF )
            // InternalMyDsl.g:205:1: ruleOptionCommand EOF
            {
             before(grammarAccess.getOptionCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleOptionCommand();

            state._fsp--;

             after(grammarAccess.getOptionCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOptionCommand"


    // $ANTLR start "ruleOptionCommand"
    // InternalMyDsl.g:212:1: ruleOptionCommand : ( ( rule__OptionCommand__Group__0 ) ) ;
    public final void ruleOptionCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:216:2: ( ( ( rule__OptionCommand__Group__0 ) ) )
            // InternalMyDsl.g:217:2: ( ( rule__OptionCommand__Group__0 ) )
            {
            // InternalMyDsl.g:217:2: ( ( rule__OptionCommand__Group__0 ) )
            // InternalMyDsl.g:218:3: ( rule__OptionCommand__Group__0 )
            {
             before(grammarAccess.getOptionCommandAccess().getGroup()); 
            // InternalMyDsl.g:219:3: ( rule__OptionCommand__Group__0 )
            // InternalMyDsl.g:219:4: rule__OptionCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OptionCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOptionCommand"


    // $ANTLR start "entryRuleSelectCommand"
    // InternalMyDsl.g:228:1: entryRuleSelectCommand : ruleSelectCommand EOF ;
    public final void entryRuleSelectCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:229:1: ( ruleSelectCommand EOF )
            // InternalMyDsl.g:230:1: ruleSelectCommand EOF
            {
             before(grammarAccess.getSelectCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleSelectCommand();

            state._fsp--;

             after(grammarAccess.getSelectCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSelectCommand"


    // $ANTLR start "ruleSelectCommand"
    // InternalMyDsl.g:237:1: ruleSelectCommand : ( ( rule__SelectCommand__Group__0 ) ) ;
    public final void ruleSelectCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:241:2: ( ( ( rule__SelectCommand__Group__0 ) ) )
            // InternalMyDsl.g:242:2: ( ( rule__SelectCommand__Group__0 ) )
            {
            // InternalMyDsl.g:242:2: ( ( rule__SelectCommand__Group__0 ) )
            // InternalMyDsl.g:243:3: ( rule__SelectCommand__Group__0 )
            {
             before(grammarAccess.getSelectCommandAccess().getGroup()); 
            // InternalMyDsl.g:244:3: ( rule__SelectCommand__Group__0 )
            // InternalMyDsl.g:244:4: rule__SelectCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SelectCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSelectCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSelectCommand"


    // $ANTLR start "entryRuleOutputCommand"
    // InternalMyDsl.g:253:1: entryRuleOutputCommand : ruleOutputCommand EOF ;
    public final void entryRuleOutputCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:254:1: ( ruleOutputCommand EOF )
            // InternalMyDsl.g:255:1: ruleOutputCommand EOF
            {
             before(grammarAccess.getOutputCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleOutputCommand();

            state._fsp--;

             after(grammarAccess.getOutputCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOutputCommand"


    // $ANTLR start "ruleOutputCommand"
    // InternalMyDsl.g:262:1: ruleOutputCommand : ( ( rule__OutputCommand__Group__0 ) ) ;
    public final void ruleOutputCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:266:2: ( ( ( rule__OutputCommand__Group__0 ) ) )
            // InternalMyDsl.g:267:2: ( ( rule__OutputCommand__Group__0 ) )
            {
            // InternalMyDsl.g:267:2: ( ( rule__OutputCommand__Group__0 ) )
            // InternalMyDsl.g:268:3: ( rule__OutputCommand__Group__0 )
            {
             before(grammarAccess.getOutputCommandAccess().getGroup()); 
            // InternalMyDsl.g:269:3: ( rule__OutputCommand__Group__0 )
            // InternalMyDsl.g:269:4: rule__OutputCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OutputCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOutputCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOutputCommand"


    // $ANTLR start "entryRuleWhileCommand"
    // InternalMyDsl.g:278:1: entryRuleWhileCommand : ruleWhileCommand EOF ;
    public final void entryRuleWhileCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:279:1: ( ruleWhileCommand EOF )
            // InternalMyDsl.g:280:1: ruleWhileCommand EOF
            {
             before(grammarAccess.getWhileCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleWhileCommand();

            state._fsp--;

             after(grammarAccess.getWhileCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhileCommand"


    // $ANTLR start "ruleWhileCommand"
    // InternalMyDsl.g:287:1: ruleWhileCommand : ( ( rule__WhileCommand__Group__0 ) ) ;
    public final void ruleWhileCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:291:2: ( ( ( rule__WhileCommand__Group__0 ) ) )
            // InternalMyDsl.g:292:2: ( ( rule__WhileCommand__Group__0 ) )
            {
            // InternalMyDsl.g:292:2: ( ( rule__WhileCommand__Group__0 ) )
            // InternalMyDsl.g:293:3: ( rule__WhileCommand__Group__0 )
            {
             before(grammarAccess.getWhileCommandAccess().getGroup()); 
            // InternalMyDsl.g:294:3: ( rule__WhileCommand__Group__0 )
            // InternalMyDsl.g:294:4: rule__WhileCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWhileCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhileCommand"


    // $ANTLR start "entryRuleCommentCommand"
    // InternalMyDsl.g:303:1: entryRuleCommentCommand : ruleCommentCommand EOF ;
    public final void entryRuleCommentCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:304:1: ( ruleCommentCommand EOF )
            // InternalMyDsl.g:305:1: ruleCommentCommand EOF
            {
             before(grammarAccess.getCommentCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleCommentCommand();

            state._fsp--;

             after(grammarAccess.getCommentCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCommentCommand"


    // $ANTLR start "ruleCommentCommand"
    // InternalMyDsl.g:312:1: ruleCommentCommand : ( ( rule__CommentCommand__Group__0 ) ) ;
    public final void ruleCommentCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:316:2: ( ( ( rule__CommentCommand__Group__0 ) ) )
            // InternalMyDsl.g:317:2: ( ( rule__CommentCommand__Group__0 ) )
            {
            // InternalMyDsl.g:317:2: ( ( rule__CommentCommand__Group__0 ) )
            // InternalMyDsl.g:318:3: ( rule__CommentCommand__Group__0 )
            {
             before(grammarAccess.getCommentCommandAccess().getGroup()); 
            // InternalMyDsl.g:319:3: ( rule__CommentCommand__Group__0 )
            // InternalMyDsl.g:319:4: rule__CommentCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__CommentCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCommentCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCommentCommand"


    // $ANTLR start "entryRuleSetCommand"
    // InternalMyDsl.g:328:1: entryRuleSetCommand : ruleSetCommand EOF ;
    public final void entryRuleSetCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:329:1: ( ruleSetCommand EOF )
            // InternalMyDsl.g:330:1: ruleSetCommand EOF
            {
             before(grammarAccess.getSetCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleSetCommand();

            state._fsp--;

             after(grammarAccess.getSetCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSetCommand"


    // $ANTLR start "ruleSetCommand"
    // InternalMyDsl.g:337:1: ruleSetCommand : ( ( rule__SetCommand__Group__0 ) ) ;
    public final void ruleSetCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:341:2: ( ( ( rule__SetCommand__Group__0 ) ) )
            // InternalMyDsl.g:342:2: ( ( rule__SetCommand__Group__0 ) )
            {
            // InternalMyDsl.g:342:2: ( ( rule__SetCommand__Group__0 ) )
            // InternalMyDsl.g:343:3: ( rule__SetCommand__Group__0 )
            {
             before(grammarAccess.getSetCommandAccess().getGroup()); 
            // InternalMyDsl.g:344:3: ( rule__SetCommand__Group__0 )
            // InternalMyDsl.g:344:4: rule__SetCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__SetCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSetCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSetCommand"


    // $ANTLR start "entryRuleOutdirCommand"
    // InternalMyDsl.g:353:1: entryRuleOutdirCommand : ruleOutdirCommand EOF ;
    public final void entryRuleOutdirCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:354:1: ( ruleOutdirCommand EOF )
            // InternalMyDsl.g:355:1: ruleOutdirCommand EOF
            {
             before(grammarAccess.getOutdirCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleOutdirCommand();

            state._fsp--;

             after(grammarAccess.getOutdirCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOutdirCommand"


    // $ANTLR start "ruleOutdirCommand"
    // InternalMyDsl.g:362:1: ruleOutdirCommand : ( ( rule__OutdirCommand__Group__0 ) ) ;
    public final void ruleOutdirCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:366:2: ( ( ( rule__OutdirCommand__Group__0 ) ) )
            // InternalMyDsl.g:367:2: ( ( rule__OutdirCommand__Group__0 ) )
            {
            // InternalMyDsl.g:367:2: ( ( rule__OutdirCommand__Group__0 ) )
            // InternalMyDsl.g:368:3: ( rule__OutdirCommand__Group__0 )
            {
             before(grammarAccess.getOutdirCommandAccess().getGroup()); 
            // InternalMyDsl.g:369:3: ( rule__OutdirCommand__Group__0 )
            // InternalMyDsl.g:369:4: rule__OutdirCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OutdirCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOutdirCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOutdirCommand"


    // $ANTLR start "entryRuleOutfileCommand"
    // InternalMyDsl.g:378:1: entryRuleOutfileCommand : ruleOutfileCommand EOF ;
    public final void entryRuleOutfileCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:379:1: ( ruleOutfileCommand EOF )
            // InternalMyDsl.g:380:1: ruleOutfileCommand EOF
            {
             before(grammarAccess.getOutfileCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleOutfileCommand();

            state._fsp--;

             after(grammarAccess.getOutfileCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOutfileCommand"


    // $ANTLR start "ruleOutfileCommand"
    // InternalMyDsl.g:387:1: ruleOutfileCommand : ( ( rule__OutfileCommand__Group__0 ) ) ;
    public final void ruleOutfileCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:391:2: ( ( ( rule__OutfileCommand__Group__0 ) ) )
            // InternalMyDsl.g:392:2: ( ( rule__OutfileCommand__Group__0 ) )
            {
            // InternalMyDsl.g:392:2: ( ( rule__OutfileCommand__Group__0 ) )
            // InternalMyDsl.g:393:3: ( rule__OutfileCommand__Group__0 )
            {
             before(grammarAccess.getOutfileCommandAccess().getGroup()); 
            // InternalMyDsl.g:394:3: ( rule__OutfileCommand__Group__0 )
            // InternalMyDsl.g:394:4: rule__OutfileCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OutfileCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOutfileCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOutfileCommand"


    // $ANTLR start "entryRuleRemoveCommand"
    // InternalMyDsl.g:403:1: entryRuleRemoveCommand : ruleRemoveCommand EOF ;
    public final void entryRuleRemoveCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:404:1: ( ruleRemoveCommand EOF )
            // InternalMyDsl.g:405:1: ruleRemoveCommand EOF
            {
             before(grammarAccess.getRemoveCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleRemoveCommand();

            state._fsp--;

             after(grammarAccess.getRemoveCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRemoveCommand"


    // $ANTLR start "ruleRemoveCommand"
    // InternalMyDsl.g:412:1: ruleRemoveCommand : ( ( rule__RemoveCommand__Group__0 ) ) ;
    public final void ruleRemoveCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:416:2: ( ( ( rule__RemoveCommand__Group__0 ) ) )
            // InternalMyDsl.g:417:2: ( ( rule__RemoveCommand__Group__0 ) )
            {
            // InternalMyDsl.g:417:2: ( ( rule__RemoveCommand__Group__0 ) )
            // InternalMyDsl.g:418:3: ( rule__RemoveCommand__Group__0 )
            {
             before(grammarAccess.getRemoveCommandAccess().getGroup()); 
            // InternalMyDsl.g:419:3: ( rule__RemoveCommand__Group__0 )
            // InternalMyDsl.g:419:4: rule__RemoveCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__RemoveCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRemoveCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRemoveCommand"


    // $ANTLR start "entryRuleMessageCommand"
    // InternalMyDsl.g:428:1: entryRuleMessageCommand : ruleMessageCommand EOF ;
    public final void entryRuleMessageCommand() throws RecognitionException {
        try {
            // InternalMyDsl.g:429:1: ( ruleMessageCommand EOF )
            // InternalMyDsl.g:430:1: ruleMessageCommand EOF
            {
             before(grammarAccess.getMessageCommandRule()); 
            pushFollow(FOLLOW_1);
            ruleMessageCommand();

            state._fsp--;

             after(grammarAccess.getMessageCommandRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMessageCommand"


    // $ANTLR start "ruleMessageCommand"
    // InternalMyDsl.g:437:1: ruleMessageCommand : ( ( rule__MessageCommand__Group__0 ) ) ;
    public final void ruleMessageCommand() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:441:2: ( ( ( rule__MessageCommand__Group__0 ) ) )
            // InternalMyDsl.g:442:2: ( ( rule__MessageCommand__Group__0 ) )
            {
            // InternalMyDsl.g:442:2: ( ( rule__MessageCommand__Group__0 ) )
            // InternalMyDsl.g:443:3: ( rule__MessageCommand__Group__0 )
            {
             before(grammarAccess.getMessageCommandAccess().getGroup()); 
            // InternalMyDsl.g:444:3: ( rule__MessageCommand__Group__0 )
            // InternalMyDsl.g:444:4: rule__MessageCommand__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MessageCommand__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMessageCommandAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMessageCommand"


    // $ANTLR start "rule__Commands__Alternatives"
    // InternalMyDsl.g:452:1: rule__Commands__Alternatives : ( ( ruleSetCommand ) | ( ruleCommentCommand ) | ( ruleWhileCommand ) | ( ruleOutputCommand ) | ( ruleSelectCommand ) | ( ruleOptionCommand ) | ( ruleInsertAfterCommand ) | ( ruleAdaptCommand ) );
    public final void rule__Commands__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:456:1: ( ( ruleSetCommand ) | ( ruleCommentCommand ) | ( ruleWhileCommand ) | ( ruleOutputCommand ) | ( ruleSelectCommand ) | ( ruleOptionCommand ) | ( ruleInsertAfterCommand ) | ( ruleAdaptCommand ) )
            int alt2=8;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt2=1;
                }
                break;
            case 29:
                {
                alt2=2;
                }
                break;
            case 27:
                {
                alt2=3;
                }
                break;
            case 26:
                {
                alt2=4;
                }
                break;
            case 24:
                {
                alt2=5;
                }
                break;
            case 22:
                {
                alt2=6;
                }
                break;
            case 15:
                {
                alt2=7;
                }
                break;
            case 13:
                {
                alt2=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // InternalMyDsl.g:457:2: ( ruleSetCommand )
                    {
                    // InternalMyDsl.g:457:2: ( ruleSetCommand )
                    // InternalMyDsl.g:458:3: ruleSetCommand
                    {
                     before(grammarAccess.getCommandsAccess().getSetCommandParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSetCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getSetCommandParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:463:2: ( ruleCommentCommand )
                    {
                    // InternalMyDsl.g:463:2: ( ruleCommentCommand )
                    // InternalMyDsl.g:464:3: ruleCommentCommand
                    {
                     before(grammarAccess.getCommandsAccess().getCommentCommandParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleCommentCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getCommentCommandParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:469:2: ( ruleWhileCommand )
                    {
                    // InternalMyDsl.g:469:2: ( ruleWhileCommand )
                    // InternalMyDsl.g:470:3: ruleWhileCommand
                    {
                     before(grammarAccess.getCommandsAccess().getWhileCommandParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleWhileCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getWhileCommandParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:475:2: ( ruleOutputCommand )
                    {
                    // InternalMyDsl.g:475:2: ( ruleOutputCommand )
                    // InternalMyDsl.g:476:3: ruleOutputCommand
                    {
                     before(grammarAccess.getCommandsAccess().getOutputCommandParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleOutputCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getOutputCommandParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:481:2: ( ruleSelectCommand )
                    {
                    // InternalMyDsl.g:481:2: ( ruleSelectCommand )
                    // InternalMyDsl.g:482:3: ruleSelectCommand
                    {
                     before(grammarAccess.getCommandsAccess().getSelectCommandParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleSelectCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getSelectCommandParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalMyDsl.g:487:2: ( ruleOptionCommand )
                    {
                    // InternalMyDsl.g:487:2: ( ruleOptionCommand )
                    // InternalMyDsl.g:488:3: ruleOptionCommand
                    {
                     before(grammarAccess.getCommandsAccess().getOptionCommandParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getOptionCommandParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalMyDsl.g:493:2: ( ruleInsertAfterCommand )
                    {
                    // InternalMyDsl.g:493:2: ( ruleInsertAfterCommand )
                    // InternalMyDsl.g:494:3: ruleInsertAfterCommand
                    {
                     before(grammarAccess.getCommandsAccess().getInsertAfterCommandParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleInsertAfterCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getInsertAfterCommandParserRuleCall_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalMyDsl.g:499:2: ( ruleAdaptCommand )
                    {
                    // InternalMyDsl.g:499:2: ( ruleAdaptCommand )
                    // InternalMyDsl.g:500:3: ruleAdaptCommand
                    {
                     before(grammarAccess.getCommandsAccess().getAdaptCommandParserRuleCall_7()); 
                    pushFollow(FOLLOW_2);
                    ruleAdaptCommand();

                    state._fsp--;

                     after(grammarAccess.getCommandsAccess().getAdaptCommandParserRuleCall_7()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Commands__Alternatives"


    // $ANTLR start "rule__AdaptCommand__Alternatives_2"
    // InternalMyDsl.g:509:1: rule__AdaptCommand__Alternatives_2 : ( ( RULE_EXPRESSION ) | ( RULE_WS ) | ( ruleOptionCommand ) | ( ruleInsertAfterCommand ) | ( ruleSelectCommand ) );
    public final void rule__AdaptCommand__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:513:1: ( ( RULE_EXPRESSION ) | ( RULE_WS ) | ( ruleOptionCommand ) | ( ruleInsertAfterCommand ) | ( ruleSelectCommand ) )
            int alt3=5;
            switch ( input.LA(1) ) {
            case RULE_EXPRESSION:
                {
                alt3=1;
                }
                break;
            case RULE_WS:
                {
                alt3=2;
                }
                break;
            case 22:
                {
                alt3=3;
                }
                break;
            case 15:
                {
                alt3=4;
                }
                break;
            case 24:
                {
                alt3=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:514:2: ( RULE_EXPRESSION )
                    {
                    // InternalMyDsl.g:514:2: ( RULE_EXPRESSION )
                    // InternalMyDsl.g:515:3: RULE_EXPRESSION
                    {
                     before(grammarAccess.getAdaptCommandAccess().getEXPRESSIONTerminalRuleCall_2_0()); 
                    match(input,RULE_EXPRESSION,FOLLOW_2); 
                     after(grammarAccess.getAdaptCommandAccess().getEXPRESSIONTerminalRuleCall_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:520:2: ( RULE_WS )
                    {
                    // InternalMyDsl.g:520:2: ( RULE_WS )
                    // InternalMyDsl.g:521:3: RULE_WS
                    {
                     before(grammarAccess.getAdaptCommandAccess().getWSTerminalRuleCall_2_1()); 
                    match(input,RULE_WS,FOLLOW_2); 
                     after(grammarAccess.getAdaptCommandAccess().getWSTerminalRuleCall_2_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:526:2: ( ruleOptionCommand )
                    {
                    // InternalMyDsl.g:526:2: ( ruleOptionCommand )
                    // InternalMyDsl.g:527:3: ruleOptionCommand
                    {
                     before(grammarAccess.getAdaptCommandAccess().getOptionCommandParserRuleCall_2_2()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionCommand();

                    state._fsp--;

                     after(grammarAccess.getAdaptCommandAccess().getOptionCommandParserRuleCall_2_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:532:2: ( ruleInsertAfterCommand )
                    {
                    // InternalMyDsl.g:532:2: ( ruleInsertAfterCommand )
                    // InternalMyDsl.g:533:3: ruleInsertAfterCommand
                    {
                     before(grammarAccess.getAdaptCommandAccess().getInsertAfterCommandParserRuleCall_2_3()); 
                    pushFollow(FOLLOW_2);
                    ruleInsertAfterCommand();

                    state._fsp--;

                     after(grammarAccess.getAdaptCommandAccess().getInsertAfterCommandParserRuleCall_2_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:538:2: ( ruleSelectCommand )
                    {
                    // InternalMyDsl.g:538:2: ( ruleSelectCommand )
                    // InternalMyDsl.g:539:3: ruleSelectCommand
                    {
                     before(grammarAccess.getAdaptCommandAccess().getSelectCommandParserRuleCall_2_4()); 
                    pushFollow(FOLLOW_2);
                    ruleSelectCommand();

                    state._fsp--;

                     after(grammarAccess.getAdaptCommandAccess().getSelectCommandParserRuleCall_2_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Alternatives_2"


    // $ANTLR start "rule__InsertAfterCommand__Alternatives_2"
    // InternalMyDsl.g:548:1: rule__InsertAfterCommand__Alternatives_2 : ( ( RULE_EXPRESSION ) | ( RULE_WS ) | ( ruleAdaptCommand ) | ( ruleSetCommand ) | ( ruleSelectCommand ) | ( ruleOutputCommand ) | ( ruleOutfileCommand ) | ( ruleOutdirCommand ) | ( ruleMessageCommand ) | ( ruleWhileCommand ) | ( ruleRemoveCommand ) | ( ruleBreakCommand ) | ( ruleSetloopCommand ) );
    public final void rule__InsertAfterCommand__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:552:1: ( ( RULE_EXPRESSION ) | ( RULE_WS ) | ( ruleAdaptCommand ) | ( ruleSetCommand ) | ( ruleSelectCommand ) | ( ruleOutputCommand ) | ( ruleOutfileCommand ) | ( ruleOutdirCommand ) | ( ruleMessageCommand ) | ( ruleWhileCommand ) | ( ruleRemoveCommand ) | ( ruleBreakCommand ) | ( ruleSetloopCommand ) )
            int alt4=13;
            switch ( input.LA(1) ) {
            case RULE_EXPRESSION:
                {
                alt4=1;
                }
                break;
            case RULE_WS:
                {
                alt4=2;
                }
                break;
            case 13:
                {
                alt4=3;
                }
                break;
            case 30:
                {
                alt4=4;
                }
                break;
            case 24:
                {
                alt4=5;
                }
                break;
            case 26:
                {
                alt4=6;
                }
                break;
            case 32:
                {
                alt4=7;
                }
                break;
            case 31:
                {
                alt4=8;
                }
                break;
            case 34:
                {
                alt4=9;
                }
                break;
            case 27:
                {
                alt4=10;
                }
                break;
            case 33:
                {
                alt4=11;
                }
                break;
            case 20:
                {
                alt4=12;
                }
                break;
            case 17:
                {
                alt4=13;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalMyDsl.g:553:2: ( RULE_EXPRESSION )
                    {
                    // InternalMyDsl.g:553:2: ( RULE_EXPRESSION )
                    // InternalMyDsl.g:554:3: RULE_EXPRESSION
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getEXPRESSIONTerminalRuleCall_2_0()); 
                    match(input,RULE_EXPRESSION,FOLLOW_2); 
                     after(grammarAccess.getInsertAfterCommandAccess().getEXPRESSIONTerminalRuleCall_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:559:2: ( RULE_WS )
                    {
                    // InternalMyDsl.g:559:2: ( RULE_WS )
                    // InternalMyDsl.g:560:3: RULE_WS
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getWSTerminalRuleCall_2_1()); 
                    match(input,RULE_WS,FOLLOW_2); 
                     after(grammarAccess.getInsertAfterCommandAccess().getWSTerminalRuleCall_2_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:565:2: ( ruleAdaptCommand )
                    {
                    // InternalMyDsl.g:565:2: ( ruleAdaptCommand )
                    // InternalMyDsl.g:566:3: ruleAdaptCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getAdaptCommandParserRuleCall_2_2()); 
                    pushFollow(FOLLOW_2);
                    ruleAdaptCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getAdaptCommandParserRuleCall_2_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:571:2: ( ruleSetCommand )
                    {
                    // InternalMyDsl.g:571:2: ( ruleSetCommand )
                    // InternalMyDsl.g:572:3: ruleSetCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getSetCommandParserRuleCall_2_3()); 
                    pushFollow(FOLLOW_2);
                    ruleSetCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getSetCommandParserRuleCall_2_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:577:2: ( ruleSelectCommand )
                    {
                    // InternalMyDsl.g:577:2: ( ruleSelectCommand )
                    // InternalMyDsl.g:578:3: ruleSelectCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getSelectCommandParserRuleCall_2_4()); 
                    pushFollow(FOLLOW_2);
                    ruleSelectCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getSelectCommandParserRuleCall_2_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalMyDsl.g:583:2: ( ruleOutputCommand )
                    {
                    // InternalMyDsl.g:583:2: ( ruleOutputCommand )
                    // InternalMyDsl.g:584:3: ruleOutputCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getOutputCommandParserRuleCall_2_5()); 
                    pushFollow(FOLLOW_2);
                    ruleOutputCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getOutputCommandParserRuleCall_2_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalMyDsl.g:589:2: ( ruleOutfileCommand )
                    {
                    // InternalMyDsl.g:589:2: ( ruleOutfileCommand )
                    // InternalMyDsl.g:590:3: ruleOutfileCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getOutfileCommandParserRuleCall_2_6()); 
                    pushFollow(FOLLOW_2);
                    ruleOutfileCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getOutfileCommandParserRuleCall_2_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalMyDsl.g:595:2: ( ruleOutdirCommand )
                    {
                    // InternalMyDsl.g:595:2: ( ruleOutdirCommand )
                    // InternalMyDsl.g:596:3: ruleOutdirCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getOutdirCommandParserRuleCall_2_7()); 
                    pushFollow(FOLLOW_2);
                    ruleOutdirCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getOutdirCommandParserRuleCall_2_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalMyDsl.g:601:2: ( ruleMessageCommand )
                    {
                    // InternalMyDsl.g:601:2: ( ruleMessageCommand )
                    // InternalMyDsl.g:602:3: ruleMessageCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getMessageCommandParserRuleCall_2_8()); 
                    pushFollow(FOLLOW_2);
                    ruleMessageCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getMessageCommandParserRuleCall_2_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalMyDsl.g:607:2: ( ruleWhileCommand )
                    {
                    // InternalMyDsl.g:607:2: ( ruleWhileCommand )
                    // InternalMyDsl.g:608:3: ruleWhileCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getWhileCommandParserRuleCall_2_9()); 
                    pushFollow(FOLLOW_2);
                    ruleWhileCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getWhileCommandParserRuleCall_2_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalMyDsl.g:613:2: ( ruleRemoveCommand )
                    {
                    // InternalMyDsl.g:613:2: ( ruleRemoveCommand )
                    // InternalMyDsl.g:614:3: ruleRemoveCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getRemoveCommandParserRuleCall_2_10()); 
                    pushFollow(FOLLOW_2);
                    ruleRemoveCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getRemoveCommandParserRuleCall_2_10()); 

                    }


                    }
                    break;
                case 12 :
                    // InternalMyDsl.g:619:2: ( ruleBreakCommand )
                    {
                    // InternalMyDsl.g:619:2: ( ruleBreakCommand )
                    // InternalMyDsl.g:620:3: ruleBreakCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getBreakCommandParserRuleCall_2_11()); 
                    pushFollow(FOLLOW_2);
                    ruleBreakCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getBreakCommandParserRuleCall_2_11()); 

                    }


                    }
                    break;
                case 13 :
                    // InternalMyDsl.g:625:2: ( ruleSetloopCommand )
                    {
                    // InternalMyDsl.g:625:2: ( ruleSetloopCommand )
                    // InternalMyDsl.g:626:3: ruleSetloopCommand
                    {
                     before(grammarAccess.getInsertAfterCommandAccess().getSetloopCommandParserRuleCall_2_12()); 
                    pushFollow(FOLLOW_2);
                    ruleSetloopCommand();

                    state._fsp--;

                     after(grammarAccess.getInsertAfterCommandAccess().getSetloopCommandParserRuleCall_2_12()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Alternatives_2"


    // $ANTLR start "rule__BreakCommand__Alternatives_2"
    // InternalMyDsl.g:635:1: rule__BreakCommand__Alternatives_2 : ( ( ruleAdaptCommand ) | ( ruleSetCommand ) | ( ruleSelectCommand ) | ( ruleOutputCommand ) | ( ruleOutfileCommand ) | ( ruleOutdirCommand ) | ( ruleMessageCommand ) | ( ruleWhileCommand ) | ( ruleRemoveCommand ) | ( ruleBreakCommand ) | ( ruleSetloopCommand ) );
    public final void rule__BreakCommand__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:639:1: ( ( ruleAdaptCommand ) | ( ruleSetCommand ) | ( ruleSelectCommand ) | ( ruleOutputCommand ) | ( ruleOutfileCommand ) | ( ruleOutdirCommand ) | ( ruleMessageCommand ) | ( ruleWhileCommand ) | ( ruleRemoveCommand ) | ( ruleBreakCommand ) | ( ruleSetloopCommand ) )
            int alt5=11;
            switch ( input.LA(1) ) {
            case 13:
                {
                alt5=1;
                }
                break;
            case 30:
                {
                alt5=2;
                }
                break;
            case 24:
                {
                alt5=3;
                }
                break;
            case 26:
                {
                alt5=4;
                }
                break;
            case 32:
                {
                alt5=5;
                }
                break;
            case 31:
                {
                alt5=6;
                }
                break;
            case 34:
                {
                alt5=7;
                }
                break;
            case 27:
                {
                alt5=8;
                }
                break;
            case 33:
                {
                alt5=9;
                }
                break;
            case 20:
                {
                alt5=10;
                }
                break;
            case 17:
                {
                alt5=11;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalMyDsl.g:640:2: ( ruleAdaptCommand )
                    {
                    // InternalMyDsl.g:640:2: ( ruleAdaptCommand )
                    // InternalMyDsl.g:641:3: ruleAdaptCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getAdaptCommandParserRuleCall_2_0()); 
                    pushFollow(FOLLOW_2);
                    ruleAdaptCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getAdaptCommandParserRuleCall_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:646:2: ( ruleSetCommand )
                    {
                    // InternalMyDsl.g:646:2: ( ruleSetCommand )
                    // InternalMyDsl.g:647:3: ruleSetCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getSetCommandParserRuleCall_2_1()); 
                    pushFollow(FOLLOW_2);
                    ruleSetCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getSetCommandParserRuleCall_2_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:652:2: ( ruleSelectCommand )
                    {
                    // InternalMyDsl.g:652:2: ( ruleSelectCommand )
                    // InternalMyDsl.g:653:3: ruleSelectCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getSelectCommandParserRuleCall_2_2()); 
                    pushFollow(FOLLOW_2);
                    ruleSelectCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getSelectCommandParserRuleCall_2_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:658:2: ( ruleOutputCommand )
                    {
                    // InternalMyDsl.g:658:2: ( ruleOutputCommand )
                    // InternalMyDsl.g:659:3: ruleOutputCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getOutputCommandParserRuleCall_2_3()); 
                    pushFollow(FOLLOW_2);
                    ruleOutputCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getOutputCommandParserRuleCall_2_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:664:2: ( ruleOutfileCommand )
                    {
                    // InternalMyDsl.g:664:2: ( ruleOutfileCommand )
                    // InternalMyDsl.g:665:3: ruleOutfileCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getOutfileCommandParserRuleCall_2_4()); 
                    pushFollow(FOLLOW_2);
                    ruleOutfileCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getOutfileCommandParserRuleCall_2_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalMyDsl.g:670:2: ( ruleOutdirCommand )
                    {
                    // InternalMyDsl.g:670:2: ( ruleOutdirCommand )
                    // InternalMyDsl.g:671:3: ruleOutdirCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getOutdirCommandParserRuleCall_2_5()); 
                    pushFollow(FOLLOW_2);
                    ruleOutdirCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getOutdirCommandParserRuleCall_2_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalMyDsl.g:676:2: ( ruleMessageCommand )
                    {
                    // InternalMyDsl.g:676:2: ( ruleMessageCommand )
                    // InternalMyDsl.g:677:3: ruleMessageCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getMessageCommandParserRuleCall_2_6()); 
                    pushFollow(FOLLOW_2);
                    ruleMessageCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getMessageCommandParserRuleCall_2_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalMyDsl.g:682:2: ( ruleWhileCommand )
                    {
                    // InternalMyDsl.g:682:2: ( ruleWhileCommand )
                    // InternalMyDsl.g:683:3: ruleWhileCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getWhileCommandParserRuleCall_2_7()); 
                    pushFollow(FOLLOW_2);
                    ruleWhileCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getWhileCommandParserRuleCall_2_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalMyDsl.g:688:2: ( ruleRemoveCommand )
                    {
                    // InternalMyDsl.g:688:2: ( ruleRemoveCommand )
                    // InternalMyDsl.g:689:3: ruleRemoveCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getRemoveCommandParserRuleCall_2_8()); 
                    pushFollow(FOLLOW_2);
                    ruleRemoveCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getRemoveCommandParserRuleCall_2_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalMyDsl.g:694:2: ( ruleBreakCommand )
                    {
                    // InternalMyDsl.g:694:2: ( ruleBreakCommand )
                    // InternalMyDsl.g:695:3: ruleBreakCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getBreakCommandParserRuleCall_2_9()); 
                    pushFollow(FOLLOW_2);
                    ruleBreakCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getBreakCommandParserRuleCall_2_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalMyDsl.g:700:2: ( ruleSetloopCommand )
                    {
                    // InternalMyDsl.g:700:2: ( ruleSetloopCommand )
                    // InternalMyDsl.g:701:3: ruleSetloopCommand
                    {
                     before(grammarAccess.getBreakCommandAccess().getSetloopCommandParserRuleCall_2_10()); 
                    pushFollow(FOLLOW_2);
                    ruleSetloopCommand();

                    state._fsp--;

                     after(grammarAccess.getBreakCommandAccess().getSetloopCommandParserRuleCall_2_10()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Alternatives_2"


    // $ANTLR start "rule__OptionCommand__Alternatives_2"
    // InternalMyDsl.g:710:1: rule__OptionCommand__Alternatives_2 : ( ( RULE_EXPRESSION ) | ( RULE_WS ) | ( ruleInsertAfterCommand ) | ( '=' ) );
    public final void rule__OptionCommand__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:714:1: ( ( RULE_EXPRESSION ) | ( RULE_WS ) | ( ruleInsertAfterCommand ) | ( '=' ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case RULE_EXPRESSION:
                {
                alt6=1;
                }
                break;
            case RULE_WS:
                {
                alt6=2;
                }
                break;
            case 15:
                {
                alt6=3;
                }
                break;
            case 12:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalMyDsl.g:715:2: ( RULE_EXPRESSION )
                    {
                    // InternalMyDsl.g:715:2: ( RULE_EXPRESSION )
                    // InternalMyDsl.g:716:3: RULE_EXPRESSION
                    {
                     before(grammarAccess.getOptionCommandAccess().getEXPRESSIONTerminalRuleCall_2_0()); 
                    match(input,RULE_EXPRESSION,FOLLOW_2); 
                     after(grammarAccess.getOptionCommandAccess().getEXPRESSIONTerminalRuleCall_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:721:2: ( RULE_WS )
                    {
                    // InternalMyDsl.g:721:2: ( RULE_WS )
                    // InternalMyDsl.g:722:3: RULE_WS
                    {
                     before(grammarAccess.getOptionCommandAccess().getWSTerminalRuleCall_2_1()); 
                    match(input,RULE_WS,FOLLOW_2); 
                     after(grammarAccess.getOptionCommandAccess().getWSTerminalRuleCall_2_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:727:2: ( ruleInsertAfterCommand )
                    {
                    // InternalMyDsl.g:727:2: ( ruleInsertAfterCommand )
                    // InternalMyDsl.g:728:3: ruleInsertAfterCommand
                    {
                     before(grammarAccess.getOptionCommandAccess().getInsertAfterCommandParserRuleCall_2_2()); 
                    pushFollow(FOLLOW_2);
                    ruleInsertAfterCommand();

                    state._fsp--;

                     after(grammarAccess.getOptionCommandAccess().getInsertAfterCommandParserRuleCall_2_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:733:2: ( '=' )
                    {
                    // InternalMyDsl.g:733:2: ( '=' )
                    // InternalMyDsl.g:734:3: '='
                    {
                     before(grammarAccess.getOptionCommandAccess().getEqualsSignKeyword_2_3()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getOptionCommandAccess().getEqualsSignKeyword_2_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Alternatives_2"


    // $ANTLR start "rule__WhileCommand__Alternatives_2"
    // InternalMyDsl.g:743:1: rule__WhileCommand__Alternatives_2 : ( ( ( rule__WhileCommand__Group_2_0__0 ) ) | ( RULE_WS ) | ( ruleOptionCommand ) | ( ruleOutputCommand ) | ( ruleSelectCommand ) | ( ruleAdaptCommand ) );
    public final void rule__WhileCommand__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:747:1: ( ( ( rule__WhileCommand__Group_2_0__0 ) ) | ( RULE_WS ) | ( ruleOptionCommand ) | ( ruleOutputCommand ) | ( ruleSelectCommand ) | ( ruleAdaptCommand ) )
            int alt7=6;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt7=1;
                }
                break;
            case RULE_WS:
                {
                alt7=2;
                }
                break;
            case 22:
                {
                alt7=3;
                }
                break;
            case 26:
                {
                alt7=4;
                }
                break;
            case 24:
                {
                alt7=5;
                }
                break;
            case 13:
                {
                alt7=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }

            switch (alt7) {
                case 1 :
                    // InternalMyDsl.g:748:2: ( ( rule__WhileCommand__Group_2_0__0 ) )
                    {
                    // InternalMyDsl.g:748:2: ( ( rule__WhileCommand__Group_2_0__0 ) )
                    // InternalMyDsl.g:749:3: ( rule__WhileCommand__Group_2_0__0 )
                    {
                     before(grammarAccess.getWhileCommandAccess().getGroup_2_0()); 
                    // InternalMyDsl.g:750:3: ( rule__WhileCommand__Group_2_0__0 )
                    // InternalMyDsl.g:750:4: rule__WhileCommand__Group_2_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__WhileCommand__Group_2_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getWhileCommandAccess().getGroup_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:754:2: ( RULE_WS )
                    {
                    // InternalMyDsl.g:754:2: ( RULE_WS )
                    // InternalMyDsl.g:755:3: RULE_WS
                    {
                     before(grammarAccess.getWhileCommandAccess().getWSTerminalRuleCall_2_1()); 
                    match(input,RULE_WS,FOLLOW_2); 
                     after(grammarAccess.getWhileCommandAccess().getWSTerminalRuleCall_2_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalMyDsl.g:760:2: ( ruleOptionCommand )
                    {
                    // InternalMyDsl.g:760:2: ( ruleOptionCommand )
                    // InternalMyDsl.g:761:3: ruleOptionCommand
                    {
                     before(grammarAccess.getWhileCommandAccess().getOptionCommandParserRuleCall_2_2()); 
                    pushFollow(FOLLOW_2);
                    ruleOptionCommand();

                    state._fsp--;

                     after(grammarAccess.getWhileCommandAccess().getOptionCommandParserRuleCall_2_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalMyDsl.g:766:2: ( ruleOutputCommand )
                    {
                    // InternalMyDsl.g:766:2: ( ruleOutputCommand )
                    // InternalMyDsl.g:767:3: ruleOutputCommand
                    {
                     before(grammarAccess.getWhileCommandAccess().getOutputCommandParserRuleCall_2_3()); 
                    pushFollow(FOLLOW_2);
                    ruleOutputCommand();

                    state._fsp--;

                     after(grammarAccess.getWhileCommandAccess().getOutputCommandParserRuleCall_2_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalMyDsl.g:772:2: ( ruleSelectCommand )
                    {
                    // InternalMyDsl.g:772:2: ( ruleSelectCommand )
                    // InternalMyDsl.g:773:3: ruleSelectCommand
                    {
                     before(grammarAccess.getWhileCommandAccess().getSelectCommandParserRuleCall_2_4()); 
                    pushFollow(FOLLOW_2);
                    ruleSelectCommand();

                    state._fsp--;

                     after(grammarAccess.getWhileCommandAccess().getSelectCommandParserRuleCall_2_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalMyDsl.g:778:2: ( ruleAdaptCommand )
                    {
                    // InternalMyDsl.g:778:2: ( ruleAdaptCommand )
                    // InternalMyDsl.g:779:3: ruleAdaptCommand
                    {
                     before(grammarAccess.getWhileCommandAccess().getAdaptCommandParserRuleCall_2_5()); 
                    pushFollow(FOLLOW_2);
                    ruleAdaptCommand();

                    state._fsp--;

                     after(grammarAccess.getWhileCommandAccess().getAdaptCommandParserRuleCall_2_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Alternatives_2"


    // $ANTLR start "rule__AdaptCommand__Group__0"
    // InternalMyDsl.g:788:1: rule__AdaptCommand__Group__0 : rule__AdaptCommand__Group__0__Impl rule__AdaptCommand__Group__1 ;
    public final void rule__AdaptCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:792:1: ( rule__AdaptCommand__Group__0__Impl rule__AdaptCommand__Group__1 )
            // InternalMyDsl.g:793:2: rule__AdaptCommand__Group__0__Impl rule__AdaptCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__AdaptCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdaptCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__0"


    // $ANTLR start "rule__AdaptCommand__Group__0__Impl"
    // InternalMyDsl.g:800:1: rule__AdaptCommand__Group__0__Impl : ( '#adapt:' ) ;
    public final void rule__AdaptCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:804:1: ( ( '#adapt:' ) )
            // InternalMyDsl.g:805:1: ( '#adapt:' )
            {
            // InternalMyDsl.g:805:1: ( '#adapt:' )
            // InternalMyDsl.g:806:2: '#adapt:'
            {
             before(grammarAccess.getAdaptCommandAccess().getAdaptKeyword_0()); 
            match(input,13,FOLLOW_2); 
             after(grammarAccess.getAdaptCommandAccess().getAdaptKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__0__Impl"


    // $ANTLR start "rule__AdaptCommand__Group__1"
    // InternalMyDsl.g:815:1: rule__AdaptCommand__Group__1 : rule__AdaptCommand__Group__1__Impl rule__AdaptCommand__Group__2 ;
    public final void rule__AdaptCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:819:1: ( rule__AdaptCommand__Group__1__Impl rule__AdaptCommand__Group__2 )
            // InternalMyDsl.g:820:2: rule__AdaptCommand__Group__1__Impl rule__AdaptCommand__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__AdaptCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdaptCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__1"


    // $ANTLR start "rule__AdaptCommand__Group__1__Impl"
    // InternalMyDsl.g:827:1: rule__AdaptCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__AdaptCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:831:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:832:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:832:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:833:2: RULE_EXPRESSION
            {
             before(grammarAccess.getAdaptCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getAdaptCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__1__Impl"


    // $ANTLR start "rule__AdaptCommand__Group__2"
    // InternalMyDsl.g:842:1: rule__AdaptCommand__Group__2 : rule__AdaptCommand__Group__2__Impl rule__AdaptCommand__Group__3 ;
    public final void rule__AdaptCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:846:1: ( rule__AdaptCommand__Group__2__Impl rule__AdaptCommand__Group__3 )
            // InternalMyDsl.g:847:2: rule__AdaptCommand__Group__2__Impl rule__AdaptCommand__Group__3
            {
            pushFollow(FOLLOW_5);
            rule__AdaptCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdaptCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__2"


    // $ANTLR start "rule__AdaptCommand__Group__2__Impl"
    // InternalMyDsl.g:854:1: rule__AdaptCommand__Group__2__Impl : ( ( rule__AdaptCommand__Alternatives_2 )* ) ;
    public final void rule__AdaptCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:858:1: ( ( ( rule__AdaptCommand__Alternatives_2 )* ) )
            // InternalMyDsl.g:859:1: ( ( rule__AdaptCommand__Alternatives_2 )* )
            {
            // InternalMyDsl.g:859:1: ( ( rule__AdaptCommand__Alternatives_2 )* )
            // InternalMyDsl.g:860:2: ( rule__AdaptCommand__Alternatives_2 )*
            {
             before(grammarAccess.getAdaptCommandAccess().getAlternatives_2()); 
            // InternalMyDsl.g:861:2: ( rule__AdaptCommand__Alternatives_2 )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>=RULE_EXPRESSION && LA8_0<=RULE_WS)||LA8_0==15||LA8_0==22||LA8_0==24) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalMyDsl.g:861:3: rule__AdaptCommand__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_6);
            	    rule__AdaptCommand__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

             after(grammarAccess.getAdaptCommandAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__2__Impl"


    // $ANTLR start "rule__AdaptCommand__Group__3"
    // InternalMyDsl.g:869:1: rule__AdaptCommand__Group__3 : rule__AdaptCommand__Group__3__Impl ;
    public final void rule__AdaptCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:873:1: ( rule__AdaptCommand__Group__3__Impl )
            // InternalMyDsl.g:874:2: rule__AdaptCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AdaptCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__3"


    // $ANTLR start "rule__AdaptCommand__Group__3__Impl"
    // InternalMyDsl.g:880:1: rule__AdaptCommand__Group__3__Impl : ( '#endadapt' ) ;
    public final void rule__AdaptCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:884:1: ( ( '#endadapt' ) )
            // InternalMyDsl.g:885:1: ( '#endadapt' )
            {
            // InternalMyDsl.g:885:1: ( '#endadapt' )
            // InternalMyDsl.g:886:2: '#endadapt'
            {
             before(grammarAccess.getAdaptCommandAccess().getEndadaptKeyword_3()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getAdaptCommandAccess().getEndadaptKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdaptCommand__Group__3__Impl"


    // $ANTLR start "rule__InsertAfterCommand__Group__0"
    // InternalMyDsl.g:896:1: rule__InsertAfterCommand__Group__0 : rule__InsertAfterCommand__Group__0__Impl rule__InsertAfterCommand__Group__1 ;
    public final void rule__InsertAfterCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:900:1: ( rule__InsertAfterCommand__Group__0__Impl rule__InsertAfterCommand__Group__1 )
            // InternalMyDsl.g:901:2: rule__InsertAfterCommand__Group__0__Impl rule__InsertAfterCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__InsertAfterCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InsertAfterCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__0"


    // $ANTLR start "rule__InsertAfterCommand__Group__0__Impl"
    // InternalMyDsl.g:908:1: rule__InsertAfterCommand__Group__0__Impl : ( '#insert-after' ) ;
    public final void rule__InsertAfterCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:912:1: ( ( '#insert-after' ) )
            // InternalMyDsl.g:913:1: ( '#insert-after' )
            {
            // InternalMyDsl.g:913:1: ( '#insert-after' )
            // InternalMyDsl.g:914:2: '#insert-after'
            {
             before(grammarAccess.getInsertAfterCommandAccess().getInsertAfterKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getInsertAfterCommandAccess().getInsertAfterKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__0__Impl"


    // $ANTLR start "rule__InsertAfterCommand__Group__1"
    // InternalMyDsl.g:923:1: rule__InsertAfterCommand__Group__1 : rule__InsertAfterCommand__Group__1__Impl rule__InsertAfterCommand__Group__2 ;
    public final void rule__InsertAfterCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:927:1: ( rule__InsertAfterCommand__Group__1__Impl rule__InsertAfterCommand__Group__2 )
            // InternalMyDsl.g:928:2: rule__InsertAfterCommand__Group__1__Impl rule__InsertAfterCommand__Group__2
            {
            pushFollow(FOLLOW_7);
            rule__InsertAfterCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InsertAfterCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__1"


    // $ANTLR start "rule__InsertAfterCommand__Group__1__Impl"
    // InternalMyDsl.g:935:1: rule__InsertAfterCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__InsertAfterCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:939:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:940:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:940:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:941:2: RULE_EXPRESSION
            {
             before(grammarAccess.getInsertAfterCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getInsertAfterCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__1__Impl"


    // $ANTLR start "rule__InsertAfterCommand__Group__2"
    // InternalMyDsl.g:950:1: rule__InsertAfterCommand__Group__2 : rule__InsertAfterCommand__Group__2__Impl rule__InsertAfterCommand__Group__3 ;
    public final void rule__InsertAfterCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:954:1: ( rule__InsertAfterCommand__Group__2__Impl rule__InsertAfterCommand__Group__3 )
            // InternalMyDsl.g:955:2: rule__InsertAfterCommand__Group__2__Impl rule__InsertAfterCommand__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__InsertAfterCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__InsertAfterCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__2"


    // $ANTLR start "rule__InsertAfterCommand__Group__2__Impl"
    // InternalMyDsl.g:962:1: rule__InsertAfterCommand__Group__2__Impl : ( ( rule__InsertAfterCommand__Alternatives_2 )* ) ;
    public final void rule__InsertAfterCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:966:1: ( ( ( rule__InsertAfterCommand__Alternatives_2 )* ) )
            // InternalMyDsl.g:967:1: ( ( rule__InsertAfterCommand__Alternatives_2 )* )
            {
            // InternalMyDsl.g:967:1: ( ( rule__InsertAfterCommand__Alternatives_2 )* )
            // InternalMyDsl.g:968:2: ( rule__InsertAfterCommand__Alternatives_2 )*
            {
             before(grammarAccess.getInsertAfterCommandAccess().getAlternatives_2()); 
            // InternalMyDsl.g:969:2: ( rule__InsertAfterCommand__Alternatives_2 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>=RULE_EXPRESSION && LA9_0<=RULE_WS)||LA9_0==13||LA9_0==17||LA9_0==20||LA9_0==24||(LA9_0>=26 && LA9_0<=27)||(LA9_0>=30 && LA9_0<=34)) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:969:3: rule__InsertAfterCommand__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__InsertAfterCommand__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getInsertAfterCommandAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__2__Impl"


    // $ANTLR start "rule__InsertAfterCommand__Group__3"
    // InternalMyDsl.g:977:1: rule__InsertAfterCommand__Group__3 : rule__InsertAfterCommand__Group__3__Impl ;
    public final void rule__InsertAfterCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:981:1: ( rule__InsertAfterCommand__Group__3__Impl )
            // InternalMyDsl.g:982:2: rule__InsertAfterCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__InsertAfterCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__3"


    // $ANTLR start "rule__InsertAfterCommand__Group__3__Impl"
    // InternalMyDsl.g:988:1: rule__InsertAfterCommand__Group__3__Impl : ( '#endinsert' ) ;
    public final void rule__InsertAfterCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:992:1: ( ( '#endinsert' ) )
            // InternalMyDsl.g:993:1: ( '#endinsert' )
            {
            // InternalMyDsl.g:993:1: ( '#endinsert' )
            // InternalMyDsl.g:994:2: '#endinsert'
            {
             before(grammarAccess.getInsertAfterCommandAccess().getEndinsertKeyword_3()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getInsertAfterCommandAccess().getEndinsertKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__InsertAfterCommand__Group__3__Impl"


    // $ANTLR start "rule__SetloopCommand__Group__0"
    // InternalMyDsl.g:1004:1: rule__SetloopCommand__Group__0 : rule__SetloopCommand__Group__0__Impl rule__SetloopCommand__Group__1 ;
    public final void rule__SetloopCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1008:1: ( rule__SetloopCommand__Group__0__Impl rule__SetloopCommand__Group__1 )
            // InternalMyDsl.g:1009:2: rule__SetloopCommand__Group__0__Impl rule__SetloopCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__SetloopCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__0"


    // $ANTLR start "rule__SetloopCommand__Group__0__Impl"
    // InternalMyDsl.g:1016:1: rule__SetloopCommand__Group__0__Impl : ( '#setloop' ) ;
    public final void rule__SetloopCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1020:1: ( ( '#setloop' ) )
            // InternalMyDsl.g:1021:1: ( '#setloop' )
            {
            // InternalMyDsl.g:1021:1: ( '#setloop' )
            // InternalMyDsl.g:1022:2: '#setloop'
            {
             before(grammarAccess.getSetloopCommandAccess().getSetloopKeyword_0()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getSetloopCommandAccess().getSetloopKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__0__Impl"


    // $ANTLR start "rule__SetloopCommand__Group__1"
    // InternalMyDsl.g:1031:1: rule__SetloopCommand__Group__1 : rule__SetloopCommand__Group__1__Impl rule__SetloopCommand__Group__2 ;
    public final void rule__SetloopCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1035:1: ( rule__SetloopCommand__Group__1__Impl rule__SetloopCommand__Group__2 )
            // InternalMyDsl.g:1036:2: rule__SetloopCommand__Group__1__Impl rule__SetloopCommand__Group__2
            {
            pushFollow(FOLLOW_9);
            rule__SetloopCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__1"


    // $ANTLR start "rule__SetloopCommand__Group__1__Impl"
    // InternalMyDsl.g:1043:1: rule__SetloopCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__SetloopCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1047:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1048:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1048:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1049:2: RULE_EXPRESSION
            {
             before(grammarAccess.getSetloopCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getSetloopCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__1__Impl"


    // $ANTLR start "rule__SetloopCommand__Group__2"
    // InternalMyDsl.g:1058:1: rule__SetloopCommand__Group__2 : rule__SetloopCommand__Group__2__Impl rule__SetloopCommand__Group__3 ;
    public final void rule__SetloopCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1062:1: ( rule__SetloopCommand__Group__2__Impl rule__SetloopCommand__Group__3 )
            // InternalMyDsl.g:1063:2: rule__SetloopCommand__Group__2__Impl rule__SetloopCommand__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__SetloopCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__2"


    // $ANTLR start "rule__SetloopCommand__Group__2__Impl"
    // InternalMyDsl.g:1070:1: rule__SetloopCommand__Group__2__Impl : ( ( rule__SetloopCommand__Group_2__0 )* ) ;
    public final void rule__SetloopCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1074:1: ( ( ( rule__SetloopCommand__Group_2__0 )* ) )
            // InternalMyDsl.g:1075:1: ( ( rule__SetloopCommand__Group_2__0 )* )
            {
            // InternalMyDsl.g:1075:1: ( ( rule__SetloopCommand__Group_2__0 )* )
            // InternalMyDsl.g:1076:2: ( rule__SetloopCommand__Group_2__0 )*
            {
             before(grammarAccess.getSetloopCommandAccess().getGroup_2()); 
            // InternalMyDsl.g:1077:2: ( rule__SetloopCommand__Group_2__0 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==19) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:1077:3: rule__SetloopCommand__Group_2__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__SetloopCommand__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getSetloopCommandAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__2__Impl"


    // $ANTLR start "rule__SetloopCommand__Group__3"
    // InternalMyDsl.g:1085:1: rule__SetloopCommand__Group__3 : rule__SetloopCommand__Group__3__Impl ;
    public final void rule__SetloopCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1089:1: ( rule__SetloopCommand__Group__3__Impl )
            // InternalMyDsl.g:1090:2: rule__SetloopCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__3"


    // $ANTLR start "rule__SetloopCommand__Group__3__Impl"
    // InternalMyDsl.g:1096:1: rule__SetloopCommand__Group__3__Impl : ( '#endsetloop' ) ;
    public final void rule__SetloopCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1100:1: ( ( '#endsetloop' ) )
            // InternalMyDsl.g:1101:1: ( '#endsetloop' )
            {
            // InternalMyDsl.g:1101:1: ( '#endsetloop' )
            // InternalMyDsl.g:1102:2: '#endsetloop'
            {
             before(grammarAccess.getSetloopCommandAccess().getEndsetloopKeyword_3()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getSetloopCommandAccess().getEndsetloopKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group__3__Impl"


    // $ANTLR start "rule__SetloopCommand__Group_2__0"
    // InternalMyDsl.g:1112:1: rule__SetloopCommand__Group_2__0 : rule__SetloopCommand__Group_2__0__Impl rule__SetloopCommand__Group_2__1 ;
    public final void rule__SetloopCommand__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1116:1: ( rule__SetloopCommand__Group_2__0__Impl rule__SetloopCommand__Group_2__1 )
            // InternalMyDsl.g:1117:2: rule__SetloopCommand__Group_2__0__Impl rule__SetloopCommand__Group_2__1
            {
            pushFollow(FOLLOW_4);
            rule__SetloopCommand__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group_2__0"


    // $ANTLR start "rule__SetloopCommand__Group_2__0__Impl"
    // InternalMyDsl.g:1124:1: rule__SetloopCommand__Group_2__0__Impl : ( ',' ) ;
    public final void rule__SetloopCommand__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1128:1: ( ( ',' ) )
            // InternalMyDsl.g:1129:1: ( ',' )
            {
            // InternalMyDsl.g:1129:1: ( ',' )
            // InternalMyDsl.g:1130:2: ','
            {
             before(grammarAccess.getSetloopCommandAccess().getCommaKeyword_2_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSetloopCommandAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group_2__0__Impl"


    // $ANTLR start "rule__SetloopCommand__Group_2__1"
    // InternalMyDsl.g:1139:1: rule__SetloopCommand__Group_2__1 : rule__SetloopCommand__Group_2__1__Impl ;
    public final void rule__SetloopCommand__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1143:1: ( rule__SetloopCommand__Group_2__1__Impl )
            // InternalMyDsl.g:1144:2: rule__SetloopCommand__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SetloopCommand__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group_2__1"


    // $ANTLR start "rule__SetloopCommand__Group_2__1__Impl"
    // InternalMyDsl.g:1150:1: rule__SetloopCommand__Group_2__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__SetloopCommand__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1154:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1155:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1155:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1156:2: RULE_EXPRESSION
            {
             before(grammarAccess.getSetloopCommandAccess().getEXPRESSIONTerminalRuleCall_2_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getSetloopCommandAccess().getEXPRESSIONTerminalRuleCall_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetloopCommand__Group_2__1__Impl"


    // $ANTLR start "rule__BreakCommand__Group__0"
    // InternalMyDsl.g:1166:1: rule__BreakCommand__Group__0 : rule__BreakCommand__Group__0__Impl rule__BreakCommand__Group__1 ;
    public final void rule__BreakCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1170:1: ( rule__BreakCommand__Group__0__Impl rule__BreakCommand__Group__1 )
            // InternalMyDsl.g:1171:2: rule__BreakCommand__Group__0__Impl rule__BreakCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__BreakCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BreakCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__0"


    // $ANTLR start "rule__BreakCommand__Group__0__Impl"
    // InternalMyDsl.g:1178:1: rule__BreakCommand__Group__0__Impl : ( '#break' ) ;
    public final void rule__BreakCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1182:1: ( ( '#break' ) )
            // InternalMyDsl.g:1183:1: ( '#break' )
            {
            // InternalMyDsl.g:1183:1: ( '#break' )
            // InternalMyDsl.g:1184:2: '#break'
            {
             before(grammarAccess.getBreakCommandAccess().getBreakKeyword_0()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getBreakCommandAccess().getBreakKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__0__Impl"


    // $ANTLR start "rule__BreakCommand__Group__1"
    // InternalMyDsl.g:1193:1: rule__BreakCommand__Group__1 : rule__BreakCommand__Group__1__Impl rule__BreakCommand__Group__2 ;
    public final void rule__BreakCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1197:1: ( rule__BreakCommand__Group__1__Impl rule__BreakCommand__Group__2 )
            // InternalMyDsl.g:1198:2: rule__BreakCommand__Group__1__Impl rule__BreakCommand__Group__2
            {
            pushFollow(FOLLOW_11);
            rule__BreakCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BreakCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__1"


    // $ANTLR start "rule__BreakCommand__Group__1__Impl"
    // InternalMyDsl.g:1205:1: rule__BreakCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__BreakCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1209:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1210:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1210:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1211:2: RULE_EXPRESSION
            {
             before(grammarAccess.getBreakCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getBreakCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__1__Impl"


    // $ANTLR start "rule__BreakCommand__Group__2"
    // InternalMyDsl.g:1220:1: rule__BreakCommand__Group__2 : rule__BreakCommand__Group__2__Impl rule__BreakCommand__Group__3 ;
    public final void rule__BreakCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1224:1: ( rule__BreakCommand__Group__2__Impl rule__BreakCommand__Group__3 )
            // InternalMyDsl.g:1225:2: rule__BreakCommand__Group__2__Impl rule__BreakCommand__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__BreakCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BreakCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__2"


    // $ANTLR start "rule__BreakCommand__Group__2__Impl"
    // InternalMyDsl.g:1232:1: rule__BreakCommand__Group__2__Impl : ( ( rule__BreakCommand__Alternatives_2 )* ) ;
    public final void rule__BreakCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1236:1: ( ( ( rule__BreakCommand__Alternatives_2 )* ) )
            // InternalMyDsl.g:1237:1: ( ( rule__BreakCommand__Alternatives_2 )* )
            {
            // InternalMyDsl.g:1237:1: ( ( rule__BreakCommand__Alternatives_2 )* )
            // InternalMyDsl.g:1238:2: ( rule__BreakCommand__Alternatives_2 )*
            {
             before(grammarAccess.getBreakCommandAccess().getAlternatives_2()); 
            // InternalMyDsl.g:1239:2: ( rule__BreakCommand__Alternatives_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==13||LA11_0==17||LA11_0==20||LA11_0==24||(LA11_0>=26 && LA11_0<=27)||(LA11_0>=30 && LA11_0<=34)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalMyDsl.g:1239:3: rule__BreakCommand__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_8);
            	    rule__BreakCommand__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getBreakCommandAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__2__Impl"


    // $ANTLR start "rule__BreakCommand__Group__3"
    // InternalMyDsl.g:1247:1: rule__BreakCommand__Group__3 : rule__BreakCommand__Group__3__Impl ;
    public final void rule__BreakCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1251:1: ( rule__BreakCommand__Group__3__Impl )
            // InternalMyDsl.g:1252:2: rule__BreakCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BreakCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__3"


    // $ANTLR start "rule__BreakCommand__Group__3__Impl"
    // InternalMyDsl.g:1258:1: rule__BreakCommand__Group__3__Impl : ( '#endbreak' ) ;
    public final void rule__BreakCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1262:1: ( ( '#endbreak' ) )
            // InternalMyDsl.g:1263:1: ( '#endbreak' )
            {
            // InternalMyDsl.g:1263:1: ( '#endbreak' )
            // InternalMyDsl.g:1264:2: '#endbreak'
            {
             before(grammarAccess.getBreakCommandAccess().getEndbreakKeyword_3()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getBreakCommandAccess().getEndbreakKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BreakCommand__Group__3__Impl"


    // $ANTLR start "rule__OptionCommand__Group__0"
    // InternalMyDsl.g:1274:1: rule__OptionCommand__Group__0 : rule__OptionCommand__Group__0__Impl rule__OptionCommand__Group__1 ;
    public final void rule__OptionCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1278:1: ( rule__OptionCommand__Group__0__Impl rule__OptionCommand__Group__1 )
            // InternalMyDsl.g:1279:2: rule__OptionCommand__Group__0__Impl rule__OptionCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__OptionCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OptionCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__0"


    // $ANTLR start "rule__OptionCommand__Group__0__Impl"
    // InternalMyDsl.g:1286:1: rule__OptionCommand__Group__0__Impl : ( '#option' ) ;
    public final void rule__OptionCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1290:1: ( ( '#option' ) )
            // InternalMyDsl.g:1291:1: ( '#option' )
            {
            // InternalMyDsl.g:1291:1: ( '#option' )
            // InternalMyDsl.g:1292:2: '#option'
            {
             before(grammarAccess.getOptionCommandAccess().getOptionKeyword_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getOptionCommandAccess().getOptionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__0__Impl"


    // $ANTLR start "rule__OptionCommand__Group__1"
    // InternalMyDsl.g:1301:1: rule__OptionCommand__Group__1 : rule__OptionCommand__Group__1__Impl rule__OptionCommand__Group__2 ;
    public final void rule__OptionCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1305:1: ( rule__OptionCommand__Group__1__Impl rule__OptionCommand__Group__2 )
            // InternalMyDsl.g:1306:2: rule__OptionCommand__Group__1__Impl rule__OptionCommand__Group__2
            {
            pushFollow(FOLLOW_12);
            rule__OptionCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OptionCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__1"


    // $ANTLR start "rule__OptionCommand__Group__1__Impl"
    // InternalMyDsl.g:1313:1: rule__OptionCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__OptionCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1317:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1318:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1318:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1319:2: RULE_EXPRESSION
            {
             before(grammarAccess.getOptionCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getOptionCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__1__Impl"


    // $ANTLR start "rule__OptionCommand__Group__2"
    // InternalMyDsl.g:1328:1: rule__OptionCommand__Group__2 : rule__OptionCommand__Group__2__Impl rule__OptionCommand__Group__3 ;
    public final void rule__OptionCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1332:1: ( rule__OptionCommand__Group__2__Impl rule__OptionCommand__Group__3 )
            // InternalMyDsl.g:1333:2: rule__OptionCommand__Group__2__Impl rule__OptionCommand__Group__3
            {
            pushFollow(FOLLOW_12);
            rule__OptionCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OptionCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__2"


    // $ANTLR start "rule__OptionCommand__Group__2__Impl"
    // InternalMyDsl.g:1340:1: rule__OptionCommand__Group__2__Impl : ( ( rule__OptionCommand__Alternatives_2 )* ) ;
    public final void rule__OptionCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1344:1: ( ( ( rule__OptionCommand__Alternatives_2 )* ) )
            // InternalMyDsl.g:1345:1: ( ( rule__OptionCommand__Alternatives_2 )* )
            {
            // InternalMyDsl.g:1345:1: ( ( rule__OptionCommand__Alternatives_2 )* )
            // InternalMyDsl.g:1346:2: ( rule__OptionCommand__Alternatives_2 )*
            {
             before(grammarAccess.getOptionCommandAccess().getAlternatives_2()); 
            // InternalMyDsl.g:1347:2: ( rule__OptionCommand__Alternatives_2 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>=RULE_EXPRESSION && LA12_0<=RULE_WS)||LA12_0==12||LA12_0==15) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalMyDsl.g:1347:3: rule__OptionCommand__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__OptionCommand__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getOptionCommandAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__2__Impl"


    // $ANTLR start "rule__OptionCommand__Group__3"
    // InternalMyDsl.g:1355:1: rule__OptionCommand__Group__3 : rule__OptionCommand__Group__3__Impl ;
    public final void rule__OptionCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1359:1: ( rule__OptionCommand__Group__3__Impl )
            // InternalMyDsl.g:1360:2: rule__OptionCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OptionCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__3"


    // $ANTLR start "rule__OptionCommand__Group__3__Impl"
    // InternalMyDsl.g:1366:1: rule__OptionCommand__Group__3__Impl : ( '#endoption' ) ;
    public final void rule__OptionCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1370:1: ( ( '#endoption' ) )
            // InternalMyDsl.g:1371:1: ( '#endoption' )
            {
            // InternalMyDsl.g:1371:1: ( '#endoption' )
            // InternalMyDsl.g:1372:2: '#endoption'
            {
             before(grammarAccess.getOptionCommandAccess().getEndoptionKeyword_3()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getOptionCommandAccess().getEndoptionKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OptionCommand__Group__3__Impl"


    // $ANTLR start "rule__SelectCommand__Group__0"
    // InternalMyDsl.g:1382:1: rule__SelectCommand__Group__0 : rule__SelectCommand__Group__0__Impl rule__SelectCommand__Group__1 ;
    public final void rule__SelectCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1386:1: ( rule__SelectCommand__Group__0__Impl rule__SelectCommand__Group__1 )
            // InternalMyDsl.g:1387:2: rule__SelectCommand__Group__0__Impl rule__SelectCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__SelectCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SelectCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__0"


    // $ANTLR start "rule__SelectCommand__Group__0__Impl"
    // InternalMyDsl.g:1394:1: rule__SelectCommand__Group__0__Impl : ( '#select' ) ;
    public final void rule__SelectCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1398:1: ( ( '#select' ) )
            // InternalMyDsl.g:1399:1: ( '#select' )
            {
            // InternalMyDsl.g:1399:1: ( '#select' )
            // InternalMyDsl.g:1400:2: '#select'
            {
             before(grammarAccess.getSelectCommandAccess().getSelectKeyword_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getSelectCommandAccess().getSelectKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__0__Impl"


    // $ANTLR start "rule__SelectCommand__Group__1"
    // InternalMyDsl.g:1409:1: rule__SelectCommand__Group__1 : rule__SelectCommand__Group__1__Impl rule__SelectCommand__Group__2 ;
    public final void rule__SelectCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1413:1: ( rule__SelectCommand__Group__1__Impl rule__SelectCommand__Group__2 )
            // InternalMyDsl.g:1414:2: rule__SelectCommand__Group__1__Impl rule__SelectCommand__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__SelectCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SelectCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__1"


    // $ANTLR start "rule__SelectCommand__Group__1__Impl"
    // InternalMyDsl.g:1421:1: rule__SelectCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__SelectCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1425:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1426:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1426:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1427:2: RULE_EXPRESSION
            {
             before(grammarAccess.getSelectCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getSelectCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__1__Impl"


    // $ANTLR start "rule__SelectCommand__Group__2"
    // InternalMyDsl.g:1436:1: rule__SelectCommand__Group__2 : rule__SelectCommand__Group__2__Impl rule__SelectCommand__Group__3 ;
    public final void rule__SelectCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1440:1: ( rule__SelectCommand__Group__2__Impl rule__SelectCommand__Group__3 )
            // InternalMyDsl.g:1441:2: rule__SelectCommand__Group__2__Impl rule__SelectCommand__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__SelectCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SelectCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__2"


    // $ANTLR start "rule__SelectCommand__Group__2__Impl"
    // InternalMyDsl.g:1448:1: rule__SelectCommand__Group__2__Impl : ( ( ruleOptionCommand )* ) ;
    public final void rule__SelectCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1452:1: ( ( ( ruleOptionCommand )* ) )
            // InternalMyDsl.g:1453:1: ( ( ruleOptionCommand )* )
            {
            // InternalMyDsl.g:1453:1: ( ( ruleOptionCommand )* )
            // InternalMyDsl.g:1454:2: ( ruleOptionCommand )*
            {
             before(grammarAccess.getSelectCommandAccess().getOptionCommandParserRuleCall_2()); 
            // InternalMyDsl.g:1455:2: ( ruleOptionCommand )*
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( (LA13_0==22) ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalMyDsl.g:1455:3: ruleOptionCommand
            	    {
            	    pushFollow(FOLLOW_15);
            	    ruleOptionCommand();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop13;
                }
            } while (true);

             after(grammarAccess.getSelectCommandAccess().getOptionCommandParserRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__2__Impl"


    // $ANTLR start "rule__SelectCommand__Group__3"
    // InternalMyDsl.g:1463:1: rule__SelectCommand__Group__3 : rule__SelectCommand__Group__3__Impl ;
    public final void rule__SelectCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1467:1: ( rule__SelectCommand__Group__3__Impl )
            // InternalMyDsl.g:1468:2: rule__SelectCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SelectCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__3"


    // $ANTLR start "rule__SelectCommand__Group__3__Impl"
    // InternalMyDsl.g:1474:1: rule__SelectCommand__Group__3__Impl : ( '#endselect' ) ;
    public final void rule__SelectCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1478:1: ( ( '#endselect' ) )
            // InternalMyDsl.g:1479:1: ( '#endselect' )
            {
            // InternalMyDsl.g:1479:1: ( '#endselect' )
            // InternalMyDsl.g:1480:2: '#endselect'
            {
             before(grammarAccess.getSelectCommandAccess().getEndselectKeyword_3()); 
            match(input,25,FOLLOW_2); 
             after(grammarAccess.getSelectCommandAccess().getEndselectKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SelectCommand__Group__3__Impl"


    // $ANTLR start "rule__OutputCommand__Group__0"
    // InternalMyDsl.g:1490:1: rule__OutputCommand__Group__0 : rule__OutputCommand__Group__0__Impl rule__OutputCommand__Group__1 ;
    public final void rule__OutputCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1494:1: ( rule__OutputCommand__Group__0__Impl rule__OutputCommand__Group__1 )
            // InternalMyDsl.g:1495:2: rule__OutputCommand__Group__0__Impl rule__OutputCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__OutputCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OutputCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputCommand__Group__0"


    // $ANTLR start "rule__OutputCommand__Group__0__Impl"
    // InternalMyDsl.g:1502:1: rule__OutputCommand__Group__0__Impl : ( '#output' ) ;
    public final void rule__OutputCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1506:1: ( ( '#output' ) )
            // InternalMyDsl.g:1507:1: ( '#output' )
            {
            // InternalMyDsl.g:1507:1: ( '#output' )
            // InternalMyDsl.g:1508:2: '#output'
            {
             before(grammarAccess.getOutputCommandAccess().getOutputKeyword_0()); 
            match(input,26,FOLLOW_2); 
             after(grammarAccess.getOutputCommandAccess().getOutputKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputCommand__Group__0__Impl"


    // $ANTLR start "rule__OutputCommand__Group__1"
    // InternalMyDsl.g:1517:1: rule__OutputCommand__Group__1 : rule__OutputCommand__Group__1__Impl rule__OutputCommand__Group__2 ;
    public final void rule__OutputCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1521:1: ( rule__OutputCommand__Group__1__Impl rule__OutputCommand__Group__2 )
            // InternalMyDsl.g:1522:2: rule__OutputCommand__Group__1__Impl rule__OutputCommand__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__OutputCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OutputCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputCommand__Group__1"


    // $ANTLR start "rule__OutputCommand__Group__1__Impl"
    // InternalMyDsl.g:1529:1: rule__OutputCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__OutputCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1533:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1534:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1534:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1535:2: RULE_EXPRESSION
            {
             before(grammarAccess.getOutputCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getOutputCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputCommand__Group__1__Impl"


    // $ANTLR start "rule__OutputCommand__Group__2"
    // InternalMyDsl.g:1544:1: rule__OutputCommand__Group__2 : rule__OutputCommand__Group__2__Impl ;
    public final void rule__OutputCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1548:1: ( rule__OutputCommand__Group__2__Impl )
            // InternalMyDsl.g:1549:2: rule__OutputCommand__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OutputCommand__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputCommand__Group__2"


    // $ANTLR start "rule__OutputCommand__Group__2__Impl"
    // InternalMyDsl.g:1555:1: rule__OutputCommand__Group__2__Impl : ( ( RULE_WS )* ) ;
    public final void rule__OutputCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1559:1: ( ( ( RULE_WS )* ) )
            // InternalMyDsl.g:1560:1: ( ( RULE_WS )* )
            {
            // InternalMyDsl.g:1560:1: ( ( RULE_WS )* )
            // InternalMyDsl.g:1561:2: ( RULE_WS )*
            {
             before(grammarAccess.getOutputCommandAccess().getWSTerminalRuleCall_2()); 
            // InternalMyDsl.g:1562:2: ( RULE_WS )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_WS) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalMyDsl.g:1562:3: RULE_WS
            	    {
            	    match(input,RULE_WS,FOLLOW_17); 

            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

             after(grammarAccess.getOutputCommandAccess().getWSTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputCommand__Group__2__Impl"


    // $ANTLR start "rule__WhileCommand__Group__0"
    // InternalMyDsl.g:1571:1: rule__WhileCommand__Group__0 : rule__WhileCommand__Group__0__Impl rule__WhileCommand__Group__1 ;
    public final void rule__WhileCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1575:1: ( rule__WhileCommand__Group__0__Impl rule__WhileCommand__Group__1 )
            // InternalMyDsl.g:1576:2: rule__WhileCommand__Group__0__Impl rule__WhileCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__WhileCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__0"


    // $ANTLR start "rule__WhileCommand__Group__0__Impl"
    // InternalMyDsl.g:1583:1: rule__WhileCommand__Group__0__Impl : ( '#while' ) ;
    public final void rule__WhileCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1587:1: ( ( '#while' ) )
            // InternalMyDsl.g:1588:1: ( '#while' )
            {
            // InternalMyDsl.g:1588:1: ( '#while' )
            // InternalMyDsl.g:1589:2: '#while'
            {
             before(grammarAccess.getWhileCommandAccess().getWhileKeyword_0()); 
            match(input,27,FOLLOW_2); 
             after(grammarAccess.getWhileCommandAccess().getWhileKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__0__Impl"


    // $ANTLR start "rule__WhileCommand__Group__1"
    // InternalMyDsl.g:1598:1: rule__WhileCommand__Group__1 : rule__WhileCommand__Group__1__Impl rule__WhileCommand__Group__2 ;
    public final void rule__WhileCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1602:1: ( rule__WhileCommand__Group__1__Impl rule__WhileCommand__Group__2 )
            // InternalMyDsl.g:1603:2: rule__WhileCommand__Group__1__Impl rule__WhileCommand__Group__2
            {
            pushFollow(FOLLOW_18);
            rule__WhileCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__1"


    // $ANTLR start "rule__WhileCommand__Group__1__Impl"
    // InternalMyDsl.g:1610:1: rule__WhileCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__WhileCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1614:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1615:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1615:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1616:2: RULE_EXPRESSION
            {
             before(grammarAccess.getWhileCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getWhileCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__1__Impl"


    // $ANTLR start "rule__WhileCommand__Group__2"
    // InternalMyDsl.g:1625:1: rule__WhileCommand__Group__2 : rule__WhileCommand__Group__2__Impl rule__WhileCommand__Group__3 ;
    public final void rule__WhileCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1629:1: ( rule__WhileCommand__Group__2__Impl rule__WhileCommand__Group__3 )
            // InternalMyDsl.g:1630:2: rule__WhileCommand__Group__2__Impl rule__WhileCommand__Group__3
            {
            pushFollow(FOLLOW_18);
            rule__WhileCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__2"


    // $ANTLR start "rule__WhileCommand__Group__2__Impl"
    // InternalMyDsl.g:1637:1: rule__WhileCommand__Group__2__Impl : ( ( rule__WhileCommand__Alternatives_2 )* ) ;
    public final void rule__WhileCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1641:1: ( ( ( rule__WhileCommand__Alternatives_2 )* ) )
            // InternalMyDsl.g:1642:1: ( ( rule__WhileCommand__Alternatives_2 )* )
            {
            // InternalMyDsl.g:1642:1: ( ( rule__WhileCommand__Alternatives_2 )* )
            // InternalMyDsl.g:1643:2: ( rule__WhileCommand__Alternatives_2 )*
            {
             before(grammarAccess.getWhileCommandAccess().getAlternatives_2()); 
            // InternalMyDsl.g:1644:2: ( rule__WhileCommand__Alternatives_2 )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_WS||LA15_0==13||LA15_0==19||LA15_0==22||LA15_0==24||LA15_0==26) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalMyDsl.g:1644:3: rule__WhileCommand__Alternatives_2
            	    {
            	    pushFollow(FOLLOW_19);
            	    rule__WhileCommand__Alternatives_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

             after(grammarAccess.getWhileCommandAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__2__Impl"


    // $ANTLR start "rule__WhileCommand__Group__3"
    // InternalMyDsl.g:1652:1: rule__WhileCommand__Group__3 : rule__WhileCommand__Group__3__Impl ;
    public final void rule__WhileCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1656:1: ( rule__WhileCommand__Group__3__Impl )
            // InternalMyDsl.g:1657:2: rule__WhileCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__3"


    // $ANTLR start "rule__WhileCommand__Group__3__Impl"
    // InternalMyDsl.g:1663:1: rule__WhileCommand__Group__3__Impl : ( '#endwhile' ) ;
    public final void rule__WhileCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1667:1: ( ( '#endwhile' ) )
            // InternalMyDsl.g:1668:1: ( '#endwhile' )
            {
            // InternalMyDsl.g:1668:1: ( '#endwhile' )
            // InternalMyDsl.g:1669:2: '#endwhile'
            {
             before(grammarAccess.getWhileCommandAccess().getEndwhileKeyword_3()); 
            match(input,28,FOLLOW_2); 
             after(grammarAccess.getWhileCommandAccess().getEndwhileKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group__3__Impl"


    // $ANTLR start "rule__WhileCommand__Group_2_0__0"
    // InternalMyDsl.g:1679:1: rule__WhileCommand__Group_2_0__0 : rule__WhileCommand__Group_2_0__0__Impl rule__WhileCommand__Group_2_0__1 ;
    public final void rule__WhileCommand__Group_2_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1683:1: ( rule__WhileCommand__Group_2_0__0__Impl rule__WhileCommand__Group_2_0__1 )
            // InternalMyDsl.g:1684:2: rule__WhileCommand__Group_2_0__0__Impl rule__WhileCommand__Group_2_0__1
            {
            pushFollow(FOLLOW_4);
            rule__WhileCommand__Group_2_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group_2_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group_2_0__0"


    // $ANTLR start "rule__WhileCommand__Group_2_0__0__Impl"
    // InternalMyDsl.g:1691:1: rule__WhileCommand__Group_2_0__0__Impl : ( ',' ) ;
    public final void rule__WhileCommand__Group_2_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1695:1: ( ( ',' ) )
            // InternalMyDsl.g:1696:1: ( ',' )
            {
            // InternalMyDsl.g:1696:1: ( ',' )
            // InternalMyDsl.g:1697:2: ','
            {
             before(grammarAccess.getWhileCommandAccess().getCommaKeyword_2_0_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getWhileCommandAccess().getCommaKeyword_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group_2_0__0__Impl"


    // $ANTLR start "rule__WhileCommand__Group_2_0__1"
    // InternalMyDsl.g:1706:1: rule__WhileCommand__Group_2_0__1 : rule__WhileCommand__Group_2_0__1__Impl ;
    public final void rule__WhileCommand__Group_2_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1710:1: ( rule__WhileCommand__Group_2_0__1__Impl )
            // InternalMyDsl.g:1711:2: rule__WhileCommand__Group_2_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WhileCommand__Group_2_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group_2_0__1"


    // $ANTLR start "rule__WhileCommand__Group_2_0__1__Impl"
    // InternalMyDsl.g:1717:1: rule__WhileCommand__Group_2_0__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__WhileCommand__Group_2_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1721:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1722:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1722:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1723:2: RULE_EXPRESSION
            {
             before(grammarAccess.getWhileCommandAccess().getEXPRESSIONTerminalRuleCall_2_0_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getWhileCommandAccess().getEXPRESSIONTerminalRuleCall_2_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileCommand__Group_2_0__1__Impl"


    // $ANTLR start "rule__CommentCommand__Group__0"
    // InternalMyDsl.g:1733:1: rule__CommentCommand__Group__0 : rule__CommentCommand__Group__0__Impl rule__CommentCommand__Group__1 ;
    public final void rule__CommentCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1737:1: ( rule__CommentCommand__Group__0__Impl rule__CommentCommand__Group__1 )
            // InternalMyDsl.g:1738:2: rule__CommentCommand__Group__0__Impl rule__CommentCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__CommentCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__CommentCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommentCommand__Group__0"


    // $ANTLR start "rule__CommentCommand__Group__0__Impl"
    // InternalMyDsl.g:1745:1: rule__CommentCommand__Group__0__Impl : ( '%' ) ;
    public final void rule__CommentCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1749:1: ( ( '%' ) )
            // InternalMyDsl.g:1750:1: ( '%' )
            {
            // InternalMyDsl.g:1750:1: ( '%' )
            // InternalMyDsl.g:1751:2: '%'
            {
             before(grammarAccess.getCommentCommandAccess().getPercentSignKeyword_0()); 
            match(input,29,FOLLOW_2); 
             after(grammarAccess.getCommentCommandAccess().getPercentSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommentCommand__Group__0__Impl"


    // $ANTLR start "rule__CommentCommand__Group__1"
    // InternalMyDsl.g:1760:1: rule__CommentCommand__Group__1 : rule__CommentCommand__Group__1__Impl ;
    public final void rule__CommentCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1764:1: ( rule__CommentCommand__Group__1__Impl )
            // InternalMyDsl.g:1765:2: rule__CommentCommand__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__CommentCommand__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommentCommand__Group__1"


    // $ANTLR start "rule__CommentCommand__Group__1__Impl"
    // InternalMyDsl.g:1771:1: rule__CommentCommand__Group__1__Impl : ( ( RULE_EXPRESSION )* ) ;
    public final void rule__CommentCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1775:1: ( ( ( RULE_EXPRESSION )* ) )
            // InternalMyDsl.g:1776:1: ( ( RULE_EXPRESSION )* )
            {
            // InternalMyDsl.g:1776:1: ( ( RULE_EXPRESSION )* )
            // InternalMyDsl.g:1777:2: ( RULE_EXPRESSION )*
            {
             before(grammarAccess.getCommentCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            // InternalMyDsl.g:1778:2: ( RULE_EXPRESSION )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_EXPRESSION) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalMyDsl.g:1778:3: RULE_EXPRESSION
            	    {
            	    match(input,RULE_EXPRESSION,FOLLOW_20); 

            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

             after(grammarAccess.getCommentCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CommentCommand__Group__1__Impl"


    // $ANTLR start "rule__SetCommand__Group__0"
    // InternalMyDsl.g:1787:1: rule__SetCommand__Group__0 : rule__SetCommand__Group__0__Impl rule__SetCommand__Group__1 ;
    public final void rule__SetCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1791:1: ( rule__SetCommand__Group__0__Impl rule__SetCommand__Group__1 )
            // InternalMyDsl.g:1792:2: rule__SetCommand__Group__0__Impl rule__SetCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__SetCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__0"


    // $ANTLR start "rule__SetCommand__Group__0__Impl"
    // InternalMyDsl.g:1799:1: rule__SetCommand__Group__0__Impl : ( '#set' ) ;
    public final void rule__SetCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1803:1: ( ( '#set' ) )
            // InternalMyDsl.g:1804:1: ( '#set' )
            {
            // InternalMyDsl.g:1804:1: ( '#set' )
            // InternalMyDsl.g:1805:2: '#set'
            {
             before(grammarAccess.getSetCommandAccess().getSetKeyword_0()); 
            match(input,30,FOLLOW_2); 
             after(grammarAccess.getSetCommandAccess().getSetKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__0__Impl"


    // $ANTLR start "rule__SetCommand__Group__1"
    // InternalMyDsl.g:1814:1: rule__SetCommand__Group__1 : rule__SetCommand__Group__1__Impl rule__SetCommand__Group__2 ;
    public final void rule__SetCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1818:1: ( rule__SetCommand__Group__1__Impl rule__SetCommand__Group__2 )
            // InternalMyDsl.g:1819:2: rule__SetCommand__Group__1__Impl rule__SetCommand__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__SetCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__1"


    // $ANTLR start "rule__SetCommand__Group__1__Impl"
    // InternalMyDsl.g:1826:1: rule__SetCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__SetCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1830:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1831:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1831:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1832:2: RULE_EXPRESSION
            {
             before(grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__1__Impl"


    // $ANTLR start "rule__SetCommand__Group__2"
    // InternalMyDsl.g:1841:1: rule__SetCommand__Group__2 : rule__SetCommand__Group__2__Impl rule__SetCommand__Group__3 ;
    public final void rule__SetCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1845:1: ( rule__SetCommand__Group__2__Impl rule__SetCommand__Group__3 )
            // InternalMyDsl.g:1846:2: rule__SetCommand__Group__2__Impl rule__SetCommand__Group__3
            {
            pushFollow(FOLLOW_22);
            rule__SetCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__2"


    // $ANTLR start "rule__SetCommand__Group__2__Impl"
    // InternalMyDsl.g:1853:1: rule__SetCommand__Group__2__Impl : ( '=' ) ;
    public final void rule__SetCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1857:1: ( ( '=' ) )
            // InternalMyDsl.g:1858:1: ( '=' )
            {
            // InternalMyDsl.g:1858:1: ( '=' )
            // InternalMyDsl.g:1859:2: '='
            {
             before(grammarAccess.getSetCommandAccess().getEqualsSignKeyword_2()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getSetCommandAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__2__Impl"


    // $ANTLR start "rule__SetCommand__Group__3"
    // InternalMyDsl.g:1868:1: rule__SetCommand__Group__3 : rule__SetCommand__Group__3__Impl rule__SetCommand__Group__4 ;
    public final void rule__SetCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1872:1: ( rule__SetCommand__Group__3__Impl rule__SetCommand__Group__4 )
            // InternalMyDsl.g:1873:2: rule__SetCommand__Group__3__Impl rule__SetCommand__Group__4
            {
            pushFollow(FOLLOW_22);
            rule__SetCommand__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetCommand__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__3"


    // $ANTLR start "rule__SetCommand__Group__3__Impl"
    // InternalMyDsl.g:1880:1: rule__SetCommand__Group__3__Impl : ( ( RULE_EXPRESSION )? ) ;
    public final void rule__SetCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1884:1: ( ( ( RULE_EXPRESSION )? ) )
            // InternalMyDsl.g:1885:1: ( ( RULE_EXPRESSION )? )
            {
            // InternalMyDsl.g:1885:1: ( ( RULE_EXPRESSION )? )
            // InternalMyDsl.g:1886:2: ( RULE_EXPRESSION )?
            {
             before(grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_3()); 
            // InternalMyDsl.g:1887:2: ( RULE_EXPRESSION )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_EXPRESSION) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // InternalMyDsl.g:1887:3: RULE_EXPRESSION
                    {
                    match(input,RULE_EXPRESSION,FOLLOW_2); 

                    }
                    break;

            }

             after(grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__3__Impl"


    // $ANTLR start "rule__SetCommand__Group__4"
    // InternalMyDsl.g:1895:1: rule__SetCommand__Group__4 : rule__SetCommand__Group__4__Impl ;
    public final void rule__SetCommand__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1899:1: ( rule__SetCommand__Group__4__Impl )
            // InternalMyDsl.g:1900:2: rule__SetCommand__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SetCommand__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__4"


    // $ANTLR start "rule__SetCommand__Group__4__Impl"
    // InternalMyDsl.g:1906:1: rule__SetCommand__Group__4__Impl : ( ( rule__SetCommand__Group_4__0 )* ) ;
    public final void rule__SetCommand__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1910:1: ( ( ( rule__SetCommand__Group_4__0 )* ) )
            // InternalMyDsl.g:1911:1: ( ( rule__SetCommand__Group_4__0 )* )
            {
            // InternalMyDsl.g:1911:1: ( ( rule__SetCommand__Group_4__0 )* )
            // InternalMyDsl.g:1912:2: ( rule__SetCommand__Group_4__0 )*
            {
             before(grammarAccess.getSetCommandAccess().getGroup_4()); 
            // InternalMyDsl.g:1913:2: ( rule__SetCommand__Group_4__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==19) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalMyDsl.g:1913:3: rule__SetCommand__Group_4__0
            	    {
            	    pushFollow(FOLLOW_10);
            	    rule__SetCommand__Group_4__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getSetCommandAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group__4__Impl"


    // $ANTLR start "rule__SetCommand__Group_4__0"
    // InternalMyDsl.g:1922:1: rule__SetCommand__Group_4__0 : rule__SetCommand__Group_4__0__Impl rule__SetCommand__Group_4__1 ;
    public final void rule__SetCommand__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1926:1: ( rule__SetCommand__Group_4__0__Impl rule__SetCommand__Group_4__1 )
            // InternalMyDsl.g:1927:2: rule__SetCommand__Group_4__0__Impl rule__SetCommand__Group_4__1
            {
            pushFollow(FOLLOW_4);
            rule__SetCommand__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__SetCommand__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group_4__0"


    // $ANTLR start "rule__SetCommand__Group_4__0__Impl"
    // InternalMyDsl.g:1934:1: rule__SetCommand__Group_4__0__Impl : ( ',' ) ;
    public final void rule__SetCommand__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1938:1: ( ( ',' ) )
            // InternalMyDsl.g:1939:1: ( ',' )
            {
            // InternalMyDsl.g:1939:1: ( ',' )
            // InternalMyDsl.g:1940:2: ','
            {
             before(grammarAccess.getSetCommandAccess().getCommaKeyword_4_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getSetCommandAccess().getCommaKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group_4__0__Impl"


    // $ANTLR start "rule__SetCommand__Group_4__1"
    // InternalMyDsl.g:1949:1: rule__SetCommand__Group_4__1 : rule__SetCommand__Group_4__1__Impl ;
    public final void rule__SetCommand__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1953:1: ( rule__SetCommand__Group_4__1__Impl )
            // InternalMyDsl.g:1954:2: rule__SetCommand__Group_4__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__SetCommand__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group_4__1"


    // $ANTLR start "rule__SetCommand__Group_4__1__Impl"
    // InternalMyDsl.g:1960:1: rule__SetCommand__Group_4__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__SetCommand__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1964:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:1965:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:1965:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:1966:2: RULE_EXPRESSION
            {
             before(grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_4_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getSetCommandAccess().getEXPRESSIONTerminalRuleCall_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__SetCommand__Group_4__1__Impl"


    // $ANTLR start "rule__OutdirCommand__Group__0"
    // InternalMyDsl.g:1976:1: rule__OutdirCommand__Group__0 : rule__OutdirCommand__Group__0__Impl rule__OutdirCommand__Group__1 ;
    public final void rule__OutdirCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1980:1: ( rule__OutdirCommand__Group__0__Impl rule__OutdirCommand__Group__1 )
            // InternalMyDsl.g:1981:2: rule__OutdirCommand__Group__0__Impl rule__OutdirCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__OutdirCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OutdirCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutdirCommand__Group__0"


    // $ANTLR start "rule__OutdirCommand__Group__0__Impl"
    // InternalMyDsl.g:1988:1: rule__OutdirCommand__Group__0__Impl : ( '#outdir' ) ;
    public final void rule__OutdirCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:1992:1: ( ( '#outdir' ) )
            // InternalMyDsl.g:1993:1: ( '#outdir' )
            {
            // InternalMyDsl.g:1993:1: ( '#outdir' )
            // InternalMyDsl.g:1994:2: '#outdir'
            {
             before(grammarAccess.getOutdirCommandAccess().getOutdirKeyword_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getOutdirCommandAccess().getOutdirKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutdirCommand__Group__0__Impl"


    // $ANTLR start "rule__OutdirCommand__Group__1"
    // InternalMyDsl.g:2003:1: rule__OutdirCommand__Group__1 : rule__OutdirCommand__Group__1__Impl ;
    public final void rule__OutdirCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2007:1: ( rule__OutdirCommand__Group__1__Impl )
            // InternalMyDsl.g:2008:2: rule__OutdirCommand__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OutdirCommand__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutdirCommand__Group__1"


    // $ANTLR start "rule__OutdirCommand__Group__1__Impl"
    // InternalMyDsl.g:2014:1: rule__OutdirCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__OutdirCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2018:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:2019:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:2019:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:2020:2: RULE_EXPRESSION
            {
             before(grammarAccess.getOutdirCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getOutdirCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutdirCommand__Group__1__Impl"


    // $ANTLR start "rule__OutfileCommand__Group__0"
    // InternalMyDsl.g:2030:1: rule__OutfileCommand__Group__0 : rule__OutfileCommand__Group__0__Impl rule__OutfileCommand__Group__1 ;
    public final void rule__OutfileCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2034:1: ( rule__OutfileCommand__Group__0__Impl rule__OutfileCommand__Group__1 )
            // InternalMyDsl.g:2035:2: rule__OutfileCommand__Group__0__Impl rule__OutfileCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__OutfileCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OutfileCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutfileCommand__Group__0"


    // $ANTLR start "rule__OutfileCommand__Group__0__Impl"
    // InternalMyDsl.g:2042:1: rule__OutfileCommand__Group__0__Impl : ( '#outfile' ) ;
    public final void rule__OutfileCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2046:1: ( ( '#outfile' ) )
            // InternalMyDsl.g:2047:1: ( '#outfile' )
            {
            // InternalMyDsl.g:2047:1: ( '#outfile' )
            // InternalMyDsl.g:2048:2: '#outfile'
            {
             before(grammarAccess.getOutfileCommandAccess().getOutfileKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getOutfileCommandAccess().getOutfileKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutfileCommand__Group__0__Impl"


    // $ANTLR start "rule__OutfileCommand__Group__1"
    // InternalMyDsl.g:2057:1: rule__OutfileCommand__Group__1 : rule__OutfileCommand__Group__1__Impl ;
    public final void rule__OutfileCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2061:1: ( rule__OutfileCommand__Group__1__Impl )
            // InternalMyDsl.g:2062:2: rule__OutfileCommand__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OutfileCommand__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutfileCommand__Group__1"


    // $ANTLR start "rule__OutfileCommand__Group__1__Impl"
    // InternalMyDsl.g:2068:1: rule__OutfileCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__OutfileCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2072:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:2073:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:2073:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:2074:2: RULE_EXPRESSION
            {
             before(grammarAccess.getOutfileCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getOutfileCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutfileCommand__Group__1__Impl"


    // $ANTLR start "rule__RemoveCommand__Group__0"
    // InternalMyDsl.g:2084:1: rule__RemoveCommand__Group__0 : rule__RemoveCommand__Group__0__Impl rule__RemoveCommand__Group__1 ;
    public final void rule__RemoveCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2088:1: ( rule__RemoveCommand__Group__0__Impl rule__RemoveCommand__Group__1 )
            // InternalMyDsl.g:2089:2: rule__RemoveCommand__Group__0__Impl rule__RemoveCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__RemoveCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__RemoveCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RemoveCommand__Group__0"


    // $ANTLR start "rule__RemoveCommand__Group__0__Impl"
    // InternalMyDsl.g:2096:1: rule__RemoveCommand__Group__0__Impl : ( '#remove' ) ;
    public final void rule__RemoveCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2100:1: ( ( '#remove' ) )
            // InternalMyDsl.g:2101:1: ( '#remove' )
            {
            // InternalMyDsl.g:2101:1: ( '#remove' )
            // InternalMyDsl.g:2102:2: '#remove'
            {
             before(grammarAccess.getRemoveCommandAccess().getRemoveKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getRemoveCommandAccess().getRemoveKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RemoveCommand__Group__0__Impl"


    // $ANTLR start "rule__RemoveCommand__Group__1"
    // InternalMyDsl.g:2111:1: rule__RemoveCommand__Group__1 : rule__RemoveCommand__Group__1__Impl ;
    public final void rule__RemoveCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2115:1: ( rule__RemoveCommand__Group__1__Impl )
            // InternalMyDsl.g:2116:2: rule__RemoveCommand__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__RemoveCommand__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RemoveCommand__Group__1"


    // $ANTLR start "rule__RemoveCommand__Group__1__Impl"
    // InternalMyDsl.g:2122:1: rule__RemoveCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__RemoveCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2126:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:2127:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:2127:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:2128:2: RULE_EXPRESSION
            {
             before(grammarAccess.getRemoveCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getRemoveCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RemoveCommand__Group__1__Impl"


    // $ANTLR start "rule__MessageCommand__Group__0"
    // InternalMyDsl.g:2138:1: rule__MessageCommand__Group__0 : rule__MessageCommand__Group__0__Impl rule__MessageCommand__Group__1 ;
    public final void rule__MessageCommand__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2142:1: ( rule__MessageCommand__Group__0__Impl rule__MessageCommand__Group__1 )
            // InternalMyDsl.g:2143:2: rule__MessageCommand__Group__0__Impl rule__MessageCommand__Group__1
            {
            pushFollow(FOLLOW_4);
            rule__MessageCommand__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MessageCommand__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__0"


    // $ANTLR start "rule__MessageCommand__Group__0__Impl"
    // InternalMyDsl.g:2150:1: rule__MessageCommand__Group__0__Impl : ( '#message' ) ;
    public final void rule__MessageCommand__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2154:1: ( ( '#message' ) )
            // InternalMyDsl.g:2155:1: ( '#message' )
            {
            // InternalMyDsl.g:2155:1: ( '#message' )
            // InternalMyDsl.g:2156:2: '#message'
            {
             before(grammarAccess.getMessageCommandAccess().getMessageKeyword_0()); 
            match(input,34,FOLLOW_2); 
             after(grammarAccess.getMessageCommandAccess().getMessageKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__0__Impl"


    // $ANTLR start "rule__MessageCommand__Group__1"
    // InternalMyDsl.g:2165:1: rule__MessageCommand__Group__1 : rule__MessageCommand__Group__1__Impl rule__MessageCommand__Group__2 ;
    public final void rule__MessageCommand__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2169:1: ( rule__MessageCommand__Group__1__Impl rule__MessageCommand__Group__2 )
            // InternalMyDsl.g:2170:2: rule__MessageCommand__Group__1__Impl rule__MessageCommand__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__MessageCommand__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MessageCommand__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__1"


    // $ANTLR start "rule__MessageCommand__Group__1__Impl"
    // InternalMyDsl.g:2177:1: rule__MessageCommand__Group__1__Impl : ( RULE_EXPRESSION ) ;
    public final void rule__MessageCommand__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2181:1: ( ( RULE_EXPRESSION ) )
            // InternalMyDsl.g:2182:1: ( RULE_EXPRESSION )
            {
            // InternalMyDsl.g:2182:1: ( RULE_EXPRESSION )
            // InternalMyDsl.g:2183:2: RULE_EXPRESSION
            {
             before(grammarAccess.getMessageCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 
            match(input,RULE_EXPRESSION,FOLLOW_2); 
             after(grammarAccess.getMessageCommandAccess().getEXPRESSIONTerminalRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__1__Impl"


    // $ANTLR start "rule__MessageCommand__Group__2"
    // InternalMyDsl.g:2192:1: rule__MessageCommand__Group__2 : rule__MessageCommand__Group__2__Impl rule__MessageCommand__Group__3 ;
    public final void rule__MessageCommand__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2196:1: ( rule__MessageCommand__Group__2__Impl rule__MessageCommand__Group__3 )
            // InternalMyDsl.g:2197:2: rule__MessageCommand__Group__2__Impl rule__MessageCommand__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__MessageCommand__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MessageCommand__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__2"


    // $ANTLR start "rule__MessageCommand__Group__2__Impl"
    // InternalMyDsl.g:2204:1: rule__MessageCommand__Group__2__Impl : ( ( RULE_EXPRESSION )* ) ;
    public final void rule__MessageCommand__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2208:1: ( ( ( RULE_EXPRESSION )* ) )
            // InternalMyDsl.g:2209:1: ( ( RULE_EXPRESSION )* )
            {
            // InternalMyDsl.g:2209:1: ( ( RULE_EXPRESSION )* )
            // InternalMyDsl.g:2210:2: ( RULE_EXPRESSION )*
            {
             before(grammarAccess.getMessageCommandAccess().getEXPRESSIONTerminalRuleCall_2()); 
            // InternalMyDsl.g:2211:2: ( RULE_EXPRESSION )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==RULE_EXPRESSION) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalMyDsl.g:2211:3: RULE_EXPRESSION
            	    {
            	    match(input,RULE_EXPRESSION,FOLLOW_20); 

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getMessageCommandAccess().getEXPRESSIONTerminalRuleCall_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__2__Impl"


    // $ANTLR start "rule__MessageCommand__Group__3"
    // InternalMyDsl.g:2219:1: rule__MessageCommand__Group__3 : rule__MessageCommand__Group__3__Impl ;
    public final void rule__MessageCommand__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2223:1: ( rule__MessageCommand__Group__3__Impl )
            // InternalMyDsl.g:2224:2: rule__MessageCommand__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MessageCommand__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__3"


    // $ANTLR start "rule__MessageCommand__Group__3__Impl"
    // InternalMyDsl.g:2230:1: rule__MessageCommand__Group__3__Impl : ( '#endmessage' ) ;
    public final void rule__MessageCommand__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2234:1: ( ( '#endmessage' ) )
            // InternalMyDsl.g:2235:1: ( '#endmessage' )
            {
            // InternalMyDsl.g:2235:1: ( '#endmessage' )
            // InternalMyDsl.g:2236:2: '#endmessage'
            {
             before(grammarAccess.getMessageCommandAccess().getEndmessageKeyword_3()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getMessageCommandAccess().getEndmessageKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MessageCommand__Group__3__Impl"


    // $ANTLR start "rule__Model__CommandsAssignment"
    // InternalMyDsl.g:2246:1: rule__Model__CommandsAssignment : ( ruleCommands ) ;
    public final void rule__Model__CommandsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalMyDsl.g:2250:1: ( ( ruleCommands ) )
            // InternalMyDsl.g:2251:2: ( ruleCommands )
            {
            // InternalMyDsl.g:2251:2: ( ruleCommands )
            // InternalMyDsl.g:2252:3: ruleCommands
            {
             before(grammarAccess.getModelAccess().getCommandsCommandsParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleCommands();

            state._fsp--;

             after(grammarAccess.getModelAccess().getCommandsCommandsParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__CommandsAssignment"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x000000006D40A002L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x000000000140C030L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000001408032L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x00000007ED53A030L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x00000007ED52A032L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000080002L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000007ED72A030L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000809030L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000009032L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000002400000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x000000007D48A020L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x000000006D48A022L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000080010L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000800000010L});

}