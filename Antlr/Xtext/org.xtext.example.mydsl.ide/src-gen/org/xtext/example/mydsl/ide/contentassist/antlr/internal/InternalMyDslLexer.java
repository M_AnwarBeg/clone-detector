package org.xtext.example.mydsl.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslLexer extends Lexer {
    public static final int RULE_STRING=8;
    public static final int RULE_EXPRESSION=4;
    public static final int RULE_SL_COMMENT=10;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__12=12;
    public static final int T__34=34;
    public static final int T__13=13;
    public static final int T__35=35;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=6;
    public static final int RULE_WS=5;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=9;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalMyDslLexer() {;} 
    public InternalMyDslLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalMyDslLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalMyDsl.g"; }

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:11:7: ( '=' )
            // InternalMyDsl.g:11:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:12:7: ( '#adapt:' )
            // InternalMyDsl.g:12:9: '#adapt:'
            {
            match("#adapt:"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:13:7: ( '#endadapt' )
            // InternalMyDsl.g:13:9: '#endadapt'
            {
            match("#endadapt"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:14:7: ( '#insert-after' )
            // InternalMyDsl.g:14:9: '#insert-after'
            {
            match("#insert-after"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:15:7: ( '#endinsert' )
            // InternalMyDsl.g:15:9: '#endinsert'
            {
            match("#endinsert"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:16:7: ( '#setloop' )
            // InternalMyDsl.g:16:9: '#setloop'
            {
            match("#setloop"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:17:7: ( '#endsetloop' )
            // InternalMyDsl.g:17:9: '#endsetloop'
            {
            match("#endsetloop"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:18:7: ( ',' )
            // InternalMyDsl.g:18:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:19:7: ( '#break' )
            // InternalMyDsl.g:19:9: '#break'
            {
            match("#break"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:20:7: ( '#endbreak' )
            // InternalMyDsl.g:20:9: '#endbreak'
            {
            match("#endbreak"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:21:7: ( '#option' )
            // InternalMyDsl.g:21:9: '#option'
            {
            match("#option"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:22:7: ( '#endoption' )
            // InternalMyDsl.g:22:9: '#endoption'
            {
            match("#endoption"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:23:7: ( '#select' )
            // InternalMyDsl.g:23:9: '#select'
            {
            match("#select"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:24:7: ( '#endselect' )
            // InternalMyDsl.g:24:9: '#endselect'
            {
            match("#endselect"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:25:7: ( '#output' )
            // InternalMyDsl.g:25:9: '#output'
            {
            match("#output"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:26:7: ( '#while' )
            // InternalMyDsl.g:26:9: '#while'
            {
            match("#while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:27:7: ( '#endwhile' )
            // InternalMyDsl.g:27:9: '#endwhile'
            {
            match("#endwhile"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:28:7: ( '%' )
            // InternalMyDsl.g:28:9: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:29:7: ( '#set' )
            // InternalMyDsl.g:29:9: '#set'
            {
            match("#set"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:30:7: ( '#outdir' )
            // InternalMyDsl.g:30:9: '#outdir'
            {
            match("#outdir"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:31:7: ( '#outfile' )
            // InternalMyDsl.g:31:9: '#outfile'
            {
            match("#outfile"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:32:7: ( '#remove' )
            // InternalMyDsl.g:32:9: '#remove'
            {
            match("#remove"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:33:7: ( '#message' )
            // InternalMyDsl.g:33:9: '#message'
            {
            match("#message"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:34:7: ( '#endmessage' )
            // InternalMyDsl.g:34:9: '#endmessage'
            {
            match("#endmessage"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2261:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalMyDsl.g:2261:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalMyDsl.g:2261:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='\t' && LA1_0<='\n')||LA1_0=='\r'||LA1_0==' ') ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalMyDsl.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_EXPRESSION"
    public final void mRULE_EXPRESSION() throws RecognitionException {
        try {
            int _type = RULE_EXPRESSION;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2263:17: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '@' | '.' | '?' | '\\'' | ':' | '\"' | '>' | '<' | '/' | '_' | ';' | '(' | ')' | '&' | '!' | '#' | '*' | '+' | '[' | ']' | '{' | '}' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '@' | '.' | '?' | '\\'' | ':' | '\"' | '>' | '<' | '/' | '_' | ';' | '(' | ')' | '&' | '!' | '#' | '%' | '*' | '+' | '[' | ']' | '{' | '}' )* )
            // InternalMyDsl.g:2263:19: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '@' | '.' | '?' | '\\'' | ':' | '\"' | '>' | '<' | '/' | '_' | ';' | '(' | ')' | '&' | '!' | '#' | '*' | '+' | '[' | ']' | '{' | '}' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '@' | '.' | '?' | '\\'' | ':' | '\"' | '>' | '<' | '/' | '_' | ';' | '(' | ')' | '&' | '!' | '#' | '%' | '*' | '+' | '[' | ']' | '{' | '}' )*
            {
            if ( (input.LA(1)>='!' && input.LA(1)<='#')||(input.LA(1)>='&' && input.LA(1)<='+')||(input.LA(1)>='-' && input.LA(1)<='<')||(input.LA(1)>='>' && input.LA(1)<='[')||input.LA(1)==']'||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='{')||input.LA(1)=='}' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalMyDsl.g:2263:141: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '-' | '@' | '.' | '?' | '\\'' | ':' | '\"' | '>' | '<' | '/' | '_' | ';' | '(' | ')' | '&' | '!' | '#' | '%' | '*' | '+' | '[' | ']' | '{' | '}' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='!' && LA2_0<='#')||(LA2_0>='%' && LA2_0<='+')||(LA2_0>='-' && LA2_0<='<')||(LA2_0>='>' && LA2_0<='[')||LA2_0==']'||LA2_0=='_'||(LA2_0>='a' && LA2_0<='{')||LA2_0=='}') ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalMyDsl.g:
            	    {
            	    if ( (input.LA(1)>='!' && input.LA(1)<='#')||(input.LA(1)>='%' && input.LA(1)<='+')||(input.LA(1)>='-' && input.LA(1)<='<')||(input.LA(1)>='>' && input.LA(1)<='[')||input.LA(1)==']'||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='{')||input.LA(1)=='}' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_EXPRESSION"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2265:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalMyDsl.g:2265:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalMyDsl.g:2265:11: ( '^' )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0=='^') ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // InternalMyDsl.g:2265:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalMyDsl.g:2265:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')||(LA4_0>='A' && LA4_0<='Z')||LA4_0=='_'||(LA4_0>='a' && LA4_0<='z')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalMyDsl.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2267:10: ( ( '0' .. '9' )+ )
            // InternalMyDsl.g:2267:12: ( '0' .. '9' )+
            {
            // InternalMyDsl.g:2267:12: ( '0' .. '9' )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalMyDsl.g:2267:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2269:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalMyDsl.g:2269:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalMyDsl.g:2269:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\"') ) {
                alt8=1;
            }
            else if ( (LA8_0=='\'') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalMyDsl.g:2269:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalMyDsl.g:2269:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='!')||(LA6_0>='#' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalMyDsl.g:2269:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalMyDsl.g:2269:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalMyDsl.g:2269:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalMyDsl.g:2269:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop7:
                    do {
                        int alt7=3;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0=='\\') ) {
                            alt7=1;
                        }
                        else if ( ((LA7_0>='\u0000' && LA7_0<='&')||(LA7_0>='(' && LA7_0<='[')||(LA7_0>=']' && LA7_0<='\uFFFF')) ) {
                            alt7=2;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // InternalMyDsl.g:2269:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalMyDsl.g:2269:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2271:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalMyDsl.g:2271:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalMyDsl.g:2271:24: ( options {greedy=false; } : . )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='*') ) {
                    int LA9_1 = input.LA(2);

                    if ( (LA9_1=='/') ) {
                        alt9=2;
                    }
                    else if ( ((LA9_1>='\u0000' && LA9_1<='.')||(LA9_1>='0' && LA9_1<='\uFFFF')) ) {
                        alt9=1;
                    }


                }
                else if ( ((LA9_0>='\u0000' && LA9_0<=')')||(LA9_0>='+' && LA9_0<='\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalMyDsl.g:2271:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2273:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalMyDsl.g:2273:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalMyDsl.g:2273:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='\u0000' && LA10_0<='\t')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalMyDsl.g:2273:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalMyDsl.g:2273:40: ( ( '\\r' )? '\\n' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='\n'||LA12_0=='\r') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalMyDsl.g:2273:41: ( '\\r' )? '\\n'
                    {
                    // InternalMyDsl.g:2273:41: ( '\\r' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0=='\r') ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // InternalMyDsl.g:2273:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalMyDsl.g:2275:16: ( . )
            // InternalMyDsl.g:2275:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalMyDsl.g:1:8: ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | RULE_WS | RULE_EXPRESSION | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_ANY_OTHER )
        int alt13=32;
        alt13 = dfa13.predict(input);
        switch (alt13) {
            case 1 :
                // InternalMyDsl.g:1:10: T__12
                {
                mT__12(); 

                }
                break;
            case 2 :
                // InternalMyDsl.g:1:16: T__13
                {
                mT__13(); 

                }
                break;
            case 3 :
                // InternalMyDsl.g:1:22: T__14
                {
                mT__14(); 

                }
                break;
            case 4 :
                // InternalMyDsl.g:1:28: T__15
                {
                mT__15(); 

                }
                break;
            case 5 :
                // InternalMyDsl.g:1:34: T__16
                {
                mT__16(); 

                }
                break;
            case 6 :
                // InternalMyDsl.g:1:40: T__17
                {
                mT__17(); 

                }
                break;
            case 7 :
                // InternalMyDsl.g:1:46: T__18
                {
                mT__18(); 

                }
                break;
            case 8 :
                // InternalMyDsl.g:1:52: T__19
                {
                mT__19(); 

                }
                break;
            case 9 :
                // InternalMyDsl.g:1:58: T__20
                {
                mT__20(); 

                }
                break;
            case 10 :
                // InternalMyDsl.g:1:64: T__21
                {
                mT__21(); 

                }
                break;
            case 11 :
                // InternalMyDsl.g:1:70: T__22
                {
                mT__22(); 

                }
                break;
            case 12 :
                // InternalMyDsl.g:1:76: T__23
                {
                mT__23(); 

                }
                break;
            case 13 :
                // InternalMyDsl.g:1:82: T__24
                {
                mT__24(); 

                }
                break;
            case 14 :
                // InternalMyDsl.g:1:88: T__25
                {
                mT__25(); 

                }
                break;
            case 15 :
                // InternalMyDsl.g:1:94: T__26
                {
                mT__26(); 

                }
                break;
            case 16 :
                // InternalMyDsl.g:1:100: T__27
                {
                mT__27(); 

                }
                break;
            case 17 :
                // InternalMyDsl.g:1:106: T__28
                {
                mT__28(); 

                }
                break;
            case 18 :
                // InternalMyDsl.g:1:112: T__29
                {
                mT__29(); 

                }
                break;
            case 19 :
                // InternalMyDsl.g:1:118: T__30
                {
                mT__30(); 

                }
                break;
            case 20 :
                // InternalMyDsl.g:1:124: T__31
                {
                mT__31(); 

                }
                break;
            case 21 :
                // InternalMyDsl.g:1:130: T__32
                {
                mT__32(); 

                }
                break;
            case 22 :
                // InternalMyDsl.g:1:136: T__33
                {
                mT__33(); 

                }
                break;
            case 23 :
                // InternalMyDsl.g:1:142: T__34
                {
                mT__34(); 

                }
                break;
            case 24 :
                // InternalMyDsl.g:1:148: T__35
                {
                mT__35(); 

                }
                break;
            case 25 :
                // InternalMyDsl.g:1:154: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 26 :
                // InternalMyDsl.g:1:162: RULE_EXPRESSION
                {
                mRULE_EXPRESSION(); 

                }
                break;
            case 27 :
                // InternalMyDsl.g:1:178: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 28 :
                // InternalMyDsl.g:1:186: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 29 :
                // InternalMyDsl.g:1:195: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 30 :
                // InternalMyDsl.g:1:207: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 31 :
                // InternalMyDsl.g:1:223: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 32 :
                // InternalMyDsl.g:1:239: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA13 dfa13 = new DFA13(this);
    static final String DFA13_eotS =
        "\2\uffff\1\30\3\uffff\1\30\1\15\4\30\3\uffff\11\30\4\uffff\1\30\1\uffff\2\30\2\uffff\1\30\1\uffff\15\30\1\uffff\2\30\1\uffff\3\30\1\113\22\30\1\uffff\24\30\1\164\4\30\1\171\2\30\1\174\12\30\1\u0087\1\uffff\1\u0088\1\u0089\1\u008a\1\30\1\uffff\1\u008c\1\30\1\uffff\11\30\1\u0097\4\uffff\1\u0098\1\uffff\1\u0099\1\u009a\3\30\1\u009e\1\30\1\u00a0\2\30\4\uffff\1\u00a3\1\30\1\u00a5\1\uffff\1\u00a6\1\uffff\2\30\1\uffff\1\u00a9\2\uffff\1\u00aa\1\30\2\uffff\1\30\1\u00ad\1\uffff";
    static final String DFA13_eofS =
        "\u00ae\uffff";
    static final String DFA13_minS =
        "\1\0\1\uffff\1\141\3\uffff\1\60\1\101\1\60\2\0\1\52\3\uffff\1\144\2\156\1\145\1\162\1\160\1\150\2\145\4\uffff\1\60\1\uffff\1\60\1\0\2\uffff\1\0\1\uffff\2\0\1\141\1\144\1\163\1\154\1\145\2\164\1\151\1\155\1\163\1\0\1\uffff\2\0\1\uffff\1\160\1\141\1\145\1\41\1\145\1\141\1\151\1\144\1\154\1\157\1\163\1\0\1\164\1\144\1\156\1\145\1\162\1\160\1\150\1\145\1\162\1\157\1\uffff\1\143\1\153\1\157\1\165\2\151\1\145\1\166\1\141\1\72\1\141\1\163\1\154\1\145\1\164\1\151\1\163\1\164\1\157\1\164\1\41\1\156\1\164\1\162\1\154\1\41\1\145\1\147\1\41\1\160\1\145\1\154\1\145\1\141\1\151\1\154\1\163\1\55\1\160\1\41\1\uffff\3\41\1\145\1\uffff\1\41\1\145\1\uffff\1\164\1\162\1\157\1\143\1\153\1\157\1\145\2\141\1\41\4\uffff\1\41\1\uffff\2\41\1\164\1\157\1\164\1\41\1\156\1\41\1\147\1\146\4\uffff\1\41\1\160\1\41\1\uffff\1\41\1\uffff\1\145\1\164\1\uffff\1\41\2\uffff\1\41\1\145\2\uffff\1\162\1\41\1\uffff";
    static final String DFA13_maxS =
        "\1\uffff\1\uffff\1\167\3\uffff\2\172\1\71\2\uffff\1\57\3\uffff\1\144\2\156\1\145\1\162\1\165\1\150\2\145\4\uffff\1\172\1\uffff\1\71\1\uffff\2\uffff\1\uffff\1\uffff\2\uffff\1\141\1\144\1\163\1\164\1\145\2\164\1\151\1\155\1\163\1\uffff\1\uffff\2\uffff\1\uffff\1\160\1\167\1\145\1\175\1\145\1\141\1\151\1\160\1\154\1\157\1\163\1\uffff\1\164\1\144\1\156\1\145\1\162\1\160\1\150\1\145\1\162\1\157\1\uffff\1\143\1\153\1\157\1\165\2\151\1\145\1\166\1\141\1\72\1\141\1\163\1\164\1\145\1\164\1\151\1\163\1\164\1\157\1\164\1\175\1\156\1\164\1\162\1\154\1\175\1\145\1\147\1\175\1\160\1\145\1\154\1\145\1\141\1\151\1\154\1\163\1\55\1\160\1\175\1\uffff\3\175\1\145\1\uffff\1\175\1\145\1\uffff\1\164\1\162\1\157\1\143\1\153\1\157\1\145\2\141\1\175\4\uffff\1\175\1\uffff\2\175\1\164\1\157\1\164\1\175\1\156\1\175\1\147\1\146\4\uffff\1\175\1\160\1\175\1\uffff\1\175\1\uffff\1\145\1\164\1\uffff\1\175\2\uffff\1\175\1\145\2\uffff\1\162\1\175\1\uffff";
    static final String DFA13_acceptS =
        "\1\uffff\1\1\1\uffff\1\10\1\22\1\31\6\uffff\1\32\1\40\1\1\11\uffff\1\32\1\10\1\22\1\31\1\uffff\1\33\2\uffff\1\35\1\32\1\uffff\1\32\15\uffff\1\36\2\uffff\1\37\26\uffff\1\23\50\uffff\1\11\4\uffff\1\20\2\uffff\1\2\12\uffff\1\15\1\13\1\17\1\24\1\uffff\1\26\12\uffff\1\6\1\25\1\27\1\3\3\uffff\1\12\1\uffff\1\21\2\uffff\1\5\1\uffff\1\16\1\14\2\uffff\1\7\1\30\2\uffff\1\4";
    static final String DFA13_specialS =
        "\1\10\10\uffff\1\0\1\6\24\uffff\1\5\2\uffff\1\2\1\uffff\1\1\1\12\12\uffff\1\4\1\uffff\1\11\1\7\14\uffff\1\3\155\uffff}>";
    static final String[] DFA13_transitionS = {
            "\11\15\2\5\2\15\1\5\22\15\1\5\1\14\1\11\1\2\1\15\1\4\1\14\1\12\4\14\1\3\2\14\1\13\12\10\3\14\1\1\3\14\32\6\1\14\1\15\1\14\1\7\1\6\1\15\32\6\1\14\1\15\1\14\uff82\15",
            "",
            "\1\17\1\23\2\uffff\1\20\3\uffff\1\21\3\uffff\1\27\1\uffff\1\24\2\uffff\1\26\1\22\3\uffff\1\25",
            "",
            "",
            "",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "\32\35\4\uffff\1\35\1\uffff\32\35",
            "\12\36",
            "\41\40\1\37\1\41\1\37\1\40\7\37\1\40\20\37\1\40\36\37\1\40\1\37\1\40\1\37\1\40\33\37\1\40\1\37\uff82\40",
            "\41\40\3\42\1\40\2\42\1\43\4\42\1\40\20\42\1\40\36\42\1\40\1\42\1\40\1\42\1\40\33\42\1\40\1\42\uff82\40",
            "\1\44\4\uffff\1\45",
            "",
            "",
            "",
            "\1\46",
            "\1\47",
            "\1\50",
            "\1\51",
            "\1\52",
            "\1\53\4\uffff\1\54",
            "\1\55",
            "\1\56",
            "\1\57",
            "",
            "",
            "",
            "",
            "\12\34\7\uffff\32\34\4\uffff\1\34\1\uffff\32\34",
            "",
            "\12\36",
            "\41\40\1\37\1\41\1\37\1\40\7\37\1\40\20\37\1\40\36\37\1\40\1\37\1\40\1\37\1\40\33\37\1\40\1\37\uff82\40",
            "",
            "",
            "\41\40\3\42\1\40\2\42\1\43\4\42\1\40\20\42\1\40\36\42\1\40\1\42\1\40\1\42\1\40\33\42\1\40\1\42\uff82\40",
            "",
            "\41\61\3\62\1\61\5\62\1\60\1\62\1\61\20\62\1\61\36\62\1\61\1\62\1\61\1\62\1\61\33\62\1\61\1\62\uff82\61",
            "\41\64\3\63\1\64\7\63\1\64\20\63\1\64\36\63\1\64\1\63\1\64\1\63\1\64\33\63\1\64\1\63\uff82\64",
            "\1\65",
            "\1\66",
            "\1\67",
            "\1\71\7\uffff\1\70",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\75",
            "\1\76",
            "\1\77",
            "\41\61\3\62\1\61\5\62\1\60\1\62\1\61\2\62\1\100\15\62\1\61\36\62\1\61\1\62\1\61\1\62\1\61\33\62\1\61\1\62\uff82\61",
            "",
            "\41\61\3\62\1\61\5\62\1\60\1\62\1\61\20\62\1\61\36\62\1\61\1\62\1\61\1\62\1\61\33\62\1\61\1\62\uff82\61",
            "\41\64\3\63\1\64\7\63\1\64\20\63\1\64\36\63\1\64\1\63\1\64\1\63\1\64\33\63\1\64\1\63\uff82\64",
            "",
            "\1\101",
            "\1\102\1\105\6\uffff\1\103\3\uffff\1\110\1\uffff\1\106\3\uffff\1\104\3\uffff\1\107",
            "\1\111",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\13\30\1\112\17\30\1\uffff\1\30",
            "\1\114",
            "\1\115",
            "\1\116",
            "\1\120\1\uffff\1\121\11\uffff\1\117",
            "\1\122",
            "\1\123",
            "\1\124",
            "\41\61\3\62\1\61\5\62\1\60\1\62\1\61\20\62\1\61\36\62\1\61\1\62\1\61\1\62\1\61\33\62\1\61\1\62\uff82\61",
            "\1\125",
            "\1\126",
            "\1\127",
            "\1\130",
            "\1\131",
            "\1\132",
            "\1\133",
            "\1\134",
            "\1\135",
            "\1\136",
            "",
            "\1\137",
            "\1\140",
            "\1\141",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "\1\151",
            "\1\152",
            "\1\154\7\uffff\1\153",
            "\1\155",
            "\1\156",
            "\1\157",
            "\1\160",
            "\1\161",
            "\1\162",
            "\1\163",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\165",
            "\1\166",
            "\1\167",
            "\1\170",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\172",
            "\1\173",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\175",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u008b",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u008d",
            "",
            "\1\u008e",
            "\1\u008f",
            "\1\u0090",
            "\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "",
            "",
            "",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u009f",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u00a1",
            "\1\u00a2",
            "",
            "",
            "",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u00a4",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "",
            "\1\u00a7",
            "\1\u00a8",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "",
            "",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            "\1\u00ab",
            "",
            "",
            "\1\u00ac",
            "\3\30\1\uffff\7\30\1\uffff\20\30\1\uffff\36\30\1\uffff\1\30\1\uffff\1\30\1\uffff\33\30\1\uffff\1\30",
            ""
    };

    static final short[] DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
    static final short[] DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
    static final char[] DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
    static final char[] DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
    static final short[] DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
    static final short[] DFA13_special = DFA.unpackEncodedString(DFA13_specialS);
    static final short[][] DFA13_transition;

    static {
        int numStates = DFA13_transitionS.length;
        DFA13_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
        }
    }

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = DFA13_eot;
            this.eof = DFA13_eof;
            this.min = DFA13_min;
            this.max = DFA13_max;
            this.accept = DFA13_accept;
            this.special = DFA13_special;
            this.transition = DFA13_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | RULE_WS | RULE_EXPRESSION | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA13_9 = input.LA(1);

                        s = -1;
                        if ( (LA13_9=='!'||LA13_9=='#'||(LA13_9>='%' && LA13_9<='+')||(LA13_9>='-' && LA13_9<='<')||(LA13_9>='>' && LA13_9<='[')||LA13_9==']'||LA13_9=='_'||(LA13_9>='a' && LA13_9<='{')||LA13_9=='}') ) {s = 31;}

                        else if ( ((LA13_9>='\u0000' && LA13_9<=' ')||LA13_9=='$'||LA13_9==','||LA13_9=='='||LA13_9=='\\'||LA13_9=='^'||LA13_9=='`'||LA13_9=='|'||(LA13_9>='~' && LA13_9<='\uFFFF')) ) {s = 32;}

                        else if ( (LA13_9=='\"') ) {s = 33;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA13_36 = input.LA(1);

                        s = -1;
                        if ( (LA13_36=='*') ) {s = 48;}

                        else if ( ((LA13_36>='\u0000' && LA13_36<=' ')||LA13_36=='$'||LA13_36==','||LA13_36=='='||LA13_36=='\\'||LA13_36=='^'||LA13_36=='`'||LA13_36=='|'||(LA13_36>='~' && LA13_36<='\uFFFF')) ) {s = 49;}

                        else if ( ((LA13_36>='!' && LA13_36<='#')||(LA13_36>='%' && LA13_36<=')')||LA13_36=='+'||(LA13_36>='-' && LA13_36<='<')||(LA13_36>='>' && LA13_36<='[')||LA13_36==']'||LA13_36=='_'||(LA13_36>='a' && LA13_36<='{')||LA13_36=='}') ) {s = 50;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA13_34 = input.LA(1);

                        s = -1;
                        if ( (LA13_34=='\'') ) {s = 35;}

                        else if ( ((LA13_34>='!' && LA13_34<='#')||(LA13_34>='%' && LA13_34<='&')||(LA13_34>='(' && LA13_34<='+')||(LA13_34>='-' && LA13_34<='<')||(LA13_34>='>' && LA13_34<='[')||LA13_34==']'||LA13_34=='_'||(LA13_34>='a' && LA13_34<='{')||LA13_34=='}') ) {s = 34;}

                        else if ( ((LA13_34>='\u0000' && LA13_34<=' ')||LA13_34=='$'||LA13_34==','||LA13_34=='='||LA13_34=='\\'||LA13_34=='^'||LA13_34=='`'||LA13_34=='|'||(LA13_34>='~' && LA13_34<='\uFFFF')) ) {s = 32;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA13_64 = input.LA(1);

                        s = -1;
                        if ( (LA13_64=='*') ) {s = 48;}

                        else if ( ((LA13_64>='!' && LA13_64<='#')||(LA13_64>='%' && LA13_64<=')')||LA13_64=='+'||(LA13_64>='-' && LA13_64<='<')||(LA13_64>='>' && LA13_64<='[')||LA13_64==']'||LA13_64=='_'||(LA13_64>='a' && LA13_64<='{')||LA13_64=='}') ) {s = 50;}

                        else if ( ((LA13_64>='\u0000' && LA13_64<=' ')||LA13_64=='$'||LA13_64==','||LA13_64=='='||LA13_64=='\\'||LA13_64=='^'||LA13_64=='`'||LA13_64=='|'||(LA13_64>='~' && LA13_64<='\uFFFF')) ) {s = 49;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA13_48 = input.LA(1);

                        s = -1;
                        if ( (LA13_48=='/') ) {s = 64;}

                        else if ( (LA13_48=='*') ) {s = 48;}

                        else if ( ((LA13_48>='!' && LA13_48<='#')||(LA13_48>='%' && LA13_48<=')')||LA13_48=='+'||(LA13_48>='-' && LA13_48<='.')||(LA13_48>='0' && LA13_48<='<')||(LA13_48>='>' && LA13_48<='[')||LA13_48==']'||LA13_48=='_'||(LA13_48>='a' && LA13_48<='{')||LA13_48=='}') ) {s = 50;}

                        else if ( ((LA13_48>='\u0000' && LA13_48<=' ')||LA13_48=='$'||LA13_48==','||LA13_48=='='||LA13_48=='\\'||LA13_48=='^'||LA13_48=='`'||LA13_48=='|'||(LA13_48>='~' && LA13_48<='\uFFFF')) ) {s = 49;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA13_31 = input.LA(1);

                        s = -1;
                        if ( (LA13_31=='\"') ) {s = 33;}

                        else if ( ((LA13_31>='\u0000' && LA13_31<=' ')||LA13_31=='$'||LA13_31==','||LA13_31=='='||LA13_31=='\\'||LA13_31=='^'||LA13_31=='`'||LA13_31=='|'||(LA13_31>='~' && LA13_31<='\uFFFF')) ) {s = 32;}

                        else if ( (LA13_31=='!'||LA13_31=='#'||(LA13_31>='%' && LA13_31<='+')||(LA13_31>='-' && LA13_31<='<')||(LA13_31>='>' && LA13_31<='[')||LA13_31==']'||LA13_31=='_'||(LA13_31>='a' && LA13_31<='{')||LA13_31=='}') ) {s = 31;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA13_10 = input.LA(1);

                        s = -1;
                        if ( ((LA13_10>='!' && LA13_10<='#')||(LA13_10>='%' && LA13_10<='&')||(LA13_10>='(' && LA13_10<='+')||(LA13_10>='-' && LA13_10<='<')||(LA13_10>='>' && LA13_10<='[')||LA13_10==']'||LA13_10=='_'||(LA13_10>='a' && LA13_10<='{')||LA13_10=='}') ) {s = 34;}

                        else if ( ((LA13_10>='\u0000' && LA13_10<=' ')||LA13_10=='$'||LA13_10==','||LA13_10=='='||LA13_10=='\\'||LA13_10=='^'||LA13_10=='`'||LA13_10=='|'||(LA13_10>='~' && LA13_10<='\uFFFF')) ) {s = 32;}

                        else if ( (LA13_10=='\'') ) {s = 35;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 7 : 
                        int LA13_51 = input.LA(1);

                        s = -1;
                        if ( ((LA13_51>='!' && LA13_51<='#')||(LA13_51>='%' && LA13_51<='+')||(LA13_51>='-' && LA13_51<='<')||(LA13_51>='>' && LA13_51<='[')||LA13_51==']'||LA13_51=='_'||(LA13_51>='a' && LA13_51<='{')||LA13_51=='}') ) {s = 51;}

                        else if ( ((LA13_51>='\u0000' && LA13_51<=' ')||LA13_51=='$'||LA13_51==','||LA13_51=='='||LA13_51=='\\'||LA13_51=='^'||LA13_51=='`'||LA13_51=='|'||(LA13_51>='~' && LA13_51<='\uFFFF')) ) {s = 52;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 8 : 
                        int LA13_0 = input.LA(1);

                        s = -1;
                        if ( (LA13_0=='=') ) {s = 1;}

                        else if ( (LA13_0=='#') ) {s = 2;}

                        else if ( (LA13_0==',') ) {s = 3;}

                        else if ( (LA13_0=='%') ) {s = 4;}

                        else if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {s = 5;}

                        else if ( ((LA13_0>='A' && LA13_0<='Z')||LA13_0=='_'||(LA13_0>='a' && LA13_0<='z')) ) {s = 6;}

                        else if ( (LA13_0=='^') ) {s = 7;}

                        else if ( ((LA13_0>='0' && LA13_0<='9')) ) {s = 8;}

                        else if ( (LA13_0=='\"') ) {s = 9;}

                        else if ( (LA13_0=='\'') ) {s = 10;}

                        else if ( (LA13_0=='/') ) {s = 11;}

                        else if ( (LA13_0=='!'||LA13_0=='&'||(LA13_0>='(' && LA13_0<='+')||(LA13_0>='-' && LA13_0<='.')||(LA13_0>=':' && LA13_0<='<')||(LA13_0>='>' && LA13_0<='@')||LA13_0=='['||LA13_0==']'||LA13_0=='{'||LA13_0=='}') ) {s = 12;}

                        else if ( ((LA13_0>='\u0000' && LA13_0<='\b')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='\u001F')||LA13_0=='$'||LA13_0=='\\'||LA13_0=='`'||LA13_0=='|'||(LA13_0>='~' && LA13_0<='\uFFFF')) ) {s = 13;}

                        if ( s>=0 ) return s;
                        break;
                    case 9 : 
                        int LA13_50 = input.LA(1);

                        s = -1;
                        if ( (LA13_50=='*') ) {s = 48;}

                        else if ( ((LA13_50>='!' && LA13_50<='#')||(LA13_50>='%' && LA13_50<=')')||LA13_50=='+'||(LA13_50>='-' && LA13_50<='<')||(LA13_50>='>' && LA13_50<='[')||LA13_50==']'||LA13_50=='_'||(LA13_50>='a' && LA13_50<='{')||LA13_50=='}') ) {s = 50;}

                        else if ( ((LA13_50>='\u0000' && LA13_50<=' ')||LA13_50=='$'||LA13_50==','||LA13_50=='='||LA13_50=='\\'||LA13_50=='^'||LA13_50=='`'||LA13_50=='|'||(LA13_50>='~' && LA13_50<='\uFFFF')) ) {s = 49;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
                    case 10 : 
                        int LA13_37 = input.LA(1);

                        s = -1;
                        if ( ((LA13_37>='!' && LA13_37<='#')||(LA13_37>='%' && LA13_37<='+')||(LA13_37>='-' && LA13_37<='<')||(LA13_37>='>' && LA13_37<='[')||LA13_37==']'||LA13_37=='_'||(LA13_37>='a' && LA13_37<='{')||LA13_37=='}') ) {s = 51;}

                        else if ( ((LA13_37>='\u0000' && LA13_37<=' ')||LA13_37=='$'||LA13_37==','||LA13_37=='='||LA13_37=='\\'||LA13_37=='^'||LA13_37=='`'||LA13_37=='|'||(LA13_37>='~' && LA13_37<='\uFFFF')) ) {s = 52;}

                        else s = 24;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 13, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}